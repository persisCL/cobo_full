object FormImprimirConvenio: TFormImprimirConvenio
  Left = 452
  Top = 287
  Caption = 'Impresi'#243'n de Convenio'
  ClientHeight = 631
  ClientWidth = 1141
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object RichEdit1: TRichEdit
    Left = 569
    Top = 8
    Width = 185
    Height = 89
    Lines.Strings = (
      'RichEdit1'
      'dfgmdflgksdlf'#241'jdlf'#241'jmhlkjmhlkdfjhlkdjlk'
      'hsdhdfgmdflgksdlf'#241'jdlf'#241'jmhlkjmhlkdfjhl'
      'kdjlkhsdhdfgmdflgksdlf'#241'jdlf'#241'jmhlkjmhl'
      'kdfjhlkdjlkhsdh')
    TabOrder = 0
  end
  object RBInterface: TRBInterface
    Report = ppReporteImpresionConvenio
    OrderIndex = 0
    Left = 769
    Top = 12
  end
  object ppReporteImpresionConvenio: TppReport
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Units = utMillimeters
    BeforePrint = ppReporteImpresionConvenioBeforePrint
    DeviceType = 'Printer'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 989
    Top = 192
    Version = '12.04'
    mmColumnWidth = 0
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 37835
      mmPrintPosition = 0
      object CampoRich: TppRichText
        UserName = 'CampoRich'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Tahoma'
        Font.Size = 8
        Font.Style = []
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'CampoRich'
        Stretch = True
        Transparent = True
        mmHeight = 33338
        mmLeft = 5027
        mmTop = 0
        mmWidth = 190236
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeftMargin = 794
      end
    end
    object ppParameterList1: TppParameterList
      object ppParameter1: TppParameter
        AutoSearchSettings.LogicalPrefix = []
        AutoSearchSettings.Mandatory = True
        DataType = dtString
        LookupSettings.DisplayType = dtNameOnly
        LookupSettings.SortOrder = soName
        Value = ''
        UserName = 'Parameter1'
      end
    end
  end
  object SPort: TSerialPort
    Active = False
    ComNumber = 1
    BaudRate = 19200
    Parity = ptNone
    StopBits = sbOne
    DataBits = 8
    FlowControl = Flow_Disabled
    Left = 829
    Top = 28
  end
  object ObtenerMensajeConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMensajeConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoImpresion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 989
    Top = 248
  end
  object spObtenerNumeroAnexo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerNumeroAnexo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 989
    Top = 296
  end
  object ppReporteImpresionFalabella: TppReport
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Units = utMillimeters
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 989
    Top = 136
    Version = '12.04'
    mmColumnWidth = 0
    object ppDetailBand: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 275000
      mmPrintPosition = 0
      object ppShape13: TppShape
        UserName = 'Shape13'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 158221
        mmTop = 110861
        mmWidth = 45244
        BandType = 4
      end
      object ppShape12: TppShape
        UserName = 'Shape12'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 95515
        mmTop = 110861
        mmWidth = 48683
        BandType = 4
      end
      object ppShape11: TppShape
        UserName = 'Shape11'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 29104
        mmTop = 110861
        mmWidth = 42069
        BandType = 4
      end
      object ppShape10: TppShape
        UserName = 'Shape10'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 70115
        mmTop = 118534
        mmWidth = 62177
        BandType = 4
      end
      object ppShape9: TppShape
        UserName = 'Shape9'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 45244
        mmTop = 103981
        mmWidth = 158221
        BandType = 4
      end
      object ppShape8: TppShape
        UserName = 'Shape8'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 63765
        mmTop = 96838
        mmWidth = 139965
        BandType = 4
      end
      object ppShape7: TppShape
        UserName = 'Shape7'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 73819
        mmTop = 83344
        mmWidth = 129646
        BandType = 4
      end
      object ppShape6: TppShape
        UserName = 'Shape6'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 168011
        mmTop = 67204
        mmWidth = 35719
        BandType = 4
      end
      object ppShape5: TppShape
        UserName = 'Shape5'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 105304
        mmTop = 67204
        mmWidth = 35719
        BandType = 4
      end
      object ppShape4: TppShape
        UserName = 'Shape4'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 36513
        mmTop = 67204
        mmWidth = 35719
        BandType = 4
      end
      object ppShape3: TppShape
        UserName = 'Shape3'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 141552
        mmTop = 54769
        mmWidth = 7938
        BandType = 4
      end
      object ppShape2: TppShape
        UserName = 'Shape2'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 130175
        mmTop = 54769
        mmWidth = 7938
        BandType = 4
      end
      object ppShape1: TppShape
        UserName = 'Shape1'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        mmHeight = 6085
        mmLeft = 118798
        mmTop = 54769
        mmWidth = 7938
        BandType = 4
      end
      object pplblMandanteNombre: TppLabel
        UserName = 'lblMandanteNombre'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblMandanteNombre'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 64823
        mmTop = 97631
        mmWidth = 137319
        BandType = 4
      end
      object pplblMandanteDomicilio: TppLabel
        UserName = 'lblMandanteDomicilio'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblMandanteDomicilio'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 47625
        mmTop = 104775
        mmWidth = 154252
        BandType = 4
      end
      object pplblDuenoVehiculo: TppLabel
        UserName = 'lblDuenoVehiculo'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblDuenoVehiculo'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 74877
        mmTop = 84138
        mmWidth = 125148
        BandType = 4
      end
      object pplblMandanteRUT: TppLabel
        UserName = 'lblMandanteRUT'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblMandanteRUT'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 30427
        mmTop = 111654
        mmWidth = 38365
        BandType = 4
      end
      object pplblMandanteTelefono: TppLabel
        UserName = 'lblMandanteTelefono'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblMandanteTelefono'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 96838
        mmTop = 111654
        mmWidth = 44979
        BandType = 4
      end
      object pplblMandanteNumeroTarjetaCMR: TppLabel
        UserName = 'lblMandanteNumeroTarjetaCMR'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblMandanteNumeroTarjetaCMR'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 72231
        mmTop = 119327
        mmWidth = 58208
        BandType = 4
      end
      object pplblPatente1: TppLabel
        UserName = 'lblPatente1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblPatente1'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 38365
        mmTop = 67998
        mmWidth = 33338
        BandType = 4
      end
      object pplblPatente2: TppLabel
        UserName = 'lblPatente2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblPatente2'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 107686
        mmTop = 67998
        mmWidth = 32015
        BandType = 4
      end
      object pplblPatente3: TppLabel
        UserName = 'lblPatente3'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblPatente3'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 169069
        mmTop = 67998
        mmWidth = 33602
        BandType = 4
      end
      object pplblDia: TppLabel
        UserName = 'lblDia'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblDia'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5027
        mmLeft = 119327
        mmTop = 55298
        mmWidth = 6879
        BandType = 4
      end
      object pplblMes: TppLabel
        UserName = 'lblMes'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblMes'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5027
        mmLeft = 130704
        mmTop = 55298
        mmWidth = 6879
        BandType = 4
      end
      object pplblAnio: TppLabel
        UserName = 'lblAnio'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblAnio'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5027
        mmLeft = 142082
        mmTop = 55298
        mmWidth = 6879
        BandType = 4
      end
      object pplblMandanteEmail: TppLabel
        UserName = 'lblMandanteEmail'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblMandanteEmail'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 159544
        mmTop = 111654
        mmWidth = 43127
        BandType = 4
      end
      object ppLabel1: TppLabel
        UserName = 'Label1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'MANDATO DE PAGO AUTOMATICO DE CUENTAS A TRAVES'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial Narrow'
        Font.Pitch = fpVariable
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 5292
        mmLeft = 60325
        mmTop = 29633
        mmWidth = 100013
        BandType = 4
      end
      object ppLabel: TppLabel
        UserName = 'Label'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'DE CMR FALABELLA PARA PAGAR A LAS CONCESIONARIAS'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial Narrow'
        Font.Pitch = fpVariable
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 5292
        mmLeft = 59267
        mmTop = 34925
        mmWidth = 102394
        BandType = 4
      end
      object ppLabel2: TppLabel
        UserName = 'Label2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 
          'AUTOPISTA CENTRAL - VESPUCIO NORTE EXPRESS - AUTOPISTA VESPUCIO ' +
          'SUR - COSTANERA NORTE'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial Narrow'
        Font.Pitch = fpVariable
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 5292
        mmLeft = 21960
        mmTop = 40217
        mmWidth = 176742
        BandType = 4
      end
      object ppLabel3: TppLabel
        UserName = 'Label3'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'FECHA OTORGAMIENTO AUTORIZACION DE CARGO'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Pitch = fpVariable
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 17727
        mmTop = 55563
        mmWidth = 96838
        BandType = 4
      end
      object ppLabel4: TppLabel
        UserName = 'Label4'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'DIA'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 119856
        mmTop = 49742
        mmWidth = 5556
        BandType = 4
      end
      object ppLabel5: TppLabel
        UserName = 'Label5'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'MES'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 130440
        mmTop = 49742
        mmWidth = 7673
        BandType = 4
      end
      object ppLabel6: TppLabel
        UserName = 'Label6'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'A'#209'O'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 141817
        mmTop = 49742
        mmWidth = 7408
        BandType = 4
      end
      object ppLabel7: TppLabel
        UserName = 'Label7'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'DATOS DEL VEHICULO'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4763
        mmLeft = 17727
        mmTop = 61648
        mmWidth = 51065
        BandType = 4
      end
      object ppLabel8: TppLabel
        UserName = 'Label8'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'PATENTE'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 17727
        mmTop = 67998
        mmWidth = 17727
        BandType = 4
      end
      object ppLabel9: TppLabel
        UserName = 'Label9'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'PATENTE'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 148167
        mmTop = 67998
        mmWidth = 17727
        BandType = 4
      end
      object ppLabel10: TppLabel
        UserName = 'Label10'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'PATENTE'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        mmHeight = 4498
        mmLeft = 86254
        mmTop = 67998
        mmWidth = 17727
        BandType = 4
      end
      object ppLabel11: TppLabel
        UserName = 'Label11'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'NOMBRE DUE'#209'O DEL VEHICULO'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 18521
        mmTop = 84138
        mmWidth = 54240
        BandType = 4
      end
      object ppLabel12: TppLabel
        UserName = 'Label12'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'ANTECEDENTES PERSONALES CLIENTES CMR'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4763
        mmLeft = 61119
        mmTop = 91546
        mmWidth = 98161
        BandType = 4
      end
      object ppLabel13: TppLabel
        UserName = 'Label13'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'NOMBRE DEL TITULAR'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 17727
        mmTop = 97631
        mmWidth = 42333
        BandType = 4
      end
      object ppLabel14: TppLabel
        UserName = 'Label14'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'DIRECCION'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 17727
        mmTop = 104775
        mmWidth = 21696
        BandType = 4
      end
      object ppLabel15: TppLabel
        UserName = 'Label15'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'R.U.T'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 17727
        mmTop = 111654
        mmWidth = 10054
        BandType = 4
      end
      object ppLabel16: TppLabel
        UserName = 'Label16'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'TELEFONO'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 74348
        mmTop = 111654
        mmWidth = 21167
        BandType = 4
      end
      object ppLabel17: TppLabel
        UserName = 'Label17'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'EMAIL'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 145257
        mmTop = 111654
        mmWidth = 10848
        BandType = 4
      end
      object ppLabel18: TppLabel
        UserName = 'Label18'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'NUMERO DE TARJETA CMR'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4498
        mmLeft = 17727
        mmTop = 119327
        mmWidth = 51329
        BandType = 4
      end
      object ppLabel19: TppLabel
        UserName = 'Label19'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'FIRMA DEL TITULAR DE LA TARJETA CMR'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 130440
        mmTop = 138642
        mmWidth = 71967
        BandType = 4
      end
      object ppLine2: TppLine
        UserName = 'Line2'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Weight = 0.750000000000000000
        mmHeight = 1588
        mmLeft = 130704
        mmTop = 136261
        mmWidth = 71173
        BandType = 4
      end
      object ppRichText: TppRichText
        UserName = 'RichText'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Tahoma'
        Font.Size = 8
        Font.Style = []
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'RichText'
        RichText = 
          '{\rtf1\ansi\ansicpg1252\deff0\deflang3082{\fonttbl{\f0\fnil\fcha' +
          'rset0 Arial;}{\f1\fnil Arial;}}'#13#10'\viewkind4\uc1\pard\fs22 He sus' +
          'crito con las Concesionarias arriba indicadas contratos referido' +
          's los veh\'#39'edculos que se indican en ellos que est\'#39'e1n habilita' +
          'dos para acceder a las autopistas a que se refieran dichos contr' +
          'atos. Conforme a lo establecido en ellos, he quedado obligado a ' +
          'pagar mensualmente por el uso o acceso a las referidas autopista' +
          's, un precio que se calcular\'#39'e1 en base a la informaci\'#39'f3n que' +
          ' se desprenda del (de los) aparato(s) electr\'#39'f3nico(s) que se p' +
          'roporcion\'#39'f3 - TAG(s) y que ha(n) sido instalado(s) en mi veh\'#39 +
          'edculo(s).\f1\par'#13#10'\f0 Por el presente instrumento autorizo a S.' +
          'A.C.I. Falabella S.A. a solicitar a la sociedad Promotora CMR Fa' +
          'labella SA los cargos correspondientes al precio de los contrato' +
          's que he suscrito con las Concesionarias arriba indicadas. Asimi' +
          'smo autorizo a Promotora CMR Falabella S.A. a efectuar los cargo' +
          's indicados precedentemente y que solicitar\'#39'e1 S.A.C.I. Falabel' +
          'la.\f1\par'#13#10'\f0 Los cargos que en definitiva efect\'#39'fae Promotor' +
          'a CMR Falabella S.A. corresponder\'#39'e1n a los montos informados p' +
          'or las Concesionarias con las cuales he contratado, de manera qu' +
          'e ni, SAC.I. Falabella S, ni Promotora CMR Falabella SA, respond' +
          'er\'#39'e1n frente a cualquier error que implique un mayor cargo en ' +
          'mi l\'#39'ednea de cr\'#39'e9dito CMR. De igual forma declaro conocer y ' +
          'aceptar que ni S.A.C.I. Falabella S.A., ni Promotora CMR Falabel' +
          'la S.A., tienen responsabilidad respecto de la prestaci\'#39'f3n del' +
          ' servicio, ni a las condiciones establecidas entre el cliente y ' +
          'las concesionarias se\'#39'f1aladas.\f1\par'#13#10'\f0 Los cargos informad' +
          'os por las Concesionarias y adheridas a esta recaudaci\'#39'f3n corr' +
          'esponder\'#39'e1n a todas las patentes inscritas, en el  o los contr' +
          'atos suscritos con estas. Las patentes descritas en este mandato' +
          ' son referenciales para la identificaci\'#39'f3n de los contratos su' +
          'scritos con las concesionarias.\f1\par'#13#10'Finalmente acepto y decl' +
          'aro conocer la facultad de Promotora CMR Falabella S.A. de abste' +
          'nerse a efectuar los cargos informados en caso de que mi cuenta ' +
          'CMR estuviese bloqueada o inhabilitada para aceptar cargos, ya s' +
          'ea par razones crediticias (falta de cupo, atraso en el pago de ' +
          'mis cuentas, etc.,) u otras. Asimismo, he tomado conocimiento de' +
          ' los riesgos asociados al atraso al pago de (de los) contrato(s)' +
          ' suscrito(s) con las Concesionarias, en especial los que se orig' +
          'inen en las multas o partes que se cursen en mi contra, liberand' +
          'o a S.A.C.I. Falabella S.A. de toda responsabilidad frente a dic' +
          'hos hechos.\par'#13#10'}'#13#10#0
        Stretch = True
        mmHeight = 106363
        mmLeft = 14552
        mmTop = 144992
        mmWidth = 185473
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeftMargin = 794
      end
      object pplblLeyendaMasVehiculos: TppLabel
        UserName = 'lblLeyendaMasVehiculos'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 
          'Este mandato de pago autom'#225'tico cancelar'#225' los consumos de la tot' +
          'alidad de los veh'#237'culos incluidos en el convenio, independientem' +
          'ente de los arriba informados'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        WordWrap = True
        mmHeight = 8996
        mmLeft = 18256
        mmTop = 73819
        mmWidth = 184415
        BandType = 4
      end
      object ppLineDireccion: TppLine
        UserName = 'LineDireccion'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Pen.Style = psDash
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 47096
        mmTop = 106892
        mmWidth = 150019
        BandType = 4
      end
      object ppLineaMail: TppLine
        UserName = 'LineaMail'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Pen.Style = psDash
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 159544
        mmTop = 112977
        mmWidth = 41804
        BandType = 4
      end
      object ppLine1: TppLine
        UserName = 'Line1'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Pen.Width = 3
        Weight = 2.250000000000000000
        mmHeight = 794
        mmLeft = 1058
        mmTop = 29104
        mmWidth = 215900
        BandType = 4
      end
      object ppLine5: TppLine
        UserName = 'Line5'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Pen.Width = 3
        Weight = 2.250000000000000000
        mmHeight = 794
        mmLeft = 0
        mmTop = 256382
        mmWidth = 215900
        BandType = 4
      end
      object RTCopia: TppRichText
        UserName = 'RTCopia'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Tahoma'
        Font.Size = 8
        Font.Style = []
        Anchors = [atTop, atRight]
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'RTCopia'
        RichText = 
          '{\rtf1\ansi\ansicpg1252\deff0\deflang3082{\fonttbl{\f0\fnil Aria' +
          'l;}}'#13#10'\viewkind4\uc1\pard\qr\f0\fs24 Copia...\par'#13#10'}'#13#10#0
        mmHeight = 5292
        mmLeft = 14108
        mmTop = 258234
        mmWidth = 182563
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeftMargin = 794
      end
    end
    object ppParameterList4: TppParameterList
    end
  end
  object cdsDatosConvenioCompleto: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 813
    Top = 180
    Data = {
      420000009619E0BD01000000180000000100000000000300000042000A4D6952
      6963684564697404004B0000000100075355425459504502004900070042696E
      617279000000}
    object cdsDatosConvenioCompletoMiRichEdit: TBlobField
      FieldName = 'MiRichEdit'
    end
  end
  object ppReporteConvenioCompleto: TppReport
    AutoStop = False
    DataPipeline = ppDBPipeline1
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Units = utMillimeters
    BeforePrint = ppReporteConvenioCompletoBeforePrint
    DeviceType = 'Printer'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 813
    Top = 116
    Version = '12.04'
    mmColumnWidth = 0
    DataPipelineName = 'ppDBPipeline1'
    object ppHeaderBand4: TppHeaderBand
      PrintOnFirstPage = False
      PrintOnLastPage = False
      mmBottomOffset = 0
      mmHeight = 48154
      mmPrintPosition = 0
      object pplblLugarFechaCompleto: TppLabel
        UserName = 'lblLugarFechaCompleto'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblLugarFechaCompleto'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 11
        Font.Style = []
        Transparent = True
        Visible = False
        mmHeight = 4498
        mmLeft = 23813
        mmTop = 27252
        mmWidth = 41275
        BandType = 0
      end
      object ppbcNumeroConvenioCompleto: TppBarCode
        UserName = 'bcNumeroConvenioCompleto'
        AlignBarCode = ahLeft
        BarCodeType = bcCode39
        BarColor = clWindowText
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Data = '00100968630601001'
        PrintHumanReadable = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        Visible = False
        mmHeight = 9525
        mmLeft = 97102
        mmTop = 26988
        mmWidth = 82021
        BandType = 0
        mmBarWidth = 254
        mmWideBarRatio = 76200
      end
      object pplblNumeroConvenioCompleto: TppLabel
        UserName = 'lblNumeroConvenioCompleto'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblNumeroConvenioCompleto'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        Visible = False
        mmHeight = 5027
        mmLeft = 97102
        mmTop = 37042
        mmWidth = 59796
        BandType = 0
      end
      object pplblAnexoConvenioCompleto: TppLabel
        UserName = 'lblAnexoConvenioCompleto'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Anexo N'#186' %d'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        Visible = False
        mmHeight = 5027
        mmLeft = 97367
        mmTop = 41804
        mmWidth = 26194
        BandType = 0
      end
      object ppLblListaAmarillaAnexo: TppLabel
        UserName = 'lblListaAmarillaAnexo'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'CONVENIO EN LISTA AMARILLA'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsUnderline]
        Transparent = True
        Visible = False
        mmHeight = 3969
        mmLeft = 126736
        mmTop = 42069
        mmWidth = 52652
        BandType = 0
      end
    end
    object ppDetailBand2: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 212990
      mmPrintPosition = 0
      object ppDBRichText1: TppDBRichText
        UserName = 'DBRichText1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Tahoma'
        Font.Size = 8
        Font.Style = []
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        DataField = 'MiRichEdit'
        DataPipeline = ppDBPipeline1
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 211667
        mmLeft = 2910
        mmTop = 1323
        mmWidth = 192352
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeftMargin = 794
      end
    end
    object ppFooterBand4: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 16669
      mmPrintPosition = 0
      object ppsvConvenioCompleto: TppSystemVariable
        UserName = 'svConvenioCompleto'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        Visible = False
        mmHeight = 3969
        mmLeft = 3440
        mmTop = 5556
        mmWidth = 191294
        BandType = 8
      end
      object pplblCopia: TppLabel
        UserName = 'lblCopia'
        HyperlinkColor = clBlue
        Anchors = [atRight, atBottom]
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblCopia'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 101600
        mmTop = 0
        mmWidth = 93663
        BandType = 8
      end
    end
    object ppParameterList2: TppParameterList
      object ppParameter2: TppParameter
        AutoSearchSettings.LogicalPrefix = []
        AutoSearchSettings.Mandatory = True
        DataType = dtString
        LookupSettings.DisplayType = dtNameOnly
        LookupSettings.SortOrder = soName
        Value = ''
        UserName = 'Parameter2'
      end
    end
  end
  object ppDBPipeline1: TppDBPipeline
    DataSource = dsDatosConvenioCompleto
    UserName = 'DBPipeline1'
    Left = 821
    Top = 284
    object ppDBPipeline1ppField1: TppField
      FieldAlias = 'MiRichEdit'
      FieldName = 'MiRichEdit'
      FieldLength = 0
      DataType = dtBLOB
      DisplayWidth = 10
      Position = 0
      Searchable = False
      Sortable = False
    end
  end
  object dsDatosConvenioCompleto: TDataSource
    DataSet = cdsDatosConvenioCompleto
    Left = 813
    Top = 228
  end
  object ppRptCaratulaConvenio: TppReport
    AutoStop = False
    DataPipeline = ppDBPlCaratulaConvenio
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Units = utMillimeters
    DeviceType = 'Printer'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 493
    Top = 536
    Version = '12.04'
    mmColumnWidth = 0
    DataPipelineName = 'ppDBPlCaratulaConvenio'
    object ppHeaderBand1: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 102394
      mmPrintPosition = 0
      object ppBCNumeroConvenioCaratula: TppBarCode
        UserName = 'bcNumeroConvenioCaratula'
        AlignBarCode = ahLeft
        BarCodeType = bcCode39
        BarColor = clWindowText
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Data = '00100968630601001'
        PrintHumanReadable = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Courier New'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 6615
        mmLeft = 113771
        mmTop = 46038
        mmWidth = 79904
        BandType = 0
        mmBarWidth = 220
        mmWideBarRatio = 76200
      end
      object ppLabel20: TppLabel
        UserName = 'Label20'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'N'#250'mero del Convenio:'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 25929
        mmTop = 48154
        mmWidth = 45170
        BandType = 0
      end
      object pplblTituloCaratulaConvenio: TppLabel
        UserName = 'lblTituloCaratulaConvenio'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Car'#225'tula para la Carpeta del Cliente - Persona Jur'#237'dica'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 25665
        mmTop = 33338
        mmWidth = 166952
        BandType = 0
      end
      object ppLabel22: TppLabel
        UserName = 'Label201'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Nombre del Cliente:'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 25929
        mmTop = 55033
        mmWidth = 40090
        BandType = 0
      end
      object ppLabel23: TppLabel
        UserName = 'Label23'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'RUT del Cliente:'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 25929
        mmTop = 60854
        mmWidth = 32766
        BandType = 0
      end
      object ppLabel24: TppLabel
        UserName = 'Label24'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Giro del Cliente:'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 25929
        mmTop = 66675
        mmWidth = 32808
        BandType = 0
      end
      object ppLine3: TppLine
        UserName = 'Line3'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Weight = 0.750000000000000000
        mmHeight = 794
        mmLeft = 25929
        mmTop = 73025
        mmWidth = 165894
        BandType = 0
      end
      object ppLabel25: TppLabel
        UserName = 'Label202'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Punto de Entrega:'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 25929
        mmTop = 74613
        mmWidth = 36534
        BandType = 0
      end
      object ppLabel26: TppLabel
        UserName = 'Label26'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Operador:'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 25929
        mmTop = 80433
        mmWidth = 20532
        BandType = 0
      end
      object ppLabel27: TppLabel
        UserName = 'Label27'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Fecha y Hora:'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 25929
        mmTop = 86254
        mmWidth = 27982
        BandType = 0
      end
      object pplblNumeroConvenio: TppLabel
        UserName = 'lblNumeroConvenio'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblNumeroConvenio'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4763
        mmLeft = 71173
        mmTop = 48154
        mmWidth = 39688
        BandType = 0
      end
      object pplblNomobreCliente: TppLabel
        UserName = 'lblNomobreCliente'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblNomobreCliente'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4868
        mmLeft = 71173
        mmTop = 55298
        mmWidth = 34925
        BandType = 0
      end
      object pplblNumeroRUTCliente: TppLabel
        UserName = 'lblNumeroRUTCliente'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblNumeroRUTCliente'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4868
        mmLeft = 71173
        mmTop = 61119
        mmWidth = 41275
        BandType = 0
      end
      object pplblGiroCliente: TppLabel
        UserName = 'lblGiroCliente'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblGiroCliente'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4868
        mmLeft = 71173
        mmTop = 66940
        mmWidth = 25442
        BandType = 0
      end
      object pplblPuntoEntrega: TppLabel
        UserName = 'lblPuntoEntrega'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblPuntoEntrega'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4868
        mmLeft = 71173
        mmTop = 74877
        mmWidth = 30226
        BandType = 0
      end
      object pplblOperador: TppLabel
        UserName = 'lblOperador'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblOperador'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4868
        mmLeft = 71173
        mmTop = 80698
        mmWidth = 22183
        BandType = 0
      end
      object pplblFechaHora: TppLabel
        UserName = 'lblFechaHora'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'lblFechaHora'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        mmHeight = 4868
        mmLeft = 71173
        mmTop = 86519
        mmWidth = 25231
        BandType = 0
      end
      object ppLine4: TppLine
        UserName = 'Line4'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Weight = 0.750000000000000000
        mmHeight = 1852
        mmLeft = 25929
        mmTop = 92869
        mmWidth = 165894
        BandType = 0
      end
      object ppLabel35: TppLabel
        UserName = 'Label35'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Documentos Incluidos en la Carpeta'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 26194
        mmTop = 96044
        mmWidth = 73321
        BandType = 0
      end
    end
    object ppDetailBand3: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      mmBottomOffset = 0
      mmHeight = 7500
      mmPrintPosition = 0
      object ppDBText1: TppDBText
        UserName = 'DBText1'
        HyperlinkColor = clBlue
        AutoSize = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        DataField = 'LineaCaratula'
        DataPipeline = ppDBPlCaratulaConvenio
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppDBPlCaratulaConvenio'
        mmHeight = 4868
        mmLeft = 25929
        mmTop = 265
        mmWidth = 26458
        BandType = 4
      end
      object ppShape14: TppShape
        UserName = 'Shape14'
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        Pen.Width = 2
        mmHeight = 4233
        mmLeft = 181240
        mmTop = 265
        mmWidth = 4233
        BandType = 4
      end
    end
    object ppFooterBand1: TppFooterBand
      BeforePrint = ppFooterBand1BeforePrint
      mmBottomOffset = 0
      mmHeight = 57415
      mmPrintPosition = 0
      object ppLine6: TppLine
        UserName = 'Line6'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Weight = 0.750000000000000000
        mmHeight = 1852
        mmLeft = 25929
        mmTop = 3440
        mmWidth = 165894
        BandType = 8
      end
      object ppLabel37: TppLabel
        UserName = 'Label37'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Comentario :'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 14
        Font.Style = []
        Transparent = True
        mmHeight = 5800
        mmLeft = 25929
        mmTop = 10054
        mmWidth = 26035
        BandType = 8
      end
      object pplblFirmaRevisor: TppLabel
        UserName = 'lblFirmaRevisor'
        HyperlinkColor = clBlue
        Anchors = [atTop, atRight]
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Firma Revisor'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 14
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        Visible = False
        mmHeight = 5800
        mmLeft = 137319
        mmTop = 32279
        mmWidth = 28490
        BandType = 8
      end
      object pplnFirmaRevisor: TppLine
        UserName = 'lnFirmaRevisor'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Visible = False
        Weight = 0.750000000000000000
        mmHeight = 1588
        mmLeft = 130440
        mmTop = 30956
        mmWidth = 40746
        BandType = 8
      end
    end
    object ppParameterList5: TppParameterList
    end
  end
  object spObtenerReporteCaratulaConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerReporteCaratulaConvenio'
    Parameters = <>
    Left = 489
    Top = 408
  end
  object dsObtenerReporteCaratulaConvenio: TDataSource
    DataSet = spObtenerReporteCaratulaConvenio
    Left = 489
    Top = 448
  end
  object ppRptEstadoConvenioNatural: TppReport
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297127
    PrinterSetup.mmPaperWidth = 210079
    PrinterSetup.PaperSize = 9
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 677
    Top = 112
    Version = '12.04'
    mmColumnWidth = 0
    object ppDetailBand4: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 10319
      mmPrintPosition = 0
      object ppSubReport1: TppSubReport
        UserName = 'SubReport1'
        ExpandAll = False
        NewPrintJob = True
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'ppDBPlEstadoConvenio'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 0
        mmWidth = 203729
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport1: TppChildReport
          AutoStop = False
          DataPipeline = ppDBPlEstadoConvenio
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 0
          PrinterSetup.mmMarginLeft = 0
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 0
          PrinterSetup.mmPaperHeight = 297127
          PrinterSetup.mmPaperWidth = 210079
          PrinterSetup.PaperSize = 9
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'ppDBPlEstadoConvenio'
          object ppHeaderBand6: TppHeaderBand
            mmBottomOffset = 0
            mmHeight = 105569
            mmPrintPosition = 0
            object ppShape30: TppShape
              UserName = 'Shape301'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 176477
              mmTop = 100542
              mmWidth = 25929
              BandType = 0
            end
            object ppShape29: TppShape
              UserName = 'Shape5'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 150813
              mmTop = 100542
              mmWidth = 25929
              BandType = 0
            end
            object ppShape28: TppShape
              UserName = 'Shape4'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 128588
              mmTop = 100542
              mmWidth = 22490
              BandType = 0
            end
            object ppShape27: TppShape
              UserName = 'Shape3'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 89165
              mmTop = 100542
              mmWidth = 39688
              BandType = 0
            end
            object ppShape25: TppShape
              UserName = 'Shape501'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 81492
              mmTop = 100542
              mmWidth = 7938
              BandType = 0
            end
            object ppShape26: TppShape
              UserName = 'Shape6'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 41010
              mmTop = 100542
              mmWidth = 40746
              BandType = 0
            end
            object ppShape24: TppShape
              UserName = 'Shape2'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 22754
              mmTop = 100542
              mmWidth = 18521
              BandType = 0
            end
            object ppShape23: TppShape
              UserName = 'Shape1'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 2381
              mmTop = 100542
              mmWidth = 20638
              BandType = 0
            end
            object pplblLugarFechaNatural: TppLabel
              UserName = 'lblLugarFechaNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblLugarFechaNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 8467
              mmWidth = 40481
              BandType = 0
            end
            object ppBCNumeroConvenioNatural: TppBarCode
              UserName = 'bcNumeroConvenioNatural'
              AlignBarCode = ahLeft
              BarCodeType = bcCode39
              BarColor = clWindowText
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Data = '00100968630601001'
              PrintHumanReadable = False
              AutoSize = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 9525
              mmLeft = 96309
              mmTop = 6350
              mmWidth = 82021
              BandType = 0
              mmBarWidth = 254
              mmWideBarRatio = 76200
            end
            object pplblNumeroConvenioNatural: TppLabel
              UserName = 'lblNumeroConvenioNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroConvenioNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 96309
              mmTop = 16404
              mmWidth = 54769
              BandType = 0
            end
            object ppLabel28: TppLabel
              UserName = 'Label1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Individualizaci'#243'n Cliente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 2646
              mmTop = 20638
              mmWidth = 49477
              BandType = 0
            end
            object ppLabel29: TppLabel
              UserName = 'Label2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombres'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 29369
              mmWidth = 17198
              BandType = 0
            end
            object ppLabel30: TppLabel
              UserName = 'Label302'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Apellido Paterno'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 34660
              mmWidth = 31221
              BandType = 0
            end
            object ppLabel31: TppLabel
              UserName = 'Label3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Apellido Materno'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 39952
              mmWidth = 31750
              BandType = 0
            end
            object ppLabel32: TppLabel
              UserName = 'Label4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RUT'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 45244
              mmWidth = 8731
              BandType = 0
            end
            object ppLabel33: TppLabel
              UserName = 'Label5'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha de Nacimiento'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 50536
              mmWidth = 40217
              BandType = 0
            end
            object ppLabel34: TppLabel
              UserName = 'Label6'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tel'#233'fono'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 55827
              mmWidth = 16669
              BandType = 0
            end
            object ppLabel36: TppLabel
              UserName = 'Label7'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tel'#233'fono'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 61119
              mmWidth = 16669
              BandType = 0
            end
            object ppLabel38: TppLabel
              UserName = 'Label8'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'e-Mail'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 66411
              mmWidth = 11642
              BandType = 0
            end
            object ppLabel74: TppLabel
              UserName = 'Label9'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 66411
              mmWidth = 1058
              BandType = 0
            end
            object ppLabel73: TppLabel
              UserName = 'Label101'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 61119
              mmWidth = 1058
              BandType = 0
            end
            object ppLabel72: TppLabel
              UserName = 'Label10'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 55827
              mmWidth = 1058
              BandType = 0
            end
            object ppLabel67: TppLabel
              UserName = 'Label11'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 50536
              mmWidth = 1058
              BandType = 0
            end
            object ppLabel68: TppLabel
              UserName = 'Label12'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 45244
              mmWidth = 1058
              BandType = 0
            end
            object ppLabel69: TppLabel
              UserName = 'Label13'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 39952
              mmWidth = 1058
              BandType = 0
            end
            object ppLabel70: TppLabel
              UserName = 'Label702'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 34660
              mmWidth = 1058
              BandType = 0
            end
            object ppLabel71: TppLabel
              UserName = 'Label14'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 29369
              mmWidth = 1058
              BandType = 0
            end
            object pplblNombreNatural: TppLabel
              UserName = 'lblNombreNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNombreNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48419
              mmTop = 29369
              mmWidth = 33073
              BandType = 0
            end
            object pplblApellidoPaternoNatural: TppLabel
              UserName = 'lblApellidoPaternoNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblApellidoPaternoNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48419
              mmTop = 34660
              mmWidth = 47890
              BandType = 0
            end
            object pplblApellidoMaternoNatural: TppLabel
              UserName = 'lblApellidoMaternoNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblApellidoMaternoNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48419
              mmTop = 39952
              mmWidth = 48683
              BandType = 0
            end
            object pplblNumeroRUTNatural: TppLabel
              UserName = 'lblNumeroRUTNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroRUTNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48419
              mmTop = 45244
              mmWidth = 41804
              BandType = 0
            end
            object pplblFechaNacimientoNatural: TppLabel
              UserName = 'lblFechaNacimientoNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblFechaNacimientoNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48419
              mmTop = 50536
              mmWidth = 51065
              BandType = 0
            end
            object pplblTelefono1Natural: TppLabel
              UserName = 'lblTelefono1Natural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblTelefono1Natural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48419
              mmTop = 55827
              mmWidth = 37042
              BandType = 0
            end
            object pplblTelefono2Natural: TppLabel
              UserName = 'lblTelefono2Natural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblTelefono2Natural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48419
              mmTop = 61119
              mmWidth = 37042
              BandType = 0
            end
            object pplblEMailNatural: TppLabel
              UserName = 'lblEMailNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblEMailNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48419
              mmTop = 66411
              mmWidth = 28575
              BandType = 0
            end
            object ppLabel42: TppLabel
              UserName = 'Label15'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C'#243'digo de '#193'rea  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 89429
              mmTop = 61119
              mmWidth = 33073
              BandType = 0
            end
            object ppLabel21: TppLabel
              UserName = 'Label16'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C'#243'digo de '#193'rea  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 89429
              mmTop = 55827
              mmWidth = 33073
              BandType = 0
            end
            object pplblCodigoArea1Natural: TppLabel
              UserName = 'lblCodigoArea1Natural'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblCodigoArea1Natural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 123561
              mmTop = 55827
              mmWidth = 10848
              BandType = 0
            end
            object pplblCodigoArea2Natural: TppLabel
              UserName = 'lblCodigoArea2Natural'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblCodigoArea2Natural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 123561
              mmTop = 61119
              mmWidth = 10848
              BandType = 0
            end
            object ppLabel45: TppLabel
              UserName = 'Label17'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 138377
              mmTop = 55827
              mmWidth = 18785
              BandType = 0
            end
            object ppLabel46: TppLabel
              UserName = 'Label18'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 138377
              mmTop = 61119
              mmWidth = 18785
              BandType = 0
            end
            object pplblNumeroTelefono2Natural: TppLabel
              UserName = 'lblNumeroTelefono2Natural'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroTelefono2Natural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 158486
              mmTop = 61119
              mmWidth = 27781
              BandType = 0
            end
            object pplblNumeroTelefono1Natural: TppLabel
              UserName = 'lblNumeroTelefono1Natural'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroTelefono1Natural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 158486
              mmTop = 55827
              mmWidth = 27781
              BandType = 0
            end
            object ppLabel39: TppLabel
              UserName = 'Label203'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Domicilio Env'#237'o Facturaci'#243'n  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 77788
              mmWidth = 56092
              BandType = 0
            end
            object pplblDomicilioEnvioFacturacionNatural: TppLabel
              UserName = 'lblDomicilioEnvioFacturacionNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblDomicilioEnvioFacturacionNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 59531
              mmTop = 77788
              mmWidth = 67733
              BandType = 0
            end
            object ppLabel40: TppLabel
              UserName = 'Label401'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Detalle (Of./Pobl./Villa)  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 83344
              mmWidth = 46567
              BandType = 0
            end
            object pplblDetalleNatural: TppLabel
              UserName = 'lblDetalleNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblDetalleNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 50271
              mmTop = 83344
              mmWidth = 31221
              BandType = 0
            end
            object ppLabel41: TppLabel
              UserName = 'Label19'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C'#243'digo Postal  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 2646
              mmTop = 88900
              mmWidth = 29898
              BandType = 0
            end
            object pplblCodigoPostalNatural: TppLabel
              UserName = 'lblCodigoPostalNatural'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblCodigoPostalNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 33602
              mmTop = 88900
              mmWidth = 23019
              BandType = 0
            end
            object ppLabel47: TppLabel
              UserName = 'Label22'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Comuna  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 58208
              mmTop = 88900
              mmWidth = 19579
              BandType = 0
            end
            object pplblComunaNatural: TppLabel
              UserName = 'lblComunaNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblComunaNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 78846
              mmTop = 88900
              mmWidth = 33867
              BandType = 0
            end
            object ppLabel48: TppLabel
              UserName = 'Label20'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Regi'#243'n  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 123031
              mmTop = 88900
              mmWidth = 16933
              BandType = 0
            end
            object pplblRegionNatural: TppLabel
              UserName = 'lblRegionNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblRegionNatural'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 140759
              mmTop = 88900
              mmWidth = 31327
              BandType = 0
            end
            object ppLabel43: TppLabel
              UserName = 'Label31'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Individualizaci'#243'n de los veh'#237'culos y telev'#237'a'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 2381
              mmTop = 94456
              mmWidth = 86064
              BandType = 0
            end
            object ppLabel59: TppLabel
              UserName = 'Label21'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Acci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4022
              mmLeft = 7938
              mmTop = 101336
              mmWidth = 10541
              BandType = 0
            end
            object ppLabel60: TppLabel
              UserName = 'Label601'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4022
              mmLeft = 25400
              mmTop = 101336
              mmWidth = 12065
              BandType = 0
            end
            object ppLabel61: TppLabel
              UserName = 'Label25'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tipo Veh'#237'culo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4022
              mmLeft = 42333
              mmTop = 101336
              mmWidth = 21463
              BandType = 0
            end
            object ppLabel62: TppLabel
              UserName = 'Label23'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Cat.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 82550
              mmTop = 101336
              mmWidth = 6350
              BandType = 0
            end
            object ppLabel63: TppLabel
              UserName = 'Label24'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 90488
              mmTop = 101336
              mmWidth = 9790
              BandType = 0
            end
            object ppLabel64: TppLabel
              UserName = 'Label26'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Telev'#237'a Asoc.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4022
              mmLeft = 129382
              mmTop = 101336
              mmWidth = 21294
              BandType = 0
            end
            object ppLabel65: TppLabel
              UserName = 'Label27'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Vto. Garant'#237'a'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 152929
              mmTop = 101336
              mmWidth = 20902
              BandType = 0
            end
            object ppLabel66: TppLabel
              UserName = 'Label303'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha Alta'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 180711
              mmTop = 101336
              mmWidth = 16669
              BandType = 0
            end
            object ppLabel75: TppLabel
              UserName = 'Label75'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Parking Parque Arauco'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 2646
              mmTop = 71702
              mmWidth = 43603
              BandType = 0
            end
            object ppLabel104: TppLabel
              UserName = 'Label104'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 45508
              mmTop = 71702
              mmWidth = 1058
              BandType = 0
            end
            object pplblAdheridoPA: TppLabel
              UserName = 'lblAdheridoPA'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblAdheridoPA'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 48419
              mmTop = 71702
              mmWidth = 26628
              BandType = 0
            end
            object pplblListaAmarillaPersonaNatural: TppLabel
              UserName = 'lblListaAmarillaPersonaNatural'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'CONVENIO EN LISTA AMARILLA'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsUnderline]
              Transparent = True
              mmHeight = 4022
              mmLeft = 96309
              mmTop = 23019
              mmWidth = 52959
              BandType = 0
            end
          end
          object ppDetailBand7: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 3970
            mmPrintPosition = 0
            object ppShape22: TppShape
              UserName = 'Shape14'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 176477
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object ppShape21: TppShape
              UserName = 'Shape13'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 150813
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object ppShape20: TppShape
              UserName = 'Shape203'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 128588
              mmTop = 0
              mmWidth = 22490
              BandType = 4
            end
            object ppShape19: TppShape
              UserName = 'Shape12'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 89165
              mmTop = 0
              mmWidth = 39688
              BandType = 4
            end
            object ppShape18: TppShape
              UserName = 'Shape11'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 81492
              mmTop = 0
              mmWidth = 7938
              BandType = 4
            end
            object ppShape17: TppShape
              UserName = 'Shape10'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 41010
              mmTop = 0
              mmWidth = 40746
              BandType = 4
            end
            object ppShape16: TppShape
              UserName = 'Shape9'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 22754
              mmTop = 0
              mmWidth = 18521
              BandType = 4
            end
            object ppShape15: TppShape
              UserName = 'Shape8'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 2381
              mmTop = 0
              mmWidth = 20638
              BandType = 4
            end
            object ppDBText3: TppDBText
              UserName = 'DBText2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'PatenteCompleta'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 23548
              mmTop = 265
              mmWidth = 17198
              BandType = 4
            end
            object ppDBText4: TppDBText
              UserName = 'DBText3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescripcionTipoVehiculo'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 41804
              mmTop = 265
              mmWidth = 39423
              BandType = 4
            end
            object ppDBText5: TppDBText
              UserName = 'DBText4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'CodigoCategoria'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 82286
              mmTop = 265
              mmWidth = 6350
              BandType = 4
            end
            object ppDBText6: TppDBText
              UserName = 'DBText5'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescripcionMarca'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 89694
              mmTop = 265
              mmWidth = 38629
              BandType = 4
            end
            object ppDBText7: TppDBText
              UserName = 'DBText6'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'TeleviaAsociado'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 129382
              mmTop = 265
              mmWidth = 20902
              BandType = 4
            end
            object ppDBText8: TppDBText
              UserName = 'DBText7'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaVencimientoTAG'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 151607
              mmTop = 265
              mmWidth = 24342
              BandType = 4
            end
            object ppDBText9: TppDBText
              UserName = 'DBText8'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaAltaCuenta'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 177271
              mmTop = 265
              mmWidth = 24342
              BandType = 4
            end
            object ppDBText2: TppDBText
              UserName = 'DBText1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'EstadoCuenta'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 2910
              mmTop = 265
              mmWidth = 19315
              BandType = 4
            end
          end
          object ppFooterBand5: TppFooterBand
            mmBottomOffset = 0
            mmHeight = 87841
            mmPrintPosition = 0
            object ppLabel44: TppLabel
              UserName = 'Label32'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Individualizaci'#243'n Concesionaria'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 2381
              mmTop = 2381
              mmWidth = 64558
              BandType = 8
            end
            object ppLabel49: TppLabel
              UserName = 'Label33'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre o Raz'#243'n Social'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 2381
              mmTop = 10054
              mmWidth = 44831
              BandType = 8
            end
            object ppLabel50: TppLabel
              UserName = 'Label502'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Rol '#218'nico Tributario N'#186
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 2381
              mmTop = 15610
              mmWidth = 43307
              BandType = 8
            end
            object ppLabel51: TppLabel
              UserName = 'Label34'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Domicilio'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 2381
              mmTop = 21167
              mmWidth = 17187
              BandType = 8
            end
            object ppLabel58: TppLabel
              UserName = 'Label35'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Declaraciones'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 2381
              mmTop = 28575
              mmWidth = 28829
              BandType = 8
            end
            object ppmDeclaracionesNatural: TppMemo
              UserName = 'mDeclaracionesNatural'
              Anchors = [atLeft, atBottom]
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'mDeclaracionesNatural'
              CharWrap = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 23019
              mmLeft = 2381
              mmTop = 34131
              mmWidth = 198438
              BandType = 8
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object ppLabel57: TppLabel
              UserName = 'Label36'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48154
              mmTop = 21167
              mmWidth = 1058
              BandType = 8
            end
            object ppLabel56: TppLabel
              UserName = 'Label37'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48154
              mmTop = 15610
              mmWidth = 1058
              BandType = 8
            end
            object ppLabel55: TppLabel
              UserName = 'Label38'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48154
              mmTop = 10054
              mmWidth = 1058
              BandType = 8
            end
            object ppLabel52: TppLabel
              UserName = 'Label39'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'SOCIEDAD CONCESIONARIA COSTANERA NORTE, S.A.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 51065
              mmTop = 10054
              mmWidth = 113242
              BandType = 8
            end
            object ppLabel53: TppLabel
              UserName = 'Label40'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = '76.496.130-7'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 51594
              mmTop = 15610
              mmWidth = 25135
              BandType = 8
            end
            object ppLabel54: TppLabel
              UserName = 'Label41'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'General Prieto 1430, Independencia - Santiago.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 51065
              mmTop = 21167
              mmWidth = 90086
              BandType = 8
            end
            object ppSystemVariable3: TppSystemVariable
              UserName = 'SystemVariable3'
              HyperlinkColor = clBlue
              Anchors = [atLeft, atBottom]
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 2381
              mmTop = 82550
              mmWidth = 197644
              BandType = 8
            end
            object pplblPieClienteNatural: TppLabel
              UserName = 'lblPieCliente1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Cliente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 138908
              mmTop = 58208
              mmWidth = 11642
              BandType = 8
            end
            object pplblPieConcesionariaNatural: TppLabel
              UserName = 'lblPieConcesionaria1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'p.p. Concesionaria'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4191
              mmLeft = 39705
              mmTop = 58208
              mmWidth = 31454
              BandType = 8
            end
          end
        end
      end
      object ppSubReport2: TppSubReport
        UserName = 'SubReport2'
        ExpandAll = False
        NewPrintJob = True
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 5292
        mmWidth = 203729
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport2: TppChildReport
          AutoStop = False
          DataPipeline = ppDBPlEstadoConvenioAnexo
          NoDataBehaviors = [ndBlankReport]
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 0
          PrinterSetup.mmMarginLeft = 0
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 0
          PrinterSetup.mmPaperHeight = 297127
          PrinterSetup.mmPaperWidth = 210079
          PrinterSetup.PaperSize = 9
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
          object ppHeaderBand2: TppHeaderBand
            mmBottomOffset = 0
            mmHeight = 20373
            mmPrintPosition = 0
            object ppShape54: TppShape
              UserName = 'Shape54'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 176477
              mmTop = 15346
              mmWidth = 25929
              BandType = 0
            end
            object ppShape53: TppShape
              UserName = 'Shape53'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 150813
              mmTop = 15346
              mmWidth = 25929
              BandType = 0
            end
            object ppShape52: TppShape
              UserName = 'Shape52'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 128588
              mmTop = 15346
              mmWidth = 22490
              BandType = 0
            end
            object ppShape51: TppShape
              UserName = 'Shape51'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 89165
              mmTop = 15346
              mmWidth = 39688
              BandType = 0
            end
            object ppShape50: TppShape
              UserName = 'Shape50'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 81492
              mmTop = 15346
              mmWidth = 7938
              BandType = 0
            end
            object ppShape49: TppShape
              UserName = 'Shape49'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 41010
              mmTop = 15346
              mmWidth = 40746
              BandType = 0
            end
            object ppShape48: TppShape
              UserName = 'Shape48'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 22754
              mmTop = 15346
              mmWidth = 18521
              BandType = 0
            end
            object ppShape47: TppShape
              UserName = 'Shape47'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 2381
              mmTop = 15346
              mmWidth = 20638
              BandType = 0
            end
            object pplblNumeroConvenioNaturalAnexo: TppLabel
              UserName = 'lblNumeroConvenioNaturalAnexo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 
                'Continuaci'#243'n de la Individualizaci'#243'n de los veh'#237'culos y telev'#237'a.' +
                ' Convenio %s'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 2381
              mmTop = 6350
              mmWidth = 154051
              BandType = 0
            end
            object ppLabel77: TppLabel
              UserName = 'Label77'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Acci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 7938
              mmTop = 16404
              mmWidth = 10583
              BandType = 0
            end
            object ppLabel78: TppLabel
              UserName = 'Label78'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 25400
              mmTop = 16404
              mmWidth = 12171
              BandType = 0
            end
            object ppLabel79: TppLabel
              UserName = 'Label701'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tipo Veh'#237'culo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 42333
              mmTop = 16404
              mmWidth = 21431
              BandType = 0
            end
            object ppLabel80: TppLabel
              UserName = 'Label80'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Cat.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 82550
              mmTop = 16404
              mmWidth = 6350
              BandType = 0
            end
            object ppLabel81: TppLabel
              UserName = 'Label81'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 90488
              mmTop = 16404
              mmWidth = 9790
              BandType = 0
            end
            object ppLabel82: TppLabel
              UserName = 'Label82'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Telev'#237'a Asoc.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 129382
              mmTop = 16404
              mmWidth = 21167
              BandType = 0
            end
            object ppLabel83: TppLabel
              UserName = 'Label83'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Vto. Garant'#237'a'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 152929
              mmTop = 16404
              mmWidth = 20902
              BandType = 0
            end
            object ppLabel84: TppLabel
              UserName = 'Label84'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha Alta'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 178594
              mmTop = 16404
              mmWidth = 16669
              BandType = 0
            end
          end
          object ppDetailBand8: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 3969
            mmPrintPosition = 0
            object ppShape62: TppShape
              UserName = 'Shape62'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 176477
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object ppShape61: TppShape
              UserName = 'Shape61'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 150813
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object ppShape60: TppShape
              UserName = 'Shape60'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 128588
              mmTop = 0
              mmWidth = 22490
              BandType = 4
            end
            object ppShape59: TppShape
              UserName = 'Shape59'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 89165
              mmTop = 0
              mmWidth = 39688
              BandType = 4
            end
            object ppDBText25: TppDBText
              UserName = 'DBText25'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaAltaCuenta'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 177271
              mmTop = 265
              mmWidth = 24342
              BandType = 4
            end
            object ppDBText24: TppDBText
              UserName = 'DBText24'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaVencimientoTAG'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 151607
              mmTop = 265
              mmWidth = 24342
              BandType = 4
            end
            object ppDBText23: TppDBText
              UserName = 'DBText23'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'TeleviaAsociado'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 129382
              mmTop = 265
              mmWidth = 20902
              BandType = 4
            end
            object ppDBText22: TppDBText
              UserName = 'DBText22'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescripcionMarca'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 89694
              mmTop = 265
              mmWidth = 38629
              BandType = 4
            end
            object ppShape58: TppShape
              UserName = 'Shape58'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 81492
              mmTop = 0
              mmWidth = 7938
              BandType = 4
            end
            object ppShape57: TppShape
              UserName = 'Shape57'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 41010
              mmTop = 0
              mmWidth = 40746
              BandType = 4
            end
            object ppShape56: TppShape
              UserName = 'Shape401'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 22754
              mmTop = 0
              mmWidth = 18521
              BandType = 4
            end
            object ppShape55: TppShape
              UserName = 'Shape55'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 2381
              mmTop = 0
              mmWidth = 20638
              BandType = 4
            end
            object ppDBText18: TppDBText
              UserName = 'DBText101'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'EstadoCuenta'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 2910
              mmTop = 265
              mmWidth = 19315
              BandType = 4
            end
            object ppDBText19: TppDBText
              UserName = 'DBText19'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'PatenteCompleta'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 23548
              mmTop = 265
              mmWidth = 17198
              BandType = 4
            end
            object ppDBText20: TppDBText
              UserName = 'DBText20'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescripcionTipoVehiculo'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 41804
              mmTop = 265
              mmWidth = 39423
              BandType = 4
            end
            object ppDBText21: TppDBText
              UserName = 'DBText21'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'CodigoCategoria'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 82286
              mmTop = 265
              mmWidth = 6350
              BandType = 4
            end
          end
          object ppFooterBand2: TppFooterBand
            mmBottomOffset = 0
            mmHeight = 6350
            mmPrintPosition = 0
            object ppSystemVariable1: TppSystemVariable
              UserName = 'SystemVariable1'
              HyperlinkColor = clBlue
              Anchors = [atTop, atRight]
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 6350
              mmTop = 2117
              mmWidth = 197644
              BandType = 8
            end
          end
        end
      end
    end
    object ppParameterList6: TppParameterList
    end
  end
  object ppDBPlCaratulaConvenio: TppDBPipeline
    DataSource = dsObtenerReporteCaratulaConvenio
    UserName = 'DBPlCaratulaConvenio'
    Left = 493
    Top = 492
  end
  object ppDBPlEstadoConvenio: TppDBPipeline
    DataSource = dsObtenerReporteEstadoConvenio
    UserName = 'DBPlEstadoConvenio'
    Left = 669
    Top = 180
  end
  object dsObtenerReporteEstadoConvenio: TDataSource
    DataSet = spObtenerReporteEstadoConvenio
    Left = 669
    Top = 232
  end
  object spObtenerReporteEstadoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerReporteEstadoConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RegistrosPrimeraPagina'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 669
    Top = 288
  end
  object spObtenerReporteEstadoConvenio2: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerReporteEstadoConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RegistrosPrimeraPagina'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 541
    Top = 304
  end
  object dsObtenerReporteEstadoConvenio2: TDataSource
    DataSet = spObtenerReporteEstadoConvenio2
    Left = 541
    Top = 248
  end
  object ppDBPlEstadoConvenioAnexo: TppDBPipeline
    DataSource = dsObtenerReporteEstadoConvenio2
    UserName = 'DBPlEstadoConvenioAnexo'
    Left = 549
    Top = 192
  end
  object ppReporteConvenioCompleto2: TppReport
    AutoStop = False
    DataPipeline = ppDBPipeline1
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Units = utMillimeters
    DeviceType = 'Printer'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 949
    Top = 376
    Version = '12.04'
    mmColumnWidth = 0
    DataPipelineName = 'ppDBPipeline1'
    object ppHeaderBand7: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 9790
      mmPrintPosition = 0
    end
    object ppDetailBand6: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 262203
      mmPrintPosition = 0
      object ppDBRichText2: TppDBRichText
        UserName = 'DBRichText1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Tahoma'
        Font.Size = 8
        Font.Style = []
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        DataField = 'MiRichEdit'
        DataPipeline = ppDBPipeline1
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 258763
        mmLeft = 2910
        mmTop = 1323
        mmWidth = 192352
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeftMargin = 794
      end
    end
    object ppParameterList3: TppParameterList
      object ppParameter3: TppParameter
        AutoSearchSettings.LogicalPrefix = []
        AutoSearchSettings.Mandatory = True
        DataType = dtString
        LookupSettings.DisplayType = dtNameOnly
        LookupSettings.SortOrder = soName
        Value = ''
        UserName = 'Parameter3'
      end
    end
  end
  object ppRptEstadoConvenioJuridico: TppReport
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 553
    Top = 124
    Version = '12.04'
    mmColumnWidth = 0
    object ppDetailBand11: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 11113
      mmPrintPosition = 0
      object ppSubReport4: TppSubReport
        UserName = 'SubReport4'
        ExpandAll = False
        NewPrintJob = True
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'ppDBPlEstadoConvenio'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 265
        mmWidth = 203650
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport4: TppChildReport
          AutoStop = False
          DataPipeline = ppDBPlEstadoConvenio
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 0
          PrinterSetup.mmMarginLeft = 0
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 0
          PrinterSetup.mmPaperHeight = 297000
          PrinterSetup.mmPaperWidth = 210000
          PrinterSetup.PaperSize = 9
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'ppDBPlEstadoConvenio'
          object ppHeaderBand8: TppHeaderBand
            mmBottomOffset = 0
            mmHeight = 121709
            mmPrintPosition = 0
            object ppShape31: TppShape
              UserName = 'Shape30'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 176477
              mmTop = 116681
              mmWidth = 25929
              BandType = 0
            end
            object ppShape32: TppShape
              UserName = 'Shape29'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 150813
              mmTop = 116681
              mmWidth = 25929
              BandType = 0
            end
            object ppShape33: TppShape
              UserName = 'Shape202'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 128588
              mmTop = 116681
              mmWidth = 22490
              BandType = 0
            end
            object ppShape34: TppShape
              UserName = 'Shape27'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 89165
              mmTop = 116681
              mmWidth = 39688
              BandType = 0
            end
            object ppShape35: TppShape
              UserName = 'Shape26'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 81492
              mmTop = 116681
              mmWidth = 7938
              BandType = 0
            end
            object ppShape36: TppShape
              UserName = 'Shape25'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 41010
              mmTop = 116681
              mmWidth = 40746
              BandType = 0
            end
            object ppShape37: TppShape
              UserName = 'Shape24'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 22754
              mmTop = 116681
              mmWidth = 18521
              BandType = 0
            end
            object ppShape38: TppShape
              UserName = 'Shape23'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 2381
              mmTop = 116681
              mmWidth = 20638
              BandType = 0
            end
            object pplblLugarFechaJuridico: TppLabel
              UserName = 'lblLugarFechaJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblLugarFechaJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 8467
              mmWidth = 41487
              BandType = 0
            end
            object ppbcNumeroConvenioJuridico: TppBarCode
              UserName = 'bcNumeroConvenioJuridico'
              AlignBarCode = ahLeft
              BarCodeType = bcCode39
              BarColor = clWindowText
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Data = '00100968630601001'
              PrintHumanReadable = False
              AutoSize = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 9525
              mmLeft = 96838
              mmTop = 6350
              mmWidth = 82021
              BandType = 0
              mmBarWidth = 254
              mmWideBarRatio = 76200
            end
            object pplblNumeroConvenioJuridico: TppLabel
              UserName = 'lblNumeroConvenioJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroConvenioJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 96838
              mmTop = 16404
              mmWidth = 56684
              BandType = 0
            end
            object ppLabel76: TppLabel
              UserName = 'Label28'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Individualizaci'#243'n Cliente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 3175
              mmTop = 20638
              mmWidth = 49445
              BandType = 0
            end
            object ppLabel85: TppLabel
              UserName = 'Label29'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre o Raz'#243'n Social'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 29369
              mmWidth = 44831
              BandType = 0
            end
            object ppLabel133: TppLabel
              UserName = 'Label71'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 29369
              mmWidth = 1058
              BandType = 0
            end
            object pplblNombreJuridico: TppLabel
              UserName = 'lblNombreJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNombreJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 29369
              mmWidth = 33951
              BandType = 0
            end
            object ppLabel86: TppLabel
              UserName = 'Label30'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RUT'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 34660
              mmWidth = 8721
              BandType = 0
            end
            object ppLabel132: TppLabel
              UserName = 'Label70'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 34660
              mmWidth = 1058
              BandType = 0
            end
            object pplblNumeroRUTJuridico: TppLabel
              UserName = 'lblNumeroRUTJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroRUTJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 34660
              mmWidth = 42672
              BandType = 0
            end
            object ppLabel87: TppLabel
              UserName = 'Label301'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre Representante 1'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 39952
              mmWidth = 47752
              BandType = 0
            end
            object ppLabel131: TppLabel
              UserName = 'Label69'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 39952
              mmWidth = 1058
              BandType = 0
            end
            object pplblNombreR1Juridico: TppLabel
              UserName = 'lblNombreR1Juridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNombreR1Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 39952
              mmWidth = 39370
              BandType = 0
            end
            object ppLabel88: TppLabel
              UserName = 'Label1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C.I. Representante 1'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 45244
              mmWidth = 39243
              BandType = 0
            end
            object ppLabel130: TppLabel
              UserName = 'Label68'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 45244
              mmWidth = 1058
              BandType = 0
            end
            object pplblNumeroCI1Juridico: TppLabel
              UserName = 'lblNumeroCI1Juridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroCI1Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 45244
              mmWidth = 40555
              BandType = 0
            end
            object ppLabel89: TppLabel
              UserName = 'Label2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre Representante 2'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 50536
              mmWidth = 47752
              BandType = 0
            end
            object ppLabel129: TppLabel
              UserName = 'Label67'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 50536
              mmWidth = 1058
              BandType = 0
            end
            object pplblNombreR2Juridico: TppLabel
              UserName = 'lblNombreR2Juridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNombreR2Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 50536
              mmWidth = 39370
              BandType = 0
            end
            object ppLabel148: TppLabel
              UserName = 'Label148'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C.I. Representante 2'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 55827
              mmWidth = 39243
              BandType = 0
            end
            object ppLabel134: TppLabel
              UserName = 'Label72'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 55827
              mmWidth = 1058
              BandType = 0
            end
            object pplblNumeroCI2Juridico: TppLabel
              UserName = 'lblNumeroCI2Juridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroCI2Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 55827
              mmWidth = 40555
              BandType = 0
            end
            object ppLabel149: TppLabel
              UserName = 'Label149'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre Representante 3'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 61119
              mmWidth = 47752
              BandType = 0
            end
            object ppLabel135: TppLabel
              UserName = 'Label73'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 61119
              mmWidth = 1058
              BandType = 0
            end
            object pplblNombreR3Juridico: TppLabel
              UserName = 'lblNombreR3Juridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNombreR3Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 61119
              mmWidth = 39370
              BandType = 0
            end
            object ppLabel150: TppLabel
              UserName = 'Label150'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C.I. Representante 3'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 66411
              mmWidth = 39243
              BandType = 0
            end
            object ppLabel136: TppLabel
              UserName = 'Label74'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 66411
              mmWidth = 1058
              BandType = 0
            end
            object pplblNumeroCI3Juridico: TppLabel
              UserName = 'lblNumeroCI3Juridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroCI3Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 66411
              mmWidth = 40555
              BandType = 0
            end
            object ppLabel90: TppLabel
              UserName = 'Label3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tel'#233'fono'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 71702
              mmWidth = 16637
              BandType = 0
            end
            object ppLabel151: TppLabel
              UserName = 'Label151'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 71702
              mmWidth = 1058
              BandType = 0
            end
            object pplblTelefono1Juridico: TppLabel
              UserName = 'lblTelefono1Juridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblTelefono1Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 71702
              mmWidth = 37846
              BandType = 0
            end
            object ppLabel105: TppLabel
              UserName = 'Label4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C'#243'digo de '#193'rea  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 97102
              mmTop = 71702
              mmWidth = 33062
              BandType = 0
            end
            object pplblCodigoArea1Juridico: TppLabel
              UserName = 'lblCodigoArea1Juridico'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblCodigoArea1Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 131234
              mmTop = 71702
              mmWidth = 10848
              BandType = 0
            end
            object ppLabel109: TppLabel
              UserName = 'Label45'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 146050
              mmTop = 71702
              mmWidth = 18669
              BandType = 0
            end
            object pplblNumeroTelefono1Juridico: TppLabel
              UserName = 'lblNumeroTelefono1Juridico'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroTelefono1Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 165629
              mmTop = 71702
              mmWidth = 27781
              BandType = 0
            end
            object pplblNumeroTelefono2Juridico: TppLabel
              UserName = 'lblNumeroTelefono2Juridico'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblNumeroTelefono2Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 165629
              mmTop = 76994
              mmWidth = 27781
              BandType = 0
            end
            object ppLabel110: TppLabel
              UserName = 'Label46'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 146050
              mmTop = 76994
              mmWidth = 18669
              BandType = 0
            end
            object pplblCodigoArea2Juridico: TppLabel
              UserName = 'lblCodigoArea2Juridico'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblCodigoArea2Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 131234
              mmTop = 76994
              mmWidth = 10848
              BandType = 0
            end
            object ppLabel106: TppLabel
              UserName = 'Label42'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C'#243'digo de '#193'rea  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 97102
              mmTop = 76994
              mmWidth = 33062
              BandType = 0
            end
            object pplblTelefono2Juridico: TppLabel
              UserName = 'lblTelefono2Juridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblTelefono2Juridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 76994
              mmWidth = 37846
              BandType = 0
            end
            object ppLabel152: TppLabel
              UserName = 'Label152'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 76994
              mmWidth = 1058
              BandType = 0
            end
            object ppLabel91: TppLabel
              UserName = 'Label5'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tel'#233'fono'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 76994
              mmWidth = 16637
              BandType = 0
            end
            object ppLabel92: TppLabel
              UserName = 'Label6'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'e-Mail'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 82286
              mmWidth = 11515
              BandType = 0
            end
            object ppLabel153: TppLabel
              UserName = 'Label153'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 53181
              mmTop = 82286
              mmWidth = 1058
              BandType = 0
            end
            object pplblEMailJuridico: TppLabel
              UserName = 'lblEMailJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblEMailJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 56092
              mmTop = 82286
              mmWidth = 29422
              BandType = 0
            end
            object ppLabel93: TppLabel
              UserName = 'Label7'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Domicilio Env'#237'o Facturaci'#243'n  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 92604
              mmWidth = 56092
              BandType = 0
            end
            object pplblDomicilioEnvioFacturacionJuridico: TppLabel
              UserName = 'lblDomicilioEnvioFacturacionJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblDomicilioEnvioFacturacionJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 60061
              mmTop = 92604
              mmWidth = 68792
              BandType = 0
            end
            object ppLabel94: TppLabel
              UserName = 'Label402'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Detalle (Of./Pobl./Villa)  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 98161
              mmWidth = 46524
              BandType = 0
            end
            object pplblDetalleJuridico: TppLabel
              UserName = 'lblDetalleJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblDetalleJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 50800
              mmTop = 98161
              mmWidth = 32046
              BandType = 0
            end
            object ppLabel95: TppLabel
              UserName = 'Label8'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C'#243'digo Postal  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 3175
              mmTop = 103717
              mmWidth = 29972
              BandType = 0
            end
            object pplblCodigoPostalJuridico: TppLabel
              UserName = 'lblCodigoPostalJuridico'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblCodigoPostalJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 34131
              mmTop = 103717
              mmWidth = 23019
              BandType = 0
            end
            object ppLabel116: TppLabel
              UserName = 'Label47'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Comuna  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 58738
              mmTop = 103717
              mmWidth = 19643
              BandType = 0
            end
            object pplblComunaJuridico: TppLabel
              UserName = 'lblComunaJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblComunaJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 79375
              mmTop = 103717
              mmWidth = 34925
              BandType = 0
            end
            object ppLabel117: TppLabel
              UserName = 'Label48'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Regi'#243'n  :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 123561
              mmTop = 103717
              mmWidth = 16976
              BandType = 0
            end
            object pplblRegionJuridico: TppLabel
              UserName = 'lblRegionJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblRegionJuridico'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 141288
              mmTop = 103717
              mmWidth = 32258
              BandType = 0
            end
            object ppLabel120: TppLabel
              UserName = 'Label43'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Individualizaci'#243'n de los veh'#237'culos y telev'#237'a'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 2381
              mmTop = 109538
              mmWidth = 86064
              BandType = 0
            end
            object ppLabel121: TppLabel
              UserName = 'Label59'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Acci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4022
              mmLeft = 7938
              mmTop = 117475
              mmWidth = 10541
              BandType = 0
            end
            object ppLabel122: TppLabel
              UserName = 'Label60'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4022
              mmLeft = 25400
              mmTop = 117475
              mmWidth = 12065
              BandType = 0
            end
            object ppLabel123: TppLabel
              UserName = 'Label61'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tipo Veh'#237'culo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4022
              mmLeft = 42333
              mmTop = 117475
              mmWidth = 21463
              BandType = 0
            end
            object ppLabel124: TppLabel
              UserName = 'Label62'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Cat.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 82550
              mmTop = 117475
              mmWidth = 6350
              BandType = 0
            end
            object ppLabel125: TppLabel
              UserName = 'Label63'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 90488
              mmTop = 117475
              mmWidth = 9790
              BandType = 0
            end
            object ppLabel126: TppLabel
              UserName = 'Label64'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Telev'#237'a Asoc.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4022
              mmLeft = 129382
              mmTop = 117475
              mmWidth = 21294
              BandType = 0
            end
            object ppLabel127: TppLabel
              UserName = 'Label65'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Vto. Garant'#237'a'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 152929
              mmTop = 117475
              mmWidth = 20902
              BandType = 0
            end
            object ppLabel128: TppLabel
              UserName = 'Label66'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha Alta'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 180711
              mmTop = 117475
              mmWidth = 16669
              BandType = 0
            end
            object ppLabel108: TppLabel
              UserName = 'Label108'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Parking Parque Arauco :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 146050
              mmTop = 82286
              mmWidth = 45974
              BandType = 0
            end
            object pplblAdheridoPAJuridico: TppLabel
              UserName = 'lblAdheridoPA1'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblAdheridoPA'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 193146
              mmTop = 82286
              mmWidth = 6085
              BandType = 0
            end
            object pplblListaAmarillaPersonaJuridica: TppLabel
              UserName = 'lblListaAmarillaPersonaNatural1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'CONVENIO EN LISTA AMARILLA'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsUnderline]
              Transparent = True
              mmHeight = 4022
              mmLeft = 96838
              mmTop = 23019
              mmWidth = 52652
              BandType = 0
            end
          end
          object ppDetailBand12: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 3969
            mmPrintPosition = 0
            object ppShape39: TppShape
              UserName = 'Shape22'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 176477
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object ppShape40: TppShape
              UserName = 'Shape201'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 150813
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object ppShape41: TppShape
              UserName = 'Shape20'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 128588
              mmTop = 0
              mmWidth = 22490
              BandType = 4
            end
            object ppShape42: TppShape
              UserName = 'Shape19'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 89165
              mmTop = 0
              mmWidth = 39688
              BandType = 4
            end
            object ppShape43: TppShape
              UserName = 'Shape18'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 81492
              mmTop = 0
              mmWidth = 7938
              BandType = 4
            end
            object ppShape44: TppShape
              UserName = 'Shape17'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 41010
              mmTop = 0
              mmWidth = 40746
              BandType = 4
            end
            object ppShape45: TppShape
              UserName = 'Shape16'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 22754
              mmTop = 0
              mmWidth = 18521
              BandType = 4
            end
            object ppShape46: TppShape
              UserName = 'Shape15'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3970
              mmLeft = 2381
              mmTop = 0
              mmWidth = 20638
              BandType = 4
            end
            object ppDBText10: TppDBText
              UserName = 'DBText1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'EstadoCuenta'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 2910
              mmTop = 265
              mmWidth = 19315
              BandType = 4
            end
            object ppDBText11: TppDBText
              UserName = 'DBText2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'PatenteCompleta'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 23548
              mmTop = 265
              mmWidth = 17198
              BandType = 4
            end
            object ppDBText12: TppDBText
              UserName = 'DBText3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescripcionTipoVehiculo'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 41804
              mmTop = 265
              mmWidth = 39423
              BandType = 4
            end
            object ppDBText13: TppDBText
              UserName = 'DBText4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'CodigoCategoria'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 82286
              mmTop = 265
              mmWidth = 6350
              BandType = 4
            end
            object ppDBText14: TppDBText
              UserName = 'DBText5'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescripcionMarca'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 89694
              mmTop = 265
              mmWidth = 38629
              BandType = 4
            end
            object ppDBText15: TppDBText
              UserName = 'DBText6'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'TeleviaAsociado'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 129382
              mmTop = 265
              mmWidth = 20902
              BandType = 4
            end
            object ppDBText16: TppDBText
              UserName = 'DBText7'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaVencimientoTAG'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 151607
              mmTop = 265
              mmWidth = 24342
              BandType = 4
            end
            object ppDBText17: TppDBText
              UserName = 'DBText9'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaAltaCuenta'
              DataPipeline = ppDBPlEstadoConvenio
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenio'
              mmHeight = 3703
              mmLeft = 177271
              mmTop = 265
              mmWidth = 24342
              BandType = 4
            end
          end
          object ppFooterBand7: TppFooterBand
            mmBottomOffset = 0
            mmHeight = 87842
            mmPrintPosition = 0
            object ppLabel137: TppLabel
              UserName = 'Label44'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Individualizaci'#243'n Concesionaria'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 2381
              mmTop = 2381
              mmWidth = 64558
              BandType = 8
            end
            object ppLabel138: TppLabel
              UserName = 'Label49'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre o Raz'#243'n Social'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 2381
              mmTop = 10054
              mmWidth = 44831
              BandType = 8
            end
            object ppLabel141: TppLabel
              UserName = 'Label52'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'SOCIEDAD CONCESIONARIA COSTANERA NORTE, S.A.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 51065
              mmTop = 10054
              mmWidth = 113242
              BandType = 8
            end
            object ppLabel144: TppLabel
              UserName = 'Label55'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48154
              mmTop = 10054
              mmWidth = 1058
              BandType = 8
            end
            object ppLabel145: TppLabel
              UserName = 'Label56'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48154
              mmTop = 15610
              mmWidth = 1058
              BandType = 8
            end
            object ppLabel146: TppLabel
              UserName = 'Label57'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = ':'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 48154
              mmTop = 21167
              mmWidth = 1058
              BandType = 8
            end
            object ppLabel142: TppLabel
              UserName = 'Label53'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = '76.496.130-7'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 51594
              mmTop = 15610
              mmWidth = 25135
              BandType = 8
            end
            object ppLabel143: TppLabel
              UserName = 'Label54'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'General Prieto 1430, Independencia - Santiago.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 51065
              mmTop = 21167
              mmWidth = 90086
              BandType = 8
            end
            object ppLabel139: TppLabel
              UserName = 'Label50'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Rol '#218'nico Tributario N'#186
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 2381
              mmTop = 15610
              mmWidth = 43307
              BandType = 8
            end
            object ppLabel140: TppLabel
              UserName = 'Label501'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Domicilio'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 2381
              mmTop = 21167
              mmWidth = 17187
              BandType = 8
            end
            object ppLabel147: TppLabel
              UserName = 'Label58'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Declaraciones'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 2381
              mmTop = 28575
              mmWidth = 28829
              BandType = 8
            end
            object ppmDeclaracionesJuridico: TppMemo
              UserName = 'mDeclaracionesJuridico'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'mDeclaracionesJuridico'
              CharWrap = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 23020
              mmLeft = 2381
              mmTop = 34131
              mmWidth = 198438
              BandType = 8
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object ppSystemVariable4: TppSystemVariable
              UserName = 'SystemVariable4'
              HyperlinkColor = clBlue
              Anchors = [atTop, atRight]
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 2381
              mmTop = 82550
              mmWidth = 197644
              BandType = 8
            end
            object pplblPieConcesionaria: TppLabel
              UserName = 'lblPieConcesionariaJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'p.p. Concesionaria'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4191
              mmLeft = 39706
              mmTop = 58208
              mmWidth = 31454
              BandType = 8
            end
            object pplblPieCliente: TppLabel
              UserName = 'lblPieClienteJuridico'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Cliente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 138907
              mmTop = 58208
              mmWidth = 11642
              BandType = 8
            end
          end
        end
      end
      object ppSubReport6: TppSubReport
        UserName = 'SubReport6'
        ExpandAll = False
        NewPrintJob = True
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 5556
        mmWidth = 203650
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport6: TppChildReport
          AutoStop = False
          DataPipeline = ppDBPlEstadoConvenioAnexo
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 0
          PrinterSetup.mmMarginLeft = 0
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 0
          PrinterSetup.mmPaperHeight = 297000
          PrinterSetup.mmPaperWidth = 210000
          PrinterSetup.PaperSize = 9
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
          object ppHeaderBand3: TppHeaderBand
            mmBottomOffset = 0
            mmHeight = 20373
            mmPrintPosition = 0
            object ppShape63: TppShape
              UserName = 'Shape7'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 176477
              mmTop = 15346
              mmWidth = 25929
              BandType = 0
            end
            object ppShape64: TppShape
              UserName = 'Shape6'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 150813
              mmTop = 15346
              mmWidth = 25929
              BandType = 0
            end
            object ppShape65: TppShape
              UserName = 'Shape5'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 128588
              mmTop = 15346
              mmWidth = 22490
              BandType = 0
            end
            object ppShape66: TppShape
              UserName = 'Shape2'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 89165
              mmTop = 15346
              mmWidth = 39688
              BandType = 0
            end
            object ppShape67: TppShape
              UserName = 'Shape502'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 81492
              mmTop = 15346
              mmWidth = 7938
              BandType = 0
            end
            object ppShape68: TppShape
              UserName = 'Shape1'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 41010
              mmTop = 15346
              mmWidth = 40746
              BandType = 0
            end
            object ppShape69: TppShape
              UserName = 'Shape3'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 22754
              mmTop = 15346
              mmWidth = 18521
              BandType = 0
            end
            object ppShape70: TppShape
              UserName = 'Shape4'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 2381
              mmTop = 15346
              mmWidth = 20638
              BandType = 0
            end
            object pplblNumeroConvenioJuridicoAnexo: TppLabel
              UserName = 'lblNumeroConvenioJuridicoAnexo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 
                'Continuaci'#243'n de la Individualizaci'#243'n de los veh'#237'culos y telev'#237'a.' +
                ' Convenio %s'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 2381
              mmTop = 6350
              mmWidth = 154051
              BandType = 0
            end
            object ppLabel96: TppLabel
              UserName = 'Label1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Acci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 7938
              mmTop = 16404
              mmWidth = 10583
              BandType = 0
            end
            object ppLabel97: TppLabel
              UserName = 'Label2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 25400
              mmTop = 16404
              mmWidth = 12171
              BandType = 0
            end
            object ppLabel98: TppLabel
              UserName = 'Label3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tipo Veh'#237'culo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 42333
              mmTop = 16404
              mmWidth = 21431
              BandType = 0
            end
            object ppLabel99: TppLabel
              UserName = 'Label801'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Cat.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 82550
              mmTop = 16404
              mmWidth = 6350
              BandType = 0
            end
            object ppLabel100: TppLabel
              UserName = 'Label4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 90488
              mmTop = 16404
              mmWidth = 9790
              BandType = 0
            end
            object ppLabel101: TppLabel
              UserName = 'Label5'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Telev'#237'a Asoc.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 129382
              mmTop = 16404
              mmWidth = 21167
              BandType = 0
            end
            object ppLabel102: TppLabel
              UserName = 'Label6'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Vto. Garant'#237'a'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 152929
              mmTop = 16404
              mmWidth = 20902
              BandType = 0
            end
            object ppLabel103: TppLabel
              UserName = 'Label7'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha Alta'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 178594
              mmTop = 16404
              mmWidth = 16669
              BandType = 0
            end
          end
          object ppDetailBand13: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 3969
            mmPrintPosition = 0
            object ppShape71: TppShape
              UserName = 'Shape14'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 176477
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object ppShape72: TppShape
              UserName = 'Shape13'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 150813
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object ppShape73: TppShape
              UserName = 'Shape601'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 128588
              mmTop = 0
              mmWidth = 22490
              BandType = 4
            end
            object ppShape74: TppShape
              UserName = 'Shape12'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 89165
              mmTop = 0
              mmWidth = 39688
              BandType = 4
            end
            object ppShape75: TppShape
              UserName = 'Shape11'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 81492
              mmTop = 0
              mmWidth = 7938
              BandType = 4
            end
            object ppShape76: TppShape
              UserName = 'Shape10'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 41010
              mmTop = 0
              mmWidth = 40746
              BandType = 4
            end
            object ppShape77: TppShape
              UserName = 'Shape9'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 22754
              mmTop = 0
              mmWidth = 18521
              BandType = 4
            end
            object ppShape78: TppShape
              UserName = 'Shape8'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 3969
              mmLeft = 2381
              mmTop = 0
              mmWidth = 20638
              BandType = 4
            end
            object ppDBText30: TppDBText
              UserName = 'DBText1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'EstadoCuenta'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 2910
              mmTop = 265
              mmWidth = 19315
              BandType = 4
            end
            object ppDBText31: TppDBText
              UserName = 'DBText2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'PatenteCompleta'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 23548
              mmTop = 265
              mmWidth = 17198
              BandType = 4
            end
            object ppDBText32: TppDBText
              UserName = 'DBText201'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescripcionTipoVehiculo'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 41804
              mmTop = 265
              mmWidth = 39423
              BandType = 4
            end
            object ppDBText33: TppDBText
              UserName = 'DBText3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'CodigoCategoria'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 82286
              mmTop = 265
              mmWidth = 6350
              BandType = 4
            end
            object ppDBText29: TppDBText
              UserName = 'DBText4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescripcionMarca'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 89694
              mmTop = 265
              mmWidth = 38629
              BandType = 4
            end
            object ppDBText28: TppDBText
              UserName = 'DBText5'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'TeleviaAsociado'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 129382
              mmTop = 265
              mmWidth = 20902
              BandType = 4
            end
            object ppDBText27: TppDBText
              UserName = 'DBText6'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaVencimientoTAG'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 151607
              mmTop = 265
              mmWidth = 24342
              BandType = 4
            end
            object ppDBText26: TppDBText
              UserName = 'DBText7'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaAltaCuenta'
              DataPipeline = ppDBPlEstadoConvenioAnexo
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              DataPipelineName = 'ppDBPlEstadoConvenioAnexo'
              mmHeight = 3704
              mmLeft = 177271
              mmTop = 265
              mmWidth = 24342
              BandType = 4
            end
          end
          object ppFooterBand3: TppFooterBand
            mmBottomOffset = 0
            mmHeight = 6350
            mmPrintPosition = 0
            object ppSystemVariable2: TppSystemVariable
              UserName = 'SystemVariable2'
              HyperlinkColor = clBlue
              Anchors = [atTop, atRight]
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3969
              mmLeft = 5292
              mmTop = 1058
              mmWidth = 197644
              BandType = 8
            end
          end
        end
      end
    end
    object ppParameterList7: TppParameterList
    end
  end
  object spObtenerMaestroConcesionaria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMaestroConcesionaria'
    Parameters = <>
    Left = 701
    Top = 346
  end
  object dsContratoArrendamientoTAG: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Patente'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ClaseVehiculo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Marca'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Modelo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'SerialTAG'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FechaInstalacion'
        DataType = ftString
        Size = 10
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 69
    Top = 80
    Data = {
      D00000009619E0BD010000001800000006000000000003000000D00007506174
      656E74650100490000000100055749445448020002000A000D436C6173655665
      686963756C6F0100490000000100055749445448020002003200054D61726361
      0100490000000100055749445448020002003200064D6F64656C6F0100490000
      0001000557494454480200020032000953657269616C54414701004900000001
      00055749445448020002003200104665636861496E7374616C6163696F6E0100
      490000000100055749445448020002000A000000}
  end
  object rptBajaContrato: TppReport
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 213
    Top = 176
    Version = '12.04'
    mmColumnWidth = 0
    object pdtlbnd1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 83873
      mmPrintPosition = 0
      object ContratoBajaCaratula: TppSubReport
        UserName = 'ContratoBajaCaratula'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 16404
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt4: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object phdrbnd1: TppHeaderBand
            mmBottomOffset = 0
            mmHeight = 100013
            mmPrintPosition = 0
            object ppBarCode2: TppBarCode
              UserName = 'BarCode2'
              AlignBarCode = ahLeft
              BarCodeType = bcCode39
              BarColor = clWindowText
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Data = '00100968630601001'
              PrintHumanReadable = False
              AutoSize = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Courier New'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 6615
              mmLeft = 113771
              mmTop = 46038
              mmWidth = 79904
              BandType = 0
              mmBarWidth = 220
              mmWideBarRatio = 76200
            end
            object ppLabel111: TppLabel
              UserName = 'Label111'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero del Convenio:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 48154
              mmWidth = 45170
              BandType = 0
            end
            object ppLabel118: TppLabel
              UserName = 'Label118'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre del Cliente:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 55033
              mmWidth = 40090
              BandType = 0
            end
            object ppLabel119: TppLabel
              UserName = 'Label119'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RUT del Cliente:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 60854
              mmWidth = 32766
              BandType = 0
            end
            object ppLine9: TppLine
              UserName = 'Line9'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 794
              mmLeft = 25929
              mmTop = 73025
              mmWidth = 165894
              BandType = 0
            end
            object ppLabel155: TppLabel
              UserName = 'Label155'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Punto de Entrega:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 74613
              mmWidth = 36534
              BandType = 0
            end
            object ppLabel156: TppLabel
              UserName = 'Label156'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Operador:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 80433
              mmWidth = 20532
              BandType = 0
            end
            object ppLabel177: TppLabel
              UserName = 'Label177'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha y Hora:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 86254
              mmWidth = 27982
              BandType = 0
            end
            object ppLine10: TppLine
              UserName = 'Line10'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 1852
              mmLeft = 25929
              mmTop = 98161
              mmWidth = 165894
              BandType = 0
            end
            object ppLabel179: TppLabel
              UserName = 'Label179'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Documentos Incluidos en la Carpeta'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 25929
              mmTop = 91811
              mmWidth = 73290
              BandType = 0
            end
            object BajaCaratulaNumeroConvenio: TppLabel
              UserName = 'BajaCaratulaNumeroConvenio'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 48154
              mmWidth = 39952
              BandType = 0
            end
            object BajaCaratulaNombreCliente: TppLabel
              UserName = 'BajaCaratulaNombreCliente'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 72496
              mmTop = 55033
              mmWidth = 119327
              BandType = 0
            end
            object BajaCaratulaRutCliente: TppLabel
              UserName = 'BajaCaratulaRutCliente'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 61119
              mmWidth = 39952
              BandType = 0
            end
            object BajaCaratulaGiroCliente: TppLabel
              UserName = 'BajaCaratulaGiroCliente'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 66675
              mmWidth = 39952
              BandType = 0
            end
            object BajaCaratulaPuntoEntrega: TppLabel
              UserName = 'BajaCaratulaPuntoEntrega'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 74613
              mmWidth = 119327
              BandType = 0
            end
            object BajaCaratulaOperador: TppLabel
              UserName = 'BajaCaratulaOperador'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 80433
              mmWidth = 119327
              BandType = 0
            end
            object BajaCaratulaFechaHora: TppLabel
              UserName = 'BajaCaratulaFechaHora'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 86254
              mmWidth = 39952
              BandType = 0
            end
            object BajaCaratulaTitulo: TppLabel
              UserName = 'BajaCaratulaTitulo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Car'#225'tula para la Carpeta del Cliente - Persona Jur'#237'dica'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 14
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 5821
              mmLeft = 25135
              mmTop = 33073
              mmWidth = 166688
              BandType = 0
            end
            object BajaCaratulaLblGiro: TppLabel
              UserName = 'BajaCaratulaLblGiro'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Giro del Cliente:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 26458
              mmTop = 67469
              mmWidth = 32808
              BandType = 0
            end
          end
          object pdtlbnd8: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 5027
            mmPrintPosition = 0
            object ppShape84: TppShape
              UserName = 'Check01'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              Pen.Width = 2
              mmHeight = 4233
              mmLeft = 181240
              mmTop = 265
              mmWidth = 4233
              BandType = 4
            end
            object BajaCaratulaLineaCaratula: TppDBText
              UserName = 'BajaCaratulaLineaCatarula'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'LineaCaratula'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 25929
              mmTop = 265
              mmWidth = 153459
              BandType = 4
            end
          end
          object pftrbnd1: TppFooterBand
            mmBottomOffset = 0
            mmHeight = 34660
            mmPrintPosition = 0
            object ppLine11: TppLine
              UserName = 'Line11'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 1852
              mmLeft = 25929
              mmTop = 3440
              mmWidth = 165894
              BandType = 8
            end
            object ppLabel196: TppLabel
              UserName = 'Label196'
              HyperlinkColor = clBlue
              Anchors = [atTop, atRight]
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Firma Revisor'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 14
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              Visible = False
              mmHeight = 5800
              mmLeft = 137319
              mmTop = 26988
              mmWidth = 28490
              BandType = 8
            end
            object ppLine12: TppLine
              UserName = 'Line12'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Visible = False
              Weight = 0.750000000000000000
              mmHeight = 1588
              mmLeft = 130440
              mmTop = 24077
              mmWidth = 40746
              BandType = 8
            end
            object ppLabel197: TppLabel
              UserName = 'Comentario01'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Comentario :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 14
              Font.Style = []
              Transparent = True
              mmHeight = 5821
              mmLeft = 25929
              mmTop = 6350
              mmWidth = 25929
              BandType = 8
            end
          end
        end
      end
      object ContratoBaja: TppSubReport
        UserName = 'ContratoBaja'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 39952
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt5: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object ptlbnd3: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 75936
            mmPrintPosition = 0
            object ppShape80: TppShape
              UserName = 'Shape80'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 2910
              mmTop = 67998
              mmWidth = 7673
              BandType = 1
            end
            object ppShape92: TppShape
              UserName = 'Shape92'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 10054
              mmTop = 67998
              mmWidth = 14288
              BandType = 1
            end
            object ppShape82: TppShape
              UserName = 'Shape82'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 23813
              mmTop = 67998
              mmWidth = 34131
              BandType = 1
            end
            object ppShape83: TppShape
              UserName = 'Shape83'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 57415
              mmTop = 67998
              mmWidth = 22490
              BandType = 1
            end
            object ppShape89: TppShape
              UserName = 'Shape89'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 78846
              mmTop = 67998
              mmWidth = 25665
              BandType = 1
            end
            object ppShape90: TppShape
              UserName = 'Shape90'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 103717
              mmTop = 67998
              mmWidth = 20902
              BandType = 1
            end
            object ppShape91: TppShape
              UserName = 'Shape91'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 124090
              mmTop = 67998
              mmWidth = 16669
              BandType = 1
            end
            object ppShape81: TppShape
              UserName = 'Shape81'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 139965
              mmTop = 67998
              mmWidth = 18785
              BandType = 1
            end
            object ppShape93: TppShape
              UserName = 'Shape93'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 157957
              mmTop = 67998
              mmWidth = 18785
              BandType = 1
            end
            object ppShape94: TppShape
              UserName = 'Shape94'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 175948
              mmTop = 67998
              mmWidth = 25135
              BandType = 1
            end
            object plblTituloContrato: TppLabel
              UserName = 'plblTitulo2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'TERMINO DE CONTRATO DE ARRENDAMIENTO DE TAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 5027
              mmLeft = 38629
              mmTop = 21960
              mmWidth = 120386
              BandType = 1
            end
            object imgLogoConcesionaria: TppImage
              UserName = 'imgLogo'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                009C0000002F0802000000998434C1000000017352474200AECE1CE900000004
                67414D410000B18F0BFC6105000000097048597300000EC300000EC301C76FA8
                6400001E3C4944415478DAED9C075C1447DBC06F0FAED0BB74057B41A404C528
                F60A268A051B364404A5496FD27BEF0A0A82220611C5466CD8308A0D44B060EC
                200888D2EEB87EDFB37B709C08C42498F77D3F33FE7239F666A7FD9F79CAECCC
                225C2E87C56452A83404417068E20A0909898989E17889CBC1FE8730188C4FAD
                6D2422515A5A02C7859C5CDE75F8D2D2DA4E673064A5A50804027CA1D1E8DD45
                7D96B85CAEA80889402062F721EDEDED1D944E2121BCBC8C345E58087EC6F597
                10844E837F0C7EB124128144227FDE42AC310897D24165B139820DC0EA2543DB
                E0775A27ED536BBB84B8A8B8B828AEFF0AB1A2701D1D14369B032D141717FFBC
                225E163C0EC7696BEB8086138904111191CF33A02534377F6230592422415656
                1AAB8E5F25C2E1B03FB6B4C11518373C5E887747473B85831682409B09046151
                51D12FFAC8AB172EB06BEA1A3A3AA85C1C574C54444549814824F173224C06C3
                272CF97EE5133289C4E1A09700AA8C94C4D41FB4D72D5F2C292989350589DD9B
                9D7BFC9C869A72A4AFE3708DA15DD520F8A7CF5EB805C57F686ED9BCFA27AB4D
                66BEE12937EF559049448C05C21F56F8042AB37ED4F771DA86C3A17779852635
                B7B4C24FC673A67B3858A2B5F4C915C1BF7AFDD63D38B18342C5E3F190054A25
                1208F272D2063A137E5E384B71883C7A235A15527CBD343A351BDA8F558AE3D5
                4FA176AE30996B6BB98EC562ED0E4BBAFCDBFD31C387C605BBCAC9CAF4E6D42D
                43F05F6EC19903BF9C061E30263A13467B395A8A8B8BF5B410C137363507C5A6
                3D7F5503F945C9240F7B0B03DD893D0522F8C2B39712337E81DBA13D0E966B7F
                5E3C47F0D7A28BD72353B2019FB3F5869F16CE866BC74E9DDF7FB8505898D778
                2E7C91919634D4D75EB36CA1B494147FC0A117F9A72E9CB970ED754D7D5B0705
                AE898B89AA282ACC9B396593D94FA81C7039484747C7CAAD2E0F1FFF2E224286
                1640712C161B3EA1290B661A4607384B494AC09D8EDE11D9F9A787A9A9642506
                EA688DE5D771FB5EC56607DF4FAD1D96EB97067BDAAFB172BD547247944C8651
                26080BE311044A64B258808C4AA399CC9D9E95148420F8E48CDC90F84C111112
                34514D79484146B4B292623F438CBF535E6966E90A44E12F369B8D7D72586CB6
                0899347AF8303F67AB1FA7E8F1246FFFA163BE517B6188D91CB42BBC99D1DE41
                B55C6F1AE9E7C460D0D7DB785DBF5DAEA1A674FC40ACAAB2527F35C28DAE01B1
                078F9E95101361B2D930E9E2029C4C97CC17A4929153E01BB907E613348CC960
                86EF7658BF7249F7B0206C16DBD2C9FFFCD55B226432B59366326F7A5AF46E54
                DA786201B7E71EF78D4885F1F175DEBE6DC34AB8E61F95BAEFD0713299CCE6B0
                211BF411868E202C34638A6E6C90ABBC9C2CE4A1523B03A2F71E3B7309D42181
                4890101385F23A285D1AC578EEF4F0DD8E9212E22854F31DDE772B1E698D1D19
                E2614B26139FFCFE2AFD60C1AB9A3A8011B5DB61D5D245509C5B40ECA1636787
                A929EF8BF19D387E341FEABDF2CA6DCE41A0D360A6FABBED78F8E829DC083309
                6A4A39905F5BD7A03954C56AC30A90260683396298AAD6F831542AD5D229E0D6
                BD87B2529274260B147BA09BCD9AE5C6FD0DF1FD8A471B767AC3ED26F38D36AE
                5A02385FBE795774E9C6EDB24A3A83A9A632645FB4EFF8B123216FD691C2E0F8
                7DD04FBBAD6B664D33A0D3E93C09505755525755A6D168506F090A55F997B4F0
                01C408A07A87261D2E28521A220FD2D1F8E1E392F94689A19E783C8269003C74
                61BB4BD0B5D2B2E14355A1EF30D6C11E3B579B2EE60FCBE3A7CFB738FA7D6A69
                17171301A8D292E2D9898163468FE0673878F45440741A40F576B4DCBC76195C
                0B8DDFB7FFF009B014EEB69B7FD0D1FAFDE59B8CDC138FAA5F824573DCB6CE79
                C766C893B4EF70F4DE43C06FA486BA95F90A9D8963C17ADE29AB4A3F54F0AEBE
                0978395B9BDB5B997741BD5D5E65A8AF75FC403CAF5FBF9CF8D5233811641050
                F9BA587F0DD44D664B02DC77F247864EA7ADB00005F06CB29E566E6A189144E2
                FF5406906C77436B80D0C5EBA5D5CFDF982E9E1D17EC262C2CDCC7287743EDA4
                D16D36AF72B3B5E05DA650A97E11A985E7AED2E874F315C661DE0E081ECF83CA
                60B0F6C7FA2E983DAD57497F16EAC1FC33E3470F1F3B42A3A8F886829C4C7652
                D0A8111AE82D08FE6ED9C34DF6BE20BB4B16CCB874BDB4A9B925D4D3B60B2AA6
                BD4167F847A78D1A3E14B4DDE1E3E7DA299400176B0C1E9727167D42DD97731C
                0CE4A1E4205DED0970E5CA8DDB0E3E51E0CACC9CAA9F931A5657FF7E9D8DD7AB
                B7752A4AF27B22BC74268EE737B9F8DA2D27FF58706EC68FD6CC4B8FEC813A45
                4FEB78660C8219EDD3E7AED879478051DCB1D9CCD576CB9F81DAD5E896D656D0
                75554F9FEB4F1A7F203E404A4A92DFE1E894CCB8F4DC09A3871F4808084BCC3C
                7DA1445951FE706AC808CD610343DD66BEDCCB711B66A5D1917D5B5BB766BB47
                7D4393E650D59C94101565453ED48C38BFF9B37EEC2904CBFF67A1661F3D3D69
                FC682BF3E581B1FB9A9A3F05BA0115535E96E098BDC9078ECE9966B0EAE7F981
                B1E96DED1401A878069D6EE1E877F1FAED0DAB4CECB7AE05FC2FDFD41A19EA65
                25040A83BF86E51900EABE98DD530D74E1CACB576F3739F8BE7AF3CE688AEE91
                F4C833E7AFDAF944802D58BB6C6188B74357BF780DE672D7EFF0FCEDCE03D088
                B929A13D507F98340E6A251089BFBF781D9A9071F7C123511191CC38BFA99375
                070F2A1E5CCA0DB65EA5F7ABD69A2E8A0D74CDC93FED139E02C6C6DF65FB06B3
                9FFB7097FA870A39B7390514DFB8033EF9E13D217ADA1330A8FB41164D8D674F
                1C379AC36633992C156585C573A72188D05F803A7ED4F09470CFA0B874200408
                61B20A0B139A9B3FAEB5F67CF4EC8587EDE6297ADA16BBFCA16D82501F563DDD
                60E7D3DADE11EE6DBFC6D478D7EEC8A3A72E2AC9CB1E480CD49E30E60FA1F267
                2A3853AE81716DEDD49F1618A546FA2482EEDD73904C2406B8DAAC5D610CBE77
                7708824E1530C987F2CF82CF9111EBD70515F4A48484B8AC34F8BAB8D6B67610
                3D395969CB75CB2CCD57F09CC8C1827AEDE6DD1DEEA1600BE3039D972C9C5DFD
                FB4BF0B3DEBD6F9A3DCD00B2E1515782F3B550B1A13F72E21CD898030981D3A7
                E8F1A08203D5D9490303030DA17476EA6B8F030D0461C95F803A4A63E8E99C84
                43F96742133261400EC4FBC1709F387BC925200EC2B3A3FBA240E36D730EEC05
                357EEFC198BD3960C87353433486A9E79F3CE71D9602B1CDAEEDEB1DACCC0780
                CAB7A920916FDFD5A51D2C00FF067E8AF1DF656A32DF3F6A4FD62F274182E383
                5C16CF9B21E0B5A150E3D30E2567E681102785B87541AD7CF2BBA8A8088C0EC4
                8E401B5CF91D5BCCEC2CD7F39417DC330850B17242E3D29332F3268E1D519815
                278A45C3F65EE1C7CF1643A49519EFA7356ECC9F82EA11140F93008CF1C1C420
                4383493CA8100C802D5452407D1C1A833E6EA4A6CB8E8D42C284BF0015FC9193
                D971750D4DEB6DBC1B9B3F3A6D3777B2D964EF199677EAC29CE99373F7869594
                96818CF640C5A1D1ED56477FF0A1404982A30057DADADA965BB8001ED0A2FB63
                FDD0D00887EB136AE6919310580205C08445FF4C10D075A68BFC5C6D20CEF68F
                4CCDCA3B0D31715CA0B3C9FC99BDA0C6A666A766E73398CCBD11DE7CEFF7B1D6
                D811E1DE763069E2D272C0BD8448232ED0455F478B07C3D53F26A7A008A042B3
                B4C68DE243BD5F5165E914086EDE46B325817F00151ADABAC6CAA3FAC5EBB123
                358CE74D171602652674FD56D9DDF247E0EF78D859586F36C3D42FF76BA0B2D9
                2CB85E7ABF524A523C373574DC98913C9BCA64B241672E5930537085043EFF1A
                D4FC7D91F2F2B2EBAC3DAE9796194DD675B23677F28B7D5DF3CED769BBD5A655
                574A6E83F32108B5F4EE83AD4E814C2673DA141D43BD89BCD8AAF0D7CB6F6AEB
                C1B13A10EF6FA0A73D30547151112027264AD65057996B3479D9E239A825C6E1
                62F76427EC3B02F36DB793D506B39F7AA95FD780988233C5E0DB1E4A09FECC51
                3A91857ABF278B8ADD831360CAFEBC70665298879090305CF40E4DC8CA3B2327
                2B951CE23EE347033ED4925BF760A6824C6DDFB8DCC3DE7260A897AEDDB4F78E
                44C599DAD941A1A28B125C1CC4C74005F4C6645D2D70707A2FCDF409154B1555
                4F2C1CFC3F7C6A01050BEA17AAE0434D8DF032993F630047292F3D424971488F
                F470B9FD4105FC90333DFB68486226C460208E0F1E3D939410CB4A080031BA5C
                72DBFE73A89149197BB28F91C924B062709D37E412E2A212E26210DBD86C5CE9
                6A67D11F549E4D85A905734958082F272385C7069FD7FEBCC25FDD0213400FAD
                34991BE1E724E828B5B777ACD9EE5EF9E4B9BAAA2274AD07EA64DD0947D32321
                F6A07576AEB7F12C7FF44C424C2C2B3140571B759DD3B28F46246741DF572F9D
                1FEA658FC7566D40517B8726E69DBC00BE094CEBA5FC45937EA07A06C51F3EFE
                ABA282ECECE906A26474AD03C156276EDFAF7C5D53472611C11341ABEB07AACD
                A6556E765D21CDAB37353E61C9A565554C16D3D56693DD36D452F0BD5FB0110B
                E7F41BD268AA291FCB8C969793FB8C7A3F508FEC0D83395DF5B87AB3835F3BA5
                1308818A9B33DD203DC60FA6452FA8144AC7EA6DEE95D52F46690E9D663089B7
                9806D93AA8D4925BE54D1F3F698D19014A1B0CFC005021A2C514E4E70D43F04F
                AA9F8323DDF4B165889C4C4290ABA1810EBFF50969392959F9341A1D5CAAE430
                AF3EA042AE8C9C82A0B87D0882DF686612E086469FCF5FBED968E753DFD00C0E
                82F1DCE940052E42978A8A6F428BB5C60CCF4E0A5690971B006AFDFBC68D76BB
                1F3F7B397FC694ACC440DE1A262FED3990179E740064D01A04198DA03802ABA4
                5D50592C3684BC738DA6804FFBB6AEA1E4D6FD5735757071DAE449C9619EBC05
                9703B9852109FB61DC4D4DE6E8688D653298388CC1C4B1236108E8349A8553C0
                ADBB1530037E5E384B52421C8AA2311810B718CF37EA599BED0B2A93C1B07209
                BE7AF32E0C3A8DCE08F6D8016E2D6F0404A1165FBFE5E01DD54EA53AA13ED106
                7E07A1A2ED2E41E7AEDE9492104F08769B3BC3F08F439ADEBE0502ADF38B4CC9
                C054B48AA2C20A93B963476BD2E90CB0EB10498366559097498FF2D19B34015D
                585FBEC5B9B4AC12D4EFD99C241E548890566FF778F5B656435D352F2D7CC4F0
                6170F168E1B9D0C40C08B4C14AA06B765C1C9BC301121026867AD91A19FED063
                0E11FCA79616D3CD4EE07FFDA003B212212525957FEABCAD6738EA2B79D96E5D
                BF5CB0C10F2A9F98EFF4067FC460D20450773232D282127AB7BC728585337C83
                EA180C066FF0D1557219A91953F53CED2DD45494BB97090BBCC29345C9243007
                E0FDF2969EA1B7E0B3642606B2984C886B61A6826E04A1466D1D826B6BEF80D1
                39BC270C4163240E1FAAB36FF4FE2385A335878243A7AAA20497F7A3EB82A9F0
                B39AB24241668CBAAA0A5CBC70E5372B9720801A1BE0BC7EE51217FF18000323
                9EBB37545760710013B8E33EE1A9543022EB9745FA3967E69EF00C4904A8109F
                806D860CBE11297BB28E62214DB0D1D41FFA0CD95B3EB57884245EB8560A8205
                4A008F4760C841E1090B0969A82BC350982C980514103A9D1E959C5556F9444F
                7B9C87BD056F351C0AD8939577E5C65D7131117BCBB5D8E2053A6AA5772B4EFC
                7AF9C5EB9AB60E0A3448465A0AF489D9B205A3866BF4D2609D9D9D5129D9954F
                7FD79B380E9C0B12897CE2ECC5DC82737272D29EF65B86A9AB093A6F2C262B32
                39ABBCEAA9B2A282DB8E4D6A6ACA8250618AC7A61D6A6BA3805BC8CB0FD8D454
                8618EA6B1BEA4FC42158148402C4DF7B50B53FE7044041703D4F69A0FF33A7EA
                6F5AB314BA9079F8C46F772B4824025F13C0281B4D9EB4D57C654F888CF91D39
                F9670ACF5D012DEA61BF450A7DAA81AB7D571F9992F5BEB1199C17AB0D2B113C
                9A0D5462CCDE43204376166B26EB6B472665C2F4D09930DAC3612B914014EC05
                DC1E9E98D9F0E1A381EE04375B8B5B77CBF7661F83FA206E447D141CEEF8998B
                F9A72FC254065F7D64AFF1141858D03780E0DAADB2FA86262A9506BC64652427
                8E1B05A2892E7861BD00D4A0EBB8E07C82FCA28EA240C768B44E213C9E402408
                7A5938CC89A550C1BA20E0E0743FA4FB72D100E92A9644C6DC4F3481AE00BF0E
                35FE5F3EC6E27240DE41E2B0EA7AAF3F60C6850D06180A411FFE08A8EE2FCD21
                97D3FBD11BBA4CC6EF17978D13408E760D8FEFED72635241A7D189442282E78F
                09DA486CA044042400CF62A2CA83802164B339E0F79245C8D848F6EE2387CDA2
                33182402110FD289E0994C0614432012F91A824EA7A14F418484FB262A301AD0
                80E6E64F20AF60B0A5A5C4454444052920D0929E31FA6C8090EECE7F0EACE73A
                AE8F5FFBA89EEFA475DFD86F2081FBE35FBFBED23EF3F76E7CFFA5F5D9DAEE01
                FDC25BC6F1DDFEAFEA45F78A69DF6D1B80E8D70D0806F56F264CF509FCCD19F0
                F9F3BFE99BA7BF0D15416A5AA9A5756DBCBF203E99A62AAD262532D04E867FD3
                374E7F1F2ABEBCBE25ABB20EFB8E6371B8DBB4D57494A5BE4A87FC9BBE4D1A04
                A80F1B5A8F3C7E8F7D47A16E9CA0325151F25FA8FFC13408501FBC6FC9A9C2A0
                E2712C3667CB44B5494AFF42FD4FA641805ADDDC51F4B219FB8E6373B84B472A
                8C9215EB2B76EED3F3FCC6E9BB94AD41F07E996C76279DC5FB0E9EB528519840
                10EEED0077FBFD140A1522B96F4E97CB259208A2A2BD36BA7E2F691066EAED7B
                15E14999E84206826FEFA0B8DA6D5E306B9AE0620A7CBC6F6C3C7FE556496959
                7DC3076C47E037872A242CA4AAACB068F6B4C5738D4444C8DF15D741807AE1CA
                4D1BF7106C2912DFD2DA961CEEB966D9628175545C6151717266DE8BD7B5783C
                823EFF45FE1125CCE5B2B105B339D327EF76B61AAAA6F2FD701D04A8C5D74B1D
                7C22BBA0B6B5C705BAACFC6901368228BFFD3905D1A907994C968828F91FB7A8
                E862777B3B65BAA16E729887AC8CCC77C2F59B41C51E005CB971DBCE2B028812
                89E8B37B168BDD893E1EF9E6230B0A418444424D3BB61ED2D141B1B558ED8A6E
                2FE57E0FAB22DF0E2A8ECEA05BEEF2BF711BDDB7087F32184C29297183491364
                A425B9DF72641104A1503AEF553C7EDFD4CC3B0042A333D494871C4C0A52FF3E
                94F03784FAB0EAE956E740D07E3063E80CA686BA72A0DB8E29FADAC83F6253AB
                1E3FF30A4BAE7EFE1A9404B48D46A3A7847B2E9C33FD5FA85F5340BF50CF5FFE
                CDD137125C62708EA89D34876DEBD00D9268FA0714202A3759BF148626641009
                04102370CB833C766E58F5D30027B1B0A671FA7EEA3270EAB9EBF33F05BC45FE
                6F3D0FF270BD9E5F0D9A69F88650AF8241F58E84004648488842A52D5B342B36
                C8F5ABCBE50E7CB8F16B82A2FC53177CC29309C2C200B58342F577B5DEB47A69
                57E15D95F04FB121EF1B3FB4B6B68F19A5F9B6A6AEE9638BFE2474DF4253D347
                90063299585E554D2408C33FF8534579C8284DF5078F9EC177F012C68C183676
                F48807954F6AEB1B44C8243DEDF1605FEE573C86EBBC8372743AA3FAC51B5054
                35EFDE0FD750873CBCD3822C365B6B1CBAD996C1643E7BFE6684861A1A7AFD97
                43ADADABDFB0D3A7B6BE110C1BB60306315D3C6BC982590A7232BC87E65F6044
                41494A88CBCA741FD9ECFFA9248341FFF8A9153B56DB97447071CA8A0A672F95
                0842458F61ADFAE92395CEBB033E65C904213CC22BF3E8C97377CAAAA2035CA2
                530EECCB399E93126AA0371182B1DFEE3CD8BA6ED9D98B254F5FBC7E5BFBDEC8
                5057576BAC949444504CDABC1986E0F4C195495A632C1CFC476AAA7339DC4F6D
                6DDE8E96E0F04F33D059B574210E3B125370FA9287BD8557685280AB8DA6863A
                8D46DB62EF4BA1527F498F1215156968FCE01614EFEFB25D7398FAA058876FEB
                FD06C5ECDD77F8047082A163B339605925C5C5789E703F890BDA72F488610B67
                FDB868CE740909B12F9F51D7BCAB3F73E1FAB59BF7DED635801AF8D2428300B1
                582C5FE76D440271975F340F2A85420DF3DCA16934FBC4E37764213C7A4A5008
                6FA5A33A448CCC53B3C74E9DBFF7E071B8EFAEE894ACDFEE948FD0508FF2732A
                2ABE71F15A693CB627FBE2D59BE7AFDC8A0E7086EFD76FDD2D38733921C49D57
                6343D307AF90C4187F67696929334B679BCD66303BCF5C2C490EF7825FC313F6
                0F919701C0B61EE101AED61AC3D44AEF96FF5278BEA383BAC16CC9CC69931B9B
                3E788624ED76DAA63154EDBF1B2A6657DE37345ABB859455564B8A8BF236A300
                DA815D5F50BB2C261B3EA7FEA0ED61B765E2F8318286E7D4F9AB09E9879FBFAA
                01E70BB47A9F2A18A03259AC306F5B29090947DF2841A8AA3FCECAAFAAE9822A
                8CB7D71FAA24DE05B5E0F405801AB6DB313C21435141B6E251F532E339501408
                10CF6A9CBB7CE3D2D5D2E84017F87EEBDE83C8A4ACD54B1790C9A4D9D30CA047
                F6DE1126F38C080442F1F55B6E761662A222302691BB1D41153B78477A396E95
                9596822B6002801C60D61E3792C16297573E0970B76DFAD00C507D7659FE2F40
                C57E7DF6E2B54F58F2EDF22AA2B030F4B96BF3D81F2528AD9D42D550534E0C71
                9BA4358E772EFCC8F1B381B1FB2034121DD0F6F0A0067BEC901013EB05557DDA
                AC638F6AF933D5564F5DF10BA8C1B1695A63472A0D91CB395634C350F75EC593
                486CE7F4AFC52597AEDD8EC1A096DE7B109290095E02405D34FB471030905D75
                15A527CF5E1ACF9D6EBD65350E3D7D1403AEBEA6BA0A94131BE8FCE1638B8B7F
                6CB0A7ADACB4E44E8F50472B73069311999C9D11EFCF66B15D03E3FF77A06219
                5ADBDAB28E9C3A5B5C525BD740A17672B9FD7839D8048629482691F098A9034F
                447FD2B88C587F506B15954FB638FAB75328646C132B7819E83A069BF365511C
                360A35C67F97BCAC8CC3EE4841A8CA5367E63E7C2B228C07F000D579F2306509
                912FA10E1FA6B66E8589774802A804B005419E76BDA082FA3D517435AEDBEF03
                F5EB1D9A14E5BBABE9C3A78098B4980067254585A28BD72EDFB82B232501B676
                B5A931E4710F8C8FF1777A5CFD223821E3079D0950ED9DB24A179B0D46867A0E
                3E512085D8A1814178FCF0EDA1E2BA6C615373F3FD078F6BEA1AE80C469F54D1
                9D7E1CEEC327CFC1A4F1DE4F009F9D7446B897EDAAA58B40F0F34E5E9494401F
                BCC064959692986334595D59918BEBADCEB9E87E420E4CA027BFBF760F8EE73B
                4A21EE36B38C173D6A6C1542F745E38410C44049528C28CC6B61DE895F6F9755
                C606B9F986276B0E55DDB2CE14E26C332BF739D30D52237D20CBE90B57CF17DF
                4C8E40CD6449E9BDB8B4C34EDBCDA1E4A1AACA62A2643BEF88608F9DE0E9EC74
                0FD19F38CEC27C794B4BAB85A31F08715662A0B292E2FBC62667DF98502FDBF4
                83C7C68D19819DD140728E9EAA78F4CCC7C9D2D63362D9E2D92A4A0A8AF2B2E0
                81FFCDD86610A05EBA76CB7177141F6A7C90EB0AC1D723F0897DDD9319F0EF0F
                E69D8C4FCFE515D841E9042D17ECB97389B97DDDFB2670B260160E91970DF7B6
                FB71B2DEC045E5159EF38D4CED0969DC6C36A147600553CF19A3BB65552FDFD4
                AE365D54587419A6F874435DB87A20F704E80C6CCF300EAC6CD5E3E7EB5799C0
                F7B7B575E02113D1B7D1306718EA8159CDCE3BB974D16C0505F99B77CA5FBCAE
                C10E3021878E9E6A6D6FB7DDBA1E240DE2A582339740448A4BEE2C5930437188
                0294D3D0D874FC4CF132E3D927CF5D7DF7BE111AA3A73DCED464DEDFDCBC3708
                50CF5FBEE1E4178343F7CFA250C10A9A1ACF1B70E7EAC074D143036BAD3DAB9F
                BF068B0550D177BAECB234DFE1FDA9B51D94735B3BC57CA57188973D967980AE
                23F9A7CEFB84A7F0A182E7B9118D53BF6EF1417017A7E01A421FEB099870E0F0
                9FAD57F4714BF70AC39765E23E3748FF0DEAF746E9FDEDAEC1A842C3E341DB80
                4F18EA6D8F7CE55A4C5FE9F6BD0AD0666D1D14E001052E986918E4BE63D916A7
                E68FAD009546474FBFA445F9C8A1F16EBF097430D8B0134597492422B6A2448D
                0B74C626C1202DDCF045B3679B38B7EF6C3D4B1CDDF8B85F644070DDB007A16D
                83001502C75596AE30478101B6119FBB78CE348832E56424FFD4031950746C36
                B7EAE9F3C3C78B20CCE785B3E0E0ECB236B7B35CB771A7F78D3B0F2054C0A18F
                0A98537427AC5832575D5509C15E25F57939F8E64FADE7AFDC2CBA7C03871D3A
                038D2D42266527046AF35F16F4FF3A0D0254369BED11189F77EA829424FA6630
                0009834E2212B063397F4AEE503C0CECA81A8F28F8B74A0AB23929E821FB9345
                C54EFEB1DDC5E268343ABA6A87BE4A0AF9A216100E36AF0DBCB72F81C65E387B
                6A52A807093CE77FA17E5D19F8C7D5CFB73905D6377EE0CD24F4303138B27F49
                9120BCDD11E8F332BA105E28C473E70A6CD1115D240A4FCE29F8554C4C4418E3
                8ABDCAAB9F42D0C7F3086FB189DA4983D0300D3BE3F73D10C50D0E54B418FC95
                1BA5BBC3F7BCAD7B0F71249120FC979FAFA16F486330E94CA692829CB3F506B3
                65E8ABB9F86F76894AC9CA3B79013D6644240A0B0B0D500B206732D10349CA8A
                F27ECED68BE7197D710AEAFF6D1A24A8D8CE1598AF19B985A5F71F36367D042A
                7F962A6FBCC944A2D21079437DAD8D663FA32F9710F46BB0D7EC141597E49FBA
                F8A8FA05184EFE21D45EE5406BC0882A2AC81AE84CD8BCE667EC61C8F7421437
                78507B22D1DA77F56FDFD583B7F997B60C72C1300F535346DFB281FDD9E7B146
                98B82F5ED7A207343B697D696034C095961457535552E6ADD17C1FBB58F8E9FF
                003D3A71485F0DDF330000000049454E44AE426082}
              mmHeight = 13229
              mmLeft = 148432
              mmTop = 2646
              mmWidth = 51329
              BandType = 1
            end
            object plbl3: TppLabel
              UserName = 'plblNumeroConvenio'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero de Convenio: '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 3969
              mmTop = 4763
              mmWidth = 47096
              BandType = 1
            end
            object ppLabel107: TppLabel
              UserName = 'plblTituloDatosCliente1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Datos Cliente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 2910
              mmTop = 28575
              mmWidth = 23548
              BandType = 1
            end
            object ppShape79: TppShape
              UserName = 'Shape79'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 33073
              mmLeft = 2910
              mmTop = 33602
              mmWidth = 198173
              BandType = 1
            end
            object lblNombreCompleto: TppLabel
              UserName = 'lblNombreCompleto'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre Completo:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 34396
              mmWidth = 29369
              BandType = 1
            end
            object ppLabel113: TppLabel
              UserName = 'plblDireccion1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Direcci'#243'n:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 47890
              mmWidth = 15346
              BandType = 1
            end
            object ppLabel114: TppLabel
              UserName = 'plblTelefonoPrincipal1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tel'#233'fono Principal:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 52388
              mmWidth = 28840
              BandType = 1
            end
            object ppLabel115: TppLabel
              UserName = 'plblEmailCliente1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Email:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 56886
              mmWidth = 9525
              BandType = 1
            end
            object plblOficina: TppLabel
              UserName = 'plblOficina'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Oficina:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 3969
              mmTop = 61383
              mmWidth = 11906
              BandType = 1
            end
            object ppLabel160: TppLabel
              UserName = 'plblModelo2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3440
              mmLeft = 85990
              mmTop = 70115
              mmWidth = 10319
              BandType = 1
            end
            object ppLabel161: TppLabel
              UserName = 'plblNumTAG'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#176' de TAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3440
              mmLeft = 105569
              mmTop = 70115
              mmWidth = 17198
              BandType = 1
            end
            object ppLabel162: TppLabel
              UserName = 'plblCondicion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Condici'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 124619
              mmTop = 70115
              mmWidth = 15081
              BandType = 1
            end
            object ppLabel163: TppLabel
              UserName = 'plblFechaDesactivacion1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Motivo devoluci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 6879
              mmLeft = 140759
              mmTop = 68792
              mmWidth = 16933
              BandType = 1
            end
            object ppLabel164: TppLabel
              UserName = 'plblPatente2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 3440
              mmLeft = 11113
              mmTop = 70115
              mmWidth = 11642
              BandType = 1
            end
            object ppLabel165: TppLabel
              UserName = 'Label165'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Estado devoluci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 6879
              mmLeft = 158750
              mmTop = 68792
              mmWidth = 16669
              BandType = 1
            end
            object ppLabel166: TppLabel
              UserName = 'Label166'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Modalidad Contrato'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 6879
              mmLeft = 179123
              mmTop = 68792
              mmWidth = 19050
              BandType = 1
            end
            object plblNumero: TppLabel
              UserName = 'plblNumero'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#176
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 3440
              mmLeft = 4233
              mmTop = 70115
              mmWidth = 3704
              BandType = 1
            end
            object BajaNombreCompleto: TppLabel
              UserName = 'BajaNombreCompleto'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 34660
              mmTop = 34396
              mmWidth = 165100
              BandType = 1
            end
            object BajaDireccion: TppLabel
              UserName = 'BajaDireccion'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 20638
              mmTop = 47890
              mmWidth = 179123
              BandType = 1
            end
            object BajaTelefonoPrincipal: TppLabel
              UserName = 'BajaTelefonoPrincipal'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 33602
              mmTop = 52388
              mmWidth = 166159
              BandType = 1
            end
            object BajaEmail: TppLabel
              UserName = 'BajaEmail'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 14552
              mmTop = 56886
              mmWidth = 185209
              BandType = 1
            end
            object BajaOficina: TppLabel
              UserName = 'BajaOficina'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 16669
              mmTop = 61383
              mmWidth = 183092
              BandType = 1
            end
            object BajaNumeroConvenio: TppLabel
              UserName = 'BajaNumeroConvenio'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = [bpBottom]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 51329
              mmTop = 4763
              mmWidth = 46302
              BandType = 1
            end
            object ppLabel158: TppLabel
              UserName = 'plblClaseVehiculo2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Clase '#13#10'veh'#237'culo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 6879
              mmLeft = 34396
              mmTop = 68792
              mmWidth = 13494
              BandType = 1
            end
            object ppLabel159: TppLabel
              UserName = 'plblMarca2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3440
              mmLeft = 63500
              mmTop = 70115
              mmWidth = 8996
              BandType = 1
            end
            object BajaLblRUT: TppLabel
              UserName = 'BajaLblRUT'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RUT:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 38894
              mmWidth = 7747
              BandType = 1
            end
            object BajaRUT: TppLabel
              UserName = 'BajaRUT'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 12435
              mmTop = 38894
              mmWidth = 187061
              BandType = 1
            end
            object BajaLblGiro: TppLabel
              UserName = 'BajaLblGiro'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Giro:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4498
              mmTop = 43392
              mmWidth = 7535
              BandType = 1
            end
            object BajaGiro: TppLabel
              UserName = 'BajaGiro'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 12435
              mmTop = 43392
              mmWidth = 187325
              BandType = 1
            end
          end
          object pdtlbnd6: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 6086
            mmPrintPosition = 0
            object ppShape95: TppShape
              UserName = 'Shape802'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 2910
              mmTop = 0
              mmWidth = 7673
              BandType = 4
            end
            object ppShape96: TppShape
              UserName = 'Shape96'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 10054
              mmTop = 0
              mmWidth = 14552
              BandType = 4
            end
            object ppShape98: TppShape
              UserName = 'Shape98'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 23813
              mmTop = 0
              mmWidth = 34131
              BandType = 4
            end
            object ppShape97: TppShape
              UserName = 'Shape97'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 57415
              mmTop = 0
              mmWidth = 23019
              BandType = 4
            end
            object ppShape99: TppShape
              UserName = 'Shape99'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 78846
              mmTop = 0
              mmWidth = 25665
              BandType = 4
            end
            object ppShape100: TppShape
              UserName = 'Shape901'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 103717
              mmTop = 0
              mmWidth = 20902
              BandType = 4
            end
            object ppShape101: TppShape
              UserName = 'Shape101'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 124090
              mmTop = 0
              mmWidth = 16669
              BandType = 4
            end
            object ppShape102: TppShape
              UserName = 'Shape102'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 139965
              mmTop = 0
              mmWidth = 18785
              BandType = 4
            end
            object ppShape103: TppShape
              UserName = 'Shape103'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 157957
              mmTop = 0
              mmWidth = 18785
              BandType = 4
            end
            object ppShape104: TppShape
              UserName = 'Shape104'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 175948
              mmTop = 0
              mmWidth = 25135
              BandType = 4
            end
            object DBCalcNumero: TppDBCalc
              UserName = 'DBCalcNumero'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              DBCalcType = dcCount
              mmHeight = 3440
              mmLeft = 3969
              mmTop = 1588
              mmWidth = 4763
              BandType = 4
            end
            object BajaPatente: TppDBText
              UserName = 'BajaPatente'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 11113
              mmTop = 1588
              mmWidth = 11642
              BandType = 4
            end
            object BajaClaseVehiculo: TppDBText
              UserName = 'BajaClaseVehiculo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'ClaseVehiculo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 24606
              mmTop = 1588
              mmWidth = 32544
              BandType = 4
            end
            object BajaMarca: TppDBText
              UserName = 'BajaMarca'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 58473
              mmTop = 1588
              mmWidth = 19844
              BandType = 4
            end
            object BajaModelo: TppDBText
              UserName = 'BajaModelo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 79904
              mmTop = 1588
              mmWidth = 23019
              BandType = 4
            end
            object BajaNumeroTAG: TppDBText
              UserName = 'BajaNumeroTAG'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'NumeroTAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 104511
              mmTop = 1588
              mmWidth = 19315
              BandType = 4
            end
            object BajaCondicion: TppDBText
              UserName = 'BajaCondicion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Condicion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 125677
              mmTop = 1588
              mmWidth = 13494
              BandType = 4
            end
            object BajaMotivoDevolucion: TppDBText
              UserName = 'BajaMotivoDevolucion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'MotivoDevolucion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 141552
              mmTop = 1588
              mmWidth = 15081
              BandType = 4
            end
            object BajaEstadoDevolucion: TppDBText
              UserName = 'BajaEstadoDevolucion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'EstadoDevolucion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 159544
              mmTop = 1588
              mmWidth = 15081
              BandType = 4
            end
            object BajaModalidadContrato: TppDBText
              UserName = 'BajaModalidadContrato'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'ModalidadContrato'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 177271
              mmTop = 1588
              mmWidth = 22754
              BandType = 4
            end
          end
          object psmrybnd3: TppSummaryBand
            AlignToBottom = False
            mmBottomOffset = 0
            mmHeight = 105834
            mmPrintPosition = 0
            object ppShape105: TppShape
              UserName = 'Shape105'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 69056
              mmLeft = 2646
              mmTop = 4498
              mmWidth = 198438
              BandType = 7
            end
            object ppShape106: TppShape
              UserName = 'Shape106'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 4763
              mmLeft = 3704
              mmTop = 5556
              mmWidth = 76994
              BandType = 7
            end
            object ppLabel167: TppLabel
              UserName = 'Label167'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Condici'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 32279
              mmTop = 5556
              mmWidth = 17314
              BandType = 7
            end
            object ppShape107: TppShape
              UserName = 'Shape107'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 3704
              mmTop = 10054
              mmWidth = 76994
              BandType = 7
            end
            object ppLabel168: TppLabel
              UserName = 'Label168'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = '1 Devoluci'#243'n    2 Sin Devoluci'#243'n F'#237'sica'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 5292
              mmTop = 10583
              mmWidth = 59563
              BandType = 7
            end
            object ppShape110: TppShape
              UserName = 'Shape110'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 33073
              mmLeft = 3704
              mmTop = 22754
              mmWidth = 99484
              BandType = 7
            end
            object ppShape111: TppShape
              UserName = 'Shape1101'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 33073
              mmLeft = 102659
              mmTop = 22754
              mmWidth = 97102
              BandType = 7
            end
            object ppLabel169: TppLabel
              UserName = 'Label169'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 
                'A. Tansferencia de Veh'#237'culo.'#13#10'B. Perdida, robo, hurto o no devol' +
                'uci'#243'n de TAG.'#13#10'C. Da'#241'o, Falla o Destrucci'#243'n del TAG abrituible a' +
                'l Cliente'#13#10'D. Renuncia voluntaria al servicio.'#13#10'E. Termino de Co' +
                'ntrato a requerimiento de Ruta del Maipo.'#13#10'F. Recambio de TAG.'#13#10 +
                'G. Baja Forzada.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              WordWrap = True
              mmHeight = 29633
              mmLeft = 7144
              mmTop = 24077
              mmWidth = 92329
              BandType = 7
            end
            object ppShape108: TppShape
              UserName = 'Shape108'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 3704
              mmTop = 18521
              mmWidth = 99484
              BandType = 7
            end
            object ppLabel170: TppLabel
              UserName = 'Label170'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 
                '1. TAG en buen estado'#13#10'2. Da'#241'o/perdida Soporte                  ' +
                '      '#13#10'3. Da'#241'o/No Devoluci'#243'n de TAG                '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              WordWrap = True
              mmHeight = 12700
              mmLeft = 106892
              mmTop = 24077
              mmWidth = 65828
              BandType = 7
            end
            object ppShape109: TppShape
              UserName = 'Shape109'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5027
              mmLeft = 102659
              mmTop = 18521
              mmWidth = 97102
              BandType = 7
            end
            object ppLabel171: TppLabel
              UserName = 'Label171'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'UF 0,11 + IVA'#13#10'UF 1,00 + IVA'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              WordWrap = True
              mmHeight = 8467
              mmLeft = 172773
              mmTop = 28310
              mmWidth = 22267
              BandType = 7
            end
            object ppShape112: TppShape
              UserName = 'Shape112'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 15610
              mmLeft = 3704
              mmTop = 56356
              mmWidth = 196057
              BandType = 7
            end
            object ppLabel173: TppLabel
              UserName = 'Label173'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Motivo Devoluci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 37042
              mmTop = 18521
              mmWidth = 32544
              BandType = 7
            end
            object ppLabel174: TppLabel
              UserName = 'Label174'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Estado Devoluci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 135467
              mmTop = 18521
              mmWidth = 32544
              BandType = 7
            end
            object ppLabel175: TppLabel
              UserName = 'plblUsuarioAtendidoPor1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Usuario atendido por:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 5027
              mmTop = 101600
              mmWidth = 33602
              BandType = 7
            end
            object ppLabel176: TppLabel
              UserName = 'plblFirmaConcesionaria1'
              HyperlinkColor = clBlue
              Border.BorderPositions = [bpTop]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Caption = 'Ruta del Maipo'#13#10'Sociedad Concesionaria S.A.'#13#10
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 11113
              mmLeft = 23019
              mmTop = 88636
              mmWidth = 61648
              BandType = 7
            end
            object ppLabel178: TppLabel
              UserName = 'plblFirmaCliente1'
              HyperlinkColor = clBlue
              Border.BorderPositions = [bpTop]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Caption = '            Firma del Cliente                '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 122502
              mmTop = 88636
              mmWidth = 56886
              BandType = 7
            end
            object BajaUsuario: TppLabel
              UserName = 'BajaUsuario'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 39952
              mmTop = 101600
              mmWidth = 42598
              BandType = 7
            end
            object prchtxt1: TppRichText
              UserName = 'prchtxt1'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'prchtxt1'
              RichText = 
                '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil Taho' +
                'ma;}{\f1\fnil\fcharset0 Tahoma;}}'#13#10'{\colortbl ;\red0\green0\blue' +
                '0;}'#13#10'\viewkind4\uc1\pard\qj\cf1\b\f0\fs20 Observaci\f1\'#39'f3n: En ' +
                'caso de no haber devuelto su dispositivo TAG de Ruta del Maipo, ' +
                'solicitamos su entrega a la brevedad. Recordamos a  Usted que en' +
                ' caso de no devoluci\'#39'f3n del dispositivo TAG, la Sociedad Conce' +
                'sionaria est\'#39'e1 facultada para realizar un Cobro por Reposici\'#39 +
                'f3n de TAG, equivalente a UF 1 m\'#39'e1s IVA.\f0\par'#13#10'}'#13#10#0
              Transparent = True
              mmHeight = 14023
              mmLeft = 5292
              mmTop = 57415
              mmWidth = 193146
              BandType = 7
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
            end
          end
        end
      end
      object ContratoBajaAnexo: TppSubReport
        UserName = 'ContratoBajaAnexo'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 60325
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt6: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object ptlbnd4: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 38629
            mmPrintPosition = 0
            object ppLabel198: TppLabel
              UserName = 'Label198'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'TERMINO DE CONTRATO DE ARRENDAMIENTO DE TAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 5027
              mmLeft = 38629
              mmTop = 21960
              mmWidth = 120386
              BandType = 1
            end
            object ppImage1: TppImage
              UserName = 'imgLogo3'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                009C0000002F0802000000998434C1000000017352474200AECE1CE900000004
                67414D410000B18F0BFC6105000000097048597300000EC300000EC301C76FA8
                6400001E3C4944415478DAED9C075C1447DBC06F0FAED0BB74057B41A404C528
                F60A268A051B364404A5496FD27BEF0A0A82220611C5466CD8308A0D44B060EC
                200888D2EEB87EDFB37B709C08C42498F77D3F33FE7239F666A7FD9F79CAECCC
                225C2E87C56452A83404417068E20A0909898989E17889CBC1FE8730188C4FAD
                6D2422515A5A02C7859C5CDE75F8D2D2DA4E673064A5A50804027CA1D1E8DD45
                7D96B85CAEA80889402062F721EDEDED1D944E2121BCBC8C345E58087EC6F597
                10844E837F0C7EB124128144227FDE42AC310897D24165B139820DC0EA2543DB
                E0775A27ED536BBB84B8A8B8B828AEFF0AB1A2701D1D14369B032D141717FFBC
                225E163C0EC7696BEB8086138904111191CF33A02534377F6230592422415656
                1AAB8E5F25C2E1B03FB6B4C11518373C5E887747473B85831682409B09046151
                51D12FFAC8AB172EB06BEA1A3A3AA85C1C574C54444549814824F173224C06C3
                272CF97EE5133289C4E1A09700AA8C94C4D41FB4D72D5F2C292989350589DD9B
                9D7BFC9C869A72A4AFE3708DA15DD520F8A7CF5EB805C57F686ED9BCFA27AB4D
                66BEE12937EF559049448C05C21F56F8042AB37ED4F771DA86C3A17779852635
                B7B4C24FC673A67B3858A2B5F4C915C1BF7AFDD63D38B18342C5E3F190054A25
                1208F272D2063A137E5E384B71883C7A235A15527CBD343A351BDA8F558AE3D5
                4FA176AE30996B6BB98EC562ED0E4BBAFCDBFD31C387C605BBCAC9CAF4E6D42D
                43F05F6EC19903BF9C061E30263A13467B395A8A8B8BF5B410C137363507C5A6
                3D7F5503F945C9240F7B0B03DD893D0522F8C2B39712337E81DBA13D0E966B7F
                5E3C47F0D7A28BD72353B2019FB3F5869F16CE866BC74E9DDF7FB8505898D778
                2E7C91919634D4D75EB36CA1B494147FC0A117F9A72E9CB970ED754D7D5B0705
                AE898B89AA282ACC9B396593D94FA81C7039484747C7CAAD2E0F1FFF2E224286
                1640712C161B3EA1290B661A4607384B494AC09D8EDE11D9F9A787A9A9642506
                EA688DE5D771FB5EC56607DF4FAD1D96EB97067BDAAFB172BD547247944C8651
                26080BE311044A64B258808C4AA399CC9D9E95148420F8E48CDC90F84C111112
                34514D79484146B4B292623F438CBF535E6966E90A44E12F369B8D7D72586CB6
                0899347AF8303F67AB1FA7E8F1246FFFA163BE517B6188D91CB42BBC99D1DE41
                B55C6F1AE9E7C460D0D7DB785DBF5DAEA1A674FC40ACAAB2527F35C28DAE01B1
                078F9E95101361B2D930E9E2029C4C97CC17A4929153E01BB907E613348CC960
                86EF7658BF7249F7B0206C16DBD2C9FFFCD55B226432B59366326F7A5AF46E54
                DA786201B7E71EF78D4885F1F175DEBE6DC34AB8E61F95BAEFD0713299CCE6B0
                211BF411868E202C34638A6E6C90ABBC9C2CE4A1523B03A2F71E3B7309D42181
                4890101385F23A285D1AC578EEF4F0DD8E9212E22854F31DDE772B1E698D1D19
                E2614B26139FFCFE2AFD60C1AB9A3A8011B5DB61D5D245509C5B40ECA1636787
                A929EF8BF19D387E341FEABDF2CA6DCE41A0D360A6FABBED78F8E829DC083309
                6A4A39905F5BD7A03954C56AC30A90260683396298AAD6F831542AD5D229E0D6
                BD87B2529274260B147BA09BCD9AE5C6FD0DF1FD8A471B767AC3ED26F38D36AE
                5A02385FBE795774E9C6EDB24A3A83A9A632645FB4EFF8B123216FD691C2E0F8
                7DD04FBBAD6B664D33A0D3E93C09505755525755A6D168506F090A55F997B4F0
                01C408A07A87261D2E28521A220FD2D1F8E1E392F94689A19E783C8269003C74
                61BB4BD0B5D2B2E14355A1EF30D6C11E3B579B2EE60FCBE3A7CFB738FA7D6A69
                17171301A8D292E2D9898163468FE0673878F45440741A40F576B4DCBC76195C
                0B8DDFB7FFF009B014EEB69B7FD0D1FAFDE59B8CDC138FAA5F824573DCB6CE79
                C766C893B4EF70F4DE43C06FA486BA95F90A9D8963C17ADE29AB4A3F54F0AEBE
                0978395B9BDB5B997741BD5D5E65A8AF75FC403CAF5FBF9CF8D5233811641050
                F9BA587F0DD44D664B02DC77F247864EA7ADB00005F06CB29E566E6A189144E2
                FF5406906C77436B80D0C5EBA5D5CFDF982E9E1D17EC262C2CDCC7287743EDA4
                D16D36AF72B3B5E05DA650A97E11A985E7AED2E874F315C661DE0E081ECF83CA
                60B0F6C7FA2E983DAD57497F16EAC1FC33E3470F1F3B42A3A8F886829C4C7652
                D0A8111AE82D08FE6ED9C34DF6BE20BB4B16CCB874BDB4A9B925D4D3B60B2AA6
                BD4167F847A78D1A3E14B4DDE1E3E7DA299400176B0C1E9727167D42DD97731C
                0CE4A1E4205DED0970E5CA8DDB0E3E51E0CACC9CAA9F931A5657FF7E9D8DD7AB
                B7752A4AF27B22BC74268EE737B9F8DA2D27FF58706EC68FD6CC4B8FEC813A45
                4FEB78660C8219EDD3E7AED879478051DCB1D9CCD576CB9F81DAD5E896D656D0
                75554F9FEB4F1A7F203E404A4A92DFE1E894CCB8F4DC09A3871F4808084BCC3C
                7DA1445951FE706AC808CD610343DD66BEDCCB711B66A5D1917D5B5BB766BB47
                7D4393E650D59C94101565453ED48C38BFF9B37EEC2904CBFF67A1661F3D3D69
                FC682BF3E581B1FB9A9A3F05BA0115535E96E098BDC9078ECE9966B0EAE7F981
                B1E96DED1401A878069D6EE1E877F1FAED0DAB4CECB7AE05FC2FDFD41A19EA65
                25040A83BF86E51900EABE98DD530D74E1CACB576F3739F8BE7AF3CE688AEE91
                F4C833E7AFDAF944802D58BB6C6188B74357BF780DE672D7EFF0FCEDCE03D088
                B929A13D507F98340E6A251089BFBF781D9A9071F7C123511191CC38BFA99375
                070F2A1E5CCA0DB65EA5F7ABD69A2E8A0D74CDC93FED139E02C6C6DF65FB06B3
                9FFB7097FA870A39B7390514DFB8033EF9E13D217ADA1330A8FB41164D8D674F
                1C379AC36633992C156585C573A72188D05F803A7ED4F09470CFA0B874200408
                61B20A0B139A9B3FAEB5F67CF4EC8587EDE6297ADA16BBFCA16D82501F563DDD
                60E7D3DADE11EE6DBFC6D478D7EEC8A3A72E2AC9CB1E480CD49E30E60FA1F267
                2A3853AE81716DEDD49F1618A546FA2482EEDD73904C2406B8DAAC5D610CBE77
                7708824E1530C987F2CF82CF9111EBD70515F4A48484B8AC34F8BAB8D6B67610
                3D395969CB75CB2CCD57F09CC8C1827AEDE6DD1DEEA1600BE3039D972C9C5DFD
                FB4BF0B3DEBD6F9A3DCD00B2E1515782F3B550B1A13F72E21CD898030981D3A7
                E8F1A08203D5D9490303030DA17476EA6B8F030D0461C95F803A4A63E8E99C84
                43F96742133261400EC4FBC1709F387BC925200EC2B3A3FBA240E36D730EEC05
                357EEFC198BD3960C87353433486A9E79F3CE71D9602B1CDAEEDEB1DACCC0780
                CAB7A920916FDFD5A51D2C00FF067E8AF1DF656A32DF3F6A4FD62F274182E383
                5C16CF9B21E0B5A150E3D30E2567E681102785B87541AD7CF2BBA8A8088C0EC4
                8E401B5CF91D5BCCEC2CD7F39417DC330850B17242E3D29332F3268E1D519815
                278A45C3F65EE1C7CF1643A49519EFA7356ECC9F82EA11140F93008CF1C1C420
                4383493CA8100C802D5452407D1C1A833E6EA4A6CB8E8D42C284BF0015FC9193
                D971750D4DEB6DBC1B9B3F3A6D3777B2D964EF199677EAC29CE99373F7869594
                96818CF640C5A1D1ED56477FF0A1404982A30057DADADA965BB8001ED0A2FB63
                FDD0D00887EB136AE6919310580205C08445FF4C10D075A68BFC5C6D20CEF68F
                4CCDCA3B0D31715CA0B3C9FC99BDA0C6A666A766E73398CCBD11DE7CEFF7B1D6
                D811E1DE763069E2D272C0BD8448232ED0455F478B07C3D53F26A7A008A042B3
                B4C68DE243BD5F5165E914086EDE46B325817F00151ADABAC6CAA3FAC5EBB123
                358CE74D171602652674FD56D9DDF247E0EF78D859586F36C3D42FF76BA0B2D9
                2CB85E7ABF524A523C373574DC98913C9BCA64B241672E5930537085043EFF1A
                D4FC7D91F2F2B2EBAC3DAE9796194DD675B23677F28B7D5DF3CED769BBD5A655
                574A6E83F32108B5F4EE83AD4E814C2673DA141D43BD89BCD8AAF0D7CB6F6AEB
                C1B13A10EF6FA0A73D30547151112027264AD65057996B3479D9E239A825C6E1
                62F76427EC3B02F36DB793D506B39F7AA95FD780988233C5E0DB1E4A09FECC51
                3A91857ABF278B8ADD831360CAFEBC70665298879090305CF40E4DC8CA3B2327
                2B951CE23EE347033ED4925BF760A6824C6DDFB8DCC3DE7260A897AEDDB4F78E
                44C599DAD941A1A28B125C1CC4C74005F4C6645D2D70707A2FCDF409154B1555
                4F2C1CFC3F7C6A01050BEA17AAE0434D8DF032993F630047292F3D424971488F
                F470B9FD4105FC90333DFB68486226C460208E0F1E3D939410CB4A080031BA5C
                72DBFE73A89149197BB28F91C924B062709D37E412E2A212E26210DBD86C5CE9
                6A67D11F549E4D85A905734958082F272385C7069FD7FEBCC25FDD0213400FAD
                34991BE1E724E828B5B777ACD9EE5EF9E4B9BAAA2274AD07EA64DD0947D32321
                F6A07576AEB7F12C7FF44C424C2C2B3140571B759DD3B28F46246741DF572F9D
                1FEA658FC7566D40517B8726E69DBC00BE094CEBA5FC45937EA07A06C51F3EFE
                ABA282ECECE906A26474AD03C156276EDFAF7C5D53472611C11341ABEB07AACD
                A6556E765D21CDAB37353E61C9A565554C16D3D56693DD36D452F0BD5FB0110B
                E7F41BD268AA291FCB8C969793FB8C7A3F508FEC0D83395DF5B87AB3835F3BA5
                1308818A9B33DD203DC60FA6452FA8144AC7EA6DEE95D52F46690E9D663089B7
                9806D93AA8D4925BE54D1F3F698D19014A1B0CFC005021A2C514E4E70D43F04F
                AA9F8323DDF4B165889C4C4290ABA1810EBFF50969392959F9341A1D5CAAE430
                AF3EA042AE8C9C82A0B87D0882DF686612E086469FCF5FBED968E753DFD00C0E
                82F1DCE940052E42978A8A6F428BB5C60CCF4E0A5690971B006AFDFBC68D76BB
                1F3F7B397FC694ACC440DE1A262FED3990179E740064D01A04198DA03802ABA4
                5D50592C3684BC738DA6804FFBB6AEA1E4D6FD5735757071DAE449C9619EBC05
                9703B9852109FB61DC4D4DE6E8688D653298388CC1C4B1236108E8349A8553C0
                ADBB1530037E5E384B52421C8AA2311810B718CF37EA599BED0B2A93C1B07209
                BE7AF32E0C3A8DCE08F6D8016E2D6F0404A1165FBFE5E01DD54EA53AA13ED106
                7E07A1A2ED2E41E7AEDE9492104F08769B3BC3F08F439ADEBE0502ADF38B4CC9
                C054B48AA2C20A93B963476BD2E90CB0EB10498366559097498FF2D19B34015D
                585FBEC5B9B4AC12D4EFD99C241E548890566FF778F5B656435D352F2D7CC4F0
                6170F168E1B9D0C40C08B4C14AA06B765C1C9BC301121026867AD91A19FED063
                0E11FCA79616D3CD4EE07FFDA003B212212525957FEABCAD6738EA2B79D96E5D
                BF5CB0C10F2A9F98EFF4067FC460D20450773232D282127AB7BC728585337C83
                EA180C066FF0D1557219A91953F53CED2DD45494BB97090BBCC29345C9243007
                E0FDF2969EA1B7E0B3642606B2984C886B61A6826E04A1466D1D826B6BEF80D1
                39BC270C4163240E1FAAB36FF4FE2385A335878243A7AAA20497F7A3EB82A9F0
                B39AB24241668CBAAA0A5CBC70E5372B9720801A1BE0BC7EE51217FF18000323
                9EBB37545760710013B8E33EE1A9543022EB9745FA3967E69EF00C4904A8109F
                806D860CBE11297BB28E62214DB0D1D41FFA0CD95B3EB57884245EB8560A8205
                4A008F4760C841E1090B0969A82BC350982C980514103A9D1E959C5556F9444F
                7B9C87BD056F351C0AD8939577E5C65D7131117BCBB5D8E2053A6AA5772B4EFC
                7AF9C5EB9AB60E0A3448465A0AF489D9B205A3866BF4D2609D9D9D5129D9954F
                7FD79B380E9C0B12897CE2ECC5DC82737272D29EF65B86A9AB093A6F2C262B32
                39ABBCEAA9B2A282DB8E4D6A6ACA8250618AC7A61D6A6BA3805BC8CB0FD8D454
                8618EA6B1BEA4FC42158148402C4DF7B50B53FE7044041703D4F69A0FF33A7EA
                6F5AB314BA9079F8C46F772B4824025F13C0281B4D9EB4D57C654F888CF91D39
                F9670ACF5D012DEA61BF450A7DAA81AB7D571F9992F5BEB1199C17AB0D2B113C
                9A0D5462CCDE43204376166B26EB6B472665C2F4D09930DAC3612B914014EC05
                DC1E9E98D9F0E1A381EE04375B8B5B77CBF7661F83FA206E447D141CEEF8998B
                F9A72FC254065F7D64AFF1141858D03780E0DAADB2FA86262A9506BC64652427
                8E1B05A2892E7861BD00D4A0EBB8E07C82FCA28EA240C768B44E213C9E402408
                7A5938CC89A550C1BA20E0E0743FA4FB72D100E92A9644C6DC4F3481AE00BF0E
                35FE5F3EC6E27240DE41E2B0EA7AAF3F60C6850D06180A411FFE08A8EE2FCD21
                97D3FBD11BBA4CC6EF17978D13408E760D8FEFED72635241A7D189442282E78F
                09DA486CA044042400CF62A2CA83802164B339E0F79245C8D848F6EE2387CDA2
                33182402110FD289E0994C0614432012F91A824EA7A14F418484FB262A301AD0
                80E6E64F20AF60B0A5A5C4454444052920D0929E31FA6C8090EECE7F0EACE73A
                AE8F5FFBA89EEFA475DFD86F2081FBE35FBFBED23EF3F76E7CFFA5F5D9DAEE01
                FDC25BC6F1DDFEAFEA45F78A69DF6D1B80E8D70D0806F56F264CF509FCCD19F0
                F9F3BFE99BA7BF0D15416A5AA9A5756DBCBF203E99A62AAD262532D04E867FD3
                374E7F1F2ABEBCBE25ABB20EFB8E6371B8DBB4D57494A5BE4A87FC9BBE4D1A04
                A80F1B5A8F3C7E8F7D47A16E9CA0325151F25FA8FFC13408501FBC6FC9A9C2A0
                E2712C3667CB44B5494AFF42FD4FA641805ADDDC51F4B219FB8E6373B84B472A
                8C9215EB2B76EED3F3FCC6E9BB94AD41F07E996C76279DC5FB0E9EB528519840
                10EEED0077FBFD140A1522B96F4E97CB259208A2A2BD36BA7E2F691066EAED7B
                15E14999E84206826FEFA0B8DA6D5E306B9AE0620A7CBC6F6C3C7FE556496959
                7DC3076C47E037872A242CA4AAACB068F6B4C5738D4444C8DF15D741807AE1CA
                4D1BF7106C2912DFD2DA961CEEB966D9628175545C6151717266DE8BD7B5783C
                823EFF45FE1125CCE5B2B105B339D327EF76B61AAAA6F2FD701D04A8C5D74B1D
                7C22BBA0B6B5C705BAACFC6901368228BFFD3905D1A907994C968828F91FB7A8
                E862777B3B65BAA16E729887AC8CCC77C2F59B41C51E005CB971DBCE2B028812
                89E8B37B168BDD893E1EF9E6230B0A418444424D3BB61ED2D141B1B558ED8A6E
                2FE57E0FAB22DF0E2A8ECEA05BEEF2BF711BDDB7087F32184C29297183491364
                A425B9DF72641104A1503AEF553C7EDFD4CC3B0042A333D494871C4C0A52FF3E
                94F03784FAB0EAE956E740D07E3063E80CA686BA72A0DB8E29FADAC83F6253AB
                1E3FF30A4BAE7EFE1A9404B48D46A3A7847B2E9C33FD5FA85F5340BF50CF5FFE
                CDD137125C62708EA89D34876DEBD00D9268FA0714202A3759BF148626641009
                04102370CB833C766E58F5D30027B1B0A671FA7EEA3270EAB9EBF33F05BC45FE
                6F3D0FF270BD9E5F0D9A69F88650AF8241F58E84004648488842A52D5B342B36
                C8F5ABCBE50E7CB8F16B82A2FC53177CC29309C2C200B58342F577B5DEB47A69
                57E15D95F04FB121EF1B3FB4B6B68F19A5F9B6A6AEE9638BFE2474DF4253D347
                90063299585E554D2408C33FF8534579C8284DF5078F9EC177F012C68C183676
                F48807954F6AEB1B44C8243DEDF1605FEE573C86EBBC8372743AA3FAC51B5054
                35EFDE0FD750873CBCD3822C365B6B1CBAD996C1643E7BFE6684861A1A7AFD97
                43ADADABDFB0D3A7B6BE110C1BB60306315D3C6BC982590A7232BC87E65F6044
                41494A88CBCA741FD9ECFFA9248341FFF8A9153B56DB97447071CA8A0A672F95
                0842458F61ADFAE92395CEBB033E65C904213CC22BF3E8C97377CAAAA2035CA2
                530EECCB399E93126AA0371182B1DFEE3CD8BA6ED9D98B254F5FBC7E5BFBDEC8
                5057576BAC949444504CDABC1986E0F4C195495A632C1CFC476AAA7339DC4F6D
                6DDE8E96E0F04F33D059B574210E3B125370FA9287BD8557685280AB8DA6863A
                8D46DB62EF4BA1527F498F1215156968FCE01614EFEFB25D7398FAA058876FEB
                FD06C5ECDD77F8047082A163B339605925C5C5789E703F890BDA72F488610B67
                FDB868CE740909B12F9F51D7BCAB3F73E1FAB59BF7DED635801AF8D2428300B1
                582C5FE76D440271975F340F2A85420DF3DCA16934FBC4E37764213C7A4A5008
                6FA5A33A448CCC53B3C74E9DBFF7E071B8EFAEE894ACDFEE948FD0508FF2732A
                2ABE71F15A693CB627FBE2D59BE7AFDC8A0E7086EFD76FDD2D38733921C49D57
                6343D307AF90C4187F67696929334B679BCD66303BCF5C2C490EF7825FC313F6
                0F919701C0B61EE101AED61AC3D44AEF96FF5278BEA383BAC16CC9CC69931B9B
                3E788624ED76DAA63154EDBF1B2A6657DE37345ABB859455564B8A8BF236A300
                DA815D5F50BB2C261B3EA7FEA0ED61B765E2F8318286E7D4F9AB09E9879FBFAA
                01E70BB47A9F2A18A03259AC306F5B29090947DF2841A8AA3FCECAAFAAE9822A
                8CB7D71FAA24DE05B5E0F405801AB6DB313C21435141B6E251F532E339501408
                10CF6A9CBB7CE3D2D5D2E84017F87EEBDE83C8A4ACD54B1790C9A4D9D30CA047
                F6DE1126F38C080442F1F55B6E761662A222302691BB1D41153B78477A396E95
                9596822B6002801C60D61E3792C16297573E0970B76DFAD00C507D7659FE2F40
                C57E7DF6E2B54F58F2EDF22AA2B030F4B96BF3D81F2528AD9D42D550534E0C71
                9BA4358E772EFCC8F1B381B1FB2034121DD0F6F0A0067BEC901013EB05557DDA
                AC638F6AF933D5564F5DF10BA8C1B1695A63472A0D91CB395634C350F75EC593
                486CE7F4AFC52597AEDD8EC1A096DE7B109290095E02405D34FB471030905D75
                15A527CF5E1ACF9D6EBD65350E3D7D1403AEBEA6BA0A94131BE8FCE1638B8B7F
                6CB0A7ADACB4E44E8F50472B73069311999C9D11EFCF66B15D03E3FF77A06219
                5ADBDAB28E9C3A5B5C525BD740A17672B9FD7839D8048629482691F098A9034F
                447FD2B88C587F506B15954FB638FAB75328646C132B7819E83A069BF365511C
                360A35C67F97BCAC8CC3EE4841A8CA5367E63E7C2B228C07F000D579F2306509
                912FA10E1FA6B66E8589774802A804B005419E76BDA082FA3D517435AEDBEF03
                F5EB1D9A14E5BBABE9C3A78098B4980067254585A28BD72EDFB82B232501B676
                B5A931E4710F8C8FF1777A5CFD223821E3079D0950ED9DB24A179B0D46867A0E
                3E512085D8A1814178FCF0EDA1E2BA6C615373F3FD078F6BEA1AE80C469F54D1
                9D7E1CEEC327CFC1A4F1DE4F009F9D7446B897EDAAA58B40F0F34E5E9494401F
                BCC064959692986334595D59918BEBADCEB9E87E420E4CA027BFBF760F8EE73B
                4A21EE36B38C173D6A6C1542F745E38410C44049528C28CC6B61DE895F6F9755
                C606B9F986276B0E55DDB2CE14E26C332BF739D30D52237D20CBE90B57CF17DF
                4C8E40CD6449E9BDB8B4C34EDBCDA1E4A1AACA62A2643BEF88608F9DE0E9EC74
                0FD19F38CEC27C794B4BAB85A31F08715662A0B292E2FBC62667DF98502FDBF4
                83C7C68D19819DD140728E9EAA78F4CCC7C9D2D63362D9E2D92A4A0A8AF2B2E0
                81FFCDD86610A05EBA76CB7177141F6A7C90EB0AC1D723F0897DDD9319F0EF0F
                E69D8C4FCFE515D841E9042D17ECB97389B97DDDFB2670B260160E91970DF7B6
                FB71B2DEC045E5159EF38D4CED0969DC6C36A147600553CF19A3BB65552FDFD4
                AE365D54587419A6F874435DB87A20F704E80C6CCF300EAC6CD5E3E7EB5799C0
                F7B7B575E02113D1B7D1306718EA8159CDCE3BB974D16C0505F99B77CA5FBCAE
                C10E3021878E9E6A6D6FB7DDBA1E240DE2A582339740448A4BEE2C5930437188
                0294D3D0D874FC4CF132E3D927CF5D7DF7BE111AA3A73DCED464DEDFDCBC3708
                50CF5FBEE1E4178343F7CFA250C10A9A1ACF1B70E7EAC074D143036BAD3DAB9F
                BF068B0550D177BAECB234DFE1FDA9B51D94735B3BC57CA57188973D967980AE
                23F9A7CEFB84A7F0A182E7B9118D53BF6EF1417017A7E01A421FEB099870E0F0
                9FAD57F4714BF70AC39765E23E3748FF0DEAF746E9FDEDAEC1A842C3E341DB80
                4F18EA6D8F7CE55A4C5FE9F6BD0AD0666D1D14E001052E986918E4BE63D916A7
                E68FAD009546474FBFA445F9C8A1F16EBF097430D8B0134597492422B6A2448D
                0B74C626C1202DDCF045B3679B38B7EF6C3D4B1CDDF8B85F644070DDB007A16D
                83001502C75596AE30478101B6119FBB78CE348832E56424FFD4031950746C36
                B7EAE9F3C3C78B20CCE785B3E0E0ECB236B7B35CB771A7F78D3B0F2054C0A18F
                0A98537427AC5832575D5509C15E25F57939F8E64FADE7AFDC2CBA7C03871D3A
                038D2D42266527046AF35F16F4FF3A0D0254369BED11189F77EA829424FA6630
                0009834E2212B063397F4AEE503C0CECA81A8F28F8B74A0AB23929E821FB9345
                C54EFEB1DDC5E268343ABA6A87BE4A0AF9A216100E36AF0DBCB72F81C65E387B
                6A52A807093CE77FA17E5D19F8C7D5CFB73905D6377EE0CD24F4303138B27F49
                9120BCDD11E8F332BA105E28C473E70A6CD1115D240A4FCE29F8554C4C4418E3
                8ABDCAAB9F42D0C7F3086FB189DA4983D0300D3BE3F73D10C50D0E54B418FC95
                1BA5BBC3F7BCAD7B0F71249120FC979FAFA16F486330E94CA692829CB3F506B3
                65E8ABB9F86F76894AC9CA3B79013D6644240A0B0B0D500B206732D10349CA8A
                F27ECED68BE7197D710AEAFF6D1A24A8D8CE1598AF19B985A5F71F36367D042A
                7F962A6FBCC944A2D21079437DAD8D663FA32F9710F46BB0D7EC141597E49FBA
                F8A8FA05184EFE21D45EE5406BC0882A2AC81AE84CD8BCE667EC61C8F7421437
                78507B22D1DA77F56FDFD583B7F997B60C72C1300F535346DFB281FDD9E7B146
                98B82F5ED7A207343B697D696034C095961457535552E6ADD17C1FBB58F8E9FF
                003D3A71485F0DDF330000000049454E44AE426082}
              mmHeight = 13229
              mmLeft = 148432
              mmTop = 2646
              mmWidth = 51329
              BandType = 1
            end
            object ppLabel199: TppLabel
              UserName = 'plblNumeroConvenio1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero de Convenio: '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 3969
              mmTop = 4763
              mmWidth = 47096
              BandType = 1
            end
            object BajaAnexoVehiculosNumeroConvenio: TppLabel
              UserName = 'BajaAnexoVehiculosNumeroConvenio'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = [bpBottom]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5122
              mmLeft = 51858
              mmTop = 4763
              mmWidth = 46186
              BandType = 1
            end
            object pshp11: TppShape
              UserName = 'Shape801'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 2910
              mmTop = 30692
              mmWidth = 7673
              BandType = 1
            end
            object pshp12: TppShape
              UserName = 'pshp12'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 10054
              mmTop = 30692
              mmWidth = 14288
              BandType = 1
            end
            object pshp13: TppShape
              UserName = 'pshp13'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 23813
              mmTop = 30692
              mmWidth = 34131
              BandType = 1
            end
            object pshp14: TppShape
              UserName = 'pshp14'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 57415
              mmTop = 30692
              mmWidth = 22490
              BandType = 1
            end
            object pshp37: TppShape
              UserName = 'pshp37'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 78846
              mmTop = 30692
              mmWidth = 25665
              BandType = 1
            end
            object pshp44: TppShape
              UserName = 'Shape902'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 103717
              mmTop = 30692
              mmWidth = 20902
              BandType = 1
            end
            object pshp45: TppShape
              UserName = 'pshp45'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 124090
              mmTop = 30692
              mmWidth = 16669
              BandType = 1
            end
            object pshp46: TppShape
              UserName = 'pshp46'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 139965
              mmTop = 30692
              mmWidth = 18785
              BandType = 1
            end
            object pshp47: TppShape
              UserName = 'pshp47'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 157957
              mmTop = 30692
              mmWidth = 18785
              BandType = 1
            end
            object pshp48: TppShape
              UserName = 'pshp48'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 8202
              mmLeft = 175948
              mmTop = 30692
              mmWidth = 25135
              BandType = 1
            end
            object plbl6: TppLabel
              UserName = 'plbl6'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3440
              mmLeft = 85990
              mmTop = 33073
              mmWidth = 10319
              BandType = 1
            end
            object plbl17: TppLabel
              UserName = 'plblNumTAG1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#176' de TAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3440
              mmLeft = 105569
              mmTop = 33073
              mmWidth = 17198
              BandType = 1
            end
            object plbl18: TppLabel
              UserName = 'plblCondicion1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Condici'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 124619
              mmTop = 33073
              mmWidth = 15081
              BandType = 1
            end
            object plbl19: TppLabel
              UserName = 'plbl19'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Motivo devoluci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 6879
              mmLeft = 140759
              mmTop = 31750
              mmWidth = 16933
              BandType = 1
            end
            object plbl20: TppLabel
              UserName = 'plbl20'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 3440
              mmLeft = 11113
              mmTop = 33073
              mmWidth = 11642
              BandType = 1
            end
            object plbl21: TppLabel
              UserName = 'plbl21'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Estado devoluci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 6879
              mmLeft = 158750
              mmTop = 31750
              mmWidth = 16669
              BandType = 1
            end
            object plbl42: TppLabel
              UserName = 'plbl42'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Modalidad Contrato'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 6879
              mmLeft = 179123
              mmTop = 31750
              mmWidth = 19050
              BandType = 1
            end
            object plbl43: TppLabel
              UserName = 'plblNumero1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#176
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 3440
              mmLeft = 4233
              mmTop = 33073
              mmWidth = 3704
              BandType = 1
            end
            object plbl44: TppLabel
              UserName = 'plbl44'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Clase '#13#10'veh'#237'culo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 6879
              mmLeft = 34396
              mmTop = 31750
              mmWidth = 13494
              BandType = 1
            end
            object plbl45: TppLabel
              UserName = 'plbl45'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 3440
              mmLeft = 63500
              mmTop = 33073
              mmWidth = 8996
              BandType = 1
            end
          end
          object pdtlbnd7: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 6085
            mmPrintPosition = 0
            object ppShape119: TppShape
              UserName = 'Shape119'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 2910
              mmTop = 0
              mmWidth = 7673
              BandType = 4
            end
            object ppShape120: TppShape
              UserName = 'Shape120'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 10054
              mmTop = 0
              mmWidth = 14552
              BandType = 4
            end
            object ppShape122: TppShape
              UserName = 'Shape122'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 23813
              mmTop = 0
              mmWidth = 34396
              BandType = 4
            end
            object ppShape121: TppShape
              UserName = 'Shape121'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 57415
              mmTop = 0
              mmWidth = 22490
              BandType = 4
            end
            object ppShape123: TppShape
              UserName = 'Shape123'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 78846
              mmTop = 0
              mmWidth = 25665
              BandType = 4
            end
            object ppShape124: TppShape
              UserName = 'Shape124'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 103717
              mmTop = 0
              mmWidth = 20902
              BandType = 4
            end
            object ppShape125: TppShape
              UserName = 'Shape125'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 124090
              mmTop = 0
              mmWidth = 16669
              BandType = 4
            end
            object ppShape126: TppShape
              UserName = 'Shape126'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 139965
              mmTop = 0
              mmWidth = 18785
              BandType = 4
            end
            object ppShape127: TppShape
              UserName = 'Shape127'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 157957
              mmTop = 0
              mmWidth = 18785
              BandType = 4
            end
            object ppShape128: TppShape
              UserName = 'Shape128'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6085
              mmLeft = 175948
              mmTop = 0
              mmWidth = 25135
              BandType = 4
            end
            object ppDBCalc1: TppDBCalc
              UserName = 'DBCalcNumero1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              DBCalcType = dcCount
              mmHeight = 3440
              mmLeft = 3969
              mmTop = 1588
              mmWidth = 4763
              BandType = 4
            end
            object BajaAnexoVehiculosPatente: TppDBText
              UserName = 'BajaAnexoVehiculosPatente'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3429
              mmLeft = 11113
              mmTop = 1588
              mmWidth = 11642
              BandType = 4
            end
            object BajaAnexoVehiculosClaseVehiculo: TppDBText
              UserName = 'BajaAnexoVehiculosClaseVehiculo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'ClaseVehiculo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 24606
              mmTop = 1588
              mmWidth = 32545
              BandType = 4
            end
            object BajaAnexoVehiculosMarca: TppDBText
              UserName = 'BajaAnexoVehiculosMarca'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 58473
              mmTop = 1588
              mmWidth = 19845
              BandType = 4
            end
            object BajaAnexoVehiculosModelo: TppDBText
              UserName = 'BajaAnexoVehiculosModelo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 79904
              mmTop = 1588
              mmWidth = 23020
              BandType = 4
            end
            object BajaAnexoVehiculosNumeroTAG: TppDBText
              UserName = 'BajaAnexoVehiculosNumeroTAG'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'NumeroTAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 104511
              mmTop = 1588
              mmWidth = 19314
              BandType = 4
            end
            object BajaAnexoVehiculosCondicion: TppDBText
              UserName = 'BajaAnexoVehiculosCondicion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Condicion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 125677
              mmTop = 1588
              mmWidth = 13495
              BandType = 4
            end
            object BajaAnexoVehiculosMotivoDevolucion: TppDBText
              UserName = 'BajaAnexoVehiculosMotivoDevolucion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'MotivoDevolucion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 141817
              mmTop = 1588
              mmWidth = 15080
              BandType = 4
            end
            object BajaAnexoVehiculosEstadoDevolucion: TppDBText
              UserName = 'BajaAnexoVehiculosEstadoDevolucion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'EstadoDevolucion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 159544
              mmTop = 1588
              mmWidth = 15080
              BandType = 4
            end
            object BajaAnexoVehiculosModalidadContrato: TppDBText
              UserName = 'BajaAnexoVehiculosModalidadContrato'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'ModalidadContrato'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 177271
              mmTop = 1588
              mmWidth = 22754
              BandType = 4
            end
          end
          object psmrybnd4: TppSummaryBand
            AlignToBottom = False
            mmBottomOffset = 0
            mmHeight = 13229
            mmPrintPosition = 0
            object ppLabel212: TppLabel
              UserName = 'Label212'
              HyperlinkColor = clBlue
              Border.BorderPositions = [bpTop]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Caption = '            Firma del Cliente                '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 122502
              mmTop = 8996
              mmWidth = 56886
              BandType = 7
            end
          end
        end
      end
    end
    object prm1: TppParameterList
    end
  end
  object dsAnexoVehiculosContrato: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Patente'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ClaseVehiculo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Marca'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Modelo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'SerialTAG'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FechaInstalacion'
        DataType = ftString
        Size = 10
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 69
    Top = 32
    Data = {
      D00000009619E0BD010000001800000006000000000003000000D00007506174
      656E74650100490000000100055749445448020002000A000D436C6173655665
      686963756C6F0100490000000100055749445448020002003200054D61726361
      0100490000000100055749445448020002003200064D6F64656C6F0100490000
      0001000557494454480200020032000953657269616C54414701004900000001
      00055749445448020002003200104665636861496E7374616C6163696F6E0100
      490000000100055749445448020002000A000000}
  end
  object rptAltaContrato: TppReport
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 69
    Top = 176
    Version = '12.04'
    mmColumnWidth = 0
    object pdtlbnd2: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 46831
      mmPrintPosition = 0
      object ContratoCaratula: TppSubReport
        UserName = 'ContratoCaratula'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 5821
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt3: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object phdrbnd2: TppHeaderBand
            mmBottomOffset = 0
            mmHeight = 100013
            mmPrintPosition = 0
            object ppBarCode1: TppBarCode
              UserName = 'bcNumeroConvenioCaratula1'
              AlignBarCode = ahLeft
              BarCodeType = bcCode39
              BarColor = clWindowText
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Data = '00100968630601001'
              PrintHumanReadable = False
              AutoSize = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Courier New'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 6615
              mmLeft = 113771
              mmTop = 46038
              mmWidth = 79904
              BandType = 0
              mmBarWidth = 220
              mmWideBarRatio = 76200
            end
            object ppLabel182: TppLabel
              UserName = 'Label204'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero del Convenio:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 48154
              mmWidth = 45170
              BandType = 0
            end
            object ppLabel184: TppLabel
              UserName = 'Label184'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre del Cliente:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 55033
              mmWidth = 40090
              BandType = 0
            end
            object ppLabel185: TppLabel
              UserName = 'Label185'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RUT del Cliente:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 60854
              mmWidth = 32766
              BandType = 0
            end
            object ppLine7: TppLine
              UserName = 'Line7'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 794
              mmLeft = 25929
              mmTop = 73025
              mmWidth = 165894
              BandType = 0
            end
            object ppLabel187: TppLabel
              UserName = 'Label187'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Punto de Entrega:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 74613
              mmWidth = 36534
              BandType = 0
            end
            object ppLabel188: TppLabel
              UserName = 'Label188'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Operador:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 80433
              mmWidth = 20532
              BandType = 0
            end
            object ppLabel189: TppLabel
              UserName = 'Label189'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha y Hora:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4995
              mmLeft = 25929
              mmTop = 86254
              mmWidth = 27982
              BandType = 0
            end
            object ppLine8: TppLine
              UserName = 'Line8'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 1852
              mmLeft = 25929
              mmTop = 98161
              mmWidth = 165894
              BandType = 0
            end
            object ppLabel203: TppLabel
              UserName = 'Label1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Documentos Incluidos en la Carpeta'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 25929
              mmTop = 91811
              mmWidth = 73290
              BandType = 0
            end
            object CaratulaNumeroConvenio: TppLabel
              UserName = 'CaratulaNumeroConvenio'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 48154
              mmWidth = 39952
              BandType = 0
            end
            object CaratulaNombreCliente: TppLabel
              UserName = 'CaratulaNombreCliente'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 72496
              mmTop = 55033
              mmWidth = 119327
              BandType = 0
            end
            object CaratulaRutCliente: TppLabel
              UserName = 'CaratulaRutCliente'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 61119
              mmWidth = 39952
              BandType = 0
            end
            object CaratulaGiroCliente: TppLabel
              UserName = 'CaratulaGiroCliente'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 66675
              mmWidth = 39952
              BandType = 0
            end
            object CaratulaPuntoEntrega: TppLabel
              UserName = 'CaratulaPuntoEntrega'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 74613
              mmWidth = 119327
              BandType = 0
            end
            object CaratulaOperador: TppLabel
              UserName = 'CaratulaOperador'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 80433
              mmWidth = 119327
              BandType = 0
            end
            object CaratulaFechaHora: TppLabel
              UserName = 'CaratulaFechaHora'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4868
              mmLeft = 72496
              mmTop = 86254
              mmWidth = 39952
              BandType = 0
            end
            object CaratulaTitulo: TppLabel
              UserName = 'CaratulaTitulo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Car'#225'tula para la Carpeta del Cliente - Persona Jur'#237'dica'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 14
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 5821
              mmLeft = 25135
              mmTop = 33073
              mmWidth = 166688
              BandType = 0
            end
            object CaratulaLblGiro: TppLabel
              UserName = 'CaratulaLblGiro'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Giro del Cliente:'
              Ellipsis = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 25929
              mmTop = 66675
              mmWidth = 32808
              BandType = 0
            end
          end
          object pdtlbnd5: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 5027
            mmPrintPosition = 0
            object Check0: TppShape
              UserName = 'Check0'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              Pen.Width = 2
              mmHeight = 4233
              mmLeft = 181240
              mmTop = 265
              mmWidth = 4233
              BandType = 4
            end
            object CaratulaLineaCatarula: TppDBText
              UserName = 'CaratulaLineaCatarula'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'LineaCaratula'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 28046
              mmTop = 265
              mmWidth = 127529
              BandType = 4
            end
          end
          object pftrbnd2: TppFooterBand
            mmBottomOffset = 0
            mmHeight = 32808
            mmPrintPosition = 0
            object pln1: TppLine
              UserName = 'pln1'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 1852
              mmLeft = 25929
              mmTop = 3440
              mmWidth = 165894
              BandType = 8
            end
            object plbl32: TppLabel
              UserName = 'lblFirmaRevisor1'
              HyperlinkColor = clBlue
              Anchors = [atTop, atRight]
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Firma Revisor'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 14
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              Visible = False
              mmHeight = 5800
              mmLeft = 137319
              mmTop = 26988
              mmWidth = 28490
              BandType = 8
            end
            object pln2: TppLine
              UserName = 'lnFirmaRevisor1'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Visible = False
              Weight = 0.750000000000000000
              mmHeight = 1588
              mmLeft = 130440
              mmTop = 24077
              mmWidth = 40746
              BandType = 8
            end
            object Comentario0: TppLabel
              UserName = 'Comentario0'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Comentario :'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 14
              Font.Style = []
              Transparent = True
              mmHeight = 5821
              mmLeft = 25929
              mmTop = 6350
              mmWidth = 25929
              BandType = 8
            end
          end
        end
      end
      object ContratoAlta: TppSubReport
        UserName = 'ContratoAlta'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 14817
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt1: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object ptlbnd1: TppTitleBand
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 69321
            mmPrintPosition = 0
            object img1: TppImage
              UserName = 'imgLogo1'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = True
              Stretch = True
              Anchors = [atTop, atRight]
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0A544A504547496D616765BD3F0000FFD8FFE000104A4649460001010104B004
                B00000FFE1112A4578696600004D4D002A000000080005011200030000000100
                010000013100020000001B000008560132000200000014000008728769000400
                00000100000886EA1C00070000080C0000004A000000001CEA00000008000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000041646F62652050686F746F73686F702043532057696E646F7773000032
                3031313A30393A32302031323A34303A33390000089003000200000014000010
                F890040002000000140000110C92910002000000033030000092920002000000
                0330300000A001000300000001FFFF0000A002000400000001000019D6A00300
                04000000010000093AEA1C00070000080C000008EC000000001CEA0000000800
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000323031313A30393A32302031323A34303A333900323031313A3039
                3A32302031323A34303A3339000000FFE109D9687474703A2F2F6E732E61646F
                62652E636F6D2F7861702F312E302F003C3F787061636B657420626567696E3D
                27EFBBBF272069643D2757354D304D7043656869487A7265537A4E54637A6B63
                3964273F3E0D0A3C783A786D706D65746120786D6C6E733A783D2261646F6265
                3A6E733A6D6574612F223E3C7264663A52444620786D6C6E733A7264663D2268
                7474703A2F2F7777772E77332E6F72672F313939392F30322F32322D7264662D
                73796E7461782D6E7323223E3C7264663A4465736372697074696F6E20726466
                3A61626F75743D22757569643A66616635626464352D626133642D313164612D
                616433312D6433336437353138326631622220786D6C6E733A786D703D226874
                74703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F223E3C786D70
                3A43726561746F72546F6F6C3E41646F62652050686F746F73686F7020435320
                57696E646F77733C2F786D703A43726561746F72546F6F6C3E3C786D703A4372
                65617465446174653E323031312D30392D32305431323A34303A33393C2F786D
                703A437265617465446174653E3C2F7264663A4465736372697074696F6E3E3C
                2F7264663A5244463E3C2F783A786D706D6574613E0D0A202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020200A20202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                0A20202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                20202020200A2020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                202020202020202020200A202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020200A20202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                20202020202020202020202020202020202020200A2020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                202020202020202020202020202020202020202020202020200A202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020200A20
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020200A20202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                20202020202020200A2020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                202020202020202020202020200A202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020200A20202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                20202020202020202020202020202020202020202020200A2020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                202020202020202020202020202020202020202020202020202020200A202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                200A202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020200A20202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                20202020202020202020200A2020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                202020202020202020202020202020200A202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020200A20202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                2020202020202020202020202020202020202020202020202020202020202020
                20202020202020202020202020202020202020202020202020200A2020202020
                20202020202020202020202020202020202020202020203C3F787061636B6574
                20656E643D2777273F3EFFDB0043000101010101010101010101010101010201
                0101010102010101020202020202020202030304030303030302020304030304
                040404040203050504040504040404FFDB004301010101010101020101020403
                0203040404040404040404040404040404040404040404040404040404040404
                0404040404040404040404040404040404040404FFC0001108004700C6030122
                00021101031101FFC4001F000001050101010101010000000000000000010203
                0405060708090A0BFFC400B5100002010303020403050504040000017D010203
                00041105122131410613516107227114328191A1082342B1C11552D1F0243362
                7282090A161718191A25262728292A3435363738393A434445464748494A5354
                55565758595A636465666768696A737475767778797A838485868788898A9293
                9495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8
                C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFF
                C4001F0100030101010101010101010000000000000102030405060708090A0B
                FFC400B511000201020404030407050404000102770001020311040521310612
                41510761711322328108144291A1B1C109233352F0156272D10A162434E125F1
                1718191A262728292A35363738393A434445464748494A535455565758595A63
                6465666768696A737475767778797A82838485868788898A9293949596979899
                9AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5
                D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C0301000211
                0311003F00FEFC28A2BF8E0FD9AA2FF8294FFC1437F6BBFF00828DF817C05FF0
                534F8C7FB3CE8FFB367C71D5B4AF06E876BE16B3F177862E2D6F7C4BE2AB1D37
                4D58D5ED8DAC16916831A023CE675909DB953BBB70983FAD42A54954508C126D
                BBF576E899CB89C57D5E508460E5295EC95BA2BF568FEC7E8AFE613FE097DFF0
                50EFDAC7F680FD8F7FE0A5FE12F8E7E3DB2F1A7C56FD8D3C1DE20B2F047C7FF0
                BD859D8DE6B928D0BC54B6F23496F0C769712D9DDF877ED56F78B0A34D05F406
                45665DEFF637FC1BF7F1DFE33FED17FF0004EFD03E237C79F89BE2CF8B7E3E93
                E2E78AB4397C61E34BD4D435E9ACED2EADC5ADBBCCA8BB96212305C8C80D8CE3
                15AE232CAF868549D492F72518E9D7997326BE4674330A5889538C13F7E2E5FF
                0080BB347EDAD15FCB37ECC5F147F6CEFDAC3F6EFF00F82D57ECA9A2FED81F12
                3E1C43E149AE340FD9F7C4735B5BF89B4EF8093C7E2AB9B58EE747D3FF007247
                FA2C1F662BE68255F764B2835F197C5CF0AFFC1553E13FFC147BE017FC13BE7F
                F82B2FC64D7356F8EFE075F1ADB7C578BC290E9FA7F85D5A1F13CC2D64D18CCE
                F3B7FC534C37ADCC63FD3578FDD9DDBC32772A92A33AD1525152FB5F0F2A95F4
                8F44FF0003196676846A46949C5CB97ECEFCDCBDCFED9A8AFE6A7F6EEF893FB5
                CFFC132BFE098CDF0BFC51FB62F8CBF68BFDB17F686FDA02DBE1A7C1EF8D3FD8
                11784FC73A15AEB1716B75716DA6DAF997019ACED74FBAB78E7625966D721C01
                B52BA9FF008216FED61FB48789FC79FB67FEC3DFB67FC49D7FE247ED11FB347C
                465D56CF5CF15EA4BACEB175A4CADFD91A95AC378B1A79F696B7B676B730C8CB
                968BC43173B76818BCB2A7D5278D84D4A317E776AE97324D6D7763558F87D663
                859C5A935E5A3D5F2BB75B23FA2DA2B3F569E5B6D2B53B985B64D6FA7CD3C4F8
                DDB5963665383C70477AFE2EFF00E099FE15FF0082AE7FC1497E0FFC5AF8C7E1
                3FF82B07C5AF84FAE7C38F8A973E01D37C27E21F0759F8BF41D7275D36C3568E
                E27B846845BC24EA021F296DE6DA21CE1810959E1303F59A552BCEA4611872DD
                BE6FB57B7C29F634C4629D0A90A51A6E5295F6B74B776BB9FDAA515FCF87FC12
                6BFE0A23FB5178FF00F687FDA1BFE09D9FB7AC3E1EBFFDA73F679B2B8D6B45F8
                8DE1EB0B7D22DFE20E9B6371676F78B750411C56F24823D4F49BEB6BBB786017
                169A8132411C90B993F2EFFE09A3A1FF00C14D3FE0A57E15F8E7E3F8FF00E0AD
                9F1ABE0647F0C3E2F4DE034D09FC27A7F8C23D5566B58F525B88D85C5988238C
                5C792B1046188B3B87DDAE8FEC8A90751D7A918C61CAF9B569A9FC2E364F431F
                ED184BD9AA34E5294B9B4D135CBBDEECFED4E8AFE5A3FE0B2BF14BF6C7FD833F
                633FD807C1BE15FDB0BE276BFF0016FF00E1727FC219F15BE3AE936B6BE12F11
                7C5F8E2D36F6F15B51B51E7A2C7B9A20620EC1BC952C5B26BFA948C968E32792
                50127D78AE3AF84950A14F11CC9C66E56B5FECBB5F5EFD0E8A389556ACE8F2B5
                28F2DFFEDE57FC07D15F875FF07057ED15F1B7F663FD8334EF887F017E27789B
                E1278CEF7E3E786BC2F7FE2DF08B5BC5ADFF00665C5AEB575756B1C92C520459
                1ECEDF714018AC657761981F38FF00821A7ED87FB41FC4BBFF00DAA7F63BFDB1
                3E20EA7F10BF687FD99BC736BAC69DE25F122C0BE22F1078635985563DD24514
                6278ED6E62596399D4C9E46B966A5982AD6B1CBAB4B2F798C64B953DBAF457F4
                BC919BC7538E33EA4D3E66B7E9D74FC19FD06515FC2EFF00C152BFE0A77FB71E
                AFFB517ED1BE32FD973F688F1CFC29FD98FF00660F8C5E19FD976CAD7C14F611
                68FE2EF164F6DACDF6AD71348D6F27DA98DC683ACC4C246282DACED8051E6B16
                FEA13FE0A81FB5BDDFEC73FF0004F6F8E1F1EF4DD5E1D2BE202780A1F0AFC33B
                C215664F14788BCBD334BB986320AB35A4974F7E5082BB34F7C820115A56CA71
                147EAF16D3955D976F877FFC09114B32A157DB349A8D3DDF7DF6FB8FD1EA2BF9
                48FF0082277ED7FF00B62F877F6CAF897FB12FEDE3F16FC6DF11FC63F107E027
                86BE3CFC2097E21EAEBAD6A7A519F4AB3D727B0B59C451906E74CD72396685B2
                22B8F0F5DA8C157DDF44FEDC1FB4D7ED0BE01FF82E8FFC13A3E01782BE3178DB
                C31F04BE247806DEFBE207C2DD26FA28BC21E319A6BCF1B4724D7F6E632D2395
                D3EC403B86DFB2A6DC1DD973CA6B43172C2F32768B9A9747151BE9F97A84331A
                53C3C711CAD5E4A36EA9DEDA9FD17515FC97FC65F14FEDE7FB567FC16BBF69AF
                D8B3E0DFEDF5F15FF65BF87DE04F869A778F7C2F6DE17D06CFC4FA0E9AB6BE1F
                F07BDC59C7A796B76DD7371AF5C5C3CEF33302A46D20A85F59FF00826E7FC145
                3F6A2F863FB447EDF3FB1B7EDE3F1374EF8EB6BFB12FC31D7FE2FA7C71D2F43B
                5D2FC413E9BE1796C9B53B7B9F2238A3B85B9B5D52CAE20FB42FDA219A3B8824
                9A61E59472CA2AAA1ED615232972C67CBAF372CAD6E96EBDC51CCA9BABECE516
                A3CCE3CDA5AF1F9DFF0003FA77A2BF919FD936CBFE0AA5FF0005A3B1F881FB54
                7FC37678CFF613FD9F22F1AEA1E0EF83DF0DBE0B688D3DEDCFD8591A65BA921B
                AB29AE23B633476F35EDD5C4CD717315C88A0B78A3453EF3FF0004EAFDB07F6D
                4FD9E7FE0A3DE35FF82537EDD9F14A1FDA0EE6F7C2B3F8B7E09FC6BBBB38EDBC
                477A21D34EB96E935C054927B6BED3E3D4094BBF36E2D6F349962171346EAC0A
                B94CE9C6A45548BA94D5E5157BA5D75B72BB75B3153CCA337072A7254E6ED196
                966FA697BABF43FA6DA28A2BC93D20AFE0D3F65BFF00827BDD7EDFFF00B527FC
                163BC3DE1FF8DFF15FE0CF8FBE1EFC64F105CF8093C05E2A9341F04F8D350D57
                C57E3858EC7C5D6A9892EECB7E9F0C602491B44B7970C377DD3FDE5D7CDBF053
                F641FD9B3F674F1D7C5FF897F053E13E83E00F1CFC7CF107FC253F17BC45A55D
                DF5CDDF8D6FF00ED57D7DF689D279E48E33E7EA77F2ECB758D375D39DBD31E96
                031EF034EB727C7251E57E92BEBF2387198358BA94F9FE18DEFF003563F9DDFF
                00822478EBC37F14BFE098BFB6CFEC85E16F847E16F873FB4BFC26F0EF8CBC05
                F10BC3FE1BD3DAC3C41F136FB5BF0FEA9A668FAC6A9E63B4B2DF8BAB1BCD1E7C
                11123693198D22595635B7FF0006E67ED97FB2FF00C2CFD827C43F073E2DFC70
                F86BF08FE237C32F8B5E23F117897C31F143C6163E03D43FB3750FB2DC437D6E
                B7B244264478EEADA6488B490CD6722C8ABBA3DFFD047C2DFD8B7F65EF827F1B
                7E29FED19F0ABE0FF87FC13F1A3E368B8FF85A7E38D1EF750173E303757B1EA5
                72D7166F70D688D2DD46B3B3C30A3348CE49CBBEEF9D3E357FC11E7FE09AFF00
                B427C41D5BE2A7C54FD947C09ABF8F3C41A91D63C47AF685AA6B5E046F11DDB7
                325D5FDBE977B6D04F3CA7E692792332C8C49776249AEDA998E0710EBD3AB19A
                A7525196966D492D56AF55ABB6BF23969E0B174634674DC5CE0A51D6F6717B6C
                B7D353F203FE0863AC5A7C71FF0082937FC15C7F6A9F878977AA7C12F1E7C415
                D37C1FE309ACE4B2B5D764BEF116B1AA5AF92AE036E6B28EDEEDA3237C71DFDB
                17553228AD1FDB13FE565FFF00827CFF00D90D87FF0048FE2857EE8781B5EFD8
                E3F634D334DFD9C7E1B68FE13F831A2787A5864B3F01F83BC1D796DA6C736A42
                375B89A6860613CF3EE8CC971348F2B1C6F724576FE2BFD903F66CF1C7ED11E0
                3FDAC7C57F09F41D6BF688F863A19F0D7813E27DCDDDF26B3E1DB12BA8C7E447
                024EB6AF85D5B520AD2C4CCBF6B7C30E31A622BD6C3E25E37114270A5569B8D3
                E68DB9972A8A95DD935B36D5F7397093C16329CB0382C4D3A95A8D44EAA8C949
                C25CCDB8C92BB8BD1A4A56D99FCC9FFC14F7E257ED03FB517FC1653E04FC1DFD
                94FE0D699FB4A6B7FF0004F2F08DB7C66D43E156BBE2EB3F0A7826FF00C47797
                1A6EA97579A9EA13CD1451AD909BC1AAB197DEF32BC7B483201F3F49F177F6BD
                FD93FF00E0B51FB3A7ED8BFB67FECE1E1AFD93E0FDAE2E23F82BE3AD1FC21F10
                2CFC73E06F14594B6DA5F86EE7579AF2DAE26114F6B7337846EE78A52BF2E9E2
                401B739AFEBB7E17FEC7FF00B35FC17F8C9F17BF683F865F09F41F0A7C67F8F3
                73F6BF8B5F106DAEEFAFB5CF18B7DA0DD112F9F3C91C2865C398ED922425132A
                762EDAFF00B4D7EC69FB31FED91A3784740FDA63E11787FE2C697E02D7A4F137
                83A1D6AF2FF4D9FC3F7D343F679A6B7B8B3B88651BE3DAAC858A36C425495522
                69E6F86828E1DD2BD154F91BFB5AEB2B7BDCBF16BB5CE9A996D79B957F69FBCE
                7E64BECE9B5FDDE6F8743E86D77FE409ACFF00D82AE3FF0044BD7F233FF06E57
                ED53FB347ECE9FB267ED309F1E3E3E7C22F845772FED1575E23B5D2BE2078FF4
                DF0C6B9A8D82F86F4284DD5A58CF32DC5C46648668C18237DCF1328CB0C57F5E
                82DA0FB37D90C61EDBC8FB318A4FDEAB47B76ED6CF27238E7AD7E5AAFF00C111
                BFE0950B7C3503FB15FC2C92E45CB5D7EFEEF5AB8B62CCC5DBF70D7C622B963F
                26DDBDB6E2B870789C2D3C2D6C2E2B9AD3717EEDBECDFBB5DCEBC551C44EBD3A
                F87E5F779BE2BFDAB76F43F1DFFE097BAFB7EDCDFF0005B4FDB6BFE0A03F0D34
                9D5A3FD9DFC2DE07B8F86FE18F185E699369B69E29BC9ECFC37A1E9B1AAC8AAC
                269ECFC3D7DA9BC0C3CC821BAB312AC6D2AAD7E627FC1277F634FF00826B7ED4
                3E03FDA1B5AFDB9FE36597C2AF17F877E334FE1DF0469D75FB4959FC0E7D5B47
                9AC62B89AE16CE79A36B9DB7325C27DA17701B767056BFBD7F869F0B3E1A7C19
                F06E95F0F3E127807C1FF0D3C09A1C663D27C23E07F0EDAF863C3D63BBEFB25A
                DBA247BDC8CBC846E76C96662735F9CF7BFF000442FF0082536A377757D79FB1
                6FC319AEAF6EA5BDB893FB4B5D8D5A49A469642A8B7E15416663B540519C0005
                7A34F39A09D58AE7845AA718B8D9C9285F7D56E704F2AA8FD9B7CB369CA52E6B
                A4DCADB6FB58FC68FF00838B1BE195A7EC67FF0004E2B4F843E2BD1BC63F09FC
                3DF18AD7C3FE06F16E8BE2A87C69A46ABA4E93E179B4FB79D7588A4923BADB1D
                A61EE03B6E68DC939CD7F4C1F0BFF6BCFD953E3378960F037C21FDA4FE057C50
                F19BE9736AD1F853C01F15744F16F88E5B5B5F2D6E2E16CADAE5E631C4658F7B
                85C2F98B9C66BCB7C67FF04DDFD877E217C0EF859FB36F8CBF673F046BBF03FE
                095E49A87C2AF87D713EA30E97E099A55BA8E692D268EE56E3320BDBADDE648C
                1BCE24E4818CDFD9F7FE098BFB067ECADF122DFE2F7ECFBFB35F823E197C49B4
                D12F3C396BE2DD16F354BAD46DEC750F27ED96EAB71772C604BE4441885DD85C
                020139E2AD8AC156C0C30F2E7E6839F2E91B3E6775CDAFDF63AE961F154B152A
                D1E5E59F2DF7D3955B4D0FCBCFF83A1FFE51A3A5FF00D9C9F85FFF004D7E24AF
                CF7FF828BFC40F1CFF00C12FFF006F0FD9CFFE0A11F0DF4AB9D4345FDA7FF62B
                BAF85FE35B05B85B5B0D4BC4BA4785EC74EB39AE091B58404782B50DAE3322E8
                774A0E5B23FABDFDA47F65BF803FB5EFC3B8FE137ED23F0D747F8ABF0F22F10D
                AF8AE3F0C6B7797B63689A859A4F1DADD092D66865DC897570B8DFB489981045
                73BFB43FEC5BFB2DFED5FF000FFC25F0B3F687F835E17F8A1E02F01EA50EAFE0
                EF0F6B52DE59C5E1EB8B7B3934F8DADE6B69E29D57ECD3490B47BF63AB61D5B0
                31AE0B34A187A34A8568B715ED39D778CD476D774E3733C5602B56AB52B53925
                27C9CBE4E2DFE8EC7F153FB4FF00ECDBA9FECF3FF040BFD9175CF15C7747E23F
                ED2DFB65E9FF00B4AFC42BCD461D9AADF49E20F0CF89BFB14DC39F9D9BFB2A1D
                3E660DD25BCB838CB1CFE90FFC1797C71F127F68DFDA0FF60CFF0082757C07F0
                37FC2E2F1A029FB4A78D3E13AF8820D034CF1A1B1B7B88F49D2F52BC9248E3B5
                80E9FA778AA59A59245DB1DCC4CBF314CFF481F1F3F63CFD99FF006A0F871E13
                F843F1E3E10F86FE217C35F02EB563E21F087842FA5BCD274AF0FDDE9B693585
                8C96FF00649A175115BDC4D088F7797B2420A918A6E9FF00B1DFECD1A5FED257
                DFB5F58FC24F0FC5FB48EA3E128FC0B73F155EEEFAE75B5D263B5B7B14B386DD
                E76B5840B7B5861DF142B21556058EF7DDAC739A4EA4711522DD48BA925DB9A7
                6E5EB7B46DF958C9E575146546124A1254E2FBDA3F174DD9FC74FEDEBF15FF00
                E0A11F03FF006CCFD8CFFE0A5DFB5A7EC6FE10FD976DFE1378C74AF8577DA9FC
                31F89BA7F8FB49F881A4C72EA57979A5EA1F65BA99EDE53A3DD7896084BAAA48
                8FB7398D16BEFBFDBD758D2FC43FF070AFFC127F5FD12FADF54D175CF84BA5EB
                1A46A567279B69A85ADD5DFC409ADE789BBA489223A9EE1857F47BFB457ECCDF
                027F6B4F86F3FC22FDA2FE1BE8BF14FE1CDC6B569E227F0CEB93DD5A5BA5F58B
                335A5D4735B4B14F1C91F9920DD1C8B9591D4E55981F387FD82BF64497E247C0
                5F8BD2FC11F0E4DF127F660F03D8FC36F80FE2C9B53D525D43E1DE87A5C3756F
                A7D85B29BAF2E65823BCBA547BA59A41E731DD9E694737C3B8C1CE9F2CA30A90
                F776B497BBF149BD1B7729E5B5D4A4A352F17284BDEDEF17AECADAA48FE5E3E2
                3FC0EF8E7FB427FC1C35FB627C3DFD9EFF0069EF12FEC93E3CFF008541A7EBB7
                5F157C27E1A8FC55AB5D6976FE19F87F1DD68FF6579E1012E24B8B598CBE6655
                AC5703278FD50F04FF00C1183C2DFB33FEC83FB7CE8FE14F889E3DFDA2FF006B
                2FDAC7E05F8AFC37E29F8C9E3E44B6F10F8A2F2F34FD4AEAD74DB3B559246856
                F350B8596E249E79E6B899E32F2958E344FD62D03F641FD9B7C2FF00B47F8BBF
                6BAD07E14685A6FED1FE3CF0E2F847C5BF14E1BCBE7D6B5AD3961D3EDC5BC96E
                D39B551E5E95A7A178E15722D532C79CFD235CF88CDEB4A14E950768C6304F45
                AB8DBAEED5D69AFC8DA865B4E12A93ADAC9CA4D6AF452F2D933F974FF83783F6
                E8FD997C21FB0EB7ECF5F163E30FC39F837F157E0FFC4AF135CEAFE16F8A9E2D
                B1F87B7B7FA76ADA949AA457B6ED7F24292F9335D5DD9DC448C64B796C584888
                1A32DE3FF057C71E1CFF008281FF00C1C747F680FD9FAECF8CFE057ECB9F085B
                4BF107C4FD2226FF00846B599ADF41D6744B76B7B8236C8977A97892EA2B661F
                F1F10E8B73347BA250F5FB93F1DFFE090DFF0004E0FDA53C77AAFC4EF8BDFB2A
                F80F5CF1EEBF7BFDA5E22F13685A86AFE03BEF11DCFF001DCEA0BA5DE5AC7733
                C9D5E7995A573CB3935F567ECF9FB317ECFDFB297824FC3AFD9D7E12F833E11F
                83E4BB3A8DEE97E12D2C5ACDABDC91B7ED5A85E396B9BB9F6E17CEBA924936A8
                5DD80056D5731C129D7C4E1E32F6B562D34EDCB1E6F8ACF77E5A233A782C5F25
                1C3D6947D9D369DD5EEF97E1F25E7B9EED451457827AE14514500145145007F3
                17FF000512FF008287FEC7DF013F6EDD77E0FF00C54F07FED43AB7C49B16F064
                973A8FC39B0F08CDE027FED4B6B1934FF2DEF6F63BCF95648C4D98F821B66E18
                AFE8A7E307C64F85DF003E1C78A3E2EFC67F1CE81F0E7E1BF832C3FB4BC49E2D
                F12DDFD934DD3E3DCB1C6A300BCB2CAEC9145042AF2CD248891A3BB2A9FE02FF
                00E0BC3FF2994F19FF00B9F0A7FF004DFA4D7E9DFF00C1D65E32F88B6F6BFB1B
                7C3D8A5BFB5F845ADDEF8B3C61A9C50C922697AE78934B4D1AD74F5BA507633D
                A59EA5A8490AB74FB5CCC395CAFDB6328D6CCE965783AD55F2F23B5EDEEA518B
                B474F2EB73E2F2EC2E5F9362334CC3054146A54A8A536B9BDE9394B595DBEB26
                F4B6E7D45E33FF0083A3BF629D135FBDD37C1BF057F693F1FE876B23456FE298
                742D07C2967AA6D6C799059DEEA6974B1B75533C713E3AC6B5CA7FC4549FB29F
                FD1B17ED2BFF007FFC27FF00CB5AF93BFE09A5FB02FF00C10FFE32FEC91F0CBE
                207C7BF8C7E0FF0012FC74D774BF3FE2D68DE3FF00DA4E4F839A9F81359DEFF6
                8D1EDF418B51B364B5B71B162B99166374A04C25C48113EFAFF8759FFC1BD1FF
                004397C15FFC4E3BCFFE686B96AD1C82854951950AADC74BFF004D1E8D3AB9C5
                5846AC6B5357D7FAD19E6DFF0011527ECA7FF46C5FB4AFFDFF00F09FFF002D68
                FF0088A93F653FFA362FDA57FEFF00F84FFF0096B5F5E7863FE0827FF047EF1B
                E8B69E24F067C2AB9F17787AFC16B1D7BC31FB4478B35FD16F40EBE4DD41AB3C
                4F8FF658D6AEA1FF0006FBFF00C126749B2BBD4F54F825AF69BA6D840D757DA8
                6A1F1E7C63676567146BB9E49A67D502A2A8E4B31000EB5CFED78653B7B1A9FD
                7FDBC6FECF3EB5FDA43FAFFB74F8BBFE22A4FD94FF00E8D8BF695FFBFF00E13F
                FE5AD74BE11FF83A4BF62DD5F5EB0D3BC5DF03BF694F03E857532C379E287D17
                C3FE28B7D2431C79D359DA6A6D72F1AF56F223924C676C6C78AE9E4FF8257FFC
                1BD90C8F14BE2FF83114B1398E48E4FDB86F5248D97865653E20C820F1835F9F
                1FF053FF00F8262FFC12BBC01FB17FC5FF008F3FB137C40F0FB7C52F81C9A1F8
                8B53D07C07FB417FC2E7D3B5BD2F56F11697E1F9A3D56C26D42EE4B6407520F1
                5DC463C4B0AA36F572B5D14A8F0FD7AB1A2A8D55CCD2BBDB5D17566152A6734A
                9CAAFB5A6EDADBD3E47F611F0ABE2AFC3AF8E1F0EFC23F167E1278C344F1F7C3
                8F1DE8F1EBDE12F17787AEBED7A56B36B26E5DC8D80C8E8EB24724522AC91491
                BC722A3AB28F40AFE507FE0D72F8C17969FB39FED4BE01F19F8B34FD33C09E01
                F8D5A3EABE0D8F5FD522D3ECB4ABAF1268F7136A56D6F24ACAA16693495B9F29
                4FFAC96E1F196635FD4D8F17786A5F0F6A1E2BB3D6F4DD53C3DA5D9DC5F5E6AD
                A3DDA6AF6891DA234971B5A12C19902365572D918C66BC3CC301530589A94526
                E29D94ADF35F33D4C163E8E2E946F24AA35771BEBDAF6DEC747457CB9E0AFDB3
                BF672F881F117C01F09FC35F10166F1FFC51FD9EF4FF00DA8FC05E1FD4340D4B
                4893C4BE0BD51B6DAEA16F34F024267C7EF24D3CB8BB8E3CC8D0845661E6DA8F
                FC144FF67BB35F85B358691F1F3C5565F1B7C3767E28F847A9F82BF66BF1CF8B
                F4BF8856F79A2CDE215874BB8B5D31D26B9874FB7B8B99ED41F3A05B797CC452
                AC2B9561F10DF2AA6FEEFEBB3FB8EA75E8A57725F79F76515F2C786BF6C3F843
                E2EF1D78D3C09A069FF15EF64F875ABEA9E1FF001CF8CCFC17F145BFC30F0BEA
                1A2E9ABAAEA76375E257B11A7ACF0432468D179C58CCEB10CC876D705E11FF00
                828B7ECB1E30D22C35983C51E35F0D5BEA96DE0FD674BB7F1FFC25F147C3FBED
                5746F1EEB569E1DF0AF892D22D42C2133E8D79A95F59DA36A70EFB7B77B88FED
                0F08752C7D5F11FC8FA74EFB07B7A3B732FBFB1F72515E07E35FDA6BE0E7C3EF
                1EB7C34F13F892EED7C5F1CDE0BB6B9B1B4D02FF0053B5B29BE21788EF3C2BE1
                1867BA861686392FF51D3EFA354660D1C76B24D204886F37B47FDA2FE0FEBDF1
                FBC63FB31693E2D4BBF8D7E02F0069FF00137C51E111A5DE469A7E91A95C1B6B
                7996F9A216B2CAACD6AF35B452B4D0C7A8D93C88AB710B3CFB2AB6E6E57DFF00
                2FF35F795ED217B7323DBA8AF3BF84FF00153C11F1B7E1E7867E29FC38D525D6
                FC13E2FB492F740D527D3AE34996F2386E26B5919ADE744953124122E1D41F97
                3D08AF44A869C5F2CB468A4D495D0514514861451450014514500145145007F9
                DFFF00C178587FC3E4FC6BC81B17E14862780BFF0012ED25BF91AFEE23F6D5FD
                8A7E077EDE9F04755F81DF1D348BD9F4796FA3D7FC2BE2AD02E134FF0018FC3E
                D6208E48EDB56D26E991D526449A689E3911E19A29A48E48DD188AFE7E7FE0BF
                7FF048BF8D5FB45FC42D27F6CAFD96BC2971F12BC58BE0DB5F03FC66F84BA44B
                1C7E31D621D2DA6FEC9D7B438E4755BB9638676B4B9B24659992DED5E1594895
                6BF18FC35FB62FFC17EFE1DE87A6F81F49D57F6E8B7D37C2F6CBA2D8DAF883F6
                6ABAF156AD6715BAF971C336A17DE1F9AEA628142EF9E591B0A32C6BED3D8BCD
                30583A982AD1854A4ACEEECD3F7574F4F99F2FED7EA18BC44315465285477D15
                D35AFF0099F7178E7FE0D4FF008FF1F88AFA3F87FF00B547C13F11F851656FEC
                8BEF1FF80B58F0EF89FCACFCAB7305AB5D405C0EAD1C8AAC7908BD071C7FE0D5
                1FDACF071FB44FECBC4E380741F12807FF00252BE6BFF86F8FF8382FFE835FB6
                7FFE224C5FFCCC5237EDF1FF0007059520EB7FB68004632BFB254608FF00CB62
                BBD2E204ADF5AA5FD7FDBA71B793B77FABD4FEBFEDE3CCFE09FC4EFDACFF00E0
                861FB7EA7C2BF14EB90A68DA3F8AB44B7F8DBF0D7C2BE229358F857F167C2BAC
                B42D1EAD6503AA08EF12DA792E2D6E5A186EA19ADDA194346D246FF4C7FC167B
                F6B5FDA3FF006ECFF8282EB1FB04FC31D4F5683E19F81FE2DE9FF00BC01F096C
                F5C1E1ED03E2578C2792DED6EF58F11485D629D56F2EA4B7B71725A0B5B7B432
                AA7992C8C7A2FD823FE08FBFB6FF00EDCBFB4FE8BFB4B7EDE7A27C4CF077C32B
                6F1858F8FF00E22F897E39F9DA77C5BF8DF369AD6F359E8B63A4CDB6EADACA51
                6F6B04B71711DBC30DA46D15B46EC57CBFA97FE0B4FF00F0467FDA42FF00F68F
                F12FEDC3FB1578735DF8831F8EF57B3F1BFC42F87FE03BF361F163C05E28B15B
                78FF00E120F0EC624496EE2B96B5B6BA68ED1FED96F74B2BC71C8927EE53C4E5
                AB33A6EB4A2F10A0D397D9E7D2DF3F8BF21C6863BFB3E6A9C64A8B92B47ED72E
                BFF03F33E78B1FF83557F6BB9ACED65BDF8FBFB2DE9F7725BA4973611697E24B
                F8ECA46505A259BEC28240A7E5DE15738CE05687C51FF8237FC68FF8269FEC11
                FF000517F89DF13BE287C22F1E695F12FE0C7827E1F697A7FC3AD3755B1D46C2
                E2DFE29784F5269AE5AEE18D0C4C91EC01096DCC38C57CBD6FFB72FF00C1C19A
                2C1169075AFDB773A620B13FDA7FB2B7F68EA03CBF97F7D712F869A491F8F99D
                D9998F24935E6FF13353FF0082E1FEDF567A4FC19F89FE0DFDB2FE2E7879B58B
                7D4E2F06EB9F06E6F861E066BA8DF16D79AB5C7F66E9F6056066DC925FCA6389
                BE71B58061718E6F29C7EB78AA7ECD38B76ED169F65DBB90DE5D18BFAB61EA7B
                4B34BE6ADDFF0043F47FFE0DE7F82DADFC77FD9EFF006C7F07683AB68FA35E69
                FF001D7C07E2392EB5B8669AD248E1F0E7896DDA351182DBCB5D2904F18535FD
                647C2BF829E24F85DFB3AF893E15DCDEE97AFF00882EB4BF100B297470D69637
                326A51DC1B78819B6ED399154B36173CE715F1B7FC11B7FE09D9AAFF00C13B3F
                65CB9F087C40BFD2356F8E3F167C4FFF000B1FE2F5C6813B5E687A15C7D961B3
                D3B41B3B8217CF8F4FB6876BDC055596E2E2E993F7663AFD6DAF0738E20C562A
                1532CA524F0AAA73AD17C495AF7DEDB9D395F0A65B86CD23C493A6D639D3F66D
                F33B725EF6E5DAFA2D4FC29D0FF626FDA2E1F0D7813C5BA278574DF0A7C73F80
                5FB0A7C01B3F81BADEABAF584D651FC4AF8790F8FADFC57E0DBC9A09D8AE9FAB
                69DE206D0AFAE39B77B7D78CB1BC8F6E0A7D09F04BF678F8C9E17F879FF048ED
                2BC41E087D1754FD9BF43B88FE37696FE20D3B507F87F2CFF08FC49E1C5B769A
                39D92F0A6A7A85ADA17B2332932799FEAC338FD52A2BC59E3AB544D492D7FF00
                9171FD5BF53E8E183A50778DFF00A69FE9F71F901F087E087C58F05FC64FDB12
                1D77E147ED18ADF193E2B7C44F13F813C5765F17B459BF662BDD2F5CD06CD74B
                9EEBC2E35F1711DFCF359B5B79CDA5191669559E458D9A45F29F821FF04E8F88
                7A47816FBE0FFC52B7F1678A3C3BF1CFFE09B9E1BF833ADFC41F883E37B5F1BF
                8BBF65EF1B787EDEDE29FC2FA0BACA0A68F25D6A16BABDA2D9ACC90DF784A567
                B86496CD23FDD5A29FD7EBEB6B6B6EFF00676EA2583A57BBD6D7FC773F197C1F
                FB3A7ED41E39F84DE1FF008ADF19FE1C69BA27ED39F143F6DBF841F16BE2E784
                74BF1469BA9DB7827C2BF0DB5EF0D6971AC37C93FD9A68859687AA78805BDBB3
                3893C49711AA34994ACDF007ECB9FB63787FE29FC1FF00DADB596F08DC78DB5D
                FDAABC49E3CF8B1F0521D1A3B6F1D785BC13F1112DFC2773A6DD78C06B92586A
                29E1DD1F45F02EA06CEDECA3323F84CA43248FB44DFB59451F5FAA934A31B3F2
                E8D5B97D2D6F3D371FD4E9E8DB7A79F5DEFEBF8791F9F9FF0004EDD37E2D7C36
                F80FE0AF803F163E04FC42F867ADFC2AD0EF2C6E3C65AF6B5E14D6BC0FE31926
                D6B5099068F269BAC5D5E90619A39B3796B6DF2B63EF656BF40E8A2B96AD4756
                A4AAB56BEA6F4E0A9538D35AD828A28ACCB0A28A2800A28A2800A28A2800A28A
                2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A
                2800A28A2800A28A2803FFD9}
              mmHeight = 18785
              mmLeft = 149225
              mmTop = 529
              mmWidth = 52388
              BandType = 1
            end
            object plbl1: TppLabel
              UserName = 'plblNumeroCuenta1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero de Convenio: '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 3969
              mmTop = 4763
              mmWidth = 47096
              BandType = 1
            end
            object plbl2: TppLabel
              UserName = 'plblTitulo'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'ANEXO I CONTRATO DE ARRENDAMIENTO DE TAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 45773
              mmTop = 17992
              mmWidth = 111390
              BandType = 1
            end
            object plbl5: TppLabel
              UserName = 'plblTituloDatosCliente'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Datos Cliente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 2910
              mmTop = 24342
              mmWidth = 23548
              BandType = 1
            end
            object pshp16: TppShape
              UserName = 'pshp16'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 28310
              mmLeft = 2910
              mmTop = 29104
              mmWidth = 198702
              BandType = 1
            end
            object plbl7: TppLabel
              UserName = 'plblDireccion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Direcci'#243'n:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 43392
              mmWidth = 15346
              BandType = 1
            end
            object plbl8: TppLabel
              UserName = 'plblTelefonoPrincipal'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Tel'#233'fono Principal:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 47890
              mmWidth = 28840
              BandType = 1
            end
            object plbl9: TppLabel
              UserName = 'plblEmailCliente'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Email:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 52388
              mmWidth = 9525
              BandType = 1
            end
            object NumeroConvenio1: TppLabel
              UserName = 'NumeroConvenio1'
              HyperlinkColor = clBlue
              Border.BorderPositions = [bpBottom]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Caption = 'NumeroConvenio1'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 52123
              mmTop = 4763
              mmWidth = 62177
              BandType = 1
            end
            object Direccion1: TppLabel
              UserName = 'Direccion1'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 20638
              mmTop = 43392
              mmWidth = 179123
              BandType = 1
            end
            object Telefono1: TppLabel
              UserName = 'Telefono1'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 34396
              mmTop = 47890
              mmWidth = 165365
              BandType = 1
            end
            object Email1: TppLabel
              UserName = 'Email1'
              HyperlinkEnabled = False
              HyperlinkColor = clBlack
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 15346
              mmTop = 52388
              mmWidth = 184415
              BandType = 1
            end
            object AltaLblRut: TppLabel
              UserName = 'Label1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RUT:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 34396
              mmWidth = 7673
              BandType = 1
            end
            object plbl4: TppLabel
              UserName = 'plbl4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Versi'#243'n:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 3969
              mmTop = 10583
              mmWidth = 12700
              BandType = 1
            end
            object ContratoLblVersion: TppLabel
              UserName = 'ContratoLblVersion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'ContratoLblVersion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 16933
              mmTop = 10583
              mmWidth = 29337
              BandType = 1
            end
            object ContratoRUT: TppLabel
              UserName = 'ContratoRUT'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 13494
              mmTop = 34396
              mmWidth = 186267
              BandType = 1
            end
            object ContratoGiro: TppLabel
              UserName = 'ContratoGiro'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 13494
              mmTop = 38894
              mmWidth = 186267
              BandType = 1
            end
            object ContratoLblGiro: TppLabel
              UserName = 'ContratoLblGiro'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Giro:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 38894
              mmWidth = 7673
              BandType = 1
            end
            object ContratoAnexoRepresentantesLegales: TppSubReport
              UserName = 'ContratoAnexoRepresentantesLegales'
              ExpandAll = False
              KeepTogether = True
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              TraverseAllData = False
              Visible = False
              mmHeight = 5027
              mmLeft = 0
              mmTop = 58473
              mmWidth = 203200
              BandType = 1
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object pchldrprt10: TppChildReport
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.PaperName = 'Letter'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 279401
                PrinterSetup.mmPaperWidth = 215900
                PrinterSetup.PaperSize = 1
                Version = '12.04'
                mmColumnWidth = 0
                object ptlbnd5: TppTitleBand
                  mmBottomOffset = 0
                  mmHeight = 5292
                  mmPrintPosition = 0
                  object plblTituloRepresentantes: TppLabel
                    UserName = 'plblTituloRepresentantes'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Representantes Legales'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 2910
                    mmTop = 0
                    mmWidth = 42069
                    BandType = 1
                  end
                  object pln12: TppLine
                    UserName = 'pln12'
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Weight = 0.750000000000000000
                    mmHeight = 265
                    mmLeft = 3175
                    mmTop = 5292
                    mmWidth = 197644
                    BandType = 1
                  end
                end
                object pdtlbnd10: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background1.Gradient.EndColor = clWhite
                  Background1.Gradient.StartColor = clWhite
                  Background1.Gradient.Style = gsNone
                  Background2.Brush.Style = bsClear
                  Background2.Gradient.EndColor = clWhite
                  Background2.Gradient.StartColor = clWhite
                  Background2.Gradient.Style = gsNone
                  mmBottomOffset = 0
                  mmHeight = 9260
                  mmPrintPosition = 0
                  object plbl16: TppLabel
                    UserName = 'plbl16'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = [bpLeft, bpRight]
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = True
                    Border.Weight = 0.750000000000000000
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 9260
                    mmLeft = 2910
                    mmTop = 0
                    mmWidth = 197909
                    BandType = 4
                  end
                  object plblRUTRepresentante: TppLabel
                    UserName = 'plblRUTRepresentante'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'RUT:'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 4498
                    mmTop = 5027
                    mmWidth = 7673
                    BandType = 4
                  end
                  object plblNombreRepresentante: TppLabel
                    UserName = 'plblNombreRepresentante'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Nombre:'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 4498
                    mmTop = 529
                    mmWidth = 13494
                    BandType = 4
                  end
                  object Anexo1RepresentanteNombre: TppDBText
                    UserName = 'Anexo1RepresentanteNombre'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    DataField = 'NombreRepresentante'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 19315
                    mmTop = 529
                    mmWidth = 180711
                    BandType = 4
                  end
                  object Anexo1RepresentanteRUT: TppDBText
                    UserName = 'Anexo1RepresentanteNombre1'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    DataField = 'RUTRepresentante'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 13758
                    mmTop = 5027
                    mmWidth = 186267
                    BandType = 4
                  end
                end
                object psmrybnd5: TppSummaryBand
                  AlignToBottom = False
                  mmBottomOffset = 0
                  mmHeight = 265
                  mmPrintPosition = 0
                  object pln13: TppLine
                    UserName = 'pln13'
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Weight = 0.750000000000000000
                    mmHeight = 265
                    mmLeft = 2910
                    mmTop = 0
                    mmWidth = 197909
                    BandType = 7
                  end
                end
              end
            end
            object ContratoAnexoFormaPago: TppSubReport
              UserName = 'ContratoAnexoFormaPago'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ShiftRelativeTo = ContratoAnexoRepresentantesLegales
              TraverseAllData = False
              mmHeight = 5027
              mmLeft = 0
              mmTop = 64294
              mmWidth = 203200
              BandType = 1
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object pchldrprt11: TppChildReport
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.PaperName = 'Letter'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 279401
                PrinterSetup.mmPaperWidth = 215900
                PrinterSetup.PaperSize = 1
                Version = '12.04'
                mmColumnWidth = 0
                object pdtlbnd11: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background1.Gradient.EndColor = clWhite
                  Background1.Gradient.StartColor = clWhite
                  Background1.Gradient.Style = gsNone
                  Background2.Brush.Style = bsClear
                  Background2.Gradient.EndColor = clWhite
                  Background2.Gradient.StartColor = clWhite
                  Background2.Gradient.Style = gsNone
                  mmBottomOffset = 0
                  mmHeight = 39688
                  mmPrintPosition = 0
                  object pshp17: TppShape
                    UserName = 'pshp17'
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 21696
                    mmLeft = 2910
                    mmTop = 5292
                    mmWidth = 198702
                    BandType = 4
                  end
                  object imgEmisionNo: TppImage
                    UserName = 'imgEmisionNo'
                    AlignHorizontal = ahCenter
                    AlignVertical = avCenter
                    AutoSize = True
                    MaintainAspectRatio = False
                    Stretch = True
                    Transparent = True
                    Visible = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Picture.Data = {
                      0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                      00250000002308020000009CA55449000000097048597300000B1300000B1301
                      009A9C180000000774494D45DC070806032B24253B7E5B000003F84944415478
                      DAB5965928B45118C7DFB1670DA5148A5228E2CA25C285286B21C99628B2DE50
                      B2454AB266C9927D19234B498A1B910B37DCE04242F622BB61ACFF713EC7F9DE
                      6FCC3733EFCCB9787BE79973CEEF7D9EF37F9EE7883E3E3E38DD8FA7A7A78383
                      03272727912E78D853241291E7FBFBFBEEEE6E6B6B6B5353534747874E7814F9
                      F0F0303232D2D8D8B8B5B5151313939696A643FF363636EAEBEBC7C7C79F9F9F
                      D3D3D32B2A2AECEDEDB5CC23BB01B6B0B0909F9FBFBDBD0D4B666666434383B1
                      B1B1FC2F2DF2885B78E9EDED2D2C2CBCBFBFC77B5C5CDCC0C0C0CF890AE7B1EA
                      B8BDBD1D1E1E2E2D2DBDB8B8D0D3D34B4D4DEDEAEA8264F04E2608E5B1B0E3E3
                      E39A9A1A48117600B2B2B29A9B9B5F5F5F0D0C0CA8EB5AF08F7C3ED22B2F2F6F
                      7A7A9AC26A6B6B4D4C4CD8380BE2B19E9D9D9D41EB737373E4AFE4E4E4BABA3A
                      5B5B5B962494479180858585ADAFAFC351180303037166CECECE0A9708F5EFF2
                      F23222226265658518EDECECFAFAFA424242786114CA23AB2010E81E194D8C08
                      E0D8D858505090429250DEFEFE3E60535353C4626E6E5E5959891C67CF550B3C
                      5A18218A898909EEAB9AC0889C2B2B2B137D0D25CBD5E311984C26CBCECEEEEE
                      EEA66B2323237B7A7AACADADB95F8E8DCFA3937E9BCD46A9ADAD2D3737F7EDED
                      8DACF5F2F242DA4190CA497CFF68A955EE1CE407E7A45229513F34323A3A1A1C
                      1CAC6284E4FD10BBA0EADCDDDD415D9B9B9B515151FEFEFEB4E2B19FB2BABA1A
                      1B1B7B7272426018E5E5E538B6FF86F18787B0BCBCBC80545C5C7C7A7A0A938D
                      8D0DFA56626222902C0C18C0906A34B048EDC9C9490B0B0B1561F27D50D1F181
                      9D9D9D901CB53A38380C0D0DF9F9F9D18DA0119C99FC42F0BDAF9999D9E2E2A2
                      AFAFAFEA30396F6D6D0DD1BFB9B9614387111D1D8DB204C991ED9067E864A072
                      DF0980268794509E6D0A7868F6B8CF141414CCCFCFD395781A1A1AC269EC8849
                      878787010101984661A1A1A1333333FAFAFA6A39F7A3979D9D1D5C31969696FE
                      58BF364D48484033B3B4B44C4A4A1A1C1CA46B70C088A48F8F8FBA308EE40359
                      866E026FD097D90AB0BCBC7C7E7E0EDEE3E323B5E3BC214B0D601C2FDF737272
                      DADBDB911BD4C59494148944829B08BD09C02D48D4D4D4545D9202DEF5F5B58B
                      8BCBD5D5157F1273ED8070D0803483FD154FF2ECEFEF475459002B22FC8513D5
                      D8394E61BDB6B2B24252FEEBA2ABABAB582CF6F6F6D618A698575D5D5D5252C2
                      8B245E704341460A81F179246E7B7B7B6E6E6E24B5E970747444827A78786826
                      4B65FE41FA4545452D2D2DD439607027CFC8C8303232D2328FFC842B28CDE818
                      28D9F1F1F1B837200D38959B807AFE611C1D1DE18A87DCAFAAAA0A0F0F57A571
                      0BE2C1B3D9D9597777774F4F4F0D8AA492F1090BE9EBAEC5A48C1D0000000049
                      454E44AE426082}
                    mmHeight = 4498
                    mmLeft = 94456
                    mmTop = 19579
                    mmWidth = 7144
                    BandType = 4
                  end
                  object plbl14: TppLabel
                    UserName = 'plblEmisionDocumentosElectronicos'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Emisi'#243'n de Documentos Electr'#243'nicos:       SI               NO'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 4498
                    mmTop = 20902
                    mmWidth = 90340
                    BandType = 4
                  end
                  object pshp38: TppShape
                    UserName = 'pshp38'
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 10319
                    mmLeft = 2910
                    mmTop = 29369
                    mmWidth = 25929
                    BandType = 4
                  end
                  object pshp39: TppShape
                    UserName = 'pshp101'
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 10319
                    mmLeft = 25400
                    mmTop = 29369
                    mmWidth = 41540
                    BandType = 4
                  end
                  object pshp40: TppShape
                    UserName = 'pshp40'
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 10319
                    mmLeft = 66411
                    mmTop = 29369
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pshp41: TppShape
                    UserName = 'pshp41'
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 10319
                    mmLeft = 97631
                    mmTop = 29369
                    mmWidth = 38894
                    BandType = 4
                  end
                  object plbl10: TppLabel
                    UserName = 'plblTituloFormaPago'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Forma de Pago'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 2910
                    mmTop = 529
                    mmWidth = 26194
                    BandType = 4
                  end
                  object plbl11: TppLabel
                    UserName = 'plblMetodoPago'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'M'#233'todo de Pago:'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 4498
                    mmTop = 6350
                    mmWidth = 26458
                    BandType = 4
                  end
                  object plbl12: TppLabel
                    UserName = 'plblRUTTitular'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'RUT Titular:'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 4498
                    mmTop = 11113
                    mmWidth = 18785
                    BandType = 4
                  end
                  object plbl13: TppLabel
                    UserName = 'plblNombreTitular'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Nombre Titular:'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 4498
                    mmTop = 15875
                    mmWidth = 24606
                    BandType = 4
                  end
                  object MetodoPago1: TppLabel
                    UserName = 'MetodoPago1'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4498
                    mmLeft = 32808
                    mmTop = 6350
                    mmWidth = 126736
                    BandType = 4
                  end
                  object RUTTitular1: TppLabel
                    UserName = 'RUTTitular1'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 25400
                    mmTop = 11113
                    mmWidth = 174625
                    BandType = 4
                  end
                  object NombreTitular1: TppLabel
                    UserName = 'NombreTitular1'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 31485
                    mmTop = 15875
                    mmWidth = 169069
                    BandType = 4
                  end
                  object pshp42: TppShape
                    UserName = 'pshp42'
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 10319
                    mmLeft = 136261
                    mmTop = 29369
                    mmWidth = 38365
                    BandType = 4
                  end
                  object pshp43: TppShape
                    UserName = 'pshp43'
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 10319
                    mmLeft = 174361
                    mmTop = 29369
                    mmWidth = 27252
                    BandType = 4
                  end
                  object plbl36: TppLabel
                    UserName = 'plblPatente4'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Patente'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4498
                    mmLeft = 7408
                    mmTop = 31485
                    mmWidth = 13758
                    BandType = 4
                  end
                  object plbl37: TppLabel
                    UserName = 'plblClasevehiculo4'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Clase '#13#10'veh'#237'culo'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taCentered
                    Transparent = True
                    WordWrap = True
                    mmHeight = 8731
                    mmLeft = 39423
                    mmTop = 29633
                    mmWidth = 14817
                    BandType = 4
                  end
                  object plbl38: TppLabel
                    UserName = 'plblMarca3'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Marca'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taCentered
                    Transparent = True
                    mmHeight = 4498
                    mmLeft = 76729
                    mmTop = 31485
                    mmWidth = 10848
                    BandType = 4
                  end
                  object plbl39: TppLabel
                    UserName = 'plblModelo3'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Modelo'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taCentered
                    Transparent = True
                    mmHeight = 4498
                    mmLeft = 110596
                    mmTop = 31485
                    mmWidth = 14023
                    BandType = 4
                  end
                  object plbl40: TppLabel
                    UserName = 'plblSerieTAG2'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Serie de TAG'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taCentered
                    Transparent = True
                    mmHeight = 4498
                    mmLeft = 144198
                    mmTop = 31485
                    mmWidth = 22490
                    BandType = 4
                  end
                  object plbl41: TppLabel
                    UserName = 'plblFechaInstalacion2'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Fecha de'#13#10'instalaci'#243'n'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Tahoma'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taCentered
                    Transparent = True
                    WordWrap = True
                    mmHeight = 8731
                    mmLeft = 178594
                    mmTop = 29633
                    mmWidth = 19050
                    BandType = 4
                  end
                  object pshp10: TppShape
                    UserName = 'pshp10'
                    Anchors = []
                    Brush.Style = bsClear
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 3175
                    mmLeft = 93927
                    mmTop = 21167
                    mmWidth = 4763
                    BandType = 4
                  end
                  object imgEmisionSi: TppImage
                    UserName = 'imgEmisionSi'
                    AlignHorizontal = ahCenter
                    AlignVertical = avCenter
                    AutoSize = True
                    MaintainAspectRatio = False
                    Stretch = True
                    Visible = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Picture.Data = {
                      0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                      00250000002308020000009CA55449000000097048597300000B1300000B1301
                      009A9C180000000774494D4507DC0806032B24F406C508000003F84944415478
                      DAB5965928B45118C7DFB1670DA5148A5228E2CA25C285286B21C99628B2DE50
                      B2454AB266C9927D19234B498A1B910B37DCE04242F622BB61ACFF713EC7F9DE
                      6FCC3733EFCCB9787BE79973CEEF7D9EF37F9EE7883E3E3E38DD8FA7A7A78383
                      03272727912E78D853241291E7FBFBFBEEEE6E6B6B6B5353534747874E7814F9
                      F0F0303232D2D8D8B8B5B5151313939696A643FF363636EAEBEBC7C7C79F9F9F
                      D3D3D32B2A2AECEDEDB5CC23BB01B6B0B0909F9FBFBDBD0D4B666666434383B1
                      B1B1FC2F2DF2885B78E9EDED2D2C2CBCBFBFC77B5C5CDCC0C0C0CF890AE7B1EA
                      B8BDBD1D1E1E2E2D2DBDB8B8D0D3D34B4D4DEDEAEA8264F04E2608E5B1B0E3E3
                      E39A9A1A48117600B2B2B29A9B9B5F5F5F0D0C0CA8EB5AF08F7C3ED22B2F2F6F
                      7A7A9AC26A6B6B4D4C4CD8380BE2B19E9D9D9D41EB737373E4AFE4E4E4BABA3A
                      5B5B5B962494479180858585ADAFAFC351180303037166CECECE0A9708F5EFF2
                      F23222226265658518EDECECFAFAFA424242786114CA23AB2010E81E194D8C08
                      E0D8D858505090429250DEFEFE3E60535353C4626E6E5E5959891C67CF550B3C
                      5A18218A898909EEAB9AC0889C2B2B2B137D0D25CBD5E311984C26CBCECEEEEE
                      EEA66B2323237B7A7AACADADB95F8E8DCFA3937E9BCD46A9ADAD2D3737F7EDED
                      8DACF5F2F242DA4190CA497CFF68A955EE1CE407E7A45229513F34323A3A1A1C
                      1CAC6284E4FD10BBA0EADCDDDD415D9B9B9B515151FEFEFEB4E2B19FB2BABA1A
                      1B1B7B7272426018E5E5E538B6FF86F18787B0BCBCBC80545C5C7C7A7A0A938D
                      8D0DFA56626222902C0C18C0906A34B048EDC9C9490B0B0B1561F27D50D1F181
                      9D9D9D901CB53A38380C0D0DF9F9F9D18DA0119C99FC42F0BDAF9999D9E2E2A2
                      AFAFAFEA30396F6D6D0DD1BFB9B9614387111D1D8DB204C991ED9067E864A072
                      DF0980268794509E6D0A7868F6B8CF141414CCCFCFD395781A1A1AC269EC8849
                      878787010101984661A1A1A1333333FAFAFA6A39F7A3979D9D1D5C31969696FE
                      58BF364D48484033B3B4B44C4A4A1A1C1CA46B70C088A48F8F8FBA308EE40359
                      866E026FD097D90AB0BCBC7C7E7E0EDEE3E323B5E3BC214B0D601C2FDF737272
                      DADBDB911BD4C59494148944829B08BD09C02D48D4D4D4545D9202DEF5F5B58B
                      8BCBD5D5157F1273ED8070D0803483FD154FF2ECEFEF475459002B22FC8513D5
                      D8394E61BDB6B2B24252FEEBA2ABABAB582CF6F6F6D618A698575D5D5D5252C2
                      8B245E704341460A81F179246E7B7B7B6E6E6E24B5E970747444827A78786826
                      4B65FE41FA4545452D2D2DD439607027CFC8C8303232D2328FFC842B28CDE818
                      28D9F1F1F1B837200D38959B807AFE611C1D1DE18A87DCAFAAAA0A0F0F57A571
                      0BE2C1B3D9D9597777774F4F4F0D8AA492F1090BE9EBAEC5A48C1D0000000049
                      454E44AE426082}
                    mmHeight = 4498
                    mmLeft = 73554
                    mmTop = 19579
                    mmWidth = 7145
                    BandType = 4
                  end
                  object pshp9: TppShape
                    UserName = 'pshp9'
                    Anchors = []
                    Brush.Style = bsClear
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    mmHeight = 3302
                    mmLeft = 73025
                    mmTop = 21167
                    mmWidth = 4826
                    BandType = 4
                  end
                end
              end
            end
            object plblNombreCliente: TppLabel
              UserName = 'plblNombreCliente'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Nombre Completo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 4233
              mmTop = 29898
              mmWidth = 28067
              BandType = 1
            end
          end
          object pdtlbnd3: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 5556
            mmPrintPosition = 0
            object pshp22: TppShape
              UserName = 'pshp22'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5556
              mmLeft = 2910
              mmTop = 0
              mmWidth = 25928
              BandType = 4
            end
            object pshp21: TppShape
              UserName = 'pshp21'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5556
              mmLeft = 25400
              mmTop = 0
              mmWidth = 41539
              BandType = 4
            end
            object pshp20: TppShape
              UserName = 'pshp20'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5556
              mmLeft = 66411
              mmTop = 0
              mmWidth = 31485
              BandType = 4
            end
            object pshp19: TppShape
              UserName = 'pshp19'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5556
              mmLeft = 97630
              mmTop = 0
              mmWidth = 38895
              BandType = 4
            end
            object pshp18: TppShape
              UserName = 'pshp18'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5556
              mmLeft = 136261
              mmTop = 0
              mmWidth = 38364
              BandType = 4
            end
            object pshp23: TppShape
              UserName = 'pshp23'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5556
              mmLeft = 174361
              mmTop = 0
              mmWidth = 27252
              BandType = 4
            end
            object Patente1: TppDBText
              UserName = 'Patente1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 3704
              mmTop = 1323
              mmWidth = 21167
              BandType = 4
            end
            object Modelo1: TppDBText
              UserName = 'Modelo1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 98690
              mmTop = 1323
              mmWidth = 36777
              BandType = 4
            end
            object Marca1: TppDBText
              UserName = 'Marca1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 67469
              mmTop = 1323
              mmWidth = 29369
              BandType = 4
            end
            object SerialTAG1: TppDBText
              UserName = 'SerialTAG1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'SerialTAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 137054
              mmTop = 1323
              mmWidth = 36777
              BandType = 4
            end
            object FechaInstalacion1: TppDBText
              UserName = 'FechaInstalacion1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaInstalacion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 175419
              mmTop = 1323
              mmWidth = 25400
              BandType = 4
            end
            object ClaseVehiculo1: TppDBText
              UserName = 'ClaseVehiculo1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'ClaseVehiculo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 26988
              mmTop = 1323
              mmWidth = 38629
              BandType = 4
            end
          end
          object psmrybnd1: TppSummaryBand
            PrintHeight = phDynamic
            AlignToBottom = False
            mmBottomOffset = 0
            mmHeight = 105304
            mmPrintPosition = 0
            object plbl33: TppLabel
              UserName = 'plblUsuarioAtendidoPor2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Usuario atendido por:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 2646
              mmTop = 98425
              mmWidth = 33602
              BandType = 7
            end
            object plbl34: TppLabel
              UserName = 'plblFirmaConcesionaria2'
              HyperlinkColor = clBlue
              Border.BorderPositions = [bpTop]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Caption = 'Ruta del Maipo'#13#10'Sociedad Concesionaria S.A.'#13#10
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 11113
              mmLeft = 20638
              mmTop = 85196
              mmWidth = 61648
              BandType = 7
            end
            object plbl35: TppLabel
              UserName = 'plblFirmaCliente2'
              HyperlinkColor = clBlue
              Border.BorderPositions = [bpTop]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Caption = '            Firma del Cliente                '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 120121
              mmTop = 85196
              mmWidth = 56886
              BandType = 7
            end
            object Usuario1: TppLabel
              UserName = 'Usuario1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Usuario1'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              mmHeight = 4233
              mmLeft = 37042
              mmTop = 98425
              mmWidth = 13494
              BandType = 7
            end
            object ContratoCondiciones: TppRichText
              UserName = 'ContratoCondiciones'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'ContratoCondiciones'
              RichText = 
                '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil Taho' +
                'ma;}{\f1\fnil\fcharset0 Tahoma;}}'#13#10'{\colortbl ;\red0\green0\blue' +
                '0;}'#13#10'\viewkind4\uc1\pard\qj\cf1\f0\fs20 El Cliente recibe los si' +
                'guientes documentos: \par'#13#10'a) Condiciones Generales y Operativas' +
                ' de Uso de TAG y Cobro de Tarifas o Peajes (\f1\ldblquote Condic' +
                'iones Generales\rdblquote ), \par'#13#10'b) Contrato de Arrendamiento ' +
                'de TAG, Pacto de Habilitaci\'#39'f3n de Uso TAG y Obligaciones Anexa' +
                's. \par'#13#10'\par'#13#10'Se hace presente que, en el evento que el Cliente' +
                ' opte por la opci\'#39'f3n de emisi\'#39'f3n de documentos electr\'#39'f3nic' +
                'os, \'#39'e9ste autoriza el env\'#39'edo de dichos solamente por un medi' +
                'o electr\'#39'f3nico v\'#39'eda e-mail o mediante su publicaci\'#39'f3n en e' +
                'l sitio web <#URL_CONCESIONARIA>, as\'#39'ed como tambi\'#39'e9n se comp' +
                'romete al cumplimiento de las condiciones, en relaci\'#39'f3n a los ' +
                'documentos tributarios, en caso de requerirlo para respaldar inf' +
                'ormaci\'#39'f3n contable en conformidad a la normativa vigente.\par'#13 +
                #10'\par'#13#10'Ruta del Maipo Sociedad Concesionaria S.A., est\'#39'e1 autor' +
                'izada para cobrar una renta mensual por concepto de arrendamient' +
                'o del dispositivo TAG, por un valor de $ <#fee_Completo>. En cas' +
                'o que el usuario suscriba el env\'#39'edo por medios electr\'#39'f3nicos' +
                ' de la Factura y/o Boleta, la cuota es de $ <#fee_Completo-Descu' +
                'ento> (ambos incluyen IVA). \par'#13#10'\par'#13#10'Estos precios se reajust' +
                'ar\'#39'e1n anualmente en el mes de diciembre conforme a la variaci\' +
                #39'f3n del \'#39'cdndice de Precios al Consumidor (IPC) registrada ent' +
                're noviembre del a\'#39'f1o anterior y noviembre del a\'#39'f1o en curso' +
                ', dicho reajuste ser\'#39'e1 aplicable a partir del 01 enero del a\'#39 +
                'f1o siguiente.\f0\par'#13#10'}'#13#10#0
              Transparent = True
              mmHeight = 64029
              mmLeft = 3175
              mmTop = 3704
              mmWidth = 198438
              BandType = 7
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
            end
          end
        end
      end
      object ContratoAnexoVehiculos: TppSubReport
        UserName = 'ContratoAnexoVehiculos'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 23019
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt2: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object ptlbnd2: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 40217
            mmPrintPosition = 0
            object img3: TppImage
              UserName = 'imgLogo2'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                009C0000002F0802000000998434C1000000017352474200AECE1CE900000004
                67414D410000B18F0BFC6105000000097048597300000EC300000EC301C76FA8
                6400001E3C4944415478DAED9C075C1447DBC06F0FAED0BB74057B41A404C528
                F60A268A051B364404A5496FD27BEF0A0A82220611C5466CD8308A0D44B060EC
                200888D2EEB87EDFB37B709C08C42498F77D3F33FE7239F666A7FD9F79CAECCC
                225C2E87C56452A83404417068E20A0909898989E17889CBC1FE8730188C4FAD
                6D2422515A5A02C7859C5CDE75F8D2D2DA4E673064A5A50804027CA1D1E8DD45
                7D96B85CAEA80889402062F721EDEDED1D944E2121BCBC8C345E58087EC6F597
                10844E837F0C7EB124128144227FDE42AC310897D24165B139820DC0EA2543DB
                E0775A27ED536BBB84B8A8B8B828AEFF0AB1A2701D1D14369B032D141717FFBC
                225E163C0EC7696BEB8086138904111191CF33A02534377F6230592422415656
                1AAB8E5F25C2E1B03FB6B4C11518373C5E887747473B85831682409B09046151
                51D12FFAC8AB172EB06BEA1A3A3AA85C1C574C54444549814824F173224C06C3
                272CF97EE5133289C4E1A09700AA8C94C4D41FB4D72D5F2C292989350589DD9B
                9D7BFC9C869A72A4AFE3708DA15DD520F8A7CF5EB805C57F686ED9BCFA27AB4D
                66BEE12937EF559049448C05C21F56F8042AB37ED4F771DA86C3A17779852635
                B7B4C24FC673A67B3858A2B5F4C915C1BF7AFDD63D38B18342C5E3F190054A25
                1208F272D2063A137E5E384B71883C7A235A15527CBD343A351BDA8F558AE3D5
                4FA176AE30996B6BB98EC562ED0E4BBAFCDBFD31C387C605BBCAC9CAF4E6D42D
                43F05F6EC19903BF9C061E30263A13467B395A8A8B8BF5B410C137363507C5A6
                3D7F5503F945C9240F7B0B03DD893D0522F8C2B39712337E81DBA13D0E966B7F
                5E3C47F0D7A28BD72353B2019FB3F5869F16CE866BC74E9DDF7FB8505898D778
                2E7C91919634D4D75EB36CA1B494147FC0A117F9A72E9CB970ED754D7D5B0705
                AE898B89AA282ACC9B396593D94FA81C7039484747C7CAAD2E0F1FFF2E224286
                1640712C161B3EA1290B661A4607384B494AC09D8EDE11D9F9A787A9A9642506
                EA688DE5D771FB5EC56607DF4FAD1D96EB97067BDAAFB172BD547247944C8651
                26080BE311044A64B258808C4AA399CC9D9E95148420F8E48CDC90F84C111112
                34514D79484146B4B292623F438CBF535E6966E90A44E12F369B8D7D72586CB6
                0899347AF8303F67AB1FA7E8F1246FFFA163BE517B6188D91CB42BBC99D1DE41
                B55C6F1AE9E7C460D0D7DB785DBF5DAEA1A674FC40ACAAB2527F35C28DAE01B1
                078F9E95101361B2D930E9E2029C4C97CC17A4929153E01BB907E613348CC960
                86EF7658BF7249F7B0206C16DBD2C9FFFCD55B226432B59366326F7A5AF46E54
                DA786201B7E71EF78D4885F1F175DEBE6DC34AB8E61F95BAEFD0713299CCE6B0
                211BF411868E202C34638A6E6C90ABBC9C2CE4A1523B03A2F71E3B7309D42181
                4890101385F23A285D1AC578EEF4F0DD8E9212E22854F31DDE772B1E698D1D19
                E2614B26139FFCFE2AFD60C1AB9A3A8011B5DB61D5D245509C5B40ECA1636787
                A929EF8BF19D387E341FEABDF2CA6DCE41A0D360A6FABBED78F8E829DC083309
                6A4A39905F5BD7A03954C56AC30A90260683396298AAD6F831542AD5D229E0D6
                BD87B2529274260B147BA09BCD9AE5C6FD0DF1FD8A471B767AC3ED26F38D36AE
                5A02385FBE795774E9C6EDB24A3A83A9A632645FB4EFF8B123216FD691C2E0F8
                7DD04FBBAD6B664D33A0D3E93C09505755525755A6D168506F090A55F997B4F0
                01C408A07A87261D2E28521A220FD2D1F8E1E392F94689A19E783C8269003C74
                61BB4BD0B5D2B2E14355A1EF30D6C11E3B579B2EE60FCBE3A7CFB738FA7D6A69
                17171301A8D292E2D9898163468FE0673878F45440741A40F576B4DCBC76195C
                0B8DDFB7FFF009B014EEB69B7FD0D1FAFDE59B8CDC138FAA5F824573DCB6CE79
                C766C893B4EF70F4DE43C06FA486BA95F90A9D8963C17ADE29AB4A3F54F0AEBE
                0978395B9BDB5B997741BD5D5E65A8AF75FC403CAF5FBF9CF8D5233811641050
                F9BA587F0DD44D664B02DC77F247864EA7ADB00005F06CB29E566E6A189144E2
                FF5406906C77436B80D0C5EBA5D5CFDF982E9E1D17EC262C2CDCC7287743EDA4
                D16D36AF72B3B5E05DA650A97E11A985E7AED2E874F315C661DE0E081ECF83CA
                60B0F6C7FA2E983DAD57497F16EAC1FC33E3470F1F3B42A3A8F886829C4C7652
                D0A8111AE82D08FE6ED9C34DF6BE20BB4B16CCB874BDB4A9B925D4D3B60B2AA6
                BD4167F847A78D1A3E14B4DDE1E3E7DA299400176B0C1E9727167D42DD97731C
                0CE4A1E4205DED0970E5CA8DDB0E3E51E0CACC9CAA9F931A5657FF7E9D8DD7AB
                B7752A4AF27B22BC74268EE737B9F8DA2D27FF58706EC68FD6CC4B8FEC813A45
                4FEB78660C8219EDD3E7AED879478051DCB1D9CCD576CB9F81DAD5E896D656D0
                75554F9FEB4F1A7F203E404A4A92DFE1E894CCB8F4DC09A3871F4808084BCC3C
                7DA1445951FE706AC808CD610343DD66BEDCCB711B66A5D1917D5B5BB766BB47
                7D4393E650D59C94101565453ED48C38BFF9B37EEC2904CBFF67A1661F3D3D69
                FC682BF3E581B1FB9A9A3F05BA0115535E96E098BDC9078ECE9966B0EAE7F981
                B1E96DED1401A878069D6EE1E877F1FAED0DAB4CECB7AE05FC2FDFD41A19EA65
                25040A83BF86E51900EABE98DD530D74E1CACB576F3739F8BE7AF3CE688AEE91
                F4C833E7AFDAF944802D58BB6C6188B74357BF780DE672D7EFF0FCEDCE03D088
                B929A13D507F98340E6A251089BFBF781D9A9071F7C123511191CC38BFA99375
                070F2A1E5CCA0DB65EA5F7ABD69A2E8A0D74CDC93FED139E02C6C6DF65FB06B3
                9FFB7097FA870A39B7390514DFB8033EF9E13D217ADA1330A8FB41164D8D674F
                1C379AC36633992C156585C573A72188D05F803A7ED4F09470CFA0B874200408
                61B20A0B139A9B3FAEB5F67CF4EC8587EDE6297ADA16BBFCA16D82501F563DDD
                60E7D3DADE11EE6DBFC6D478D7EEC8A3A72E2AC9CB1E480CD49E30E60FA1F267
                2A3853AE81716DEDD49F1618A546FA2482EEDD73904C2406B8DAAC5D610CBE77
                7708824E1530C987F2CF82CF9111EBD70515F4A48484B8AC34F8BAB8D6B67610
                3D395969CB75CB2CCD57F09CC8C1827AEDE6DD1DEEA1600BE3039D972C9C5DFD
                FB4BF0B3DEBD6F9A3DCD00B2E1515782F3B550B1A13F72E21CD898030981D3A7
                E8F1A08203D5D9490303030DA17476EA6B8F030D0461C95F803A4A63E8E99C84
                43F96742133261400EC4FBC1709F387BC925200EC2B3A3FBA240E36D730EEC05
                357EEFC198BD3960C87353433486A9E79F3CE71D9602B1CDAEEDEB1DACCC0780
                CAB7A920916FDFD5A51D2C00FF067E8AF1DF656A32DF3F6A4FD62F274182E383
                5C16CF9B21E0B5A150E3D30E2567E681102785B87541AD7CF2BBA8A8088C0EC4
                8E401B5CF91D5BCCEC2CD7F39417DC330850B17242E3D29332F3268E1D519815
                278A45C3F65EE1C7CF1643A49519EFA7356ECC9F82EA11140F93008CF1C1C420
                4383493CA8100C802D5452407D1C1A833E6EA4A6CB8E8D42C284BF0015FC9193
                D971750D4DEB6DBC1B9B3F3A6D3777B2D964EF199677EAC29CE99373F7869594
                96818CF640C5A1D1ED56477FF0A1404982A30057DADADA965BB8001ED0A2FB63
                FDD0D00887EB136AE6919310580205C08445FF4C10D075A68BFC5C6D20CEF68F
                4CCDCA3B0D31715CA0B3C9FC99BDA0C6A666A766E73398CCBD11DE7CEFF7B1D6
                D811E1DE763069E2D272C0BD8448232ED0455F478B07C3D53F26A7A008A042B3
                B4C68DE243BD5F5165E914086EDE46B325817F00151ADABAC6CAA3FAC5EBB123
                358CE74D171602652674FD56D9DDF247E0EF78D859586F36C3D42FF76BA0B2D9
                2CB85E7ABF524A523C373574DC98913C9BCA64B241672E5930537085043EFF1A
                D4FC7D91F2F2B2EBAC3DAE9796194DD675B23677F28B7D5DF3CED769BBD5A655
                574A6E83F32108B5F4EE83AD4E814C2673DA141D43BD89BCD8AAF0D7CB6F6AEB
                C1B13A10EF6FA0A73D30547151112027264AD65057996B3479D9E239A825C6E1
                62F76427EC3B02F36DB793D506B39F7AA95FD780988233C5E0DB1E4A09FECC51
                3A91857ABF278B8ADD831360CAFEBC70665298879090305CF40E4DC8CA3B2327
                2B951CE23EE347033ED4925BF760A6824C6DDFB8DCC3DE7260A897AEDDB4F78E
                44C599DAD941A1A28B125C1CC4C74005F4C6645D2D70707A2FCDF409154B1555
                4F2C1CFC3F7C6A01050BEA17AAE0434D8DF032993F630047292F3D424971488F
                F470B9FD4105FC90333DFB68486226C460208E0F1E3D939410CB4A080031BA5C
                72DBFE73A89149197BB28F91C924B062709D37E412E2A212E26210DBD86C5CE9
                6A67D11F549E4D85A905734958082F272385C7069FD7FEBCC25FDD0213400FAD
                34991BE1E724E828B5B777ACD9EE5EF9E4B9BAAA2274AD07EA64DD0947D32321
                F6A07576AEB7F12C7FF44C424C2C2B3140571B759DD3B28F46246741DF572F9D
                1FEA658FC7566D40517B8726E69DBC00BE094CEBA5FC45937EA07A06C51F3EFE
                ABA282ECECE906A26474AD03C156276EDFAF7C5D53472611C11341ABEB07AACD
                A6556E765D21CDAB37353E61C9A565554C16D3D56693DD36D452F0BD5FB0110B
                E7F41BD268AA291FCB8C969793FB8C7A3F508FEC0D83395DF5B87AB3835F3BA5
                1308818A9B33DD203DC60FA6452FA8144AC7EA6DEE95D52F46690E9D663089B7
                9806D93AA8D4925BE54D1F3F698D19014A1B0CFC005021A2C514E4E70D43F04F
                AA9F8323DDF4B165889C4C4290ABA1810EBFF50969392959F9341A1D5CAAE430
                AF3EA042AE8C9C82A0B87D0882DF686612E086469FCF5FBED968E753DFD00C0E
                82F1DCE940052E42978A8A6F428BB5C60CCF4E0A5690971B006AFDFBC68D76BB
                1F3F7B397FC694ACC440DE1A262FED3990179E740064D01A04198DA03802ABA4
                5D50592C3684BC738DA6804FFBB6AEA1E4D6FD5735757071DAE449C9619EBC05
                9703B9852109FB61DC4D4DE6E8688D653298388CC1C4B1236108E8349A8553C0
                ADBB1530037E5E384B52421C8AA2311810B718CF37EA599BED0B2A93C1B07209
                BE7AF32E0C3A8DCE08F6D8016E2D6F0404A1165FBFE5E01DD54EA53AA13ED106
                7E07A1A2ED2E41E7AEDE9492104F08769B3BC3F08F439ADEBE0502ADF38B4CC9
                C054B48AA2C20A93B963476BD2E90CB0EB10498366559097498FF2D19B34015D
                585FBEC5B9B4AC12D4EFD99C241E548890566FF778F5B656435D352F2D7CC4F0
                6170F168E1B9D0C40C08B4C14AA06B765C1C9BC301121026867AD91A19FED063
                0E11FCA79616D3CD4EE07FFDA003B212212525957FEABCAD6738EA2B79D96E5D
                BF5CB0C10F2A9F98EFF4067FC460D20450773232D282127AB7BC728585337C83
                EA180C066FF0D1557219A91953F53CED2DD45494BB97090BBCC29345C9243007
                E0FDF2969EA1B7E0B3642606B2984C886B61A6826E04A1466D1D826B6BEF80D1
                39BC270C4163240E1FAAB36FF4FE2385A335878243A7AAA20497F7A3EB82A9F0
                B39AB24241668CBAAA0A5CBC70E5372B9720801A1BE0BC7EE51217FF18000323
                9EBB37545760710013B8E33EE1A9543022EB9745FA3967E69EF00C4904A8109F
                806D860CBE11297BB28E62214DB0D1D41FFA0CD95B3EB57884245EB8560A8205
                4A008F4760C841E1090B0969A82BC350982C980514103A9D1E959C5556F9444F
                7B9C87BD056F351C0AD8939577E5C65D7131117BCBB5D8E2053A6AA5772B4EFC
                7AF9C5EB9AB60E0A3448465A0AF489D9B205A3866BF4D2609D9D9D5129D9954F
                7FD79B380E9C0B12897CE2ECC5DC82737272D29EF65B86A9AB093A6F2C262B32
                39ABBCEAA9B2A282DB8E4D6A6ACA8250618AC7A61D6A6BA3805BC8CB0FD8D454
                8618EA6B1BEA4FC42158148402C4DF7B50B53FE7044041703D4F69A0FF33A7EA
                6F5AB314BA9079F8C46F772B4824025F13C0281B4D9EB4D57C654F888CF91D39
                F9670ACF5D012DEA61BF450A7DAA81AB7D571F9992F5BEB1199C17AB0D2B113C
                9A0D5462CCDE43204376166B26EB6B472665C2F4D09930DAC3612B914014EC05
                DC1E9E98D9F0E1A381EE04375B8B5B77CBF7661F83FA206E447D141CEEF8998B
                F9A72FC254065F7D64AFF1141858D03780E0DAADB2FA86262A9506BC64652427
                8E1B05A2892E7861BD00D4A0EBB8E07C82FCA28EA240C768B44E213C9E402408
                7A5938CC89A550C1BA20E0E0743FA4FB72D100E92A9644C6DC4F3481AE00BF0E
                35FE5F3EC6E27240DE41E2B0EA7AAF3F60C6850D06180A411FFE08A8EE2FCD21
                97D3FBD11BBA4CC6EF17978D13408E760D8FEFED72635241A7D189442282E78F
                09DA486CA044042400CF62A2CA83802164B339E0F79245C8D848F6EE2387CDA2
                33182402110FD289E0994C0614432012F91A824EA7A14F418484FB262A301AD0
                80E6E64F20AF60B0A5A5C4454444052920D0929E31FA6C8090EECE7F0EACE73A
                AE8F5FFBA89EEFA475DFD86F2081FBE35FBFBED23EF3F76E7CFFA5F5D9DAEE01
                FDC25BC6F1DDFEAFEA45F78A69DF6D1B80E8D70D0806F56F264CF509FCCD19F0
                F9F3BFE99BA7BF0D15416A5AA9A5756DBCBF203E99A62AAD262532D04E867FD3
                374E7F1F2ABEBCBE25ABB20EFB8E6371B8DBB4D57494A5BE4A87FC9BBE4D1A04
                A80F1B5A8F3C7E8F7D47A16E9CA0325151F25FA8FFC13408501FBC6FC9A9C2A0
                E2712C3667CB44B5494AFF42FD4FA641805ADDDC51F4B219FB8E6373B84B472A
                8C9215EB2B76EED3F3FCC6E9BB94AD41F07E996C76279DC5FB0E9EB528519840
                10EEED0077FBFD140A1522B96F4E97CB259208A2A2BD36BA7E2F691066EAED7B
                15E14999E84206826FEFA0B8DA6D5E306B9AE0620A7CBC6F6C3C7FE556496959
                7DC3076C47E037872A242CA4AAACB068F6B4C5738D4444C8DF15D741807AE1CA
                4D1BF7106C2912DFD2DA961CEEB966D9628175545C6151717266DE8BD7B5783C
                823EFF45FE1125CCE5B2B105B339D327EF76B61AAAA6F2FD701D04A8C5D74B1D
                7C22BBA0B6B5C705BAACFC6901368228BFFD3905D1A907994C968828F91FB7A8
                E862777B3B65BAA16E729887AC8CCC77C2F59B41C51E005CB971DBCE2B028812
                89E8B37B168BDD893E1EF9E6230B0A418444424D3BB61ED2D141B1B558ED8A6E
                2FE57E0FAB22DF0E2A8ECEA05BEEF2BF711BDDB7087F32184C29297183491364
                A425B9DF72641104A1503AEF553C7EDFD4CC3B0042A333D494871C4C0A52FF3E
                94F03784FAB0EAE956E740D07E3063E80CA686BA72A0DB8E29FADAC83F6253AB
                1E3FF30A4BAE7EFE1A9404B48D46A3A7847B2E9C33FD5FA85F5340BF50CF5FFE
                CDD137125C62708EA89D34876DEBD00D9268FA0714202A3759BF148626641009
                04102370CB833C766E58F5D30027B1B0A671FA7EEA3270EAB9EBF33F05BC45FE
                6F3D0FF270BD9E5F0D9A69F88650AF8241F58E84004648488842A52D5B342B36
                C8F5ABCBE50E7CB8F16B82A2FC53177CC29309C2C200B58342F577B5DEB47A69
                57E15D95F04FB121EF1B3FB4B6B68F19A5F9B6A6AEE9638BFE2474DF4253D347
                90063299585E554D2408C33FF8534579C8284DF5078F9EC177F012C68C183676
                F48807954F6AEB1B44C8243DEDF1605FEE573C86EBBC8372743AA3FAC51B5054
                35EFDE0FD750873CBCD3822C365B6B1CBAD996C1643E7BFE6684861A1A7AFD97
                43ADADABDFB0D3A7B6BE110C1BB60306315D3C6BC982590A7232BC87E65F6044
                41494A88CBCA741FD9ECFFA9248341FFF8A9153B56DB97447071CA8A0A672F95
                0842458F61ADFAE92395CEBB033E65C904213CC22BF3E8C97377CAAAA2035CA2
                530EECCB399E93126AA0371182B1DFEE3CD8BA6ED9D98B254F5FBC7E5BFBDEC8
                5057576BAC949444504CDABC1986E0F4C195495A632C1CFC476AAA7339DC4F6D
                6DDE8E96E0F04F33D059B574210E3B125370FA9287BD8557685280AB8DA6863A
                8D46DB62EF4BA1527F498F1215156968FCE01614EFEFB25D7398FAA058876FEB
                FD06C5ECDD77F8047082A163B339605925C5C5789E703F890BDA72F488610B67
                FDB868CE740909B12F9F51D7BCAB3F73E1FAB59BF7DED635801AF8D2428300B1
                582C5FE76D440271975F340F2A85420DF3DCA16934FBC4E37764213C7A4A5008
                6FA5A33A448CCC53B3C74E9DBFF7E071B8EFAEE894ACDFEE948FD0508FF2732A
                2ABE71F15A693CB627FBE2D59BE7AFDC8A0E7086EFD76FDD2D38733921C49D57
                6343D307AF90C4187F67696929334B679BCD66303BCF5C2C490EF7825FC313F6
                0F919701C0B61EE101AED61AC3D44AEF96FF5278BEA383BAC16CC9CC69931B9B
                3E788624ED76DAA63154EDBF1B2A6657DE37345ABB859455564B8A8BF236A300
                DA815D5F50BB2C261B3EA7FEA0ED61B765E2F8318286E7D4F9AB09E9879FBFAA
                01E70BB47A9F2A18A03259AC306F5B29090947DF2841A8AA3FCECAAFAAE9822A
                8CB7D71FAA24DE05B5E0F405801AB6DB313C21435141B6E251F532E339501408
                10CF6A9CBB7CE3D2D5D2E84017F87EEBDE83C8A4ACD54B1790C9A4D9D30CA047
                F6DE1126F38C080442F1F55B6E761662A222302691BB1D41153B78477A396E95
                9596822B6002801C60D61E3792C16297573E0970B76DFAD00C507D7659FE2F40
                C57E7DF6E2B54F58F2EDF22AA2B030F4B96BF3D81F2528AD9D42D550534E0C71
                9BA4358E772EFCC8F1B381B1FB2034121DD0F6F0A0067BEC901013EB05557DDA
                AC638F6AF933D5564F5DF10BA8C1B1695A63472A0D91CB395634C350F75EC593
                486CE7F4AFC52597AEDD8EC1A096DE7B109290095E02405D34FB471030905D75
                15A527CF5E1ACF9D6EBD65350E3D7D1403AEBEA6BA0A94131BE8FCE1638B8B7F
                6CB0A7ADACB4E44E8F50472B73069311999C9D11EFCF66B15D03E3FF77A06219
                5ADBDAB28E9C3A5B5C525BD740A17672B9FD7839D8048629482691F098A9034F
                447FD2B88C587F506B15954FB638FAB75328646C132B7819E83A069BF365511C
                360A35C67F97BCAC8CC3EE4841A8CA5367E63E7C2B228C07F000D579F2306509
                912FA10E1FA6B66E8589774802A804B005419E76BDA082FA3D517435AEDBEF03
                F5EB1D9A14E5BBABE9C3A78098B4980067254585A28BD72EDFB82B232501B676
                B5A931E4710F8C8FF1777A5CFD223821E3079D0950ED9DB24A179B0D46867A0E
                3E512085D8A1814178FCF0EDA1E2BA6C615373F3FD078F6BEA1AE80C469F54D1
                9D7E1CEEC327CFC1A4F1DE4F009F9D7446B897EDAAA58B40F0F34E5E9494401F
                BCC064959692986334595D59918BEBADCEB9E87E420E4CA027BFBF760F8EE73B
                4A21EE36B38C173D6A6C1542F745E38410C44049528C28CC6B61DE895F6F9755
                C606B9F986276B0E55DDB2CE14E26C332BF739D30D52237D20CBE90B57CF17DF
                4C8E40CD6449E9BDB8B4C34EDBCDA1E4A1AACA62A2643BEF88608F9DE0E9EC74
                0FD19F38CEC27C794B4BAB85A31F08715662A0B292E2FBC62667DF98502FDBF4
                83C7C68D19819DD140728E9EAA78F4CCC7C9D2D63362D9E2D92A4A0A8AF2B2E0
                81FFCDD86610A05EBA76CB7177141F6A7C90EB0AC1D723F0897DDD9319F0EF0F
                E69D8C4FCFE515D841E9042D17ECB97389B97DDDFB2670B260160E91970DF7B6
                FB71B2DEC045E5159EF38D4CED0969DC6C36A147600553CF19A3BB65552FDFD4
                AE365D54587419A6F874435DB87A20F704E80C6CCF300EAC6CD5E3E7EB5799C0
                F7B7B575E02113D1B7D1306718EA8159CDCE3BB974D16C0505F99B77CA5FBCAE
                C10E3021878E9E6A6D6FB7DDBA1E240DE2A582339740448A4BEE2C5930437188
                0294D3D0D874FC4CF132E3D927CF5D7DF7BE111AA3A73DCED464DEDFDCBC3708
                50CF5FBEE1E4178343F7CFA250C10A9A1ACF1B70E7EAC074D143036BAD3DAB9F
                BF068B0550D177BAECB234DFE1FDA9B51D94735B3BC57CA57188973D967980AE
                23F9A7CEFB84A7F0A182E7B9118D53BF6EF1417017A7E01A421FEB099870E0F0
                9FAD57F4714BF70AC39765E23E3748FF0DEAF746E9FDEDAEC1A842C3E341DB80
                4F18EA6D8F7CE55A4C5FE9F6BD0AD0666D1D14E001052E986918E4BE63D916A7
                E68FAD009546474FBFA445F9C8A1F16EBF097430D8B0134597492422B6A2448D
                0B74C626C1202DDCF045B3679B38B7EF6C3D4B1CDDF8B85F644070DDB007A16D
                83001502C75596AE30478101B6119FBB78CE348832E56424FFD4031950746C36
                B7EAE9F3C3C78B20CCE785B3E0E0ECB236B7B35CB771A7F78D3B0F2054C0A18F
                0A98537427AC5832575D5509C15E25F57939F8E64FADE7AFDC2CBA7C03871D3A
                038D2D42266527046AF35F16F4FF3A0D0254369BED11189F77EA829424FA6630
                0009834E2212B063397F4AEE503C0CECA81A8F28F8B74A0AB23929E821FB9345
                C54EFEB1DDC5E268343ABA6A87BE4A0AF9A216100E36AF0DBCB72F81C65E387B
                6A52A807093CE77FA17E5D19F8C7D5CFB73905D6377EE0CD24F4303138B27F49
                9120BCDD11E8F332BA105E28C473E70A6CD1115D240A4FCE29F8554C4C4418E3
                8ABDCAAB9F42D0C7F3086FB189DA4983D0300D3BE3F73D10C50D0E54B418FC95
                1BA5BBC3F7BCAD7B0F71249120FC979FAFA16F486330E94CA692829CB3F506B3
                65E8ABB9F86F76894AC9CA3B79013D6644240A0B0B0D500B206732D10349CA8A
                F27ECED68BE7197D710AEAFF6D1A24A8D8CE1598AF19B985A5F71F36367D042A
                7F962A6FBCC944A2D21079437DAD8D663FA32F9710F46BB0D7EC141597E49FBA
                F8A8FA05184EFE21D45EE5406BC0882A2AC81AE84CD8BCE667EC61C8F7421437
                78507B22D1DA77F56FDFD583B7F997B60C72C1300F535346DFB281FDD9E7B146
                98B82F5ED7A207343B697D696034C095961457535552E6ADD17C1FBB58F8E9FF
                003D3A71485F0DDF330000000049454E44AE426082}
              mmHeight = 13229
              mmLeft = 148432
              mmTop = 2646
              mmWidth = 51329
              BandType = 1
            end
            object plbl22: TppLabel
              UserName = 'plblNumeroCuenta2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero de Convenio: '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 3969
              mmTop = 4763
              mmWidth = 47096
              BandType = 1
            end
            object plbl23: TppLabel
              UserName = 'plblTitulo1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'CONTINUACION ANEXO I CONTRATO DE ARRENDAMIENTO DE TAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 31485
              mmTop = 21960
              mmWidth = 147373
              BandType = 1
            end
            object NumeroConvenio2: TppLabel
              UserName = 'NumeroConvenio2'
              HyperlinkColor = clBlue
              Border.BorderPositions = [bpBottom]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Caption = 'AnexoVehiculoNumeroConvenio'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5027
              mmLeft = 52123
              mmTop = 4763
              mmWidth = 67998
              BandType = 1
            end
            object plbl31: TppLabel
              UserName = 'plbl31'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Versi'#243'n:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3429
              mmLeft = 3969
              mmTop = 10583
              mmWidth = 10202
              BandType = 1
            end
            object AnexoAltaVersion: TppLabel
              UserName = 'AnexoAltaVersion'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'AnexoAltaVersion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3440
              mmLeft = 14552
              mmTop = 10583
              mmWidth = 21696
              BandType = 1
            end
            object pshp24: TppShape
              UserName = 'pshp24'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 10319
              mmLeft = 2910
              mmTop = 30163
              mmWidth = 25929
              BandType = 1
            end
            object pshp25: TppShape
              UserName = 'pshp25'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 10319
              mmLeft = 25400
              mmTop = 30163
              mmWidth = 41540
              BandType = 1
            end
            object pshp26: TppShape
              UserName = 'pshp401'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 10319
              mmLeft = 66411
              mmTop = 30163
              mmWidth = 31485
              BandType = 1
            end
            object pshp27: TppShape
              UserName = 'pshp27'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 10319
              mmLeft = 97631
              mmTop = 30163
              mmWidth = 38894
              BandType = 1
            end
            object pshp28: TppShape
              UserName = 'pshp28'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 10319
              mmLeft = 136261
              mmTop = 30163
              mmWidth = 38365
              BandType = 1
            end
            object pshp29: TppShape
              UserName = 'pshp29'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 10319
              mmLeft = 174361
              mmTop = 30163
              mmWidth = 27252
              BandType = 1
            end
            object plbl24: TppLabel
              UserName = 'plbl24'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4498
              mmLeft = 7408
              mmTop = 32015
              mmWidth = 13758
              BandType = 1
            end
            object plbl25: TppLabel
              UserName = 'plbl25'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Clase '#13#10'veh'#237'culo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 8731
              mmLeft = 39423
              mmTop = 30163
              mmWidth = 14817
              BandType = 1
            end
            object plbl26: TppLabel
              UserName = 'plbl26'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4498
              mmLeft = 76729
              mmTop = 32015
              mmWidth = 10848
              BandType = 1
            end
            object plbl27: TppLabel
              UserName = 'plbl27'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4498
              mmLeft = 110596
              mmTop = 32015
              mmWidth = 14023
              BandType = 1
            end
            object plbl28: TppLabel
              UserName = 'plbl28'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Serie de TAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4498
              mmLeft = 144198
              mmTop = 32015
              mmWidth = 22490
              BandType = 1
            end
            object plbl29: TppLabel
              UserName = 'plbl29'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha de'#13#10'instalaci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 8731
              mmLeft = 178594
              mmTop = 30163
              mmWidth = 19050
              BandType = 1
            end
          end
          object pdtlbnd4: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 6350
            mmPrintPosition = 0
            object pshp34: TppShape
              UserName = 'pshp34'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 2910
              mmTop = 0
              mmWidth = 23813
              BandType = 4
            end
            object pshp33: TppShape
              UserName = 'pshp33'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 25400
              mmTop = 0
              mmWidth = 41540
              BandType = 4
            end
            object pshp32: TppShape
              UserName = 'pshp32'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 66411
              mmTop = 0
              mmWidth = 32544
              BandType = 4
            end
            object pshp31: TppShape
              UserName = 'pshp31'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 97631
              mmTop = 0
              mmWidth = 40217
              BandType = 4
            end
            object pshp30: TppShape
              UserName = 'pshp30'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 136261
              mmTop = 0
              mmWidth = 41010
              BandType = 4
            end
            object pshp35: TppShape
              UserName = 'pshp35'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 174361
              mmTop = 0
              mmWidth = 27252
              BandType = 4
            end
            object Modelo2: TppDBText
              UserName = 'Modelo2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 98690
              mmTop = 1323
              mmWidth = 36777
              BandType = 4
            end
            object Marca2: TppDBText
              UserName = 'Marca2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 67469
              mmTop = 1323
              mmWidth = 29370
              BandType = 4
            end
            object Patente2: TppDBText
              UserName = 'Patente2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3429
              mmLeft = 3704
              mmTop = 1323
              mmWidth = 20638
              BandType = 4
            end
            object ClaseVehiculo2: TppDBText
              UserName = 'ClaseVehiculo2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'ClaseVehiculo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 26723
              mmTop = 1323
              mmWidth = 38628
              BandType = 4
            end
            object SerialTAG2: TppDBText
              UserName = 'SerialTAG2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'SerialTAG'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 137054
              mmTop = 1323
              mmWidth = 36777
              BandType = 4
            end
            object FechaInstalacion2: TppDBText
              UserName = 'FechaInstalacion2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaInstalacion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 3440
              mmLeft = 175155
              mmTop = 1323
              mmWidth = 25400
              BandType = 4
            end
          end
          object psmrybnd2: TppSummaryBand
            AlignToBottom = False
            mmBottomOffset = 0
            mmHeight = 19844
            mmPrintPosition = 0
            object plbl30: TppLabel
              UserName = 'plblFirmaCliente3'
              HyperlinkColor = clBlue
              Border.BorderPositions = [bpTop]
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = True
              Caption = '            Firma del Cliente                '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 136790
              mmTop = 13758
              mmWidth = 56886
              BandType = 7
            end
          end
        end
      end
      object ContratoMandatoPACPAT: TppSubReport
        UserName = 'ContratoMandatoPACPAT'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 31750
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt7: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object pdtlbnd9: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 157427
            mmPrintPosition = 0
            object ContratoTextoMandatoPACPAT: TppRichText
              UserName = 'ContratoTextoMandatoPACPAT'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'ContratoTextoMandatoPACPAT'
              RichText = 
                '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fswiss\fp' +
                'rq2\fcharset0 Arial;}{\f1\froman\fprq2\fcharset0 Times New Roman' +
                ';}{\f2\fnil Tahoma;}}'#13#10'{\colortbl ;\red0\green0\blue0;}'#13#10'{\style' +
                'sheet{ Normal;}{\s1 heading 1;}}'#13#10'\viewkind4\uc1\pard\nowidctlpa' +
                'r\qj\lang3082\b\f0\fs20 Identificador de Servicio <#CUENTA_COMER' +
                'CIAL>\par'#13#10'\pard\nowidctlpar\b0\f1\fs24\par'#13#10'\par'#13#10'\pard\keepn\n' +
                'owidctlpar\s1\qc\tx922\tx1064\tx2340\tx3332\tx5317\tx6451\b\f0 M' +
                'ANDATO PAGO AUTOMATICO \ul\b0\f1\par'#13#10'\pard\nowidctlpar\ulnone\f' +
                's20\par'#13#10'\par'#13#10'\pard\nowidctlpar\qj\f0 Por el presente instrumen' +
                'to, \ldblquote el Mandante\rdblquote , el cual se individualiza ' +
                'm\'#39'e1s adelante, otorga el mandato para que por intermedio de Tr' +
                'ansbank S.A., del Banco concentrador o, a trav\'#39'e9s de este \'#39'fa' +
                'ltimo, del banco que ha elegido en el presente mandato, se cargu' +
                'e a su tarjeta de cr\'#39'e9dito o cuenta bancaria, respectivamente ' +
                'seg\'#39'fan aplique, las cantidades correspondientes por los cobros' +
                ' de servicios que \'#39'e9sta le presente, mediante el cargo en la c' +
                'uenta bancaria que se se\'#39'f1ala al final de este instrumento.\pa' +
                'r'#13#10'\par'#13#10'En caso de sustituci\'#39'f3n, revocaci\'#39'f3n, vencimiento o' +
                ' reemplazo de la tarjeta de cr\'#39'e9dito o el cierre de la cuenta ' +
                'bancaria seg\'#39'fan corresponda, me obligo a informar por escrito ' +
                'a las concesionarias seleccionadas en el presente instrumento o ' +
                'sus operadoras, de dicha situaci\'#39'f3n. No obstante, para los cas' +
                'os de tarjeta de cr\'#39'e9dito, autoriza a Transbank o al emisor de' +
                ' la tarjeta, seg\'#39'fan corresponda, a informar la nueva fecha de ' +
                'vencimiento, el nuevo n\'#39'famero y/o tarjeta asignada y que los c' +
                'argos respectivos se efect\'#39'faen en ella. Asimismo, en caso de d' +
                'etectarse una tarjeta que reemplace o sustituya a la elegida en ' +
                'este instrumento, autoriza a Transbank S.A. a cargar las cantida' +
                'des y conceptos se\'#39'f1alados en cualquier otra tarjeta de cr\'#39'e9' +
                'dito emitida a su nombre y operada por Transbank S.A., as\'#39'ed co' +
                'mo tambi\'#39'e9n a informar dicha tarjeta a <#NOMBRE_COMERCIAL_CONC' +
                'ESIONARIA>.\par'#13#10'\par'#13#10'El mandante declara conocer que el mandat' +
                'o para cargo en Cuenta Bancaria, constituye una instrucci\'#39'f3n d' +
                'e pago \'#39'fanica y exclusiva con el banco a quien se le otorga el' +
                ' mandato, liberando desde ya a <#NOMBRE_COMERCIAL_CONCESIONARIA>' +
                ' y al banco, de toda responsabilidad que pudiera derivarse de la' +
                ' suscripci\'#39'f3n err\'#39'f3nea de \'#39'e9ste. <#NOMBRE_COMERCIAL_CONCES' +
                'IONARIA> proceder\'#39'e1 a solicitar el cargo de los servicios que ' +
                'en virtud de este instrumento se autoriza, s\'#39'f3lo una vez que e' +
                'l banco acepte el mandato e informe de este hecho expresamente a' +
                ' <#NOMBRE_COMERCIAL_CONCESIONARIA>\f1 .\par'#13#10'\lang2058\fs24\par'#13 +
                #10'\lang3082\f0\fs20 De acuerdo al banco o tarjeta de cr\'#39'e9dito q' +
                'ue elegido, asume el compromiso de mantener en la cuenta se\'#39'f1a' +
                'lada procedentemente, los fondos necesarios, incluidos los de su' +
                ' l\'#39'ednea de cr\'#39'e9dito, si la hubiera, para cubrir cada uno de ' +
                'los cargos que se hagan en virtud de este instrumento, como as\'#39 +
                'ed tambi\'#39'e9n los impuestos y los gastos que pudieran existir, l' +
                'iberando de toda responsabilidad, en especial por los d\'#39'e9bitos' +
                ' que realice o si el banco o tarjeta de cr\'#39'e9dito no recibe de ' +
                'la empresa oportunamente la informaci\'#39'f3n del monto a cargar.\f' +
                '1\par'#13#10'\par'#13#10'\f0 Acepto y declaro conocer la facultad de Transba' +
                'nk S.A. y/o de Banco concentrador o del banco elegido, de absten' +
                'erse de efectuar los cargos informados en caso de que mi tarjeta' +
                ' de cr\'#39'e9dito estuviese bloqueada o mi cuenta bancaria cerrada ' +
                'o sin fondos para aceptar cargos por cualquier causa. Asimismo, ' +
                'he tomado conocimiento de los riesgos asociados al atraso o no p' +
                'ago del precio del servicio y dem\'#39'e1s conceptos recibidos de pa' +
                'rte de <#NOMBRE_COMERCIAL_CONCESIONARIA>, en especial los que se' +
                ' originen en las sanciones, infracciones, multas o partes que se' +
                ' curse, libera a Transbank S.A. y/o Banco concentrador o el banc' +
                'o elegido, de toda responsabilidad frente a dichos hechos y renu' +
                'ncia a cualquier acci\'#39'f3n en contra de estos \'#39'faltimos derivad' +
                'a de esta instrucci\'#39'f3n.\f1\par'#13#10'\par'#13#10'\f0 El presente mandato ' +
                'se regir\'#39'e1 por las condiciones generales de cada instituci\'#39'f3' +
                'n, a disposici\'#39'f3n del p\'#39'fablico en cada una de las sucursales' +
                ' y dejar\'#39'e1 de tener efecto por voluntad del mandante o de la i' +
                'nstituci\'#39'f3n financiera, o por el cierre de la cuenta, sin perj' +
                'uicio de lo dispuesto en el art\'#39'edculo N\'#39'b0 2163 del C\'#39'f3digo' +
                ' Civil (art\'#39'edculo que trata causales de terminaci\'#39'f3n de mand' +
                'ato). El mandante puede revocar el poder comunic\'#39'e1ndolo por es' +
                'crito, dentro los primeros 20 d\'#39'edas de cada mes, a la instituc' +
                'i\'#39'f3n financiera, revocaci\'#39'f3n que tendr\'#39'e1 pleno efecto a pa' +
                'rtir del cobro del mes siguiente al del aviso del t\'#39'e9rmino. En' +
                ' caso de cierre de cuenta bancaria o tarjeta de cr\'#39'e9dito, el m' +
                'andato termina ipso facto. \par'#13#10'\pard\cf1\lang1033\f2\fs16\par'#13 +
                #10'}'#13#10#0
              Stretch = True
              Transparent = True
              mmHeight = 64294
              mmLeft = 21960
              mmTop = 22225
              mmWidth = 157692
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
            end
            object pgbrk1: TppPageBreak
              UserName = 'pgbrk1'
              mmHeight = 3440
              mmLeft = 0
              mmTop = 90488
              mmWidth = 203200
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
            end
            object ContratoTextoCondiciones2: TppRichText
              UserName = 'ContratoTextoCondiciones2'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'ContratoTextoCondiciones2'
              RichText = 
                '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fswiss\fp' +
                'rq2\fcharset0 Arial;}{\f1\froman\fprq2\fcharset0 Times New Roman' +
                ';}{\f2\fnil Tahoma;}}'#13#10'{\colortbl ;\red0\green0\blue0;}'#13#10'\viewki' +
                'nd4\uc1\pard\nowidctlpar\qj\lang3082\f0\fs20\par'#13#10'\par'#13#10'\par'#13#10'\p' +
                'ar'#13#10'El campo se\'#39'f1alado como \ldblquote\b Identificador de Serv' +
                'icio\b0\rdblquote , es de uso interno, y por tanto autorizo a co' +
                'mpletarlo a su criterio o a modificarlo, sin que ninguna de esta' +
                's acciones constituya causa de nulidad o vicio alguno del presen' +
                'te mandato.\f1\par'#13#10'\ul\par'#13#10'\par'#13#10'\f0 Datos del mandante (titul' +
                'ar de la Cuenta Bancaria o Tarjeta de Cr\'#39'e9dito).\par'#13#10'\ulnone\' +
                'f1\par'#13#10'\f0 Nombre: <#PAGO_NOMBRE>\par'#13#10'\par'#13#10'Rut.: <#PAGO_RUT> ' +
                '                          Tel\'#39'e9fono: <#PAGO_TELEFONO>\par'#13#10'\pa' +
                'r'#13#10'\par'#13#10'\ul Datos de la Cuenta Bancaria \par'#13#10'\ulnone\f1\par'#13#10'\' +
                'pard\nowidctlpar\tx5680\f0 Tipo: <#PAGO_TIPO_CUENTA> \tab\par'#13#10'N' +
                '\'#39'ba Cuenta bancaria: <#PAGO_NUMERO_CUENTA> \par'#13#10'Banco: <#PAGO_' +
                'BANCO>\par'#13#10'\pard\nowidctlpar\qj\f1\par'#13#10'\par'#13#10'\ul\f0 Datos de l' +
                'a Tarjeta de Cr\'#39'e9dito\par'#13#10'\pard\nowidctlpar\tx5680\ulnone\f1\' +
                'par'#13#10'\f0 N\'#39'b0 Tarjeta: <#PAGO_NUMERO_TARJETA>\par'#13#10'Tipo Tarjeta' +
                ': <#PAGO_TIPO_TARJETA> \par'#13#10'Fecha Vencimiento: <#PAGO_FECHA_VEN' +
                'CIMIENTO>\par'#13#10'Banco: <#PAGO_BANCO_EMISOR> \tab\par'#13#10'\pard\nowid' +
                'ctlpar\qj\b\f1\par'#13#10'\par'#13#10'\f0 Identificador de Servicio <#CUENTA' +
                '_COMERCIAL>\par'#13#10'\b0\f1\par'#13#10'\f0 En Santiago, a <#PAGO_DIA> de <' +
                '#PAGO_MES> de <#PAGO_ANIO>\par'#13#10'\par'#13#10'\par'#13#10'\pard\nowidctlpar\li' +
                '1136\qj\f1\par'#13#10'\pard\nowidctlpar\li1136\qc\f0 _________________' +
                '_____________\par'#13#10'Firma de \ldblquote el Mandante\rdblquote\par' +
                #13#10'\pard\cf1\lang1033\f2\fs16\par'#13#10'}'#13#10#0
              ShiftRelativeTo = pgbrk1
              Stretch = True
              Transparent = True
              mmHeight = 53181
              mmLeft = 21960
              mmTop = 96573
              mmWidth = 157692
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
            end
          end
        end
      end
    end
    object prm2: TppParameterList
    end
  end
  object dsDocumentacion: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'LineaCaratula'
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 216
    Top = 128
    Data = {
      3C0000009619E0BD0100000018000000010000000000030000003C000D4C696E
      65614361726174756C6101004900000001000557494454480200020064000000}
  end
  object dsContratoBajaTAG: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Patente'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ClaseVehiculo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Marca'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Modelo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroTAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Condicion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'MotivoDevolucion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EstadoDevolucion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ModalidadContrato'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 216
    Top = 80
    Data = {
      390100009619E0BD010000001800000009000000000003000000390107506174
      656E746501004900000001000557494454480200020014000D436C6173655665
      686963756C6F0100490000000100055749445448020002001400054D61726361
      0100490000000100055749445448020002001400064D6F64656C6F0100490000
      000100055749445448020002001400094E756D65726F54414701004900000001
      0005574944544802000200140009436F6E646963696F6E010049000000010005
      5749445448020002001400104D6F7469766F4465766F6C7563696F6E01004900
      000001000557494454480200020014001045737461646F4465766F6C7563696F
      6E0100490000000100055749445448020002001400114D6F64616C6964616443
      6F6E747261746F01004900000001000557494454480200020014000000}
  end
  object dsAnexoBaja: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Patente'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ClaseVehiculo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Marca'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Modelo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroTAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Condicion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'MotivoDevolucion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EstadoDevolucion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ModalidadContrato'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 216
    Top = 32
    Data = {
      390100009619E0BD010000001800000009000000000003000000390107506174
      656E746501004900000001000557494454480200020014000D436C6173655665
      686963756C6F0100490000000100055749445448020002001400054D61726361
      0100490000000100055749445448020002001400064D6F64656C6F0100490000
      000100055749445448020002001400094E756D65726F54414701004900000001
      0005574944544802000200140009436F6E646963696F6E010049000000010005
      5749445448020002001400104D6F7469766F4465766F6C7563696F6E01004900
      000001000557494454480200020014001045737461646F4465766F6C7563696F
      6E0100490000000100055749445448020002001400114D6F64616C6964616443
      6F6E747261746F01004900000001000557494454480200020014000000}
  end
  object rpAltaContratoSuscripcion: TppReport
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    AllowPrintToArchive = True
    AllowPrintToFile = True
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 72
    Top = 280
    Version = '12.04'
    mmColumnWidth = 0
    object ppDetailBand5: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 14023
      mmPrintPosition = 0
      object FAltaSuscripcion: TppSubReport
        UserName = 'FAltaSuscripcion'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 1852
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt8: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object ppTitleBand1: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 1852
            mmPrintPosition = 0
          end
          object ppHeaderBand5: TppHeaderBand
            mmBottomOffset = 0
            mmHeight = 61913
            mmPrintPosition = 0
            object pshp3: TppShape
              UserName = 'pshp3'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5821
              mmLeft = 1588
              mmTop = 56092
              mmWidth = 25929
              BandType = 0
            end
            object pshp4: TppShape
              UserName = 'pshp4'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5822
              mmLeft = 67998
              mmTop = 56092
              mmWidth = 31221
              BandType = 0
            end
            object pshp5: TppShape
              UserName = 'pshp5'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5822
              mmLeft = 98954
              mmTop = 56092
              mmWidth = 40746
              BandType = 0
            end
            object pshp6: TppShape
              UserName = 'pshp6'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5822
              mmLeft = 26723
              mmTop = 56092
              mmWidth = 41540
              BandType = 0
            end
            object lbl_CodS1: TppLabel
              UserName = 'plblPatente3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 2910
              mmTop = 56621
              mmWidth = 22225
              BandType = 0
            end
            object lbl_CodS2: TppLabel
              UserName = 'plblClasevehiculo3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Clase veh'#237'culo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              WordWrap = True
              mmHeight = 4234
              mmLeft = 28840
              mmTop = 56621
              mmWidth = 37042
              BandType = 0
            end
            object lbl_CodS6: TppLabel
              UserName = 'plblMarca4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 68792
              mmTop = 56622
              mmWidth = 29104
              BandType = 0
            end
            object lbl_CodS7: TppLabel
              UserName = 'plblModelo4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4233
              mmLeft = 100277
              mmTop = 56622
              mmWidth = 38100
              BandType = 0
            end
            object ppRichText3: TppRichText
              UserName = 'RichText3'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RichText3'
              RichText = 
                '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fswiss\fp' +
                'rq2\fcharset0 Calibri;}{\f1\fnil Calibri;}}'#13#10'{\colortbl ;\red34\' +
                'green42\blue53;\red128\green128\blue128;\red0\green0\blue0;}'#13#10'\v' +
                'iewkind4\uc1\pard\nowidctlpar\sb10\sl-260\slmult0\qc\cf1\lang133' +
                '22\b\f0\fs22 FORMULARIO DE SUSCRIPCI\'#39'd3N \par'#13#10'\pard\nowidctlpa' +
                'r\sl-200\slmult0\qc\tx426\cf2 SISTEMA ELECTR\'#39'd3NICO DE COBRO DE' +
                ' TARIFAS O PEAJES\par'#13#10'\pard\nowidctlpar\sl-200\slmult0\qc SOCIE' +
                'DAD CONCESIONARIA RUTA DEL MAIPO S.A.\cf3\lang1033\b0\f1\par'#13#10'}'#13 +
                #10#0
              Transparent = True
              mmHeight = 13229
              mmLeft = 0
              mmTop = 23019
              mmWidth = 201613
              BandType = 0
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
            end
            object ppImage4: TppImage
              UserName = 'Image4'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                009C0000002F0802000000998434C1000000017352474200AECE1CE900000004
                67414D410000B18F0BFC6105000000097048597300000EC300000EC301C76FA8
                6400001E3C4944415478DAED9C075C1447DBC06F0FAED0BB74057B41A404C528
                F60A268A051B364404A5496FD27BEF0A0A82220611C5466CD8308A0D44B060EC
                200888D2EEB87EDFB37B709C08C42498F77D3F33FE7239F666A7FD9F79CAECCC
                225C2E87C56452A83404417068E20A0909898989E17889CBC1FE8730188C4FAD
                6D2422515A5A02C7859C5CDE75F8D2D2DA4E673064A5A50804027CA1D1E8DD45
                7D96B85CAEA80889402062F721EDEDED1D944E2121BCBC8C345E58087EC6F597
                10844E837F0C7EB124128144227FDE42AC310897D24165B139820DC0EA2543DB
                E0775A27ED536BBB84B8A8B8B828AEFF0AB1A2701D1D14369B032D141717FFBC
                225E163C0EC7696BEB8086138904111191CF33A02534377F6230592422415656
                1AAB8E5F25C2E1B03FB6B4C11518373C5E887747473B85831682409B09046151
                51D12FFAC8AB172EB06BEA1A3A3AA85C1C574C54444549814824F173224C06C3
                272CF97EE5133289C4E1A09700AA8C94C4D41FB4D72D5F2C292989350589DD9B
                9D7BFC9C869A72A4AFE3708DA15DD520F8A7CF5EB805C57F686ED9BCFA27AB4D
                66BEE12937EF559049448C05C21F56F8042AB37ED4F771DA86C3A17779852635
                B7B4C24FC673A67B3858A2B5F4C915C1BF7AFDD63D38B18342C5E3F190054A25
                1208F272D2063A137E5E384B71883C7A235A15527CBD343A351BDA8F558AE3D5
                4FA176AE30996B6BB98EC562ED0E4BBAFCDBFD31C387C605BBCAC9CAF4E6D42D
                43F05F6EC19903BF9C061E30263A13467B395A8A8B8BF5B410C137363507C5A6
                3D7F5503F945C9240F7B0B03DD893D0522F8C2B39712337E81DBA13D0E966B7F
                5E3C47F0D7A28BD72353B2019FB3F5869F16CE866BC74E9DDF7FB8505898D778
                2E7C91919634D4D75EB36CA1B494147FC0A117F9A72E9CB970ED754D7D5B0705
                AE898B89AA282ACC9B396593D94FA81C7039484747C7CAAD2E0F1FFF2E224286
                1640712C161B3EA1290B661A4607384B494AC09D8EDE11D9F9A787A9A9642506
                EA688DE5D771FB5EC56607DF4FAD1D96EB97067BDAAFB172BD547247944C8651
                26080BE311044A64B258808C4AA399CC9D9E95148420F8E48CDC90F84C111112
                34514D79484146B4B292623F438CBF535E6966E90A44E12F369B8D7D72586CB6
                0899347AF8303F67AB1FA7E8F1246FFFA163BE517B6188D91CB42BBC99D1DE41
                B55C6F1AE9E7C460D0D7DB785DBF5DAEA1A674FC40ACAAB2527F35C28DAE01B1
                078F9E95101361B2D930E9E2029C4C97CC17A4929153E01BB907E613348CC960
                86EF7658BF7249F7B0206C16DBD2C9FFFCD55B226432B59366326F7A5AF46E54
                DA786201B7E71EF78D4885F1F175DEBE6DC34AB8E61F95BAEFD0713299CCE6B0
                211BF411868E202C34638A6E6C90ABBC9C2CE4A1523B03A2F71E3B7309D42181
                4890101385F23A285D1AC578EEF4F0DD8E9212E22854F31DDE772B1E698D1D19
                E2614B26139FFCFE2AFD60C1AB9A3A8011B5DB61D5D245509C5B40ECA1636787
                A929EF8BF19D387E341FEABDF2CA6DCE41A0D360A6FABBED78F8E829DC083309
                6A4A39905F5BD7A03954C56AC30A90260683396298AAD6F831542AD5D229E0D6
                BD87B2529274260B147BA09BCD9AE5C6FD0DF1FD8A471B767AC3ED26F38D36AE
                5A02385FBE795774E9C6EDB24A3A83A9A632645FB4EFF8B123216FD691C2E0F8
                7DD04FBBAD6B664D33A0D3E93C09505755525755A6D168506F090A55F997B4F0
                01C408A07A87261D2E28521A220FD2D1F8E1E392F94689A19E783C8269003C74
                61BB4BD0B5D2B2E14355A1EF30D6C11E3B579B2EE60FCBE3A7CFB738FA7D6A69
                17171301A8D292E2D9898163468FE0673878F45440741A40F576B4DCBC76195C
                0B8DDFB7FFF009B014EEB69B7FD0D1FAFDE59B8CDC138FAA5F824573DCB6CE79
                C766C893B4EF70F4DE43C06FA486BA95F90A9D8963C17ADE29AB4A3F54F0AEBE
                0978395B9BDB5B997741BD5D5E65A8AF75FC403CAF5FBF9CF8D5233811641050
                F9BA587F0DD44D664B02DC77F247864EA7ADB00005F06CB29E566E6A189144E2
                FF5406906C77436B80D0C5EBA5D5CFDF982E9E1D17EC262C2CDCC7287743EDA4
                D16D36AF72B3B5E05DA650A97E11A985E7AED2E874F315C661DE0E081ECF83CA
                60B0F6C7FA2E983DAD57497F16EAC1FC33E3470F1F3B42A3A8F886829C4C7652
                D0A8111AE82D08FE6ED9C34DF6BE20BB4B16CCB874BDB4A9B925D4D3B60B2AA6
                BD4167F847A78D1A3E14B4DDE1E3E7DA299400176B0C1E9727167D42DD97731C
                0CE4A1E4205DED0970E5CA8DDB0E3E51E0CACC9CAA9F931A5657FF7E9D8DD7AB
                B7752A4AF27B22BC74268EE737B9F8DA2D27FF58706EC68FD6CC4B8FEC813A45
                4FEB78660C8219EDD3E7AED879478051DCB1D9CCD576CB9F81DAD5E896D656D0
                75554F9FEB4F1A7F203E404A4A92DFE1E894CCB8F4DC09A3871F4808084BCC3C
                7DA1445951FE706AC808CD610343DD66BEDCCB711B66A5D1917D5B5BB766BB47
                7D4393E650D59C94101565453ED48C38BFF9B37EEC2904CBFF67A1661F3D3D69
                FC682BF3E581B1FB9A9A3F05BA0115535E96E098BDC9078ECE9966B0EAE7F981
                B1E96DED1401A878069D6EE1E877F1FAED0DAB4CECB7AE05FC2FDFD41A19EA65
                25040A83BF86E51900EABE98DD530D74E1CACB576F3739F8BE7AF3CE688AEE91
                F4C833E7AFDAF944802D58BB6C6188B74357BF780DE672D7EFF0FCEDCE03D088
                B929A13D507F98340E6A251089BFBF781D9A9071F7C123511191CC38BFA99375
                070F2A1E5CCA0DB65EA5F7ABD69A2E8A0D74CDC93FED139E02C6C6DF65FB06B3
                9FFB7097FA870A39B7390514DFB8033EF9E13D217ADA1330A8FB41164D8D674F
                1C379AC36633992C156585C573A72188D05F803A7ED4F09470CFA0B874200408
                61B20A0B139A9B3FAEB5F67CF4EC8587EDE6297ADA16BBFCA16D82501F563DDD
                60E7D3DADE11EE6DBFC6D478D7EEC8A3A72E2AC9CB1E480CD49E30E60FA1F267
                2A3853AE81716DEDD49F1618A546FA2482EEDD73904C2406B8DAAC5D610CBE77
                7708824E1530C987F2CF82CF9111EBD70515F4A48484B8AC34F8BAB8D6B67610
                3D395969CB75CB2CCD57F09CC8C1827AEDE6DD1DEEA1600BE3039D972C9C5DFD
                FB4BF0B3DEBD6F9A3DCD00B2E1515782F3B550B1A13F72E21CD898030981D3A7
                E8F1A08203D5D9490303030DA17476EA6B8F030D0461C95F803A4A63E8E99C84
                43F96742133261400EC4FBC1709F387BC925200EC2B3A3FBA240E36D730EEC05
                357EEFC198BD3960C87353433486A9E79F3CE71D9602B1CDAEEDEB1DACCC0780
                CAB7A920916FDFD5A51D2C00FF067E8AF1DF656A32DF3F6A4FD62F274182E383
                5C16CF9B21E0B5A150E3D30E2567E681102785B87541AD7CF2BBA8A8088C0EC4
                8E401B5CF91D5BCCEC2CD7F39417DC330850B17242E3D29332F3268E1D519815
                278A45C3F65EE1C7CF1643A49519EFA7356ECC9F82EA11140F93008CF1C1C420
                4383493CA8100C802D5452407D1C1A833E6EA4A6CB8E8D42C284BF0015FC9193
                D971750D4DEB6DBC1B9B3F3A6D3777B2D964EF199677EAC29CE99373F7869594
                96818CF640C5A1D1ED56477FF0A1404982A30057DADADA965BB8001ED0A2FB63
                FDD0D00887EB136AE6919310580205C08445FF4C10D075A68BFC5C6D20CEF68F
                4CCDCA3B0D31715CA0B3C9FC99BDA0C6A666A766E73398CCBD11DE7CEFF7B1D6
                D811E1DE763069E2D272C0BD8448232ED0455F478B07C3D53F26A7A008A042B3
                B4C68DE243BD5F5165E914086EDE46B325817F00151ADABAC6CAA3FAC5EBB123
                358CE74D171602652674FD56D9DDF247E0EF78D859586F36C3D42FF76BA0B2D9
                2CB85E7ABF524A523C373574DC98913C9BCA64B241672E5930537085043EFF1A
                D4FC7D91F2F2B2EBAC3DAE9796194DD675B23677F28B7D5DF3CED769BBD5A655
                574A6E83F32108B5F4EE83AD4E814C2673DA141D43BD89BCD8AAF0D7CB6F6AEB
                C1B13A10EF6FA0A73D30547151112027264AD65057996B3479D9E239A825C6E1
                62F76427EC3B02F36DB793D506B39F7AA95FD780988233C5E0DB1E4A09FECC51
                3A91857ABF278B8ADD831360CAFEBC70665298879090305CF40E4DC8CA3B2327
                2B951CE23EE347033ED4925BF760A6824C6DDFB8DCC3DE7260A897AEDDB4F78E
                44C599DAD941A1A28B125C1CC4C74005F4C6645D2D70707A2FCDF409154B1555
                4F2C1CFC3F7C6A01050BEA17AAE0434D8DF032993F630047292F3D424971488F
                F470B9FD4105FC90333DFB68486226C460208E0F1E3D939410CB4A080031BA5C
                72DBFE73A89149197BB28F91C924B062709D37E412E2A212E26210DBD86C5CE9
                6A67D11F549E4D85A905734958082F272385C7069FD7FEBCC25FDD0213400FAD
                34991BE1E724E828B5B777ACD9EE5EF9E4B9BAAA2274AD07EA64DD0947D32321
                F6A07576AEB7F12C7FF44C424C2C2B3140571B759DD3B28F46246741DF572F9D
                1FEA658FC7566D40517B8726E69DBC00BE094CEBA5FC45937EA07A06C51F3EFE
                ABA282ECECE906A26474AD03C156276EDFAF7C5D53472611C11341ABEB07AACD
                A6556E765D21CDAB37353E61C9A565554C16D3D56693DD36D452F0BD5FB0110B
                E7F41BD268AA291FCB8C969793FB8C7A3F508FEC0D83395DF5B87AB3835F3BA5
                1308818A9B33DD203DC60FA6452FA8144AC7EA6DEE95D52F46690E9D663089B7
                9806D93AA8D4925BE54D1F3F698D19014A1B0CFC005021A2C514E4E70D43F04F
                AA9F8323DDF4B165889C4C4290ABA1810EBFF50969392959F9341A1D5CAAE430
                AF3EA042AE8C9C82A0B87D0882DF686612E086469FCF5FBED968E753DFD00C0E
                82F1DCE940052E42978A8A6F428BB5C60CCF4E0A5690971B006AFDFBC68D76BB
                1F3F7B397FC694ACC440DE1A262FED3990179E740064D01A04198DA03802ABA4
                5D50592C3684BC738DA6804FFBB6AEA1E4D6FD5735757071DAE449C9619EBC05
                9703B9852109FB61DC4D4DE6E8688D653298388CC1C4B1236108E8349A8553C0
                ADBB1530037E5E384B52421C8AA2311810B718CF37EA599BED0B2A93C1B07209
                BE7AF32E0C3A8DCE08F6D8016E2D6F0404A1165FBFE5E01DD54EA53AA13ED106
                7E07A1A2ED2E41E7AEDE9492104F08769B3BC3F08F439ADEBE0502ADF38B4CC9
                C054B48AA2C20A93B963476BD2E90CB0EB10498366559097498FF2D19B34015D
                585FBEC5B9B4AC12D4EFD99C241E548890566FF778F5B656435D352F2D7CC4F0
                6170F168E1B9D0C40C08B4C14AA06B765C1C9BC301121026867AD91A19FED063
                0E11FCA79616D3CD4EE07FFDA003B212212525957FEABCAD6738EA2B79D96E5D
                BF5CB0C10F2A9F98EFF4067FC460D20450773232D282127AB7BC728585337C83
                EA180C066FF0D1557219A91953F53CED2DD45494BB97090BBCC29345C9243007
                E0FDF2969EA1B7E0B3642606B2984C886B61A6826E04A1466D1D826B6BEF80D1
                39BC270C4163240E1FAAB36FF4FE2385A335878243A7AAA20497F7A3EB82A9F0
                B39AB24241668CBAAA0A5CBC70E5372B9720801A1BE0BC7EE51217FF18000323
                9EBB37545760710013B8E33EE1A9543022EB9745FA3967E69EF00C4904A8109F
                806D860CBE11297BB28E62214DB0D1D41FFA0CD95B3EB57884245EB8560A8205
                4A008F4760C841E1090B0969A82BC350982C980514103A9D1E959C5556F9444F
                7B9C87BD056F351C0AD8939577E5C65D7131117BCBB5D8E2053A6AA5772B4EFC
                7AF9C5EB9AB60E0A3448465A0AF489D9B205A3866BF4D2609D9D9D5129D9954F
                7FD79B380E9C0B12897CE2ECC5DC82737272D29EF65B86A9AB093A6F2C262B32
                39ABBCEAA9B2A282DB8E4D6A6ACA8250618AC7A61D6A6BA3805BC8CB0FD8D454
                8618EA6B1BEA4FC42158148402C4DF7B50B53FE7044041703D4F69A0FF33A7EA
                6F5AB314BA9079F8C46F772B4824025F13C0281B4D9EB4D57C654F888CF91D39
                F9670ACF5D012DEA61BF450A7DAA81AB7D571F9992F5BEB1199C17AB0D2B113C
                9A0D5462CCDE43204376166B26EB6B472665C2F4D09930DAC3612B914014EC05
                DC1E9E98D9F0E1A381EE04375B8B5B77CBF7661F83FA206E447D141CEEF8998B
                F9A72FC254065F7D64AFF1141858D03780E0DAADB2FA86262A9506BC64652427
                8E1B05A2892E7861BD00D4A0EBB8E07C82FCA28EA240C768B44E213C9E402408
                7A5938CC89A550C1BA20E0E0743FA4FB72D100E92A9644C6DC4F3481AE00BF0E
                35FE5F3EC6E27240DE41E2B0EA7AAF3F60C6850D06180A411FFE08A8EE2FCD21
                97D3FBD11BBA4CC6EF17978D13408E760D8FEFED72635241A7D189442282E78F
                09DA486CA044042400CF62A2CA83802164B339E0F79245C8D848F6EE2387CDA2
                33182402110FD289E0994C0614432012F91A824EA7A14F418484FB262A301AD0
                80E6E64F20AF60B0A5A5C4454444052920D0929E31FA6C8090EECE7F0EACE73A
                AE8F5FFBA89EEFA475DFD86F2081FBE35FBFBED23EF3F76E7CFFA5F5D9DAEE01
                FDC25BC6F1DDFEAFEA45F78A69DF6D1B80E8D70D0806F56F264CF509FCCD19F0
                F9F3BFE99BA7BF0D15416A5AA9A5756DBCBF203E99A62AAD262532D04E867FD3
                374E7F1F2ABEBCBE25ABB20EFB8E6371B8DBB4D57494A5BE4A87FC9BBE4D1A04
                A80F1B5A8F3C7E8F7D47A16E9CA0325151F25FA8FFC13408501FBC6FC9A9C2A0
                E2712C3667CB44B5494AFF42FD4FA641805ADDDC51F4B219FB8E6373B84B472A
                8C9215EB2B76EED3F3FCC6E9BB94AD41F07E996C76279DC5FB0E9EB528519840
                10EEED0077FBFD140A1522B96F4E97CB259208A2A2BD36BA7E2F691066EAED7B
                15E14999E84206826FEFA0B8DA6D5E306B9AE0620A7CBC6F6C3C7FE556496959
                7DC3076C47E037872A242CA4AAACB068F6B4C5738D4444C8DF15D741807AE1CA
                4D1BF7106C2912DFD2DA961CEEB966D9628175545C6151717266DE8BD7B5783C
                823EFF45FE1125CCE5B2B105B339D327EF76B61AAAA6F2FD701D04A8C5D74B1D
                7C22BBA0B6B5C705BAACFC6901368228BFFD3905D1A907994C968828F91FB7A8
                E862777B3B65BAA16E729887AC8CCC77C2F59B41C51E005CB971DBCE2B028812
                89E8B37B168BDD893E1EF9E6230B0A418444424D3BB61ED2D141B1B558ED8A6E
                2FE57E0FAB22DF0E2A8ECEA05BEEF2BF711BDDB7087F32184C29297183491364
                A425B9DF72641104A1503AEF553C7EDFD4CC3B0042A333D494871C4C0A52FF3E
                94F03784FAB0EAE956E740D07E3063E80CA686BA72A0DB8E29FADAC83F6253AB
                1E3FF30A4BAE7EFE1A9404B48D46A3A7847B2E9C33FD5FA85F5340BF50CF5FFE
                CDD137125C62708EA89D34876DEBD00D9268FA0714202A3759BF148626641009
                04102370CB833C766E58F5D30027B1B0A671FA7EEA3270EAB9EBF33F05BC45FE
                6F3D0FF270BD9E5F0D9A69F88650AF8241F58E84004648488842A52D5B342B36
                C8F5ABCBE50E7CB8F16B82A2FC53177CC29309C2C200B58342F577B5DEB47A69
                57E15D95F04FB121EF1B3FB4B6B68F19A5F9B6A6AEE9638BFE2474DF4253D347
                90063299585E554D2408C33FF8534579C8284DF5078F9EC177F012C68C183676
                F48807954F6AEB1B44C8243DEDF1605FEE573C86EBBC8372743AA3FAC51B5054
                35EFDE0FD750873CBCD3822C365B6B1CBAD996C1643E7BFE6684861A1A7AFD97
                43ADADABDFB0D3A7B6BE110C1BB60306315D3C6BC982590A7232BC87E65F6044
                41494A88CBCA741FD9ECFFA9248341FFF8A9153B56DB97447071CA8A0A672F95
                0842458F61ADFAE92395CEBB033E65C904213CC22BF3E8C97377CAAAA2035CA2
                530EECCB399E93126AA0371182B1DFEE3CD8BA6ED9D98B254F5FBC7E5BFBDEC8
                5057576BAC949444504CDABC1986E0F4C195495A632C1CFC476AAA7339DC4F6D
                6DDE8E96E0F04F33D059B574210E3B125370FA9287BD8557685280AB8DA6863A
                8D46DB62EF4BA1527F498F1215156968FCE01614EFEFB25D7398FAA058876FEB
                FD06C5ECDD77F8047082A163B339605925C5C5789E703F890BDA72F488610B67
                FDB868CE740909B12F9F51D7BCAB3F73E1FAB59BF7DED635801AF8D2428300B1
                582C5FE76D440271975F340F2A85420DF3DCA16934FBC4E37764213C7A4A5008
                6FA5A33A448CCC53B3C74E9DBFF7E071B8EFAEE894ACDFEE948FD0508FF2732A
                2ABE71F15A693CB627FBE2D59BE7AFDC8A0E7086EFD76FDD2D38733921C49D57
                6343D307AF90C4187F67696929334B679BCD66303BCF5C2C490EF7825FC313F6
                0F919701C0B61EE101AED61AC3D44AEF96FF5278BEA383BAC16CC9CC69931B9B
                3E788624ED76DAA63154EDBF1B2A6657DE37345ABB859455564B8A8BF236A300
                DA815D5F50BB2C261B3EA7FEA0ED61B765E2F8318286E7D4F9AB09E9879FBFAA
                01E70BB47A9F2A18A03259AC306F5B29090947DF2841A8AA3FCECAAFAAE9822A
                8CB7D71FAA24DE05B5E0F405801AB6DB313C21435141B6E251F532E339501408
                10CF6A9CBB7CE3D2D5D2E84017F87EEBDE83C8A4ACD54B1790C9A4D9D30CA047
                F6DE1126F38C080442F1F55B6E761662A222302691BB1D41153B78477A396E95
                9596822B6002801C60D61E3792C16297573E0970B76DFAD00C507D7659FE2F40
                C57E7DF6E2B54F58F2EDF22AA2B030F4B96BF3D81F2528AD9D42D550534E0C71
                9BA4358E772EFCC8F1B381B1FB2034121DD0F6F0A0067BEC901013EB05557DDA
                AC638F6AF933D5564F5DF10BA8C1B1695A63472A0D91CB395634C350F75EC593
                486CE7F4AFC52597AEDD8EC1A096DE7B109290095E02405D34FB471030905D75
                15A527CF5E1ACF9D6EBD65350E3D7D1403AEBEA6BA0A94131BE8FCE1638B8B7F
                6CB0A7ADACB4E44E8F50472B73069311999C9D11EFCF66B15D03E3FF77A06219
                5ADBDAB28E9C3A5B5C525BD740A17672B9FD7839D8048629482691F098A9034F
                447FD2B88C587F506B15954FB638FAB75328646C132B7819E83A069BF365511C
                360A35C67F97BCAC8CC3EE4841A8CA5367E63E7C2B228C07F000D579F2306509
                912FA10E1FA6B66E8589774802A804B005419E76BDA082FA3D517435AEDBEF03
                F5EB1D9A14E5BBABE9C3A78098B4980067254585A28BD72EDFB82B232501B676
                B5A931E4710F8C8FF1777A5CFD223821E3079D0950ED9DB24A179B0D46867A0E
                3E512085D8A1814178FCF0EDA1E2BA6C615373F3FD078F6BEA1AE80C469F54D1
                9D7E1CEEC327CFC1A4F1DE4F009F9D7446B897EDAAA58B40F0F34E5E9494401F
                BCC064959692986334595D59918BEBADCEB9E87E420E4CA027BFBF760F8EE73B
                4A21EE36B38C173D6A6C1542F745E38410C44049528C28CC6B61DE895F6F9755
                C606B9F986276B0E55DDB2CE14E26C332BF739D30D52237D20CBE90B57CF17DF
                4C8E40CD6449E9BDB8B4C34EDBCDA1E4A1AACA62A2643BEF88608F9DE0E9EC74
                0FD19F38CEC27C794B4BAB85A31F08715662A0B292E2FBC62667DF98502FDBF4
                83C7C68D19819DD140728E9EAA78F4CCC7C9D2D63362D9E2D92A4A0A8AF2B2E0
                81FFCDD86610A05EBA76CB7177141F6A7C90EB0AC1D723F0897DDD9319F0EF0F
                E69D8C4FCFE515D841E9042D17ECB97389B97DDDFB2670B260160E91970DF7B6
                FB71B2DEC045E5159EF38D4CED0969DC6C36A147600553CF19A3BB65552FDFD4
                AE365D54587419A6F874435DB87A20F704E80C6CCF300EAC6CD5E3E7EB5799C0
                F7B7B575E02113D1B7D1306718EA8159CDCE3BB974D16C0505F99B77CA5FBCAE
                C10E3021878E9E6A6D6FB7DDBA1E240DE2A582339740448A4BEE2C5930437188
                0294D3D0D874FC4CF132E3D927CF5D7DF7BE111AA3A73DCED464DEDFDCBC3708
                50CF5FBEE1E4178343F7CFA250C10A9A1ACF1B70E7EAC074D143036BAD3DAB9F
                BF068B0550D177BAECB234DFE1FDA9B51D94735B3BC57CA57188973D967980AE
                23F9A7CEFB84A7F0A182E7B9118D53BF6EF1417017A7E01A421FEB099870E0F0
                9FAD57F4714BF70AC39765E23E3748FF0DEAF746E9FDEDAEC1A842C3E341DB80
                4F18EA6D8F7CE55A4C5FE9F6BD0AD0666D1D14E001052E986918E4BE63D916A7
                E68FAD009546474FBFA445F9C8A1F16EBF097430D8B0134597492422B6A2448D
                0B74C626C1202DDCF045B3679B38B7EF6C3D4B1CDDF8B85F644070DDB007A16D
                83001502C75596AE30478101B6119FBB78CE348832E56424FFD4031950746C36
                B7EAE9F3C3C78B20CCE785B3E0E0ECB236B7B35CB771A7F78D3B0F2054C0A18F
                0A98537427AC5832575D5509C15E25F57939F8E64FADE7AFDC2CBA7C03871D3A
                038D2D42266527046AF35F16F4FF3A0D0254369BED11189F77EA829424FA6630
                0009834E2212B063397F4AEE503C0CECA81A8F28F8B74A0AB23929E821FB9345
                C54EFEB1DDC5E268343ABA6A87BE4A0AF9A216100E36AF0DBCB72F81C65E387B
                6A52A807093CE77FA17E5D19F8C7D5CFB73905D6377EE0CD24F4303138B27F49
                9120BCDD11E8F332BA105E28C473E70A6CD1115D240A4FCE29F8554C4C4418E3
                8ABDCAAB9F42D0C7F3086FB189DA4983D0300D3BE3F73D10C50D0E54B418FC95
                1BA5BBC3F7BCAD7B0F71249120FC979FAFA16F486330E94CA692829CB3F506B3
                65E8ABB9F86F76894AC9CA3B79013D6644240A0B0B0D500B206732D10349CA8A
                F27ECED68BE7197D710AEAFF6D1A24A8D8CE1598AF19B985A5F71F36367D042A
                7F962A6FBCC944A2D21079437DAD8D663FA32F9710F46BB0D7EC141597E49FBA
                F8A8FA05184EFE21D45EE5406BC0882A2AC81AE84CD8BCE667EC61C8F7421437
                78507B22D1DA77F56FDFD583B7F997B60C72C1300F535346DFB281FDD9E7B146
                98B82F5ED7A207343B697D696034C095961457535552E6ADD17C1FBB58F8E9FF
                003D3A71485F0DDF330000000049454E44AE426082}
              mmHeight = 16933
              mmLeft = 1323
              mmTop = 529
              mmWidth = 55298
              BandType = 0
            end
            object ppRichText4: TppRichText
              UserName = 'RichText4'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 12
              Font.Style = [fsBold]
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RichText4'
              RichText = 
                '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil Cali' +
                'bri;}{\f1\fnil\fcharset0 Calibri;}}'#13#10'{\colortbl ;\red0\green0\bl' +
                'ue0;}'#13#10'\viewkind4\uc1\pard\qc\cf1\b\f0\fs24 N\f1\'#39'daMERO CLIENTE' +
                ' RUTA DEL MAIPO\par'#13#10'\fs20 (Uso interno de Ruta del Maipo)\f0\pa' +
                'r'#13#10'}'#13#10#0
              Transparent = True
              mmHeight = 10319
              mmLeft = 121444
              mmTop = 3704
              mmWidth = 80169
              BandType = 0
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
            end
            object lbl_NumeroConvenioS: TppLabel
              UserName = 'lbl_NumeroConvenioS'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_NumeroConvenioS'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 14
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 6085
              mmLeft = 121444
              mmTop = 12965
              mmWidth = 80169
              BandType = 0
            end
            object ppImage3: TppImage
              UserName = 'Image1'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0A544A504547496D616765270B0000FFD8FFE000104A46494600010101006000
                600000FFE1005A4578696600004D4D002A000000080005030100050000000100
                00004A0303000100000001000000005110000100000001010000005111000400
                00000100000EC4511200040000000100000EC400000000000186A00000B18FFF
                DB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A
                1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C
                2E333432FFDB0043010909090C0B0C180D0D1832211C21323232323232323232
                3232323232323232323232323232323232323232323232323232323232323232
                323232323232323232FFC00011080016032E03012200021101031101FFC4001F
                0000010501010101010100000000000000000102030405060708090A0BFFC400
                B5100002010303020403050504040000017D0102030004110512213141061351
                6107227114328191A1082342B1C11552D1F02433627282090A161718191A2526
                2728292A3435363738393A434445464748494A535455565758595A6364656667
                68696A737475767778797A838485868788898A92939495969798999AA2A3A4A5
                A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DA
                E1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101
                010101010000000000000102030405060708090A0BFFC400B511000201020404
                0304070504040001027700010203110405213106124151076171132232810814
                4291A1B1C109233352F0156272D10A162434E125F11718191A262728292A3536
                3738393A434445464748494A535455565758595A636465666768696A73747576
                7778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2
                B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7
                E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00EB68A28AF50F
                9A0A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
                800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
                800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
                800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A3F13
                4514007E268FC4D145001F89A3F134514007E268FC4D145001F89A3F13451400
                7E268FC4D145001F89A3F1345140051451400514514005145140051451400514
                51400514514005145140051451400514514005145140051451400559B1FF005E
                DFEEFF005155AACD8FFAF6FF0077FA8ACEAFC0CDF0DFC689ADFF00088DFF00FC
                F6B6FF00BE9BFF0089A3FE111BFF00F9ED6DFF007D37FF001345159F3C8DFD8C
                3B07FC2237FF00F3DADBFEFA6FFE268FF8446FFF00E7B5B7FDF4DFFC4D1451CF
                20F630EC1FF088DFFF00CF6B6FFBE9BFF89A3FE111BFFF009ED6DFF7D37FF134
                51473C83D8C3B07FC2237FFF003DADBFEFA6FF00E268FF008446FF00FE7B5B7F
                DF4DFF00C4D1451CF20F630EC1FF00088DFF00FCF6B6FF00BE9BFF0089A3FE11
                1BFF00F9ED6DFF007D37FF0013451473C83D8C3B07FC2237FF00F3DADBFEFA6F
                FE268FF8446FFF00E7B5B7FDF4DFFC4D1451CF20F630EC1FF088DFFF00CF6B6F
                FBE9BFF89A3FE111BFFF009ED6DFF7D37FF13451473C83D8C3B07FC2237FFF00
                3DADBFEFA6FF00E268FF008446FF00FE7B5B7FDF4DFF00C4D1451CF20F630EC1
                FF00088DFF00FCF6B6FF00BE9BFF0089A3FE111BFF00F9ED6DFF007D37FF0013
                451473C83D8C3B07FC2237FF00F3DADBFEFA6FFE268FF8446FFF00E7B5B7FDF4
                DFFC4D1451CF20F630EC1FF088DFFF00CF6B6FFBE9BFF89A3FE111BFFF009ED6
                DFF7D37FF13451473C83D8C3B07FC2237FFF003DADBFEFA6FF00E268FF008446
                FF00FE7B5B7FDF4DFF00C4D1451CF20F630EC1FF00088DFF00FCF6B6FF00BE9B
                FF0089A3FE111BFF00F9ED6DFF007D37FF0013451473C83D8C3B07FC2237FF00
                F3DADBFEFA6FFE268FF8446FFF00E7B5B7FDF4DFFC4D1451CF20F630EC1FF088
                DFFF00CF6B6FFBE9BFF89A3FE111BFFF009ED6DFF7D37FF13451473C83D8C3B0
                7FC2237FFF003DADBFEFA6FF00E268FF008446FF00FE7B5B7FDF4DFF00C4D145
                1CF20F630EC1FF00088DFF00FCF6B6FF00BE9BFF0089A3FE111BFF00F9ED6DFF
                007D37FF0013451473C83D8C3B07FC2237FF00F3DADBFEFA6FFE268FF8446FFF
                00E7B5B7FDF4DFFC4D1451CF20F630EC1FF088DFFF00CF6B6FFBE9BFF89A3FE1
                11BFFF009ED6DFF7D37FF13451473C83D8C3B07FC2237FFF003DADBFEFA6FF00
                E268FF008446FF00FE7B5B7FDF4DFF00C4D1451CF20F630EC1FF00088DFF00FC
                F6B6FF00BE9BFF0089A3FE111BFF00F9ED6DFF007D37FF0013451473C83D8C3B
                07FC2237FF00F3DADBFEFA6FFE268FF8446FFF00E7B5B7FDF4DFFC4D1451CF20
                F630EC1FF088DFFF00CF6B6FFBE9BFF89A3FE111BFFF009ED6DFF7D37FF13451
                473C83D8C3B07FC2237FFF003DADBFEFA6FF00E268FF008446FF00FE7B5B7FDF
                4DFF00C4D1451CF20F630EC1FF00088DFF00FCF6B6FF00BE9BFF0089A3FE111B
                FF00F9ED6DFF007D37FF0013451473C83D8C3B07FC2237FF00F3DADBFEFA6FFE
                268FF8446FFF00E7B5B7FDF4DFFC4D1451CF20F630EC1FF088DFFF00CF6B6FFB
                E9BFF89A3FE111BFFF009ED6DFF7D37FF13451473C83D8C3B07FC2237FFF003D
                ADBFEFA6FF00E268FF008446FF00FE7B5B7FDF4DFF00C4D1451CF20F630EC1FF
                00088DFF00FCF6B6FF00BE9BFF0089A3FE111BFF00F9ED6DFF007D37FF001345
                1473C83D8C3B07FC2237FF00F3DADBFEFA6FFE268FF8446FFF00E7B5B7FDF4DF
                FC4D1451CF20F630EC1FF088DFFF00CF6B6FFBE9BFF89A3FE111BFFF009ED6DF
                F7D37FF13451473C83D8C3B07FC2237FFF003DADBFEFA6FF00E268FF008446FF
                00FE7B5B7FDF4DFF00C4D1451CF20F630EC1FF00088DFF00FCF6B6FF00BE9BFF
                0089A3FE111BFF00F9ED6DFF007D37FF0013451473C83D8C3B07FC2237FF00F3
                DADBFEFA6FFE268FF8446FFF00E7B5B7FDF4DFFC4D1451CF20F630EC1FF088DF
                FF00CF6B6FFBE9BFF89A3FE111BFFF009ED6DFF7D37FF13451473C83D8C3B07F
                C2237FFF003DADBFEFA6FF00E268FF008446FF00FE7B5B7FDF4DFF00C4D1451C
                F20F630EC1FF00088DFF00FCF6B6FF00BE9BFF0089A3FE111BFF00F9ED6DFF00
                7D37FF0013451473C83D8C3B07FC2237FF00F3DADBFEFA6FFE268FF8446FFF00
                E7B5B7FDF4DFFC4D1451CF20F630EC1FF088DFFF00CF6B6FFBE9BFF89A3FE111
                BFFF009ED6DFF7D37FF13451473C83D8C3B07FC2237FFF003DADBFEFA6FF00E2
                68FF008446FF00FE7B5B7FDF4DFF00C4D1451CF20F630EC1FF00088DFF00FCF6
                B6FF00BE9BFF0089A3FE111BFF00F9ED6DFF007D37FF0013451473C83D8C3B07
                FC2237FF00F3DADBFEFA6FFE268FF8446FFF00E7B5B7FDF4DFFC4D1451CF20F6
                30EC1FF088DFFF00CF6B6FFBE9BFF89A3FE111BFFF009ED6DFF7D37FF1345147
                3C83D8C3B07FC2237FFF003DADBFEFA6FF00E268FF008446FF00FE7B5B7FDF4D
                FF00C4D1451CF20F630EC1FF00088DFF00FCF6B6FF00BE9BFF0089A3FE111BFF
                00F9ED6DFF007D37FF0013451473C83D8C3B07FC2237FF00F3DADBFEFA6FFE26
                8FF8446FFF00E7B5B7FDF4DFFC4D1451CF20F630EC1FF088DFFF00CF6B6FFBE9
                BFF89A3FE111BFFF009ED6DFF7D37FF13451473C83D8C3B07FC2237FFF003DAD
                BFEFA6FF00E268FF008446FF00FE7B5B7FDF4DFF00C4D1451CF20F630EC1FF00
                088DFF00FCF6B6FF00BE9BFF0089A3FE111BFF00F9ED6DFF007D37FF00134514
                73C83D8C3B07FC2237FF00F3DADBFEFA6FFE268FF8446FFF00E7B5B7FDF4DFFC
                4D1451CF20F630EC1FF088DFFF00CF6B6FFBE9BFF89A6BE8775A62F9D3490B2B
                1D802124E7AF71ED4515339B71669469414D348FFFD9}
              mmHeight = 3440
              mmLeft = 0
              mmTop = 19315
              mmWidth = 203730
              BandType = 0
            end
            object lbl_plbl36: TppLabel
              UserName = 'lbl_plbl36'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'NOMBRE'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 1323
              mmTop = 40217
              mmWidth = 15081
              BandType = 0
            end
            object lbl_NombreCliente: TppLabel
              UserName = 'lbl_NombreCliente'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_NombreCliente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 21697
              mmTop = 40217
              mmWidth = 177008
              BandType = 0
            end
            object pln3: TppLine
              UserName = 'pln3'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 3175
              mmLeft = 20373
              mmTop = 44715
              mmWidth = 179388
              BandType = 0
            end
            object lbl_1: TppLabel
              UserName = 'lbl_1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RUT'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 1323
              mmTop = 46831
              mmWidth = 6879
              BandType = 0
            end
            object pln8: TppLine
              UserName = 'pln8'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 1323
              mmLeft = 20373
              mmTop = 51858
              mmWidth = 51858
              BandType = 0
            end
            object lbl_RUTS2: TppLabel
              UserName = 'lbl_RUTS2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_RUT2'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 21697
              mmTop = 47096
              mmWidth = 51329
              BandType = 0
            end
          end
          object ppDetailBand9: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 6879
            mmPrintPosition = 0
            object pshp7: TppShape
              UserName = 'pshp7'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 1588
              mmTop = 0
              mmWidth = 25929
              BandType = 4
            end
            object pshp15: TppShape
              UserName = 'pshp15'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 67998
              mmTop = 0
              mmWidth = 31221
              BandType = 4
            end
            object pshp8: TppShape
              UserName = 'pshp8'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 26723
              mmTop = 0
              mmWidth = 41539
              BandType = 4
            end
            object pshp36: TppShape
              UserName = 'pshp36'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 6350
              mmLeft = 98954
              mmTop = 0
              mmWidth = 40747
              BandType = 4
            end
            object dbt_PatenteS: TppDBText
              UserName = 'dbt_PatenteS'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Patente'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3440
              mmLeft = 2910
              mmTop = 1588
              mmWidth = 23283
              BandType = 4
            end
            object dbt_MarcaS: TppDBText
              UserName = 'dbt_MarcaS'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Marca'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3440
              mmLeft = 69850
              mmTop = 1588
              mmWidth = 27781
              BandType = 4
            end
            object dbt_ModeloS: TppDBText
              UserName = 'dbt_ModeloS'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Modelo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3440
              mmLeft = 101071
              mmTop = 1588
              mmWidth = 37042
              BandType = 4
            end
            object dbt_ClaseVehiculoS: TppDBText
              UserName = 'dbt_ClaseVehiculoS'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'ClaseVehiculo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Tahoma'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3440
              mmLeft = 28840
              mmTop = 1588
              mmWidth = 35983
              BandType = 4
            end
          end
          object ppFooterBand6: TppFooterBand
            mmBottomOffset = 0
            mmHeight = 12435
            mmPrintPosition = 0
            object ppImage5: TppImage
              UserName = 'Image5'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Stretch = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0A544A504547496D616765EB0F0000FFD8FFE000104A46494600010101006000
                600000FFE100784578696600004D4D002A000000080006013100020000001100
                0000560301000500000001000000680303000100000001020000005110000100
                00000101000000511100040000000100000EC4511200040000000100000EC400
                0000004D6963726F736F6674204F66666963650000000186A00000B18FFFDB00
                4300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E
                1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E33
                3432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232
                3232323232323232323232323232323232323232323232323232323232323232
                32323232323232FFC0001108002F033103012200021101031101FFC4001F0000
                010501010101010100000000000000000102030405060708090A0BFFC400B510
                0002010303020403050504040000017D01020300041105122131410613516107
                227114328191A1082342B1C11552D1F02433627282090A161718191A25262728
                292A3435363738393A434445464748494A535455565758595A63646566676869
                6A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7
                A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2
                E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301010101010101
                01010000000000000102030405060708090A0BFFC400B5110002010204040304
                0705040400010277000102031104052131061241510761711322328108144291
                A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738
                393A434445464748494A535455565758595A636465666768696A737475767778
                797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4
                B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9
                EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F04A28A2BD5330A2
                8A2800A28A2800A28A2800A28A2800A28A2800A28A2800A2BBFF00F8529F10BF
                E85FFF00C9DB7FFE3947FC294F885FF42FFF00E4EDBFFF001CA9F690EE3B1C05
                15DFFF00C294F885FF0042FF00FE4EDBFF00F1CA3FE14A7C42FF00A17FFF0027
                6DFF00F8E51ED21DC2C7014577FF00F0A53E217FD0BFFF0093B6FF00FC728FF8
                529F10BFE85FFF00C9DB7FFE3947B48770B1C0515DFF00FC294F885FF42FFF00
                E4EDBFFF001CA3FE14A7C42FFA17FF00F276DFFF008E51ED21DC2C7014577FFF
                000A53E217FD0BFF00F93B6FFF00C728FF008529F10BFE85FF00FC9DB7FF00E3
                947B48770B1C0515DFFF00C294F885FF0042FF00FE4EDBFF00F1CA3FE14A7C42
                FF00A17FFF00276DFF00F8E51ED21DC2C7014577FF00F0A53E217FD0BFFF0093
                B6FF00FC728FF8529F10BFE85FFF00C9DB7FFE3947B48770B1C0515DFF00FC29
                4F885FF42FFF00E4EDBFFF001CA3FE14A7C42FFA17FF00F276DFFF008E51ED21
                DC2C7014577FFF000A53E217FD0BFF00F93B6FFF00C728FF008529F10BFE85FF
                00FC9DB7FF00E3947B48770B1C0515DFFF00C294F885FF0042FF00FE4EDBFF00
                F1CA3FE14A7C42FF00A17FFF00276DFF00F8E51ED21DC2C7014577FF00F0A53E
                217FD0BFFF0093B6FF00FC728FF8529F10BFE85FFF00C9DB7FFE3947B48770B1
                C0515DFF00FC294F885FF42FFF00E4EDBFFF001CA3FE14A7C42FFA17FF00F276
                DFFF008E51ED21DC2C7014577FFF000A53E217FD0BFF00F93B6FFF00C728FF00
                8529F10BFE85FF00FC9DB7FF00E3947B48770B1C0515DFFF00C294F885FF0042
                FF00FE4EDBFF00F1CA3FE14A7C42FF00A17FFF00276DFF00F8E51ED21DC2C701
                4577FF00F0A53E217FD0BFFF0093B6FF00FC728FF8529F10BFE85FFF00C9DB7F
                FE3947B48770B1C0515DFF00FC294F885FF42FFF00E4EDBFFF001CA3FE14A7C4
                2FFA17FF00F276DFFF008E51ED21DC2C7014577FFF000A53E217FD0BFF00F93B
                6FFF00C728FF008529F10BFE85FF00FC9DB7FF00E3947B48770B1C0515DFFF00
                C294F885FF0042FF00FE4EDBFF00F1CA3FE14A7C42FF00A17FFF00276DFF00F8
                E51ED21DC2C7014577FF00F0A53E217FD0BFFF0093B6FF00FC728FF8529F10BF
                E85FFF00C9DB7FFE3947B48770B1C0515DFF00FC294F885FF42FFF00E4EDBFFF
                001CA3FE14A7C42FFA17FF00F276DFFF008E51ED21DC2C7014577FFF000A53E2
                17FD0BFF00F93B6FFF00C728FF008529F10BFE85FF00FC9DB7FF00E3947B4877
                0B1C0515DFFF00C294F885FF0042FF00FE4EDBFF00F1CA3FE14A7C42FF00A17F
                FF00276DFF00F8E51ED21DC2C7014577FF00F0A53E217FD0BFFF0093B6FF00FC
                728FF8529F10BFE85FFF00C9DB7FFE3947B48770B1C0515DFF00FC294F885FF4
                2FFF00E4EDBFFF001CA3FE14A7C42FFA17FF00F276DFFF008E51ED21DC2C7014
                577FFF000A53E217FD0BFF00F93B6FFF00C728FF008529F10BFE85FF00FC9DB7
                FF00E3947B48770B1C0514515420A28A2800A28A2800A2AC58D95C6A5A85B585
                A47E65CDCCAB0C29B80DCEC405193C0C923AD76FFF000A53E217FD0BFF00F93B
                6FFF00C7293925BB19C0515DFF00FC294F885FF42FFF00E4EDBFFF001CA3FE14
                A7C42FFA17FF00F276DFFF008E52F690EE16380A2BBFFF008529F10BFE85FF00
                FC9DB7FF00E3947FC294F885FF0042FF00FE4EDBFF00F1CA3DA43B858E028AEF
                FF00E14A7C42FF00A17FFF00276DFF00F8E51FF0A53E217FD0BFFF0093B6FF00
                FC728F690EE16380A2BBFF00F8529F10BFE85FFF00C9DB7FFE3947FC294F885F
                F42FFF00E4EDBFFF001CA3DA43B858E028AEFF00FE14A7C42FFA17FF00F276DF
                FF008E51FF000A53E217FD0BFF00F93B6FFF00C728F690EE16380A2BBFFF0085
                29F10BFE85FF00FC9DB7FF00E3947FC294F885FF0042FF00FE4EDBFF00F1CA3D
                A43B858E028AEFFF00E14A7C42FF00A17FFF00276DFF00F8E51FF0A53E217FD0
                BFFF0093B6FF00FC728F690EE16380A2BBFF00F8529F10BFE85FFF00C9DB7FFE
                3947FC294F885FF42FFF00E4EDBFFF001CA3DA43B858E028AEFF00FE14A7C42F
                FA17FF00F276DFFF008E51FF000A53E217FD0BFF00F93B6FFF00C728F690EE16
                380A2BBFFF008529F10BFE85FF00FC9DB7FF00E3947FC294F885FF0042FF00FE
                4EDBFF00F1CA3DA43B858E028AEFFF00E14A7C42FF00A17FFF00276DFF00F8E5
                1FF0A53E217FD0BFFF0093B6FF00FC728F690EE16380A2BBFF00F8529F10BFE8
                5FFF00C9DB7FFE3947FC294F885FF42FFF00E4EDBFFF001CA3DA43B858E028AD
                7F11F85F59F096A11D86B967F65B9922132A79A8F94248072A48EAA7F2AC8AA4
                D3D5005145140828A28A008A8A28AF28D0968A28AF54CC28A28A00E8E4FB2687
                A569ADFD9D6F79757B09B8792E4332A2EF650AA011CFCB924FAD4DE1ED26D6F2
                092EAF6CD8C5793FD920281B10123264FA292839CF53E95976DAE3C5611D95CD
                8DA5F410B16845C07CC79E4805194E09E707229971AE5E4D1C1144DF6486142A
                91DB332AF2C5893CF2727F4141938CAD636F46D20A69BA81934EB29EF2DAF638
                1D6F67F295176C9BB92EBCE547BF5A490E8693EB6969A559DD5BD945E6C12BCB
                3E5F33469838900200720600E80F3CE72350D7EEB538278E78E01E7CB14D2BA2
                9059D10A03D7B8624FBFA552B7BC92DA0BB890295BA8844E48E400EAFC7BE507
                E19A03924F566FF87B4EB3BA5B9D42FAC59ED65996DA28E10E4445CE59C72490
                8BEA4F2C339A8ED6C534BB3D70DED9417175652C510136EDA32CC091823AE056
                5B6B37BF62B5B486530436E1B02162BBCB1C966E793D07D00ABBFF00093DC3F9
                FF0068B2B3B8F3E28A397CD57F9CC630AE70C3E6C75EC71D280719DD9A90691A
                65EFD8F56FB2986D5ED6E2E26B3491B05A1ECAC790AD91DF239E6A921B4D7349
                D4E4FECEB6B3B9B2884F1BDB6E5565DEAA5581273F78107AF1549BC457FF006F
                82E90C51882331450A46046A873B976F7072739E4E7AD36E35C7934F96CADACA
                D2CA199834DF670F9971C804B33719E703028172C8E97C4FA4E9F636BAA32D95
                A42B0CC915A3DB48CCFB8E09120DC40F97711900E4570D5AD71E21BBBA9F5392
                58A0235103CE4DA76A90410CBCE4118F7EA6B2682E9C6515691F7D514515E51B
                8514514005145140051451400514514005145140051451400514514005145140
                0514514005145140051451400514514005145140051451400514514005145140
                0514514005145140051451400514514005145140051451400514514005145140
                1F02D14515EA998577175A4E9F168BE7496768B0AE9514E658E463389D946D25
                437DD2C40248C735C3D6B2F886EC5CA4C628182D90B268CA9DAF185DBCF39CF4
                39047205044E3276B16E4D3ED46B5E1F80423CBB98ADDA65C9F9CB361BF3A920
                3A7A2EB49268D673358A3491BBC9302DFBF44C36D900C0573D00E83DF34ED7C4
                53DAC76B9B4B49AE2D062DAE65562F10C9231860A7049237038ACF86F65822BC
                41B5BED7108A42D9271BD5F23DF283F5A09E593DCD9F053AC9F11FC3AEB1AC6A
                DABDB10884E1479CBC0C92703DC935F6C57C49E05FF9283E1AFF00B0ADAFFE8D
                5AFB6EB8F13BA3788514515CC505145140051451400514514005145140051451
                400514514005145140051451400514514005145140051451401F31FED17FF250
                6C3FEC151FFE8D96BC86BD7BF68BFF00928361FF0060A8FF00F46CB5E435E8D2
                F8110F70A28A2B410514514011514515E51A12D149B87AD1B87AD7A7ED21DC8B
                0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1
                EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4
                526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB4
                7B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526
                E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B4
                8770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1E
                B46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B4877
                0B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46
                E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0
                B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1E
                B47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B45
                26E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47
                B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E
                1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48
                770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB
                46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770
                B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E
                1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B
                4526E1EB46E1EB47B48770B0B4526E1EB46E1EB47B48770B0B4526E1EB46E1EB
                47B48770B0B4526E1EB46E1EB47B48770B11D14515E6167FFFD9}
              mmHeight = 12435
              mmLeft = 794
              mmTop = 0
              mmWidth = 200819
              BandType = 8
            end
            object lbl_NombreCaptador: TppLabel
              UserName = 'lbl_NombreCaptador'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_NombreCaptador'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 30163
              mmTop = 3969
              mmWidth = 94456
              BandType = 8
            end
            object lbl_RutCaptador: TppLabel
              UserName = 'lbl_RUTCaptador'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_RUTCaptador'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 141288
              mmTop = 3969
              mmWidth = 47625
              BandType = 8
            end
          end
          object ppSummaryBand1: TppSummaryBand
            AlignToBottom = False
            mmBottomOffset = 0
            mmHeight = 82286
            mmPrintPosition = 0
            object ppImageImgBoleta: TppImage
              UserName = 'ImageImgBoleta'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              AutoSize = True
              MaintainAspectRatio = False
              Visible = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                00250000002308020000009CA55449000000097048597300000B1300000B1301
                009A9C180000000774494D4507DC0806032B24F406C508000003F84944415478
                DAB5965928B45118C7DFB1670DA5148A5228E2CA25C285286B21C99628B2DE50
                B2454AB266C9927D19234B498A1B910B37DCE04242F622BB61ACFF713EC7F9DE
                6FCC3733EFCCB9787BE79973CEEF7D9EF37F9EE7883E3E3E38DD8FA7A7A78383
                03272727912E78D853241291E7FBFBFBEEEE6E6B6B6B5353534747874E7814F9
                F0F0303232D2D8D8B8B5B5151313939696A643FF363636EAEBEBC7C7C79F9F9F
                D3D3D32B2A2AECEDEDB5CC23BB01B6B0B0909F9FBFBDBD0D4B666666434383B1
                B1B1FC2F2DF2885B78E9EDED2D2C2CBCBFBFC77B5C5CDCC0C0C0CF890AE7B1EA
                B8BDBD1D1E1E2E2D2DBDB8B8D0D3D34B4D4DEDEAEA8264F04E2608E5B1B0E3E3
                E39A9A1A48117600B2B2B29A9B9B5F5F5F0D0C0CA8EB5AF08F7C3ED22B2F2F6F
                7A7A9AC26A6B6B4D4C4CD8380BE2B19E9D9D9D41EB737373E4AFE4E4E4BABA3A
                5B5B5B962494479180858585ADAFAFC351180303037166CECECE0A9708F5EFF2
                F23222226265658518EDECECFAFAFA424242786114CA23AB2010E81E194D8C08
                E0D8D858505090429250DEFEFE3E60535353C4626E6E5E5959891C67CF550B3C
                5A18218A898909EEAB9AC0889C2B2B2B137D0D25CBD5E311984C26CBCECEEEEE
                EEA66B2323237B7A7AACADADB95F8E8DCFA3937E9BCD46A9ADAD2D3737F7EDED
                8DACF5F2F242DA4190CA497CFF68A955EE1CE407E7A45229513F34323A3A1A1C
                1CAC6284E4FD10BBA0EADCDDDD415D9B9B9B515151FEFEFEB4E2B19FB2BABA1A
                1B1B7B7272426018E5E5E538B6FF86F18787B0BCBCBC80545C5C7C7A7A0A938D
                8D0DFA56626222902C0C18C0906A34B048EDC9C9490B0B0B1561F27D50D1F181
                9D9D9D901CB53A38380C0D0DF9F9F9D18DA0119C99FC42F0BDAF9999D9E2E2A2
                AFAFAFEA30396F6D6D0DD1BFB9B9614387111D1D8DB204C991ED9067E864A072
                DF0980268794509E6D0A7868F6B8CF141414CCCFCFD395781A1A1AC269EC8849
                878787010101984661A1A1A1333333FAFAFA6A39F7A3979D9D1D5C31969696FE
                58BF364D48484033B3B4B44C4A4A1A1C1CA46B70C088A48F8F8FBA308EE40359
                866E026FD097D90AB0BCBC7C7E7E0EDEE3E323B5E3BC214B0D601C2FDF737272
                DADBDB911BD4C59494148944829B08BD09C02D48D4D4D4545D9202DEF5F5B58B
                8BCBD5D5157F1273ED8070D0803483FD154FF2ECEFEF475459002B22FC8513D5
                D8394E61BDB6B2B24252FEEBA2ABABAB582CF6F6F6D618A698575D5D5D5252C2
                8B245E704341460A81F179246E7B7B7B6E6E6E24B5E970747444827A78786826
                4B65FE41FA4545452D2D2DD439607027CFC8C8303232D2328FFC842B28CDE818
                28D9F1F1F1B837200D38959B807AFE611C1D1DE18A87DCAFAAAA0A0F0F57A571
                0BE2C1B3D9D9597777774F4F4F0D8AA492F1090BE9EBAEC5A48C1D0000000049
                454E44AE426082}
              mmHeight = 9260
              mmLeft = 1323
              mmTop = 47361
              mmWidth = 9790
              BandType = 7
            end
            object pshpShpNo: TppShape
              UserName = 'pshpShpNo'
              Brush.Style = bsClear
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 4233
              mmLeft = 1588
              mmTop = 52123
              mmWidth = 6350
              BandType = 7
            end
            object lbl_CelularS: TppLabel
              UserName = 'lbl_CelularS'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_CelularS'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 132292
              mmTop = 24871
              mmWidth = 18521
              BandType = 7
            end
            object lbl_CodS: TppLabel
              UserName = 'lbl_Cod1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Cod'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 21696
              mmTop = 24606
              mmWidth = 9260
              BandType = 7
            end
            object lbl_ComunaS: TppLabel
              UserName = 'lbl_Comuna1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_Comuna'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 21696
              mmTop = 14023
              mmWidth = 87577
              BandType = 7
            end
            object lbl_DireccionS: TppLabel
              UserName = 'lbl_Direccion1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_Direccion'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 21696
              mmTop = 4233
              mmWidth = 177007
              BandType = 7
            end
            object lbl_EmailS: TppLabel
              UserName = 'lbl_Email1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_Email'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 21696
              mmTop = 36248
              mmWidth = 177007
              BandType = 7
            end
            object lbl_FechaS: TppLabel
              UserName = 'lbl_Fecha1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_Fecha'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4763
              mmLeft = 116681
              mmTop = 70379
              mmWidth = 64294
              BandType = 7
            end
            object lbl_NumeroTlfS: TppLabel
              UserName = 'lbl_NumeroTlf'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_NumeroTlf'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 34660
              mmTop = 24871
              mmWidth = 72231
              BandType = 7
            end
            object lbl_plbl37: TppLabel
              UserName = 'lbl_plbl37'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'DIRECCI'#211'N'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 1323
              mmTop = 4763
              mmWidth = 17992
              BandType = 7
            end
            object lbl_plbl38: TppLabel
              UserName = 'lbl_plbl38'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'COMUNA'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 1323
              mmTop = 14817
              mmWidth = 15610
              BandType = 7
            end
            object lbl_plbl39: TppLabel
              UserName = 'lbl_plbl39'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'TEL'#201'FONO'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 1323
              mmTop = 24871
              mmWidth = 16933
              BandType = 7
            end
            object lbl_plbl40: TppLabel
              UserName = 'lbl_plbl40'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'CELULAR'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 112977
              mmTop = 24606
              mmWidth = 14552
              BandType = 7
            end
            object lbl_plbl41: TppLabel
              UserName = 'lbl_plbl41'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'E-MAIL'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 1323
              mmTop = 36248
              mmWidth = 11377
              BandType = 7
            end
            object lbl_plbl43: TppLabel
              UserName = 'lbl_plbl43'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Deseo recibir mi boleta / factura por e-mail       '
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 11642
              mmTop = 52123
              mmWidth = 75936
              BandType = 7
            end
            object lbl_plbl44: TppLabel
              UserName = 'lbl_plbl44'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Modalidad de Pago'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4763
              mmLeft = 128588
              mmTop = 52123
              mmWidth = 36513
              BandType = 7
            end
            object lbl_plbl45: TppLabel
              UserName = 'lbl_plbl45'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'PAT/PAC'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 166159
              mmTop = 57679
              mmWidth = 12965
              BandType = 7
            end
            object lbl_plbl46: TppLabel
              UserName = 'lbl_plbl46'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Otros'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Transparent = True
              mmHeight = 4763
              mmLeft = 186267
              mmTop = 57679
              mmWidth = 8731
              BandType = 7
            end
            object lbl_plbl47: TppLabel
              UserName = 'lbl_plbl47'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Firma Titular'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4763
              mmLeft = 11906
              mmTop = 75406
              mmWidth = 64294
              BandType = 7
            end
            object lbl_plbl48: TppLabel
              UserName = 'lbl_plbl48'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 4763
              mmLeft = 116681
              mmTop = 75936
              mmWidth = 63765
              BandType = 7
            end
            object lbl_plbl49: TppLabel
              UserName = 'lbl_plbl49'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'C'#243'd.'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              mmHeight = 3969
              mmLeft = 22225
              mmTop = 29633
              mmWidth = 5821
              BandType = 7
            end
            object lbl_plbl50: TppLabel
              UserName = 'lbl_plbl50'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              mmHeight = 3969
              mmLeft = 34396
              mmTop = 29633
              mmWidth = 10583
              BandType = 7
            end
            object pln10: TppLine
              UserName = 'pln10'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 2910
              mmLeft = 130704
              mmTop = 29633
              mmWidth = 67998
              BandType = 7
            end
            object pln11: TppLine
              UserName = 'pln11'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 3440
              mmLeft = 116681
              mmTop = 75406
              mmWidth = 64294
              BandType = 7
            end
            object pln4: TppLine
              UserName = 'pln4'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 3969
              mmLeft = 20373
              mmTop = 8731
              mmWidth = 179388
              BandType = 7
            end
            object pln5: TppLine
              UserName = 'pln5'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 2910
              mmLeft = 20373
              mmTop = 18521
              mmWidth = 88900
              BandType = 7
            end
            object pln6: TppLine
              UserName = 'pln6'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 2910
              mmLeft = 20373
              mmTop = 40746
              mmWidth = 179388
              BandType = 7
            end
            object pln7: TppLine
              UserName = 'pln7'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 3440
              mmLeft = 11906
              mmTop = 75936
              mmWidth = 64294
              BandType = 7
            end
            object pln9: TppLine
              UserName = 'pln9'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Weight = 0.750000000000000000
              mmHeight = 2910
              mmLeft = 20373
              mmTop = 29369
              mmWidth = 88900
              BandType = 7
            end
            object ppImageImgOtros: TppImage
              UserName = 'ImageImgOtros'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              AutoSize = True
              MaintainAspectRatio = False
              Visible = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                00250000002308020000009CA55449000000097048597300000B1300000B1301
                009A9C180000000774494D45DC070806032B24253B7E5B000003F84944415478
                DAB5965928B45118C7DFB1670DA5148A5228E2CA25C285286B21C99628B2DE50
                B2454AB266C9927D19234B498A1B910B37DCE04242F622BB61ACFF713EC7F9DE
                6FCC3733EFCCB9787BE79973CEEF7D9EF37F9EE7883E3E3E38DD8FA7A7A78383
                03272727912E78D853241291E7FBFBFBEEEE6E6B6B6B5353534747874E7814F9
                F0F0303232D2D8D8B8B5B5151313939696A643FF363636EAEBEBC7C7C79F9F9F
                D3D3D32B2A2AECEDEDB5CC23BB01B6B0B0909F9FBFBDBD0D4B666666434383B1
                B1B1FC2F2DF2885B78E9EDED2D2C2CBCBFBFC77B5C5CDCC0C0C0CF890AE7B1EA
                B8BDBD1D1E1E2E2D2DBDB8B8D0D3D34B4D4DEDEAEA8264F04E2608E5B1B0E3E3
                E39A9A1A48117600B2B2B29A9B9B5F5F5F0D0C0CA8EB5AF08F7C3ED22B2F2F6F
                7A7A9AC26A6B6B4D4C4CD8380BE2B19E9D9D9D41EB737373E4AFE4E4E4BABA3A
                5B5B5B962494479180858585ADAFAFC351180303037166CECECE0A9708F5EFF2
                F23222226265658518EDECECFAFAFA424242786114CA23AB2010E81E194D8C08
                E0D8D858505090429250DEFEFE3E60535353C4626E6E5E5959891C67CF550B3C
                5A18218A898909EEAB9AC0889C2B2B2B137D0D25CBD5E311984C26CBCECEEEEE
                EEA66B2323237B7A7AACADADB95F8E8DCFA3937E9BCD46A9ADAD2D3737F7EDED
                8DACF5F2F242DA4190CA497CFF68A955EE1CE407E7A45229513F34323A3A1A1C
                1CAC6284E4FD10BBA0EADCDDDD415D9B9B9B515151FEFEFEB4E2B19FB2BABA1A
                1B1B7B7272426018E5E5E538B6FF86F18787B0BCBCBC80545C5C7C7A7A0A938D
                8D0DFA56626222902C0C18C0906A34B048EDC9C9490B0B0B1561F27D50D1F181
                9D9D9D901CB53A38380C0D0DF9F9F9D18DA0119C99FC42F0BDAF9999D9E2E2A2
                AFAFAFEA30396F6D6D0DD1BFB9B9614387111D1D8DB204C991ED9067E864A072
                DF0980268794509E6D0A7868F6B8CF141414CCCFCFD395781A1A1AC269EC8849
                878787010101984661A1A1A1333333FAFAFA6A39F7A3979D9D1D5C31969696FE
                58BF364D48484033B3B4B44C4A4A1A1C1CA46B70C088A48F8F8FBA308EE40359
                866E026FD097D90AB0BCBC7C7E7E0EDEE3E323B5E3BC214B0D601C2FDF737272
                DADBDB911BD4C59494148944829B08BD09C02D48D4D4D4545D9202DEF5F5B58B
                8BCBD5D5157F1273ED8070D0803483FD154FF2ECEFEF475459002B22FC8513D5
                D8394E61BDB6B2B24252FEEBA2ABABAB582CF6F6F6D618A698575D5D5D5252C2
                8B245E704341460A81F179246E7B7B7B6E6E6E24B5E970747444827A78786826
                4B65FE41FA4545452D2D2DD439607027CFC8C8303232D2328FFC842B28CDE818
                28D9F1F1F1B837200D38959B807AFE611C1D1DE18A87DCAFAAAA0A0F0F57A571
                0BE2C1B3D9D9597777774F4F4F0D8AA492F1090BE9EBAEC5A48C1D0000000049
                454E44AE426082}
              mmHeight = 9260
              mmLeft = 187590
              mmTop = 47625
              mmWidth = 9790
              BandType = 7
            end
            object ppImageImgPATPAC: TppImage
              UserName = 'ImageImgPATPAC'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              AutoSize = True
              MaintainAspectRatio = False
              Visible = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                00250000002308020000009CA55449000000097048597300000B1300000B1301
                009A9C180000000774494D45DC070806032B24253B7E5B000003F84944415478
                DAB5965928B45118C7DFB1670DA5148A5228E2CA25C285286B21C99628B2DE50
                B2454AB266C9927D19234B498A1B910B37DCE04242F622BB61ACFF713EC7F9DE
                6FCC3733EFCCB9787BE79973CEEF7D9EF37F9EE7883E3E3E38DD8FA7A7A78383
                03272727912E78D853241291E7FBFBFBEEEE6E6B6B6B5353534747874E7814F9
                F0F0303232D2D8D8B8B5B5151313939696A643FF363636EAEBEBC7C7C79F9F9F
                D3D3D32B2A2AECEDEDB5CC23BB01B6B0B0909F9FBFBDBD0D4B666666434383B1
                B1B1FC2F2DF2885B78E9EDED2D2C2CBCBFBFC77B5C5CDCC0C0C0CF890AE7B1EA
                B8BDBD1D1E1E2E2D2DBDB8B8D0D3D34B4D4DEDEAEA8264F04E2608E5B1B0E3E3
                E39A9A1A48117600B2B2B29A9B9B5F5F5F0D0C0CA8EB5AF08F7C3ED22B2F2F6F
                7A7A9AC26A6B6B4D4C4CD8380BE2B19E9D9D9D41EB737373E4AFE4E4E4BABA3A
                5B5B5B962494479180858585ADAFAFC351180303037166CECECE0A9708F5EFF2
                F23222226265658518EDECECFAFAFA424242786114CA23AB2010E81E194D8C08
                E0D8D858505090429250DEFEFE3E60535353C4626E6E5E5959891C67CF550B3C
                5A18218A898909EEAB9AC0889C2B2B2B137D0D25CBD5E311984C26CBCECEEEEE
                EEA66B2323237B7A7AACADADB95F8E8DCFA3937E9BCD46A9ADAD2D3737F7EDED
                8DACF5F2F242DA4190CA497CFF68A955EE1CE407E7A45229513F34323A3A1A1C
                1CAC6284E4FD10BBA0EADCDDDD415D9B9B9B515151FEFEFEB4E2B19FB2BABA1A
                1B1B7B7272426018E5E5E538B6FF86F18787B0BCBCBC80545C5C7C7A7A0A938D
                8D0DFA56626222902C0C18C0906A34B048EDC9C9490B0B0B1561F27D50D1F181
                9D9D9D901CB53A38380C0D0DF9F9F9D18DA0119C99FC42F0BDAF9999D9E2E2A2
                AFAFAFEA30396F6D6D0DD1BFB9B9614387111D1D8DB204C991ED9067E864A072
                DF0980268794509E6D0A7868F6B8CF141414CCCFCFD395781A1A1AC269EC8849
                878787010101984661A1A1A1333333FAFAFA6A39F7A3979D9D1D5C31969696FE
                58BF364D48484033B3B4B44C4A4A1A1C1CA46B70C088A48F8F8FBA308EE40359
                866E026FD097D90AB0BCBC7C7E7E0EDEE3E323B5E3BC214B0D601C2FDF737272
                DADBDB911BD4C59494148944829B08BD09C02D48D4D4D4545D9202DEF5F5B58B
                8BCBD5D5157F1273ED8070D0803483FD154FF2ECEFEF475459002B22FC8513D5
                D8394E61BDB6B2B24252FEEBA2ABABAB582CF6F6F6D618A698575D5D5D5252C2
                8B245E704341460A81F179246E7B7B7B6E6E6E24B5E970747444827A78786826
                4B65FE41FA4545452D2D2DD439607027CFC8C8303232D2328FFC842B28CDE818
                28D9F1F1F1B837200D38959B807AFE611C1D1DE18A87DCAFAAAA0A0F0F57A571
                0BE2C1B3D9D9597777774F4F4F0D8AA492F1090BE9EBAEC5A48C1D0000000049
                454E44AE426082}
              mmHeight = 9260
              mmLeft = 169334
              mmTop = 47625
              mmWidth = 9790
              BandType = 7
            end
            object pshp1: TppShape
              UserName = 'Rect2'
              Brush.Style = bsClear
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 4233
              mmLeft = 169334
              mmTop = 52652
              mmWidth = 6350
              BandType = 7
            end
            object pshp2: TppShape
              UserName = 'Rect1'
              Brush.Style = bsClear
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 4233
              mmLeft = 187590
              mmTop = 52652
              mmWidth = 6350
              BandType = 7
            end
          end
        end
      end
      object FContratoSuscripcion: TppSubReport
        UserName = 'FContratoSuscripcion'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ParentPrinterSetup = False
        PrintBehavior = pbSection
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 7673
        mmWidth = 203200
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object pchldrprt9: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '12.04'
          mmColumnWidth = 0
          object ptlbnd6: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 15875
            mmPrintPosition = 0
            object ppImage2: TppImage
              UserName = 'Image2'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0D544C4D44504E474F626A65637489504E470D0A1A0A0000000D494844520000
                009C0000002F0802000000998434C1000000017352474200AECE1CE900000004
                67414D410000B18F0BFC6105000000097048597300000EC300000EC301C76FA8
                6400001E3C4944415478DAED9C075C1447DBC06F0FAED0BB74057B41A404C528
                F60A268A051B364404A5496FD27BEF0A0A82220611C5466CD8308A0D44B060EC
                200888D2EEB87EDFB37B709C08C42498F77D3F33FE7239F666A7FD9F79CAECCC
                225C2E87C56452A83404417068E20A0909898989E17889CBC1FE8730188C4FAD
                6D2422515A5A02C7859C5CDE75F8D2D2DA4E673064A5A50804027CA1D1E8DD45
                7D96B85CAEA80889402062F721EDEDED1D944E2121BCBC8C345E58087EC6F597
                10844E837F0C7EB124128144227FDE42AC310897D24165B139820DC0EA2543DB
                E0775A27ED536BBB84B8A8B8B828AEFF0AB1A2701D1D14369B032D141717FFBC
                225E163C0EC7696BEB8086138904111191CF33A02534377F6230592422415656
                1AAB8E5F25C2E1B03FB6B4C11518373C5E887747473B85831682409B09046151
                51D12FFAC8AB172EB06BEA1A3A3AA85C1C574C54444549814824F173224C06C3
                272CF97EE5133289C4E1A09700AA8C94C4D41FB4D72D5F2C292989350589DD9B
                9D7BFC9C869A72A4AFE3708DA15DD520F8A7CF5EB805C57F686ED9BCFA27AB4D
                66BEE12937EF559049448C05C21F56F8042AB37ED4F771DA86C3A17779852635
                B7B4C24FC673A67B3858A2B5F4C915C1BF7AFDD63D38B18342C5E3F190054A25
                1208F272D2063A137E5E384B71883C7A235A15527CBD343A351BDA8F558AE3D5
                4FA176AE30996B6BB98EC562ED0E4BBAFCDBFD31C387C605BBCAC9CAF4E6D42D
                43F05F6EC19903BF9C061E30263A13467B395A8A8B8BF5B410C137363507C5A6
                3D7F5503F945C9240F7B0B03DD893D0522F8C2B39712337E81DBA13D0E966B7F
                5E3C47F0D7A28BD72353B2019FB3F5869F16CE866BC74E9DDF7FB8505898D778
                2E7C91919634D4D75EB36CA1B494147FC0A117F9A72E9CB970ED754D7D5B0705
                AE898B89AA282ACC9B396593D94FA81C7039484747C7CAAD2E0F1FFF2E224286
                1640712C161B3EA1290B661A4607384B494AC09D8EDE11D9F9A787A9A9642506
                EA688DE5D771FB5EC56607DF4FAD1D96EB97067BDAAFB172BD547247944C8651
                26080BE311044A64B258808C4AA399CC9D9E95148420F8E48CDC90F84C111112
                34514D79484146B4B292623F438CBF535E6966E90A44E12F369B8D7D72586CB6
                0899347AF8303F67AB1FA7E8F1246FFFA163BE517B6188D91CB42BBC99D1DE41
                B55C6F1AE9E7C460D0D7DB785DBF5DAEA1A674FC40ACAAB2527F35C28DAE01B1
                078F9E95101361B2D930E9E2029C4C97CC17A4929153E01BB907E613348CC960
                86EF7658BF7249F7B0206C16DBD2C9FFFCD55B226432B59366326F7A5AF46E54
                DA786201B7E71EF78D4885F1F175DEBE6DC34AB8E61F95BAEFD0713299CCE6B0
                211BF411868E202C34638A6E6C90ABBC9C2CE4A1523B03A2F71E3B7309D42181
                4890101385F23A285D1AC578EEF4F0DD8E9212E22854F31DDE772B1E698D1D19
                E2614B26139FFCFE2AFD60C1AB9A3A8011B5DB61D5D245509C5B40ECA1636787
                A929EF8BF19D387E341FEABDF2CA6DCE41A0D360A6FABBED78F8E829DC083309
                6A4A39905F5BD7A03954C56AC30A90260683396298AAD6F831542AD5D229E0D6
                BD87B2529274260B147BA09BCD9AE5C6FD0DF1FD8A471B767AC3ED26F38D36AE
                5A02385FBE795774E9C6EDB24A3A83A9A632645FB4EFF8B123216FD691C2E0F8
                7DD04FBBAD6B664D33A0D3E93C09505755525755A6D168506F090A55F997B4F0
                01C408A07A87261D2E28521A220FD2D1F8E1E392F94689A19E783C8269003C74
                61BB4BD0B5D2B2E14355A1EF30D6C11E3B579B2EE60FCBE3A7CFB738FA7D6A69
                17171301A8D292E2D9898163468FE0673878F45440741A40F576B4DCBC76195C
                0B8DDFB7FFF009B014EEB69B7FD0D1FAFDE59B8CDC138FAA5F824573DCB6CE79
                C766C893B4EF70F4DE43C06FA486BA95F90A9D8963C17ADE29AB4A3F54F0AEBE
                0978395B9BDB5B997741BD5D5E65A8AF75FC403CAF5FBF9CF8D5233811641050
                F9BA587F0DD44D664B02DC77F247864EA7ADB00005F06CB29E566E6A189144E2
                FF5406906C77436B80D0C5EBA5D5CFDF982E9E1D17EC262C2CDCC7287743EDA4
                D16D36AF72B3B5E05DA650A97E11A985E7AED2E874F315C661DE0E081ECF83CA
                60B0F6C7FA2E983DAD57497F16EAC1FC33E3470F1F3B42A3A8F886829C4C7652
                D0A8111AE82D08FE6ED9C34DF6BE20BB4B16CCB874BDB4A9B925D4D3B60B2AA6
                BD4167F847A78D1A3E14B4DDE1E3E7DA299400176B0C1E9727167D42DD97731C
                0CE4A1E4205DED0970E5CA8DDB0E3E51E0CACC9CAA9F931A5657FF7E9D8DD7AB
                B7752A4AF27B22BC74268EE737B9F8DA2D27FF58706EC68FD6CC4B8FEC813A45
                4FEB78660C8219EDD3E7AED879478051DCB1D9CCD576CB9F81DAD5E896D656D0
                75554F9FEB4F1A7F203E404A4A92DFE1E894CCB8F4DC09A3871F4808084BCC3C
                7DA1445951FE706AC808CD610343DD66BEDCCB711B66A5D1917D5B5BB766BB47
                7D4393E650D59C94101565453ED48C38BFF9B37EEC2904CBFF67A1661F3D3D69
                FC682BF3E581B1FB9A9A3F05BA0115535E96E098BDC9078ECE9966B0EAE7F981
                B1E96DED1401A878069D6EE1E877F1FAED0DAB4CECB7AE05FC2FDFD41A19EA65
                25040A83BF86E51900EABE98DD530D74E1CACB576F3739F8BE7AF3CE688AEE91
                F4C833E7AFDAF944802D58BB6C6188B74357BF780DE672D7EFF0FCEDCE03D088
                B929A13D507F98340E6A251089BFBF781D9A9071F7C123511191CC38BFA99375
                070F2A1E5CCA0DB65EA5F7ABD69A2E8A0D74CDC93FED139E02C6C6DF65FB06B3
                9FFB7097FA870A39B7390514DFB8033EF9E13D217ADA1330A8FB41164D8D674F
                1C379AC36633992C156585C573A72188D05F803A7ED4F09470CFA0B874200408
                61B20A0B139A9B3FAEB5F67CF4EC8587EDE6297ADA16BBFCA16D82501F563DDD
                60E7D3DADE11EE6DBFC6D478D7EEC8A3A72E2AC9CB1E480CD49E30E60FA1F267
                2A3853AE81716DEDD49F1618A546FA2482EEDD73904C2406B8DAAC5D610CBE77
                7708824E1530C987F2CF82CF9111EBD70515F4A48484B8AC34F8BAB8D6B67610
                3D395969CB75CB2CCD57F09CC8C1827AEDE6DD1DEEA1600BE3039D972C9C5DFD
                FB4BF0B3DEBD6F9A3DCD00B2E1515782F3B550B1A13F72E21CD898030981D3A7
                E8F1A08203D5D9490303030DA17476EA6B8F030D0461C95F803A4A63E8E99C84
                43F96742133261400EC4FBC1709F387BC925200EC2B3A3FBA240E36D730EEC05
                357EEFC198BD3960C87353433486A9E79F3CE71D9602B1CDAEEDEB1DACCC0780
                CAB7A920916FDFD5A51D2C00FF067E8AF1DF656A32DF3F6A4FD62F274182E383
                5C16CF9B21E0B5A150E3D30E2567E681102785B87541AD7CF2BBA8A8088C0EC4
                8E401B5CF91D5BCCEC2CD7F39417DC330850B17242E3D29332F3268E1D519815
                278A45C3F65EE1C7CF1643A49519EFA7356ECC9F82EA11140F93008CF1C1C420
                4383493CA8100C802D5452407D1C1A833E6EA4A6CB8E8D42C284BF0015FC9193
                D971750D4DEB6DBC1B9B3F3A6D3777B2D964EF199677EAC29CE99373F7869594
                96818CF640C5A1D1ED56477FF0A1404982A30057DADADA965BB8001ED0A2FB63
                FDD0D00887EB136AE6919310580205C08445FF4C10D075A68BFC5C6D20CEF68F
                4CCDCA3B0D31715CA0B3C9FC99BDA0C6A666A766E73398CCBD11DE7CEFF7B1D6
                D811E1DE763069E2D272C0BD8448232ED0455F478B07C3D53F26A7A008A042B3
                B4C68DE243BD5F5165E914086EDE46B325817F00151ADABAC6CAA3FAC5EBB123
                358CE74D171602652674FD56D9DDF247E0EF78D859586F36C3D42FF76BA0B2D9
                2CB85E7ABF524A523C373574DC98913C9BCA64B241672E5930537085043EFF1A
                D4FC7D91F2F2B2EBAC3DAE9796194DD675B23677F28B7D5DF3CED769BBD5A655
                574A6E83F32108B5F4EE83AD4E814C2673DA141D43BD89BCD8AAF0D7CB6F6AEB
                C1B13A10EF6FA0A73D30547151112027264AD65057996B3479D9E239A825C6E1
                62F76427EC3B02F36DB793D506B39F7AA95FD780988233C5E0DB1E4A09FECC51
                3A91857ABF278B8ADD831360CAFEBC70665298879090305CF40E4DC8CA3B2327
                2B951CE23EE347033ED4925BF760A6824C6DDFB8DCC3DE7260A897AEDDB4F78E
                44C599DAD941A1A28B125C1CC4C74005F4C6645D2D70707A2FCDF409154B1555
                4F2C1CFC3F7C6A01050BEA17AAE0434D8DF032993F630047292F3D424971488F
                F470B9FD4105FC90333DFB68486226C460208E0F1E3D939410CB4A080031BA5C
                72DBFE73A89149197BB28F91C924B062709D37E412E2A212E26210DBD86C5CE9
                6A67D11F549E4D85A905734958082F272385C7069FD7FEBCC25FDD0213400FAD
                34991BE1E724E828B5B777ACD9EE5EF9E4B9BAAA2274AD07EA64DD0947D32321
                F6A07576AEB7F12C7FF44C424C2C2B3140571B759DD3B28F46246741DF572F9D
                1FEA658FC7566D40517B8726E69DBC00BE094CEBA5FC45937EA07A06C51F3EFE
                ABA282ECECE906A26474AD03C156276EDFAF7C5D53472611C11341ABEB07AACD
                A6556E765D21CDAB37353E61C9A565554C16D3D56693DD36D452F0BD5FB0110B
                E7F41BD268AA291FCB8C969793FB8C7A3F508FEC0D83395DF5B87AB3835F3BA5
                1308818A9B33DD203DC60FA6452FA8144AC7EA6DEE95D52F46690E9D663089B7
                9806D93AA8D4925BE54D1F3F698D19014A1B0CFC005021A2C514E4E70D43F04F
                AA9F8323DDF4B165889C4C4290ABA1810EBFF50969392959F9341A1D5CAAE430
                AF3EA042AE8C9C82A0B87D0882DF686612E086469FCF5FBED968E753DFD00C0E
                82F1DCE940052E42978A8A6F428BB5C60CCF4E0A5690971B006AFDFBC68D76BB
                1F3F7B397FC694ACC440DE1A262FED3990179E740064D01A04198DA03802ABA4
                5D50592C3684BC738DA6804FFBB6AEA1E4D6FD5735757071DAE449C9619EBC05
                9703B9852109FB61DC4D4DE6E8688D653298388CC1C4B1236108E8349A8553C0
                ADBB1530037E5E384B52421C8AA2311810B718CF37EA599BED0B2A93C1B07209
                BE7AF32E0C3A8DCE08F6D8016E2D6F0404A1165FBFE5E01DD54EA53AA13ED106
                7E07A1A2ED2E41E7AEDE9492104F08769B3BC3F08F439ADEBE0502ADF38B4CC9
                C054B48AA2C20A93B963476BD2E90CB0EB10498366559097498FF2D19B34015D
                585FBEC5B9B4AC12D4EFD99C241E548890566FF778F5B656435D352F2D7CC4F0
                6170F168E1B9D0C40C08B4C14AA06B765C1C9BC301121026867AD91A19FED063
                0E11FCA79616D3CD4EE07FFDA003B212212525957FEABCAD6738EA2B79D96E5D
                BF5CB0C10F2A9F98EFF4067FC460D20450773232D282127AB7BC728585337C83
                EA180C066FF0D1557219A91953F53CED2DD45494BB97090BBCC29345C9243007
                E0FDF2969EA1B7E0B3642606B2984C886B61A6826E04A1466D1D826B6BEF80D1
                39BC270C4163240E1FAAB36FF4FE2385A335878243A7AAA20497F7A3EB82A9F0
                B39AB24241668CBAAA0A5CBC70E5372B9720801A1BE0BC7EE51217FF18000323
                9EBB37545760710013B8E33EE1A9543022EB9745FA3967E69EF00C4904A8109F
                806D860CBE11297BB28E62214DB0D1D41FFA0CD95B3EB57884245EB8560A8205
                4A008F4760C841E1090B0969A82BC350982C980514103A9D1E959C5556F9444F
                7B9C87BD056F351C0AD8939577E5C65D7131117BCBB5D8E2053A6AA5772B4EFC
                7AF9C5EB9AB60E0A3448465A0AF489D9B205A3866BF4D2609D9D9D5129D9954F
                7FD79B380E9C0B12897CE2ECC5DC82737272D29EF65B86A9AB093A6F2C262B32
                39ABBCEAA9B2A282DB8E4D6A6ACA8250618AC7A61D6A6BA3805BC8CB0FD8D454
                8618EA6B1BEA4FC42158148402C4DF7B50B53FE7044041703D4F69A0FF33A7EA
                6F5AB314BA9079F8C46F772B4824025F13C0281B4D9EB4D57C654F888CF91D39
                F9670ACF5D012DEA61BF450A7DAA81AB7D571F9992F5BEB1199C17AB0D2B113C
                9A0D5462CCDE43204376166B26EB6B472665C2F4D09930DAC3612B914014EC05
                DC1E9E98D9F0E1A381EE04375B8B5B77CBF7661F83FA206E447D141CEEF8998B
                F9A72FC254065F7D64AFF1141858D03780E0DAADB2FA86262A9506BC64652427
                8E1B05A2892E7861BD00D4A0EBB8E07C82FCA28EA240C768B44E213C9E402408
                7A5938CC89A550C1BA20E0E0743FA4FB72D100E92A9644C6DC4F3481AE00BF0E
                35FE5F3EC6E27240DE41E2B0EA7AAF3F60C6850D06180A411FFE08A8EE2FCD21
                97D3FBD11BBA4CC6EF17978D13408E760D8FEFED72635241A7D189442282E78F
                09DA486CA044042400CF62A2CA83802164B339E0F79245C8D848F6EE2387CDA2
                33182402110FD289E0994C0614432012F91A824EA7A14F418484FB262A301AD0
                80E6E64F20AF60B0A5A5C4454444052920D0929E31FA6C8090EECE7F0EACE73A
                AE8F5FFBA89EEFA475DFD86F2081FBE35FBFBED23EF3F76E7CFFA5F5D9DAEE01
                FDC25BC6F1DDFEAFEA45F78A69DF6D1B80E8D70D0806F56F264CF509FCCD19F0
                F9F3BFE99BA7BF0D15416A5AA9A5756DBCBF203E99A62AAD262532D04E867FD3
                374E7F1F2ABEBCBE25ABB20EFB8E6371B8DBB4D57494A5BE4A87FC9BBE4D1A04
                A80F1B5A8F3C7E8F7D47A16E9CA0325151F25FA8FFC13408501FBC6FC9A9C2A0
                E2712C3667CB44B5494AFF42FD4FA641805ADDDC51F4B219FB8E6373B84B472A
                8C9215EB2B76EED3F3FCC6E9BB94AD41F07E996C76279DC5FB0E9EB528519840
                10EEED0077FBFD140A1522B96F4E97CB259208A2A2BD36BA7E2F691066EAED7B
                15E14999E84206826FEFA0B8DA6D5E306B9AE0620A7CBC6F6C3C7FE556496959
                7DC3076C47E037872A242CA4AAACB068F6B4C5738D4444C8DF15D741807AE1CA
                4D1BF7106C2912DFD2DA961CEEB966D9628175545C6151717266DE8BD7B5783C
                823EFF45FE1125CCE5B2B105B339D327EF76B61AAAA6F2FD701D04A8C5D74B1D
                7C22BBA0B6B5C705BAACFC6901368228BFFD3905D1A907994C968828F91FB7A8
                E862777B3B65BAA16E729887AC8CCC77C2F59B41C51E005CB971DBCE2B028812
                89E8B37B168BDD893E1EF9E6230B0A418444424D3BB61ED2D141B1B558ED8A6E
                2FE57E0FAB22DF0E2A8ECEA05BEEF2BF711BDDB7087F32184C29297183491364
                A425B9DF72641104A1503AEF553C7EDFD4CC3B0042A333D494871C4C0A52FF3E
                94F03784FAB0EAE956E740D07E3063E80CA686BA72A0DB8E29FADAC83F6253AB
                1E3FF30A4BAE7EFE1A9404B48D46A3A7847B2E9C33FD5FA85F5340BF50CF5FFE
                CDD137125C62708EA89D34876DEBD00D9268FA0714202A3759BF148626641009
                04102370CB833C766E58F5D30027B1B0A671FA7EEA3270EAB9EBF33F05BC45FE
                6F3D0FF270BD9E5F0D9A69F88650AF8241F58E84004648488842A52D5B342B36
                C8F5ABCBE50E7CB8F16B82A2FC53177CC29309C2C200B58342F577B5DEB47A69
                57E15D95F04FB121EF1B3FB4B6B68F19A5F9B6A6AEE9638BFE2474DF4253D347
                90063299585E554D2408C33FF8534579C8284DF5078F9EC177F012C68C183676
                F48807954F6AEB1B44C8243DEDF1605FEE573C86EBBC8372743AA3FAC51B5054
                35EFDE0FD750873CBCD3822C365B6B1CBAD996C1643E7BFE6684861A1A7AFD97
                43ADADABDFB0D3A7B6BE110C1BB60306315D3C6BC982590A7232BC87E65F6044
                41494A88CBCA741FD9ECFFA9248341FFF8A9153B56DB97447071CA8A0A672F95
                0842458F61ADFAE92395CEBB033E65C904213CC22BF3E8C97377CAAAA2035CA2
                530EECCB399E93126AA0371182B1DFEE3CD8BA6ED9D98B254F5FBC7E5BFBDEC8
                5057576BAC949444504CDABC1986E0F4C195495A632C1CFC476AAA7339DC4F6D
                6DDE8E96E0F04F33D059B574210E3B125370FA9287BD8557685280AB8DA6863A
                8D46DB62EF4BA1527F498F1215156968FCE01614EFEFB25D7398FAA058876FEB
                FD06C5ECDD77F8047082A163B339605925C5C5789E703F890BDA72F488610B67
                FDB868CE740909B12F9F51D7BCAB3F73E1FAB59BF7DED635801AF8D2428300B1
                582C5FE76D440271975F340F2A85420DF3DCA16934FBC4E37764213C7A4A5008
                6FA5A33A448CCC53B3C74E9DBFF7E071B8EFAEE894ACDFEE948FD0508FF2732A
                2ABE71F15A693CB627FBE2D59BE7AFDC8A0E7086EFD76FDD2D38733921C49D57
                6343D307AF90C4187F67696929334B679BCD66303BCF5C2C490EF7825FC313F6
                0F919701C0B61EE101AED61AC3D44AEF96FF5278BEA383BAC16CC9CC69931B9B
                3E788624ED76DAA63154EDBF1B2A6657DE37345ABB859455564B8A8BF236A300
                DA815D5F50BB2C261B3EA7FEA0ED61B765E2F8318286E7D4F9AB09E9879FBFAA
                01E70BB47A9F2A18A03259AC306F5B29090947DF2841A8AA3FCECAAFAAE9822A
                8CB7D71FAA24DE05B5E0F405801AB6DB313C21435141B6E251F532E339501408
                10CF6A9CBB7CE3D2D5D2E84017F87EEBDE83C8A4ACD54B1790C9A4D9D30CA047
                F6DE1126F38C080442F1F55B6E761662A222302691BB1D41153B78477A396E95
                9596822B6002801C60D61E3792C16297573E0970B76DFAD00C507D7659FE2F40
                C57E7DF6E2B54F58F2EDF22AA2B030F4B96BF3D81F2528AD9D42D550534E0C71
                9BA4358E772EFCC8F1B381B1FB2034121DD0F6F0A0067BEC901013EB05557DDA
                AC638F6AF933D5564F5DF10BA8C1B1695A63472A0D91CB395634C350F75EC593
                486CE7F4AFC52597AEDD8EC1A096DE7B109290095E02405D34FB471030905D75
                15A527CF5E1ACF9D6EBD65350E3D7D1403AEBEA6BA0A94131BE8FCE1638B8B7F
                6CB0A7ADACB4E44E8F50472B73069311999C9D11EFCF66B15D03E3FF77A06219
                5ADBDAB28E9C3A5B5C525BD740A17672B9FD7839D8048629482691F098A9034F
                447FD2B88C587F506B15954FB638FAB75328646C132B7819E83A069BF365511C
                360A35C67F97BCAC8CC3EE4841A8CA5367E63E7C2B228C07F000D579F2306509
                912FA10E1FA6B66E8589774802A804B005419E76BDA082FA3D517435AEDBEF03
                F5EB1D9A14E5BBABE9C3A78098B4980067254585A28BD72EDFB82B232501B676
                B5A931E4710F8C8FF1777A5CFD223821E3079D0950ED9DB24A179B0D46867A0E
                3E512085D8A1814178FCF0EDA1E2BA6C615373F3FD078F6BEA1AE80C469F54D1
                9D7E1CEEC327CFC1A4F1DE4F009F9D7446B897EDAAA58B40F0F34E5E9494401F
                BCC064959692986334595D59918BEBADCEB9E87E420E4CA027BFBF760F8EE73B
                4A21EE36B38C173D6A6C1542F745E38410C44049528C28CC6B61DE895F6F9755
                C606B9F986276B0E55DDB2CE14E26C332BF739D30D52237D20CBE90B57CF17DF
                4C8E40CD6449E9BDB8B4C34EDBCDA1E4A1AACA62A2643BEF88608F9DE0E9EC74
                0FD19F38CEC27C794B4BAB85A31F08715662A0B292E2FBC62667DF98502FDBF4
                83C7C68D19819DD140728E9EAA78F4CCC7C9D2D63362D9E2D92A4A0A8AF2B2E0
                81FFCDD86610A05EBA76CB7177141F6A7C90EB0AC1D723F0897DDD9319F0EF0F
                E69D8C4FCFE515D841E9042D17ECB97389B97DDDFB2670B260160E91970DF7B6
                FB71B2DEC045E5159EF38D4CED0969DC6C36A147600553CF19A3BB65552FDFD4
                AE365D54587419A6F874435DB87A20F704E80C6CCF300EAC6CD5E3E7EB5799C0
                F7B7B575E02113D1B7D1306718EA8159CDCE3BB974D16C0505F99B77CA5FBCAE
                C10E3021878E9E6A6D6FB7DDBA1E240DE2A582339740448A4BEE2C5930437188
                0294D3D0D874FC4CF132E3D927CF5D7DF7BE111AA3A73DCED464DEDFDCBC3708
                50CF5FBEE1E4178343F7CFA250C10A9A1ACF1B70E7EAC074D143036BAD3DAB9F
                BF068B0550D177BAECB234DFE1FDA9B51D94735B3BC57CA57188973D967980AE
                23F9A7CEFB84A7F0A182E7B9118D53BF6EF1417017A7E01A421FEB099870E0F0
                9FAD57F4714BF70AC39765E23E3748FF0DEAF746E9FDEDAEC1A842C3E341DB80
                4F18EA6D8F7CE55A4C5FE9F6BD0AD0666D1D14E001052E986918E4BE63D916A7
                E68FAD009546474FBFA445F9C8A1F16EBF097430D8B0134597492422B6A2448D
                0B74C626C1202DDCF045B3679B38B7EF6C3D4B1CDDF8B85F644070DDB007A16D
                83001502C75596AE30478101B6119FBB78CE348832E56424FFD4031950746C36
                B7EAE9F3C3C78B20CCE785B3E0E0ECB236B7B35CB771A7F78D3B0F2054C0A18F
                0A98537427AC5832575D5509C15E25F57939F8E64FADE7AFDC2CBA7C03871D3A
                038D2D42266527046AF35F16F4FF3A0D0254369BED11189F77EA829424FA6630
                0009834E2212B063397F4AEE503C0CECA81A8F28F8B74A0AB23929E821FB9345
                C54EFEB1DDC5E268343ABA6A87BE4A0AF9A216100E36AF0DBCB72F81C65E387B
                6A52A807093CE77FA17E5D19F8C7D5CFB73905D6377EE0CD24F4303138B27F49
                9120BCDD11E8F332BA105E28C473E70A6CD1115D240A4FCE29F8554C4C4418E3
                8ABDCAAB9F42D0C7F3086FB189DA4983D0300D3BE3F73D10C50D0E54B418FC95
                1BA5BBC3F7BCAD7B0F71249120FC979FAFA16F486330E94CA692829CB3F506B3
                65E8ABB9F86F76894AC9CA3B79013D6644240A0B0B0D500B206732D10349CA8A
                F27ECED68BE7197D710AEAFF6D1A24A8D8CE1598AF19B985A5F71F36367D042A
                7F962A6FBCC944A2D21079437DAD8D663FA32F9710F46BB0D7EC141597E49FBA
                F8A8FA05184EFE21D45EE5406BC0882A2AC81AE84CD8BCE667EC61C8F7421437
                78507B22D1DA77F56FDFD583B7F997B60C72C1300F535346DFB281FDD9E7B146
                98B82F5ED7A207343B697D696034C095961457535552E6ADD17C1FBB58F8E9FF
                003D3A71485F0DDF330000000049454E44AE426082}
              mmHeight = 13229
              mmLeft = 1852
              mmTop = 0
              mmWidth = 51329
              BandType = 1
            end
          end
          object ppDetailBand10: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 227278
            mmPrintPosition = 0
            object ppRichText1: TppRichText
              UserName = 'RichText1'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Calibri'
              Font.Size = 11
              Font.Style = []
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'RichText1'
              RichText = 
                '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcha' +
                'rset0 Calibri;}{\f1\fnil Calibri;}}'#13#10'{\colortbl ;\red0\green0\bl' +
                'ue0;}'#13#10'\viewkind4\uc1\pard\nowidctlpar\sl-200\slmult0\qj\cf1\lan' +
                'g13322\f0\fs22 Por el presente Instrumento, el Cliente autoriza ' +
                'a Sociedad Concesionaria Ruta del Maipo S.A. (en adelante la Con' +
                'cesionaria) para facturar y cobrar el uso de los caminos p\'#39'fabl' +
                'icos interurbanos concesionados en los que opera un Sistema de C' +
                'obro Electr\'#39'f3nico de Tarifas o Peajes, en el tramo espec\'#39'edfi' +
                'co Santiago - Talca y Acceso Sur a Santiago, donde podr\'#39'e1 circ' +
                'ular con el(los) veh\'#39'edculo(s) autorizados y con un TAG o Telev' +
                '\'#39'eda habilitado, por las V\'#39'edas Dedicadas de Telepeaje sin det' +
                'enci\'#39'f3n en un modelo conocido como Non Stop, en los p\'#39'f3rtico' +
                's de las 3 Plazas de Peaje Troncal, y/o por las V\'#39'edas Mixtas d' +
                'e Telepeaje con detenci\'#39'f3n, en un modelo conocido como Stop & ' +
                'Go, en las 3 Plazas de Peaje Troncal y 39 Laterales de la Conces' +
                'ionaria.\f1\par'#13#10'\par'#13#10'\f0 El presente documento se firma para d' +
                'ar consentimiento en la suscripci\'#39'f3n al Sistema Electr\'#39'f3nico' +
                ' de Cobro de Tarifas o Peajes de la Concesionaria, entregando en' +
                ' este acto un ejemplar de las Condiciones Generales y Operativas' +
                ' de dicha Concesi\'#39'f3n.\f1\par'#13#10'\par'#13#10'\f0 El Cliente declara con' +
                'ocer y aceptar las Condiciones Generales y Operativas de Uso de ' +
                'Sistema Electr\'#39'f3nico de Cobro de Tarifas o Peajes de la Conces' +
                'ionaria, entregadas en este acto, asumiendo la responsabilidad a' +
                'nte el no cumplimiento de dichas condiciones. \f1\par'#13#10'\par'#13#10'\f0' +
                ' El Cliente deber\'#39'e1 asociar una modalidad de pago de acuerdo a' +
                ' las establecidas por la Concesionaria. En caso de suscribir un ' +
                'Convenio de Pago Autom\'#39'e1tico (PAC o PAT), \'#39'e9ste se activar\'#39 +
                'e1 como medio de pago principal una vez aprobado por la entidad ' +
                'financiera correspondiente. En caso de Rechazo del Convenio de P' +
                'ago Autom\'#39'e1tico, la Concesionaria notificar\'#39'e1 mediante corre' +
                'o electr\'#39'f3nico para su regularizaci\'#39'f3n y env\'#39'edo de nueva d' +
                'ocumentaci\'#39'f3n, si corresponde.\f1\par'#13#10'\par'#13#10'\pard\qj\f0 Las t' +
                'arifas a facturar y cobrar ser\'#39'e1n aquellas que se encuentren v' +
                'igentes para el Contrato de Concesiones de Autopistas Interurban' +
                'as, conforme a las pasadas del(los) veh\'#39'edculo(s) inscrito(s) e' +
                'n su Cuenta de Cliente hayan sido detectadas por los puntos de c' +
                'obro que el sistema electr\'#39'f3nico. Las tarifas son actualizadas' +
                ' e informadas al p\'#39'fablico conforme a la normativa aplicable al' +
                ' correspondiente Contrato de Concesiones de Autopistas Interurba' +
                'nas y, adicionalmente, se encuentran publicadas en el sitio web ' +
                'de Ruta del Maipo (\ul\b www.rutamaipo.cl\ulnone\b0 ) a disposic' +
                'i\'#39'f3n del Cliente. \f1\par'#13#10'\par'#13#10'\f0 La Concesionaria enviar\'#39 +
                'e1 mensualmente al Cliente y en forma gratuita, a m\'#39'e1s tardar ' +
                'el quinto d\'#39'eda de cada mes, un Documento de Cobro que contendr' +
                '\'#39'e1 la informaci\'#39'f3n resumida del correspondiente cobro de Tar' +
                'ifas o Peajes por el uso mensual de la infraestructura del Siste' +
                'ma Electr\'#39'f3nico de Cobro de Tarifas o Peajes de la Concesi\'#39'f3' +
                'n u otras aplicaciones autorizadas por el Ministerio de Obras P\' +
                #39'fablicas. Dicha informaci\'#39'f3n se encontrar\'#39'e1 disponible para' +
                ' el Cliente, adem\'#39'e1s, en las Unidades de Atenci\'#39'f3n de Client' +
                'es y sitio web de la Concesionaria. El Cliente no podr\'#39'e1 efect' +
                'uar pagos parciales, salvo autorizaci\'#39'f3n expresa de la Concesi' +
                'onaria.\f1\par'#13#10'\par'#13#10'\f0 El Cliente deber\'#39'e1 pagar a la Conces' +
                'ionaria la totalidad del monto de dinero consignado en el Docume' +
                'nto de Cobro, a m\'#39'e1s tardar en la fecha de su vencimiento a tr' +
                'av\'#39'e9s de los Canales de Recaudaci\'#39'f3n Internos y Externos (We' +
                'b o Presenciales) o de acuerdo al Convenio de Pago Autom\'#39'e1tico' +
                ' seleccionado (PAC o PAT).\f1\par'#13#10'\par'#13#10'\f0 En caso de Rechazo ' +
                'de Convenio de Pago Autom\'#39'e1tico o Rechazo de Cargo Autom\'#39'e1ti' +
                'co, el Cliente podr\'#39'e1 pagar el monto consignado en el Document' +
                'o de Cobro, a trav\'#39'e9s de los Canales alternativos establecidos' +
                ' por la Concesionaria. \f1\par'#13#10'\par'#13#10'\pard\nowidctlpar\ri401\sl' +
                '-200\slmult0\qj\f0 Ser\'#39'e1n causales de t\'#39'e9rmino anticipado de' +
                ' la vinculaci\'#39'f3n existente entre el Cliente y la Concesionaria' +
                ', las siguientes:\f1\par'#13#10'\pard\nowidctlpar\li357\sl-200\slmult0' +
                '\qj 1.\tab Por mutuo acuerdo.\par'#13#10'2.\tab\f0 Unilateralmente, y ' +
                'en cualquier momento por el Cliente. La terminaci\'#39'f3n unilatera' +
                'l del Cliente no lo excusa del deber legal de pagar las cantidad' +
                'es que adeudare a la Concesionaria.\f1\par'#13#10'3.\tab Unilateralmen' +
                'te, por la Concesionaria, por incumplimiento por parte del Clien' +
                'te de las obligaciones de las Condiciones Generales y Operativas' +
                '. \par'#13#10'4.\tab\f0 En caso de transferencia del Veh\'#39'edculo, debi' +
                'damente notificada a la Concesionaria. \f1\par'#13#10'5.\tab Uso indeb' +
                'ido del TAG imputable al Cliente.\par'#13#10'\pard\nowidctlpar\sl-200\' +
                'slmult0\qj\par'#13#10'\f0 Para todos los efectos legales, formaran par' +
                'te del presente Formulario de Suscripci\'#39'f3n, las Condiciones Ge' +
                'nerales y Operativas de Uso de Sistema Electr\'#39'f3nico de Cobro d' +
                'e Tarifas o Peajes de la Concesionaria entregadas en este acto, ' +
                'conjuntamente con un ejemplar del presente documento. Cabe indic' +
                'ar que a su vez, las Condiciones Generales se encontrar\'#39'e1n a d' +
                'isposici\'#39'f3n del cliente en la p\'#39'e1gina web de Ruta del Maipo,' +
                ' as\'#39'ed como tambi\'#39'e9n videos demostrativos para el correcto us' +
                'o de la Infraestructura.\f1\par'#13#10'\par'#13#10'\pard\qj\f0 Si su(s) plac' +
                'a(s) patente(s) se encuentra(n) registrada(s) en el Sistema Inte' +
                'roperable, el Cliente podr\'#39'e1 comenzar a utilizar la infraestru' +
                'ctura del Sistema TAG Interurbano, transcurridas 2hrs. de realiz' +
                'ada la suscripci\'#39'f3n exitosamente por parte de Ruta del Maipo.\' +
                'lang1033\f1\par'#13#10'}'#13#10#0
              Stretch = True
              Transparent = True
              mmHeight = 226219
              mmLeft = 0
              mmTop = 265
              mmWidth = 203730
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
            end
          end
          object ppFooterBand8: TppFooterBand
            mmBottomOffset = 0
            mmHeight = 9525
            mmPrintPosition = 0
            object ppImage8: TppImage
              UserName = 'Image8'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Picture.Data = {
                0A544A504547496D6167650E090000FFD8FFE000104A46494600010101006000
                600000FFE100784578696600004D4D002A000000080006013100020000001100
                0000560301000500000001000000680303000100000001020000005110000100
                00000101000000511100040000000100000EC4511200040000000100000EC400
                0000004D6963726F736F6674204F66666963650000000186A00000B18FFFDB00
                4300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E
                1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E33
                3432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232
                3232323232323232323232323232323232323232323232323232323232323232
                32323232323232FFC00011080006033103012200021101031101FFC4001F0000
                010501010101010100000000000000000102030405060708090A0BFFC400B510
                0002010303020403050504040000017D01020300041105122131410613516107
                227114328191A1082342B1C11552D1F02433627282090A161718191A25262728
                292A3435363738393A434445464748494A535455565758595A63646566676869
                6A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7
                A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2
                E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301010101010101
                01010000000000000102030405060708090A0BFFC400B5110002010204040304
                0705040400010277000102031104052131061241510761711322328108144291
                A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738
                393A434445464748494A535455565758595A636465666768696A737475767778
                797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4
                B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9
                EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F13FEDDD5FFE82B7
                DFF810FF00E347F6EEAFFF00415BEFFC087FF1A28AF539576203FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D803FB7757FF00A0
                ADF7FE043FF8D1FDBBABFF00D056FBFF00021FFC68A28E55D80D1D0759D4A5F1
                16991CFA8DE490B5DC4B2234EC43297190467D2BDF6086D2484325BA6D3FDE50
                4D1452715D8F371D26A4ACC97ECB6FFF003C22FF00BE051F65B7FF009E117FDF
                028A2972AEC70F3CBB87D96DFF00E7845FF7C0A3ECB6FF00F3C22FFBE051451C
                ABB073CBB87D96DFFE7845FF007C0A3ECB6FFF003C22FF00BE051451CABB073C
                BB87D96DFF00E7845FF7C0A3ECB6FF00F3C22FFBE051451CABB073CBB87D96DF
                FE7845FF007C0A3ECB6FFF003C22FF00BE051451CABB073CBB87D96DFF00E784
                5FF7C0A3ECB6FF00F3C22FFBE051451CABB073CBB87D96DFFE7845FF007C0A3E
                CB6FFF003C22FF00BE051451CABB073CBB87D96DFF00E7845FF7C0A3ECB6FF00
                F3C22FFBE051451CABB073CBB87D96DFFE7845FF007C0A3ECB6FFF003C22FF00
                BE051451CABB073CBB87D96DFF00E7845FF7C0A3ECB6FF00F3C22FFBE051451C
                ABB073CBB87D96DFFE7845FF007C0A3ECB6FFF003C22FF00BE051451CABB073C
                BB87D96DFF00E7845FF7C0A3ECB6FF00F3C22FFBE051451CABB073CBB87D96DF
                FE7845FF007C0A3ECB6FFF003C22FF00BE051451CABB073CBB87D96DFF00E784
                5FF7C0A3ECB6FF00F3C22FFBE051451CABB073CBB87D96DFFE7845FF007C0A3E
                CB6FFF003C22FF00BE051451CABB073CBB87D96DFF00E7845FF7C0A3ECB6FF00
                F3C22FFBE051451CABB073CBB87D96DFFE7845FF007C0A3ECB6FFF003C22FF00
                BE051451CABB073CBB87D96DFF00E7845FF7C0A3ECB6FF00F3C22FFBE051451C
                ABB073CBB87D96DFFE7845FF007C0A3ECB6FFF003C22FF00BE051451CABB073C
                BB87D96DFF00E7845FF7C0A3ECB6FF00F3C22FFBE051451CABB073CBB87D96DF
                FE7845FF007C0A3ECB6FFF003C22FF00BE051451CABB073CBB87D96DFF00E784
                5FF7C0A3ECB6FF00F3C22FFBE051451CABB073CBB87D96DFFE7845FF007C0A3E
                CB6FFF003C22FF00BE051451CABB073CBB87D96DFF00E7845FF7C0A3ECB6FF00
                F3C22FFBE051451CABB073CBB87D96DFFE7845FF007C0A3ECB6FFF003C22FF00
                BE051451CABB073CBB8BF6483FE7845FF7C0A28A28E55D839E5DCFFFD9}
              mmHeight = 9525
              mmLeft = 0
              mmTop = 0
              mmWidth = 205052
              BandType = 8
            end
          end
        end
      end
    end
    object ppParameterList8: TppParameterList
    end
  end
  object dsRepresentantesLegales: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'RUTRepresentante'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'NombreRepresentante'
        DataType = ftString
        Size = 200
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 72
    Top = 128
    Data = {
      670000009619E0BD010000001800000002000000000003000000670010525554
      526570726573656E74616E74650100490000000100055749445448020002000F
      00134E6F6D627265526570726573656E74616E74650100490000000100055749
      44544802000200C8000000}
  end
end
