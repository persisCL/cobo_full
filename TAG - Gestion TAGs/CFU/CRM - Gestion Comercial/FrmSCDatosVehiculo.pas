{********************************** Unit Header ********************************
File Name : FrmSCDatosVehiculo.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision : 1
Author :   jconcheyro
Date Created: 26/01/2007
Language : ES-AR
Description : Se agreg� para indemnizaciones de telev�as, el dblConceptos con
todo lo demas necesario para que al momento de un alta, se indique un motivo
y quede registrado en MovimientosCuentasTelev�as.

Revision : 2
Author :   Fsandi
Date Created: 01-06-2007
Language : ES-AR
Description : Se permite modificar la fecha de alta de una cuenta al momento de agregar un vehiculo.

Revision : 3
Author :   Fsandi
Date Created: 10-08-2007
Language : ES-AR
Description : Se agrega una validacion al televia para confirmar que solo se permiten arriendos
            para tags identificados como propiedad CN en MaestroTags

Revision : 4
Author :   Fsandi
Date Created: 13-09-2007
Language : ES-AR
Description : Se agrega la funcionalidad de recuperar un veh�culo robado al momento de dar de alta.

Revision 5
Author: mbecerra
Date: 23-Dic-2008
Description:	Se agrega el campo IDMotivoMovCuentaTelevia a ser desplegado y
				retornado, para ser validado por frmModificacionConvenio.

Revision 6
Author: mbecerra
Date: 07-Julio-2009
Description:	Se agrega el concepto de arriendo en cuotas al RadioGroup, para que
            	el otro formulario pueda traer dichos conceptos.

Revision :
      Author : vpaszkowicz
      Date : 01/09/2009
      Description :Modifiqu� la funci�n
         - btn_AceptarClick

Revision 8
    Author      : mbecerra
    Date        : 14-Septiembre-2010
    Description :       (Ref. SS 921)
                    Se agregan dos objetos que permitan la edici�n de la Fecha de
                    vencimiento de la garant�a del TAG, con permisos:
                        btnEditFechaVctoTag y
                        deFechaVctoTag

                    Se agregan la variable FFechaVctoTag, la propiedad FechaVencimientoTag
                    y la variable FAniosGarantiaTag.

                    La fecha de vencimiento sugerida es la fecha de alta de la cuenta
                    m�s los a�os que dice el par�metro ANIOS_VTO_GARANTIA_TAG

                    Se valida que si el operador edita la fecha de vencimiento,
                    �sta se mueva entre los rangos FFechaAltaCuenta y
                    FFechaAltaCuenta + ANIOS_VTO_GARANTIA_TAG.
    
Etiqueta	:	TASK_055_MGO_20160801
Descripci�n	:	Se agrega propiedad Foraneo                
*******************************************************************************}
unit FrmSCDatosVehiculo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DmiCtrls, MaskCombo, PeaProcs,
  DMCOnnection, Util, UtilProc, peatypes, Convenios, VariantComboBox, DB, ADODB,
  DBClient, RStrings, UtilDB, Mensajes, FrmObservacionesGeneral,
  FrameSCDatosVehiculo,  Menus, frmSeleccionarMovimientosTelevia, ListBoxEx, DBListEx, ModificarFechaAlta,
  Validate, DateEdit, ConstParametrosGenerales, DateUtils, PeaProcsCN;

type
    TFormSCDatosVehiculo = class(TForm)
    GroupBox: TGroupBox;
    GroupBoxNuevo: TGroupBox;
    FreDatosVehiculoNuevo: TFmeSCDatosVehiculo;
    FreDatosVehiculo: TFmeSCDatosVehiculo;
    dblConceptos: TDBListEx;
    Bevel1: TBevel;
    Label10: TLabel;
    lblSeleccionar: TLabel;
    dsConceptos: TDataSource;
    mnuGrillaConceptos: TPopupMenu;
    mnuEliminarConcepto: TMenuItem;
    AgregarConceptos1: TMenuItem;
    lblTotalMovimientos: TLabel;
    lblTotalMovimientosTxt: TLabel;
    rgModalidadEntregaTag: TRadioGroup;
    rgGarantiaTag: TRadioGroup;
    gbObservacionesAdicionales: TGroupBox;
    edNumeroVoucher: TNumericEdit;
    cdConceptos: TClientDataSet;
    spValidarFechasAltaCuenta: TADOStoredProc;							// SS_916_PDO_20120106
    spEnviarCorreoErroresIngresoPatente: TADOStoredProc;
    rbArriendoEnCuotas: TRadioButton;
    rbComodato: TRadioButton;
    rbArriendo: TRadioButton;
    pnlRadioButtons: TPanel;
    pnlTotalMontos: TPanel;
    pnlMotivoFacturacion: TPanel;
    pnlFechas: TPanel;
    lblFechaAltaCuenta: TLabel;
    lblFechaVtoTAG: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    BtnFechaAlta: TButton;
    txtFechaAlta: TEdit;
    btnEditFechaVctoTag: TButton;
    edtFechaVctoTag: TEdit;
    pnlIndemnizaciones: TPanel;
    pnlAceptarCancelar: TPanel;
    pnlModalidadEntregaTelevia: TPanel;
    lbl1: TLabel;
    lblModalidadEntregaTelevia: TLabel;							//SS_1172_NDR_20140221
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblSeleccionarClick(Sender: TObject);
    procedure mnuEliminarConceptoClick(Sender: TObject);
    procedure AgregarConceptos1Click(Sender: TObject);
    procedure dblConceptosContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure dblConceptosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNumeroVoucherExit(Sender: TObject);
    procedure BtnFechaAltaClick(Sender: TObject);
    procedure dblConceptosDrawDone(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState);
    procedure btnEditFechaVctoTagClick(Sender: TObject);
    procedure rgGarantiaTagClick(Sender: TObject);								// SS_916_PDO_20120106
    procedure FreDatosVehiculotxtDigitoVerificadorExit(Sender: TObject);		// SS_916_PDO_20120106
    procedure FreDatosVehiculotxtPatenteChange(Sender: TObject);				// SS_916_PDO_20120106
    procedure FreDatosVehiculotxtNumeroTeleviaChange(Sender: TObject);			// SS_916_PDO_20120106
    procedure FreDatosVehiculoNuevotxtDigitoVerificadorExit(Sender: TObject);
    procedure rbArriendoEnCuotasClick(Sender: TObject);
    procedure rbComodatoClick(Sender: TObject);
    procedure rbArriendoClick(Sender: TObject);	//SS_1172_NDR_20140221
  private
    FCodigoVehiculo: Integer;
    FPuntoEntrega: Integer;
    FImporteTotalMovimientos: Int64;
    FListaVouchers: TStringList ;
    FCambioFecha: Boolean;
    //Revision 2
    FFechaAlta: TDateTime;
    Fserialnumber: Int64;
    FContextMark: Integer;
    FPatente: String;
    //Fin de Revision 2
    FFechaVctoTag : TDateTime;      //REV.8
    FAniosGarantiaTag : Integer;    //REV.8
    FHoy,												// SS_916_PDO_20120106
    FUltimaBajaCuenta: TDateTime;						// SS_916_PDO_20120106
    FDatosConvenioSCDDatosVehiculo: TDatosConvenio;   	// SS_916_PDO_20120106

    PERMITIR_ALTA_TAG_COMODATO : Integer;
    PERMITIR_ALTA_TAG_ARRIENDO : Integer;
    PERMITIR_ALTA_TAG_ARRIENDO_CUOTAS : Integer;

    function GetAnio: integer;
    function GetCategoria: integer;
    function GetCodigoColor: integer;
    function GetCodigoMarca: integer;
    function GetCodigoTipo: integer;
    function GetDatoCuenta: TCuentaVehiculo;
    function GetDatoCuentaNueva: TCuentaVehiculo;
    function GetDescripcionColor: AnsiString;
    function GetDescripcionMarca: AnsiString;
    function GetDescripcionModelo: AnsiString;
    function GetDescripcionTipo: AnsiString;
    function GetDetalleVehiculoCompleta: Ansistring;
    function GetDetalleVehiculoSimple: Ansistring;
    function GetDigitoVerificador: AnsiString;
    function GetDigitoVerificadorCorrecto: boolean;
    function GetDigitoVerificadorCorrectoRVM: boolean;
    function GetDigitoVerificadorRVM: AnsiString;      
    function GetForaneo: Boolean;                   // TASK_055_MGO_20160801
    function GetPatente: AnsiString;
    function GetPatenteRVM: String;
    function GetSerialNumber: DWORD;
    function GetTag: AnsiString;
    function GetTipoPatente: AnsiString;
    function GetContextMark: integer;
    function GetObservaciones: Ansistring;
    function GetFechaCreacion: TDateTime;
    function GetNumeroVoucher: integer;
    function GetAlmacenDestino: integer;
    function GetFechaAlta: TDateTime;    //Revision 2
    procedure SetFechaAlta(Value: TDateTime);		// SS_916_PDO_20120106
    procedure SetFechaVtoTAG(Value: TDateTime);		// SS_916_PDO_20120106
    function GetTipoAsignacionTag: string;

    { Private declarations }
  public
    Property TipoPatente: AnsiString read GetTipoPatente;
    Property Patente: AnsiString read GetPatente;
    property Marca: integer read GetCodigoMarca;
    property DescripcionMarca: AnsiString read GetDescripcionMarca;
    property CodigoTipo: integer read GetCodigoTipo;
    property DescripcionTipo: AnsiString read GetDescripcionTipo;
    property DescripcionModelo: AnsiString read GetDescripcionModelo;
    property Anio: integer read GetAnio;
    property Color: integer read GetCodigoColor;
    property DescripcionColor: AnsiString read GetDescripcionColor;
    property CodigoCategoria: integer read GetCategoria;
    property DigitoVerificadorCorrecto: boolean read GetDigitoVerificadorCorrecto;
    property DigitoVerificadorCorrectoRVM: boolean read GetDigitoVerificadorCorrectoRVM;
    property DigitoVerificador: AnsiString read GetDigitoVerificador;
    property DetalleVehiculoSimple: Ansistring read GetDetalleVehiculoSimple;
    property DetalleVehiculoCompleto: Ansistring read GetDetalleVehiculoCompleta;
    property Observaciones: Ansistring read GetObservaciones;
    property SerialNumber:DWORD read GetSerialNumber;
    property CodigoVehiculo:Integer read FCodigoVehiculo;
    property ContextMark: integer read GetContextMark;
    property NumeroTag:AnsiString read GetTag;
    property DatoCuenta:TCuentaVehiculo read GetDatoCuenta;
    property DatoCuentaNueva:TCuentaVehiculo read GetDatoCuentaNueva;
    property PatenteRVM  : String read GetPatenteRVM;
    property DigitoVerificadorRVM: AnsiString read GetDigitoVerificadorRVM;
    property Foraneo: Boolean read GetForaneo;                                  // TASK_055_MGO_20160801
    property FechaCreacion: TDateTime read GetFechaCreacion;
    property PuntoEntrega: Integer read FPuntoEntrega;
    property NumeroVoucher: integer read GetNumeroVoucher;
    property AlmacenDestino: Integer read GetAlmacenDestino;
    property TipoAsignacionTag: string read GetTipoAsignacionTag;
    property FechaAlta:TDateTime read  GetFechaAlta write SetFechaAlta;  					// SS_916_PDO_20120106
    property CambioFecha: Boolean read FCambioFecha;
    property FechaVencimientoTag : TDateTime read FFechaVctoTag write SetFechaVtoTAG;       // SS_916_PDO_20120106
    Function Inicializar(CaptionAMostrar: string;
                    PuntoEntrega: Integer;
                    CodigoTipoVehiculo: integer = 1;
                    Solicitud: integer = -1;
                    ListaPatentes: TStringList = nil;
                    EsConvenio: boolean = False;
                    ListaTags: TStringList = nil;
                    ContextMark: integer = -1;
                    CodigoConvenio: integer = -1;
                    cdsVehiculos: TDataSet = nil;
                    CodigoPersona: integer = -1;
                    Estado: TStateFrmVehiculo = Solicitud;
                    ListaVouchers: TStringList = nil;                                           // SS_916_PDO_20120106
                    pDatosConvenio: TDatosConvenio = nil): Boolean; overload; //Para el Alta.   // SS_916_PDO_20120106

    Function Inicializar(CaptionAMostrar: string;
                    PuntoEntrega: Integer;
                    TipoPatente, Patente, PatenteRVM , Modelo: AnsiString;
                    CodigoMarca, Anio, CodigoTipo, CodigoColor: integer;
                    DigitoVerificador: string;
                    DigitoVerificadorRVM: string;
                    CodVechiculo : Integer = -1;
                    Observaciones: string = '';
                    Solicitud: integer = -1;
                    ListaPatentes: TStringList = nil;
                    EsConvenio: boolean = False;
                    ListaTags: TStringList = nil;
                    ContextMark: integer = -1;
                    NumeroTag: string = '';
                    AsignarTag: boolean = False;
                    PuedeEditarTag: Boolean = True;
                    CodigoConvenio: integer = -1;
                    cdsVehiculos: TDataSet = nil;
                    Vendido: Boolean = False;
                    CodigoPersona: integer = -1;
                    Estado: TStateFrmVehiculo = Solicitud;
                    ListaVouchers: TStringList = nil;
                    Recuperado: boolean = False;
                    pDatosConvenio: TDatosConvenio = nil): Boolean; overload;       // SS_916_PDO_20120106
    { Public declarations }

    function ValidarExistenciaVoucher : boolean;

    procedure ValidarFechasAltaCuenta;		// SS_916_PDO_20120106
  end;

var
  FormSCDatosVehiculo: TFormSCDatosVehiculo;
  FParametroIntentosPatente,FIntentosPatente,FCodigoConvenio:Integer;                           //SS_1172_NDR_20140221


  implementation

{$R *.dfm}

Function TFormSCDatosVehiculo.Inicializar(CaptionAMostrar: string;
    PuntoEntrega: Integer;
    CodigoTipoVehiculo: integer = 1;
    Solicitud: integer = -1;
    ListaPatentes: TStringList = nil;
    EsConvenio: boolean = False;
    ListaTags: TStringList = nil;
    ContextMark: integer = -1;
    CodigoConvenio: integer = -1;
    cdsVehiculos: TDataSet = nil;
    CodigoPersona: integer = -1;
    Estado: TStateFrmVehiculo = Solicitud;
    ListaVouchers: TStringList = nil;
    pDatosConvenio: TDatosConvenio = nil): Boolean;   // SS_916_PDO_20120106

resourcestring
    MSG_PARAMETRO   = 'ANIOS_VTO_GARANTIA_TAG';           //REV.8
begin
    FCodigoConvenio:=CodigoConvenio;                                                                                    //SS_1172_NDR_20140221
    ObtenerParametroGeneral(DMConnections.BaseCAC,'INTENTOS_ERROR_INGRESAR_PATENTE_CAC',FParametroIntentosPatente);     //SS_1172_NDR_20140221
    FIntentosPatente:=0;                                                                                                //SS_1172_NDR_20140221


    //este inicializar se utiliza desde FrmSolicitudContacto
    Self.Caption := CaptionAMostrar;

    FDatosConvenioSCDDatosVehiculo := pDatosConvenio;   // SS_916_PDO_20120109

    FPuntoEntrega := PuntoEntrega;
    FListaVouchers := ListaVouchers;
    FCambioFecha := False;
    Result := FreDatosVehiculo.Inicializar(PuntoEntrega, CodigoTipoVehiculo, Solicitud, ListaPatentes, EsConvenio,
                                             ListaTags, ContextMark, CodigoConvenio, cdsVehiculos, CodigoPersona, Estado,
                                             Estado <> Peatypes.Solicitud);

    //Revision 2
    //FFechaAlta := NowBase(DMConnections.BaseCAC);								// SS_916_PDO_20120106
    //txtFechaAlta.Text:= FormatDateTime('dd-mm-yyyy / hh:nn', FFechaAlta);		// SS_916_PDO_20120106
    //Fin de Revision 2

    FechaAlta := NullDate;		// SS_916_PDO_20120106

    ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO, FAniosGarantiaTag);       //REV.8
    //FechaVencimientoTag := IncMonth(FFechaAlta, 12 * FAniosGarantiaTag);                    	// SS_916_PDO_20120106
    //edtFechaVctoTag.Text := FormatDateTime('dd-mm-yyyy / hh:nn', FechaVencimientoTag);      	// SS_916_PDO_20120106

    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'PERMITIR_ALTA_TAG_COMODATO', PERMITIR_ALTA_TAG_COMODATO) then PERMITIR_ALTA_TAG_COMODATO := 0;
    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'PERMITIR_ALTA_TAG_ARRIENDO', PERMITIR_ALTA_TAG_ARRIENDO) then PERMITIR_ALTA_TAG_ARRIENDO := 0;
    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'PERMITIR_ALTA_TAG_ARRIENDO_CUOTAS', PERMITIR_ALTA_TAG_ARRIENDO_CUOTAS) then PERMITIR_ALTA_TAG_ARRIENDO_CUOTAS := 1;

    rbArriendoEnCuotas.Enabled  := (PERMITIR_ALTA_TAG_ARRIENDO_CUOTAS = 1);
    rbComodato.Enabled          := (PERMITIR_ALTA_TAG_COMODATO = 1);
    rbArriendo.Enabled          := (PERMITIR_ALTA_TAG_ARRIENDO = 1);

    rbArriendoEnCuotas.Checked  := True;

    FechaVencimientoTag := NullDate;															// SS_916_PDO_20120106
    FHoy := Trunc(NowBase(DMConnections.BaseCAC));												// SS_916_PDO_20120106
{INICIO: TASK_036_JMA_20160707
    Case Estado of
        Peatypes.Solicitud:
            begin
                GroupBox.Align := alClient;
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                Height := 489; //440; //250;    731        780
                GroupBoxNuevo.Visible := False;
                gbIndemnizaciones.Visible := False;
                //Revision 2
                BtnFechaAlta.Visible := ExisteAcceso('Boton_ModificarFechaAlta');
//                BtnFechaAlta.Enabled := ExisteAcceso('Boton_ModificarFechaAlta');				// SS_916_PDO_20120106
                txtFechaAlta.Visible := ExisteAcceso('Boton_ModificarFechaAlta');
                lblFechaAltaCuenta.Visible := txtFechaAlta.Visible;								// SS_916_PDO_20120106
                //Fin de Revision 2
                btnEditFechaVctoTag.Visible := ExisteAcceso('Modif_Fecha_Vencimiento_TAG');         //REV.8
                edtFechaVctoTag.Visible     := btnEditFechaVctoTag.Visible;                         //REV.8
                lblFechaVtoTAG.Visible      := edtFechaVctoTag.Visible;							// SS_916_PDO_20120106
            end;
        Peatypes.ConvenioAltaVehiculo:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                Height := 545; //496 ;//306;
                GroupBoxNuevo.Visible := False;
                gbIndemnizaciones.Align := alClient;
                //Revision 2
                BtnFechaAlta.Visible := ExisteAcceso('Boton_ModificarFechaAlta');
//                BtnFechaAlta.Enabled := ExisteAcceso('Boton_ModificarFechaAlta');				// SS_916_PDO_20120106
                txtFechaAlta.Visible := ExisteAcceso('Boton_ModificarFechaAlta');
                lblFechaAltaCuenta.Visible := txtFechaAlta.Visible;								// SS_916_PDO_20120106
                //Fin de Revision 2
                btnEditFechaVctoTag.Visible := ExisteAcceso('Modif_Fecha_Vencimiento_TAG');         //REV.8
                edtFechaVctoTag.Visible     := btnEditFechaVctoTag.Visible;                         //REV.8
                lblFechaVtoTAG.Visible      := edtFechaVctoTag.Visible;							// SS_916_PDO_20120106
            end;
        Peatypes.ConvenioModiVehiculo:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                Height := 306;
                gbIndemnizaciones.Visible := False;
                GroupBoxNuevo.Visible := False;
                //Revision 2
                BtnFechaAlta.Visible := False;
                BtnFechaAlta.Enabled := False;
                txtFechaAlta.Visible := False;
                lblFechaAltaCuenta.Visible := False;	// SS_916_PDO_20120106
                txtFechaAlta.Text:= EmptyStr;
                edtFechaVctoTag.Visible     := False;	// SS_916_PDO_20120106
                lblFechaVtoTAG.Visible      := False;	// SS_916_PDO_20120106
                //Fin de Revision 2
            end;
        Peatypes.ConvenioCambiarVehiculo:
            begin
                Height := 724; //675; //485;
                //Revision 2
                BtnFechaAlta.Visible := False;
                BtnFechaAlta.Enabled := False;
                txtFechaAlta.Visible := False;
                lblFechaAltaCuenta.Visible := False;	// SS_916_PDO_20120106
                txtFechaAlta.Text:= EmptyStr;
                edtFechaVctoTag.Visible     := False;	// SS_916_PDO_20120106
                lblFechaVtoTAG.Visible      := False;	// SS_916_PDO_20120106
                //Fin de Revision 2
            end;
    end;
     }
     dblConceptos.Visible:=False;
     pnlMotivoFacturacion.Visible:=False;
     pnlTotalMontos.Visible:=False;
     pnlIndemnizaciones.Height := pnlIndemnizaciones.Height - dblConceptos.Height;
     pnlIndemnizaciones.Height := pnlIndemnizaciones.Height - pnlMotivoFacturacion.Height;
     pnlIndemnizaciones.Height := pnlIndemnizaciones.Height - pnlTotalMontos.Height;
    Case Estado of
        Peatypes.Solicitud:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                GroupBoxNuevo.Visible := False;
                pnlIndemnizaciones.Visible := False;
                BtnFechaAlta.Visible := ExisteAcceso('Boton_ModificarFechaAlta');
                txtFechaAlta.Visible := ExisteAcceso('Boton_ModificarFechaAlta');
                lblFechaAltaCuenta.Visible := txtFechaAlta.Visible;
                btnEditFechaVctoTag.Visible := ExisteAcceso('Modif_Fecha_Vencimiento_TAG');
                edtFechaVctoTag.Visible     := btnEditFechaVctoTag.Visible;
                lblFechaVtoTAG.Visible      := edtFechaVctoTag.Visible;
            end;
        Peatypes.ConvenioAltaVehiculo:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                GroupBoxNuevo.Visible := False;
                BtnFechaAlta.Visible := ExisteAcceso('Boton_ModificarFechaAlta');
                txtFechaAlta.Visible := ExisteAcceso('Boton_ModificarFechaAlta');
                lblFechaAltaCuenta.Visible := txtFechaAlta.Visible;
                btnEditFechaVctoTag.Visible := ExisteAcceso('Modif_Fecha_Vencimiento_TAG');
                edtFechaVctoTag.Visible     := btnEditFechaVctoTag.Visible;
                lblFechaVtoTAG.Visible      := edtFechaVctoTag.Visible;
                gbObservacionesAdicionales.Visible:= False;
                pnlIndemnizaciones.Height := pnlIndemnizaciones.Height - gbObservacionesAdicionales.Height;
                pnlModalidadEntregaTelevia.Visible:= False;
            end;
        Peatypes.ConvenioModiVehiculo:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                pnlIndemnizaciones.Visible := False;
                GroupBoxNuevo.Visible := False;
                BtnFechaAlta.Visible := False;
                BtnFechaAlta.Enabled := False;
                txtFechaAlta.Visible := False;
                lblFechaAltaCuenta.Visible := False;
                txtFechaAlta.Text:= EmptyStr;
                edtFechaVctoTag.Visible     := False;
                lblFechaVtoTAG.Visible      := False;
            end;
        Peatypes.ConvenioCambiarVehiculo:
            begin
                BtnFechaAlta.Visible := False;
                BtnFechaAlta.Enabled := False;
                txtFechaAlta.Visible := False;
                lblFechaAltaCuenta.Visible := False;
                txtFechaAlta.Text:= EmptyStr;
                edtFechaVctoTag.Visible     := False;
                lblFechaVtoTAG.Visible      := False;
            end;
    end;
{TERMINO: TASK_036_JMA_20160707}

end;

Function TFormSCDatosVehiculo.Inicializar(CaptionAMostrar: string;
    PuntoEntrega: Integer;
    TipoPatente, Patente, PatenteRVM, Modelo: AnsiString;
    CodigoMarca, Anio, CodigoTipo, CodigoColor: integer;
    DigitoVerificador: string;  DigitoVerificadorRVM: string;
    CodVechiculo : Integer = -1; Observaciones: string = '';
    Solicitud: integer = -1; ListaPatentes: TStringList = nil;
    EsConvenio: boolean = False;
    ListaTags: TStringList = nil; ContextMark: integer = -1;
    NumeroTag: string = ''; AsignarTag: boolean = False;
    PuedeEditarTag: Boolean = True; CodigoConvenio: integer = -1;
    cdsVehiculos: TDataSet = nil;
    Vendido: Boolean = False;
    CodigoPersona: integer = -1;
    Estado: TStateFrmVehiculo = Solicitud;
    ListaVouchers: TStringList = nil;
    Recuperado: boolean = False;     //Revision 4
    pDatosConvenio: TDatosConvenio = nil      // SS_916_PDO_20120106
    ): Boolean;

var
    xdelta: integer;

begin
    FCodigoConvenio:=CodigoConvenio;                                                                                    //SS_1172_NDR_20140221
    ObtenerParametroGeneral(DMConnections.BaseCAC,'INTENTOS_ERROR_INGRESAR_PATENTE_CAC',FParametroIntentosPatente);     //SS_1172_NDR_20140221
    FIntentosPatente:=0;                                                                                                //SS_1172_NDR_20140221
    //este inicializar se utiliza desde FrmModificacionConvenio

    // 755
    //Revision 2
    //FFechaAlta := NowBase(DMConnections.BaseCAC);								// SS_916_PDO_20120106
    //txtFechaAlta.Text:= FormatDateTime('dd-mm-yyyy / hh:nn', FFechaAlta);		// SS_916_PDO_20120106

    FechaAlta := NullDate;														// SS_916_PDO_20120106

    FechaVencimientoTag := NullDate;											// SS_916_PDO_20120106
    FHoy := Trunc(NowBase(DMConnections.BaseCAC));								// SS_916_PDO_20120106

    Self.Caption := CaptionAMostrar;
    FPuntoEntrega := PuntoEntrega;
    FListaVouchers := ListaVouchers;
    if Assigned(cdsVehiculos) then begin
        lblModalidadEntregaTelevia.Caption := cdsVehiculos.FieldByName('Almacen').Value;
		if cdsVehiculos.FieldByName('Almacen').Value = CONST_STR_ALMACEN_COMODATO then rbComodato.Checked := True;
    	if cdsVehiculos.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ARRIENDO then rbArriendo.Checked := True;
        if cdsVehiculos.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS then rbArriendoEnCuotas.Checked := True;
    end;

    Result := FreDatosVehiculo.Inicializar(TipoPatente, Patente, PatenteRVM, Modelo, CodigoMarca,
                                             Anio, CodigoTipo, CodigoColor, DigitoVerificador,
                                             DigitoVerificadorRVM, PuntoEntrega, CodVechiculo, Observaciones,
                                             Solicitud, ListaPatentes, EsConvenio,
                                             ListaTags, ContextMark, NumeroTag, AsignarTag,
                                             PuedeEditarTag, cdsVehiculos, CodigoConvenio,  Vendido, CodigoPersona, Estado, Estado <> Peatypes.Solicitud, nulldate,0,Recuperado);
 {INICIO: TASK_036_JMA_20160707
    Case Estado of
        Peatypes.Solicitud:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                GroupBox.Align := alClient;
                FreDatosVehiculoNuevo.Enabled := False;
                Height := 489 ;//440 ; //250;
                GroupBoxNuevo.Visible := False;
                GroupBox.Align := alClient;
            end;
        Peatypes.ConvenioAltaVehiculo:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                Height := 545; //496 ;//306;
                GroupBoxNuevo.Visible := False;
            end;
        Peatypes.ConvenioModiVehiculo:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                Height := 344;
                GroupBoxNuevo.Visible := False;
                gbIndemnizaciones.Visible := False;
                gbModalidadEntregaTelevia.Visible := True;
            end;
        Peatypes.ConvenioCambiarVehiculo:
            begin
                //gbIndemnizaciones.Visible := False;
                Height := 695; //665; //675; //485;
                rgGarantiaTag.Visible := False ;
                rgModalidadEntregaTag.Visible := False;
                rbArriendoEnCuotas.Visible := False;
                rbArriendo.Visible := False;
                rbComodato.Visible := False;
				xdelta := 60 ;
				label10.top                      :=  label10.top                     - xdelta;
				lblseleccionar.top               :=  lblseleccionar.top              - xdelta;
				dblConceptos.top                 :=  dblConceptos.top                - xdelta;
				lblTotalMovimientostxt.top       :=  lblTotalMovimientostxt.top      - xdelta;
				lblTotalMovimientos.top          :=  lblTotalMovimientos.top         - xdelta;
				gbObservacionesAdicionales.Visible   :=  False;

				groupBoxNuevo.Height := groupBoxNuevo.Height  - xdelta ;
				gbIndemnizaciones.Height := gbIndemnizaciones.Height - 100 ;

                gbModalidadEntregaTelevia.Visible := True;
                GroupBoxNuevo.Top := gbIndemnizaciones.Top + gbIndemnizaciones.Height + 2;
                Result := FreDatosVehiculoNuevo.Inicializar(TipoPatente, '', '', '', 1,
                                                         -1, 1, 1, '',
                                                         '', PuntoEntrega,  -1, '',
                                                         -1, ListaPatentes, True,
                                                         ListaTags, ContextMark, NumeroTag, False,
                                                         False, cdsVehiculos, CodigoConvenio, Vendido, CodigoPersona,  Peatypes.ConvenioAltaVehiculo, False);
            end;
    end;

  }

    Case Estado of
        Peatypes.Solicitud:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                GroupBoxNuevo.Visible := False;
                pnlModalidadEntregaTelevia.Visible := False;
            end;
        Peatypes.ConvenioAltaVehiculo:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                GroupBoxNuevo.Visible := False;
                pnlModalidadEntregaTelevia.Visible := False;
            end;
        Peatypes.ConvenioModiVehiculo:
            begin
                EnableControlsInContainer(FreDatosVehiculoNuevo, False);
                FreDatosVehiculoNuevo.Enabled := False;
                GroupBoxNuevo.Visible := False;
                pnlIndemnizaciones.Visible := False;
                pnlFechas.Visible:= false;
            end;
        Peatypes.ConvenioCambiarVehiculo:
            begin

                rbArriendoEnCuotas.Visible := False;
                rbArriendo.Visible := False;
                rbComodato.Visible := False;
                pnlIndemnizaciones.Visible:= False;
                pnlFechas.Visible := False;
                Result := FreDatosVehiculoNuevo.Inicializar(TipoPatente, '', '', '', 1,
                                                         -1, 1, 1, '',
                                                         '', PuntoEntrega,  -1, '',
                                                         -1, ListaPatentes, True,
                                                         ListaTags, ContextMark, NumeroTag, False,
                                                         False, cdsVehiculos, CodigoConvenio, Vendido, CodigoPersona,  Peatypes.ConvenioAltaVehiculo, False);
            end;
    end;

{TERMINO: TASK_036_JMA_20160707}

end;

procedure TFormSCDatosVehiculo.FormShow(Sender: TObject);
begin
    FreDatosVehiculo.ActualizarControlActivo;  //Para que ponga el foco en el contro correcto
    FreDatosVehiculoNuevo.ActualizarControlActivo;
end;

//BEGIN : SS_1172_NDR_20140221 -------------------------------------------------------------------------------------------------------------
procedure TFormSCDatosVehiculo.FreDatosVehiculoNuevotxtDigitoVerificadorExit(
  Sender: TObject);
resourcestring                                                                                                          //SS_1172_NDR_20140221
    MSG_ERROR_MENSAJE = 'Se produjo un Error al ejecutar el SP EnviarCorreoErroresIngresoPatente. Error: %s';           //SS_1172_NDR_20140221
    MSG_ERROR_CAPTION = 'Enviar Correo Errores Ingreso Patente';                                                        //SS_1172_NDR_20140221
begin
    FreDatosVehiculoNuevo.DigitosVerificadoresExit(Sender);
    if FreDatosVehiculoNuevo.txtDigitoVerificador.text=EmptyStr then
    begin
      FIntentosPatente:=FIntentosPatente+1;
      if FIntentosPatente>=FParametroIntentosPatente then
      begin
        try
            ShowMessage('Usted ha agotado los intentos para ingresar la patente.'+CRLF+'Revise el padr�n del veh�culo y reintente el ingreso.');
            with spEnviarCorreoErroresIngresoPatente do
            begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@Patente').Value        := FreDatosVehiculoNuevo.txtPatente.Text;
                Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
                Parameters.ParamByName('@Usuario').Value        := UsuarioSistema;
                ExecProc;
            end;
            Screen.ActiveForm.Close;
            keybd_event(VK_ESCAPE, 0, 0, 0);
            exit;
        except
            on e:Exception do begin
                ShowMsgBoxCN(MSG_ERROR_CAPTION,Format(MSG_ERROR_MENSAJE, [e.Message]), MB_ICONERROR, Self);
            end;
        end;
      end;
    end;
end;
//END : SS_1172_NDR_20140221 -------------------------------------------------------------------------------------------------------------

// Inicio Bloque: SS_916_PDO_20120106
procedure TFormSCDatosVehiculo.FreDatosVehiculotxtDigitoVerificadorExit(Sender: TObject);                               //SS_1172_NDR_20140221
resourcestring                                                                                                          //SS_1172_NDR_20140221
    MSG_ERROR_MENSAJE = 'Se produjo un Error al ejecutar el SP EnviarCorreoErroresIngresoPatente. Error: %s';           //SS_1172_NDR_20140221
    MSG_ERROR_CAPTION = 'Enviar Correo Errores Ingreso Patente';                                                        //SS_1172_NDR_20140221
begin
    FreDatosVehiculo.DigitosVerificadoresExit(Sender);
	  //BEGIN : SS_1172_NDR_20140221 -------------------------------------------------------------------------------------------------------------
    if FreDatosVehiculo.txtDigitoVerificador.text=EmptyStr then
    begin
      FIntentosPatente:=FIntentosPatente+1;
      if FIntentosPatente>=FParametroIntentosPatente then
      begin
        try
            ShowMessage('Usted ha agotado los intentos para ingresar la patente.'+CRLF+'Revise el padr�n del veh�culo y reintente el ingreso.');
            with spEnviarCorreoErroresIngresoPatente do
            begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@Patente').Value        := FreDatosVehiculo.txtPatente.Text;
                Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
                Parameters.ParamByName('@Usuario').Value        := UsuarioSistema;
                ExecProc;
            end;
            Screen.ActiveForm.Close;
            keybd_event(VK_ESCAPE, 0, 0, 0);
            exit;
        except
            on e:Exception do begin
                ShowMsgBoxCN(MSG_ERROR_CAPTION,Format(MSG_ERROR_MENSAJE, [e.Message]), MB_ICONERROR, Self);
            end;
        end;
      end;
    end;
	  //END : SS_1172_NDR_20140221 -------------------------------------------------------------------------------------------------------------
    ValidarFechasAltaCuenta;
end;

procedure TFormSCDatosVehiculo.FreDatosVehiculotxtNumeroTeleviaChange(
  Sender: TObject);
begin
  FreDatosVehiculo.txtNumeroTeleviaChange(Sender);

  ValidarFechasAltaCuenta;
end;

procedure TFormSCDatosVehiculo.FreDatosVehiculotxtPatenteChange(
  Sender: TObject);
begin
    FreDatosVehiculo.txtPatenteChange(Sender);

    ValidarFechasAltaCuenta;
end;
// Fin Bloque: SS_916_PDO_20120106

procedure TFormSCDatosVehiculo.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------
        btnEditFechaVctoTagClick

    Author      : mbecerra
    Date        : 14-Septiembre-2010
    Description :   (Ref SS 921)
                    Permite la edici�n de la fecha de vencimiento del Tag
-------------------------------------------------------------------------}
procedure TFormSCDatosVehiculo.btnEditFechaVctoTagClick(Sender: TObject);
var
    f : TfrmModificarFechaAlta;
begin
    Application.CreateForm(TfrmModificarFechaAlta, f);
    if f.Inicializa(FechaVencimientoTag, FechaAlta, FAniosGarantiaTag) then begin
        if f.ShowModal = mrOk then begin
            FechaVencimientoTag := f.DtFechaAlta.Date + f.DtHoraAlta.Time;
            edtFechaVctoTag.Text := FormatDateTime('dd-mm-yyyy / hh:nn', FechaVencimientoTag);
        end;
    end;

    f.Release;

end;

//Revision 2
procedure TFormSCDatosVehiculo.BtnFechaAltaClick(Sender: TObject);
resourcestring
    MSG_FECHA_ALTA_DATOS_ERROR = 'Para modificar la fecha de Alta, debe completar la Patente y el Telev�a';
    MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION = 'Se debe ingresar al menos un motivo de indemnizaci�n';
    MSG_TRANSFERENCIA_NO_POSIBLE = 'El telev�a es nuevo, no se puede hacer transferencia';
    MSG_TRANSFERENCIA_DIFIERE_ALMACEN = 'La modalidad seleccionada de entrega difiere de la �ltima utilizada en este telev�a';
var
    ModalResultado : TmodalResult;
    f: TfrmModificarFechaAlta;
    Hora: Ttime;
    Fecha: TDate;
    UltimoAlmacenTag: integer;
begin
    ModalResultado := mrNone;
    if pnlIndemnizaciones.Visible then begin
        if cdConceptos.IsEmpty then begin
            MsgBoxBalloon(MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION,'Error',MB_ICONSTOP, dblConceptos);
            dblConceptos.SetFocus;
            ModalResultado := mrNone;
            Exit;
        end;
        UltimoAlmacenTag := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerUltimaUbicacionTAG(%d,%d)',[FreDatosVehiculo.ContextMark , FreDatosVehiculo.SerialNumber]));
        if (UltimoAlmacenTag = 0) and ( rgGarantiaTag.ItemIndex = 1 )  then begin
            MsgBoxBalloon(MSG_TRANSFERENCIA_NO_POSIBLE,'Error',MB_ICONSTOP, rgGarantiaTag);
            dblConceptos.SetFocus;
            ModalResultado := mrNone;
            Exit;
        end;
        if (UltimoAlmacenTag <> 0) and ( rgGarantiaTag.ItemIndex = 1 ) then begin
            if UltimoAlmacenTag <> AlmacenDestino  then begin
                MsgBoxBalloon(MSG_TRANSFERENCIA_DIFIERE_ALMACEN,'Error',MB_ICONSTOP, rgModalidadEntregaTag);
                dblConceptos.SetFocus;
                ModalResultado := mrNone;
                Exit;
            end;
        end;
        if (edNumeroVoucher.ValueInt > 0) and (not ValidarVoucher(edNumeroVoucher.ValueInt, FListaVouchers, edNumeroVoucher)) then begin
            ModalResultado := mrNone;
            Exit;
        end;
    end;
    if (not (FreDatosVehiculo.Enabled) or (FreDatosVehiculo.ValidarDatos(AlmacenDestino))) and  //Revision 3
       (not (FreDatosVehiculoNuevo.Enabled) or (FreDatosVehiculoNuevo.ValidarDatos(AlmacenDestino)))then  //Revision 3
        ModalResultado := mrRetry;

    while (ModalResultado = mrRetry) do begin
        Application.CreateForm(TfrmModificarFechaAlta, f);
        Fserialnumber:= serialnumber;
        FContextMark:= contextmark;
        FPatente:= patente;
        if f.Inicializa(FPatente, Fserialnumber, FContextMark, FechaAlta, FUltimaBajaCuenta) then begin       // SS_916_PDO_20120109  
            ModalResultado := f.ShowModal;
            if ModalResultado = mrOk then begin
                Hora := f.DtHoraAlta.Time;
                Fecha := f.DtFechaAlta.Date;
                FechaAlta := Fecha + Hora;																											// SS_916_PDO_20120106
//                txtFechaAlta.Text :=  FormatDateTime('dd-mm-yyyy', f.DtFechaAlta.Date ) + ' / ' + FormatDateTime('hh:nn', f.DtHoraAlta.Time);		// SS_916_PDO_20120106
                FCambioFecha := True;
            end;
        end else begin
            MsgBoxBalloon(MSG_FECHA_ALTA_DATOS_ERROR,'Faltan Datos',MB_ICONSTOP, btnFechaAlta);
            ModalResultado := mrCancel;
        end;
        f.Release;
    end;
end;
//Fin de Revision 2

{-----------------------------------------------------------------
        	ValidarExistenciaVoucher
 Revision 5
 Author: mbecerra
 Date: 08-Agosto-2008
 Description: Se agrega una validaci�n solicitada por el cliente, y es:
            	* si el cliente ingresa un n�mero de voucher,
                  debe tener entre los motivos de facturaci�n "Promoci�n La Tercera"
                * Si no ingresa el n�mero de voucher, entonces NO DEBE
                  tener entre los motivos de facturaci�n "Promoci�n la Tercera"
 -------------------------------------------------------------------------------}
function TFormSCDatosVehiculo.ValidarExistenciaVoucher;
resourcestring
    MSG_CONCEPTO_DEBE    = 'Falta el concepto "Promoci�n la Tercera"';
    MSG_CONCEPTO_NO_DEBE = 'Para incluir el concepto "Promoci�n la Tercera" debe ingresar un n�mero de voucher';

const
    CONST_CONCEPTO_LA_TERCERA = 152;
    
var
    ConceptoEncontrado : boolean;
    Book : TBookmark;
begin
	Result := False;
    ConceptoEncontrado := False;
    cdConceptos.DisableControls;
    Book := cdConceptos.GetBookmark;
    cdConceptos.First;
    while not (cdConceptos.Eof or ConceptoEncontrado) do begin
    	if not cdConceptos.FieldByName('CodigoConcepto').IsNull then begin
    		ConceptoEncontrado := (cdConceptos.FieldByName('CodigoConcepto').AsInteger = CONST_CONCEPTO_LA_TERCERA);
        end;

        cdConceptos.Next;
    end;


    if edNumeroVoucher.Text <> '' then begin
        if ConceptoEncontrado then Result := True
        else MsgBox(MSG_CONCEPTO_DEBE, Caption, MB_ICONEXCLAMATION);
    end
    else begin
        if ConceptoEncontrado then MsgBox(MSG_CONCEPTO_NO_DEBE, Caption, MB_ICONEXCLAMATION)
        else Result := True;

    end;

    cdConceptos.GotoBookmark(Book);
    cdConceptos.EnableControls;
end;

{******************************** Function Header ******************************
Function Name: btnAceptarClick
Author : vpaszkowicz
Date Created : 22/08/2008
Description : Controlo que no ingresen la misma RVM que la del veh�culo original.
Parameters : Sender: TObject
Return Value : None
Revision :1
    Author : vpaszkowicz
    Date : 27/08/2009
    Description :Luego de ejecutar las validaciones sobre veh�culos, agrego un
    control que me permita setear el -1 el c�digo de veh�culo si ha sido liberada
    la patente.
*******************************************************************************}
procedure TFormSCDatosVehiculo.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_MISMA_PATENTE = 'La patente del nuevo veh�culo no puede ser la misma que la del veh�culo original';
    MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION = 'Se debe ingresar al menos un motivo de indemnizaci�n';
    MSG_TRANSFERENCIA_NO_POSIBLE = 'El telev�a es nuevo, no se puede hacer transferencia';
    MSG_TRANSFERENCIA_DIFIERE_ALMACEN = 'La modalidad seleccionada de entrega difiere de la �ltima utilizada en este telev�a';
    MSG_VOUCHER_YA_UTILIZADO = 'El n�mero de voucher ya ha sido utilizado';
    MSG_FECHA_ALTA_ERROR = 'La fecha indicada como fecha de alta, no es v�lida para este telev�a y esta patente';
    MSG_FECHA_VCTO_ERROR = 'La fecha de vencimiento del Tag est� fuera del rango permitido';       //REV.8
var
    UltimoAlmacenTag: integer;
begin
    if (not (FreDatosVehiculo.Enabled) or (FreDatosVehiculo.ValidarDatos(AlmacenDestino))) and  //Revision 3
       (not (FreDatosVehiculoNuevo.Enabled) or (FreDatosVehiculoNuevo.ValidarDatos(AlmacenDestino)))then  //Revision 3
        ModalResult := mrOk;

    if (ModalResult = mrOk) then begin
        //Revision 5
        if not ValidarExistenciaVoucher() then begin
            ModalResult := mrNone;
            Exit;
        end;

    	//Valido que las patentes ingresadas sean distintas
        if not (ValidateControls([FreDatosVehiculoNuevo.txtPatente],
                                 [FreDatosVehiculo.Patente <> FreDatosVehiculoNuevo.Patente],
                                 Caption,
                                 [MSG_ERROR_MISMA_PATENTE])) then begin
            ModalResult := mrNone;

            Exit;
        end;

        //Si llega a liberar la patente, puede que el c�digo de veh�culo haya
        //Cambiado y ahora sea -1
        if not FreDatosVehiculoNuevo.Enabled then
            FreDatosVehiculo.ComprobarCodigoVehiculo
        else FreDatosVehiculoNuevo.ComprobarCodigoVehiculo;

        //Revision 2
        {   // Inicio Bloque: SS_916_PDO_20120220
        if txtFechaAlta.Visible then begin
            if not ModificarFechaAlta.frmModificarFechaAlta.ValidarFechaHoraAlta(FFechaAlta, FPatente, Fserialnumber, FContextMark) then begin
                MsgBoxBalloon(MSG_FECHA_ALTA_ERROR,'Error',MB_ICONSTOP, txtFechaAlta);
                ModalResult := mrNone;
                Exit;
            end;
        end;
        }   // Fin Bloque: SS_916_PDO_20120220
//Fin de Revision 2

        //REV.8
        //Valido que la fechavencimiento Tag est� dentro del rango esperado
        if pnlFechas.Visible AND edtFechaVctoTag.Visible then begin
            if (FechaVencimientoTag < FFechaAlta) or (FechaVencimientoTag > (IncMonth(FFechaAlta, 12 * FAniosGarantiaTag))) then begin
                MsgBoxBalloon(MSG_FECHA_VCTO_ERROR, 'Error', MB_ICONERROR, edtFechaVctoTag);
                ModalResult := mrNone;
                Exit;
            end;
        end;

      
		//FIN REV.8

        if pnlIndemnizaciones.Visible then begin
{INICIO: TASK_036_JMA_20160707
            if cdConceptos.IsEmpty then begin
                MsgBoxBalloon(MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION,'Error',MB_ICONSTOP, dblConceptos);
                dblConceptos.SetFocus;
                ModalResult := mrNone;
                Exit;
            end;
TERMINO: TASK_036_JMA_20160707}
            UltimoAlmacenTag := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerUltimaUbicacionTAG(%d,%d)',[FreDatosVehiculo.ContextMark , FreDatosVehiculo.SerialNumber]));
            if (UltimoAlmacenTag = 0) and ( rgGarantiaTag.ItemIndex = 1 )  then begin
                MsgBoxBalloon(MSG_TRANSFERENCIA_NO_POSIBLE,'Error',MB_ICONSTOP, rgGarantiaTag);
                dblConceptos.SetFocus;
                ModalResult := mrNone;
                Exit;
            end;
            if (UltimoAlmacenTag <> 0) and ( rgGarantiaTag.ItemIndex = 1 ) then begin
                if UltimoAlmacenTag <> AlmacenDestino  then begin
                    MsgBoxBalloon(MSG_TRANSFERENCIA_DIFIERE_ALMACEN,'Error',MB_ICONSTOP, rgModalidadEntregaTag);
                    dblConceptos.SetFocus;
                    ModalResult := mrNone;
                    Exit;
                end;
            end;
            if (edNumeroVoucher.ValueInt > 0) and (not ValidarVoucher(edNumeroVoucher.ValueInt, FListaVouchers, edNumeroVoucher)) then begin
                ModalResult := mrNone;
                Exit;
            end;
        end;
    end;

end;

// PROPIEDADES

function TFormSCDatosVehiculo.GetAlmacenDestino: integer;
var
	encontrado : boolean;
begin
//    case rgModalidadEntregaTag.ItemIndex of
//    	0 : Result := CONST_ALMACEN_EN_COMODATO;
//        1 : Result := CONST_ALMACEN_ARRIENDO;
//        2 : Result := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
//    end;

    if rbArriendoEnCuotas.Checked then  Result := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
    if rbArriendo.Checked then  Result := CONST_ALMACEN_ARRIENDO;
    if rbComodato.Checked then  Result := CONST_ALMACEN_EN_COMODATO;


	{REV.6
    if rgModalidadEntregaTag.ItemIndex = 0 then Result := CONST_ALMACEN_EN_COMODATO
	else begin
    	// Rev 5   ==> SS 769
    	// si dentro de los IDMotivoMovimientosCuentastelevias est� el Arriendo en Cuotas,
    	// entonces devuelve como c�digo de almacen el se�alado en CodigoConcepto.
        //
        // 29-Dic-2008, se prefija el almacen
        encontrado := False;
        cdConceptos.First;
        while not (encontrado or cdConceptos.Eof) do begin
            encontrado := (cdConceptos.FieldByName('IDMotivoMovCuentaTelevia').Value = CONST_CONCEPTO_ARRIENDO_TAG_CUOTAS);
        	cdConceptos.Next;
        end;

        if encontrado then Result := CONST_ALMACEN_ENTREGADO_EN_CUOTAS
        else Result := CONST_ALMACEN_ARRIENDO;
    end;
    }

end;

function TFormSCDatosVehiculo.GetAnio: integer;
begin
  Result := FreDatosVehiculo.Anio;
end;

function TFormSCDatosVehiculo.GetCategoria: integer;
begin
    Result := FreDatosVehiculo.CodigoCategoria;
end;

function TFormSCDatosVehiculo.GetCodigoColor: integer;
begin
    Result := FreDatosVehiculo.Color;
end;

function TFormSCDatosVehiculo.GetCodigoMarca: integer;
begin
    Result := FreDatosVehiculo.Marca;
end;

function TFormSCDatosVehiculo.GetCodigoTipo: integer;
begin
    Result := FreDatosVehiculo.CodigoTipo;
end;

function TFormSCDatosVehiculo.GetDatoCuenta: TCuentaVehiculo;
begin
    Result := FreDatosVehiculo.DatoCuenta;
end;

function TFormSCDatosVehiculo.GetDescripcionColor: AnsiString;
begin
    Result := FreDatosVehiculo.DescripcionColor;
end;

function TFormSCDatosVehiculo.GetDescripcionMarca: AnsiString;
begin
    Result := FreDatosVehiculo.DescripcionMarca;
end;

function TFormSCDatosVehiculo.GetDescripcionModelo: AnsiString;
begin
    Result := FreDatosVehiculo.DescripcionModelo;
end;

function TFormSCDatosVehiculo.GetDescripcionTipo: AnsiString;
begin
    Result := FreDatosVehiculo.DescripcionTipo;
end;

function TFormSCDatosVehiculo.GetDetalleVehiculoCompleta: Ansistring;
begin
    Result := FreDatosVehiculo.DetalleVehiculoCompleto;
end;

function TFormSCDatosVehiculo.GetDetalleVehiculoSimple: Ansistring;
begin
    Result := FreDatosVehiculo.DetalleVehiculoSimple;
end;

function TFormSCDatosVehiculo.GetDigitoVerificador: AnsiString;
begin
    Result := FreDatosVehiculo.DigitoVerificador;
end;

function TFormSCDatosVehiculo.GetDigitoVerificadorCorrecto: boolean;
begin
    Result := FreDatosVehiculo.DigitoVerificadorCorrecto;
end;

function TFormSCDatosVehiculo.GetDigitoVerificadorCorrectoRVM: boolean;
begin
    Result := FreDatosVehiculo.DigitoVerificadorCorrectoRVM;
end;

function TFormSCDatosVehiculo.GetDigitoVerificadorRVM: AnsiString;
begin
    Result := FreDatosVehiculo.DigitoVerificadorRVM;
end;

// INICIO : TASK_055_MGO_20160801
function TFormSCDatosVehiculo.GetForaneo: Boolean;
begin
    Result := FreDatosVehiculo.Foraneo;
end;
// FIN : TASK_055_MGO_20160801

function TFormSCDatosVehiculo.GetPatente: AnsiString;
begin
    Result := FreDatosVehiculo.Patente;
end;

function TFormSCDatosVehiculo.GetPatenteRVM: String;
begin
    Result := FreDatosVehiculo.PatenteRVM;
end;

function TFormSCDatosVehiculo.GetSerialNumber: DWORD;
begin
    Result := FreDatosVehiculo.SerialNumber;
end;

function TFormSCDatosVehiculo.GetTag: AnsiString;
begin
    Result := FreDatosVehiculo.NumeroTag;
end;

function TFormSCDatosVehiculo.GetTipoAsignacionTag: string;
begin
    if rgGarantiaTag.ItemIndex = 0 then Result := CONST_OP_TRANSFERENCIA_TAG_NUEVO
    else Result := CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA ;
end;

function TFormSCDatosVehiculo.GetTipoPatente: AnsiString;
begin
    Result := FreDatosVehiculo.TipoPatente;
end;

function TFormSCDatosVehiculo.GetContextMark: integer;
begin
    Result := FreDatosVehiculo.ContextMark;
end;

function TFormSCDatosVehiculo.GetObservaciones: Ansistring;
begin
    Result := FreDatosVehiculo.Observaciones;
end;

function TFormSCDatosVehiculo.GetNumeroVoucher: integer;
begin
    Result := edNumeroVoucher.ValueInt ;
end;

function TFormSCDatosVehiculo.GetDatoCuentaNueva: TCuentaVehiculo;
begin
    Result := FreDatosVehiculoNuevo.DatoCuenta;
end;

//Revision 2
function TFormSCDatosVehiculo.GetFechaAlta: TDateTime;
begin
    Result := FFechaAlta;
end;
//Fin de Revision 2

// Inicio Bloque: SS_916_PDO_20120106
procedure TFormSCDatosVehiculo.SetFechaAlta(Value: TDateTime);
begin
    FFechaAlta := Value;
    if FFechaAlta = NullDate then begin
        txtFechaAlta.Text    := EmptyStr;
        BtnFechaAlta.Enabled := False;
    end
    else begin
        txtFechaAlta.Text    := FormatDateTime('dd"-"mm"-"yyyy "/" hh:nn', FFechaAlta);
        BtnFechaAlta.Enabled := ExisteAcceso('Boton_ModificarFechaAlta');
    end;
end;

procedure TFormSCDatosVehiculo.SetFechaVtoTAG(Value: TDateTime);
begin
    FFechaVctoTag := Value;
    if FFechaVctoTag = NullDate then begin
        edtFechaVctoTag.Text        := EmptyStr;
        btnEditFechaVctoTag.Enabled := False;
    end
    else begin
        edtFechaVctoTag.Text := FormatDateTime('dd"-"mm"-"yyyy "/" hh:nn', FFechaVctoTag);
        if FFechaVctoTag < FHoy then begin
            edtFechaVctoTag.Font.Color := clRed;
        end
        else begin
            edtFechaVctoTag.Font.Color := clBlack;
        end;
        btnEditFechaVctoTag.Enabled := ExisteAcceso('Modif_Fecha_Vencimiento_TAG');
    end;
end;
// Fin Bloque: SS_916_PDO_20120106

function TFormSCDatosVehiculo.GetFechaCreacion: TDateTime;
begin
    Result := FreDatosVehiculoNuevo.FechaCreacion;
end;

procedure TFormSCDatosVehiculo.lblSeleccionarClick(Sender: TObject);
var
    f: TfrmSeleccionarMovimientosTeleviaFORM;
    ParametroTipoOperacion: string;
    ParametroCodigoAlmacen: integer;
    ParametroOperacionTAG: integer;
begin
    //en este momento no sabemos si el telev�a va a arriendo o comodato o si es nuevo o transferencia
    //como esa informacion es necesaria para armar la lista de cargos de indemnizacion posibles
    //lo leemos de lo que indicaron en los radiobuttons

//    case rgModalidadEntregaTag.ItemIndex of
//    	0 : ParametroCodigoAlmacen := CONST_ALMACEN_EN_COMODATO;
//        1 : ParametroCodigoAlmacen := CONST_ALMACEN_ARRIENDO;
//        2 : ParametroCodigoAlmacen := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
//    end;

    if rbArriendoEnCuotas.Checked then ParametroCodigoAlmacen := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
    if rbArriendo.Checked then ParametroCodigoAlmacen := CONST_ALMACEN_ARRIENDO;
    if rbComodato.Checked then ParametroCodigoAlmacen := CONST_ALMACEN_EN_COMODATO;


    if rgGarantiaTag.ItemIndex = 0 then ParametroTipoOperacion := CONST_OP_TRANSFERENCIA_TAG_NUEVO
    else ParametroTipoOperacion := CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA;

    if GroupBoxNuevo.Visible then begin
        ParametroOperacionTAG := CONST_OPERACION_TAG_CBIOVEH;
        ParametroTipoOperacion := CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA;
    end else ParametroOperacionTAG := CONST_OPERACION_TAG_AGREGAR;


    Application.CreateForm(TfrmSeleccionarMovimientosTeleviaFORM, f);
    if f.Inicializar(ParametroOperacionTAG, ParametroCodigoAlmacen, ParametroTipoOperacion ) and (f.ShowModal = mrOk) then begin
      // Cargamos el dataset con lo que se selecciono
        f.ListaConceptos.First;
        while not f.ListaConceptos.Eof do begin
            if f.ListaConceptos.FieldByName('Seleccionado').AsBoolean then begin
               //INICIO: TASK_028_ECA_20160608
               if cdConceptos.RecordCount > 0 then
                  cdConceptos.Delete;
               //FIN: TASK_028_ECA_20160608

              //Agregamos el concepto a la lista
                cdConceptos.AppendRecord([
                    f.ListaConceptos.FieldByName('Seleccionado'     ).Value,
                    f.ListaConceptos.FieldByName('Codigoconcepto'   ).Value,
                    f.ListaConceptos.FieldByName('DescripcionMotivo').Value,
                    f.ListaConceptos.FieldByName('PrecioOrigen'     ).Value,
                    f.ListaConceptos.FieldByName('Moneda'           ).Value,
                    f.ListaConceptos.FieldByName('Cotizacion'       ).Value,
                    f.ListaConceptos.FieldByName('PrecioEnPesos'    ).Value,
                    f.ListaConceptos.FieldByName('Comentario'       ).Value,
                    //Revision 5
                    f.ListaConceptos.FieldByName('IDMotivoMovCuentaTelevia').Value ]);
                FImporteTotalMovimientos := FImporteTotalMovimientos + f.ListaConceptos.FieldByName('PrecioEnPesos').AsVariant;
          end;
          f.ListaConceptos.Next;
      end;
      f.Release;
    end else begin
        f.Release;
    end;
    lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
end;

procedure TFormSCDatosVehiculo.mnuEliminarConceptoClick(Sender: TObject);
begin
    if not cdConceptos.eof then begin
        FImporteTotalMovimientos := FImporteTotalMovimientos - cdConceptos.FieldByName('PrecioEnPesos').AsVariant ;
        lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
        cdConceptos.Delete;
    end;
end;

// Inicio Bloque: SS_916_PDO_20120106
procedure TFormSCDatosVehiculo.rbArriendoClick(Sender: TObject);
begin
    rbArriendoEnCuotas.Checked := False;
    rbArriendo.Checked := True;
    rbComodato.Checked := False;
end;

procedure TFormSCDatosVehiculo.rbArriendoEnCuotasClick(Sender: TObject);
begin
    rbArriendoEnCuotas.Checked := True;
    rbArriendo.Checked := False;
    rbComodato.Checked := False;
end;

procedure TFormSCDatosVehiculo.rbComodatoClick(Sender: TObject);
begin
    rbArriendoEnCuotas.Checked := False;
    rbArriendo.Checked := False;
    rbComodato.Checked := True;
end;

procedure TFormSCDatosVehiculo.rgGarantiaTagClick(Sender: TObject);
begin
    if FreDatosVehiculo.txtNumeroTelevia.Enabled then begin
        ValidarFechasAltaCuenta;
    end;
end;
// Fin Bloque: SS_916_PDO_20120106

procedure TFormSCDatosVehiculo.AgregarConceptos1Click(Sender: TObject);
begin
    lblSeleccionarClick(Sender);
end;

procedure TFormSCDatosVehiculo.dblConceptosContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
    mnuEliminarConcepto.Enabled := not cdConceptos.IsEmpty;
end;


procedure TFormSCDatosVehiculo.dblConceptosDrawDone(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState);
begin
  rgModalidadEntregaTag.Enabled := dsConceptos.DataSet.IsEmpty;
  rbArriendoEnCuotas.Enabled := rgModalidadEntregaTag.Enabled and (PERMITIR_ALTA_TAG_ARRIENDO_CUOTAS = 1);
  rbArriendo.Enabled := rgModalidadEntregaTag.Enabled and (PERMITIR_ALTA_TAG_ARRIENDO = 1);
  rbComodato.Enabled := rgModalidadEntregaTag.Enabled and (PERMITIR_ALTA_TAG_COMODATO = 1);

  rgGarantiaTag.Enabled := dsConceptos.DataSet.IsEmpty;
end;

procedure TFormSCDatosVehiculo.dblConceptosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = VK_DELETE) or (Key = VK_BACK) then mnuEliminarConceptoClick(Sender);
end;

procedure TFormSCDatosVehiculo.edNumeroVoucherExit(
  Sender: TObject);
begin
    if edNumeroVoucher.ValueInt > 0 then
        if not ValidarVoucher( edNumeroVoucher.ValueInt, FListaVouchers, edNumeroVoucher) then
            edNumeroVoucher.SetFocus;
end;

// Inicio Bloque: SS_916_PDO_20120109
procedure TFormSCDatosVehiculo.ValidarFechasAltaCuenta;
    var
        _Patente,
        _DigitoVerificador: string;
        _ContractSerialNumber: Cardinal;
        _ContextMark: Integer;
        _TAGEnTransferencia: Integer;
        _PFechaBajaEnVehiculosEliminados: TDateTime;
    resourcestring
        MSG_ERROR_MENSAJE = 'Se produjo un Error al ejecutar el SP ObtenerFechasAltaCuenta. Error: %s';
        MSG_ERROR_CAPTION = 'Validar Fechas Alta Cuenta';
begin

    if (FreDatosVehiculo.txtNumeroTelevia.Enabled) then begin

        _Patente              := Trim(FreDatosVehiculo.txtPatente.Text);
        _DigitoVerificador    := Trim(FreDatosVehiculo.txtDigitoVerificador.Text);
        _ContractSerialNumber := FreDatosVehiculo.SerialNumber;
        _ContextMark          := FreDatosVehiculo.ContextMark;
        _TAGEnTransferencia   := rgGarantiaTag.ItemIndex;

        if  (_Patente <> EmptyStr) and (_DigitoVerificador <> EmptyStr) and (_ContractSerialNumber <> 0) and (_ContextMark <> 0) then begin
            try
                with spValidarFechasAltaCuenta do begin
                    if Active then Close;
                    Parameters.Refresh;

                    Parameters.ParamByName('@Patente').Value              := _Patente;
                    Parameters.ParamByName('@ContractSerialNumber').Value := _ContractSerialNumber;
                    Parameters.ParamByName('@ContextMark').Value          := _ContextMark;
                    Parameters.ParamByName('@FechaHoraUltimaBaja').Value  := null;
                    Parameters.ParamByName('@FechaHoraAltaCuenta').Value  := null;
                    Parameters.ParamByName('@FechaHoraAltaTAG').Value     := null;
                    Parameters.ParamByName('@FechaHoraVtoTAG').Value      := null;
                    Parameters.ParamByName('@TAGEnTransferencia').Value   := _TAGEnTransferencia;
                    ExecProc;

                    FechaAlta           := Parameters.ParamByName('@FechaHoraAltaCuenta').Value;
                    FUltimaBajaCuenta   := IIf(Parameters.ParamByName('@FechaHoraUltimaBaja').Value <> Null, Parameters.ParamByName('@FechaHoraUltimaBaja').Value, NullDate) ;
                    FechaVencimientoTag := Parameters.ParamByName('@FechaHoraVtoTAG').Value;

                    if Assigned(FDatosConvenioSCDDatosVehiculo) then begin
                        if FDatosConvenioSCDDatosVehiculo.Cuentas.ExisteDadoBaja(_Patente, _ContextMark, _ContractSerialNumber, _PFechaBajaEnVehiculosEliminados) then begin
                            FUltimaBajaCuenta   := _PFechaBajaEnVehiculosEliminados;
                            FechaAlta           := IncMinute(_PFechaBajaEnVehiculosEliminados, 1);
                            FechaVencimientoTag := IncMonth(FFechaAlta, 12 * FAniosGarantiaTag);    // SS_916_PDO_20120214
                        end;
                    end;
                    
                    {
                    ShowMessage
                        (
                            '�ltima Baja Cta.: ' + IIf(FUltimaBajaCuenta <> NullDate, FormatDateTime('dd/mm/yyyy hh:nn:ss', FUltimaBajaCuenta), 'Sin Historial') +
                            '  Alta Cta.: ' + FormatDateTime('dd/mm/yyyy hh:nn:ss', Parameters.ParamByName('@FechaHoraAltaCuenta').Value) +
                            '  Alta TAG: ' + FormatDateTime('dd/mm/yyyy hh:nn:ss', Parameters.ParamByName('@FechaHoraAltaTAG').Value) +
                            '  Vto. TAG: ' + FormatDateTime('dd/mm/yyyy hh:nn:ss', Parameters.ParamByName('@FechaHoraVtoTAG').Value)
                        );
                    }
                end;
            except
                on e:Exception do begin
                    ShowMsgBoxCN(MSG_ERROR_CAPTION,Format(MSG_ERROR_MENSAJE, [e.Message]), MB_ICONERROR, Self);
                end;
            end;
        end
        else begin
            FechaAlta           := NullDate;
            FechaVencimientoTag := NullDate;
            FUltimaBajaCuenta   := NullDate;
            FCambioFecha        := False;

            //ShowMessage('// vaciarfechas');
        end;
    end;
end;
// Fin Bloque: SS_916_PDO_20120109

end.
