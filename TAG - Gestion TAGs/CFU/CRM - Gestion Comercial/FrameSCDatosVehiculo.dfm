object FmeSCDatosVehiculo: TFmeSCDatosVehiculo
  Left = 0
  Top = 0
  Width = 420
  Height = 215
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  TabOrder = 0
  TabStop = True
  DesignSize = (
    420
    215)
  object Label13: TLabel
    Left = 7
    Top = 12
    Width = 49
    Height = 13
    Caption = 'Patente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 6
    Top = 62
    Width = 40
    Height = 13
    Caption = 'Marca:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label20: TLabel
    Left = 6
    Top = 86
    Width = 38
    Height = 13
    Caption = 'Modelo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label47: TLabel
    Left = 7
    Top = 136
    Width = 27
    Height = 13
    Caption = 'A'#241'o:'
    FocusControl = txt_anio
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblTipoVehiculo: TLabel
    Left = 6
    Top = 37
    Width = 30
    Height = 13
    Caption = 'Tipo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblTelevia: TLabel
    Left = 144
    Top = 136
    Width = 40
    Height = 13
    Caption = 'Telev'#237'a:'
    FocusControl = txtNumeroTelevia
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lbl_PatenteRVM: TLabel
    Left = 342
    Top = 59
    Width = 75
    Height = 13
    Caption = 'Patente RNVM:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label21: TLabel
    Left = 6
    Top = 112
    Width = 27
    Height = 13
    Caption = 'Color:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblVehiculoRobado: TLabel
    Left = 228
    Top = 12
    Width = 185
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = 'lblVehiculoRobado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object cb_marca: TComboBox
    Left = 85
    Top = 58
    Width = 236
    Height = 21
    Style = csDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 6
  end
  object txt_anio: TNumericEdit
    Left = 86
    Top = 132
    Width = 47
    Height = 21
    Color = 16444382
    MaxLength = 4
    TabOrder = 8
    OnExit = txt_anioExit
  end
  object cbTipoVehiculo: TVariantComboBox
    Left = 85
    Top = 33
    Width = 166
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 5
    Items = <>
  end
  object txtDescripcionModelo: TEdit
    Left = 85
    Top = 84
    Width = 236
    Height = 21
    Hint = 'Modelo'
    CharCase = ecUpperCase
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
    OnKeyPress = txtDescripcionModeloKeyPress
  end
  object txtPatente: TEdit
    Left = 85
    Top = 9
    Width = 108
    Height = 21
    Hint = 'Patente'
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 8
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnChange = txtPatenteChange
    OnEnter = txtPatenteEnter
    OnExit = PatentesExit
    OnKeyPress = PatentesKeyPress
  end
  object txtDigitoVerificador: TEdit
    Left = 199
    Top = 8
    Width = 26
    Height = 21
    Hint = 'D'#237'gito Verificador'
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Visible = False
    OnChange = txtDigitoVerificadorChange
    OnExit = DigitosVerificadoresExit
    OnKeyPress = DigitosVerificadoresKeyPress
  end
  object txtNumeroTelevia: TEdit
    Left = 197
    Top = 132
    Width = 126
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 11
    TabOrder = 9
    Visible = False
    OnChange = txtNumeroTeleviaChange
    OnExit = txtNumeroTeleviaExit
    OnKeyPress = txtNumeroTeleviaKeyPress
  end
  object txt_PatenteRVM: TEdit
    Left = 278
    Top = 31
    Width = 108
    Height = 21
    CharCase = ecUpperCase
    Enabled = False
    MaxLength = 6
    TabOrder = 3
    Visible = False
    OnEnter = txtPatenteEnter
    OnExit = PatentesExit
    OnKeyPress = PatentesKeyPress
  end
  object txt_DigitoVerificadorRVM: TEdit
    Left = 391
    Top = 32
    Width = 26
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 1
    TabOrder = 4
    Visible = False
    OnExit = DigitosVerificadoresExit
    OnKeyPress = DigitosVerificadoresKeyPress
  end
  object cb_color: TComboBox
    Left = 85
    Top = 108
    Width = 126
    Height = 21
    Style = csDropDownList
    Enabled = False
    ItemHeight = 13
    TabOrder = 10
    TabStop = False
  end
  object cb_modelo: TComboBox
    Left = 509
    Top = 85
    Width = 41
    Height = 21
    Style = csDropDownList
    Enabled = False
    ItemHeight = 13
    TabOrder = 11
    TabStop = False
    Visible = False
  end
  object GBListas: TGroupBox
    Left = 0
    Top = 155
    Width = 322
    Height = 60
    Align = alCustom
    Caption = '  Status de Listas en el Tag  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    object ledVerde: TLed
      Left = 18
      Top = 18
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGreen
      ColorOff = clBtnFace
    end
    object ledGris: TLed
      Left = 18
      Top = 36
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGray
      ColorOff = clBtnFace
    end
    object ledAmarilla: TLed
      Left = 189
      Top = 18
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clYellow
      ColorOff = clBtnFace
    end
    object ledNegra: TLed
      Left = 189
      Top = 36
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clBlack
      ColorOff = clBtnFace
    end
    object Label5: TLabel
      Left = 39
      Top = 18
      Width = 28
      Height = 13
      Caption = 'Verde'
    end
    object Label6: TLabel
      Left = 39
      Top = 36
      Width = 18
      Height = 13
      Caption = 'Gris'
    end
    object Label7: TLabel
      Left = 210
      Top = 18
      Width = 36
      Height = 13
      Caption = 'Amarilla'
    end
    object Label8: TLabel
      Left = 210
      Top = 36
      Width = 29
      Height = 13
      Caption = 'Negra'
    end
  end
  object BtnRecuperar: TButton
    Left = 358
    Top = 8
    Width = 61
    Height = 21
    Caption = 'Recuperar'
    Enabled = False
    TabOrder = 2
    Visible = False
    OnClick = BtnRecuperarClick
  end
  object VerificarFormatoPatenteChilena: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarFormatoPatenteChilena'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 82
    Top = 175
  end
  object VerificarFormatoPatenteDiplomatica: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarFormatoPatenteDiplomatica'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 114
    Top = 175
  end
  object spLeerDatosTAG: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'LeerDatosTAG'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 146
    Top = 175
  end
  object spValidarSiTagTieneGarantia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ValidarSiTagTieneGarantia'
    Parameters = <>
    Left = 328
    Top = 40
  end
end
