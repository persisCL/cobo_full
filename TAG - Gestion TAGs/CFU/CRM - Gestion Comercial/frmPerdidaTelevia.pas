unit frmPerdidaTelevia;

interface

{******************************************************************************
Revision    : 1
Author      : jconcheyro
Date Created: 19/12/2006
Description : Se agrega combo de Motivos para generar los MovimientosCuentas
por  indemnizaciones
*******************************************************************************}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, UtilDB,
  Dialogs, DateEdit, StdCtrls, Validate, TimeEdit, ExtCtrls, PeaProcs, DMConnection,
  Util, VariantComboBox, UtilProc, Menus, ListBoxEx, DBListEx, DB, DBClient,
  frmSeleccionarMovimientosTelevia, Convenios;

type
  Tfrm_PerdidaTelevia = class(TForm)
    cb_Robado: TCheckBox;
    gb_Denuncia: TGroupBox;
    txt_HoraLimDenuncia: TTimeEdit;
    txt_FechaLimDenuncia: TDateEdit;
    gb_Cuenta: TGroupBox;
    txt_HoraLimCambio: TTimeEdit;
    txt_FechaLimCambio: TDateEdit;
    pnl_Botones: TPanel;
    btn_Cancelar: TButton;
    btn_Aceptar: TButton;
    Memo_Mensaje: TMemo;
    dblConceptos: TDBListEx;
    Bevel1: TBevel;
    Label10: TLabel;
    lblSeleccionar: TLabel;
    cdConceptos: TClientDataSet;
    dsConceptos: TDataSource;
    mnuGrillaConceptos: TPopupMenu;
    mnuEliminarConcepto: TMenuItem;
    Label9: TLabel;
    lblTotalMovimientos: TLabel;
    lblTotalMovimientosTxt: TLabel;
    gbModalidadEntregaTelevia: TGroupBox;
    lblModalidadEntregaTelevia: TLabel;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure lblSeleccionarClick(Sender: TObject);
    procedure mnuEliminarConceptoClick(Sender: TObject);
    procedure dblConceptosContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure dblConceptosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    FImporteTotalMovimientos: int64;
    FVehiculo: TVehiculosConvenio;
    function GetFechaLimiteEnvioDenuncia: TDateTime;
    function GetCambiarTelevia: Boolean;
    function GetVehiculoRobado: Boolean;
    function GetFechaLimiteCambioTelevia: TDateTime;
    function GetFechaLimiteElimnacionCuenta: TDateTime;
//    function  GetConceptoMotivoMovimientoCuenta: integer;
//    function  GetDescripcionConceptoMotivoMovimientoCuenta: string;

    { Private declarations }
  public
    { Public declarations }
    function Inicializar(const Vehiculo: TVehiculosConvenio): Boolean;
    property FechaLimiteEnvioDenuncia: TDateTime read GetFechaLimiteEnvioDenuncia;
    property FechaLimiteCambioTelevia: TDateTime read GetFechaLimiteCambioTelevia;
    property FechaLimiteElimnacionCuenta: TDateTime read GetFechaLimiteElimnacionCuenta;
    property CambiarTelevia: Boolean read GetCambiarTelevia;
    property VehiculoRobado: Boolean read GetVehiculoRobado;

  end;

var
  frm_PerdidaTelevia: Tfrm_PerdidaTelevia;


resourcestring
    CONS_CAMBIO_TELEVIA = 'Fecha Limite Cambio Televia';
    CONS_CAMBIO_CUENTA = 'Fecha Limite Eliminación de Cuenta';

implementation

uses PeaTypes, RStrings;

{$R *.dfm}

procedure Tfrm_PerdidaTelevia.btn_CancelarClick(Sender: TObject);
begin
    close;
end;

procedure Tfrm_PerdidaTelevia.btn_AceptarClick(Sender: TObject);
ResourceString
    MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION = 'Se debe ingresar al menos un motivo de indemnización';
begin
    if cdConceptos.IsEmpty then begin
         MsgBoxBalloon(MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION,'Error',MB_ICONSTOP, dblConceptos);
         dblConceptos.SetFocus;
         Exit;
    end;
    ModalResult := mrOk;
end;


procedure Tfrm_PerdidaTelevia.dblConceptosContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
    mnuEliminarConcepto.Enabled := not cdConceptos.IsEmpty;
end;

procedure Tfrm_PerdidaTelevia.dblConceptosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Key = VK_DELETE) or (Key = VK_BACK) then mnuEliminarConceptoClick(Sender);
end;

function Tfrm_PerdidaTelevia.Inicializar(const Vehiculo: TVehiculosConvenio): Boolean;
var
	TituloMsg,
	DescripcionMsg,
	ComentarioMsg: ANSIString;
begin
	txt_FechaLimDenuncia.Date :=  NowBase(DMConnections.BaseCAC) + 2;
	txt_HoraLimDenuncia.Time := NowBase(DMConnections.BaseCAC);

	txt_FechaLimCambio.Date :=  NowBase(DMConnections.BaseCAC) + 2;
	txt_HoraLimCambio.Time := NowBase(DMConnections.BaseCAC);


    FImporteTotalMovimientos := 0;
    FVehiculo := Vehiculo;

    if FVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_EN_COMODATO then
        lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_COMODATO;

    if FVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ARRIENDO then
        lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_ARRIENDO;

    //SS 769
	if FVehiculo.Cuenta.CodigoAlmacenDestino = CONST_ALMACEN_ENTREGADO_EN_CUOTAS then
        lblModalidadEntregaTelevia.Caption := CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS;


	ObtenerMensaje(DMConnections.BaseCAC, CONST_CODIGO_MSG_PERDIDA_TAG, TituloMsg, DescripcionMsg, ComentarioMsg);
	Memo_Mensaje.Lines.Clear;
	Memo_Mensaje.Lines.Add (DescripcionMsg);
    gb_Cuenta.Caption := CONS_CAMBIO_CUENTA;
	Result := True
end;

procedure Tfrm_PerdidaTelevia.lblSeleccionarClick(Sender: TObject);
var
    f: TfrmSeleccionarMovimientosTeleviaFORM;
begin
    Application.CreateForm(TfrmSeleccionarMovimientosTeleviaFORM, f);
    if f.Inicializar(CONST_OPERACION_TAG_PERDIDO, FVehiculo.Cuenta.CodigoAlmacenDestino, '') and
            (f.ShowModal = mrOk) then begin
      // Cargamos el dataset con lo que se selecciono
        f.ListaConceptos.First;
        while not f.ListaConceptos.Eof do begin
            if f.ListaConceptos.FieldByName('Seleccionado').AsBoolean then begin
              //Agregamos el concepto a la lista
                cdConceptos.AppendRecord([
                    f.ListaConceptos.FieldByName('Seleccionado'     ).Value,
                    f.ListaConceptos.FieldByName('Codigoconcepto'   ).Value,
                    f.ListaConceptos.FieldByName('DescripcionMotivo').Value,
                    f.ListaConceptos.FieldByName('PrecioOrigen'     ).Value,
                    f.ListaConceptos.FieldByName('Moneda'           ).Value,
                    f.ListaConceptos.FieldByName('Cotizacion'       ).Value,
                    f.ListaConceptos.FieldByName('PrecioEnPesos'    ).Value,
                    f.ListaConceptos.FieldByName('Comentario'       ).Value
                ]);
                FImporteTotalMovimientos := FImporteTotalMovimientos + f.ListaConceptos.FieldByName('PrecioEnPesos').AsVariant;

          end;
          f.ListaConceptos.Next;
      end;
      f.Release;
    end else begin
        f.Release;
    end;
    lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);

end;

procedure Tfrm_PerdidaTelevia.mnuEliminarConceptoClick(Sender: TObject);
begin
    if not cdConceptos.eof then begin
        FImporteTotalMovimientos := FImporteTotalMovimientos - cdConceptos.FieldByName('PrecioEnPesos').AsVariant ;
        lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
        cdConceptos.Delete;
    end;
end;

function Tfrm_PerdidaTelevia.GetFechaLimiteEnvioDenuncia: TDateTime;
begin
    Result := txt_FechaLimDenuncia.Date + txt_HoraLimDenuncia.Time;
end;

function Tfrm_PerdidaTelevia.GetCambiarTelevia: Boolean;
begin
    Result := False;
end;

function Tfrm_PerdidaTelevia.GetVehiculoRobado: Boolean;
begin
    Result := cb_Robado.Checked;
end;

function Tfrm_PerdidaTelevia.GetFechaLimiteCambioTelevia: TDateTime;
begin
    if CambiarTelevia then Result := txt_FechaLimCambio.Date + txt_HoraLimCambio.Time
    else Result := NullDate;
end;

function Tfrm_PerdidaTelevia.GetFechaLimiteElimnacionCuenta: TDateTime;
begin
    if not CambiarTelevia then Result := txt_FechaLimCambio.Date + txt_HoraLimCambio.Time
    else Result := NullDate;
end;

end.



