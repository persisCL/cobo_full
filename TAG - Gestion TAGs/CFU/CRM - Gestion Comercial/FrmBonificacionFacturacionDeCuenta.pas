{-----------------------------------------------------------------------------
 Unit Name: FrmBonificacionFacturacionDeCuenta
 Author:    ggomez
 Purpose: Permitir colocar una cuenta de un convenio para que se bonifique la
    facturaci�n los tr�nsitos de la misma.
 History:

 Revision 1
 	Author: ggomez
    Date: 19/05/2005
    Description: Agregu� el control de que el rango de la bonificaci�n que se
    	est� por ingresar NO se solape con las fechas de alguna otra
        bonificaci�n.

 Revision 2
 	Author: ggomez
    Date: 19/05/2005
    Description: Correg� un bug de que no permit�a modificar los datos de una
        bonificaci�n vigente que ya exist�a en la BD.

 Revision 3
 	Author: mbecerra
    Date: 14-Septiembre-2009
    Description:		(Ref. SS 832)
            	El cliente solicita que no se muestre el digito verificador
-----------------------------------------------------------------------------}

unit FrmBonificacionFacturacionDeCuenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Validate, DateEdit, ExtCtrls, StdCtrls, DPSControls,
  RStrings, UtilProc, PeaProcs, PeaTypes, Util, DMConnection, DB, ADODB,
  DmiCtrls;

type
  TFormBonificacionFacturacionDeCuenta = class(TForm)
    gb_DatosVehiculo: TGroupBox;
    lbl_Patente: TLabel;
    lbl_Televia: TLabel;
    nb_Botones: TNotebook;
    pnlDatos: TPanel;
    lbl_FechaDesde: TLabel;
    lbl_FechaFin: TLabel;
    lbl_Usuario: TLabel;
    lbl_FechaModificacion: TLabel;
    lbl_Tipo: TLabel;
    lbl_Valor: TLabel;
    txt_FechaInicio: TDateEdit;
    txt_FechaFin: TDateEdit;
    txt_Porcentaje: TNumericEdit;
    txt_Importe: TNumericEdit;
    rb_Porcentaje: TRadioButton;
    rb_Importe: TRadioButton;
    lblPatente: TLabel;
    lblTelevia: TLabel;
    lblCodUsuario: TLabel;
    lblFechaModif: TLabel;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    btn_Salir: TButton;
    procedure btn_SalirClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure rb_PorcentajeClick(Sender: TObject);
    procedure rb_ImporteClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoConvenio,
    FIndiceVehiculo: Integer;
    FBonificarFacturacion: TBonificarFacturacion;
    FEximirFacturacion: TEximirFacturacion;
    FPatenteVehiculo,
    FDigitoVerificadorPatente,
    FNroTelevia: AnsiString;
    FModo: TStateConvenio;

    procedure HabilitarControlesIngresoDatos(Valor: Boolean);
    procedure MostrarDatos;
    function ExisteBonificacion(FechaInicio, FechaFin: TDate): Boolean;
    function ExisteEximicion(FechaInicio, FechaFin: TDate): Boolean;
    function SolapaConEximicion: Boolean;
  public
    { Public declarations }
    function Inicializar(Modo: TStateConvenio; CodigoConvenio, IndiceVehiculo: Integer;
        BonificarFacturacion: TBonificarFacturacion;
        EximirFacturacion: TEximirFacturacion;
        PatenteVehiculo, DigitoVerificadorPatente, NroTelevia: AnsiString): Boolean;

    function GetBonificarFacturacion: TBonificarFacturacion;
  end;

var
  FormBonificacionFacturacionDeCuenta: TFormBonificacionFacturacionDeCuenta;

implementation

{$R *.dfm}

{ TFormBonificacionFacturacionDeCuenta }

function TFormBonificacionFacturacionDeCuenta.GetBonificarFacturacion: TBonificarFacturacion;
begin
    Result := FBonificarFacturacion;
end;

procedure TFormBonificacionFacturacionDeCuenta.HabilitarControlesIngresoDatos(
  Valor: Boolean);
begin
    EnableControlsInContainer(pnlDatos, valor);
    (* Si Valor es True, mostrar los botonos Aceptar y Cancelar. *)
    nb_Botones.PageIndex := iif(Valor, 0, 1);
end;

function TFormBonificacionFacturacionDeCuenta.Inicializar(
  Modo: TStateConvenio; CodigoConvenio, IndiceVehiculo: Integer;
  BonificarFacturacion: TBonificarFacturacion;
  EximirFacturacion: TEximirFacturacion;
  PatenteVehiculo, DigitoVerificadorPatente, NroTelevia: AnsiString): Boolean;
resourcestring
    STR_BONIFICAR_TRANSITOS = 'Bonificar Tr�nsitos';
begin
    try
        FModo := Modo;
        FCodigoConvenio := CodigoConvenio;
        FIndiceVehiculo := IndiceVehiculo;
        FBonificarFacturacion := BonificarFacturacion;
        FEximirFacturacion := EximirFacturacion;

        FPatenteVehiculo := Trim(PatenteVehiculo);
        FDigitoVerificadorPatente := Trim(DigitoVerificadorPatente);
        FNroTelevia := Trim(NroTelevia);

        lblPatente.Caption := FPatenteVehiculo; //REV.3 + '-' + FDigitoVerificadorPatente;
        lblTelevia.Caption := FNroTelevia;

        (* Habilitar los controles de ingreso de datos seg�n el Modo con el que
        se llama al form. *)
        HabilitarControlesIngresoDatos(FModo <> scNormal);

        (* Mostrar los datos de la bonificaci�n de facturaci�n para la cuenta. *)
        MostrarDatos;

        Result := True;
    except
        on e: Exception do begin
    		Result := False;
            MsgBoxErr(Format(MSG_ERROR_INICIALIZACION, [STR_BONIFICAR_TRANSITOS]),
                E.Message, Caption, MB_ICONSTOP);
        end;

    end;
end;

procedure TFormBonificacionFacturacionDeCuenta.MostrarDatos;
begin
    // Incializar los valores de los controles.
    with FBonificarFacturacion do begin
        txt_FechaInicio.Date := FechaInicio;
        txt_FechaFin.Date := FechaFinalizacion;
        rb_Porcentaje.Checked := (txt_FechaInicio.Date < 0) or (Porcentaje > 0);
        rb_PorcentajeClick(Self);
        rb_Importe.Checked := (Importe > 0);
        rb_ImporteClick(Self);
        txt_Porcentaje.Value := Porcentaje;
        txt_Importe.ValueInt := Importe;
        lblCodUsuario.Caption := Usuario;
        lblFechaModif.Caption := EmptyStr;
        if FechaHoraModificacion > 0 then
            lblFechaModif.Caption := FormatDateTime('dd/mm/yyyy hh:nn:ss', FechaHoraModificacion);

        txt_FechaInicio.ReadOnly := not FechaInicioEditable;
        txt_FechaFin.ReadOnly := not FechaFinalizacionEditable;
        (* Si la Fecha de Fin no se permite editar, NO permitir editar el resto de
        los datos. *)
        rb_Porcentaje.Enabled := FechaFinalizacionEditable;
        rb_Importe.Enabled := rb_Porcentaje.Enabled;
        txt_Porcentaje.Enabled := rb_Porcentaje.Enabled;
        txt_Importe.Enabled := rb_Porcentaje.Enabled;

    end; // with

end;

procedure TFormBonificacionFacturacionDeCuenta.btn_SalirClick(
  Sender: TObject);
begin
    Close;
end;

procedure TFormBonificacionFacturacionDeCuenta.btn_CancelarClick(
  Sender: TObject);
begin
    Close;
end;

procedure TFormBonificacionFacturacionDeCuenta.btn_AceptarClick(
  Sender: TObject);
resourcestring
    MSG_FECHA_DEBE_CONTENER_UN_VALOR = 'La Fecha debe contener un valor.';
    MSG_FECHAFIN_DEBE_SER_POSTERIOR_A_FECHAINICIO = 'La Fecha Hasta debe ser posterior a la Fecha Desde';
    MSG_ERROR_EXISTE_BONIFICACION_BD = 'El veh�culo tiene una bonificaci�n almacenada en la base de datos,'
        + CRLF + 'cuyas fechas se intercalan con las de esta bonificaci�n.'
        + CRLF + 'No se permite Bonificar la Facturaci�n.';
    MSG_ERROR_EXISTE_EXIMICION_BD = 'El veh�culo tiene una eximici�n almacenada en la base de datos,'
        + CRLF + 'cuyas fechas se intercalan con las de la bonificaci�n.'
        + CRLF + 'No se permite Bonificar la Facturaci�n.';
    MSG_ERROR_EXISTE_EXIMICION_MEMORIA = 'El veh�culo tiene una eximici�n entre los datos modificados recientemente,'
        + CRLF + 'cuyas fechas se intercalan con las de la bonificaci�n.'
        + CRLF + 'No se permite Bonificar la Facturaci�n.';
    MSG_PORCENTAJE_DEBE_SER_MAYOR_A_CERO_Y_MENOR_IGUAL_A_100 = 'El valor del porcentaje debe ser un valor mayor a cero y menor o igual a 100.';
    MSG_IMPORTE_DEBE_SER_MAYOR_A_CERO = 'El valor del importe debe ser un valor mayor a cero.';
begin
    (* Si el form NO se llam� para consultar, realizar el control de los
    datos. *)
    if FModo <> scNormal then begin
        (* Controlar los datos
        FechaInicio debe contener un valor.
        Si FechaFin se puede editar, debe ser posterior a la FechaInicio.
        Si el Tipo es Porcentaje, Porcentaje debe ser mayor a cero y menor o igual a 100.
        Si el Tipo es Importe, Importe debe ser mayor o igual a cero. *)
        if not (ValidateControls([txt_FechaInicio,
                txt_FechaFin,
                txt_Porcentaje,
                txt_Importe],
                [not (txt_FechaInicio.IsEmpty),
                (txt_FechaFin.IsEmpty or txt_FechaFin.ReadOnly) or (txt_FechaFin.Date > txt_FechaInicio.Date),
                (not rb_Porcentaje.Checked) or ((txt_Porcentaje.Value > 0) and (txt_Porcentaje.Value <= 100)),
                (not rb_Importe.Checked) or (txt_Importe.Value > 0)],
                Caption,
                [MSG_FECHA_DEBE_CONTENER_UN_VALOR,
                MSG_FECHAFIN_DEBE_SER_POSTERIOR_A_FECHAINICIO,
                MSG_PORCENTAJE_DEBE_SER_MAYOR_A_CERO_Y_MENOR_IGUAL_A_100,
                MSG_IMPORTE_DEBE_SER_MAYOR_A_CERO])) then Exit;

        (* Si la FechaIncio es anterior a la FechaInicioOriginal, Controlar que
        no exista una bonificaci�n cuyo rango de fechas comparta parte o todo
        el rango de fechas de esta bonificaci�n.*)
        if (Trunc(txt_FechaInicio.Date) < FBonificarFacturacion.FechaInicioOriginal)
                and (ExisteBonificacion(Trunc(txt_FechaInicio.Date), Trunc(txt_FechaFin.Date))) then begin
            MsgBox(MSG_ERROR_EXISTE_BONIFICACION_BD, Caption, MB_ICONSTOP);
            Exit;
        end;

        (* Si la bonificaci�n NO exist�a en la BD, Controlar que no exista una
        bonificaci�n cuyo rango de fechas comparta parte o todo el rango de fechas
        de esta bonificaci�n.
        Si la bonificaci�n exist�a en la BD, es porque se est� haciendo una
        modificaci�n de los datos. *)
        if (not FBonificarFacturacion.ExisteEnBaseDeDatos) and (ExisteBonificacion(Trunc(txt_FechaInicio.Date), Trunc(txt_FechaFin.Date))) then begin
            MsgBox(MSG_ERROR_EXISTE_BONIFICACION_BD, Caption, MB_ICONSTOP);
            Exit;
        end;

        (* Controlar que no exista una eximici�n cuyo rango de fechas
        comparta parte o todo el rango de fechas de la bonificaci�n. *)
        if ExisteEximicion(Trunc(txt_FechaInicio.Date), Trunc(txt_FechaFin.Date)) then begin
            MsgBox(MSG_ERROR_EXISTE_EXIMICION_BD, Caption, MB_ICONSTOP);
            Exit;
        end;
        if SolapaConEximicion then begin
            MsgBox(MSG_ERROR_EXISTE_EXIMICION_MEMORIA, Caption, MB_ICONSTOP);
            Exit;
        end;

        (* Actualizar los datos de bonificaci�n de facturaci�n. *)
        with FBonificarFacturacion do begin
            FechaInicio := Trunc(txt_FechaInicio.Date);
            FechaFinalizacion := Trunc(txt_FechaFin.Date);
            if rb_Porcentaje.Checked then begin
                Porcentaje := txt_Porcentaje.Value;
                Importe := 0;
            end else begin
                Importe := txt_Importe.ValueInt;
                Porcentaje := 0;
            end;
        end; // with
    end;
    ModalResult := mrOk;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormBonificacionFacturacionDeCuenta.ExisteEximicion
  Author:    ggomez
  Date:      06-May-2005
  Arguments: FechaInicio, FechaFin: TDate
  Result:    Boolean
  Description: Retorna True si existe una eximici�n para el vehiculo cuyo
    rango de fechas comparta parte o todo el rango de fechas pasado como
    par�metro.
-----------------------------------------------------------------------------}
function TFormBonificacionFacturacionDeCuenta.ExisteEximicion(FechaInicio,
  FechaFin: TDate): Boolean;
var
    sp_ExisteEximicion: TADOStoredProc;
begin
    sp_ExisteEximicion := TADOStoredProc.Create(Self);
    try
        with sp_ExisteEximicion, Parameters do begin
            Connection := DMConnections.BaseCAC;
            ProcedureName := 'ExisteEximicion';

            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@IndiceVehiculo').Value := FIndiceVehiculo;
            ParamByName('@FechaInicio').Value := FechaInicio;
            if FechaFin < 0 then begin
                ParamByName('@FechaFinalizacion').Value := Null;
            end else begin
                ParamByName('@FechaFinalizacion').Value := FechaFin;
            end;
            ExecProc;
            Result := (ParamByName('@RETURN_VALUE').Value = 1);
        end; // with
    finally
        sp_ExisteEximicion.Free;
    end; // finally
end;


{-----------------------------------------------------------------------------
  Procedure: TFormBonificacionFacturacionDeCuenta.SolapaConEximicion
  Author:    ggomez
  Date:      06-May-2005
  Arguments: None
  Result:    Boolean
  Description: Retorna True si las fechas de la eximici�n almacenada en
    memoria comparte parte o todo el rango de fechas de la bonificaci�n.
-----------------------------------------------------------------------------}
function TFormBonificacionFacturacionDeCuenta.SolapaConEximicion: Boolean;
begin
    Result := False;

    (* Si NO hay datos de eximici�n, NO testear pues no solapan. *)
    if (FEximirFacturacion.FechaInicio < 0) then begin
        Exit;
    end; // if

    if txt_FechaFin.Date < 0 then begin
        (* NO hay fecha de Fin de Bonificaci�n. *)

        if (FEximirFacturacion.FechaFinalizacion < 0) then begin
            (* NO hay Fecha de Fin de Eximici�n. S�lo testar por fecha de
            Inicio, luego, Solapan seguro. *)

            Result := True;
        end else begin
            (* HAY Fecha de Fin de Eximici�n. *)

            Result := (txt_FechaInicio.Date <= FEximirFacturacion.FechaFinalizacion);
        end; // else if (FEximirFacturacion.FechaFinalizacion < 0)
    end else begin
        (* HAY fecha de Fin de Bonificaci�n. *)

        if (FEximirFacturacion.FechaFinalizacion < 0) then begin
            (* NO hay Fecha de Fin de Eximici�n. *)

            Result := (FEximirFacturacion.FechaInicio <= txt_FechaFin.Date);

        end else begin
            (* HAY Fecha de Fin de Eximici�n. *)

            Result := ((txt_FechaInicio.Date >= FEximirFacturacion.FechaInicio) and (txt_FechaInicio.Date <= FEximirFacturacion.FechaFinalizacion))
                        or ((txt_FechaFin.Date >= FEximirFacturacion.FechaInicio) and (txt_FechaFin.Date <= FEximirFacturacion.FechaFinalizacion))
                        or ((FEximirFacturacion.FechaInicio >= txt_FechaInicio.Date) and (FEximirFacturacion.FechaInicio <= txt_FechaFin.Date))
                        or ((FEximirFacturacion.FechaFinalizacion >= txt_FechaInicio.Date) and (FEximirFacturacion.FechaInicio <= txt_FechaFin.Date));

        end; // else if (FEximirFacturacion.FechaFinalizacion < 0)

    end; // else if txt_FechaFin.Date < 0
end;

procedure TFormBonificacionFacturacionDeCuenta.rb_PorcentajeClick(
  Sender: TObject);
begin
    txt_Porcentaje.Visible := rb_Porcentaje.Checked;
    txt_Importe.Visible := not rb_Porcentaje.Checked;
end;

procedure TFormBonificacionFacturacionDeCuenta.rb_ImporteClick(
  Sender: TObject);
begin
    txt_Importe.Visible := rb_Importe.Checked;
    txt_Porcentaje.Visible := not rb_Importe.Checked;
end;

function TFormBonificacionFacturacionDeCuenta.ExisteBonificacion(
  FechaInicio, FechaFin: TDate): Boolean;
var
    sp_ExisteBonificacion: TADOStoredProc;
begin
    sp_ExisteBonificacion := TADOStoredProc.Create(Self);
    try
        with sp_ExisteBonificacion, Parameters do begin
            Connection := DMConnections.BaseCAC;
            ProcedureName := 'ExisteBonificacion';
            Parameters.Refresh;

            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@IndiceVehiculo').Value := FIndiceVehiculo;
            ParamByName('@FechaInicio').Value := FechaInicio;
            if FechaFin < 0 then begin
                ParamByName('@FechaFinalizacion').Value := Null;
            end else begin
                ParamByName('@FechaFinalizacion').Value := FechaFin;
            end;

            ExecProc;

            Result := (ParamByName('@RETURN_VALUE').Value = 1);
        end; // with
    finally
        sp_ExisteBonificacion.Free;
    end; // finally
end;

end.
