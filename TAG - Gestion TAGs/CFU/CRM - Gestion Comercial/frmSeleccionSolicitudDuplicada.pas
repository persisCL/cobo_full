{-----------------------------------------------------------------------------
 File Name: frmSeleccionSolicitudDuplicada
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 24/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit frmSeleccionSolicitudDuplicada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, DPSControls,
  DMConnection, Peaprocs, Peatypes, Util, UtilDB, UtilProc, frmRegistrarLlamada, RStrings;

type
  TformSeleccionSolicitudDuplicada = class(TForm)
    GroupBox1: TGroupBox;
    PanelDeAbajo: TPanel;
    dbl_Solicitud: TDBListEx;
    ObtenerSolicitudContacto: TADOStoredProc;
    DS_SolicitudContacto: TDataSource;
    Pnl_LabelDescripcion: TPanel;
    lblDescripcionMensaje: TLabel;
    lblRutBuscado: TLabel;
    BtnCancelar: TButton;
    btnAceptar: TButton;
    btnNueva: TButton;
    btnRegistrarLlamada: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbl_SolicitudDblClick(Sender: TObject);
    procedure btnNuevaClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure ObtenerSolicitudContactoAfterScroll(DataSet: TDataSet);
    procedure btnRegistrarLlamadaClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoSolicitud: integer;
    function GetCodigoSolicitud: integer;
  public
    { Public declarations }
    property CodigoSolicitud: integer read GetCodigoSolicitud;
    function Inicializar(CodigoDocumento, NumeroDocumento: AnsiString; ShowBtnNueva: Boolean = True;  EsSolicitud: boolean = false): Boolean;
  end;

var
  formSeleccionSolicitudDuplicada: TformSeleccionSolicitudDuplicada;

implementation

{$R *.dfm}

procedure TformSeleccionSolicitudDuplicada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//    action := caFree;
end;

function TformSeleccionSolicitudDuplicada.GetCodigoSolicitud: integer;
begin
    Result := FCodigoSolicitud;
end;

function TformSeleccionSolicitudDuplicada.Inicializar(CodigoDocumento, NumeroDocumento: AnsiString; ShowBtnNueva: Boolean = True; EsSolicitud: boolean = false): Boolean;
resourcestring
    ATENCION = 'Atenci�n';
    MSG_OPENTABLES = 'Error al buscar las Solicitudes.';
var
    tituloMensaje, Descripcion, Comentario: AnsiString;
begin
    //* *//
    tituloMensaje := '';
    Descripcion := '';
    Comentario := '';
    ObtenerMensaje(DMConnections.BaseCAC, MENSAJE_SOLICITUD_RUT_EXISTENTE_CAC,
                    tituloMensaje, Descripcion, Comentario);
    self.Caption := iif(tituloMensaje<>'', tituloMensaje, ATENCION);
    lblRutBuscado.Caption := lblRutBuscado.Caption + NumeroDocumento;

    lblDescripcionMensaje.Caption := Descripcion;
    //lblComentarioMensaje.Caption := comentario;
    with ObtenerSolicitudContacto do begin
        close;
        Parameters.Refresh;
        Parameters.ParamByName('@RUT').Value := NumeroDocumento;
        try
            Open;
        except
            on E: Exception do begin
              MsgBoxErr(MSG_OPENTABLES,e.Message,self.Caption,MB_ICONSTOP);
              result := false;
              exit;
            end;
        end;
    end;
    btnNueva.visible := ShowBtnNueva;
    btnRegistrarLlamada.Visible := EsSolicitud;
    result := true;
end;

procedure TformSeleccionSolicitudDuplicada.dbl_SolicitudDblClick(
  Sender: TObject);
begin

    if not (ObtenerSolicitudContacto.IsEmpty) then begin
        FCodigoSolicitud := ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsInteger;
        ModalResult := mrOk;
    end;

end;

procedure TformSeleccionSolicitudDuplicada.btnNuevaClick(Sender: TObject);
begin
    FCodigoSolicitud := 0;
    ModalResult := mrOk;
end;

procedure TformSeleccionSolicitudDuplicada.BtnCancelarClick(
  Sender: TObject);
begin
    FCodigoSolicitud := 0;
    ModalResult := mrCancel;
end;

procedure TformSeleccionSolicitudDuplicada.btnAceptarClick(
  Sender: TObject);
begin
    if not (ObtenerSolicitudContacto.IsEmpty) then begin
        FCodigoSolicitud := ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsInteger;
        ModalResult := mrOk;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerSolicitudContactoAfterScroll
  Author:
  Date Created:  /  /
  Description:
  Parameters: DataSet: TDataSet
  Return Value: N/A

  Revision : 1
    Date: 24/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TformSeleccionSolicitudDuplicada.ObtenerSolicitudContactoAfterScroll(
  DataSet: TDataSet);
var
    CodigoEstadoSolicitud:String;
    PuedeEditar:Boolean;
begin

    CodigoEstadoSolicitud:=Trim(ObtenerSolicitudContacto.FieldByName('CodigoEstadoSolicitud').AsString);
    PuedeEditar:=QueryGetValueInt(DMConnections.BaseCAC,'SELECT PermiteEdicion*10 FROM EstadosSolicitudContacto  WITH (NOLOCK) WHERE CodigoEstadoSolicitud =0'+CodigoEstadoSolicitud)>0;

//        btnNueva.Enabled
        PuedeEditar:= not(ObtenerSolicitudContacto.IsEmpty) and
                        ( PuedeEditar or (CodigoEstadoSolicitud='') );

        if PuedeEditar then btnAceptar.Caption:='&Modificar'
        else btnAceptar.Caption:='&Consultar';

        btnRegistrarLlamada.Enabled := (ObtenerSolicitudContacto.FieldByName('CodigoEstadoSolicitud').AsInteger <> ESTADO_SOLICITUD_ANULADA) and
                                       (ObtenerSolicitudContacto.FieldByName('CodigoEstadoSolicitud').AsInteger <> ESTADO_SOLICITUD_CONVENIO_FIRMADO);

end;

procedure TformSeleccionSolicitudDuplicada.btnRegistrarLlamadaClick(
  Sender: TObject);
var
    f: TFormRegistrarLlamada;
begin
    Application.CreateForm(TFormRegistrarLlamada, f);
    f.Inicializar(ESTADO_SOLICITUD_CONTACTO_TELEFONICO, ObtenerSolicitudContacto.FieldByName('CodigoEstadoResultado').AsInteger);
    if (f.ShowModal = mrOk) then begin
        try
            QueryExecute(DMConnections.BaseCAC, 'UPDATE SolicitudContacto SET CodigoEstadoSolicitud = ' + Trim(inttostr(ESTADO_SOLICITUD_CONTACTO_TELEFONICO)) +
                ',CodigoEstadoResultado = ' + inttostr(f.CodigoEstadoResultado) +
                ' WHERE CodigoSolicitudContacto = ' + Trim(ObtenerSolicitudContacto.FieldByName('CodigoSolicitudContacto').AsString));
           BtnCancelar.Click;
        except
            on E: exception do begin
                MsgBoxErr(MSG_ERROR_ACTUALIZAR_RESULTADO, e.message, self.Caption, MB_ICONSTOP);
               ModalResult := mrCancel;
               Exit;
            end;
        end;
    end;
    if f.ModalResult = mrOk then
        ModalResult := mrYes
    else
        ModalResult := mrCancel;
    f.Release;
end;

end.
