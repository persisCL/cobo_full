{-----------------------------------------------------------------------------
 Unit Name: frmMedioEnvioDocumentosCobro
 Author:
 Purpose:
 History:

Revision 1:
 Author: ggomez
 Date: 12/05/2005
 Description: Elimin� unos ChexkBox que no tenian sentido y habilit� el de
    Detalle Impreso de Facturaci�n.

Revision 2:
 Author: nefernandez
 Date: 16/07/2008
 Description: Se redefinen las opciones de env�o. A partir de ahora se usa RadioButton
 para poder seleccionar solo una opcion.

Revision 3:
 Author: nefernandez
 Date: 26/07/2008
 Description: Se agrega el radiobutton "Ninguno", que es la opcion por defecto (indica que no
 hay Medios de Envio de Documentos). Para el caso de existencia de Medios, seleccionando esta
 opcion y luego al grabar el convenio se borraran los medios existentes.
-----------------------------------------------------------------------------}

unit frmMedioEnvioDocumentosCobro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, RStrings, Peatypes,UtilProc, utilDB,
  ConstParametrosGenerales, ExtCtrls;

type

  TMedioEnvioDocumentosCobro = array of TRegistroMedioEnvioDocumentosCobro;

type
  TformMedioEnvioDocumentosCobro = class(TForm)
    gb_Datos: TGroupBox;
    rbDetalleImpresoConNotaCobro: TRadioButton;
    pnl_ConvenioNoTieneEmail: TPanel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    rbNotaCobroYDetalleEmail: TRadioButton;
    rbNotaCobroEmail: TRadioButton;
    rbDetalleEmail: TRadioButton;
    rbNinguno: TRadioButton;
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    FModo: TStateConvenio;
    FTieneEmail: Boolean;
    FCantidadVehiculosConvenio: Integer;
    FCantidadMaximaVehiculosFacturacionDetallada: Integer;

    function GetRecMedioEnvioDoc: TMedioEnvioDocumentosCobro;

    procedure Habilitar(Control: TWinControl);

    procedure HabilitarComponentes;

  public
    { Public declarations }

    property RegistroMedioEvioDoc: TMedioEnvioDocumentosCobro read GetRecMedioEnvioDoc;

    function Inicializar(Datos: TMedioEnvioDocumentosCobro;
        TieneEmail: Boolean;
        CantidadVehiculosConvenio: Integer;
        Modo: TStateConvenio = scAlta): Boolean;

    class procedure LimpiarRegistro(var Registro: TMedioEnvioDocumentosCobro);
    class function EnvioDocumentoCobroViaMail(Registro: TMedioEnvioDocumentosCobro; SoloSocumentodeCobro: Boolean = False): Boolean;

  end;

var
  formMedioEnvioDocumentosCobro: TformMedioEnvioDocumentosCobro;

implementation

uses DMConnection;

{$R *.dfm}

{ TfrmMedioEnvioDocumentosCobro }


procedure TformMedioEnvioDocumentosCobro.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_DETALLE_TRANSITO = 'Al marcar "�Desea disponer del Detalle de Tr�nsitos?" se debe marcar una de las opciones del Medio de Env�o correspondiente';
begin
    ModalResult := mrOk;
end;

class function TformMedioEnvioDocumentosCobro.EnvioDocumentoCobroViaMail(
  Registro: TMedioEnvioDocumentosCobro;SoloSocumentodeCobro:Boolean=False): Boolean;
var
    i: Integer;
begin
    Result := False;
    for i := 0 to Length(registro) - 1 do begin
        if {(Registro[i].CodigoMedioEnvioDocumento=CodDocumentoCobro) and}
                (Registro[i].CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO) and
        	    ( not(SoloSocumentodeCobro) or (Registro[i].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DOCUMENTACION_COBRO)) then begin
            Result := True;
        end; //if
    end; // for
end;

function TformMedioEnvioDocumentosCobro.GetRecMedioEnvioDoc: TMedioEnvioDocumentosCobro;
var
    aux: TMedioEnvioDocumentosCobro;
begin
    SetLength(aux, 0);
    // Se retorna el documento y tipo de acuerdo a la opcion seleccionada
    if Self.rbNotaCobroYDetalleEmail.Checked then begin
        SetLength(aux, Length(aux) + 1);
        aux[Length(aux) - 1].CodigoMedioEnvioDocumento  := CONST_MEDIO_ENVIO_DOCUMENTACION_COBRO;
        aux[Length(aux) - 1].CodigoTipoMedioEnvio       := CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO;

        SetLength(aux, Length(aux) + 1);
        aux[Length(aux) - 1].CodigoMedioEnvioDocumento  := CONST_MEDIO_ENVIO_DETALLE_FACTURACION;
        aux[Length(aux) - 1].CodigoTipoMedioEnvio       := CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO;
    end;

    if Self.rbNotaCobroEmail.Checked then begin
        SetLength(aux, Length(aux) + 1);
        aux[Length(aux) - 1].CodigoMedioEnvioDocumento  := CONST_MEDIO_ENVIO_DOCUMENTACION_COBRO;
        aux[Length(aux) - 1].CodigoTipoMedioEnvio       := CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO;
    end;

    if Self.rbDetalleEmail.Checked then begin
        SetLength(aux, Length(aux) + 1);
        aux[Length(aux) - 1].CodigoMedioEnvioDocumento  := CONST_MEDIO_ENVIO_DETALLE_FACTURACION;
        aux[Length(aux) - 1].CodigoTipoMedioEnvio       := CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO;
    end;

    if Self.rbDetalleImpresoConNotaCobro.Checked then begin
        SetLength(aux, Length(aux) + 1);
        aux[Length(aux) - 1].CodigoMedioEnvioDocumento  := CONST_MEDIO_ENVIO_DETALLE_FACTURACION;
        aux[Length(aux) - 1].CodigoTipoMedioEnvio       := CONST_TIPO_MEDIO_ENVIO_CORREO_POSTAL;
    end;

    Result := aux;
end;

procedure TformMedioEnvioDocumentosCobro.Habilitar(Control: TWinControl);
begin
    if Control <> nil then Begin
        Control.Enabled := True;
        Habilitar(Control.Parent);
    end;
end;

function TformMedioEnvioDocumentosCobro.Inicializar(Datos: TMedioEnvioDocumentosCobro;
    TieneEmail: Boolean;
    CantidadVehiculosConvenio: Integer;
    Modo: TStateConvenio = scAlta): Boolean;
var
    i: Integer;
    TipoMedioEnvioNotaCobro: Integer;
    TipoMedioEnvioDetalle: Integer;
begin
    FModo := Modo;

    FTieneEmail := TieneEmail;

    FCantidadVehiculosConvenio := CantidadVehiculosConvenio;

    ObtenerParametroGeneral(DMConnections.BaseCAC, 'TOPE_VEHICULOS_FACT_DETALLADA', FCantidadMaximaVehiculosFacturacionDetallada);

    Self.Caption := MSG_CAPTION_MEDIO_ENVIO_DOCUMENTOS_COBRO;

    TipoMedioEnvioNotaCobro := 0;
    TipoMedioEnvioDetalle := 0;
    for i := 0 to Length(Datos) - 1 do begin
        //Se determina el Tipo Medio Envio para el documento de Nota Cobro
        if (Datos[i].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DOCUMENTACION_COBRO) then begin
            TipoMedioEnvioNotaCobro := Datos[i].CodigoTipoMedioEnvio;
        end;

        //Se determina el Tipo Medio Envio para el documento de Detalle Transacciones
        if (Datos[i].CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DETALLE_FACTURACION) then begin
            TipoMedioEnvioDetalle := Datos[i].CodigoTipoMedioEnvio;
        end;
    end; // for

    if FModo = scNormal then begin
        EnableControlsInContainer(Self, False);
        Habilitar(BtnCancelar);
    end else begin
        HabilitarComponentes;
    end;

    (* Si no tiene Email, Mostrar una leyenda que lo indique. *)
    if not FTieneEmail then begin
        gb_Datos.Top := pnl_ConvenioNoTieneEmail.Top + pnl_ConvenioNoTieneEmail.Height;
        pnl_ConvenioNoTieneEmail.Show;

    end else begin;
        pnl_ConvenioNoTieneEmail.Hide;
        gb_Datos.Top := pnl_ConvenioNoTieneEmail.Top;
        Self.Height := Self.Height - pnl_ConvenioNoTieneEmail.Height;
    end; // else if

    // De acuerdo a lo que se tiene actualmente en Envio Documentos, se setea el radio correspondiente
    rbNinguno.Checked := True; //Revision 3: Opcion por defecto (indica que no hay Medios Envio Documentos)
    if TipoMedioEnvioNotaCobro = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO then begin
        if TipoMedioEnvioDetalle = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO then begin
            rbNotaCobroYDetalleEmail.Checked := True;
        end else begin
            rbNotaCobroEmail.Checked := True;
        end;
    end else begin
        if TipoMedioEnvioDetalle = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO then begin
            rbDetalleEmail.Checked := True;
        end else begin
            if TipoMedioEnvioDetalle = CONST_TIPO_MEDIO_ENVIO_CORREO_POSTAL then begin
                rbDetalleImpresoConNotaCobro.Checked := True;
            end;
        end;
    end;

    Result := True;
end;

class procedure TformMedioEnvioDocumentosCobro.LimpiarRegistro(
  var Registro: TMedioEnvioDocumentosCobro);
begin
    SetLength(Registro, 0);
end;

procedure TformMedioEnvioDocumentosCobro.HabilitarComponentes;
begin
    // Si la cantidad de vehiculos del convenio supera la cantidad indicada en el par�metro
    // general "TOPE_VEHICULOS_FACT_DETALLADA", se deshabilitan las siguientes opciones
    rbNotaCobroYDetalleEmail.Enabled := (FCantidadVehiculosConvenio <= FCantidadMaximaVehiculosFacturacionDetallada);
    rbDetalleEmail.Enabled := (FCantidadVehiculosConvenio <= FCantidadMaximaVehiculosFacturacionDetallada);
end;

end.
