object frmSuspensionCuenta: TfrmSuspensionCuenta
  Left = 479
  Top = 221
  BorderStyle = bsDialog
  Caption = 'Suspensi'#243'n de Cuenta'
  ClientHeight = 168
  ClientWidth = 250
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 40
    Width = 136
    Height = 13
    Caption = 'Fecha Limite de Suspensi'#243'n:'
  end
  object ch_Notificado: TCheckBox
    Left = 5
    Top = 16
    Width = 165
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Cliente notificado'
    TabOrder = 0
  end
  object txt_FechaDeadline: TDateEdit
    Left = 157
    Top = 36
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 1
    Date = -693594.000000000000000000
  end
  object pnlAbajo: TPanel
    Left = 0
    Top = 127
    Width = 250
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 3
    DesignSize = (
      250
      41)
    object btn_Aceptar: TButton
      Left = 78
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TButton
      Left = 162
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Cancelar'
      Default = True
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
  end
  object gbDatosPatente: TGroupBox
    Left = 0
    Top = 62
    Width = 250
    Height = 65
    Align = alBottom
    Caption = 'Confirmar  Patente'
    TabOrder = 2
    object lblPatente: TLabel
      Left = 5
      Top = 31
      Width = 37
      Height = 13
      Caption = 'Patente'
    end
    object txtPatente: TEdit
      Left = 48
      Top = 28
      Width = 94
      Height = 21
      Hint = 'Patente'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 8
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnKeyDown = txtPatenteKeyDown
      OnKeyPress = txtPatenteKeyPress
    end
  end
end
