{-------------------------------------------------------------------------------
Firma       : SS-1006-NDR-20120803
Description : Poder excluir veh�culos antes de dar de alta las cuentas.

Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

--------------------------------------------------------------------------------}
unit frmConsultaAltaPArauco;

interface

uses
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaProcs,
  PeaTypes,
  BuscaClientes,
  //Otros
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, DbList, StdCtrls, Validate, DateEdit, ExtCtrls, ListBoxEx,
  DBListEx,  DmiCtrls, DBClient, ImgList, VariantComboBox, Grids, DBGrids,
  ComCtrls, RStrings, ImprimirWO,frmMuestraMensaje, ppCtrls, ppPrnabl, ppClass,
  ppBands, ppCache, ppDB, ppParameter, ppProd, ppReport, ppComm, ppRelatv,
  ppDBPipe, UtilRB, ppStrtch, ppSubRpt,
  frmMedioEnvioMail,
  frmReporteContratoAdhesionPA,
  ConstParametrosGenerales,
  PeaProcsCN, SysutilsCN,  ppMemo, ppVar, ppTypes,                              //SS-1006-NDR-20120803
  frmHabilitarCuentasVentanaEnrolamiento, Provider,                             //SS-1006-NDR-20120803
  RBSetup, PRinters;

type
  TFormConsultaAltaPArauco = class(TForm)
    dblInfracciones: TDBListEx;
    Panel1: TPanel;
    btnImprimirImpedimentos: TButton;
    btnImprimirContrato: TButton;
    btnSalir: TButton;
    ppdbImpedimentos: TppDBPipeline;
    ttprImpedimentos: TppReport;
    ppParameterList2: TppParameterList;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand3: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppImage2: TppImage;
    ppLabel6: TppLabel;
    ppLabel1: TppLabel;
    ppLabel20: TppLabel;
    RBInterface: TRBInterface;
    pnlFiltros: TPanel;
    grpFiltros: TGroupBox;
    Label1: TLabel;
    lblNombreCli: TLabel;
    lblPersoneria: TLabel;
    lblPatente: TLabel;
    btn_Limpiar: TButton;
    btn_Filtrar: TButton;
    peNumeroDocumento: TPickEdit;
    edtPatente: TEdit;
    fnObtenerCodigoPersonaPorCuentaActiva: TADOStoredProc;
    spValidarRestriccionesAdhesionPAK: TADOStoredProc;
    dsValidarRestriccionesAdhesionPA: TDataSource;
    lblRUT: TLabel;
    ppLine1: TppLine;
    ppLabel2: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    pplblRUT: TppLabel;
    pplblNombreCliente: TppLabel;
    ppDBMemDescripcion: TppDBMemo;
    ppLine2: TppLine;
    btnExclusionVehiculos: TButton;														//SS-1006-NDR-20120803
    spObtenerCuentasPersona: TADOStoredProc;											//SS-1006-NDR-20120803
    dspCuentasPersona: TDataSetProvider;												//SS-1006-NDR-20120803
    cdsCuentasPersona: TClientDataSet;													//SS-1006-NDR-20120803
    cdsCuentasPersonaPatente: TStringField;												//SS-1006-NDR-20120803
    cdsCuentasPersonaMarcaModeloColor: TStringField;									//SS-1006-NDR-20120803
    cdsCuentasPersonaConvenio: TStringField;											//SS-1006-NDR-20120803
    cdsCuentasPersonaCodigoEstadoCuenta: TSmallintField;								//SS-1006-NDR-20120803
    cdsCuentasPersonaAdheridoPA: TBooleanField;											//SS-1006-NDR-20120803
    cdsCuentasPersonaChequeado: TBooleanField;											//SS-1006-NDR-20120803
    cdsCuentasPersonaCodigoConvenio: TIntegerField;										//SS-1006-NDR-20120803
    cdsCuentasPersonaIndiceVehiculo: TIntegerField;										//SS-1006-NDR-20120803
    procedure BtnSalirClick(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnImprimirImpedimentosClick(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure peNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure edtPatenteChange(Sender: TObject);
    procedure dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure ppDBMemDescripcionPrint(Sender: TObject);
    procedure ttprImpedimentosBeforePrint(Sender: TObject);
    procedure btnImprimirContratoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnExclusionVehiculosClick(Sender: TObject);
    procedure cdsCuentasPersonaAfterOpen(DataSet: TDataSet);
    procedure cdsCuentasPersonaAfterPost(DataSet: TDataSet);								//SS-1006-NDR-20120803
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    FCodigoPersona,
    FCodigoAuditoria: Integer;
    FPersoneria,
    FRUTPersona: string;
    FLimpiando,
    FSoloConsulta: Boolean;
    FFechaHoy: TDate;
    procedure MostrarDatosCliente(CodigoCliente: Integer; RUT, Personeria: string);
    procedure BuscarCliente;
    procedure Limpiar(ObjetoALimpiar: TObject);
    function ValidarFiltros: Boolean;
    function ValidarCliente: Boolean;
    procedure ValidarRestriccionesAdhesionPA;
    procedure ActualizaEMailPersona(pEMail: string; pCodigoPersona: Integer);
    procedure ActualizarConveniosPersonaEnvioPorEMail(pCodigoPersona: Integer);
    procedure AdherirClienteAraucoTAG(pCodigoPersona: Integer; pEMail: string);
    function AgregarAuditoria_AdhesionPAK(pNumeroDocumento, pPatente, pEMail: string; pCodigoPersona, pCumpleRequisitos, pAdheridoEnvioEMail, pImpresionContrato, pFirmaContrato, pCodigoAuditoria: Integer): Integer;
    procedure AdherirPorCuenta();														//SS-1006-NDR-20120803
  public
    { Public declarations }
    constructor ModalCreate(AOwner: TComponent);
    function Inicializar(): Boolean; overload;
    function Inicializar(pNumeroDocumento: string): boolean; overload;
  end;


var
  FormConsultaAltaPArauco: TFormConsultaAltaPArauco;
  FormConsultaAltaPAraucoSoloConsulta: TFormConsultaAltaPArauco;

  
implementation

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}

constructor TFormConsultaAltaPArauco.ModalCreate(AOwner: TComponent);
begin
	inherited Create(AOwner);

    FormStyle   := fsNormal;
    BorderIcons := [biSystemMenu];
end;

function TFormConsultaAltaPArauco.Inicializar(): Boolean;
begin
    try
    	Result := False;

        FSoloConsulta    := False;
        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;
        FLimpiando       := False;

        lblRUT.Caption        := EmptyStr;
        lblNombreCli.Caption  := EmptyStr;
        lblPersoneria.Caption := EmptyStr;

        FFechaHoy := SysutilsCN.NowBaseCN(DMConnections.BaseCAC);

        Result     := True;
    except
    	on e: Exception do begin
        	ShowMsgBoxCN(e, Self);
        end;
    end;
end;


function TFormConsultaAltaPArauco.Inicializar(pNumeroDocumento: string): Boolean;
begin
    try
    	Result := False;

        FSoloConsulta    := True;
        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;
        FLimpiando       := True;

        pnlFiltros.Visible              := False;
        btnImprimirImpedimentos.Visible := False;
        btnImprimirContrato.Visible     := False;
        btnExclusionVehiculos.Visible   := False;                               //SS-1006-NDR-20120803

        peNumeroDocumento.Text := pNumeroDocumento;
        btn_FiltrarClick(nil);

        Caption := Caption + ' - [ ' + lblNombreCli.Caption + lblPersoneria.Caption + ' ]';

        FLimpiando       := False;

        Result := True;
    except
    	on e: Exception do begin
        	ShowMsgBoxCN(e, Self);
        end;
    end;
end;

//BEGIN : SS-1006-NDR-20120803-------------------------------------------------
procedure TFormConsultaAltaPArauco.btnExclusionVehiculosClick(Sender: TObject);
var
    f: TFormHabilitarCuentasVentanaEnrolamiento;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        try

            if not cdsCuentasPersona.Active then
            begin
                SPObtenerCuentasPersona.Parameters.Refresh;
                SPObtenerCuentasPersona.Parameters.ParamByName('@CodigoPersona').Value:=FCodigoPersona;
                cdsCuentasPersona.Open;
            end;

            f := TFormHabilitarCuentasVentanaEnrolamiento.Create(nil);
            if f.Inicializar(cdsCuentasPersona) then
            begin
                f.ShowModal;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(f) then FreeAndNil(f);
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;
//END : SS-1006-NDR-20120803-------------------------------------------------

procedure TFormConsultaAltaPArauco.btnImprimirContratoClick(Sender: TObject);
	resourcestring
    	rsTituloActivacion  	 = 'Activaci�n Servicio Arauco TAG';
        rsMensajeActivacion 	 = '� Firm� el Cliente todos los documentos del contrato del Servicio Arauco TAG ?';
        rsMensajeActivado   	 = 'El Servicio Arauco TAG, se activ� correctamente para el Cliente.';
    	rsTituloImpresion   	 = 'Impresi�n Contrato Servicio Arauco TAG';
        rsMensajeImpresion  	 = '� Se imprimi� el contrato del Servicio Arauco TAG correctamente ?';
    	rsTituloImpresionAnexo   = 'Impresi�n Anexo Contrato Servicio Arauco TAG';
    	rsAvisoImpresionAnexo    = 'La impresi�n del contrato Servicio Arauco TAG, lleva Anexo.';
        rsMensajeImpresionAnexo  = '� Se imprimi� el Anexo del Contrato del Servicio Arauco TAG correctamente ?';
	var
        FormMedioEnvioMail: TFormMedioEnvioMail;
        EMailPersona: string;
        ClienteAdheridoEnvioEMail: Boolean;
        ReporteContratoAdhesionPAForm: TReporteContratoAdhesionPAForm;
        RBInterfaceConfig: TRBConfig;
        spValidarRestriccionesAdhesionPAKAnexo: TADOStoredProc;			// SS_1006_PDO_20121122
        SituacionesARegularizar: AnsiString;							// SS_1006_PDO_20121122
        DocumentoImpreso: Boolean;
        RutaLogo: AnsiString;                                                   //SS_1147_NDR_20140710

    const
    	  cSQLObtenerEmailPersona                  = 'SELECT dbo.ObtenerEmailPersona(%d)';
        cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
          ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
          try                                                                                                 //SS_1147_NDR_20140710
            ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
          except                                                                                              //SS_1147_NDR_20140710
            On E: Exception do begin                                                                          //SS_1147_NDR_20140710
              Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
              MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
              Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
            end;                                                                                              //SS_1147_NDR_20140710
          end;                                                                                                //SS_1147_NDR_20140710

        	// SS_1006_PDO_20121122 Inicio Bloque
          SituacionesARegularizar := EmptyStr;

        	spValidarRestriccionesAdhesionPAKAnexo := TADOStoredProc.Create(nil);
            with spValidarRestriccionesAdhesionPAKAnexo do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'ValidarRestriccionesAdhesionPAKAnexo';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
                Parameters.ParamByName('@DevolverValidaciones').Value := 1;
                Open;

                if RecordCount > 0 then begin
                	while not Eof do begin

                        if SituacionesARegularizar <> EmptyStr then begin
                        	SituacionesARegularizar := SituacionesARegularizar + '\par \par ';
                        end;

                        SituacionesARegularizar := SituacionesARegularizar + ' \tab ' + '- ' + AnsiUpperCase(FieldByName('Descripcion').AsString);

                        Next;
                    end;
                end;
            end;
            // SS_1006_PDO_20121122 Fin Bloque

            EMailPersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerEmailPersona, [FCodigoPersona]));
            ClienteAdheridoEnvioEMail := QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [FCodigoPersona]));

	        FormMedioEnvioMail := TFormMedioEnvioMail.Create(nil);

            with FormMedioEnvioMail do begin
            	Inicializar(EMailPersona, ClienteAdheridoEnvioEMail);

	        	if ShowModal = mrOk then begin
	                if EMailPersona <> Trim(txtEmail.Text) then begin
                    	ActualizaEMailPersona(Trim(txtEmail.Text), FCodigoPersona);
                        EMailPersona := Trim(txtEmail.Text);
                    end;
                    if (not ClienteAdheridoEnvioEMail) and chkNotaCobroPorEMail.Checked then begin
                    	ActualizarConveniosPersonaEnvioPorEMail(FCodigoPersona);
                        ClienteAdheridoEnvioEMail := True;
                    end;

            		AgregarAuditoria_AdhesionPAK(
                    	Trim(peNumeroDocumento.Text),
                        Trim(edtPatente.Text),
                        EMailPersona,
                        FCodigoPersona,
                        1,
                        IIf(ClienteAdheridoEnvioEMail, 1, 0),
                        -1,
                        -1,
                        FCodigoAuditoria);

            	end;
            end;

            if SituacionesARegularizar <> EmptyStr then begin
                ShowMsgBoxCN(rsTituloImpresion, rsAvisoImpresionAnexo, MB_ICONINFORMATION, Self);
            end;


            ReporteContratoAdhesionPAForm := TReporteContratoAdhesionPAForm.Create(nil);

            with ReporteContratoAdhesionPAForm do  begin
                Inicializar(
                	lblNombreCli.Caption,
                    lblNombreCli.Caption,
                    peNumeroDocumento.Text,
                    EMailPersona,
                    FFechaHoy,
                    ClienteAdheridoEnvioEMail,
                    True,
                    False,							// SS_1006_PDO_20121122
                    SituacionesARegularizar);		// SS_1006_PDO_20121122

                RBInterfaceConfig := RBInterface.GetConfig;

                with RBInterfaceConfig do begin
	                Copies       := 2;
                    MarginTop    := 0;
                    MarginLeft   := 0;
                    MarginRight  := 0;
                    MarginBottom := 0;
                    Orientation  := poPortrait;
	                {$IFDEF DESARROLLO or TESTKTC}
                    	PaperName    := 'Letter';
                    {$ELSE}
                    	PaperName    := 'Carta';
                    {$ENDIF}
                    ShowUser     := False;
                    ShowDateTime := False;
                end;

                RBInterface.SetConfig(RBInterfaceConfig);

                ReporteContratoAdhesionPAForm.ConfigurarImpresionContrato(True, False);

                DocumentoImpreso := RBInterface.Execute(True);

                if DocumentoImpreso then begin

            		AgregarAuditoria_AdhesionPAK(
                    	Trim(peNumeroDocumento.Text),
                        Trim(edtPatente.Text),
                        EmptyStr,
                        FCodigoPersona,
                        1,
                        -1,
                        1,
                        -1,
                        FCodigoAuditoria);

                    while DocumentoImpreso and (ShowMsgBoxCN(rsTituloImpresion, rsMensajeImpresion, MB_ICONQUESTION + MB_YESNO, Self) <> mrOk) do begin
	                   DocumentoImpreso := RBInterface.Execute(True);
                    end;

                    if DocumentoImpreso then begin

                        if SituacionesARegularizar <> EmptyStr then begin
                            ReporteContratoAdhesionPAForm.ConfigurarImpresionContrato(False, True);
                            DocumentoImpreso := RBInterface.Execute(True);

                            while DocumentoImpreso and (ShowMsgBoxCN(rsTituloImpresionAnexo, rsMensajeImpresionAnexo, MB_ICONQUESTION + MB_YESNO, Self) <> mrOk) do begin
                               DocumentoImpreso := RBInterface.Execute(True);
                            end;
                        end;

                        if DocumentoImpreso then begin

                            if ShowMsgBoxCN(rsTituloActivacion, rsMensajeActivacion, MB_ICONQUESTION + MB_YESNO, Self) = mrOk then begin

                                AdherirClienteAraucoTAG(FCodigoPersona, EMailPersona);

                                AgregarAuditoria_AdhesionPAK(
                                    Trim(peNumeroDocumento.Text),
                                    Trim(edtPatente.Text),
                                    EmptyStr,
                                    FCodigoPersona,
                                    1,
                                    -1,
                                    1,
                                    1,
                                    FCodigoAuditoria);

                                ShowMsgBoxCN(rsTituloActivacion, rsMensajeActivado, MB_ICONINFORMATION, Self);
                                Limpiar(nil);
                            end
                            else begin
                                AgregarAuditoria_AdhesionPAK(
                                    Trim(peNumeroDocumento.Text),
                                    Trim(edtPatente.Text),
                                    EmptyStr,
                                    FCodigoPersona,
                                    1,
                                    -1,
                                    1,
                                    0,
                                    FCodigoAuditoria);
                            end;
                        end;
                    end;
                end;

                if not DocumentoImpreso then begin
            		AgregarAuditoria_AdhesionPAK(
                    	Trim(peNumeroDocumento.Text),
                        Trim(edtPatente.Text),
                        EmptyStr,
                        FCodigoPersona,
                        1,
                        -1,
                        0,
                        -1,
                        FCodigoAuditoria);
                end;
            end;
        except
        	on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	if Assigned(FormMedioEnvioMail) then FreeAndNil(FormMedioEnvioMail);
    	if Assigned(ReporteContratoAdhesionPAForm) then FreeAndNil(ReporteContratoAdhesionPAForm);
        // SS_1006_PDO_20121122 Inicio Bloque
        if Assigned(spValidarRestriccionesAdhesionPAKAnexo) then begin
        	if spValidarRestriccionesAdhesionPAKAnexo.Active then spValidarRestriccionesAdhesionPAKAnexo.Close;
            FreeAndNil(spValidarRestriccionesAdhesionPAKAnexo);
        end;
        // SS_1006_PDO_20121122 Fin BLoque        
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaPArauco.btnImprimirImpedimentosClick(Sender: TObject);
begin
	RBInterface.Execute(True);
end;

procedure TFormConsultaAltaPArauco.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormConsultaAltaPArauco.btn_FiltrarClick(Sender: TObject);
begin
    if ValidarFiltros then begin
        if FCodigoPersona <= 0 then begin
        	BuscarCliente;
        end;

        if FCodigoPersona > 0 then begin
            if ValidarCliente then begin
            	ValidarRestriccionesAdhesionPA;
            end;
        end;
    end;
end;

procedure TFormConsultaAltaPArauco.edtPatenteChange(Sender: TObject);
begin
	if not FLimpiando then Limpiar(peNumeroDocumento);
end;

procedure TFormConsultaAltaPArauco.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:= caFree;
end;

procedure TFormConsultaAltaPArauco.FormCreate(Sender: TObject);
begin
    FFormState := FFormState - [fsVisible];
end;

procedure TFormConsultaAltaPArauco.FormDestroy(Sender: TObject);
begin
	FormConsultaAltaPArauco := nil;
end;

procedure TFormConsultaAltaPArauco.Limpiar(ObjetoALimpiar: TObject);
begin
	try
    	FLimpiando := True;

        FCodigoPersona   := -1;
        FCodigoAuditoria := -1;
        FPersoneria      := EmptyStr;
        FRUTPersona      := EmptyStr;

        lblRUT.Caption        := EmptyStr;
        lblNombreCli.Caption  := EmptyStr;
        lblPersoneria.Caption := EmptyStr;

        if not Assigned(ObjetoALimpiar) then begin
            peNumeroDocumento.Text := EmptyStr;
            edtPatente.Text        := EmptyStr;

            peNumeroDocumento.SetFocus;
        end
        else begin
            (ObjetoALimpiar as TCustomEdit).Text := EmptyStr;
        end;

        btnImprimirImpedimentos.Enabled := False;
        btnImprimirContrato.Enabled     := False;
        btnExclusionVehiculos.Enabled   := False;                               //SS-1006-NDR-20120803

        if spValidarRestriccionesAdhesionPAK.Active then spValidarRestriccionesAdhesionPAK.Close;
        if cdsCuentasPersona.Active then cdsCuentasPersona.Close;				//SS-1006-NDR-20120803
    finally
    	FLimpiando := False;
    end;
end;


procedure TFormConsultaAltaPArauco.peNumeroDocumentoButtonClick(Sender: TObject);
    var
        formBuscaClientes: TFormBuscaClientes;
begin
    try
        formBuscaClientes := TFormBuscaClientes.Create(nil);

        if formBuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if (formBuscaClientes.ShowModal = mrok) then begin

                FUltimaBusqueda        := formBuscaClientes.UltimaBusqueda;
                peNumeroDocumento.Text := Trim(formBuscaClientes.Persona.NumeroDocumento);
                FCodigoPersona         := formBuscaClientes.Persona.CodigoPersona;
                FRUTPersona            := Trim(formBuscaClientes.Persona.NumeroDocumento);
                FPersoneria            := formBuscaClientes.Persona.Personeria;

                if FCodigoPersona <> 0 then begin
                	MostrarDatosCliente(FCodigoPersona, FRUTPersona, FPersoneria);
                    btn_FiltrarClick(nil);
                end;
            end;
        end;
    finally
    	if Assigned(formBuscaClientes) then FreeAndNil(formBuscaClientes);
    end;
end;


procedure TFormConsultaAltaPArauco.peNumeroDocumentoChange(Sender: TObject);
begin
	if not FLimpiando then Limpiar(edtPatente);
end;

procedure TFormConsultaAltaPArauco.peNumeroDocumentoKeyPress(Sender: TObject;var Key: Char);
begin
    if Key = #13 then btn_FiltrarClick(nil);
end;

procedure TFormConsultaAltaPArauco.ppDBMemDescripcionPrint(Sender: TObject);
begin
    if spValidarRestriccionesAdhesionPAK.FieldByName('Codigo').AsInteger = 0 then begin
    	ppDBMemDescripcion.Font.Style    := ppDBMemDescripcion.Font.Style + [fsBold];
        ppDBMemDescripcion.Transparent   := False;
    end
    else begin
    	ppDBMemDescripcion.Font.Style    := ppDBMemDescripcion.Font.Style - [fsBold];
        ppDBMemDescripcion.Transparent   := True;
    end;
end;

procedure TFormConsultaAltaPArauco.ttprImpedimentosBeforePrint(Sender: TObject);
begin
    pplblRUT.Caption           := lblRUT.Caption;
    pplblNombreCliente.Caption := lblNombreCli.Caption;
end;

procedure TFormConsultaAltaPArauco.btn_LimpiarClick(Sender: TObject);
begin
	Limpiar(nil);
end;

procedure TFormConsultaAltaPArauco.MostrarDatosCliente(CodigoCliente: Integer; RUT, Personeria: string);
	const
      	cSQLObtenerNombrePersona  = 'SELECT dbo.ObtenerNombrePersona(%d)';
    	cSQLFormatearRutConPuntos = 'SELECT dbo.FormatearRutConPuntos(''%s'')';
begin
	lblRUT.Caption       := QueryGetValue(DMConnections.BaseCAC, Format(cSQLFormatearRutConPuntos, [RUT])) + '   ';
    lblNombreCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [CodigoCliente]));

    if Personeria = PERSONERIA_FISICA then begin
        lblPersoneria.Caption := ' - (Persona Natural)';
    end

    else begin
        if Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jur�dica)';
        if Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jur�dica)';
    end;

    lblNombreCli.Left  := lblRUT.Left + lblRUT.Width;
    lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
end;

function TFormConsultaAltaPArauco.ValidarFiltros: Boolean;
	resourcestring
    	rsTituloValidacion = 'Validaci�n Filtros Consulta';
        rsErrorDatosConsulta = 'No ha ingresado datos para la consulta.';
        rsRUTNoValido = 'El RUT ingresado NO es v�lido.';
        rsPatenteNoValida = 'La Patente ingresada NO es v�lida.';
    const
        MaxArray = 3;
    var
        vControles  : Array [1..MaxArray] of TControl;
        vCondiciones: Array [1..MaxArray] of Boolean;
        vMensajes   : Array [1..MaxArray] of String;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);

        vControles[01] := peNumeroDocumento;
        vControles[02] := peNumeroDocumento;
        vControles[03] := edtPatente;

        vCondiciones[01] := (edtPatente.Text <> EmptyStr) or ((peNumeroDocumento.Text) <> EmptyStr);
        vCondiciones[02] := (peNumeroDocumento.Text = EmptyStr) or ValidarRUT(DMConnections.BaseCAC, peNumeroDocumento.Text);
        vCondiciones[03] := (edtPatente.Text = EmptyStr) or VerificarFormatoPatenteChilena(DMConnections.BaseCAC, edtPatente.Text);

        vMensajes[01] := rsErrorDatosConsulta;
        vMensajes[02] := rsRUTNoValido;
        vMensajes[03] := rsPatenteNoValida;

    	try
        	Result := ValidateControls(vControles, vCondiciones, rsTituloValidacion, vMensajes);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaPArauco.BuscarCliente;
	resourcestring
    	rsTituloMensaje = 'Busqueda Cliente';
        rsClienteNoencontrado = 'No se encontr� el Cliente por los filtros de b�squeda indicados.';
    const
    	cSQLObtenerRUTPorCuentaActiva = 'SELECT dbo.ObtenerRUTPorCuentaActiva(''%s'')';
    var
    	Persona: TDatosPersonales;
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
        	if Trim(edtPatente.Text) <> EmptyStr then begin
            	FRUTPersona := Trim(QueryGetValue(DMConnections.BaseCAC, Format(cSQLObtenerRUTPorCuentaActiva, [Trim(edtPatente.Text)])));
            end
            else begin
            	FRUTPersona := Trim(peNumeroDocumento.Text);
            end;

            if FRUTPersona = EmptyStr then begin
            	raise EWarningExceptionCN.Create(rsTituloMensaje, rsClienteNoencontrado);
            end;

            Persona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', FRUTPersona);

            if Persona.CodigoPersona <> -1 then begin
            	FCodigoPersona := Persona.CodigoPersona;
                FPersoneria    := Persona.Personeria;

                MostrarDatosCliente(FCodigoPersona, FRUTPersona, FPersoneria);
            end
            else begin
            	raise EWarningExceptionCN.Create(rsTituloMensaje, rsClienteNoencontrado);
            end;
        except
        	on e: Exception do begin
                if not FSoloConsulta then begin
		            ShowMsgBoxCN(e, Self);
                end
                else raise;
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

function TFormConsultaAltaPArauco.ValidarCliente: Boolean;
	resourcestring
    	rsTituloMensaje          = 'Validaci�n Cliente';
        rsClientePersonaJuridica = 'El Cliente NO puede ser Persona Jur�dica para adherise al servicio Arauco TAG.';
        rsClienteYaAdherido      = 'El Cliente ya est� adherido al servicio Arauco TAG.';
        rsClienteYaInscrito      = 'El Cliente ya se inscribi� v�a WEB, para solicitar la adhesi�n al servicio Arauco TAG.';
	const
    	cSQLValidarPersonaAdheridaPA = 'SELECT dbo.ValidarPersonaAdheridaPAK(%d)';
        cSQLValidarPersonaInscritaWebPA = 'SELECT dbo.ValidarPersonaInscritaWebPAK(%d)';
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
        Result := False;

    	try
            if (StrToInt64(Copy(FRUTPersona, 1, Length(FRUTPersona) - 1)) >= 50000000) or (FPersoneria = PERSONERIA_JURIDICA) then begin
            	if not FSoloConsulta then begin
                    AgregarAuditoria_AdhesionPAK(
                        Trim(peNumeroDocumento.Text),
                        Trim(edtPatente.Text),
                        EmptyStr,
                        FCodigoPersona,
                        0,
                        -1,
                        -1,
                        -1,
                        FCodigoAuditoria);
                end;

            	raise EWarningExceptionCN.Create(rsTituloMensaje, rsClientePersonaJuridica);
            end;

            if QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarPersonaAdheridaPA, [FCodigoPersona])) then begin
            	if not FSoloConsulta then begin
                    AgregarAuditoria_AdhesionPAK(
                        Trim(peNumeroDocumento.Text),
                        Trim(edtPatente.Text),
                        EmptyStr,
                        FCodigoPersona,
                        2,
                        -1,
                        -1,
                        -1,
                        FCodigoAuditoria);
                end;

            	raise EInfoExceptionCN.Create(rsTituloMensaje, rsClienteYaAdherido);
            end;

//            if QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarPersonaInscritaWebPA, [FCodigoPersona])) then begin
//            	if not FSoloConsulta then begin
//                    AgregarAuditoria_AdhesionPAK(
//                        Trim(peNumeroDocumento.Text),
//                        Trim(edtPatente.Text),
//                        EmptyStr,
//                        FCodigoPersona,
//                        3,
//                        -1,
//                        -1,
//                        -1,
//                        FCodigoAuditoria);
//                end;
//
//            	raise EInfoExceptionCN.Create(rsTituloMensaje, rsClienteYaInscrito);
//            end;

            Result := True;
        except
        	on e: Exception do begin
            	if not FSoloConsulta then begin
		            ShowMsgBoxCN(e, Self);
                end
                else raise;
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaPArauco.ValidarRestriccionesAdhesionPA;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);

    	try
            with spValidarRestriccionesAdhesionPAK do begin
            	if Active then Close;

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
                Parameters.ParamByName('@ConDetalle').Value := 1;
                Open;

                if Parameters.ParamByName('@PuedeAdherirsePA').Value = 0 then begin
                	if not FSoloConsulta then begin

                        AgregarAuditoria_AdhesionPAK(
                            Trim(peNumeroDocumento.Text),
                            Trim(edtPatente.Text),
                            EmptyStr,
                            FCodigoPersona,
                            0,
                            -1,
                            -1,
                            -1,
                            FCodigoAuditoria);

	                    btnImprimirImpedimentos.Enabled := True;
                    end;
                end
                else begin
					if not FSoloConsulta then begin

                        AgregarAuditoria_AdhesionPAK(
                            Trim(peNumeroDocumento.Text),
                            Trim(edtPatente.Text),
                            EmptyStr,
                            FCodigoPersona,
                            1,
                            -1,
                            -1,
                            -1,
                            FCodigoAuditoria);

                        btnExclusionVehiculos.Enabled := True;                  //SS-1006-NDR-20120803
	                	btnImprimirContrato.Enabled := True;
                    end;
                end;
            end;
        except
        	on e: Exception do begin
            	if not FSoloConsulta then begin
		            ShowMsgBoxCN(e, Self);
                end
                else raise;
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaPArauco.dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if spValidarRestriccionesAdhesionPAK.FieldByName('Codigo').AsInteger = 0 then begin
        Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
    end;
end;

procedure TFormConsultaAltaPArauco.ActualizaEMailPersona(pEMail: string; pCodigoPersona: Integer);
	var
        CodigoMedioComunicacionEmailPersona: Integer;
        spActualizarMedioComunicacionPersona: TADOStoredProc;
	const
        cSQLObtenerCodigoMedioComunicacionEmailPersona = 'SELECT dbo.ObtenerCodigoMedioComunicacionEmailPersona(%d)';
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            CodigoMedioComunicacionEmailPersona := QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerCodigoMedioComunicacionEmailPersona, [FCodigoPersona]));

            spActualizarMedioComunicacionPersona := TADOStoredProc.Create(nil);

            with spActualizarMedioComunicacionPersona do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarMedioComunicacionPersona';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoTipoMedioContacto').Value := 1;
                	ParamByName('@CodigoArea').Value              := null;
                	ParamByName('@Valor').Value                   := pEMail;
                	ParamByName('@Anexo').Value                   := null;
                	ParamByName('@CodigoDomicilio').Value         := null;
                	ParamByName('@HorarioDesde').Value            := null;
                	ParamByName('@HorarioHasta').Value            := null;
                	ParamByName('@Observaciones').Value           := null;
                	ParamByName('@CodigoPersona').Value           := pCodigoPersona;
                	ParamByName('@Principal').Value               := 0;
                	ParamByName('@EstadoVerificacion').Value      := null;
                	ParamByName('@CodigoMedioComunicacion').Value := IIf(CodigoMedioComunicacionEmailPersona = 0, null, CodigoMedioComunicacionEmailPersona);
                end;
            end;

            DMConnections.BaseCAC.BeginTrans;
            spActualizarMedioComunicacionPersona.ExecProc;
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TFormConsultaAltaPArauco.ActualizaEMailPersona',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarMedioComunicacionPersona) then FreeAndNil(spActualizarMedioComunicacionPersona);
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaPArauco.ActualizarConveniosPersonaEnvioPorEMail(pCodigoPersona: Integer);
	var
        spActualizarConveniosPersonaEnvioPorEMail: TADOStoredProc;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            spActualizarConveniosPersonaEnvioPorEMail := TADOStoredProc.Create(nil);

            with spActualizarConveniosPersonaEnvioPorEMail do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarConveniosPersonaEnvioPorEMail';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoPersona').Value := pCodigoPersona;
                	ParamByName('@Usuario').Value       := UsuarioSistema;
                end;
            end;

            DMConnections.BaseCAC.BeginTrans;
            spActualizarConveniosPersonaEnvioPorEMail.ExecProc;
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TFormConsultaAltaPArauco.ActualizarConveniosPersonaEnvioPorEMail',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarConveniosPersonaEnvioPorEMail) then FreeAndNil(spActualizarConveniosPersonaEnvioPorEMail);
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConsultaAltaPArauco.AdherirClienteAraucoTAG(pCodigoPersona: Integer; pEMail: string);
	resourcestring
    	rsParametroGeneralNOExiste = 'El Par�metro General "%s" NO Existe, o contiene un valor inv�lido.';
        rsErrorspActualizarConveniosClienteEnListaDeAcceso = 'Error al ejecutar el procedimiento spActualizarConveniosClienteEnListaDeAcceso. Error: %s.';
	const
        cSQLTemporal = 'UPDATE Personas SET AdheridoPA = 1 WHERE CodigoPersona = %d';
        cSQLObtenerPrimerCodigoConvenioPersona = 'SELECT dbo.ObtenerPrimerCodigoConvenioPersona(%d, 1)';
        cFormatoParametros = '"%s"';

        COD_PLANTILLA_EMAIL_PAK = 'COD_PLANTILLA_EMAIL_PAK';
        COD_FIRMA_EMAIL_PAK = 'COD_FIRMA_EMAIL_PAK';

	var
        spActualizarConveniosClienteEnListaDeAcceso,
        spEMAIL_AgregarMensaje: TADOStoredProc;
    	CodigoConvenio,
        CodigoPlantillaEMail,
        CodigoFirmaEMail: Integer;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
        try
            spActualizarConveniosClienteEnListaDeAcceso := TADOStoredProc.Create(nil);
            spEMAIL_AgregarMensaje := TADOStoredProc.Create(nil);

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, COD_PLANTILLA_EMAIL_PAK, CodigoPlantillaEMail) then begin
            	raise Exception.Create(Format(rsParametroGeneralNOExiste, [COD_PLANTILLA_EMAIL_PAK]));
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, COD_FIRMA_EMAIL_PAK, CodigoFirmaEMail) then begin
            	raise Exception.Create(Format(rsParametroGeneralNOExiste, [COD_FIRMA_EMAIL_PAK]));
            end;

        	CodigoConvenio := QueryGetIntegerValue(DMConnections.BaseCAC, Format(cSQLObtenerPrimerCodigoConvenioPersona, [pCodigoPersona]));

            with spActualizarConveniosClienteEnListaDeAcceso do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarConveniosClienteEnListaDeAcceso';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@CodigoPersona').Value := pCodigoPersona;
                	ParamByName('@Usuario').Value       := UsuarioSistema;
                	ParamByName('@MensajeError').Value  := EmptyStr;
                end;
            end;

            with spEMAIL_AgregarMensaje do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'EMAIL_AgregarMensaje';
                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@Email').Value 				   := pEMail;
                	ParamByName('@CodigoPlantilla').Value          := CodigoPlantillaEMail;
                	ParamByName('@CodigoFirma').Value  			   := CodigoFirmaEMail;
                	ParamByName('@CodigoConvenio').Value  		   := CodigoConvenio;
                	ParamByName('@Parametros').Value               := Format(cFormatoParametros, [lblNombreCli.Caption]);
                	ParamByName('@NumeroProcesoFacturacion').Value := null;
                end;
            end;

            DMConnections.BaseCAC.BeginTrans;
			//BEGIN : SS-1006-NDR-20120803-------------------------------------------------
            if cdsCuentasPersona.Active then
            begin
                AdherirPorCuenta();
            end
            else
            begin
                with spActualizarConveniosClienteEnListaDeAcceso do begin
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                        raise Exception.Create(Format(rsErrorspActualizarConveniosClienteEnListaDeAcceso, [Parameters.ParamByName('@MensajeError').Value]));
                    end;
                end;
			//END : SS-1006-NDR-20120803-------------------------------------------------

            end;

            if (pEMail <> EmptyStr) then begin
	            spEMAIL_AgregarMensaje.ExecProc;
            end;

            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
        except
        	on e: Exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
            	raise EErrorExceptionCN.Create('procedure TFormConsultaAltaPArauco.AdherirClienteAraucoTAG',e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarConveniosClienteEnListaDeAcceso) then begin
        	FreeAndNil(spActualizarConveniosClienteEnListaDeAcceso);
        end;
    	if Assigned(spEMAIL_AgregarMensaje) then begin
        	FreeAndNil(spEMAIL_AgregarMensaje);
        end;
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;


function TFormConsultaAltaPArauco.AgregarAuditoria_AdhesionPAK(
	pNumeroDocumento, pPatente, pEMail: string;
    pCodigoPersona, pCumpleRequisitos, pAdheridoEnvioEMail, pImpresionContrato, pFirmaContrato, pCodigoAuditoria: Integer): Integer;
	var
        spAgregarAuditoria_AdhesionPAK: TADOStoredProc;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
            spAgregarAuditoria_AdhesionPAK := TADOStoredProc.Create(nil);

            with spAgregarAuditoria_AdhesionPAK do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'AgregarAuditoria_AdhesionPAK';

                Parameters.Refresh;
                with Parameters do begin
                	ParamByName('@Usuario').Value                := UsuarioSistema;
                	ParamByName('@NumeroDocumento').Value        := IIf(pNumeroDocumento = EmptyStr, null, pNumeroDocumento);
                	ParamByName('@Patente').Value                := IIf(pPatente = EmptyStr, null, pPatente);
                	ParamByName('@CodigoPersona').Value          := pCodigoPersona;
                	ParamByName('@CumpleRequisitos').Value       := pCumpleRequisitos;
                	ParamByName('@EMail').Value        	         := IIf(pEMail = EmptyStr, null, pEMail);
                	ParamByName('@AdheridoEnvioEMail').Value     := IIf(pAdheridoEnvioEMail = -1, null, pAdheridoEnvioEMail);
                	ParamByName('@ImpresionContrato').Value      := IIf(pImpresionContrato = -1, null, pImpresionContrato);
                	ParamByName('@FirmaContrato').Value          := IIf(pFirmaContrato = -1, null, pFirmaContrato);
                	ParamByName('@AdhesionPreInscripcion').Value := null;
                	ParamByName('@IDCodigoAuditoria').Value      := IIf(pCodigoAuditoria = -1, null, pCodigoAuditoria);

	                ExecProc;

                    FCodigoAuditoria := ParamByName('@IDCodigoAuditoria').Value;
                end;
            end;
        except
        	on e: Exception do begin
            	raise EErrorExceptionCN.Create('procedure TFormConsultaAltaPArauco.AgregarAuditoria_AdhesionPAK', e.Message);
            end;
        end;
    finally
    	if Assigned(spAgregarAuditoria_AdhesionPAK) then FreeAndNil(spAgregarAuditoria_AdhesionPAK);
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

//BEGIN : SS-1006-NDR-20120803--------------------------------------------------
procedure TFormConsultaAltaPArauco.AdherirPorCuenta;
resourcestring
    rsErrorspActualizarCuentaEnListaDeAcceso = 'Error al ejecutar el procedimiento spActualizarCuentaEnListaDeAcceso. Error: %s.';
    rsErrorspActualizarAdheridoPAConvenioPersona = 'Error al ejecutar el procedimiento spActualizarAdheridoPAConvenioPersona. Error: %s.';    //SS-1006-NDR-20120814
var
    CodigoConcesionaria: Integer;
    FechaAltaListaDeAcceso: TDateTime;
    spActualizarCuentaEnListaDeAcceso: TADOStoredProc;
    spActualizarAdheridoPAConvenioPersona : TADOStoredProc;                     //SS-1006-NDR-20120814
    CodigoConvenioActual: Integer;
begin
    FechaAltaListaDeAcceso  := NowBaseCN(DMConnections.BaseCAC);
    CodigoConcesionaria     := QueryGetTinyintValue(DMConnections.BaseCAC,'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()');

    try
        try
            spActualizarAdheridoPAConvenioPersona := TADOStoredProc.Create(nil);                            //SS-1006-NDR-20120814
            spActualizarAdheridoPAConvenioPersona.Connection := DMConnections.BaseCAC;                      //SS-1006-NDR-20120814
            spActualizarAdheridoPAConvenioPersona.ProcedureName := 'ActualizarAdheridoPAConvenioPersona';   //SS-1006-NDR-20120814

            spActualizarCuentaEnListaDeAcceso := TADOStoredProc.Create(nil);
            with spActualizarCuentaEnListaDeAcceso do
            begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'ActualizarCuentaEnListaDeAcceso';
                Parameters.Refresh;
                with Parameters do
                begin
                    ParamByName('@CodigoConcesionaria').Value   := CodigoConcesionaria;
                    ParamByName('@Usuario').Value               := UsuarioSistema;
                    ParamByName('@Fecha').Value                 := FechaAltaListaDeAcceso;
                    ParamByName('@TipoMovimiento').Value        := 'A';
                    ParamByName('@ActualizarBITAdherido').Value := 1;
                    with cdsCuentasPersona do
                    begin
                        First;
                        CodigoConvenioActual:=-1;
                        while not eof do
                        begin
                            if cdsCuentasPersona.FieldByName('Chequeado').AsBoolean then
                            begin
                                ParamByName('@CodigoConvenio').Value := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;
                                ParamByName('@IndiceVehiculo').Value := cdsCuentasPersona.FieldByName('IndiceVehiculo').AsInteger;
                                ParamByName('@MensajeError').Value :='';
                                ExecProc;
                                if Parameters.ParamByName('@MensajeError').Value <> '' then begin
                                    raise Exception.Create(Format(rsErrorspActualizarCuentaEnListaDeAcceso, [Parameters.ParamByName('@MensajeError').Value]));
                                end;
                            end
                            else
                            begin
                                with spActualizarAdheridoPAConvenioPersona do                                                                       //SS-1006-NDR-20120814
                                begin                                                                                                               //SS-1006-NDR-20120814
                                    Close;                                                                                                          //SS-1006-NDR-20120814
                                    Parameters.Refresh;                                                                                             //SS-1006-NDR-20120814
                                    Parameters.ParamByName('@CodigoConvenio').Value := cdsCuentasPersona.FieldByName('CodigoConvenio').AsInteger;   //SS-1006-NDR-20120814
                                    Parameters.ParamByName('@Usuario').Value := UsuarioSistema;                                                     //SS-1006-NDR-20120814
                                    ExecProc;                                                                                                       //SS-1006-NDR-20120814
                                    if Parameters.ParamByName('@MensajeError').Value <> '' then                                                     //SS-1006-NDR-20120814
                                    begin                                                                                                           //SS-1006-NDR-20120814
                                        raise Exception.Create( Format(rsErrorspActualizarAdheridoPAConvenioPersona,                                //SS-1006-NDR-20120814
                                                                [Parameters.ParamByName('@MensajeError').Value])                                    //SS-1006-NDR-20120814
                                                              );                                                                                    //SS-1006-NDR-20120814
                                    end;                                                                                                            //SS-1006-NDR-20120814
                                end;                                                                                                                //SS-1006-NDR-20120814
                            end;
                            Next;
                        end;
                    end;
                end;
            end;
        except
            on e: Exception do begin
            	raise EErrorExceptionCN.Create('Error al dar de alta los veh�culos del cliente en AraucoTAG', e.Message);
            end;
        end;
    finally
    	if Assigned(spActualizarCuentaEnListaDeAcceso) then FreeAndNil(spActualizarCuentaEnListaDeAcceso);
    end;
end;
//END : SS-1006-NDR-20120803--------------------------------------------------


procedure TFormConsultaAltaPArauco.cdsCuentasPersonaAfterOpen(DataSet: TDataSet);
begin
    cdsCuentasPersona.Tag := cdsCuentasPersona.RecordCount;
end;

procedure TFormConsultaAltaPArauco.cdsCuentasPersonaAfterPost(DataSet: TDataSet);
begin
    if cdsCuentasPersonaChequeado.AsBoolean then begin
        cdsCuentasPersona.Tag := cdsCuentasPersona.Tag + 1;
    end
    else begin
        cdsCuentasPersona.Tag := cdsCuentasPersona.Tag - 1;
    end;

    btnImprimirContrato.Enabled := cdsCuentasPersona.Tag > 0;
end;

end.


