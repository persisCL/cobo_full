object FormCheckListConvenio: TFormCheckListConvenio
  Left = 258
  Top = 154
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Lista de Observaciones'
  ClientHeight = 447
  ClientWidth = 646
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel2: TBevel
    Left = 6
    Top = 237
    Width = 635
    Height = 169
  end
  object Label1: TLabel
    Left = 16
    Top = 243
    Width = 165
    Height = 13
    Caption = '&Lista de Chequeo - Convenio'
    FocusControl = ConvenioCheckListBox
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 264
    Top = 243
    Width = 158
    Height = 13
    Caption = '&Lista de Chequeo - Cuentas'
    FocusControl = ConvenioCheckListBox
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object ConvenioCheckListBox: TVariantCheckListBox
    Left = 16
    Top = 266
    Width = 241
    Height = 137
    ItemHeight = 13
    Items = <>
    TabOrder = 0
  end
  object btnAceptar: TButton
    Left = 472
    Top = 416
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 560
    Top = 416
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
  object CuentasCheckListBox: TVariantCheckListBox
    Left = 264
    Top = 266
    Width = 369
    Height = 137
    OnClickCheck = CuentasCheckListBoxClickCheck
    ItemHeight = 13
    Items = <>
    TabOrder = 3
    OnClick = CuentasCheckListBoxClick
  end
  object btnEditarVencimiento: TButton
    Left = 517
    Top = 243
    Width = 115
    Height = 17
    Caption = 'Cambiar Vencimiento'
    TabOrder = 4
    OnClick = btnEditarVencimientoClick
  end
  object pgcConvenioObservaciones: TPageControl
    Left = 6
    Top = 4
    Width = 633
    Height = 229
    ActivePage = tsObservaciones
    TabOrder = 5
    object tsObservaciones: TTabSheet
      Caption = 'Observaciones'
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 625
        Height = 198
        Align = alTop
      end
      object Label2: TLabel
        Left = 6
        Top = 7
        Width = 85
        Height = 13
        Caption = '&Observaciones'
        FocusControl = ConvenioCheckListBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
  object btnAgregar: TButton
        Left = 560
        Top = 26
    Width = 57
    Height = 25
    Caption = '&Agregar'
        TabOrder = 0
    OnClick = btnAgregarClick
  end
  object btnModificar: TButton
        Left = 560
        Top = 57
    Width = 57
    Height = 25
    Caption = '&Ver/Modif.'
        TabOrder = 1
    OnClick = btnModificarClick
  end
  object btnEliminar: TButton
        Left = 560
        Top = 88
    Width = 57
    Height = 25
    Caption = '&Eliminar'
        TabOrder = 2
    OnClick = btnEliminarClick
  end
  object dblObservaciones: TDBListEx
        Left = 6
        Top = 25
    Width = 545
        Height = 172
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'N'#250'mero'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'IndiceObservacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 400
        Header.Caption = 'Descripci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Descripcion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Fecha Observaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaObservacion'
      end>
    DataSource = dsObservaciones
    DragReorder = True
    ParentColor = False
        TabOrder = 3
    TabStop = True
    OnDblClick = dblObservacionesDblClick
    OnDrawText = dblObservacionesDrawText
  end
  end
    object tsHistorico: TTabSheet
      Caption = 'Hist'#243'rico'
      ImageIndex = 1
      object pnlBuscar: TPanel
        Left = 0
        Top = 0
        Width = 625
        Height = 41
        Align = alTop
        TabOrder = 0
        object lblHistoricoDesde: TLabel
          Left = 13
          Top = 13
          Width = 34
          Height = 13
          Caption = 'Desde:'
        end
        object lblHistoricoHasta: TLabel
          Left = 159
          Top = 13
          Width = 31
          Height = 13
          Caption = 'Hasta:'
        end
        object deHistoricoDesde: TDateEdit
          Left = 57
          Top = 10
          Width = 90
          Height = 21
          AutoSelect = False
          TabOrder = 0
          OnChange = deHistoricoDesdeChange
          Date = -693594.000000000000000000
        end
        object deHistoricoHasta: TDateEdit
          Left = 198
          Top = 10
          Width = 90
          Height = 21
          AutoSelect = False
          TabOrder = 1
          OnChange = deHistoricoHastaChange
          Date = -693594.000000000000000000
        end
        object btnBuscar: TButton
          Left = 546
          Top = 7
    Width = 75
    Height = 25
          Caption = 'Buscar'
          TabOrder = 2
          OnClick = btnBuscarClick
  end
  end
      object DBLEHistoricoObservaciones: TDBListEx
        Left = 0
        Top = 41
        Width = 625
        Height = 160
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 55
            Header.Caption = 'N'#250'mero'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'IndiceObservacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 350
            Header.Caption = 'Observaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Observacion'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 130
            Header.Caption = 'Fecha Observaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaObservacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'EstadoObservacion'
          end>
        DataSource = dsHistoricoConvenioObservaciones
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
        OnDrawText = DBLEHistoricoObservacionesDrawText
      end
      object chkIncluyeDeBaja: TCheckBox
        Left = 307
        Top = 12
        Width = 174
    Height = 17
        Caption = 'Incluir Observaciones de Baja'
        TabOrder = 2
        OnClick = chkIncluyeDeBajaClick
      end
    end
  end
  object cdsObservaciones: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'IndiceObservacion'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 400
      end
      item
        Name = 'Activo'
        DataType = ftBoolean
      end
      item
        Name = 'FechaObservacion'
        DataType = ftDateTime
      end
      item
        Name = 'Modificada'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterPost = cdsObservacionesAfterPost
    AfterDelete = cdsObservacionesAfterPost
    Left = 349
    Top = 139
  end
  object dsObservaciones: TDataSource
    DataSet = cdsObservaciones
    Left = 315
    Top = 139
  end
  object spObtenerHistoricoConvenioObservaciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerHistoricoConvenioObservaciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@IncluyeDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 552
    Top = 104
  end
  object dsHistoricoConvenioObservaciones: TDataSource
    DataSet = spObtenerHistoricoConvenioObservaciones
    Left = 512
    Top = 104
  end
end
