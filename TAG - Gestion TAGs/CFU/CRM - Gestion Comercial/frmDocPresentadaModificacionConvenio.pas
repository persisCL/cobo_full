unit frmDocPresentadaModificacionConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DB, ADODB,DMConnection,RStrings, PeaProcs,
  DBClient,util,UtilProc, DmiCtrls,Peatypes, UtilDB, StrUtils, Convenios;

type
    TRepresentante = (Primero,Segundo,Tercero);

type
  TformDocPresentadaModificacionConvenio = class(TForm)
    gb_Convenio: TGroupBox;
    Panel10: TPanel;
    sp: TADOStoredProc;
    sb_Convenio: TScrollBox;
    QryMaestroDoc: TADOQuery;
    btn_Cancelar: TButton;
    btn_ok: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_okClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FTienePAT: boolean;
    function MostrarDocumentacion(Panel: TScrollBox;
                    MargenSuperior: Integer;
                    Documentacion: TDocumentacion):Integer;
    procedure OnClickCombo(Sender: TObject);
    function BuscarComponente(Original:TCheckBox):TCheckBox;
    procedure SetTienePAT(const Value: boolean);
    function CalculaHeightLabel(AnchoLabel:integer;Texto:String): integer; //lenghtTexto
    procedure Habilitar(Control: TWinControl);

  public
    { Public declarations }

    function InicializarModificacionConvenio(Caption: TCaption;
            Convenio: TDatosConvenio;
            AutorizacionRecepcionFacturasMail: boolean;
            var CantDocumentosPedidos: integer;
            Modo: TStateConvenio = scAlta): Boolean;

    property TienePAT: boolean read FTienePAT write SetTienePAT;

  end;

var

  formDocPresentadaModificacionConvenio: TformDocPresentadaModificacionConvenio;

const
    MARGENTOP=17;
    MARGENLEFT=10;


implementation

{$R *.dfm}

{ TformDocPresentada }


procedure TformDocPresentadaModificacionConvenio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action :=  caFree;
end;


procedure TformDocPresentadaModificacionConvenio.OnClickCombo(Sender: TObject);
var
    auxColor:TColor;
begin
    with TCheckBox(sender) do begin
        if Checked then begin
            auxColor:=clWindowText;
        end else begin
            auxColor:=clRed;
        end;
        if BuscarComponente(TCheckBox(sender))<>nil then begin
            if Checked then
                BuscarComponente(TCheckBox(sender)).Checked:=not(Checked);
            BuscarComponente(TCheckBox(sender)).font.Color:=auxColor;
        end;
        TCheckBox(sender).Font.Color:=auxColor;
    end;

end;

{*****************************************************************************
  Function Name: BuscarComponente
  Author: dcalani
  Date Created: 09/11/2004
  Description: Busca el TCheckBox de "no Corresponde" del TCheckBox original
  Parameters: Original:TCheckBox
  Return Value: TCheckBox
*****************************************************************************}
function TformDocPresentadaModificacionConvenio.BuscarComponente(Original:TCheckBox): TCheckBox;
var
    i: integer;
begin
    result := nil;
    for i := 0 to Original.Parent.ComponentCount - 1 do
        if (Original.Parent.Components[i].ClassType = TCheckBox) and
            (Original.Parent.Components[i].Name <> Original.Name) and
            (Original.Parent.Components[i].Tag = Original.Tag) then
                result:=TCheckBox(Original.Parent.Components[i]);

end;

procedure TformDocPresentadaModificacionConvenio.btn_okClick(Sender: TObject);
var
    i :Integer;
begin
    // verifico si quedo algo sin controlar
    for i:=0 to sb_Convenio.ComponentCount - 1 do begin
        if (sb_Convenio.Components[i].ClassType = TCheckBox) and
           (StrLeft(sb_Convenio.Components[i].Name,5) = 'Check') and
           not(TCheckBox(sb_Convenio.Components[i]).Checked) and
           ( (BuscarComponente(TCheckBox(sb_Convenio.Components[i])) = nil) or not(BuscarComponente(TCheckBox(sb_Convenio.Components[i])).Checked))
            then begin
                    MsgBoxBalloon(MSG_ERROR_DOCUMENTACION,MSG_CAPTION_VALIDAR_DOCUMENTACION,MB_ICONSTOP,TCheckBox(sb_Convenio.Components[i]));
                    exit;
        end;
    end;

    // Si esta todo ok, marco la doc como "marcada"
    for i:=0 to sb_Convenio.ComponentCount - 1 do begin
        if (sb_Convenio.Components[i].ClassType = TCheckBox) and
           (StrLeft(sb_Convenio.Components[i].Name,5) = 'Check') then begin
            TDocumentacion(sb_Convenio.Components[i].tag).Marcado := True;
            TDocumentacion(sb_Convenio.Components[i].tag).NoCorresponde := not TCheckBox(sb_Convenio.Components[i]).Checked;
        end;
    end;

    // esta todo ok
    ModalResult := mrOk;
end;

procedure TformDocPresentadaModificacionConvenio.btn_CancelarClick(Sender: TObject);
begin
    ModalResult:=mrCancel;
end;

procedure TformDocPresentadaModificacionConvenio.SetTienePAT(const Value: boolean);
begin
  FTienePAT := Value;
end;

function TformDocPresentadaModificacionConvenio.CalculaHeightLabel(
  AnchoLabel:integer;Texto:String): integer;
var
    i,suficiente: integer;
    TlabelAux:Tlabel;
begin
    TlabelAux:=Tlabel.create(nil);
    with TlabelAux do begin
        parent:=self;
        AutoSize:=True;
        Caption:=Texto;
        WordWrap:=False;
        AutoSize:=False;
    end;

    i:=1;
    while TlabelAux.Width>AnchoLabel do begin
        i:=i+1;
        TlabelAux.Width:=TlabelAux.Width-AnchoLabel;
    end;

    suficiente :=16;// 13;
    {Esta funcion depende del width del label, habr�a que buscar
    la manera de calcula cuantos caracteres entran de un determinado
    font (con su tama�o y styles). Si se cambia el witdth, revisarlo esto...
    Tener en cuenta que las propiedades del label cuando se crea:
    Wordwrap = true
    Autosize = false
    }

    Result:=i*suficiente;
    TlabelAux.free;

end;

procedure TformDocPresentadaModificacionConvenio.Habilitar(Control: TWinControl);
begin
    if Control <> nil then Begin
        Control.Enabled := True;
        Habilitar(Control.Parent);
    end;
end;

function TformDocPresentadaModificacionConvenio.InicializarModificacionConvenio(
  Caption: TCaption; Convenio: TDatosConvenio; AutorizacionRecepcionFacturasMail: boolean;
  var CantDocumentosPedidos: integer; Modo: TStateConvenio): Boolean;
var
    margenSup, i: integer;
begin

    CantDocumentosPedidos := 0;
    margenSup := 1;
    for i := 0 to Convenio.DocumentacionConvenio.CantidadDocumentacion do begin
        with Convenio.DocumentacionConvenio.DarDocumentacion(i) do begin
            if (SoloImpresion <> 1) and (EsNuevo) and (FechaBaja = NullDate) then begin

                margenSup := MostrarDocumentacion(sb_Convenio,
                                        margenSup,
                                        Convenio.DocumentacionConvenio.DarDocumentacion(i));
                Inc(CantDocumentosPedidos);
            end;

        end;
    end;

    result := true;
    if (CantDocumentosPedidos = 0) then  ModalResult := mrOk;

    if Modo = scNormal then begin
        EnableControlsInContainer(sb_Convenio,False);
        btn_ok.Enabled := False;
    end;
end;


function TformDocPresentadaModificacionConvenio.MostrarDocumentacion(Panel: TScrollBox;
                    MargenSuperior: Integer;
                    Documentacion: TDocumentacion):Integer;
var
    CheckBoxAux, CheckBoxAuxNoObligatorio:TCheckBox;
    LabelExplicacion: TLabel;
    auxWidth,topLabel, heigthLabel: integer;
begin
    CheckBoxAux:=TCheckBox.Create(panel);
    with CheckBoxAux do begin //principal
        Parent:=Panel;
        top:=MargenSuperior+MARGENTOP;
        left:=MARGENLEFT;
        font.Style:=[];
        Alignment:= taLeftJustify;
        Caption:=STR_NO_CORRESPONDE; // trucada para que me de la long de un combo con este texto
        auxWidth:=Panel.Width-(5*MARGENLEFT)-Width;
        Caption:= Documentacion.Descripcion + iif(trim(Documentacion.InformacionExtra) <> '', ' (' + trim(Documentacion.InformacionExtra) + ')','' );
        Width:=auxWidth;
        Hint := Documentacion.Descripcion + iif(trim(Documentacion.InformacionExtra) <> '', #13#10 + Documentacion.InformacionExtra, '');
        ShowHint := true;
        Cursor:=crHandPoint;
        OnClick:=OnClickCombo;
        tag:= integer(Documentacion);
        font.Color:=clRed;
        name:='Check'+inttostr(panel.ComponentCount);
        if Documentacion.NoCorresponde then Checked := False
        else Checked := Documentacion.Marcado;
    end;
    if not(Documentacion.Obligatorio) then begin // check "no corresponde"
        CheckBoxAuxNoObligatorio:= TCheckBox.Create(panel);
        with CheckBoxAuxNoObligatorio do begin
            parent:=Panel;
            left:=CheckBoxAux.Left+CheckBoxAux.Width+MARGENLEFT;
            top:=MargenSuperior+MARGENTOP;
            Caption:=STR_NO_CORRESPONDE;
            Alignment:=taLeftJustify;
            Cursor:=crHandPoint;
            OnClick:=OnClickCombo;
            tag:=Integer(Documentacion);
            font.Color:=clRed;
            name:='NoCorresponde'+inttostr(panel.ComponentCount);
            Checked := Documentacion.NoCorresponde;
            if Checked then BuscarComponente(CheckBoxAuxNoObligatorio).font.Color:= clWindowText;
       end;
    end;

    topLabel := CheckBoxAux.Top + CheckBoxAux.Height;
    if trim(Documentacion.Explicacion) <> '' then begin //explicacion
        LabelExplicacion := TLabel.Create(panel);
        with LabelExplicacion do
        begin
            Parent:=Panel;
            top:= topLabel + 1;
            left:=MARGENLEFT;
            font.Style:=[];
            Alignment:= taLeftJustify;
            Caption:=Documentacion.Explicacion; // trucada para que me de la long de un combo con este texto
            Width:= CheckBoxAux.Width - 15;
            AutoSize := false;
            WordWrap := true;
            name:='lbl'+inttostr(panel.ComponentCount);
            topLabel := LabelExplicacion.Top;
            heigthLabel := CalculaHeightLabel(CheckBoxAux.Width,Documentacion.Explicacion);
            Height := heigthLabel;
        end;
        topLabel := topLabel + heigthLabel;        
    end;


    if not(Documentacion.Obligatorio) and (CheckBoxAux.Checked) then BuscarComponente(CheckBoxAux).font.Color:= clWindowText;

    result:= topLabel;
end;

end.

