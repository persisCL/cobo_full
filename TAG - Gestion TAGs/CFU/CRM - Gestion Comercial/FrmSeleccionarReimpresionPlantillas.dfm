object FrmSeleccionarReimpresion: TFrmSeleccionarReimpresion
  Left = 324
  Top = 252
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Seleccionar Documentos'
  ClientHeight = 320
  ClientWidth = 287
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object gb_Convenio: TGroupBox
    Left = 0
    Top = 0
    Width = 287
    Height = 281
    Align = alClient
    Caption = 'Seleccione los Documentos a Reimprimir'
    TabOrder = 0
    DesignSize = (
      287
      281)
    object sb_Convenio: TScrollBox
      Left = 16
      Top = 28
      Width = 258
      Height = 247
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      object cbAnverso: TCheckBox
        Left = 18
        Top = 9
        Width = 217
        Height = 16
        Alignment = taLeftJustify
        Caption = 'Anexo 1'
        TabOrder = 0
      end
      object cbCaratula: TCheckBox
        Left = 18
        Top = 222
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Car'#225'tula del Convenio'
        TabOrder = 8
        Visible = False
      end
      object cbMandato: TCheckBox
        Left = 18
        Top = 56
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Mandato PAC/PAT'
        TabOrder = 1
      end
      object cbEtiqueta: TCheckBox
        Left = 18
        Top = 80
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Etiquetas'
        TabOrder = 2
        Visible = False
      end
      object cbAnexoVehiculo: TCheckBox
        Left = 18
        Top = 128
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Resumen de Vehiculos'
        TabOrder = 4
        Visible = False
      end
      object cbAnexoAltaCuenta: TCheckBox
        Left = 18
        Top = 152
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Anexo de Alta / Baja / Modif de Cuentas'
        TabOrder = 5
        Visible = False
      end
      object CbMotos: TCheckBox
        Left = 18
        Top = 104
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Contrato de Motos'
        TabOrder = 3
        Visible = False
      end
      object cbAnexo3y4: TCheckBox
        Left = 18
        Top = 176
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Anexos 3 y 4'
        TabOrder = 6
        Visible = False
      end
      object cbContratoAdhesionPA: TCheckBox
        Left = 18
        Top = 199
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Contrato Adhesi'#243'n  Parque Arauco'
        TabOrder = 7
        Visible = False
      end
      object cbSuscripcionLB: TCheckBox
        Left = 18
        Top = 31
        Width = 217
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Suscripci'#243'n Lista Blanca'
        TabOrder = 9
      end
    end
  end
  object Panel10: TPanel
    Left = 0
    Top = 281
    Width = 287
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      287
      39)
    object btn_Cancelar: TButton
      Left = 154
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Cancelar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object btn_ok: TButton
      Left = 73
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Documentaci'#243'n presentada en forma correcta'
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btn_okClick
    end
  end
end
