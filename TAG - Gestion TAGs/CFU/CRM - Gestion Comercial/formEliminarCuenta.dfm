object frmEliminarCuenta: TfrmEliminarCuenta
  Left = 631
  Top = 169
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Eliminar Cuenta'
  ClientHeight = 445
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 11
    Top = 153
    Width = 407
    Height = 29
  end
  object Label1: TLabel
    Left = 8
    Top = 14
    Width = 40
    Height = 13
    Caption = 'Patente:'
  end
  object lbl_Patente: TLabel
    Left = 56
    Top = 14
    Width = 65
    Height = 13
    Caption = 'lbl_Patente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 17
    Top = 160
    Width = 254
    Height = 13
    Caption = 'Para seleccionar un motivo de facturaci'#243'n haga click '
  end
  object lblSeleccionar: TLabel
    Left = 277
    Top = 160
    Width = 22
    Height = 13
    Cursor = crHandPoint
    Caption = 'ac'#225
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
    OnClick = lblSeleccionarClick
  end
  object Label9: TLabel
    Left = 11
    Top = 188
    Width = 169
    Height = 13
    Caption = 'Motivos seleccionados actualmente'
  end
  object lblTotalMovimientos: TLabel
    Left = 332
    Top = 337
    Width = 78
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '0'
  end
  object lblTotalMovimientosTxt: TLabel
    Left = 234
    Top = 337
    Width = 98
    Height = 13
    AutoSize = False
    Caption = 'Total Movimientos :'
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 40
    Width = 233
    Height = 81
    Caption = 'Televia a Devolver'
    TabOrder = 0
    object Label2: TLabel
      Left = 12
      Top = 26
      Width = 42
      Height = 13
      Caption = 'Etiqueta:'
    end
    object Label3: TLabel
      Left = 12
      Top = 54
      Width = 33
      Height = 13
      Caption = 'Estado'
    end
    object txt_TagDevuelto: TEdit
      Left = 60
      Top = 22
      Width = 145
      Height = 21
      MaxLength = 11
      TabOrder = 0
      OnExit = txt_TagDevueltoExit
      OnKeyPress = txt_TagDevueltoKeyPress
    end
    object cb_EstadoTagDevuelto: TVariantComboBox
      Left = 60
      Top = 50
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cb_EstadoTagDevueltoChange
      Items = <>
    end
  end
  object cK_TagRobado: TCheckBox
    Left = 8
    Top = 127
    Width = 108
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Tag Robado'
    TabOrder = 1
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 404
    Width = 431
    Height = 41
    Align = alBottom
    TabOrder = 3
    object btnCancelar: TButton
      Left = 343
      Top = 9
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 0
    end
    object btnAceptar: TButton
      Left = 263
      Top = 9
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      Default = True
      Enabled = False
      TabOrder = 1
      OnClick = btnAceptarClick
    end
  end
  object dblConceptos: TDBListEx
    Left = 8
    Top = 207
    Width = 407
    Height = 124
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 60
        Header.Caption = 'Concepto'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'CodigoConcepto'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 250
        Header.Caption = 'Descripci'#243'n de Concepto'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'DescripcionMotivo'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Precio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'PrecioEnPesos'
      end>
    DataSource = dsConceptos
    DragReorder = True
    ParentColor = False
    PopupMenu = mnuGrillaConceptos
    TabOrder = 2
    TabStop = True
    OnContextPopup = dblConceptosContextPopup
    OnKeyDown = dblConceptosKeyDown
  end
  object gbModalidadEntregaTelevia: TGroupBox
    Left = 245
    Top = 40
    Width = 177
    Height = 49
    Caption = 'Modalidad de entrega del Telev'#237'a'
    TabOrder = 4
    object lblModalidadEntregaTelevia: TLabel
      Left = 10
      Top = 24
      Width = 158
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'lblModalidadEntregaTelevia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object ObtenerEstadosConservacionTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEstadosConservacionTAGs'
    Parameters = <>
    Left = 200
    Top = 8
  end
  object mnuGrillaConceptos: TPopupMenu
    Left = 134
    Top = 288
    object mnuEliminarConcepto: TMenuItem
      Caption = 'Eliminar Concepto'
      OnClick = mnuEliminarConceptoClick
    end
    object CargarConceptos1: TMenuItem
      Caption = 'Cargar Conceptos'
      OnClick = lblSeleccionarClick
    end
  end
  object dsConceptos: TDataSource
    DataSet = cdConceptos
    Left = 86
    Top = 288
  end
  object cdConceptos: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionMotivo'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'PrecioOrigen'
        DataType = ftInteger
      end
      item
        Name = 'Moneda'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Cotizacion'
        DataType = ftFloat
      end
      item
        Name = 'PrecioEnPesos'
        DataType = ftInteger
      end
      item
        Name = 'Comentario'
        DataType = ftString
        Size = 255
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 46
    Top = 288
    Data = {
      E40000009619E0BD010000001800000008000000000003000000E4000C53656C
      656363696F6E61646F02000300000000000E436F6469676F436F6E636570746F
      0400010000000000114465736372697063696F6E4D6F7469766F020049000000
      010005574944544802000200FF000C50726563696F4F726967656E0400010000
      000000064D6F6E656461020049000000010005574944544802000200FF000A43
      6F74697A6163696F6E08000400000000000D50726563696F456E5065736F7304
      000100000000000A436F6D656E746172696F0200490000000100055749445448
      02000200FF000000}
  end
end
