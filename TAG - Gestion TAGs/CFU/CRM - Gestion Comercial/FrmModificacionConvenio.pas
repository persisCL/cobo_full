{-------------------------------------------------------------------------------
File Name   : FrmModificacionConvenio.pas
Author		: Nelson Droguett Sierra
Date		: 13-Abril-2010
Description	: Permite la modificacion de Convenios y sus cuentas.

Revision	: 1
Author		: Nelson Droguett Sierra
Date		: 13-Abril-2010
Description	: Se agrega un Bit HabilitadoAMB por cada cuenta del convenio. Si
            	se modifica el Bit del convenio, este afecta todas las cuentas
                que descienden de el. Si se cambia el Bit de una cuenta, solo
                afecta a la cuenta..

Revision	: 2
Author		: mpiazza
Date		: 20/04/2009
Description	: ref: ss-787 se modifican los siguientes procedimientos
	Func. / Proc.	TFormModificacionConvenio.Inicializa*
	Acci�n(es)	Inicializo cb_RecibeClavePorMail

	Func. / Proc.	btn_GuardarClick*
	Acci�n(es)	se agrega la lectura del dato RecibeClavePorMail
	
	Func. / Proc.	CargarConvenio*
	Acci�n(es)	se agrega la lectura del dato RecibeClavePorMail
	
    Auhtor: mpiazza
    Date: 22/04/2010
    Description: ref:ss-680 se modifica txtEmailParticularExit


Revision	: 3
Author		: Nelson Droguett Sierra
Date		: 01-Junio-2010
Description	: ref: SS-887
              Se cambian el uso de las transacciones controladas por delphi
              a transacciones explicitas de la BD, por ejemplo :
                donde dice : DMConnections.BaseCAC.BeginTrans
                debe decir : QueryExecute(DMConnections.BaseCAC, 'BEGIN TRAN NombreDeLaTransaccion');

Revision	:5
Author		: Nelson Droguett Sierra
Date		: 09-Junio-2010
Description	: Ref. SS-887
              1.- Se crea funcion ArmarListaPatentesTags que mezcla la funcionalidad
                	de ArmarListaPatentes y ArmarListaTags, pero recorriendo solo una
                    vez la lista de vehiculos
              2.- Cuando se llama al metodo ObtenerVehiculos, antes se desconecta el
                  dataset y el cds se desconecta de la grilla

Revision	: 6
Author		: pdominguez
Date		: 01/07/2010
Description	: SS 887 - Mejorar la Performance - Cuentas Duplicadas.
                - se modificaron los siguientes Procedimientos/funciones:
                    btn_GuardarClick,
                    Inicializa


Revision	: 7
Author		: Nelson Droguett Sierra
Date		: 26-Julio-2010
Description	: SS 901 - Error al grabar el almacen de destino.

Revision    : 8
Author      : mbecerra
Date        : 15-Septiembre-2010
Description :       (Ref SS 921)
                La fecha de vencimiento del tag, para las cuentas nuevas
                se obtiene desde el formulario frmSCDatosVehiculo, el cual
                valida la fecha ingresada acorde a los par�metros indicados
                en la SS.

                Antes de grabar, vuelve a validar que la fecha ingresada est�
                dentro de los rangos indicados.

                Adicionalmente si el campo no ha sido ingresado, se valida
                como hasta ahora lo estaba haciendo.

Revision    : 9
Author      : mbecerra
Date        : 13-Abril-2011
Description :       (Ref SS 948)
                Se modifica la funci�n ValidarTagsEnArriendo para desplegar
                el importe correcto en el mensaje al usuario

Firma       : SS-948-MBE-20110413

    Firma: SS_964_PDO_20110531

    Descripci�n: SS 964 - Despliegue de Ventana de Facturaci�n
        - Al finalizar la grabaci�n del Convenio, en el caso de este poseer,
        movimientos cuenta con conceptos de Facturaci�n Inmediata sin facturar,
        se lanza la ventana de Facturaci�n con dichos movimientos ya seleccionados
        para facturar.

Description : Se agrega validaci�n en m�todo PuedeGuardarContrato y CambioPagoAutomatico
              para que se pueda agregar solamente un ConvenioSinCuenta
              y se pueda agregar convenios con medios de pago excluyendo de la
              validaci�n de los medios de pago a los ConvenioSinCuenta.
Firma       : SS_628_ALA_20110720

Description : Se quitan los par�metros del m�todo ObtenerConvenioMedioPago.
Firma       : SS_628_ALA_20110818

Firma       : SS-842-NDR-20110912
Description : Asignar a la variable FCodigoConvenio del frmCheckListConvenio el valor del convenio

Firma       : SS-1006-NDR-20111105
Description : Se implementa el bit HabilitadoPA para las listas de acceso de parque arauco
              Si se modifica el Bit del convenio, este afecta todas las cuentas
                que descienden de el. Si se cambia el Bit de una cuenta, solo
                afecta a la cuenta..
Firma       : SS_628_ALA_20111202
Description : Se incluye checkbox chkConvenioSinCuenta para indicar si un Convenio es sin cuenta o no.
              Adem�s se crea funci�n EsConvenioSinCuenta el cual indica si este convenio es Sin Cuenta.
              Si el convenio es sin cuenta se impide el ingreso de Cuentas

Firma       : SS_628_ALA_20111207
Description : Se habilita el bot�n Guardar cuando se cambia el valor del checkbox SinCuenta
              Adem�s se deshabilita el checkbox cuando el convenio no es de CN o se
              encuentra Vigente.

Firma       : SS_628_ALA_20111219
Description : Se agrega validaci�n si existe alg�n otro convenio sin cuenta, cuando este convenio
              se ha marcado como SinCuenta.

Firma       : SS-1006-NDR-20120531
Description : Se implementa el estado de cuenta INHABILITADA para mostrar en la interfaz esta condicion.

Firma       : SS-1006-NDR-20120614
Descripcion : El Bit HabilitadoPA ahora se llama AdheridoPA
              Nueva operatoria de la Habilitacion de Convenio y Cuentas para EPA

Firma       : SS-1006-NDR-20120715
Descripcion : Si el convenio a actualizar es AdheridoPA=1 y no estaba adherido de antes , entonces
							adherir� a todos los convenios no adheridos de la persona. Y dentro de los convenios no
							adheridos, a todas las cuentas no adheridas.
							Ademas actualizar� el bit AdheridoPA de la persona.
							Si el convenio es AdheridoPA=0, entonces desadherir� a todos los convenios Adheridos y
							dentro de esos convenios a todas las cuentas adheridas de esos convenios.
              Si en "PorCuenta" desmarca todas las cuentas, el convenio seguir� marcado.
              Al activar una cuenta suspendida, la cuenta hereda el AdheridoPA del convenio.
              Al dar por "Perdido" una cuenta, se debe dar de baja en la lista de acceso (si estaba adherida)
              Al eliminar una cuenta, se debe dar de baja en la lista de acceso (si estaba adherida)

Firma       : SS-1006-NDR-20120727
Descripcion : Imprimir el contrato de AdhesionPA.
              Avisar que esta el turno cerrado pero no impedir las acciones a realizar por que pueden ser solo de consulta.

Firma       : SS_1006_1015_MCO_20121003
Descripcion : Se valida que en la adhesion de cuentas en lista de acceso, permita seleccionar todos las cuentas.

Firma       : SS_1006_NDR_20121003
Descripcion : Dejar fuera de AdhesionPA las patentes liberadas.

Firma       : SS_1006_1015_NDR_20121130
Descripcion : No dejar cambiar vehiculo o Tag si el televia esta vencido

Firma       : SS_1098_MDI_20130322
Descripcion : Se cancela deshabilitaci�n de botones 'Eximir' y 'Bonificar' al tratar con un convenio que no pertenece a CN

Firma 		: SS_660_MVI_20130610
Desription 	: Se valida si cliente est� en LA al momento de abrir el formulario, deshabilitando el Check �Adherido PA�
			y agregando un cartel que indica �En Lista Amarilla�.

Firma       : SS-1092-MVI-20130627
Descripcion : Se reemplaza la funci�n que valida el mail de "IsValidEMail" a "IsValidEMailCN"
              Esto para validar "." despu�s del "@" y que existan caracteres entre estos.
              Adem�s se cambia reemplaza la variable MSG_VALIDAR_SINTAXIS_EMAIL por MSG_VALIDAR_DIRECCION_EMAIL

Firma       : SS_660_CQU_20130711
Descripcion : Se alinea el Label de Lista Amarilla y se modifica su texto.
              Se agrega el par�metro CodigoConcesionaria en la funci�n EstaConvenioEnListaAmarilla

Firma       : SS-1092-MVI-20130723
Descripcion : Se sacan las validaciones de correo de un "IF" que solo aplicaba para Costanera Norte.
              As� esta validacion de correos se aplicar� para todas las concesionarias.

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla



Firma       :   SS_660_MVI_20130909
Descripcion :   Se realiza la limpieza del label lista amarilla, dentro de la limpieza de los convenios.
                Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.

Firma		: SS_390_NDR_20131213
Descripcion	: Si el convenio esta tomado, solo puede Reimprimir, Blanquear Clave o Cancelar
Firma       : SS_1178_NDR_20140430
Descripcion : Los inhabilitados en PAK por morosidad, se pueden rehabilitar en forma manual, deschequeando el BIT de inhabilitadoPA


Firma         : SS_1147_NDR_20140515
Descripcion   : Si da de baja la ultima cuenta de un convenio, y este es un convenio ANTIGUO de la concesionaria nativa y no tiene
              nomenclatura CN en el NUMEROCONVENIO (17 caracteres), entonces se da de baja el convenio completo (sin preguntar),
              forzando as� a que si quiere volver a usarlo tenga que crear un convenio nuevo

Firma       : SS_1208_NDR_20140819
Descripcion : Cuando agrega una observacion, llenar la propiedad FechaObservacion en las clases ObservacionDomicilio y ObservacionMedioComunicacion

Author      : Claudio Quezada Ib��ez
Date        : 23-Septiembre-2013
Firma       : SS_1214_CQU_20140917
Description : Se agrega Columna para desplegar la imagen de referencia de una cuenta.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_MCA_20150303
Descripcion : se reemplaza el ancho del txt dado que marcaba una linea blanca cuando se oculta el chk No generar Inteses

Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

Firma       : SS_1147_MCA_20150410
Descripcion : Si la concesionaria es CN entonces los chk de AdheridoPA e InhabilitadoPA se dejan visibles.

Firma       : SS_1419_NDR_20151130
Descripcion : Envia el parametro @CodigoUsario al SP TipoClienteModificarTipo para grabar los nuevos campos de auditoria en la tabla
            Personas y en el HistoricoPersonas

Firma       : TASK_004_ECA_20160411
Descripcion : Se agrega campo para el manejo de el CodigoConvenioFacturacion que
              sera utilizado para la facturacion.
              Se agrega RUT de Facturacion y Tipo de Convenio

Firma       : TASK_005_ECA_20160420
Descripcion : Se incluye las opciones agregar y eliminar vehiculas a las cuentas comerciales

Etiqueta	: 20160603 MGO
Descripci�n	: Se marcan en rojo oscuro las cuentas de convenios for�neos para baja forzada

Etiqueta	: TASK_070_MGO_20160907
Descripci�n	: Alta Forzada

--------------------------------------------------------------------------------}
unit FrmModificacionConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, Dateedit, DB, ADODB, UtilDB, Util,
  DBTables, UtilProc, PeaProcs, Grids, DBGrids, math, FrmCheckListConvenio,
  Peatypes, ComCtrls, frmEliminarTag, DPSControls, DBListEx,
  frmDocPresentadaModificacionConvenio, DBClient, ListBoxEx, frmDatoPesona,
  VariantComboBox, FreDomicilio, FreTelefono, Provider, Menus,frmDocPresentada,
  ConstParametrosGenerales, formReacuperacionTag, FrmEditarDomicilio,
  FrmSeleccionarReimpresionPlantillas, UtilMails, Validate, RStrings,
  frmMediosPagosConvenio, frmFirmarConvenio, frmMedioEnvioDocumentosCobro,
  FrmRepresentantes, frmImprimirConvenio, Convenios, FrmObservacionesGeneral,
  formEliminarCuenta, formSuspensionCuenta, FormConfirmarBajaModifConvenio,
  frmPerdidaTelevia, frmCambiarTag, FormActivarCuenta, ImgList, DMConnection,
  FrmSCDatosVehiculo, frmSeleccionSolicitudDuplicada, Mensajes, ToolWin,
  FrmNoFacturarTransitosDeCuenta, FrmBonificacionFacturacionDeCuenta,
  FrmMsgNormalizar, XMLCAC, ConvenioToXML, FormConfirmarImpresion,
  StrUtils,
  frmFacturacionManual,
  SysUtilsCN,
  PeaProcsCN,
  frmMuestraMensaje, DmiCtrls,	                                                // SS_390_PDO_20120221
  FrmHabilitarCuentasListasAcceso,                                              //SS-1006-NDR-20120614
  RBSetup, Printers,
  ImagenesTransitos,    // SS_1214_CQU_20140917
  frmConsultaAltaPArauco, frmCodigoConvenioFacturacion, BuscaClientes, frmConvenioCuentasRUT,
  frmConsultaAltaOtraConsecionaria,
  frmAltaForzada;                                                               // TASK_070_MGO_20160907


type

  TFormModificacionConvenio = class(TForm)
    Panel1: TPanel;
    pnlDatosContacto: TPanel;
    GBMediosComunicacion: TGroupBox;
    lblEmail: TLabel;
    FreTelefonoPrincipal: TFrameTelefono;
    FreTelefonoSecundario: TFrameTelefono;
    NBook_Email: TNotebook;
    txtEMailContacto: TEdit;
    txtEmailParticular: TEdit;
    GBDomicilios: TGroupBox;
    FMDomicilioPrincipal: TFrameDomicilio;
    GBOtrosDatos: TGroupBox;
    FreDatoPersona: TFreDatoPresona;
    Panel6: TPanel;
    pnl_Botones: TPanel;
    lbl_EstadoSolicitud: TLabel;
    NB_Botones: TNotebook;
    pnl_RepLegal: TPanel;
    lbl_RepresentanteLegal: TLabel;
    gbVehiculos: TGroupBox;
    dblVehiculos: TDBListEx;
    DSVehiculosContacto: TDataSource;
    debuger: TButton;
    InsertarHistoricoConvenio: TADOStoredProc;
    BajaConvenioCompleto: TADOStoredProc;
    BlanquearPasswordPersona: TADOStoredProc;
    Label1: TLabel;
    txt_UbicacionFisicaCarpeta: TEdit;
    cb_Observaciones: TCheckBox;
    cb_RecInfoMail: TCheckBox;
    PopupMenu: TPopupMenu;
    mnu_EnviarMail: TMenuItem;
    lblEnviar: TLabel;
    spCancelarUltimoAnexo: TADOStoredProc;
    cdsVehiculosContacto: TClientDataSet;
    Imagenes: TImageList;
    cbMostrarVehiculosEliminados: TCheckBox;
    pnl_BotonesVehiculo: TPanel;
    btn_DocRequerido: TButton;
    btn_VerObservaciones: TButton;
    btn_RepresentanteLegal: TButton;
    btnAgregarVehiculo: TButton;
    btnEditarVehiculo: TButton;
    btnEliminarVehiculo: TButton;
    btn_Cambiar: TButton;
    btn_CambioVehiculo: TButton;
    btn_Suspender: TButton;
    btn_Perdido: TButton;
    btn_EximirFacturacion: TButton;
    btn_BonificarFacturacion: TButton;
    btn_Salir: TButton;
    btn_ReImprimir: TButton;
    btn_BlanquearClaveConsulta: TButton;
    btn_CancelarConsulta: TButton;
    btn_Guardar: TButton;
    btn_CancelarContrato: TButton;
    btn_Baja_Contrato: TButton;
    btn_ReImprmirModif: TButton;
    btn_blanquearClaveModif: TButton;
    btn_CancelarModificar: TButton;
    btn_GuardarAlta: TButton;
    btn_CancelarContrato_Alta: TButton;
    btn_CancelarGuardar: TButton;
    spConvenio_BloquearConvenio: TADOStoredProc;
    spConvenio_ActualizarGenerarIntereses: TADOStoredProc;
    SPObtenerUltimosConsumos: TADOStoredProc;
    SPAgregarAuditoriaConveniosCAC: TADOStoredProc;
    lnCheck: TImageList;
    cb_RecibeClavePorMail: TCheckBox;
    chkInactivaDomicilioRNUT: TCheckBox;
    chkInactivaMediosComunicacionRNUT: TCheckBox;
    spRegistraAuditoriaDomicilios: TADOStoredProc;
    spRegistraAuditoriaMediosDeContacto: TADOStoredProc;
    spExistenMovimientosFacturacionInmediata: TADOStoredProc;										 //SS-1006-NDR-20120614
    lblEnListaAmarilla: TLabel;
    cb_NoGenerarIntereses: TCheckBox;
    GBFacturacion: TGroupBox;
    lbl1: TLabel;
    lbl_OtroDomicilio: TLabel;
    lbl_PagoAutomatico: TLabel;
    lbl_DescripcionMedioPago: TLabel;
    lbl2: TLabel;
    lblConvenioFacturacion: TLabel;
    lbl4: TLabel;
    lbl_NombreRutFacturacion: TLabel;
    RBDomicilioEntregaOtro: TRadioButton;
    RBDomicilioEntrega: TRadioButton;
    BtnEditarDomicilioEntrega: TButton;
    cb_PagoAutomatico: TVariantComboBox;
    btn_EditarPagoAutomatico: TButton;
    vcbTipoDocumentoElectronico: TVariantComboBox;
    btn_ConvenioFacturacion: TButton;
    txt_NumeroConvenioFacturacion: TEdit;
    btn_MedioDoc: TButton;
    txt_RUTFacturacion: TEdit;
    btn_RUTFacturacion: TButton;
    cbbTipoConvenio: TVariantComboBox;
    lblTipoConvenio: TLabel;
    btnConvFactVehiculo: TButton;
    chkCtaComercialPredeterminada: TCheckBox;
    btnSuscribirLista: TButton;
    btnDesuscribirLista: TButton;
    cbbTipoCliente: TVariantComboBox;
    lblTipoCliente: TLabel;
   // procedure btn_editarClick(Sender: TObject); TASK_056_JMA_20161012 (NO SE USA)
   // procedure btnSalirClick(Sender: TObject);   TASK_056_JMA_20161012 (NO SE USA)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtDocumentoExit(Sender: TObject);
    procedure dblVehiculosDblClick(Sender: TObject);
    procedure txtEmailParticularExit(Sender: TObject);
    procedure txtDocumentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FreTelefonoSecundariotxtTelefonoExit(Sender: TObject);
    procedure RBDomicilioEntregaClick(Sender: TObject);
    procedure BtnEditarDomicilioEntregaClick(Sender: TObject);
    procedure btn_EditarPagoAutomaticoClick(Sender: TObject);
    procedure cb_PagoAutomaticoChange(Sender: TObject);
    procedure btn_GuardarClick(Sender: TObject);
    procedure btn_RepresentanteLegalClick(Sender: TObject);
    procedure btn_DocRequeridoClick(Sender: TObject);
    procedure btn_MedioDocClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
    procedure FreDatoPersonacbPersoneriaChange(Sender: TObject);
    procedure btn_CancelarContratoClick(Sender: TObject);
    procedure cdsVehiculosContactoAfterScroll(DataSet: TDataSet);
    procedure debugerClick(Sender: TObject);
    procedure btn_Baja_ContratoClick(Sender: TObject);
    procedure FreDatoPersonatxtDocumentoKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure btn_ReImprmirModifClick(Sender: TObject);
    procedure btn_BlanquearClaveConsultaClick(Sender: TObject);
    procedure btn_VerObservacionesClick(Sender: TObject);
    procedure cb_ObservacionesClick(Sender: TObject);
    procedure txtEMailContactoChange(Sender: TObject);
    procedure txtEMailContactoKeyPress(Sender: TObject; var Key: Char);
    procedure dblVehiculosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btn_CancelarConsultaClick(Sender: TObject);
    procedure mnu_EnviarMailClick(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
    procedure lblEnviarMouseEnter(Sender: TObject);
    procedure lblEnviarMouseLeave(Sender: TObject);
    procedure lblEnviarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cbMostrarVehiculosEliminadosClick(Sender: TObject);
    procedure dblVehiculosColumnsHeaderClick(Sender: TObject);
    procedure btnAgregarVehiculoClick(Sender: TObject);
    procedure btnEditarVehiculoClick(Sender: TObject);
    procedure btnEliminarVehiculoClick(Sender: TObject);
    procedure btn_CambiarClick(Sender: TObject);
    procedure btn_CambioVehiculoClick(Sender: TObject);
    procedure btn_SuspenderClick(Sender: TObject);
    procedure btn_PerdidoClick(Sender: TObject);
    procedure btn_EximirFacturacionClick(Sender: TObject);
    procedure btn_BonificarFacturacionClick(Sender: TObject);
    procedure vcbTipoDocumentoElectronicoChange(Sender: TObject);
   // procedure cb_AdheridoPAClick(Sender: TObject);                              //SS-1006-NDR-20120614 TASK_004_ECA_20160411
    procedure FormCreate(Sender: TObject);
    procedure OnChangeGenerico(Sender: TObject); // SS-887-PDO-20100715
    procedure chkInactivaMediosComunicacionRNUTClick(Sender: TObject);
    procedure chkInactivaDomicilioRNUTClick(Sender: TObject);
    procedure FreDatoPersonacboTipoClienteCloseUp(Sender: TObject);
    procedure FreDatoPersonacboSemaforo1CloseUp(Sender: TObject);
    procedure FreDatoPersonacboSemaforo3CloseUp(Sender: TObject);
    procedure FMDomicilioPrincipalcb_RegionesChange(Sender: TObject);
    procedure FMDomicilioPrincipalcb_ComunasChange(Sender: TObject);

    //procedure chkConvenioSinCuentaClick(Sender: TObject); //TASK_004_ECA_20160411
    procedure btnAdherirPorCuentaClick(Sender: TObject);                        //SS-1006-NDR-20120614
  //  procedure cb_InhabilitadoPAClick(Sender: TObject);      TASK_056_JMA_20161012 (NO SE USA)
//    procedure cb_MotivosInhabilitacionChange(Sender: TObject); TASK_004_ECA_20160411
    procedure cb_AdheridoPAEnter(Sender: TObject);                  //SS-1006-NDR-20120614
    procedure dblVehiculosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure CargarDatosFacturacion(CodigoConvenio: Integer);                      // TASK_004_ECA_20160411
    procedure CargarTipoConvenios();                                                           // TASK_004_ECA_20160411
    procedure btn_ConvenioFacturacionClick(Sender: TObject);
    procedure btnConvFactVehculoClick(Sender: TObject);
    procedure btn_RUTFacturacionClick(Sender: TObject);                                   // TASK_004_ECA_20160411
    function BuscarNombreRUT(CodigoPersona: Integer): string;
    procedure chkCtaComercialPredeterminadaClick(Sender: TObject);
    procedure btnSuscribirListaClick(Sender: TObject);
    procedure btnDesuscribirListaClick(Sender: TObject);
    procedure cbbTipoClienteChange(Sender: TObject);
  private
    FDatosConvenio: TDatosConvenio;
    // variables de control de accesos
    FDatosConvenioOriginal: TDatosConvenio;
    FPuntoVenta, FPuntoEntrega: Integer;
    FCantidadVehiculoRobados: Integer;
    FConvenioFactory: TConvenioFactory;
    FAdministrador: Boolean;
    DescriError : string;
    POS_Anterior:Integer;
    RegMedioEnvioCorreo:TMedioEnvioDocumentosCobro;
    ModoPantalla: TStateConvenio;
    FNumeroConvenio:String;
    FCargando : Boolean;
    FPrimeraVez: Boolean;
    FEstaConvenioDeBaja: Boolean;
    //FEsConvenioSinCuenta: Boolean;                            //SS_628_ALA_20111202
    //FCambioHabilitadoaAMB: Boolean;                           //SS-1006-NDR-20111105
    //FCambioHabilitadoaPA: Boolean;                            //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
    FCambioAdheridoPA:Boolean;                                  //SS-1006-NDR-20120614 
    FCambioInhabilitado:Boolean;                                //SS-1006-NDR-20120614
    //FValorActualHabilitadoAMB: Boolean;                       //SS-1006-NDR-20111105
    //FValorActualHabilitadoPA: Boolean;                        //SS-1006-NDR-20120614 //SS-1006-NDR-20111105

    FAceptoBorrarEnvios: Boolean;
    FCambioDomicilio,FCambioDomicilioOtro: Boolean;

    FUltimaBusqueda: TBusquedaCliente; //TASK_004_ECA_20160411

    //procedure ArmarListaPatentes(var Cadena: TStringList; sacarPatente: AnsiString = ''; sacarPatenteRVM: AnsiString = '');         TASK_056_JMA_20161012 (NO SE USA)
   // procedure ArmarListaTags(var Cadena: TStringList; sacarTag: AnsiString = '');                                                      TASK_056_JMA_20161012 (NO SE USA)
    //Rev.5 / 09-Junio-2010 / Nelson Droguett Sierra ---------------------------------
    procedure ArmarListaPatentesTags(var CadenaPatentes: TStringList; var CadenaTags: TStringList;  sacarPatente: AnsiString = ''; sacarPatenteRVM: AnsiString = ''; sacarTag: AnsiString = '');
    //Rev.5---------------------------------------------------------------------------
    function  PuedeGuardarGenerico:Boolean;
    function  PuedeGuardarContrato:Boolean;
    procedure HabilitarBotonesVehiculos;
    procedure LimpiarCampos;
    procedure HabilitarControles(habilita: boolean = true);
    procedure CambioPagoAutomatico(MostrarPantallita: boolean = true);
    //procedure IniciarCargaConRut;                                                                                                 TASK_056_JMA_20161012 (NO SE USA)
    procedure SetModoPantalla(Modo: TStateConvenio);
    function DarCaptionMensaje:String;
    function CargarConvenio(NumeroConvenio:String):Boolean;
    procedure CambioDomicilioFacturacion(Sender: TObject);
    procedure CambioMedioPago(Sender: TObject);
    Function  TieneUltimosConsumosSinFacturar (CodigoConvenio : Integer): Boolean;
    procedure ActualizarClases;
    procedure CambioRepLegal;
    procedure ModificarVehiculo(AsignarTag : Boolean);
    procedure SicronizarDoc;
    procedure DispararObservaciones;
    function BloquearConvenio(CodigoConvenio,Bloqueado : integer;Usuario : string;var DescriError : string ):boolean;
    procedure ActualizarConvenioDeBaja;
    procedure AcomodarBotonesVehiculo;
    procedure ChequearRVM(var unVehiculo: TVehiculosConvenio);
//    function CargarComboMotivosInhabilitacion():Boolean;                        //SS-1006-NDR-20120614 //TASK_004_ECA_20160411
   // procedure ImprimirContratoAdhesionPA();                                     //SS-1006-NDR-20120727   TASK_056_JMA_20161012 (NO SE USA)

    procedure CargarConvenioClienteTipo;                                              //TASK_040_ECA_20160628
 protected
    FPuedeDarBaja: Boolean;
    FTipoContacto: AnsiString;
    FCodigoCliente: integer;
    FCodigoPersona: integer;
    FActualizarBusqueda: Boolean;
    FCodigoComunicacion: Integer;
    FFechaHoraInicioComunicacion: TDateTime;
    FIndicePersona: integer;
    FFuente:integer;
    FMsgBoxCaption:string;
    procedure Loaded; override;
 private
    FRNUTUTF8: Integer;
    FPathNovedadXML: string;
    FEsConvenioGuardadoOK,
    FEsConvenioDadoBajaOK,  // SS_964_PDO_20110531
    FSeModificaronDatos: boolean; // SS-887-PDO-20100715
    FCodigoConcesionariaNativa: Integer; //SS_1147_MCA_20140408

    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216

    function RegistrarLogOperacionConvenio(NumeroDocumento: string; CodigoConvenio: integer;
      Patente: string;  IndiceVehiculo: integer; Etiqueta: string; CodigoPuntoEntrega: integer; Usuario,
      Accion: string): boolean;
    procedure ActualizarTipoCliente();
    procedure ActualizaControlesPorModificacion(SeModificaronDatos: Boolean);
    function EsConvenioSinCuenta: Boolean;                                    //SS_628_ALA_20111202
  public
    { Public declarations }
    function Inicializa(MDIChild: Boolean; CaptionForm: AnsiString; NumeroConvenio:String;
      PuntoVenta, PuntoEntrega: Integer; Modo: TStateConvenio = scNormal;
      Fuente:integer = FUENTE_CALLCENTER;
      Administrador:Boolean = False): Boolean;
    property PuntoEntrega: Integer read FPuntoEntrega;
    property PuntoVenta: Integer read FPuntoVenta;
    function ValidarTagsEnArriendo : boolean;
    function EsGiroValido(Giro : string; MostrarMensaje : boolean) : boolean;
    function PuedeGuardarTipoDocumentoElectronico : boolean;
    procedure ActualizarConvenioYCliente;
    procedure ValidaCambioDomicilios;
    procedure ValidarCambioMediosDeContacto;
    function RegistraAuditoriaDomicilio( CodigoConvenio: Integer ; CodigoDomicilio: Integer ; InactivaDomicilioRNUT : Boolean ): boolean;
    function RegistraAuditoriaMedioContacto( CodigoConvenio: Integer ; CodigoMedioContacto: Integer ; InactivaMedioContactoRNUT : Boolean ): boolean;

    procedure OnClickConvenioSelected(NumeroConvenio: String; CodigoConvenio: Integer);  //TASK_004_ECA_20160411
    procedure OnClickVehiculoSelected(NumeroConvenio: String; CodigoConvenio: Integer);  //TASK_004_ECA_20160411

    procedure OnClickVehiculoRUTSelected(CodigoConvenio: Integer; CodigoVehiculo: Integer );  //TASK_005_ECA_20160420
    procedure MostrarVehiculosRUT();                                                          //TASK_005_ECA_20160420
    procedure AgregarVehiculosCuentaComercial(Vehiculo: array of TCuentaVehiculo);                     //TASK_005_ECA_20160420
    procedure EliminarVehiculosCuentaComercial();                                             //TASK_005_ECA_20160420
    procedure GuardarVehiculosCuentaComercial();                                             //TASK_005_ECA_20160420
    procedure ActualizarVehiculosCuentaComercial(CodigoConvenioRUT,CodigoConvenioFacturacion,CodigoVehiculoRUT: Integer; Usuario, Opcion: string);  //TASK_005_ECA_20160420
    //function ValidarEstadoTAGvsMotivo(): Boolean;             //TASK_011_ECA_20160514 // TASK_012_ECA_20160514

    procedure ActualizarPersonaCuentaComercial(CodigoPersona,CuentaComercial: Integer);   //TASK_012_ECA_20160514
    function ValidarCuentaSuscrita(CodigoConvenio: Integer; Patente: string): Boolean;    //TASK_034_ECA_20160611
    Procedure ActualizarCuentaSuscripcion();                                                 //TASK_034_ECA_20160611
    function CuentaAltaSuscripcion(CodigoConvenio: Integer; Patente: string; out Mensaje: string): Boolean;    //TASK_034_ECA_20160611
    function CuentaBajaSuscripcion(CodigoConvenio: Integer; Patente: string; out Mensaje: string): Boolean;    //TASK_034_ECA_20160611
    function ValidarRestriccionesSuscripcionListaBlanca(CodigoPersona: Integer): Boolean;                      //TASK_037_ECA_20160622

  end;

    TNuevoConvenio = class(TFormModificacionConvenio)
    public
        function Inicializa(MDIChild: Boolean; CaptionForm: AnsiString; PuntoVenta, PuntoEntrega: Integer; Fuente:integer = FUENTE_CALLCENTER; Administrador:Boolean = False): Boolean;
    end;

    TConsultaConvenio = class(TFormModificacionConvenio)
    public
		function Inicializa(MDIChild: Boolean; CaptionForm: AnsiString; NumeroConvenio:String; PuntoVenta, PuntoEntrega: Integer; Fuente:integer = FUENTE_CALLCENTER; Administrador:Boolean = False): Boolean;
    end;

var
    FormModificacionConvenio: TFormModificacionConvenio;
    CodigoConvenioFacturacion : Integer;        //TASK_004_ECA_20160411
    CodigoClienteFacturacion  : Integer;            //TASK_004_ECA_20160411
    CodigoTipoConvenio        : Integer;            //TASK_004_ECA_20160411
    CODIGO_TIPO_CONVENIO_RNUT : Integer;             //TASK_004_ECA_20160411
    CODIGO_TIPO_CONVENIO_CTA_COMERCIAL: Integer;    //TASK_004_ECA_20160411
    CODIGO_TIPO_CONVENIO_INFRACTOR: Integer;        //TASK_004_ECA_20160411
    CodigoConvFactVehiculo    : Integer;            //TASK_004_ECA_20160411
    NumeroConvFactVehiculo    : string;            //TASK_004_ECA_20160411
    CodigoConvenioRUT         : Integer;            //TASK_005_ECA_20160420
    CodigoVehiculoRUT         : Integer;            //TASK_005_ECA_20160420
    CuentaComercialPredeterminada : Integer;       //TASK_012_ECA_20160514
    SuscribirVehiculos, DeSuscribirVehiculos: string; //TASK_012_ECA_20160514
    TipoMedioPagoActualCambio: Boolean;
implementation

uses DateUtils, frmReporteContratoAdhesionPA;                //SS-1006-20120727

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}


resourceString
  MSG_ERROR_BLOQUEO_CONVENIO 	= 'No se pudo ejecutar El bloqueo del convenio '+
                                  #13+ 'Consulte al Administrador del Sistema ';
  MSG_GUARDAR_DATOS_CAPTION 	= '&Guardar Datos';
  CAPTION_EDITAR_PAGO			='Editar &pago';
  CAPTION_MEDIO_PAGO			='Editar &Pago';
  CAPTION_MEDIO_PAGO_VER		='Ver &Pago';
  CAPTION_OTRO_DOMICILIO		='Editar &domicilio de facturaci�n';
  CAPTION_OTRO_DOMICILIO_VER	='Ver &domicilio de facturaci�n';
  CAPTION_BTN_SUSPENDER			= '&Suspender';
  CAPTION_BTN_ACTIVAR			= 'Ac&tivar';
  CAPTION_BTN_PERDER			= '&Perdida';
  CAPTION_BTN_RECUPERAR			= '&Recuperar';
  MSG_TAG_SIN_USAR				= 'El cliente posee %d telev�a/s sin uso';

{$REGION 'INICIALIZAR'}


{------------------------------------------------------------------------------
Procedure Name	: FormCreate
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Creacion del Formulario
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.FormCreate(Sender: TObject);
begin
    //FCambioHabilitadoaAMB := False;             //SS-1006-NDR-20111105
    //FCambioHabilitadoaPA := False;              //SS-1006-NDR-20120614
    FCambioAdheridoPA := False;                   //SS-1006-NDR-20120614
    FCambioInhabilitado:=False;                   //SS-1006-NDR-20120614
    FCambioDomicilio := False;
    FCambioDomicilioOtro := False;

end;


{-------------------------------------------------------------------------------
Function Name	: Inicializa
Author 			: Nelson Droguett Sierra
Date Created 	: 19-Abril-2010
Description 	: Inicializar valores necesarios para la ejecucion del formulario.
Parameters 		: MDIChild: Boolean; CaptionForm: AnsiString; NumeroConvenio: String; PuntoVenta, PuntoEntrega: Integer; Modo: TStateConvenio; Fuente: Integer; Administrador:Boolean
Return Value 	: Boolean

Revision 1:
	Date: 16-10-2009
	Author: mpiazza
	Description:  ss-787 Inicializo cb_RecibeClavePorMail

Revision 6:
	Date: 01/07/2010
	Author: pdominguez
	Description: SS 887 - Mejorar Performance - Cuentas Duplicadas
        - Se coloca un bloque Try Finally para cambiar el cursor de la pantalla
        mientras procesa.
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.Inicializa(MDIChild: Boolean;
  CaptionForm: AnsiString; NumeroConvenio: String;
  PuntoVenta, PuntoEntrega: Integer; Modo: TStateConvenio; Fuente: Integer; Administrador:Boolean): Boolean;

    function EstadoConvenio(NumeroConvenio: String): Integer;
    begin
        Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoEstadoConvenio FROM Convenio  WITH (NOLOCK) WHERE NumeroConvenio = ''' + Trim(NumeroConvenio) + '''');
    end;

    //INICIO: TASK_004_ECA_20160411
    function BuscarConvenioFacturacion(NumeroConvenio: String): Integer;
    begin
        Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoConvenioFacturacion FROM Convenio  WITH (NOLOCK) WHERE NumeroConvenio = ''' + Trim(NumeroConvenio) + '''');
    end;

    function BuscarNumeroConvenioFacturacion(CodigoConvenio: Integer): String;
    begin
        Result := QueryGetStringValue(DMConnections.BaseCAC, 'SELECT NumeroConvenio FROM Convenio  WITH (NOLOCK) WHERE CodigoConvenio = ' + IntToStr(CodigoConvenio) );
    end;
    //FIN: TASK_004_ECA_20160411
    //INICIO: TASK_012_ECA_20160514
    function ObtenerCuentaComercialPredetrminada(NumeroDocumento: String): Integer;
    begin
        Result := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT isNull(dbo.ObtenerCtaComercialPredeterminada(''%s''),0)', [NumeroDocumento]));
    end;
 //FIN: TASK_012_ECA_20160514
resourcestring
    MSG_ERROR_MODO_PANTALLA_INDEFINIDO	= 'Modo de pantalla no definido';
    MSG_ESTADO_CONVENIO					= 'Estado de Convenio: ';
    MSG_MODIFICACION_CONVENIO		= 'Modificaci�n de Convenio';
    STR_NRO_CONVENIO					  = ' - Nro de Convenio: ';
    STR_FIRMADO_EN						  = ' Firmado en: ';
    STR_NRO_CONVENIORNUT    			  = ' - Nro de Convenio RNUT: ';
    MSG_ERROR_BLOQUEO_CONVENIO	= 'No se pudo ejecutar El bloqueo del convenio '+ CR + 'Consulte al Administrador del Sistema ';
    MSG_PARAM_XML_NOT_FOUND			= 'Ha ocurrido un error intentando obtener el par�metro %s';
    MSG_PARAM_XML_TITLE					= 'Error obteniendo un par�metro general de la interfaz RNUT-XML';

    SQL_CONVENIOINHABILITADO    = 'SELECT dbo.ConvenioInhabilitadoEnListaAcceso(%d,%d)';              //SS-1006-NDR-20120614
    SQL_CONCESIONARIA           = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()';            //SS-1006-NDR-20120614
    SQL_INHABILITACION_MOROSIDAD= 'SELECT dbo.CONST_CODIGO_MOTIVO_INHABILITACION_POR_MOROSIDAD()';    //SS-1006-NDR-20120614
    //SQLValidarClienteListaAmarilla = 'SELECT 1 FROM Convenio WITH(NOLOCK) WHERE CodigoCliente = %d AND dbo.EstaConvenioEnListaAmarilla(Convenio.CodigoConvenio, GETDATE()) = 1';      // SS_660_CQU_20130711 //SS_660_MVI_20130610
    //SQLValidarClienteListaAmarilla = 'SELECT 1 FROM Convenio WITH(NOLOCK) WHERE CodigoCliente = %d AND dbo.EstaConvenioEnListaAmarilla(Convenio.CodigoConvenio, GETDATE(), NULL) = 1';  //SS_660_MVI_20130729   // SS_660_CQU_20130711
    //SQLValidarClienteListaAmarilla = 'SELECT TOP 1 dbo.ObtenerEstadoConvenioEnListaAmarilla(Convenio.CodigoConvenio) FROM Convenio WITH(NOLOCK) WHERE dbo.ObtenerEstadoConvenioEnListaAmarilla(Convenio.CodigoConvenio) <> ''NULL''  and CodigoCliente = %d  order by Convenio.CodigoConvenio DESC';  //SS_660_MVI_20130909  //SS_660_MVI_20130729
    SQLValidarConvenioEnListaAmarilla = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d)';        //SS_660_MVI_20130909

    CONST_CODIGO_TIPO_CONVENIO_RNUT='SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_RNUT() ';                    //TASK_004_ECA_20160411
    CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL= 'SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL() '; //TASK_004_ECA_20160411
    CONST_CODIGO_TIPO_CONVENIO_INFRACTOR= 'SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_INFRACTOR() ';         //TASK_004_ECA_20160411
Var
    S: TSize;
    ok: boolean;
    i: integer;
    EstadoListaAmarilla : String;                                                                     //SS_660_MVI_20130729
begin
    //INICIO: TASK_012_ECA_20160514
    chkCtaComercialPredeterminada.Visible:=False;
    chkCtaComercialPredeterminada.Checked:=False;
    CuentaComercialPredeterminada:=0;
    //FIN: TASK_012_ECA_20160514

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

      //El habilitado PA parte deshabilitado, el boton por cuenta tambien.
      //EL inhabilitadoPA parte invisible y deshabilitado al igual que el combo
      //cb_HabilitadoPA.Enabled:=False;                                         //SS-1006-NDR-20120614
        ok := true;
    try
        //CargarComboMotivosInhabilitacion;                                       //SS-1006-NDR-20120614 TASK_004_ECA_20160411
        //HabilitarBitsListaDeAcceso(False);                                      //SS-1006-NDR-20120614 //TASK_038_ECA_20160625

        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        Result := False;
        // Si no se puede obtener el par�metro general de la interfaz RNUT no se podr� inicializar el form
        if not ObtenerParametroGeneral(DMConnections.BaseCAC,'PATH_ARCHIVO_SALIDA_XML', FPathNovedadXML) then begin
            MsgBoxErr(MSG_PARAM_XML_TITLE, Format(MSG_PARAM_XML_NOT_FOUND, ['PATH_ARCHIVO_SALIDA_XML']), Caption, MB_ICONERROR);
            Exit;
        end;
        // Si no se puede obtener el par�metro general de la interfaz RNUT de la codificaci�n UTF-8 no se podr� inicializar el form
        if not ObtenerParametroGeneral(DMConnections.BaseCAC,'RNUT_CODIFICAR_UTF8', FRNUTUTF8) then begin
            MsgBoxErr(MSG_PARAM_XML_TITLE, Format(MSG_PARAM_XML_NOT_FOUND, ['RNUT_CODIFICAR_UTF8']), Caption, MB_ICONERROR);
            Exit;
        end;

       
        FEsConvenioGuardadoOK := False;
        FEsConvenioDadoBajaOK := False;     // SS_964_PDO_20110531

        FCodigoConcesionariaNativa:=ObtenerCodigoConcesionariaNativa;			//SS_1147_MCA_20140408

        FCargando := True;
        FEstaConvenioDeBaja := False;
        try
            if MDIChild then begin
                S := GetFormClientSize(Application.MainForm);
                SetBounds(0, 0, S.cx, S.cy);
            end else begin
                FormStyle := fsNormal;
                Visible := False;
            end;
            Update;
            // Si el estado del convenio es Dado de Baja, setear la variable FEstaConvenioDeBaja
            // para operar en forma adecuada, ya que s�lo se puede actualizar un s�lo dato
            // que es el check de No Generar Intereses
            if EstadoConvenio(NumeroConvenio) = ESTADO_CONVENIO_BAJA then begin
                ModoPantalla := scModi;
                FEstaConvenioDeBaja := True;
            end else begin
                ModoPantalla := Modo;
            end;

            vcbTipoDocumentoElectronico.Clear;
            vcbTipoDocumentoElectronico.Items.Add(TDCBX_AUTOMATICO_STR, TDCBX_AUTOMATICO_COD);
            vcbTipoDocumentoElectronico.Items.Add(DescriTipoComprobante(TC_BOLETA), TC_BOLETA);
            vcbTipoDocumentoElectronico.Items.Add(DescriTipoComprobante(TC_FACTURA), TC_FACTURA);
            vcbTipoDocumentoElectronico.ItemIndex := 0;

            FPrimeraVez := True;
            FPuedeDarBaja := True;
            cdsVehiculosContacto.CreateDataSet;
            FreTelefonoPrincipal.Inicializa(STR_TELEFONO_PRINCIPAL, true);
            FreTelefonoSecundario.Inicializa(STR_TELEFONO_ALTERNATIVO, false, false, true);
            FMDomicilioPrincipal.inicializa();
            FreDatoPersona.PermiteNingunSexo := False;
            FAdministrador := Administrador;
            LimpiarCampos;
            FFuente:=Fuente;
            FNumeroConvenio := NumeroConvenio;
            FPuntoVenta := PuntoVenta;
            FPuntoEntrega := PuntoEntrega;
            FCantidadVehiculoRobados := 0;
            FreeAndNil(FDatosConvenio);
            FreeAndNil(FDatosConvenioOriginal);
            FreeAndNil(FConvenioFactory);
            cbMostrarVehiculosEliminados.Checked := False;
            cb_RecibeClavePorMail.Checked := False;
            case ModoPantalla of
                //** -----------------------------------------------------CONSULTA **
                scNormal: begin

                    if not(CargarConvenio(FNumeroConvenio)) then begin
                            result := False;
                            exit;
                    end;

                    if not Assigned(FDatosConvenioOriginal) then FDatosConvenioOriginal := TDatosConvenio.Create(FNumeroConvenio, PuntoVenta, PuntoEntrega);
                end;

                //** ------------------------------------------------MODIFICACION **
                scModi: begin
                    if not(CargarConvenio(FNumeroConvenio)) then begin
                        Result := False;
                        Exit;

                    end else begin
                        if not BloquearConvenio(FDatosConvenio.CodigoConvenio , 1 , UsuarioSistema , DescriError) then begin
                            if  DescriError <> EmptyStr then begin
                                MsgBoxErr( MSG_ERROR_BLOQUEO_CONVENIO , DescriError , Self.Caption , MB_ICONSTOP);
                            end;
                            if not Assigned(FDatosConvenioOriginal) then FDatosConvenioOriginal := TDatosConvenio.Create(FNumeroConvenio, PuntoVenta, PuntoEntrega);  //SS_390_NDR_20131213
                            Result := False;
                            Exit;
                        end;

                    end;
                    if not Assigned(FDatosConvenioOriginal) then FDatosConvenioOriginal := TDatosConvenio.Create(FNumeroConvenio, PuntoVenta, PuntoEntrega);  //SS_390_NDR_20131213
                end;
                //** ------------------------------------------------------ALTA **
                scAlta: begin
                end;
                else raise Exception.Create(MSG_ERROR_MODO_PANTALLA_INDEFINIDO);
            end;
            //chkConvenioSinCuenta.Enabled := ExisteAcceso('BIT_Convenio_Sin_Cuenta');        //SS_628_ALA_20111202           //SS_628_ALA_20111207 //TASK_004_ECA_20160411

            //FreDatoPersona.EsConvenioCN := FDatosConvenio.EsConvenioCN ;		//SS_1147_MCA_20140408
            FreDatoPersona.EsConvenioNativo:= FDatosConvenio.EsConvenioNativo;		//SS_1147_MCA_20140408

           //Caption := CaptionForm + STR_NRO_CONVENIO + FDatosConvenio.NumeroConvenioFormateado + STR_FIRMADO_EN + FDatosConvenio.DescripcionConcesionaria; //TASK_046_ECA_20160702

            FMsgBoxCaption := CaptionForm + ': '+ FDatosConvenio.NumeroConvenioFormateado;
            lbl_EstadoSolicitud.Caption := MSG_ESTADO_CONVENIO + FDatosConvenio.EstadoConvenio;
            SetModoPantalla(ModoPantalla);
            HabilitarBotonesVehiculos;
            if ExisteAcceso('NO_GENERAR_INTERESES') then begin
                cb_NoGenerarIntereses.Visible := True;
                txt_UbicacionFisicaCarpeta.Width := 124;
            end else begin
                cb_NoGenerarIntereses.Visible := False;
                //txt_UbicacionFisicaCarpeta.Width := 248;                      //SS_1147_MCA_20150303
                txt_UbicacionFisicaCarpeta.Width := 160;                        //SS_1147_MCA_20150303
            end;

            //INICIO: TASK_037_ECA_20160622
            if FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE then
            begin
                // Si el usuario log�eado tiene permiso para generar los XML, entonces mostrar el bot�n.
                FreDatoPersona.cboTipoCliente.Enabled :=  ExisteAcceso('combo_CambiarTipoCliente');
                FreDatoPersona.cboSemaforo1.Enabled :=  ExisteAcceso('combo_CambiarTipoCliente');
                FreDatoPersona.cboSemaforo3.Enabled :=  ExisteAcceso('combo_CambiarTipoCliente');
            end;
            //FIN: TASK_037_ECA_20160622

            if FreDatoPersona.cboSemaforo1.Enabled  then begin
                FreDatoPersona.lblSemaforo1.Enabled := true;
                for i := 0 to FreDatoPersona.cboSemaforo1.ControlCount - 1 do
                FreDatoPersona.cboSemaforo1.Controls[i].Enabled := true;
            end;

            if FreDatoPersona.cboSemaforo3.Enabled  then begin
                FreDatoPersona.lblSemaforo3.Enabled := true;
                for i := 0 to FreDatoPersona.cboSemaforo3.ControlCount - 1 do
                  FreDatoPersona.cboSemaforo3.Controls[i].Enabled := true;
            end;

            if FreDatoPersona.cboTipoCliente.Enabled  then begin
                FreDatoPersona.lblTipo.Enabled := true;
                for i := 0 to FreDatoPersona.cboTipoCliente.ControlCount - 1 do
                  FreDatoPersona.cboTipoCliente.Controls[i].Enabled := true;
            end;
            //FEsConvenioSinCuenta := StrToBool(QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.EsConvenioSinCuenta(%d)', [FDatosConvenio.CodigoConvenio])));     //SS_628_ALA_20111202
            //cb_HabilitadoAMB.Enabled := ExisteAcceso('BIT_Habilitado_Lista_Blanca_AMB') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE);    // SS-1006-NDR-20111105
            //HabilitarBitsListaDeAcceso(True);                                   //SS-1006-NDR-20120614  //TASK_038_ECA_20160625

            //INICIO: TASK_038_ECA_20160625                                                           //SS-1006-NDR-20120614
            //chkInactivaDomicilioRNUT.Enabled := not FDatosConvenio.EsConvenioNativo and ExisteAcceso('BIT_Inactiva_RNUT') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); //SS_1147_MCA_20140408
            //chkInactivaMediosComunicacionRNUT.Enabled := not FDatosConvenio.EsConvenioNativo and ExisteAcceso('BIT_Inactiva_RNUT') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); //SS_1147_MCA_20140408
            chkInactivaDomicilioRNUT.Enabled := ExisteAcceso('BIT_Inactiva_RNUT') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); //SS_1147_MCA_20140408
            chkInactivaMediosComunicacionRNUT.Enabled := ExisteAcceso('BIT_Inactiva_RNUT') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); //SS_1147_MCA_20140408
            //FIN: TASK_038_ECA_20160625

			//BEGIN : SS_660_MVI_20130610 ------------------------------------------------------------------------------
            //if (QueryGetIntegerValue(DMConnections.BaseCAC, Format(SQLValidarClienteListaAmarilla, [FDatosConvenio.CodigoPersona])) = 1 )then begin
			//EstadoListaAmarilla := QueryGetValue(DMConnections.BaseCAC, Format(SQLValidarClienteListaAmarilla, [FDatosConvenio.CodigoPersona]));         //SS_660_MVI_20130909 //SS_660_MVI_20130729
			//if (EstadoListaAmarilla <> 'NULL') and (EstadoListaAmarilla <> '')then begin                                                                 //SS_660_MVI_20130909 //SS_660_MVI_20130729
            EstadoListaAmarilla := QueryGetValue(DMConnections.BaseCAC, Format(SQLValidarConvenioEnListaAmarilla, [FDatosConvenio.CodigoConvenio]));     //SS_660_MVI_20130909
            if EstadoListaAmarilla <> '' then begin                                                                                                        //SS_660_MVI_20130909 //SS_660_MVI_20130729
                lblEnListaAmarilla.Visible:=True;
                //cb_AdheridoPA.Enabled := False; //TASK_004_ECA_20160411
                lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
            end else
            begin
                lblEnListaAmarilla.Visible:=False;
                //cb_AdheridoPA.Enabled := True;
                //cb_AdheridoPA.Enabled := (not FEstaConvenioDeBaja);             //SS_1006_NDR_20130822 //TASK_004_ECA_20160411

            end;

			//END : SS_660_MVI_20130610 ------------------------------------------------------------------------------
//            if FCodigoConcesionariaNativa = CODIGO_VS then                      //SS_1147_MCA_20150410     //TASK_004_ECA_20160411
//            begin                                                               //SS_1147_MCA_20150410     //TASK_004_ECA_20160411
//                //cb_AdheridoPA.Visible := False;                                 //SS_1147_MCA_20150410   //TASK_004_ECA_20160411
//                //cb_InhabilitadoPA.Visible := False;                             //SS_1147_MCA_20150410   //TASK_004_ECA_20160411
//                cb_MotivosInhabilitacion.Visible := False;                      //SS_1147_MCA_20150410     //TASK_004_ECA_20160411
//            end;                                                                //SS_1147_MCA_20150410     //TASK_004_ECA_20160411
            //INICIO : TASK_004_ECA_20160411
            CODIGO_TIPO_CONVENIO_RNUT:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_RNUT);
            CODIGO_TIPO_CONVENIO_CTA_COMERCIAL:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL);
            CODIGO_TIPO_CONVENIO_INFRACTOR:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_INFRACTOR);



            CargarTipoConvenios;
            //FIN : TASK_004_ECA_20160411

            //INICIO:TASK_012_ECA_20160514
            //CuentaComercialPredeterminada:=ObtenerCuentaComercialPredetrminada(FreDatoPersona.txtDocumento.Text);  //TASK_038_ECA_20160625
            if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL then
            begin
               CuentaComercialPredeterminada:=ObtenerCuentaComercialPredetrminada(FreDatoPersona.txtDocumento.Text); //TASK_038_ECA_20160625
               chkCtaComercialPredeterminada.Visible:=True;
               chkCtaComercialPredeterminada.Checked:=False;
               chkCtaComercialPredeterminada.Enabled:=True;
               CargarDatosFacturacion(FDatosConvenio.CodigoConvenio);  //JLO   20161003
               if CuentaComercialPredeterminada=FDatosConvenio.CodigoConvenio then
               begin
                    chkCtaComercialPredeterminada.Checked:=True;
                    chkCtaComercialPredeterminada.Enabled:=False;
               end;
            end
            else begin
                if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT then begin

                    CargarDatosFacturacion(FDatosConvenio.CodigoConvenio);
                end;
            end;

            //FIN: TASK_012_ECA_20160514

            //INICIO: TASK_040_ECA_20160628
            if FDatosConvenio.CodigoConcesionaria <> FCodigoConcesionariaNativa then
            begin
                //FreDatoPersona.Enabled:= False;                               //TASK_040_ECA_20160628
                FreDatoPersona.Pc_Datos.Enabled:= False;
                FreDatoPersona.cbPersoneria.Enabled:= False;
                FreDatoPersona.txtDocumento.Enabled:= False;
                FreDatoPersona.txtRazonSocial.Enabled:= False;
                FreDatoPersona.txtGiro.Enabled:= False;
                GBMediosComunicacion.Enabled:=False;
            end;
            //FIN: TASK_040_ECA_20160628

            //INICIO: TASK_034_ECA_20160611
             SuscribirVehiculos:='';
             DeSuscribirVehiculos:='';
            //FIN: TASK_034_ECA_20160611

            if FDatosConvenio.CodigoEstadoConvenio <> ESTADO_CONVENIO_VIGENTE then //TASK_037_ECA_20160622
                btn_RepresentanteLegal.Enabled:=False;                            //TASK_037_ECA_20160622

            //INICIO: TASK_038_ECA_20160625
            if FDatosConvenio.CodigoTipoConvenio=CODIGO_TIPO_CONVENIO_RNUT then
            begin
                chkInactivaDomicilioRNUT.Visible:=True;
                chkInactivaMediosComunicacionRNUT.Visible:=True;
            end;
            //FIN: TASK_038_ECA_20160625

            //INICIO: TASK_040_ECA_20160628
            cbbTipoCliente.Visible:=False;
            lblTipoCliente.Visible:=False;

            if (FDatosConvenio.CodigoTipoConvenio=CODIGO_TIPO_CONVENIO_RNUT) or (FDatosConvenio.CodigoTipoConvenio=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL)  then
            begin
                CargarConvenioClienteTipo;
                cbbTipoCliente.Visible:=True;
                lblTipoCliente.Visible:=True;
            end;
            //FIN: TASK_040_ECA_20160628

            //INICIO: TASK_046_ECA_20160702
            if FDatosConvenio.CodigoTipoConvenio=CODIGO_TIPO_CONVENIO_RNUT then
                Caption := CaptionForm + STR_NRO_CONVENIO + FDatosConvenio.NumeroConvenio + STR_FIRMADO_EN + FDatosConvenio.DescripcionConcesionaria + STR_NRO_CONVENIORNUT + FDatosConvenio.CodigoConvenioExterno
            else
                Caption := CaptionForm + STR_NRO_CONVENIO + FDatosConvenio.NumeroConvenio + STR_FIRMADO_EN + FDatosConvenio.DescripcionConcesionaria;
            //FIN: TASK_046_ECA_20160702

        except
            on e: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_INICIALIZACION, [MSG_MODIFICACION_CONVENIO]), e.Message, CaptionForm, MB_ICONSTOP);
                ok := false;
            end;
        end;
        btn_DocRequerido.Visible:= False;
    finally
        FCargando := False;

        FreDatoPersona.cboTipoCliente.Tag := FreDatoPersona.cboTipoCliente.KeyValue;
        FreDatoPersona.cboSemaforo1.Tag := FreDatoPersona.cboSemaforo1.KeyValue;
        FreDatoPersona.cboSemaforo3.Tag := FreDatoPersona.cboSemaforo3.KeyValue;
        Result := ok;

        if Result then ActualizaControlesPorModificacion(False); // SS-887-PDO-20100715

        Screen.Cursor := crDefault;
        Application.ProcessMessages;

{INICIO: TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)-2}
        if (FDatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_INFRACTOR)
            OR ((FDatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) and (FDatosConvenio.MedioPago.MedioPago.TipoMedioPago = NINGUNO)) then
            btn_ReImprmirModif.Enabled := False;
{TERMINO: TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)-2}

    end;
end;


{-------------------------------------------------------------------------------
Procedure Name	: Inicializa
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
{ TNuevoConvenio }
function TNuevoConvenio.Inicializa(MDIChild: Boolean; CaptionForm: AnsiString; PuntoVenta, PuntoEntrega: Integer; Fuente: integer; Administrador: Boolean): Boolean;
begin
    Result := inherited Inicializa(MDIChild, CaptionForm, '',PuntoVenta, PuntoEntrega, scAlta, Fuente, Administrador);
end;

{-------------------------------------------------------------------------------
Procedure Name	: Inicializa
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
{ TConsultaConvenio }
function TConsultaConvenio.Inicializa(MDIChild: Boolean; CaptionForm: AnsiString; NumeroConvenio:String; PuntoVenta, PuntoEntrega: Integer; Fuente: integer; Administrador: Boolean): Boolean;
begin
    Result := inherited Inicializa(MDIChild, CaptionForm, NumeroConvenio, PuntoVenta, PuntoEntrega, scNormal, Fuente, Administrador);
end;

{-------------------------------------------------------------------------------
Procedure Name  : Loaded
Author          : Nelson Droguett Sierra
Date Created    : 19-Abril-2010
Description     :
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.Loaded;
begin
    inherited;
    Color := $00FFFDFB;
end;

{-------------------------------------------------------------------------------
Procedure Name	: LimpiarCampos
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Limpiar valores en los controles de la interfaz
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.LimpiarCampos;
var
    DatoPersona: TDatosPersonales;
begin
    //variables de control
    POS_Anterior:=-1;
    FreDatoPersona.Panel1.Enabled:=True;
    FreDatoPersona.txtDocumento.Enabled:=True;
    FreDatoPersona.lblRutRun.Enabled:=True;
    FreDatoPersona.Limpiar;
    txtEmailParticular.Clear;
    txtEMailContacto.Clear;
    FreTelefonoPrincipal.CargarTelefono(CODIGO_AREA_SANTIAGO,'',-1);
    FreTelefonoSecundario.CargarTelefono(CODIGO_AREA_SANTIAGO,'',-1);
    FMDomicilioPrincipal.Limpiar;
    FMDomicilioPrincipal.inicializa();
    FreDatoPersona.LimpiarRegistro(DatoPersona);
    FreDatoPersona.RegistrodeDatos:=DatoPersona; // es para que el frame tome estos valores
    lbl_RepresentanteLegal.Caption := SELECCIONAR;
    lbl_DescripcionMedioPago.Caption := SELECCIONAR;
    lbl_DescripcionMedioPago.Hint := '';
    lbl_EstadoSolicitud.Caption := '';
    CargarMediosPagoAutomatico(cb_PagoAutomatico,-1,False);
    CambioMedioPago(self);
    TformMedioEnvioDocumentosCobro.LimpiarRegistro(RegMedioEnvioCorreo);
    btn_EditarPagoAutomatico.Caption := CAPTION_EDITAR_PAGO;
    RBDomicilioEntrega.Checked := true;
    RBDomicilioEntrega.OnClick(RBDomicilioEntrega);
    //INICIO: TASK_004_ECA_20160411
    txt_NumeroConvenioFacturacion.Text:='';
    CodigoConvenioFacturacion:=0;
    lbl_NombreRutFacturacion.Caption:='';
    txt_RUTFacturacion.Text:='';
    //FIN: TASK_004_ECA_20160411
    lbl_OtroDomicilio.Caption:='';
end;


{-------------------------------------------------------------------------------
Procedure Name	: CargarConvenio
 Author 		: dcepeda       (Nelson Droguett Sierra)
 Date Created	: 03-Mayo-2010  (19-Abril-2010)
 Description	: Carga Convenio en pantalla
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.CargarConvenio(NumeroConvenio: String): Boolean;
resourcestring
	MSG_ACTUALIZAR_ERROR		= 'No se pudieron actualizar los datos del contacto.';
	CAPTION_ACTUALIZAR_CONTACTO = 'Actualizar Contacto';
  SQL_CONVENIOINHABILITADO    = 'SELECT dbo.ConvenioInhabilitadoEnListaAcceso(%d,%d)';              //SS-1006-NDR-20120614
  SQL_CONCESIONARIA           = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()';            //SS-1006-NDR-20120614
  SQL_INHABILITACION_MOROSIDAD= 'SELECT dbo.CONST_CODIGO_MOTIVO_INHABILITACION_POR_MOROSIDAD()';    //SS-1006-NDR-20120614
begin
  try
      if not Assigned(FDatosConvenio) then FDatosConvenio := TDatosConvenio.Create(NumeroConvenio, FPuntoVenta, FPuntoEntrega);

{INICIO: TASK_039_JMA_20160708 Revisi�n 2}
        TipoMedioPagoActualCambio := false;
{TERMINO: TASK_039_JMA_20160708}

      FDatosConvenio.Cliente.Domicilios.OnChangeDomicilioFacturacion:=CambioDomicilioFacturacion;
      HabilitarControles(true);
      // datos del cliente
      FreDatoPersona.RegistrodeDatos := FDatosConvenio.Cliente.Datos;

      // Rep Legales
      if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA then CambioRepLegal;

      // medios de comunicacion
      if FDatosConvenio.Cliente.Telefonos.TieneMedioPrincipal then begin
          with FDatosConvenio.Cliente.Telefonos.MedioPrincipal do
              FreTelefonoPrincipal.CargarTelefono(CodigoArea, Valor, CodigoTipoMedioContacto, HoraDesde, HoraHasta);
      end;

      if  FDatosConvenio.Cliente.Telefonos.TieneMedioSecundario then begin
          with FDatosConvenio.Cliente.Telefonos.MedioSecundario do
              FreTelefonoSecundario.CargarTelefono(CodigoArea, Valor, CodigoTipoMedioContacto, HoraDesde, HoraHasta);
      end;

      if FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion > 0 then begin
          if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA then
              txtEMailContacto.Text := FDatosConvenio.Cliente.EMail.MediosComunicacion[0].Valor
          else
              txtEmailParticular.Text := FDatosConvenio.Cliente.EMail.MediosComunicacion[0].Valor;
      end;

      //domicilios
      if FDatosConvenio.Cliente.Domicilios.CantidadDomiciliosPersonas > 0 then
          {INICIO: TASK_053_JMA_20160930 (Asi nos aseguramos que va a cargar el domicilio principal)
          FMDomicilioPrincipal.CargarDatosDomicilio(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal[0]);
          }
          FMDomicilioPrincipal.CargarDatosDomicilio(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal);
          {TERMINO: TASK_053_JMA_20160930 }
      if FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion) > 0 then
          CambioDomicilioFacturacion(RBDomicilioEntregaOtro);

      // medio de pago
      FDatosConvenio.MedioPago.OnCambioMedioPago := CambioMedioPago;
      CargarMediosPagoAutomatico(cb_PagoAutomatico, FDatosConvenio.MedioPago.CodigoMedioPago, False);
      CambioMedioPago(cb_PagoAutomatico);

      // otros datos
      txt_UbicacionFisicaCarpeta.Text       := FDatosConvenio.UbicacionFisicaCarpeta;
      cb_NoGenerarIntereses.Checked         := FDatosConvenio.NoGenerarIntereses;
      cb_Observaciones.Checked              := FDatosConvenio.TieneObservacion;
      btn_VerObservaciones.Enabled          := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or cb_Observaciones.Checked;
      cb_Observaciones.Enabled              := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or (not cb_Observaciones.Checked);
      cb_RecInfoMail.Checked                := FDatosConvenio.RecibirInfoPorMail;
      cb_RecInfoMail.Enabled                := FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion > 0;
      //cb_HabilitadoAMB.OnClick              := Nil;                                      //SS-1006-NDR-20111105
      //cb_HabilitadoAMB.Checked              := FDatosConvenio.HabilitadoAMB;             //SS-1006-NDR-20111105
      //cb_HabilitadoAMB.OnClick              := cb_HabilitadoAMBClick;                    //SS-1006-NDR-20111105
      //cb_AdheridoPA.OnClick              := Nil;                                           //SS-1006-NDR-20100614 //SS-1006-NDR-20111105  TASK_004_ECA_20160411
      //cb_InhabilitadoPA.OnClick          := nil;                                           //SS-10006-NDR-20120614 TASK_004_ECA_20160411
      //cb_MotivosInhabilitacion.OnChange  := nil;                                           //SS-1006-NDR-20100614 TASK_004_ECA_20160411
      //cb_AdheridoPA.Checked              := FDatosConvenio.AdheridoPA;                     //SS-1006-NDR-20100614 //SS-1006-NDR-20111105 TASK_004_ECA_20160411
      
      CodigoConvenioFacturacion             := FDatosConvenio.CodigoConvenioFacturacion;    //TASK_004_ECA_20160411
      CodigoTipoConvenio                    := FDatosConvenio.CodigoTipoConvenio;           //TASK_004_ECA_20160411
      CodigoClienteFacturacion              := FDatosConvenio.CodigoClienteFacturacion;     //TASK_004_ECA_20160411


     { iCodMotivoInhabilitacion:=QueryGetValueInt( DMConnections.BaseCAC,                                //SS-1006-NDR-20100614
                                                  Format( SQL_CONVENIOINHABILITADO,                     //SS-1006-NDR-20100614
                                                           [ FDatosConvenio.CodigoConvenio,             //SS-1006-NDR-20100614
                                                             QueryGetValueInt( DMConnections.BaseCAC,   //SS-1006-NDR-20100614
                                                                               SQL_CONCESIONARIA        //SS-1006-NDR-20100614
                                                                             )                          //SS-1006-NDR-20100614
                                                           ]                                            //SS-1006-NDR-20100614
                                                         )                                              //SS-1006-NDR-20100614
                                                 );     }                                                //SS-1006-NDR-20100614

      //cb_InhabilitadoPA.Checked :=iCodMotivoInhabilitacion>0;                                           //SS-1006-NDR-20100614 TASK_004_ECA_20160411

      //cb_MotivosInhabilitacion.OnChange  := cb_MotivosInhabilitacionChange;                 //SS-1006-NDR-20100614 TASK_004_ECA_20160411
      //cb_InhabilitadoPA.OnClick          := cb_InhabilitadoPAClick;                         //SS-1006-NDR-20100614 TASK_004_ECA_20160411
      //cb_AdheridoPA.OnClick              := cb_AdheridoPAClick;                             //SS-1006-NDR-20100614 //SS-1006-NDR-20111105 TASK_004_ECA_20160411

      //chkConvenioSinCuenta.Checked         := FDatosConvenio.SinCuenta;                    //SS_628_ALA_20111202 //TASK_004_ECA_20160411
      vcbTipoDocumentoElectronico.ItemIndex := vcbTipoDocumentoElectronico.Items.IndexOfValue(FDatosConvenio.TipoComprobanteFiscal);
      if vcbTipoDocumentoElectronico.ItemIndex < 0 then vcbTipoDocumentoElectronico.ItemIndex := 0;

      cb_RecibeClavePorMail.Checked := FDatosConvenio.Cliente.RecibeClavePorMail;
 	    chkInactivaDomicilioRNUT.Checked           := FDatosConvenio.Cliente.InactivaDomicilioRNUT; //REV.7}
   	  chkInactivaMediosComunicacionRNUT.Checked           := FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT; //REV.7
      // vehiculos
      //Rev.5 / 09-Junio-2010-----------------------------------------------------
      try
        DSVehiculosContacto.DataSet := Nil;
        cdsVehiculosContacto.DisableControls;
        FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
      finally
        cdsVehiculosContacto.EnableControls;
        DSVehiculosContacto.DataSet := cdsVehiculosContacto;
      end;
      //Rev.5---------------------------------------------------------------------
      FAceptoBorrarEnvios := False;
      Result := True;
  except
      on E: Exception do begin
          MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, CAPTION_ACTUALIZAR_CONTACTO, MB_ICONSTOP);
          FDatosConvenio.Free;
          HabilitarControles(false);
          result := False;
          exit;
      end;
  end;
end;

//Rev.5 / 09-Junio-2010 / Nelson Droguett Sierra ---------------------------------
{-------------------------------------------------------------------------------
Procedure Name	: ArmarListaPatentesTags
 Author 		: Nelson Droguett Sierra
 Date Created	: 09-Junio-2010
 Description	: Arma una lista de Patentes y una de Tags del convenio
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.ArmarListaPatentesTags(var CadenaPatentes, CadenaTags: TStringList; sacarPatente, sacarPatenteRVM, sacarTag: AnsiString);
var
    NroTag: string;
    Book: TBookmark;
begin
    Book:= nil;
      try
        DSVehiculosContacto.DataSet := Nil;
        cdsVehiculosContacto.DisableControls;
        if not (cdsVehiculosContacto.IsEmpty) then begin
            book := cdsVehiculosContacto.GetBookmark;
            cdsVehiculosContacto.First;
            while not cdsVehiculosContacto.Eof do begin
                if not cdsVehiculosContacto.FieldByName('VehiculoEliminado').Value then begin
                    if (cdsVehiculosContacto.fieldbyname('ContractSerialNumber').IsNull) then
                        NroTag := ''
                    else
                        NroTag := cdsVehiculosContacto.fieldbyname('ContractSerialNumber').Value;

                    if (trim(NroTag)<>'') and (sacarTag <> NroTag) then begin
                        CadenaTags.Add(cdsVehiculosContacto.FieldByName('ContractSerialNumber').Value);
                    end;

                    if sacarPatente <> trim(cdsVehiculosContacto.FieldByName('Patente').Value) then begin
                        CadenaPatentes.Add(cdsVehiculosContacto.FieldByName('Patente').Value);
                    end;

                    if sacarPatenteRVM <> trim(cdsVehiculosContacto.FieldByName('PatenteRVM').Value) then begin
                        CadenaPatentes.Add(cdsVehiculosContacto.FieldByName('PatenteRVM').Value);
                    end;
                end;
                cdsVehiculosContacto.Next;
            end;
        end;
      finally
        cdsVehiculosContacto.GotoBookmark(book);
        cdsVehiculosContacto.FreeBookmark(book);
        cdsVehiculosContacto.EnableControls;
        DSVehiculosContacto.DataSet := cdsVehiculosContacto;
      end;
end;
//FinRev.5


{-------------------------------------------------------------------------------
Procedure Name	: btn_CancelarContratoClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Boton que cancela Contrato.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_CancelarContratoClick(Sender: TObject);
begin
    if FormStyle = fsNormal then ModalResult := mrCancel
    else close;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_CancelarConsultaClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_CancelarConsultaClick(Sender: TObject);
begin
    //FCambioHabilitadoaAMB := False;                             //SS-1006-NDR-20111105
    //FCambioHabilitadoaPA := False;                              //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
    FCambioAdheridoPA := False;                                   //SS-1006-NDR-20120614
    Close;
end;

{Procedure Name	: SetModoPantalla
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Determina la visibilidad o disponibilidd de los botones dependiendo
 					de si es consulta o modificacion de datos. 
 Parameters		:
}
procedure TFormModificacionConvenio.SetModoPantalla(Modo: TStateConvenio);

    {--------------------------------------------------------------------------------
    Procedure Name	: SetConsulta
     Author 		: Nelson Droguett Sierra
     Date Created	: 19-Abril-2010
     Description	: Configura la visibilidad de los botones, para consulta.
    --------------------------------------------------------------------------------}
	procedure SetConsulta;
    begin
      NB_Botones.PageIndex:=0; //pageSalir
      HabilitarControles(False);
      btn_DocRequerido.Enabled 				:= true;
      btn_MedioDoc.Enabled 					:= True;
      BtnEditarDomicilioEntrega.Enabled		:=RBDomicilioEntregaOtro.Checked;
      BtnEditarDomicilioEntrega.caption 	:= CAPTION_OTRO_DOMICILIO_VER;
      btn_EditarPagoAutomatico.Enabled 		:= (FDatosConvenio.MedioPago.MedioPago.TipoMedioPago=PAT) or (FDatosConvenio.MedioPago.MedioPago.TipoMedioPago=PAC);
      btn_EditarPagoAutomatico.Caption 		:= CAPTION_MEDIO_PAGO_VER;
      btn_VerObservaciones.Enabled 			:=  (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or (FDatosConvenio.ListObservacionConvenio.TieneObservaciones);
      btn_RepresentanteLegal.Enabled 		:= FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA;
      FreDatoPersona.cbPersoneria.Enabled 	:= false;
      FreDatoPersona.txtDocumento.ReadOnly 	:= true;
      btnEditarVehiculo.Visible           	:= false;
      btnAgregarVehiculo.Visible          	:= false;
      btnEliminarVehiculo.Visible         	:= false;
      btn_Cambiar.Visible                 	:= False;
      btn_CambioVehiculo.Visible          	:= False;
      btn_Suspender.Visible               	:= False;
      btn_Perdido.Visible                 	:= False;
      btn_EximirFacturacion.Visible       	:= False;
      btn_BonificarFacturacion.Visible    	:= False;
      AcomodarBotonesVehiculo;
      gbVehiculos.Enabled                 	:= True;
      dblVehiculos.Enabled                	:= True;
      btn_MedioDoc.Enabled                	:= False;
      btn_DocRequerido.Enabled            	:= False;
      btn_VerObservaciones.Enabled        	:= (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or  False;
      btn_EditarPagoAutomatico.Enabled    	:= False;
      btn_BlanquearClaveConsulta.Visible 	:= ExisteAcceso('boton_blanquear_clave');
      btn_ReImprimir.Visible      			:= ExisteAcceso('boton_reimprimir');
      btnConvFactVehiculo.Visible           := False;
      btn_ConvenioFacturacion.Enabled    	:= False;
      btn_RUTFacturacion.Enabled    	    := False;
      chkCtaComercialPredeterminada.Enabled := False;
      GBFacturacion.Enabled    	            := False;
      cbbTipoCliente.Enabled  	            := False; //TASK_040_ECA_20160628
    end;

resourcestring
	MSG_ERROR_MODO_PANTALLA = 'ERROR al setear el modo de pantalla. No se carg� el convenio';
begin
    EnableControlsInContainer(NB_Botones,true);
    //btn_Baja_Contrato.Enabled := (not Assigned(FDatosConvenio) or (FDatosConvenio.CodigoConcesionaria = CODIGO_CN));								//SS_1147_MCA_20140408
    btn_Baja_Contrato.Enabled := (not Assigned(FDatosConvenio) or (FDatosConvenio.CodigoConcesionaria = FCodigoConcesionariaNativa));			//SS_1147_MCA_20140408
    ModoPantalla := Modo;
    btn_MedioDoc.Visible := true;
    btn_DocRequerido.Visible := true;
    case Modo of
        scNormal: begin //Esto es consulta
            if not Assigned(FDatosConvenio) then begin
                raise Exception.Create(MSG_ERROR_MODO_PANTALLA);
                exit;
            end;
            SetConsulta;
        end;
        scModi: begin
            NB_Botones.PageIndex := 1; // convenios Modif
            btn_EditarPagoAutomatico.Caption    := CAPTION_MEDIO_PAGO;
            BtnEditarDomicilioEntrega.Caption   := CAPTION_OTRO_DOMICILIO;
            btn_RepresentanteLegal.Enabled      := FreDatoPersona.RegistrodeDatos.Personeria = PERSONERIA_JURIDICA;
            btnAgregarVehiculo.Visible          := ExisteAcceso('boton_agregar');
            btnEditarVehiculo.Visible           := ExisteAcceso('boton_editar');
            btnEliminarVehiculo.Visible         := ExisteAcceso('boton_eliminar');
            btn_Cambiar.Visible                 := ExisteAcceso('boton_cambiar_tag');
            btn_CambioVehiculo.Visible          := ExisteAcceso('boton_cambiar_vehiculo');
            btn_Suspender.Visible               := False; //ExisteAcceso('boton_suspender'); //TASK_041_ECA_20160629
            btn_Perdido.Visible                 := ExisteAcceso('boton_perdido');
            btn_EximirFacturacion.Visible       := ExisteAcceso('boton_EximirFacturacion');
            btn_BonificarFacturacion.Visible    := ExisteAcceso('boton_BonificarFacturacion');
            AcomodarBotonesVehiculo;
            btn_blanquearClaveModif.Visible     := ExisteAcceso('boton_blanquear_clave');
            btn_ReImprmirModif.Visible          := ExisteAcceso('boton_reimprimir'); // and (not FSeModificaronDatos) and (Assigned(FDatosConvenio)) and (FDatosConvenio.EsConvenioCN);  // SS_960_PDO_20120115
            btn_Baja_Contrato.Visible           := ExisteAcceso('boton_baja_convenio');
            //btn_Baja_Contrato.Enabled           := Assigned(FDatosConvenio) and FDatosConvenio.EsConvenioCN;		//SS_1147_MCA_20140408
            btn_Baja_Contrato.Enabled           := Assigned(FDatosConvenio) and FDatosConvenio.EsConvenioNativo;	//SS_1147_MCA_20140408
            btn_MedioDoc.Enabled                := True;
            HabilitarControles(true);

            if FEstaConvenioDeBaja then begin
                // Se inhabilitan todos los controles menos el checkbox
                // de No Generar Intereses, que es el �nico dato que se
                // permite editar de un Convenio Dado de Baja
                SetConsulta;
                NB_Botones.PageIndex := 1;
                cb_NoGenerarIntereses.Enabled := True;
            end;

        end;
        scAlta: begin
            raise Exception.Create('Funcionalidad no Implementada');
        end;
        scSuspendido: begin
            SetConsulta;
        end;
    end;
end;

{$ENDREGION}

{$REGION 'DATOS PERSONALES'}


{------------------------------------------------------------------------------
Procedure Name	: txtDocumentoKeyDown
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.txtDocumentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (Shift = []) and (Key = VK_ESCAPE) then free;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_RepresentanteLegalClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Accion al hacer click en el boton del representante legal.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_RepresentanteLegalClick(Sender: TObject);
var
   f:TFormRepresentantes;
   RepresentateLegal1, RepresentateLegal2, RepresentateLegal3: TDatosPersonales;
resourcestring
	MSG_ERROR_CONVENIO_NO_JURIDICO = 'El convenio no es juridico';
begin
    if not(FDatosConvenio.Cliente is TPersonaJuridica) then begin
        raise Exception.Create(MSG_ERROR_CONVENIO_NO_JURIDICO);
        exit;
    end;

    if TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes > 0 then
        RepresentateLegal1 := TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[0].Datos
    else
        TFormRepresentantes.Limpiar(RepresentateLegal1);

    if TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes > 1 then
        RepresentateLegal2 := TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[1].Datos
    else
        TFormRepresentantes.Limpiar(RepresentateLegal2);

    if TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes > 2 then
        RepresentateLegal3 := TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[2].Datos
    else
        TFormRepresentantes.Limpiar(RepresentateLegal3);

    Application.CreateForm(TFormRepresentantes,f);
    try
        f.inicializar(LimpiarCaracteresLabel(btn_RepresentanteLegal.Caption),
                RepresentateLegal1, RepresentateLegal2, RepresentateLegal3,
                FDatosConvenio.Cliente.Datos.NumeroDocumento,
                //FDatosConvenio.EsConvenioCN,
                FDatosConvenio.EsConvenioNativo,
                //TStateConvenio(iif(FDatosConvenio.EsConvenioCN,
                TStateConvenio(iif(FDatosConvenio.EsConvenioNativo,
                                    iif(ExisteAcceso('datos_cliente'),
                                        Integer(ModoPantalla),
                                        Integer(scNormal)),
                                    Integer(scNormal))),
                True);
        if f.ShowModal = mrok then begin

            with TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales do begin
                if ((RepresentateLegal1.NumeroDocumento <> f.RepresentanteUno.NumeroDocumento) and
                   (RepresentateLegal1.NumeroDocumento <> f.RepresentanteDos.NumeroDocumento) and
                   (RepresentateLegal1.NumeroDocumento <> f.RepresentanteTres.NumeroDocumento)) or
                   (ObtenerIndice(RepresentateLegal1.TipoDocumento, RepresentateLegal1.NumeroDocumento) <>
                    ObtenerIndice(f.RepresentanteUno.TipoDocumento, f.RepresentanteUno.NumeroDocumento)) then
                    QuitarRepresentante(RepresentateLegal1.TipoDocumento, RepresentateLegal1.NumeroDocumento);

                if (RepresentateLegal2.NumeroDocumento <> f.RepresentanteUno.NumeroDocumento) and
                   (RepresentateLegal2.NumeroDocumento <> f.RepresentanteDos.NumeroDocumento) and
                   (RepresentateLegal2.NumeroDocumento <> f.RepresentanteTres.NumeroDocumento) or
                   (ObtenerIndice(RepresentateLegal2.TipoDocumento, RepresentateLegal2.NumeroDocumento) <>
                    ObtenerIndice(f.RepresentanteDos.TipoDocumento, f.RepresentanteDos.NumeroDocumento)) then
                    QuitarRepresentante(RepresentateLegal2.TipoDocumento, RepresentateLegal2.NumeroDocumento);

                if (RepresentateLegal3.NumeroDocumento <> f.RepresentanteUno.NumeroDocumento) and
                   (RepresentateLegal3.NumeroDocumento <> f.RepresentanteDos.NumeroDocumento) and
                   (RepresentateLegal3.NumeroDocumento <> f.RepresentanteTres.NumeroDocumento) or
                   (ObtenerIndice(RepresentateLegal3.TipoDocumento, RepresentateLegal3.NumeroDocumento) <>
                    ObtenerIndice(f.RepresentanteTres.TipoDocumento, f.RepresentanteTres.NumeroDocumento)) then
                    QuitarRepresentante(RepresentateLegal3.TipoDocumento, RepresentateLegal3.NumeroDocumento);

                if f.RepresentanteUno.NumeroDocumento <> '' then begin
                    if ObtenerIndice(f.RepresentanteUno.TipoDocumento, f.RepresentanteUno.NumeroDocumento) >= 0 then

                        ModificarRepresentante(f.RepresentanteUno.TipoDocumento, f.RepresentanteUno.NumeroDocumento, f.RepresentanteUno, 0)
                    else
                        AgregarRepresentante(f.RepresentanteUno, 0);
                end;

                if f.RepresentanteDos.NumeroDocumento <> '' then begin
                    if ObtenerIndice(f.RepresentanteDos.TipoDocumento, f.RepresentanteDos.NumeroDocumento) >= 0 then
                        ModificarRepresentante(f.RepresentanteDos.TipoDocumento, f.RepresentanteDos.NumeroDocumento, f.RepresentanteDos, 1)
                    else
                        AgregarRepresentante(f.RepresentanteDos, 1);
                end;

                if f.RepresentanteTres.NumeroDocumento <> '' then begin
                    if ObtenerIndice(f.RepresentanteTres.TipoDocumento, f.RepresentanteTres.NumeroDocumento) >= 0 then
                        ModificarRepresentante(f.RepresentanteTres.TipoDocumento, f.RepresentanteTres.NumeroDocumento, f.RepresentanteTres, 2)
                    else
                        AgregarRepresentante(f.RepresentanteTres, 2);
                end;
            end;

            CambioRepLegal;
            ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715            
        end;
    except
        on E:Exception do MsgBoxErr(MSG_ERROR_GUARDAR_REP_LEGAL,e.Message,FMsgBoxCaption,MB_ICONSTOP);
    end;
    f.free;
end;

{-------------------------------------------------------------------------------
Procedure Name	: txtDocumentoExit
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Accion al salir del txtDocumento
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.txtDocumentoExit(Sender: TObject);
begin
    // Verifico si el contacto ya existe
	if btn_CancelarContrato.Focused or btn_Salir.Focused then exit;
end;

{-------------------------------------------------------------------------------
Procedure Name	: FreDatoPersonacbPersoneriaChange
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.FreDatoPersonacboSemaforo1CloseUp(Sender: TObject);
begin
  FreDatoPersona.cboSemaforo1CloseUp(Sender);
   if FreDatoPersona.cboSemaforo1.Tag <> FreDatoPersona.cboSemaforo1.KeyValue then
        ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
end;

procedure TFormModificacionConvenio.FreDatoPersonacboSemaforo3CloseUp(Sender: TObject);
begin
  FreDatoPersona.cboSemaforo3CloseUp(Sender);
  if FreDatoPersona.cboSemaforo3.Tag <> FreDatoPersona.cboSemaforo3.KeyValue then
        ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
end;

procedure TFormModificacionConvenio.FreDatoPersonacboTipoClienteCloseUp(Sender: TObject);
begin
    FreDatoPersona.cboTipoClienteCloseUp(Sender);
    if FreDatoPersona.cboTipoCliente.Tag <> FreDatoPersona.cboTipoCliente.KeyValue then
        ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
end;

procedure TFormModificacionConvenio.FreDatoPersonacbPersoneriaChange(Sender: TObject);
resourcestring
    CAPTION_DOMICILIO_PARTICULAR = 'Domicilio Particular';
    CAPTION_DOMICILIO_COMERCIAL  = 'Domicilio Comercial';
begin
    FreDatoPersona.cbPersoneriaChange(Sender);
    if FreDatoPersona.Personeria=PERSONERIA_JURIDICA then begin
        NBook_Email.PageIndex:=0;
        GBDomicilios.Caption:=CAPTION_DOMICILIO_COMERCIAL;
        pnl_RepLegal.Visible:=True;
    end else begin
        NBook_Email.PageIndex:=1;
        GBDomicilios.Caption:=CAPTION_DOMICILIO_PARTICULAR;
        pnl_RepLegal.Visible:=False;
    end;
    ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715    
end;


{-------------------------------------------------------------------------------
Procedure Name	: FreDatoPersonatxtDocumentoKeyDown
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
-------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.FreDatoPersonatxtDocumentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (Shift = []) and (Key = VK_ESCAPE) then close;
end;


{-------------------------------------------------------------------------------
Procedure Name	: CambioRepLegal
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.CambioRepLegal;
resourcestring
	STR_Y_OTROS = ' y otro/s';
begin
    if not Assigned(FDatosConvenio) or
        not(FDatosConvenio.Cliente is TPersonaJuridica) or
        (TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes = 0) then
			lbl_RepresentanteLegal.Caption := SELECCIONAR
    else begin
		with TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[0].Datos do
            lbl_RepresentanteLegal.Caption := ArmarNombrePersona(PERSONERIA_FISICA,nombre,apellido,ApellidoMaterno) + iif(TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes > 1 ,STR_Y_OTROS,'');
    end;
end;

{$ENDREGION}

{$REGION 'MEDIOS COMUNICACION'}

{-------------------------------------------------------------------------------
Procedure Name	: txtEmailParticularExit
 Author 		: mpiazza
 Date Created	: 22/04/2010
 Description	: Valida el email
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.txtEmailParticularExit(Sender: TObject);
begin
    if (trim(txtEmailParticular.Text) <> '') and (not IsValidEmail(trim(txtEmailParticular.Text))) then begin
		MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, txtEmailParticular);
        txtEmailParticular.SetFocus;
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: FreTelefonoSecundariotxtTelefonoExit
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.FreTelefonoSecundariotxtTelefonoExit(Sender: TObject);
begin
    if trim(FreTelefonoSecundario.txtTelefono.Text) <> '' then
        FreTelefonoSecundario.TelefonoValido;
end;


{-------------------------------------------------------------------------------
Procedure Name	: txtEMailContactoChange
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.txtEMailContactoChange(Sender: TObject);
begin
    cb_RecInfoMail.Enabled := Trim(TEdit(Sender).Text) <> '';
    if not cb_RecInfoMail.Enabled then cb_RecInfoMail.Checked := False;

    ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
end;

{-------------------------------------------------------------------------------
Procedure Name	: txtEMailContactoKeyPress
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.txtEMailContactoKeyPress(Sender: TObject; var Key: Char);
begin
    if key in ['}',''''] then Key := #0;
end;


{$ENDREGION}

{$REGION 'DATOS FACTURACION'}

{-------------------------------------------------------------------------------
Procedure Name	: btn_MedioDocClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Accion al hacer click al boton MedioDoc
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_MedioDocClick(Sender: TObject);
var
    f: TformMedioEnvioDocumentosCobro;
    aux: TMedioEnvioDocumentosCobro;
    i: Integer;
begin
    if ModoPantalla <> scNormal then ActualizarClases;

    Application.CreateForm(TformMedioEnvioDocumentosCobro, f);

    SetLength(aux, FDatosConvenio.MediosEnvioPago.CantidadMediosEnvio);
    for i := 0 to FDatosConvenio.MediosEnvioPago.CantidadMediosEnvio - 1 do
        aux[i] := FDatosConvenio.MediosEnvioPago.MediosEnvio[i];

    if f.Inicializar(aux, (FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion = 1),
            FDatosConvenio.CantidadVehiculos, (TStateConvenio(iif(ExisteAcceso('datos_otros'), Integer(ModoPantalla), Integer(scNormal))))) and
            (f.ShowModal = mrok) then begin
        for i := 0 to FDatosConvenio.MediosEnvioPago.CantidadMediosEnvio - 1 do
            FDatosConvenio.MediosEnvioPago.QuitarMedioEnvioDocumentosConvenio(0);

        for i := 0 to Length(f.RegistroMedioEvioDoc) - 1 do
            FDatosConvenio.MediosEnvioPago.AgregarMedioEnvioDocumentosConvenio(f.RegistroMedioEvioDoc[i]);

        ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
    end;
    f.Release;
end;


{-------------------------------------------------------------------------------
Procedure Name	: RBDomicilioEntregaOtroClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.RBDomicilioEntregaClick(Sender: TObject);
begin
    if (ActiveControl = Sender) then
    begin
        BtnEditarDomicilioEntrega.Enabled := (RBDomicilioEntregaOtro.Checked) and (ModoPantalla <> scNormal);

        ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715

        if RBDomicilioEntrega.Checked then
        begin
            // elimino el domicilio secudnario
            if Assigned(FDatosConvenio) and (FDatosConvenio.Cliente.Domicilios.CantidadDomiciliosPersonas > 1) then // por las dudas prgunto que tenga mas de un domicilio
                FDatosConvenio.Cliente.Domicilios.QuitarDomicilio(FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion));
            lbl_OtroDomicilio.Hint := '';
            lbl_OtroDomicilio.Caption:='';
        end
        else
                BtnEditarDomicilioEntrega.Click;
    end;

end;


{-------------------------------------------------------------------------------
Procedure Name	: BtnEditarDomicilioEntregaClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.BtnEditarDomicilioEntregaClick(Sender: TObject);
var
    f: TFormEditarDomicilio;
    AuxDomicilios: TDatosDomicilio;
begin

    Application.CreateForm(TFormEditarDomicilio, f);
    if (FDatosConvenio.Cliente.Domicilios.CantidadDomiciliosPersonas > 1) and
        (FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion) > 0) and
        (FDatosConvenio.CodigoDomicilioFacturacion <> FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoDomicilio)	//TASK_076_MGO_20161003
    then
            AuxDomicilios := FDatosConvenio.Cliente.Domicilios.Domicilios[FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion)];

    if f.Inicializar(
        LimpiarCaracteresLabel(BtnEditarDomicilioEntrega.Caption),
        AuxDomicilios,
        1, false, False,
        //TStateConvenio(iif((FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaDomicilioRNUT or FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT))		//SS_1147_MCA_20140408
        TStateConvenio(iif((FDatosConvenio.EsConvenioNativo or (FDatosConvenio.Cliente.InactivaDomicilioRNUT or FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT))		//SS_1147_MCA_20140408
        and ExisteAcceso('datos_domicilios'), Integer(ModoPantalla), Integer(scNormal))))
        and (f.ShowModal = mrOK) then begin
            FDatosConvenio.CodigoDomicilioFacturacion := f.RegistroDomicilio.CodigoDomicilio;
            if FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion) > 0 then
                 FDatosConvenio.Cliente.Domicilios.EditarDomicilio(FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion) ,f.RegistroDomicilio)
            else
                FDatosConvenio.Cliente.Domicilios.AgregarDomicilio(f.RegistroDomicilio);

            FDatosConvenio.CodigoDomicilioFacturacion := f.RegistroDomicilio.CodigoDomicilio;
            ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715            
        end else CambioDomicilioFacturacion(self); // para que actualiza los datos en pantalle
    f.Release;
end;

{------------------------------------------------------------------------------
Procedure Name	: btn_EditarPagoAutomaticoClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
-------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_EditarPagoAutomaticoClick(Sender: TObject);
var
    f: TFormMediosPagosConvenio;
    Indice, CodigoArea: Integer;
    Valor: String;
    i: Integer;
begin
    if (ModoPantalla <> scNormal) then ActualizarClases;

    Application.CreateForm(TFormMediosPagosConvenio, f);
    if FDatosConvenio.Cliente.Telefonos.CantidadMediosComunicacion > 0 then begin
         CodigoArea := FDatosConvenio.Cliente.Telefonos.MediosComunicacion[0].CodigoArea;
         Indice     := FDatosConvenio.Cliente.Telefonos.MediosComunicacion[0].Indice;
         Valor      := FDatosConvenio.Cliente.Telefonos.MediosComunicacion[0].Valor;
    end else begin
        CodigoArea  := 0;
        Indice      := -1;
        Valor       := EmptyStr;
    end;
    if f.Inicializar(LimpiarCaracteresLabel(btn_EditarPagoAutomatico.Caption),
                FDatosConvenio.MedioPago.MedioPago,
                FDatosConvenio.MedioPago.Mandante.Datos,
                FDatosConvenio.Cliente.Datos,
                FDatosConvenio.MedioPago.OrigenMandato,
                TStateConvenio(iif(ExisteAcceso('datos_medio_pago'), Integer(ModoPantalla), Integer(scNormal))),
                CodigoArea,
                Valor,
                False,
                True,
                Indice,
                FDatosConvenio.CodigoConvenio,
                FDatosConvenio.MedioPago.ForzarConfirmado)
            and (f.ShowModal = mrOk) then begin

{INICIO: TASK_039_JMA_20160708 Revisi�n 2}
       TipoMedioPagoActualCambio := (FDatosConvenio.MedioPago.MedioPago.TipoMedioPago <> f.RegistroMedioPago.TipoMedioPago) OR
                                    (FDatosConvenio.MedioPago.Mandante.CodigoPersona <> f.RegistroPersonaMandante.CodigoPersona) OR
                                    TipoMedioPagoActualCambio;

       if (FDatosConvenio.MedioPago.MedioPago.TipoMedioPago = f.RegistroMedioPago.TipoMedioPago) AND NOT TipoMedioPagoActualCambio then
        begin
            if f.RegistroMedioPago.TipoMedioPago = PAT then
            begin
                 TipoMedioPagoActualCambio := (FDatosConvenio.MedioPago.MedioPago.PAT.EmisorTarjetaCredito <> f.RegistroMedioPago.PAT.EmisorTarjetaCredito) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAT.NroTarjeta <> f.RegistroMedioPago.PAT.NroTarjeta) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAT.FechaVencimiento <> f.RegistroMedioPago.PAT.FechaVencimiento) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAT.Confirmado <> f.RegistroMedioPago.PAT.Confirmado) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAT.Cuotas <> f.RegistroMedioPago.PAT.Cuotas) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAT.TipoTarjeta <> f.RegistroMedioPago.PAT.TipoTarjeta) OR
                                    TipoMedioPagoActualCambio;
            end;

            if f.RegistroMedioPago.TipoMedioPago = PAC then
            begin
                 TipoMedioPagoActualCambio :=(FDatosConvenio.MedioPago.MedioPago.PAC.CodigoBanco <> f.RegistroMedioPago.PAC.CodigoBanco) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAC.NroCuentaBancaria <> f.RegistroMedioPago.PAC.NroCuentaBancaria) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAC.Sucursal <> f.RegistroMedioPago.PAC.Sucursal) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAC.TipoCuenta <> f.RegistroMedioPago.PAC.TipoCuenta) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAC.Confirmado <> f.RegistroMedioPago.PAC.Confirmado) OR
                                    (FDatosConvenio.MedioPago.MedioPago.PAC.NumeroCheque <> f.RegistroMedioPago.PAC.NumeroCheque) OR
                                    TipoMedioPagoActualCambio;
            end;
        end;

       if (FDatosConvenio.MedioPago.Mandante.CodigoPersona = f.RegistroPersonaMandante.CodigoPersona) AND NOT TipoMedioPagoActualCambio then
       begin
           TipoMedioPagoActualCambio := (FDatosConvenio.MedioPago.MedioPago.RUT <> f.RegistroMedioPago.RUT) OR
                                    (FDatosConvenio.MedioPago.MedioPago.Nombre <> f.RegistroMedioPago.Nombre) OR
                                    (FDatosConvenio.MedioPago.MedioPago.Telefono <> f.RegistroMedioPago.Telefono) OR
                                    (FDatosConvenio.MedioPago.MedioPago.CodigoArea <> f.RegistroMedioPago.CodigoArea) OR
                                    TipoMedioPagoActualCambio;
       end;
       
{TERMINO: TASK_039_JMA_20160708}

        FDatosConvenio.MedioPago.MedioPago              := f.RegistroMedioPago;
        FDatosConvenio.MedioPago.Mandante.CodigoPersona := f.RegistroPersonaMandante.CodigoPersona;
        FDatosConvenio.MedioPago.Mandante.Datos         := f.RegistroPersonaMandante;
        if f.PuedeForzarConfirmado then begin
            FDatosConvenio.MedioPago.ForzarConfirmado := f.ForzarConfirmado;
          	CambioPagoAutomatico(True);
        end;
        if Trim(f.Telefono.Valor) <> EmptyStr then begin
            if FDatosConvenio.MedioPago.Mandante.Telefonos.TieneMedioPrincipal then
                FDatosConvenio.MedioPago.Mandante.Telefonos.MediosComunicacion[0] := f.Telefono
            else
                FDatosConvenio.MedioPago.Mandante.Telefonos.AgregarMediocomunicacion(f.Telefono);
        end else begin
            if FDatosConvenio.MedioPago.Mandante.Telefonos.CantidadMediosComunicacion > 0 then begin
                if (FDatosConvenio.MedioPago.Mandante.Telefonos.CantidadMediosComunicacion > 1) and
                   (MsgBox(MSG_QUESTION_BORRAR_TELEFONES_MANDANTE, Caption, MB_YESNO or MB_ICONQUESTION) = IDYES) then begin
                       for i := 0 to FDatosConvenio.MedioPago.Mandante.Telefonos.CantidadMediosComunicacion - 1 do
                            FDatosConvenio.MedioPago.Mandante.Telefonos.QuitarMedioComunicacion(0);
                end else
                    FDatosConvenio.MedioPago.Mandante.Telefonos.QuitarMedioComunicacion(0);
            end;
        end;
        ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
    end;
	f.Release;
end;


{-------------------------------------------------------------------------------
Procedure Name	: cb_PagoAutomaticoChange
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
-------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.cb_PagoAutomaticoChange(Sender: TObject);
begin
{INICIO: TASK_039_JMA_20160708 Revisi�n 2}
    TipoMedioPagoActualCambio:= True;
{TERMINO: TASK_039_JMA_20160708}
    ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
	CambioPagoAutomatico(true);
end;


{-------------------------------------------------------------------------------
Procedure Name	: CambioPagoAutomatico
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Pone el Tipo de Pago Automatico
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.CambioPagoAutomatico(MostrarPantallita: boolean);
var
    AuxDatosMediosPago: TDatosMediosPago;
begin
    lbl_DescripcionMedioPago.Caption := '';
    lbl_DescripcionMedioPago.Hint := '';
    btn_EditarPagoAutomatico.Enabled := cb_PagoAutomatico.Value > TPA_NINGUNO;
    with FDatosConvenio.MedioPago do begin
        case Integer(cb_PagoAutomatico.Value) of
            TPA_NINGUNO: begin
                TipoMedioPago:=NINGUNO;
                lbl_DescripcionMedioPago.Caption := '';
                lbl_DescripcionMedioPago.Hint := '';
                AuxDatosMediosPago.TipoMedioPago := NINGUNO;
                FDatosConvenio.MedioPago.MedioPago := AuxDatosMediosPago;
              //INICIO: TASK_022_ECA_20160601
                //Controlo si ya tiene este medio de pago cargado
              {*  if ObtenerConvenioMedioPago(DMConnections.BaseCAC,
                                            FDatosConvenio.Cliente.Datos.NumeroDocumento,
                                            FDatosConvenio.CodigoConvenio,
                                            TPA_NINGUNO,
                                            //-1, '', '', -1, -1,                                                                       //SS_628_ALA_20110818
                                            1) > -1 then                                                                                //SS_628_ALA_20110720
                        MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO_DUPLICADO ,MSG_CAPTION_ERROR_MEDIO_PAGO, MB_ICONSTOP, cb_PagoAutomatico);*}
             //FIN: TASK_022_ECA_20160601
            end;
            TPA_PAT: begin
                    //Si el tipo era Ninguno el mandante no existia.
                    TipoMedioPago := peatypes.PAT;
                    if (trim(MedioPago.PAT.NroTarjeta) = '') or
                        (Trim(MedioPago.PAT.FechaVencimiento) = '') then begin
                            if MostrarPantallita then btn_EditarPagoAutomatico.OnClick(self);
                    end else CambioMedioPago(cb_PagoAutomatico);
            end;
            TPA_PAC: begin
                    //Si el tipo era Ninguno el mandante no existia.
                    TipoMedioPago := peatypes.PAC;
                    if (trim(MedioPago.PAC.NroCuentaBancaria) = '') then begin
                        if MostrarPantallita then btn_EditarPagoAutomatico.OnClick(self);
                    end else CambioMedioPago(cb_PagoAutomatico);

            end;
        end;
    end;
end;


{-------------------------------------------------------------------------------
Procedure Name	: CambioDomicilioFacturacion
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Permite Cambiar el domicilio de Facturaci�n
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.CambioDomicilioFacturacion(Sender: TObject);
var
    DomicilioFacturacion: TDatosDomicilio;
    Indice: Integer;
begin
    {INICIO: TASK_053_JMA_20160930
    if FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion) > 0 then
            with FDatosConvenio.Cliente.Domicilios.Domicilios[FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion)] do
                lbl_OtroDomicilio.Caption:=ArmarDomicilioSimple(DMConnections.BaseCAC, iif(trim(CalleDesnormalizada)='',Descripcion,CalleDesnormalizada), NumeroCalleSTR,0,'','')
    	//TASK_076_MGO_20161003
    else
        lbl_OtroDomicilio.Caption:='';

    if FDatosConvenio.CodigoDomicilioFacturacion <> FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoDomicilio then begin	//TASK_076_MGO_20161003
        BtnEditarDomicilioEntrega.Enabled:=true;
        RBDomicilioEntregaOtro.Checked:=True;
	end else begin
        RBDomicilioEntrega.Checked:=true;
        //if RBDomicilioEntrega.Enabled then RBDomicilioEntrega.SetFocus;	//TASK_076_MGO_20161003
        lbl_OtroDomicilio.Caption:='';
        BtnEditarDomicilioEntrega.Enabled:=False;
    end;}

    Indice := FDatosConvenio.Cliente.Domicilios.ObtenerIndiceDomicilio(FDatosConvenio.CodigoDomicilioFacturacion);
    if Indice > 0 then
    begin
        DomicilioFacturacion := FDatosConvenio.Cliente.Domicilios.Domicilios[Indice];
        lbl_OtroDomicilio.Caption:=ArmarDomicilioSimple(DMConnections.BaseCAC,
                                                        iif(trim(DomicilioFacturacion.CalleDesnormalizada)='',
                                                        DomicilioFacturacion.Descripcion,
                                                        DomicilioFacturacion.CalleDesnormalizada),
                                                        DomicilioFacturacion.NumeroCalleSTR,0,'','');
        BtnEditarDomicilioEntrega.Enabled:=true;
        RBDomicilioEntregaOtro.Checked := True;
    end
    else
    begin
        lbl_OtroDomicilio.Caption:='';
        BtnEditarDomicilioEntrega.Enabled:=False;
        RBDomicilioEntrega.Checked := true;
    end;
    {TERMINO: TASK_053_JMA_20160930}
end;


{------------------------------------------------------------------------------
Procedure Name	: CambioMedioPago
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.CambioMedioPago(Sender: TObject);
var
    AuxMedioPAgo: String;
begin
    if (FDatosConvenio = nil) or (FDatosConvenio.MedioPago = nil) then begin
        lbl_DescripcionMedioPago.Caption := SELECCIONAR;
        lbl_DescripcionMedioPago.Hint := '';
        btn_EditarPagoAutomatico.Enabled := False;
	end else begin
        if (FDatosConvenio.MedioPago.CodigoMedioPago = TPA_NINGUNO) then begin
            lbl_DescripcionMedioPago.Caption := '';
            lbl_DescripcionMedioPago.Hint := '';
            btn_EditarPagoAutomatico.Enabled := False;
        end else begin
            if not TFormMediosPagosConvenio.ArmarMedioPago(FDatosConvenio.MedioPago.ObtenerMedioPago,
                                                           FDatosConvenio.MedioPago.Confirmado or FDatosConvenio.MedioPago.ForzarConfirmado,
                                                           FDatosConvenio.CodigoConvenio,
                                                           AuxMedioPAgo) then begin
                AuxMedioPAgo := UPPERCASE(STR_ERROR);
            end;
            lbl_DescripcionMedioPago.Caption := AuxMedioPAgo + ' Fecha actualizaci�n : '+FormatDateTime('dd/mm/yyyy', FDatosConvenio.FechaActualizacionMedioPago); //SS_1068_NDR_20120910
            lbl_DescripcionMedioPago.Hint := AuxMedioPAgo;
            btn_EditarPagoAutomatico.Enabled := True;
        end;
    end;
end;


procedure TFormModificacionConvenio.chkCtaComercialPredeterminadaClick(Sender: TObject);
begin
   btn_Guardar.Enabled:= True;
end;

{$ENDREGION}

{$REGION 'OTROS'}

{-------------------------------------------------------------------------------
Procedure Name	: btn_DocRequeridoClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Accion al hacer click al boton DocRequerido
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_DocRequeridoClick(Sender: TObject);
var
    f: TformDocPresentadaModificacionConvenio;
	cantDocumentos: integer;
resourcestring
	MSG_SIN_DOCUMENTACION_PENDIENTE = 'No hay documentaci�n pendiente de presentaci�n';
begin
    if ModoPantalla <> scNormal then begin
        ActualizarClases;
        FDatosConvenio.ObtenerCambios(FDatosConvenioOriginal);
    end;

    try
        cantDocumentos := 0;
        Application.CreateForm(TformDocPresentadaModificacionConvenio,f);
        f.InicializarModificacionConvenio(LimpiarCaracteresLabel(btn_DocRequerido.Caption),
                      FDatosConvenio,
                      true, //corregir esto para implementarlo por medio de un metodo de clase //TformMedioEnvioDocumentosCobro.EnvioDocumentoCobroViaMail(RegMedioEnvioCorreo)
                      cantDocumentos,
                      ModoPantalla);
		if (cantDocumentos = 0) then  MsgBox(MSG_SIN_DOCUMENTACION_PENDIENTE, FMsgBoxCaption)
		else begin                                                              // SS-887-PDO-20100715
            if f.ShowModal = mrok then ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
        end;                                                                    // SS-887-PDO-20100715


    finally
        f.release;
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_VerObservacionesClick
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_VerObservacionesClick(Sender: TObject);
var
    f : TFormCheckListConvenio;
resourcestring
	MSG_CANT_INITIALIZE_FORM = 'El formulario no ha podido inicializarse.';
begin
    btn_VerObservaciones.OnClick := Nil;
    Application.CreateForm(TFormCheckListConvenio, f);
    f.FCodigoConvenio:=FDatosConvenio.CodigoConvenio;                           //SS-842-NDR-20110912
    if f.Inicializa(TStateConvenio(iif(ExisteAcceso('datos_otros'), Integer(ModoPantalla), Integer(scNormal))), FDatosConvenio.CheckListConvenio, FDatosConvenio.ListObservacionConvenio, FDatosConvenio.ChekListCuenta) then begin
        if f.ShowModal = mrOk then begin
            FDatosConvenio.ListObservacionConvenio := f.ListaObservacion;
            FDatosConvenio.CheckListConvenio := f.ListaCheck;
            FDatosConvenio.ChekListCuenta := f.ListaCheckCuenta;

            if f.FModificado then begin                                             // SS_960_PDO_20120114
                ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715     // SS_960_PDO_20120114
            end;                                                                    // SS_960_PDO_20120114
        end;
        cb_Observaciones.Checked := FDatosConvenio.TieneObservacion;
        btn_VerObservaciones.Enabled := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or cb_Observaciones.Checked;
        cb_Observaciones.Enabled := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or (not cb_Observaciones.Checked);
    end
    else MsgBox(MSG_CANT_INITIALIZE_FORM, FMsgBoxCaption, MB_Ok + MB_ICONERROR);
    f.Free;
    HabilitarBotonesVehiculos;
    btn_VerObservaciones.OnClick := btn_VerObservacionesClick;
end;


{-------------------------------------------------------------------------------
Procedure Name	: cb_ObservacionesClick
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.cb_ObservacionesClick(Sender: TObject);
begin
    cb_Observaciones.OnClick:=Nil;
    btn_VerObservaciones.Enabled := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) OR cb_Observaciones.Checked;
    cb_Observaciones.Enabled := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or (not cb_Observaciones.Checked);
    if not FCargando then begin
		if btn_VerObservaciones.Enabled then btn_VerObservaciones.Click
        else FDatosConvenio.ListObservacionConvenio.Clear;
    end;
    cb_Observaciones.OnClick := cb_ObservacionesClick;
end;

{-------------------------------------------------------------------------------
Procedure Name	: chkInactivaDomicilioRNUTClick
 Author 		: Jose Jofre
 Date Created	: 01-Agosto-2010
 Description	: cambia la propiedad InactivaDomicilioRNUT
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.chkInactivaDomicilioRNUTClick(Sender: TObject);
begin
    FDatosConvenio.Cliente.InactivaDomicilioRNUT := chkInactivaDomicilioRNUT.Checked ;
  	HabilitarControles(true);
    ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715

     if Assigned(FDatosConvenioOriginal) then begin
        if (FDatosConvenioOriginal.Cliente.InactivaDomicilioRNUT = FDatosConvenio.Cliente.InactivaDomicilioRNUT) then begin
            if (RBDomicilioEntrega.Checked <> FCambioDomicilio) then begin
                RBDomicilioEntrega.Checked :=  FCambioDomicilio;
            end;
            if (RBDomicilioEntregaOtro.Checked <> FCambioDomicilioOtro) then begin
                RBDomicilioEntregaOtro.Checked :=  FCambioDomicilioOtro;
                FDatosConvenio.Cliente.Domicilios.AgregarDomicilio(FDatosConvenioOriginal.Cliente.Domicilios.DomicilioFacturacion);
            end;
        end;
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: chkInactivaMediosComunicacionRNUTClick
 Author 		: Jose Jofre
 Date Created	: 01-Agosto-2010
 Description	: cambia la propiedad InactivaMedioComunicacionRNUT
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.chkInactivaMediosComunicacionRNUTClick(Sender: TObject);
begin
    FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT := chkInactivaMediosComunicacionRNUT.Checked ;
  	HabilitarControles(true);
    ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
    GBMediosComunicacion.Enabled:= True; //TASK_038_ECA_20160625
end;


{$ENDREGION}

{$REGION 'VEHICULOS'}

procedure TFormModificacionConvenio.btnSuscribirListaClick(Sender: TObject);
ResourceString
    MSG_QUESTION_SUSCRIBIR= '�Est� seguro de querer Suscribir la Patente %s a la Lista Blanca?';
begin
    if ValidarCuentaSuscrita(FDatosConvenio.CodigoConvenio,cdsVehiculosContacto.FieldByName('Patente').AsString) then
    Begin
        MsgBox('La patente ' + cdsVehiculosContacto.FieldByName('Patente').AsString + ' ya est� Suscrita a la lista Blanca', FMsgBoxCaption, MB_ICONINFORMATION);
    End
    else
    begin
       if ValidarRestriccionesSuscripcionListaBlanca(FDatosConvenio.CodigoPersona) then
       begin
           if MsgBox(format(MSG_QUESTION_SUSCRIBIR,[cdsVehiculosContacto.FieldByName('Patente').AsString]), FMsgBoxCaption, MB_YESNO or MB_ICONWARNING) = IDYES then
           begin
                SuscribirVehiculos:= SuscribirVehiculos +  cdsVehiculosContacto.FieldByName('Patente').AsString + ';';
                btn_Guardar.Enabled:=True;
           end;
       end
       else
       begin
          MsgBox('El Cliente presenta Impedimentos para la suscripci�n a la lista blanca', FMsgBoxCaption, MB_ICONINFORMATION);
            FormConsultaAltaOtraConsecionaria := TFormConsultaAltaOtraConsecionaria.ModalCreate(nil);
            if FormConsultaAltaOtraConsecionaria.Inicializar(FDatosConvenio.CodigoConvenio,True) then
            begin
                FormConsultaAltaOtraConsecionaria.ShowModal;
            end;
       end;
    end;
end;

procedure TFormModificacionConvenio.btnDesuscribirListaClick(Sender: TObject);
ResourceString
    MSG_QUESTION_DESUSCRIBIR= '�Est� seguro de querer DeSuscribir la Patente %s de la Lista Blanca?';
begin
   if not ValidarCuentaSuscrita(FDatosConvenio.CodigoConvenio,cdsVehiculosContacto.FieldByName('Patente').AsString) then
   Begin
       MsgBox('La patente ' + cdsVehiculosContacto.FieldByName('Patente').AsString + ' no est� Suscrita a la lista Blanca', FMsgBoxCaption, MB_ICONINFORMATION);
   End
   else
       if MsgBox(format(MSG_QUESTION_DESUSCRIBIR,[cdsVehiculosContacto.FieldByName('Patente').AsString]), FMsgBoxCaption, MB_YESNO or MB_ICONWARNING) = IDYES then //TASK_019_20160531
       begin
           DeSuscribirVehiculos:= DeSuscribirVehiculos +  cdsVehiculosContacto.FieldByName('Patente').AsString + ';';
           btn_Guardar.Enabled:=True;
       end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: dblVehiculosDblClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: DobleClick en la grilla de vehiculos
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.dblVehiculosDblClick(Sender: TObject);
begin
    if btnEditarVehiculo.Enabled and btnEditarVehiculo.visible then btnEditarVehiculo.Click;
end;



{-------------------------------------------------------------------------------
Procedure Name	: cdsVehiculosContactoAfterScroll
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Se hace llamada a procedimiento que habilita Botones.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.cdsVehiculosContactoAfterScroll(DataSet: TDataSet);
begin
    if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_RNUT then    //TASK_004_ECA_20160411
        if (Not cdsVehiculosContacto.ControlsDisabled) then
            HabilitarBotonesVehiculos;
end;


function TFormModificacionConvenio.ValidarCuentaSuscrita(CodigoConvenio: Integer; Patente: string): Boolean;
var
    IndiceVehiculo: Integer;
begin
   IndiceVehiculo:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerIndiceVehiculoConvenio('+ IntToStr(CodigoConvenio) + ',''CHL'',''' + Trim(Patente) + ''')');

   Result:=QueryGetBooleanValue(DMConnections.BaseCAC, 'SELECT dbo.ValidarCuentaSuscritaListaBlanca('+ IntToStr(CodigoConvenio) + ',' + IntToStr(IndiceVehiculo) + ')');
end;


{-------------------------------------------------------------------------------
Procedure Name	: btnEditarVehiculoClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btnEditarVehiculoClick(Sender: TObject);
begin
    ModificarVehiculo(False);
end;

{------------------------------------------------------------------------------
Procedure Name	: Inicializa
 Author 		: Nelson Droguett Sierra (dAlegretti)
 Date Created	: 19-Abril-2010
 Description	: Se elimina el uso de la FechaModifCuenta ya que pasa a ser uso exclusivo de la auditoria
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.ModificarVehiculo(AsignarTag: Boolean);
ResourceString
    MSG_ERROR_EDITAR_VEHICULO      = 'No se pudieron modificar los datos del veh�culo';
	MSG_EDITAR_VEHICULO_CAPTION    = 'Actualizar datos del veh�culo';
var
    f : TFormSCDatosVehiculo;
    Cadena, CadenaTags: TStringList;
    dondeEstaba: TBookmark;
    NoCargar: string;
    auxVehiculo: TVehiculosConvenio;
begin
    if dblVehiculos.DataSource.DataSet.RecordCount = 0 then Exit;

    Cadena := TStringList.Create;
    CadenaTags := TStringList.Create;
    dondeEstaba := cdsVehiculosContacto.GetBookmark;
    if (cdsVehiculosContacto.fieldbyname('ContractSerialNumber').IsNull) then
        NoCargar := ''
    else
        NoCargar := cdsVehiculosContacto.fieldbyname('ContractSerialNumber').Value;

    //Rev.5 / 09-Junio-2010 / Nelson Droguett Sierra ----------------------------
    //ArmarListaPatentes(Cadena, Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString), Trim(dsVehiculosContacto.DataSet.FieldByName('PatenteRVM').AsString));
    //ArmarListaTags(CadenaTags, Trim(NoCargar));
    ArmarListaPatentesTags(Cadena,
                           CadenaTags,
                           Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString),
                           Trim(dsVehiculosContacto.DataSet.FieldByName('PatenteRVM').AsString),
                           Trim(NoCargar)
                          );
    //FinRev.5-------------------------------------------------------------------

    cdsVehiculosContacto.GotoBookmark(dondeEstaba);
    cdsVehiculosContacto.FreeBookmark(dondeEstaba);
    Application.CreateForm(TFormSCDatosVehiculo, f);
    if f.Inicializar(MSG_CAPTION_EDITAR_VEHICULO, PuntoEntrega,
        Trim(cdsVehiculosContacto.FieldByName('TipoPatente').AsString),
        Trim(cdsVehiculosContacto.FieldByName('Patente').AsString),
        Trim(cdsVehiculosContacto.FieldByName('PatenteRVM').AsString),
        cdsVehiculosContacto.FieldByName('Modelo').AsString,
        cdsVehiculosContacto.FieldByName('CodigoMarca').AsInteger,
        cdsVehiculosContacto.FieldByName('AnioVehiculo').AsInteger,
        cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger,
        cdsVehiculosContacto.FieldByName('CodigoColor').AsInteger,
        Trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatente').AsString),
        Trim(dsVehiculosContacto.DataSet.FieldByName('DigitoVerificadorPatenteRVM').AsString),
        cdsVehiculosContacto.FieldByName('CodigoVehiculo').AsInteger,
        cdsVehiculosContacto.FieldByName('Observaciones').AsString,
        -1,
        Cadena,true, CadenaTags,
        cdsVehiculosContacto.FieldByName('ContextMark').AsInteger,
        cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString,
        AsignarTag,
        False,
        FDatosConvenio.CodigoConvenio,
        cdsVehiculosContacto,
        cdsVehiculosContacto.FieldByName('Vendido').AsBoolean or (Trim(cdsVehiculosContacto.FieldByName('Patente').AsString) = ''),
        FDatosConvenio.Cliente.Datos.CodigoPersona,
        Peatypes.ConvenioModiVehiculo)
        and (f.ShowModal = mrOK) then begin
        try
            with FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).Cuenta do begin
                auxVehiculo.Cuenta := f.DatoCuenta;
                auxVehiculo.Cuenta.IndiceVehiculo := IndiceVehiculo;
                auxVehiculo.Cuenta.FechaCreacion := FechaCreacion;
                auxVehiculo.Cuenta.FechaAltaTag := FechaAltaTag;
                auxVehiculo.Cuenta.FechaBajaTag := FechaBajaTag;
                auxVehiculo.Cuenta.FechaVencimientoTag := FechaVencimientoTag;
                auxVehiculo.EsVehiculoNuevo := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).EsVehiculoNuevo;
                auxVehiculo.EsVehiculoModificado := True;
                auxVehiculo.Documentacion := nil;
                auxVehiculo.Cuenta.CodigoConvenioFacturacion:= CodigoConvenioFacturacion;
                auxVehiculo.EsTagVendido := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).EsTagVendido;
                auxVehiculo.Cuenta.CodigoAlmacenDestino :=   f.almacendestino;
                auxVehiculo.EsCambioVehiculo := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).EsCambioVehiculo;
                auxVehiculo.Cuenta.Vehiculo.CodigoColor:= f.Color ;//TASK_016_ECA_20160526
                FDatosConvenio.Cuentas.EditarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString), auxVehiculo);
                //Rev.5 / 09-Junio-2010-----------------------------------------------------
                try
                  DSVehiculosContacto.DataSet := Nil;
                  cdsVehiculosContacto.DisableControls;
                  FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                finally
                  cdsVehiculosContacto.EnableControls;
                  DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                end;
                //Rev.5---------------------------------------------------------------------
            end;

            ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR_VEHICULO, E.message, MSG_EDITAR_VEHICULO_CAPTION, MB_ICONSTOP);
            end;
        end;
    end;
    Cadena.Free;
    CadenaTags.Free;
    f.Release;
end;


{-------------------------------------------------------------------------------
Procedure Name	: dblVehiculosDrawText
 Author 		: Nelson Droguett Sierra (dAlegretti)
 Date Created	: 19-Abril-2010
 Description	:Se elimina el uso de la FechaModifCuenta ya que pasa a ser uso exclusivo de la auditoria
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.dblVehiculosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp, Blanco: TBitMap;
begin
    bmp:=nil;
    // Dibujo los led de las listas
	with Sender do begin
        if (Column.FieldName = 'IndicadorListaVerde') then begin
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            Blanco := TBitMap.Create;
            if (cdsVehiculosContacto.FieldByName('IndicadorListaVerde').AsBoolean ) then
          	    Imagenes.GetBitmap(1, Bmp)
            else
                Imagenes.GetBitmap(0, Bmp);

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo balnco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;

        if (Column.FieldName = 'IndicadorListaNegra') then begin
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            Blanco := TBitMap.Create;
            if (cdsVehiculosContacto.FieldByName('IndicadorListaNegra').AsBoolean ) then
          	    Imagenes.GetBitmap(2, Bmp)
            else
                Imagenes.GetBitmap(0, Bmp);

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
			Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp);
            Bmp.Free;
            Blanco.Free;
        end;
{INICIO: TASK_036_JMA_20160707}
        if Column.FieldName='SuscritoListaBlanca' then
        begin
            DefaultDraw := False;
	        Text        := '';

	        Canvas.FillRect(Rect);
            try
	            bmp := TBitMap.Create;
                if cdsVehiculosContacto.FieldByName('SuscritoListaBlanca').AsBoolean then
                begin
	                lnCheck.GetBitmap(1, bmp);
                end
                else
                begin
    	            lnCheck.GetBitmap(0, bmp);
                end;
				Canvas.Draw(((Rect.Right - Rect.Left) div 2) - bmp.Width + Rect.Left, Rect.Top + 1, bmp);
            finally
                bmp.Free;
            end;
	    end;

        if Column.FieldName='HabilitadoListaBlanca' then
        begin
            DefaultDraw := False;
	        Text        := '';

	        Canvas.FillRect(Rect);
            try
	            bmp := TBitMap.Create;
                if cdsVehiculosContacto.FieldByName('HabilitadoListaBlanca').AsBoolean then
                begin
	                lnCheck.GetBitmap(1, bmp);
                end
                else
                begin
    	            lnCheck.GetBitmap(0, bmp);
                end;
				Canvas.Draw(((Rect.Right - Rect.Left) div 2) - bmp.Width + Rect.Left, Rect.Top + 1, bmp);
            finally
                bmp.Free;
            end;
	    end;
{TERMINO: TASK_036_JMA_20160707}
    end;

    // Pinto de rojo la linea si es una cuenta suspendida
    if (cdsVehiculosContacto.FieldByName('Suspendido').AsBoolean)  then begin
        if odSelected in State then
            Sender.Canvas.Font.Color := clYellow
        else
            Sender.Canvas.Font.Color := clRed;
    end
        //Azul si se trata de un vehiculo eliminado
        else if (cdsVehiculosContacto.FieldByName('VehiculoEliminado').AsBoolean) then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clBlue;
        end
        // INICIO : 20160603 MGO
        // si implica baja forzada
        else if (cdsVehiculosContacto.FieldByName('Foraneo').AsBoolean) then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clMaroon
            else
                Sender.Canvas.Font.Color := clMaroon;
        end
        // FIN : 20160603 MGO
        else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end;

    // Formateo la Fecha de Modificacion
    if Column.FieldName = 'FechaBajaCuenta' then begin
        if not cdsVehiculosContacto.FieldByName('FechaBajaCuenta').IsNull then begin
            Text := FormatDateTime('dd/mm/yyyy hh:mm',cdsVehiculosContacto.FieldByName('FechaBajaCuenta').AsDateTime);
        end                 // SS_916_PDO_20120103
        else Text := '';    // SS_916_PDO_20120103
    end;
    if Column.FieldName = 'FechaCreacion' then
        Text := FormatDateTime('dd/mm/yyyy hh:mm',cdsVehiculosContacto.FieldByName('FechaCreacion').AsDateTime);
{INICIO: TASK_036_JMA_20160707}
    if Column.FieldName = 'FechaVencimientoGarantia' then
        Text := FormatDateTime('dd/mm/yyyy hh:mm',cdsVehiculosContacto.FieldByName('FechaVencimientoGarantia').AsDateTime);
{TERMINO: TASK_036_JMA_20160707}
    //if Column.FieldName='HabilitadoAMB' then begin                                                                                                   //SS-1006-NDR-20111105
    //  lnCheck.Draw(dblVehiculos.Canvas, Rect.Left + 3, Rect.Top + 1, IfThen(cdsVehiculosContacto.FieldByName('HabilitadoAMB').AsBoolean,1,0));       //SS-1006-NDR-20111105
    if Column.FieldName='AdheridoPA' then begin                                                                                                        //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
      lnCheck.Draw(dblVehiculos.Canvas, Rect.Left + 3, Rect.Top + 1, IfThen(cdsVehiculosContacto.FieldByName('AdheridoPA').AsBoolean,1,0));            //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
      DefaultDraw := False;
      ItemWidth   := lnCheck.Width + 2;
    end;
    if Column.FieldName = 'ImagenReferencia' then Text := 'Ver';    // SS_1214_CQU_20140917
end;

{$ENDREGION}

{$REGION 'GUARDAR CAMBIOS'}

{-------------------------------------------------------------------------------
Procedure Name	: btn_GuardarClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Si el Convenio est� Dado de Baja, s�lo actualizar el dato asociado al checkbox No Generar Intereses.
                	Controlo por afuera que los telefonos no sean iguales, dado que por los framse de tel�fono
    se validan las componentes pero no se validan entre si ellos.Quito la validaci�n por tel�fonos iguales.
    Se modifica el guardar para que no genere XML de convenios sin cuentas.

    Revision : 6
        Author: pdominguez
        Date  : 01/07/2010
        Description: SS 887 - Mejorar la Performance - Cuentas Duplicadas
            - Se incorpora un nuevo bloque Try Finally y la variable EnTransaccion para
            controlar la acci�n a tomar en caso de error.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_GuardarClick(Sender: TObject);
    procedure CancelarAnexo(NroAnexo: Integer);
    begin
        if NroAnexo <> -1 then begin
            //Vuelve para atras el numero de anexo que se incrementa al imprimir
            with spCancelarUltimoAnexo do begin
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroConvenio').Value := FNumeroConvenio;
                Parameters.ParamByName('@AnexoCancelado').Value := NroAnexo;
                ExecProc;
            end;
        end;
    end;

resourcestring
    MSG_CONFIRMATION        = 'Confirmar Cambios Convenio';
    STR_CONF_BUTTON_CAPTION = 'Confir&ma Cambios';
    MSG_XMLFILE_ERROR       = 'No se gener� archivo de novedades XML';
    MSG_ERROR_INSERT_LOG_SOR= 'Error al intentar insertar evento en la tabla Eventos.';
    MSG_ERROR_AUDITORIA     = 'Error guardando datos auditoria';
    CAPTION_ERROR           = 'Error en tel�fonos del contacto.';
    MSG_ERROR_TELS_IGUALES  = 'Los tel�fonos principal y alternativo est�n duplicados.';
    MSG_ERROR_INSERT_AUDIT_RNUT = 'Error al intentar Insertar registro en AuditoriaConveniosRNUT';
    MSG_DOMICILIO_ADMINISTRADO_CN = 'Los Domicilios de �ste convenio son administrados por la Concesionaria';
    MSG_MEDIO_ADMINISTRADO_CN = 'Los Medios de Comunicaci�n de �ste Convenio son administrados por la Concesionaria';
    MSG_CAPTION_CONTRATO_PA   = 'Seleccione la bandeja que tiene la hoja del contrato de Adhesi�n a Parque Arauco en la Impresora';        //SS-1006-NDR-20120727
var
    f: TfrmConfirmarBajaModifConvenio;
    fi: TfrmConfirmarImpresion;
    ModalResultado: TModalResult;
    Imprimio: Boolean;
    AgregaObservacionDomicilio, AgregaObservacionMedioComunicacion: Boolean;
    ObservacionDomicilio, ObservacionMedioComunicacion: TObservacionConvenio;
    i: Integer;
    Form : TFormImprimirConvenio;
    Cuentas: array of TCuentaVehiculo;                                                                              //SS-1006-NDR-20120813
    ImpCaratula, ImpAnexo1, ImpMandato: Boolean;
begin
    AgregaObservacionDomicilio:= false;
    AgregaObservacionMedioComunicacion:= False;
    
    if FCodigoConcesionariaNativa = FDatosConvenio.CodigoConcesionaria then //TASK_037_ECA_20160622
        if not ValidateControls([FMDomicilioPrincipal.txt_CodigoPostal],
          [PermitirSoloDigitos(FMDomicilioPrincipal.txt_CodigoPostal.Text)],
          MSG_CAPTION_DOMICILIO, [format(MSG_VALIDAR_CODIGO_POSTAL_DIGITOS,
          [FLD_CODIGO_POSTAL])]) then exit;

    if FEstaConvenioDeBaja then begin
        ActualizarConvenioDeBaja;
        FDatosConvenio.ListObservacionConvenio.Guardar(FDatosConvenio.CodigoConvenio);
        Exit;
    end;
    SicronizarDoc;
    try
        Screen.Cursor := crHourglass;
        Application.ProcessMessages;

        Imprimio := False;
        ModalResultado := mrOk;
        //EnTransaccion := False; // SS_953_PDO_20110202

        if PuedeGuardarGenerico then begin
            if PuedeGuardarContrato then try  // SS-887-PDO-20100715

                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text , FDatosConvenio.CodigoConvenio, '', 0 ,'' ,  FPuntoEntrega, UsuarioSistema, 'Guardar')  then
                    MsgBoxErr(MSG_ERROR_AUDITORIA, MSG_ERROR_AUDITORIA, FMsgBoxCaption, MB_ICONSTOP);

                if not ValidarTagsEnArriendo() then begin
                    ModalResultado := mrCancel;
                    Exit;
                end;

                HabilitarControles(False);
                NB_Botones.Enabled := False;

                Application.CreateForm(TfrmConfirmarBajaModifConvenio, f);
                if f.Inicializar(MSG_CONFIRMATION, STR_CONF_BUTTON_CAPTION) then
                    ModalResultado := f.ShowModal;
                f.Release;
                if ModalResultado <> mrOk then //Si cancela o reintenta vuelvo para atras el anexo
                    CancelarAnexo(FDatosConvenio.NroAnexoEmitido);

                if ModalResultado = mrCancel then begin
                    CancelarAnexo(FDatosConvenio.NroAnexoEmitido);
                    HabilitarControles(True);
                    NB_Botones.Enabled := True;
                    HabilitarBotonesVehiculos;
                    Exit;
                end;


                try // guardo
                   FDatosConvenio.TurnoAbierto; // Actualizo los datos de los turnos
                   FDatosConvenio.Cliente.RecibeClavePorMail := cb_RecibeClavePorMail.Checked;
                   FDatosConvenio.Cliente.InactivaDomicilioRNUT := chkInactivaDomicilioRNUT.Checked;
                   FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT := chkInactivaMediosComunicacionRNUT.Checked;
                   FDatosConvenio.SinCuenta := iif((Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_INFRACTOR) OR (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL),true, false); // chkConvenioSinCuenta.Checked;     //SS_628_ALA_20111202  //TASK_004_ECA_20160411
                   FDatosConvenio.AdheridoPA := False; // cb_AdheridoPA.Checked;                  //SS-1006-NDR-20120813 //TASK_004_ECA_20160411

                   FDatosConvenio.CodigoConvenioFacturacion:= CodigoConvenioFacturacion; // TASK_004_ECA_20160411
                   FDatosConvenio.CodigoTipoConvenio:= Integer(cbbTipoConvenio.Value);   // TASK_004_ECA_20160411
                   FDatosConvenio.CodigoClienteFacturacion := CodigoClienteFacturacion;  // TASK_004_ECA_20160411

                   //INICIO: TASK_004_ECA_20160411
                   if (FDatosConvenio.CodigoTipoConvenio=CODIGO_TIPO_CONVENIO_RNUT) or (FDatosConvenio.CodigoTipoConvenio=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL)  then
                        FDatosConvenio.CodigoTipoCliente := Integer(cbbTipoCliente.Value);
                   //FIN: TASK_004_ECA_20160411

				   //Rev.3 / 01-Junio-2010 / Nelson Droguett Sierra
                   DMConnections.BaseCAC.BeginTrans;
//            	   QueryExecute(DMConnections.BaseCAC, 'BEGIN TRAN spGuardarConvenio');
//                   EnTransaccion := True; // SS-887-PDO-20100715
					//FinRev.3

                   //INICIO:  TASK_038_ECA_20160625
                   if  not FDatosConvenio.EsConvenioNativo then
                   begin
                        if (FDatosConvenio.Cliente.InactivaDomicilioRNUT) and (FDatosConvenio.Cliente.InactivaDomicilioRNUT <> FDatosConvenioOriginal.Cliente.InactivaDomicilioRNUT) then begin
                            ObservacionDomicilio:= TObservacionConvenio.Create();
                            ObservacionDomicilio.Observacion := FormatDateTime('dd/mm/yyyy hh:mm',now) + ' - ' +  UpperCase(UsuarioSistema) + ' ' + MSG_DOMICILIO_ADMINISTRADO_CN;
                            ObservacionDomicilio.FechaObservacion := QueryGetValueDateTime(DmConnections.BaseCAC, 'SELECT GETDATE()');                        //SS_1208_NDR_20140819
                            ObservacionDomicilio.Activo := True;
                            FDatosConvenio.ListObservacionConvenio.Add(ObservacionDomicilio);
                            AgregaObservacionDomicilio := True;
                        end;

                        if (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT) and (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT <> FDatosConvenioOriginal.Cliente.InactivaMedioComunicacionRNUT) then begin
                            ObservacionMedioComunicacion := TObservacionConvenio.Create();
                            ObservacionMedioComunicacion.Observacion := FormatDateTime('dd/mm/yyyy hh:mm',now) + ' - ' +  UpperCase(UsuarioSistema) + ' ' + MSG_MEDIO_ADMINISTRADO_CN;
                            ObservacionMedioComunicacion.FechaObservacion := QueryGetValueDateTime(DmConnections.BaseCAC, 'SELECT GETDATE()');                //SS_1208_NDR_20140819
                            ObservacionMedioComunicacion.Activo := True;
                            FDatosConvenio.ListObservacionConvenio.Add(ObservacionMedioComunicacion);
                            AgregaObservacionMedioComunicacion := True;
                        end;
                   end;
                   //FIN: TASK_038_ECA_20160625

                    //InsertarHistoricoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;			//SS_633_MCA_20140318 //TASK_008_ECA_20160502
                    //InsertarHistoricoConvenio.ExecProc;                                                                                   //SS_633_MCA_20140318 //TASK_008_ECA_20160502

                  

                    //FDatosConvenio.Guardar(FCambioHabilitadoaAMB,cb_HabilitadoAMB.Checked); //SS-1006-NDR-20111105
                    FDatosConvenio.Guardar( FCambioAdheridoPA,                        //SS-1006-NDR-20120614
                                            False, //cb_AdheridoPA.Checked,                    //SS-1006-NDR-20120614 TASK_004_ECA_20160411
                                            FCambioInhabilitado,                      //SS-1006-NDR-20120614
                                            False, //cb_InhabilitadoPA.Checked,                //SS-1006-NDR-20120614 TASK_004_ECA_20160411
                                            -1); // cb_MotivosInhabilitacion.Value);          //SS-1006-NDR-20120614 //SS-1006-NDR-20111105 TASK_004_ECA_20160411

                    //primero guardo los cambio del convenio y luego el historico, siempre que se haya modificado algun dato del convenio
                    //InsertarHistoricoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;				//SS_633_MCA_20140318
                    // InsertarHistoricoConvenio.ExecProc;                                                                                  	//SS_633_MCA_20140318

                    //INICIO: TASK_035_ECA_20160620
                    //Actualizamos la suscripcion de los vehiculos
                    ActualizarCuentaSuscripcion();

				   	//Rev.3 / 01-Junio-2010 / Nelson Droguett Sierra


                    //INICIO: TASK_005_ECA_20160420
                    //Actualizamos las vehiculos de la cuenta Comercial
                    GuardarVehiculosCuentaComercial();

                    //INICIO: TASK_005_ECA_20160420
                    if (chkCtaComercialPredeterminada.Checked) and (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
                       ActualizarPersonaCuentaComercial(FDatosConvenio.CodigoPersona, FDatosConvenio.CodigoConvenio);
                    //FIN: TASK_005_ECA_20160420

                     //INICIO: TASK_008_ECA_20160502
                     InsertarHistoricoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;			//SS_633_MCA_20140318
                     InsertarHistoricoConvenio.ExecProc;
                     //FIN: TASK_008_ECA_20160502

                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;

                    //FIN: TASK_005_ECA_20160420

                    //INICIO: TASK_034_ECA_20160611
                    //Actualizamos la suscripcion de los vehiculos
                    //ActualizarCuentaSuscripcion;
                    //FIN: TASK_034_ECA_20160611
                    //FIN: TASK_035_ECA_20160620

//            	    QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRAN spGuardarConvenio END');
//                    EnTransaccion := False; // SS-887-PDO-20100715
					//FinRev.3

                   //FEsConvenioSinCuenta := StrToBool(QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.EsConvenioSinCuenta(%d)', [FDatosConvenio.CodigoConvenio])));    //SS_628_ALA_20111202
                   //if (FDatosConvenio.CodigoConcesionaria = CODIGO_CN)		//SS_1147_MCA_20140408
//                   if (FDatosConvenio.CodigoConcesionaria = FCodigoConcesionariaNativa)	//SS_1147_MCA_20140408
//                        And (not EsConvenioSinCuenta)then begin                                                                                                                //SS_628_ALA_20111202
//                       if not GenerarNovedadXML(DMConnections.BaseCAC, FDatosConvenio.CodigoConvenio, FCodigoConcesionariaNativa, FPathNovedadXML,
//                         iif(FRNUTUTF8 = 1, True, False), DescriError) then begin
//                            if DescriError <> EmptyStr then begin
//                                try
//                                    spEventosSOR :=  TADOStoredProc.Create(nil);
//                                    spEventosSOR.Connection := DMConnections.BaseSOR;
//                                    spEventosSOR.ProcedureName := 'CrearEvento';
//                                    spEventosSOR.Parameters.Refresh;
//                                    spEventosSOR.Parameters.ParamByName('@CodigoTipoEvento').Value := 213;
//                                    spEventosSOR.Parameters.ParamByName('@CodigoSistema').Value := SistemaActual;
//                                    spEventosSOR.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
//                                    spEventosSOR.Parameters.ParamByName('@NombreArchivo').Value := Application.Title;
//                                    spEventosSOR.Parameters.ParamByName('@Detalle').Value := DescriError + ' ' +
//                                    'CONVENIO: ' + FDatosConvenio.NumeroConvenio;
//                                    spEventosSOR.ExecProc;
//                                except
//                                    on e: Exception do begin
//                                        MsgBoxErr(MSG_ERROR_INSERT_LOG_SOR, e.Message, Self.Caption, MB_ICONSTOP);
//                                    end;
//                                end;
//                                MsgBoxErr(MSG_XMLFILE_ERROR, DescriError, MSG_CONFIRMATION, MB_OK);
//    	                   end;
//	                   end;
//                   end;

                    //INICIO:TASK_037_ECA_20160622
                    // SE AGREGA ESTA CONDICION DEBIDO A QUE ES UN CONVENIO FORANEO Y CUANDO NO EXISTEN LOS DATOS ABAJO VALIDADOS DA PROBLEMAS
                    if FCodigoConcesionariaNativa = FDatosConvenio.CodigoConcesionaria then
                    begin
                        ValidaCambioDomicilios();
                        ValidarCambioMediosDeContacto();
                    end;
                    //FIN: TASK_037_ECA_20160622

                    MsgBox(MSG_CAPTION_CONVENIO_OK, FMsgBoxCaption, MB_ICONINFORMATION);

    {INICIO: TASK_011_JMA_201605}
                   // Despu�s de guardar los datos, se presenta la pantalla para confirmar impresion
                   // si hay documentos por imprimir
                   ImpCaratula:= false;
                   ImpAnexo1 := false;
                   ImpMandato := False;
                   //ExisteVehiculos:= False;  //TASK_039_JMA_20160708  Revisi�n 2

                   if (FDatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_RNUT) or (FDatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_CTA_COMERCIAL) then
                   begin


                        if FDatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_RNUT then
                        begin
                            //Debo cargar solo las cuentas que sufrieron modificacion de tag o vehiculos y las nuevas
                            for i := 0 to FDatosConvenio.Cuentas.CantidadVehiculos - 1 do
                            begin
                                if FDatosConvenio.Cuentas.Vehiculos[i].EsVehiculoNuevo or
                                    FDatosConvenio.Cuentas.Vehiculos[i].EsTagNuevo or
                                    FDatosConvenio.Cuentas.Vehiculos[i].EsCambioTag or
                                    FDatosConvenio.Cuentas.Vehiculos[i].EsCambioVehiculo  then
                                begin
                                    ImpAnexo1:= True;
                                    SetLength(Cuentas, Length(Cuentas)+1);
                                    Cuentas[Length(Cuentas)-1] := FDatosConvenio.Cuentas.Vehiculos[i].Cuenta;
                                end;
                            end;
                        end;
 {INICIO: TASK_039_JMA_20160708  Revisi�n 2
                        if FDatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_CTA_COMERCIAL then
                        begin
                            if cdsVehiculosContacto.RecordCount>0 then
                                cdsVehiculosContacto.First;

                            while not cdsVehiculosContacto.eof do
                            begin
                                if cdsVehiculosContacto.FieldByName('Estado').Value<>'Eliminado' then
                                begin
                                    ExisteVehiculos:=True;
                                    Break;
                                end;
                                cdsVehiculosContacto.Next;
                            end;
                        end;


                        If (FDatosConvenio.MedioPago.MedioPago.TipoMedioPago <> Ninguno) then
}
                        If ((FDatosConvenio.MedioPago.MedioPago.TipoMedioPago <> Ninguno) and TipoMedioPagoActualCambio) then
                                //OR ((FDatosConvenio.MedioPago.MedioPago.TipoMedioPago <> Ninguno) and (FDatosConvenio.MedioPago.MedioPago.TipoMedioPago <> TipoMedioPagoActual))) then
{TERMINO: TASK_039_JMA_20160708}
                            ImpMandato := True;

                       {INICIO: TASK_018_JMA_20160606
                        if (FDatosConvenio.DocumentacionConvenio.CantidadOperacionesConvenio > 0 ) then begin
                        TERMINO: TASK_018_JMA_20160606}
                       if ImpCaratula OR ImpAnexo1 OR ImpMandato or (FDatosConvenio.Cuentas.CantidadVehiculosEliminados > 0) then
                       begin
                          // ModalResultado := mrRetry;
                           // while (ModalResultado = mrRetry) do begin
                                Application.CreateForm(TfrmConfirmarImpresion,fi);
                                if fi.Inicializar then
                                    ModalResultado := fi.ShowModal;
                                fi.Release;
                                if ModalResultado = mrOk then begin//Si cancela entonces asumo que imprimio para continuar el proceso

                                    Form := TFormImprimirConvenio.Create(nil);
                                    Form.Inicializar(FDatosConvenio);

                                    if ImpCaratula OR ImpAnexo1 OR ImpMandato then
                                        Imprimio := Form.ImprimirAltaConvenio(ImpCaratula, ImpAnexo1, ImpMandato, Cuentas, FDatosConvenio);

                                    if FDatosConvenio.Cuentas.CantidadVehiculosEliminados > 0 then
                                       Imprimio := Form.ImprimirBajaConvenio();

                                end;
                           // end;
                       end;
                   end;  
                   Imprimio := True;
 {TERMINO: TASK_011_JMA_201605}



//                   if (FCambioAdheridoPA and cb_AdheridoPA.Checked) and                                            //SS-1006-NDR-20120727
//                      (MsgBox(MSG_CAPTION_CONTRATO_PA,Caption,MB_ICONQUESTION or MB_YESNO) = IDYES) then           //SS-1006-NDR-20120727
//                          ImprimirContratoAdhesionPA();                                                            //SS-1006-NDR-20120727
                   // SS_1006_PDO_20120812	Inicio Bloque
                   //if (FCambioAdheridoPA and cb_AdheridoPA.Checked) then begin   //TASK_004_ECA_20160411
{INICIO: TASK_018_JMA_20160606
                    if (FCambioAdheridoPA) then begin
                        //BEGIN SS-1006-NDR-20120813 -----------------------------------------------
                        if (FDatosConvenio.DocumentacionConvenio.CantidadOperacionesConvenio = 0)  then                                               //SS-1006-NDR-20120813
                        begin
                            Application.CreateForm(TFrmSeleccionarReimpresion,fic);
                            if fic.Inicializar(Caption, FDatosConvenio, FDatosConvenioOriginal, PuntoEntrega, nil, True) then
                            begin
                                fic.cbAnverso.Checked:=True;
                                fic.btn_okClick(self);
                            end;
                            fic.Release;
                        end;
                        //END SS-1006-NDR-20120813 -----------------------------------------------
                        ImprimirContratoAdhesionPA();
                   end;
}
{TERMINO: TASK_018_JMA_20160606}
                   // SS_1006_PDO_20120812	Fin Bloque

                   //FCambioHabilitadoaAMB := False;    //SS-1006-NDR-20111105
                   //FCambioHabilitadoaPA := False;     //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
                   FCambioAdheridoPA := False;          //SS-1006-NDR-20120614
                   FCambioInhabilitado:=False;          //SS-1006-NDR-20120614
               except
                   On E: Exception do begin
				   	  //Rev.3 / 01-Junio-2010 / Nelson Droguett Sierra
                      if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
//                      if EnTransaccion then QueryExecute(DMConnections.BaseCAC, 'ROLLBACK TRAN spGuardarConvenio'); // Rev. 6 (SS 887)
//                      if EnTransaccion then QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRAN spGuardarConvenio END'); // SS_953_PDO_20110202
					  //FinRev.3

                      CancelarAnexo(FDatosConvenio.NroAnexoEmitido);

                       if E is EExtendedError then MsgBoxErr(EExtendedError(E).Message, EExtendedError(E).DetailMessage, FMsgBoxCaption,MB_ICONSTOP)
                       else MsgBoxErr(MSG_ERROR_SOLICITUD_CONVENIO, E.Message, FMsgBoxCaption, MB_ICONSTOP);

                   end;
               end;
            // Rev. 6 (SS 887)
            finally
//                if EnTransaccion or ((ModalResultado = mrCancel) and (not Imprimio)) then begin
                if DMConnections.BaseCAC.InTransaction or ((ModalResultado = mrCancel) and (not Imprimio)) then begin
                    NB_Botones.Enabled := True;
                    HabilitarControles(True);
                end
                else begin
                    FEsConvenioGuardadoOK := True;
                    if (FormStyle = fsNormal) then ModalResult := MROK
                    else close;
                end;
            end;
            // Fin Rev. 6 (SS 887)


        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
        if AgregaObservacionDomicilio then FreeAndNil(ObservacionDomicilio);
        if AgregaObservacionMedioComunicacion then FreeAndNil(ObservacionMedioComunicacion);  
    end;
end;


{-------------------------------------------------------------------------------
Procedure Name	: SicronizarDoc
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.SicronizarDoc;
begin
    ActualizarClases;
    if not FDatosConvenio.ObtenerCambios(FDatosConvenioOriginal) then exit;
end;


{-------------------------------------------------------------------------------
Procedure Name	: ActualizarConvenioYCliente
 Author 		: Nelson Droguett Sierra  (mbecerra)
 Date Created	: 19-Abril-2010
 Description	: Graba en el convenio de baja el Tipo de comprobante electr�nico
                deseado por el cliente.

                Graba en la tabla Personas el Giro, en caso de ser persona natural
                y ha solicitado factura
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.ActualizarConvenioYCliente;
resourcestring
    SQL_CONVENIO	= 'UPDATE Convenio SET TipoComprobanteFiscalAEmitir = ''%s'' WHERE CodigoConvenio = %d';
    SQL_CONV_NULL	= 'UPDATE Convenio SET TipoComprobanteFiscalAEmitir = NULL WHERE CodigoConvenio = %d';
    SQL_PERSONA		= 'UPDATE Personas SET Giro = ''%s'' WHERE CodigoPersona = %d';
begin
	if vcbTipoDocumentoElectronico.Value = TDCBX_AUTOMATICO_COD then begin
    	DMConnections.BaseCAC.Execute(Format(SQL_CONV_NULL, [ FDatosConvenio.CodigoConvenio] ));
    end
    else begin
		DMConnections.BaseCAC.Execute(Format(SQL_CONVENIO, [vcbTipoDocumentoElectronico.Value,
                                                            FDatosConvenio.CodigoConvenio]	));
    end;
    DMConnections.BaseCAC.Execute(Format(SQL_PERSONA, [		FDatosConvenio.Cliente.Datos.Giro,
                                                            FDatosConvenio.CodigoPersona ]	));
end;


{-------------------------------------------------------------------------------
Function Name	: ValidarTagsEnArriendo
Author			: Nelson Droguett Sierra
Date Created	: 19-Abril-2010
Description		: Despliega los mensajes apropiados cuando se da de alta un veh�culo
            	que indican si el costo asociado por arriendo de telev�a en cuotas.
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.ValidarTagsEnArriendo;
resourcestring
    MSG_ERROR_TAGS_ARRIENDO = 'Error al validar Tags en arriendo';
    //MSG_COBRO_450           = 'Debido a que posee env�o de Nota de Cobro por correo postal el cargo mensual ser� de $450. �Est� seguro de continuar?';            //SS-948-MBE-20110413
    //MSG_COBRO_390           = 'Debido a que posee env�o de Nota de Cobro por correo electr�nico el cargo mensual ser� de $390. �Est� seguro de continuar?';       //SS-948-MBE-20110413
    MSG_COBRO_POSTAL        = 'Debido a que posee env�o de Nota de Cobro por correo postal, el cargo mensual de su Cuota de Arriendo de TAG ser� de $%d. �Est� seguro de continuar?';               //SS-948-MBE-20110413
    MSG_COBRO_MAIL          = 'Debido a que posee env�o de Nota de Cobro por correo electr�nico, el cargo mensual de su Cuota de Arriendo de TAG ser� de $%d. �Est� seguro de continuar?';          //SS-948-MBE-20110413
{INICIO: TASK_005_JMA_20160418
    MSG_SQL_MAIL            = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL()';
    MSG_SQL_POSTAL          = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL()';
}
	MSG_SQL_MAIL            = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL(dbo.ObtenerConcesionariaNativa())';
    MSG_SQL_POSTAL          = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL(dbo.ObtenerConcesionariaNativa())';
{TERMINO: TASK_005_JMA_20160418}



    MSG_SQL_VALOR			= 'SELECT dbo.ObtenerImporteTotalConceptoMovimiento(%d, ''%s'')';  		   //SS-948-MBE-20110413
var
    ConceptoMail, ConceptoPostal, ValorConcepto, Indice : integer;
    ValorPostal, ValorMail : Integer;																   //SS-948-MBE-20110413
    FechaDeHoy : TDateTime;																			   //SS-948-MBE-20110413
    TieneMail : boolean;
begin
    try
        ConceptoMail	:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_MAIL);
        ConceptoPostal	:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_POSTAL);
        FechaDeHoy		:= NowBase(DMConnections.BaseCAC);                 							   //SS-948-MBE-20110413
        ValorPostal		:= QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_VALOR, [ConceptoPostal, FormatDateTime('yyyymmdd', FechaDeHoy)]));				//SS-948-MBE-20110413
        ValorMail		:= QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_VALOR, [ConceptoMail, FormatDateTime('yyyymmdd', FechaDeHoy)]));				//SS-948-MBE-20110413
        ValorPostal		:= ValorPostal div 100;                                                 	   //SS-948-MBE-20110413
        ValorMail		:= ValorMail div 100;                                                          //SS-948-MBE-20110413
        //ver si tiene env�o por correo electr�nico
        TieneMail := FDatosConvenio.TieneMedioEnvioPorEMail();

        //revisar todos los vehiculos agregados y si hay Conceptos 213 � 214.
        Result := False;
        Indice := 0;
        while (not Result) and (Indice < FDatosConvenio.Cuentas.CantidadVehiculosACobrar) do begin
            ValorConcepto := FDatosConvenio.Cuentas.ObtenerIDMotivoMovimientoCuentaTelevia(Indice);
            Result := (ValorConcepto = ConceptoMail) or (ValorConcepto = ConceptoPostal);
            Inc(Indice);
        end;
        if Result then begin
            if TieneMail then Result := (MsgBox(Format(MSG_COBRO_MAIL, [ValorMail]), Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES)     //SS-948-MBE-20110413
            else Result := (MsgBox(Format(MSG_COBRO_POSTAL, [ValorPostal]), Caption, MB_YESNO + MB_ICONQUESTION) = IDYES);              //SS-948-MBE-20110413
        end
        else Result := True;
    except on e:exception do begin
            MsgBoxErr(MSG_ERROR_TAGS_ARRIENDO, e.Message, Caption, MB_ICONERROR);
            Result := False;
        end;
    end;

end;


{Procedure Name	: ActualizarConvenioDeBaja
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: es metodo actualiza el bit de generar intereses del convenio cuyo
                   codigo de convenio es ingresado por el parametro codigo de convenio del sp
                   no se reutilizo los metodos implementados por la clase DatosConvenio
                   ya q en el metodo guardar esta hardcodeado el valor del campo estado_convenio
                   y con cada cambio lo pone en estado activo
                   entonces se implemento un sp a tal fin q cabia el estado y inserta un registro
                   en historico_convenio. Se agrego la actualizacion del tipo de cliente para los
                      convenios que estan dados de baja
 Parameters		:
}
procedure TFormModificacionConvenio.ActualizarConvenioDeBaja;
resourcestring
    MSG_ERROR_ACTUALIZAR_GENERAR_INTERESES_CONVENIO = 'Se ha generado un error al actualizar el convenio.';
begin
	if not PuedeGuardarTipoDocumentoElectronico then Exit;
    FDatosConvenio.Cliente.Datos := FreDatoPersona.RegistrodeDatos; // SS 690

   	try
        spConvenio_ActualizarGenerarIntereses.Close;
        spConvenio_ActualizarGenerarIntereses.Parameters.Refresh;
        spConvenio_ActualizarGenerarIntereses.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;
        spConvenio_ActualizarGenerarIntereses.Parameters.ParamByName('@EstadoGenerarIntereses').Value := cb_NoGenerarIntereses.Checked;
        spConvenio_ActualizarGenerarIntereses.Parameters.ParamByName('@UsuarioSistema').Value := UsuarioSistema;
        spConvenio_ActualizarGenerarIntereses.ExecProc;
        ActualizarTipoCliente;
        ActualizarConvenioYCliente();
        MsgBox(MSG_CAPTION_CONVENIO_OK, FMsgBoxCaption, MB_ICONINFORMATION);
        Self.Close;
        ModalResult := mrOk;
    except  on e: Exception do begin
            MsgBoxErr(MSG_ERROR_ACTUALIZAR_GENERAR_INTERESES_CONVENIO, e.Message, Self.Caption, MB_ICONSTOP);
            Exit;
        end;
    end;
end;



{-------------------------------------------------------------------------------
Procedure Name	: PuedeGuardarGenerico
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Determina si puede guardar los datos de pantalla.
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.PuedeGuardarGenerico: Boolean;
resourcestring
    MSG_COMO_SE_ENTERO = 'Responda a la pregunta: ';
    ERROR_DATOS_MEDIO_PAGO = 'Los Datos del Medio de Pago son incorrectos.';
var
    F: TFormMsgNormalizar;
begin
    FDatosConvenio.Cliente.Datos := FreDatoPersona.RegistrodeDatos;
    result := True;
    if FCodigoConcesionariaNativa = FDatosConvenio.CodigoConcesionaria then //TASK_037_ECA_20160622
        if not(FreDatoPersona.ValidarDatos) then begin
            result := False;
            exit;
        end;

    if not PuedeGuardarTipoDocumentoElectronico then Exit;
        // BEGIN SS-1092-MVI-20130723
       //Validar el dominio y sint�xis del mail
        if NBook_Email.PageIndex=0 then begin
            if (EsComponenteObligatorio(txtEMailContacto) and (trim(txtEMailContacto.Text) = '')) then begin
                MsgBoxBalloon(MSG_DEBE_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP, txtEMailContacto);
                txtEMailContacto.SetFocus;
                result := False;
                exit;
            end;
            if (EsComponenteObligatorio(txtEMailContacto) or (trim(txtEMailContacto.Text) <> '')) and (not(IsValidEMailCN(trim(txtEMailContacto.Text)))) then begin      //SS-1092-MVI-20130627
                MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP, txtEMailContacto);                             //SS-1092-MVI-20130627
                txtEMailContacto.SetFocus;
                Result := False;
                Exit;
            end;
            if (EsComponenteObligatorio(txtEMailContacto) or (trim(txtEMailContacto.Text) <> '')) and (not ValidarEmail(DMConnections.BaseCAC, trim(txtEMailContacto.Text))) then begin
                MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, txtEMailContacto);
                txtEMailContacto.SetFocus;
                Result := False;
                Exit;
            end;
        end else begin
            if EsComponenteObligatorio(txtEmailParticular) and (trim(txtEmailParticular.Text) = '') then begin
                MsgBoxBalloon(MSG_DEBE_DIRECCION_EMAIL,Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP, txtEmailParticular);
                txtEmailParticular.SetFocus;
                result:=False;
                exit;
            end;

            if (EsComponenteObligatorio(txtEmailParticular) or (trim(txtEmailParticular.Text) <> '')) and (not(IsValidEMailCN(trim(txtEmailParticular.Text)))) then begin      //SS-1092-MVI-20130627
                MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP, txtEmailParticular);                                 //SS-1092-MVI-20130627
                txtEmailParticular.SetFocus;
                Result := False;
                Exit;
            end;
        end;
        // END SS-1092-MVI-20130723

    //if FDatosConvenio.EsConvenioCN then begin								   //SS_1147_MCA_20140408
    If FDatosConvenio.EsConvenioNativo then begin                              //SS_1147_MCA_20140408
       if not (EsConvenioSinCuenta) then begin                                 //SS_628_ALA_20111202
            if (not(FreTelefonoPrincipal.ValidarTelefono)) then begin
                Result := False;
                Exit;
            end;
            if (not(FreTelefonoSecundario.ValidarTelefono)) then begin
                Result := False;
                Exit;
            end;
        end;


        if not (FMDomicilioPrincipal.ValidarDomicilio) then begin
            result := False;
            Exit;
        end;

        if (RBDomicilioEntrega.Checked = false) and (RBDomicilioEntregaOtro.Checked = false) then begin
            MsgBoxBalloon(MSG_VALIDAR_DOMICILIO_ENTREGA, Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]), MB_ICONSTOP, RBDomicilioEntrega);
            RBDomicilioEntrega.SetFocus;
            Result := False;
            Exit;
        end;

        if not(FMDomicilioPrincipal.EsCalleNormalizada) then begin
            Application.CreateForm(TFormMsgNormalizar, F);
            if f.Inicializa(Caption) and (f.ShowModal = mrCancel) then begin
                f.Release;
                FMDomicilioPrincipal.ForzarBusqueda;
                result := False;
                Exit;
            end;
            f.Release;
        end
        else begin
            if not(FMDomicilioPrincipal.EsNumeroNormalizada) then begin
                if MsgBox(format(MSG_VALIDAR_DOMICILIO_NRO_DESNORAMLIZADO, [STR_DOMICILIO_PARTICULAR]), FMsgBoxCaption, MB_YESNO or MB_ICONWARNING) = IDNO then begin
                    result:=False;
                    exit;
                end;
            end;
        end;

        if RBDomicilioEntregaOtro.Checked then begin
            if FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoCalle<=0 then begin
                if MsgBox(format(MSG_VALIDAR_DOMICILIO_DESNORAMLIZADO, [STR_DOMICILIO_PARTICULAR]), FMsgBoxCaption, MB_YESNO or MB_ICONWARNING) = IDNO then begin
                    RBDomicilioEntrega.SetFocus;
                    Result := False;
                    Exit;
                end;
            end
            else begin
                if FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoSegmento <= 0 then begin
                    if MsgBox(format(MSG_VALIDAR_DOMICILIO_NRO_DESNORAMLIZADO, [STR_DOMICILIO_ALTERNATIVO]), FMsgBoxCaption, MB_YESNO or MB_ICONWARNING) = IDNO then begin
                        result := False;
                        exit;
                    end;
                end;
            end;
        end;
    end;

end;


{Procedure Name	: PuedeGuardarContrato
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Valida que se pueda guardar el contrato
 Parameters		:
}
function TFormModificacionConvenio.PuedeGuardarContrato: Boolean;
var
    i                           : Integer;
    error                       : String;
    CantidadMaximaVehiculos     : Integer;
    Respuesta                   : integer;
resourcestring
    MSG_FALTA_INGRESO                                           = 'Falta ingresar los %s';
    MSG_MEDIO_ENVIO_DETALLE_FACTURACION_SUPERA_LIMITE_VEHICULOS = 'Tiene especificado un Medio de Env�o que no es compatible con la cantidad de veh�culos del convenio.';
    MSG_CAMBIO_MEDIO_ENVIO_DETALLE_FACTURACION                  = 'La cantidad de veh�culos del convenio a superado la cantidad m�xima de veh�culos'
        + CRLF + 'para recibir el Detalle de Tr�nsitos impreso junto a la Nota de Cobro.'
        + CRLF + 'A partir de este momento lo podr� obtener desde un archivo disponible desde la Web.';
    MSG_CAPTION_VALIDAR_ENVIO = 'Existen otros Convenios para este Cliente que tienen marcado el Envio por Email';
    MSG_ESTA_SEGURO = 'Estos medios de env�o se borrar�n tambi�n.' +  CRLF +  'Desea borrar todos los medios de env�o por Email?';
    GUARDAR_CONVENIO_SIN_CUENTA = 'Va a guardar un convenio sin cuenta. �Est� seguro que desea continuar con la operaci�n?';
    MSG_EROR_EXISTE_CONVENIO_SIN_CUENTA                         = 'El Cliente ya posee un Convenio Tipo INFRACTOR.';           //SS_628_ALA_20111219
begin
    //if (FDatosConvenio.Cliente is TPersonaJuridica) and (TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes = 0) and FDatosConvenio.EsConvenioCN then begin		//SS_1147_MCA_20140408
    if (FDatosConvenio.Cliente is TPersonaJuridica) and (TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes = 0) and FDatosConvenio.EsConvenioNativo then begin		//SS_1147_MCA_20140408
        MsgBoxBalloon(format(MSG_FALTA_INGRESO,[STR_REPRESENTANTE]),Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,btn_RepresentanteLegal);
        btn_RepresentanteLegal.SetFocus;
        result:=False;
        exit;
    end;

    if (cb_PagoAutomatico.ItemIndex<0) then begin
        MsgBoxBalloon(format(MSG_VALIDAR_DEBE_EL,[STR_FORMA_PAGO]),Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,cb_PagoAutomatico);
        cb_PagoAutomatico.SetFocus;
        result:=False;
        exit;
    end;

    if FDatosConvenio.MedioDePagoModificado then begin
        if  Assigned(FDatosConvenio.MedioPago.Mandante) and
            (((ModoPantalla = scAlta) and (cb_PagoAutomatico.ItemIndex > 1)) or ((ModoPantalla = scModi) and (cb_PagoAutomatico.ItemIndex > 0))) and //(cb_PagoAutomatico.ItemIndex >= 1) and
            not(TFormMediosPagosConvenio.ValidarMandante(FDatosConvenio.MedioPago.OrigenMandato,
                FDatosConvenio.MedioPago.Mandante.Datos, FDatosConvenio.Cliente.Datos.NumeroDocumento, error)) then begin
            MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO,Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,cb_PagoAutomatico);
            cb_PagoAutomatico.SetFocus;
            result:=False;
            exit;
        end;

        if Assigned(FDatosConvenio.MedioPago.Mandante) and
            (((ModoPantalla = scAlta) and (cb_PagoAutomatico.ItemIndex > 1))
                or ((ModoPantalla = scModi) and (cb_PagoAutomatico.ItemIndex > 0))) and //(cb_PagoAutomatico.ItemIndex >= 1) and
            (not(TFormMediosPagosConvenio.ValidarMedioPago(FDatosConvenio.MedioPago.MedioPago,
                FDatosConvenio.MedioPago.OrigenMandato,
                FDatosConvenio.Cliente.Datos.NumeroDocumento, error, True))) then begin
            MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),
                MB_ICONSTOP, cb_PagoAutomatico);
            cb_PagoAutomatico.SetFocus;
            Result := False;
            Exit;
        end;
    end;

    //INICIO BLOQUE SS_628_ALA_20111219
    //Si el actual convenio es sin cuenta verificamos que no exista alguno ya creado.
//    if EsConvenioSinCuenta() then begin
//
//        if VerificarExisteConvenioSinCuenta(DMConnections.BaseCAC, FDatosConvenio.Cliente.Datos.NumeroDocumento, FDatosConvenio.CodigoConvenio) then begin
//            MsgBoxBalloon(MSG_EROR_EXISTE_CONVENIO_SIN_CUENTA, Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, cbbTipoConvenio);
//            dblVehiculos.SetFocus;
//            Result := False;
//            Exit;
//        end;
//    end;
    //FIN BLOQUE SS_628_ALA_20111219

    // Controlamos que el medio de pago no este cargado en otro convenio del mismo tipo
    //INICIO: TASK_022_ECA_20160601
    {*if NoPermitirMedioPAgoDuplicado(DMConnections.BaseCAC, FDatosConvenio.Cliente.Datos.NumeroDocumento) then begin
        case FDatosConvenio.MedioPago.CodigoMedioPago of
            TPA_NINGUNO: begin
                if ObtenerConvenioMedioPago(DMConnections.BaseCAC,
                                                        FDatosConvenio.Cliente.Datos.NumeroDocumento,
                                                        FDatosConvenio.CodigoConvenio,
                                                        TPA_NINGUNO,
                                                        //-1, '', '', -1, -1,                                                              //SS_628_ALA_20110818
                                                        1) > -1 then begin                                                                 //SS_628_ALA_20110720
                    MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO_DUPLICADO ,MSG_CAPTION_ERROR_MEDIO_PAGO, MB_ICONSTOP, cb_PagoAutomatico);
                    cb_PagoAutomatico.SetFocus;
                    Result := False;
                    Exit;
                end;
            end;
            TPA_PAC: begin
                if ObtenerConvenioMedioPago(DMConnections.BaseCAC,
                                                        FDatosConvenio.Cliente.Datos.NumeroDocumento,
                                                        FDatosConvenio.CodigoConvenio,
                                                        TPA_PAC,
                                                        //-1, '',                                                                         //SS_628_ALA_20110818
                                                        //FDatosConvenio.MedioPago.MedioPago.PAC.NroCuentaBancaria,                       //SS_628_ALA_20110818
                                                        //FDatosConvenio.MedioPago.MedioPago.PAC.CodigoBanco,                             //SS_628_ALA_20110818
                                                        //FDatosConvenio.MedioPago.MedioPago.PAC.TipoCuenta,                              //SS_628_ALA_20110818
                                                        1) > -1 then begin                                                                //SS_628_ALA_20110720
                    MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO_DUPLICADO ,MSG_CAPTION_ERROR_MEDIO_PAGO, MB_ICONINFORMATION, cb_PagoAutomatico);
                    cb_PagoAutomatico.SetFocus;
                    Result := False;
                    Exit;
                end;
            end;
            TPA_PAT: begin
                if ObtenerConvenioMedioPago(DMConnections.BaseCAC,
                                                        FDatosConvenio.Cliente.Datos.NumeroDocumento,
                                                        FDatosConvenio.CodigoConvenio,
                                                        TPA_PAT,
                                                        //FDatosConvenio.MedioPago.MedioPago.PAT.TipoTarjeta,                            //SS_628_ALA_20110818
                                                        //FDatosConvenio.MedioPago.MedioPago.PAT.NroTarjeta,                             //SS_628_ALA_20110818
                                                        //'', -1, -1,                                                                    //SS_628_ALA_20110818
                                                        1) > -1 then begin                                                               //SS_628_ALA_20110720
                    MsgBoxBalloon(MSG_ERROR_MEDIO_PAGO_DUPLICADO ,MSG_CAPTION_ERROR_MEDIO_PAGO, MB_ICONINFORMATION, cb_PagoAutomatico);
                    cb_PagoAutomatico.SetFocus;
                    Result := False;
                    Exit;
                end;
            end;

        end;
    end; *}
    //FIN: TASK_022_ECA_20160601


    // Medio de envio de Doc
    for i := 0 to FDatosConvenio.MediosEnvioPago.CantidadMediosEnvio - 1 do begin
        with FDatosConvenio.MediosEnvioPago.MediosEnvio[i] do begin
            if (CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO) and (FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion = 0) then begin
                MsgBoxBalloon(MSG_ERROR_MEDIO_ENVIO_DOC,Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]),MB_ICONSTOP,btn_MedioDoc);
                btn_MedioDoc.SetFocus;
                Result := False;
                Exit;
            end;

            (* Si existe al Medio de Env�o Detalle de Facturaci�n Impreso junto
            con Nota de Cobro, controlar que la cantidad de veh�culos del
            convenio NO supere el M�ximo para el env�o de Faturaci�n Detallada
            Gratis.
            Si supera la cantidad m�xima y NO tiene E-mail, mostrar un mensaje
            de error.
            Si supera la cantidad m�xima y TIENE E-mail, mostrar un mensaje
            indicando que a partir de ahora se le enviar� por E-mail. *)
            if (CodigoTipoMedioEnvio = CONST_TIPO_MEDIO_ENVIO_CORREO_POSTAL)
                    and (CodigoMedioEnvioDocumento = CONST_MEDIO_ENVIO_DETALLE_FACTURACION) then begin

                ObtenerParametroGeneral(DMConnections.BaseCAC, 'TOPE_VEHICULOS_FACT_DETALLADA', CantidadMaximaVehiculos);
                if (FDatosConvenio.CantidadVehiculos > CantidadMaximaVehiculos) then begin
                    if (FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion = 0) then begin
                        MsgBoxBalloon(MSG_MEDIO_ENVIO_DETALLE_FACTURACION_SUPERA_LIMITE_VEHICULOS,
                            Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]),
                            MB_ICONSTOP, btn_MedioDoc);
                        btn_MedioDoc.SetFocus;
                        Result := False;
                        Exit;
                    end else begin
                        MsgBox(MSG_CAMBIO_MEDIO_ENVIO_DETALLE_FACTURACION,
                            Format(MSG_CAPTION_VALIDAR, [DarCaptionMensaje]),
                            MB_ICONINFORMATION);
                    end;
                end;

            end;

        end;
    end;

    if (FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion = 0) and (FDatosConvenio.TieneOtrosConveniosEnvioMails) and (not FAceptoBorrarEnvios) then begin
        //Avisarle que se van a borrar los mails de otros convenios si desea aceptar o no.
        Respuesta := MsgBox(FDatosConvenio.DescripcionMensajeRestoEnvioMail + CRLF + MSG_ESTA_SEGURO, MSG_CAPTION_VALIDAR_ENVIO, MB_OKCANCEL + MB_ICONQUESTION);
        if Respuesta <> mrOk then begin
            Result := False;
            Exit;
        end
        else begin
            FAceptoBorrarEnvios := True
        end;

    end;
    
{INICIO:  TASK_018_JMA_20160606
    // controlar doc requerida
    if not(FAdministrador) and FDatosConvenio.DocumentacionConvenio.HayDocumentacionSinControlar then begin
            MsgBoxBalloon(MSG_ERROR_DOCUMENTACION,Format(MSG_CAPTION_VALIDAR,[DarCaptionMensaje]), MB_ICONSTOP, btn_DocRequerido);
            btn_DocRequerido.SetFocus;
            result:=False;
            exit;
    end;
 }
{TERMINO:  TASK_018_JMA_20160606}

//    if (EsConvenioSinCuenta) then                                               //SS_628_ALA_20111202    //TASK_005_ECA_20160420
//        if MsgBox(GUARDAR_CONVENIO_SIN_CUENTA, self.Caption, MB_YESNO) = mrNo then begin                 //TASK_005_ECA_20160420
//            dblVehiculos.SetFocus;                                                                       //TASK_005_ECA_20160420
//            result:=False;                                                                               //TASK_005_ECA_20160420
//            exit;                                                                                        //TASK_005_ECA_20160420
//        end;                                                                                             //TASK_005_ECA_20160420
    result:=true;
end;


{-------------------------------------------------------------------------------
Procedure Name	: btn_Baja_ContratoClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Agregu� el llamado a la generaci�n del XML de de Novedades
                  Agregu� el control para que no genere el XML de baja cuando el
        Convenio tiene todas sus cuentas dadas de baja o suspendidas.
        Esto se hizo pues al dar de baja o suspender la �ltima cuenta de un
        convenio, ya se genera el XML de la Baja del Convenio, luego cuando se
        hace la baja "real" del convenio NO hay que generar el XML.
                  Ahora si es el ultimo vehiculo y el convenio
                  tiene saldo, informo que la cuenta no deberia ser dada de baja,
                  pero si el usuario acepta permito realizarlo igual.
        - Se a�ade la llamada al procedimiento Inicializar del objeto
        Form: TFormImprimirConvenio.
        - Se actualiza la propiedad CodigoEstadoConvenio del objeto
        FDatosConvenio: TDatosConvenio
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_Baja_ContratoClick(Sender: TObject);
resourcestring
    MSG_CAN_NOT_DELETE      = 'No se puede dar de baja el convenio porque ha sufrido modificaciones.'#13 +
                              'Guarde o cancele los cambios realizados y vuelva a intentarlo.';
    MSG_QUESTION_BAJA_ULT_CUENTA_WITH_DEBT = 'El Convenio tiene montos adeudados. Recuerde despu�s de dar de baja el convenio facturar la totalidad de los montos';
    MSG_BAJA_CUENTA_ULTIMOS_CONSUMOS_SIN_FACTURAR = 'El Convenio tiene �ltimos consumos sin facturar. Recuerde despu�s de dar de baja el convenio facturar la totalidad de los montos.';
    MSG_CAN_NOT_BAJA        = 'No se puede dar de baja el convenio porque tiene cuentas Suspendidas';
    MSG_CONVENIO_YA_ESTA_BAJA =  'No se puede dar de baja el convenio porque ya est� dado de baja';
    MSG_CONFIRMATION        = 'Confirmar Baja Convenio';
    MSG_PARAM_ERROR         = 'No se encontro el parametro con Cant. de copias';
    STR_CONF_BUTTON_CAPTION = 'Confir&ma Baja';
    MSG_XMLFILE_ERROR       = 'No se gener� archivo de novedades XML';
    MSG_BAJA_IMPRESION_ERROR = 'No se pudo imprimir los documentos del Convenio';
var
    CantCopias, idxVehiculo : Integer;
    Form : TFormImprimirConvenio;
    f: TfrmConfirmarBajaModifConvenio;
    ModalResultado: TModalResult;
    frmEliminarTag: TFormEliminarTag;
    I: Integer;                                                                 //TASK_070_MGO_20160907
    auxVehiculo: TVehiculosConvenio;
    TieneSaldo : Boolean;
    ConceptoIndemnizacion: integer;
    DescripcionConceptoIndemnizacion: string;
begin
    if FEstaConvenioDeBaja then begin
        MsgBox(MSG_CONVENIO_YA_ESTA_BAJA, FMsgBoxCaption, MB_ICONSTOP);
        Exit;
	end;
    if not FPuedeDarBaja then begin
        MsgBox(MSG_CAN_NOT_DELETE, FMsgBoxCaption, MB_ICONSTOP);
        Exit;
	end;

     if GNumeroTurno < 1 then begin
        MsgBox(MSG_ERROR_TURNO_BAJA_CONVENIO, FMsgBoxCaption, MB_ICONSTOP);
        Exit;
    end;

    //Obtengo si tiene saldo el convenio
    TieneSaldo := QueryGetValueINT(DMConnections.BaseCAC,'SELECT DBO.ObtenerSaldoDelConvenio('+ IntToStr(FDatosConvenio.CodigoConvenio) +')') > 0;
    //si es el ultimo vehiculo y el convenio tiene saldo, informo que la cuenta no deberia ser dada de baja, pero si el usuario acepta permito realizarlo igual.
    if TieneSaldo then
        MsgBox(MSG_QUESTION_BAJA_ULT_CUENTA_WITH_DEBT, FMsgBoxCaption, MB_ICONWARNING);


    //si es el ultimo vehiculo y el convenio tiene saldo, informo que la cuenta no deberia ser dada de baja, pero si el usuario acepta permito realizarlo igual.
    if TieneUltimosConsumosSinFacturar(FDatosConvenio.CodigoConvenio) then
        MsgBox(MSG_BAJA_CUENTA_ULTIMOS_CONSUMOS_SIN_FACTURAR, FMsgBoxCaption, MB_ICONWARNING);

    if (MsgBox(format(MSG_QUESTION_BAJA,[STR_CONVENIO]),FMsgBoxCaption,MB_ICONWARNING+MB_YESNO) = mrYes) then begin
		Application.CreateForm(TFormEliminarTag, frmEliminarTag);
        try
            if ((FDatosConvenio.Cuentas.CantidadVehiculos = 0) or (frmEliminarTag.Inicializar(FMsgBoxCaption,FDatosConvenio.Cuentas) and (frmEliminarTag.ShowModal = MROK))) then begin
				try
                    if not ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_BAJA_CONVENIO,CantCopias) then
                        raise Exception.Create(MSG_PARAM_ERROR);
                    ModalResultado := mrCancel;
                    Application.CreateForm(TfrmConfirmarBajaModifConvenio, f);
                    if f.Inicializar(MSG_CONFIRMATION, STR_CONF_BUTTON_CAPTION) then
                        ModalResultado := f.ShowModal;
                    f.Release;

                    if ModalResultado = mrCancel then
                        Exit;

                    // FIXME falta controlar que el convenio no tenga deuda

                    //En el CDS del form de devol de televias quedaron cargados los televias con su estado
                    if (FDatosConvenio.Cuentas.CantidadVehiculos > 0) then begin
                        frmEliminarTag.cdsEliminarTelevia.First;
                        while not frmEliminarTag.cdsEliminarTelevia.Eof do begin
                            idxVehiculo := FDatosConvenio.Cuentas.BuscarIndice(frmEliminarTag.cdsEliminarTelevia.FieldByName('Patente').AsString);
//INICIO: TASK_011_ECA_20160514
                            frmEliminarTag.cdConceptos.First;
                            while not frmEliminarTag.cdConceptos.eof do begin
                                if (frmEliminarTag.cdConceptos.FieldByName('Patente').asString) = (frmEliminarTag.cdsEliminarTelevia.FieldByName('Patente').AsString) then begin
                                    ConceptoIndemnizacion := frmEliminarTag.cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                                    DescripcionConceptoIndemnizacion := frmEliminarTag.cdConceptos.FieldByName('DescripcionMotivo').AsString;
                                    FDatosConvenio.Cuentas.AgregarVehiculoTagACobrar(idxVehiculo, ConceptoIndemnizacion, DescripcionConceptoIndemnizacion, 0, 0, '');
                                end;
                                frmEliminarTag.cdConceptos.Next;
                            end;
//FIN: TASK_011_ECA_20160514
                            FDatosConvenio.Cuentas.CambiarEstadoConservacionTAG(idxVehiculo, frmEliminarTag.EstadoConservacion);
                            // Actualizo los datos del vehiculo
                            auxVehiculo := FDatosConvenio.Cuentas.Vehiculos[idxVehiculo];
                            auxVehiculo.Cuenta.Vehiculo.Robado := frmEliminarTag.cdsEliminarTelevia.FieldByName('Robado').AsBoolean;
                            auxVehiculo.Cuenta.EstadoConservacionTAG := frmEliminarTag.EstadoConservacion;
                            auxVehiculo.EsTagPerdido := False;
                            auxVehiculo.Cuenta.Suspendida := NullDate;
                            auxVehiculo.Cuenta.EstaSuspendida := False;
                            auxVehiculo.Cuenta.SuspensionForzada := False;
                            auxVehiculo.Cuenta.DeadlineSuspension := NullDate;
                            auxVehiculo.Cuenta.CodigoMotivoBaja:=    frmEliminarTag.MotivodeBaja; //TASK_011_ECA_20160514
                            auxVehiculo.Cuenta.DescripcionMotivoBaja:=    frmEliminarTag.DescripcionMotivodeBaja;
                            auxVehiculo.Cuenta.DescripcionEstadoTAG:=    frmEliminarTag.DescripcionEstadoTAG;
                            auxVehiculo.Cuenta.SerialNumberaMostrar := frmEliminarTag.cdsEliminarTelevia.FieldByName('Televia').AsString;
                            auxVehiculo.Cuenta.FechaBajaCuenta := NowBaseSmall(DMConnections.BaseCAC);    // SS_916_PDO_20120215
                            FDatosConvenio.Cuentas.EditarVehiculo(idxVehiculo, auxVehiculo);
                            FDatosConvenio.Cuentas.QuitarVehiculo(idxVehiculo);

                            //Se agrega al listado de bajas forzadas por deshacer si es que tiene               // TASK_070_MGO_20160907
                            FDatosConvenio.BajasForzadas.AgregarVehiculo(auxVehiculo.Cuenta.IndiceVehiculo);    // TASK_070_MGO_20160907

                            frmEliminarTag.cdsEliminarTelevia.Next;
                        end;

                        //INICIO : TASK_070_MGO_20160907
                        if FDatosConvenio.BajasForzadas.CantidadBajasForzadas > 0 then //TASK_056_JMA_20161012
                        begin                                                            //TASK_056_JMA_20161012
                            Application.CreateForm(TFormAltaForzada, formAltaForzada);
                            try
                                if formAltaForzada.Inicializar(FDatosConvenio.BajasForzadas) then begin
                                    if formAltaForzada.ShowModal = mrOk then
                                        for I := 0 to formAltaForzada.FBajasForzadasConvenio.CantidadBajasForzadas - 1 do
                                            FDatosConvenio.BajasForzadas.MarcarBajaForzadaDeshacer(
                                                formAltaForzada.FBajasForzadasConvenio.BajasForzadas[I].CodigoBajaForzada,
                                                formAltaForzada.FBajasForzadasConvenio.BajasForzadas[I].Deshacer);
                                end;
                            finally
                                formAltaForzada.Release;
                            end;
                        end;                                                                  //TASK_056_JMA_20161012
                        // FIN : TASK_070_MGO_20160907
                    end;
                    FDatosConvenio.TurnoAbierto; // Actualizo los datos de los turnos
                    DMConnections.BaseCAC.BeginTrans;

                    FDatosConvenio.Guardar(FCambioAdheridoPA,False); //cb_AdheridoPA.Checked);                //SS-1006-NDR-20100614 //SS-1006-NDR-20111105 TASK_004_ECA_20160411
                    FDatosConvenio.BajaContrato(PuntoEntrega, UsuarioSistema);
                    DMConnections.BaseCAC.CommitTrans;
                    FDatosConvenio.CodigoEstadoConvenio := ESTADO_CONVENIO_BAJA;

                    if FDatosConvenio.CodigoTipoConvenio = CODIGO_TIPO_CONVENIO_RNUT then
                    Begin
                        QueryExecute(DMConnections.BaseCAC, Format('EXEC XML_ConveniosBaja_GenerarArchivo %d', [FDatosConvenio.CodigoConvenio]));


                        MsgBox(MSG_CAPTION_BAJA_CONVENIO,FMsgBoxCaption,MB_ICONINFORMATION);
                        Form := TFormImprimirConvenio.Create(nil);
                        Form.Inicializar(FDatosConvenio);
                        Form.ImprimirBajaConvenio();
                    End;

                    Form.Free;

                    FEsConvenioDadoBajaOK := True;      // SS_964_PDO_20110531
                    if FormStyle = fsNormal then ModalResult := mrOk
                    else close;
                except
                    on E:Exception do begin
                        if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;

                        if E is EExtendedError then MsgBoxErr(EExtendedError(E).Message, EExtendedError(E).DetailMessage, FMsgBoxCaption,MB_ICONSTOP)
                        else MsgBoxErr(MSG_ERROR_BAJA_CONVENIO,e.Message,FMsgBoxCaption,MB_ICONERROR);
                    end;
                end;
            end;
        finally
            frmEliminarTag.Release;
        end;
    end;
end;


Procedure TFormModificacionConvenio.ActualizarCuentaSuscripcion;
var
  Suscribir,DeSuscribir : TStrings;
  i: Integer;
  Resultado: Boolean;
  Mensaje: string;
begin
   Suscribir := TStringList.Create;
   Suscribir.Clear;
   Suscribir.Delimiter       := ';';
   Suscribir.StrictDelimiter := True;
   Suscribir.DelimitedText   := SuscribirVehiculos;
   for i := 0 to Suscribir.Count - 2 do
   begin
       Resultado:=CuentaAltaSuscripcion(FDatosConvenio.CodigoConvenio, Suscribir[i], Mensaje);
   end;
   Suscribir.Free;

   DeSuscribir := TStringList.Create;
   DeSuscribir.Clear;
   DeSuscribir.Delimiter       := ';';
   DeSuscribir.StrictDelimiter := True;
   DeSuscribir.DelimitedText   := DeSuscribirVehiculos;
   for i := 0 to DeSuscribir.Count - 2 do
   begin
      Resultado:=CuentaBajaSuscripcion(FDatosConvenio.CodigoConvenio, DeSuscribir[i], Mensaje);
   end;
   DeSuscribir.Free;
end;


function TFormModificacionConvenio.CuentaAltaSuscripcion(CodigoConvenio: Integer; Patente: string; out Mensaje: string): Boolean;   //TASK_034_ECA_20160611
var
    sp: TADOStoredProc;
    Resultado: Integer;
    IndiceVehiculo: Integer;
begin
    CambiarEstadoCursor(CURSOR_RELOJ);
    Result:=False;
    try
     try
        IndiceVehiculo:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerIndiceVehiculoConvenio('+ IntToStr(CodigoConvenio) + ',''CHL'',''' + Trim(Patente) + ''')');

        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'CRM_CuentasAltaSuscripcion';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoConvenio').Value  := CodigoConvenio;
        sp.Parameters.ParamByName('@IndiceVehiculo').Value  := IndiceVehiculo;
        sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
        sp.Parameters.ParamByName('@Mensaje').Value         := null;
        sp.ExecProc;

        Resultado:= Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
        if Resultado=0 then
        begin
            Result  :=True;
            Mensaje := 'Suscrito de la Lista Blanca';
        end
        else
        begin
            if Resultado<0 then
            begin
                Mensaje:= sp.Parameters.ParamByName('@ErrorDescription').Value;
                raise exception.Create(Mensaje)                                   //TASK_035_ECA_20160620
            end
            else
            begin
                Mensaje:= sp.Parameters.ParamByName('@Mensaje').Value;
                raise exception.Create(Mensaje)                                   //TASK_035_ECA_20160620
            end;
        end;
      except
        on e: Exception do begin
            raise;
        end;
     end;
    finally
        FreeAndNil(sp);
    end;
    CambiarEstadoCursor(CURSOR_DEFECTO);
end;


function TFormModificacionConvenio.CuentaBajaSuscripcion(CodigoConvenio: Integer; Patente: string; out Mensaje: string): Boolean;    //TASK_034_ECA_20160611
var
    sp: TADOStoredProc;
    Resultado: Integer;
    IndiceVehiculo: Integer;
begin
    CambiarEstadoCursor(CURSOR_RELOJ);
    Result:=False;
    try
     try
        IndiceVehiculo:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerIndiceVehiculoConvenio('+ IntToStr(CodigoConvenio) + ',''CHL'',''' + Trim(Patente) + ''')');

        sp:= TADOStoredProc.Create(nil);
        sp.Connection:=DMConnections.BaseCAC;
        sp.ProcedureName := 'CRM_CuentasBajaSuscripcion';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoConvenio').Value  := CodigoConvenio;
        sp.Parameters.ParamByName('@IndiceVehiculo').Value  := IndiceVehiculo;
        sp.Parameters.ParamByName('@Usuario').Value         := UsuarioSistema;
        sp.Parameters.ParamByName('@Mensaje').Value         := null;
        sp.ExecProc;

        Resultado:= Integer(sp.Parameters.ParamByName('@RETURN_VALUE').Value);
        if Resultado=0 then
        begin
            Result  :=True;
            Mensaje := 'Desuscrito de la Lista Blanca';
        end
        else
        begin
            if Resultado<0 then
            begin
                Mensaje:= sp.Parameters.ParamByName('@ErrorDescription').Value;
                raise exception.Create(Mensaje)                                  //TASK_035_ECA_20160620
            end
            else
            begin
                Mensaje:= sp.Parameters.ParamByName('@Mensaje').Value;
                raise exception.Create(Mensaje)                                   //TASK_035_ECA_20160620
            end;
        end;
      except
        on e: Exception do begin
            raise;
        end;
     end;
    finally
        FreeAndNil(sp);
    end;
    CambiarEstadoCursor(CURSOR_DEFECTO);
end;


{$ENDREGION}

{$REGION 'COMUNES'}

{-------------------------------------------------------------------------------
Procedure Name  : HabilitarBotonesVehiculos
Author          : Nelson Droguett Sierra
Date Created	: 19-Abril-2010
Description     : Habilita los botones de la interfaz segun valores de los datos.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.HabilitarBotonesVehiculos;
var
    PuedeEliminarCuentaPerdida: Boolean;
    Vehiculo: TVehiculosConvenio;
begin
    btn_Suspender.Visible     := False; //TASK_041_ECA_20160629
    btn_Suspender.Caption     := CAPTION_BTN_SUSPENDER;
    btn_Perdido.Caption       := '&Perdido';
    //if FDatosConvenio.EsConvenioCN then begin									//SS_1147_MCA_20140408
    if FDatosConvenio.EsConvenioNativo then begin    							//SS_1147_MCA_20140408
        if (cdsVehiculosContacto.RecNo > 0) and not cdsVehiculosContacto.FieldByName('VehiculoEliminado').AsBoolean then begin
            Vehiculo := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString);
            if Vehiculo.EsVehiculoNuevo then begin
                btnAgregarVehiculo.Enabled        := True;
                btnEditarVehiculo.Enabled         := True;
                btnEliminarVehiculo.Enabled       := False;
                btn_Cambiar.Enabled               := False;
                btn_CambioVehiculo.Enabled        := False;
                btn_Suspender.Enabled             := False;
                btn_Perdido.Enabled               := False;
                btn_EximirFacturacion.Enabled     := True;
                btn_BonificarFacturacion.Enabled  := True;
                btnConvFactVehiculo.Enabled       := True; //TASK_004_ECA_20160411
                btnSuscribirLista.Enabled         := False; //TASK_034_ECA_20160611
                btnDesuscribirLista.Enabled       := False; //TASK_034_ECA_20160611
            end else begin
                (* No es un veh�culo nuevo. *)
                if Vehiculo.EsTagPerdido then
                    PuedeEliminarCuentaPerdida := FDatosConvenio.ChekListCuenta.PuedeCambiarElimnarCuenta(Vehiculo.Cuenta.IndiceVehiculo)
                else
                    PuedeEliminarCuentaPerdida := True;

                btnAgregarVehiculo.Enabled     := True;
                btnEditarVehiculo.Enabled      := (not Vehiculo.Cuenta.EstaSuspendida) and not(Vehiculo.EsTagPerdido);
                btnConvFactVehiculo.Enabled    := (not Vehiculo.Cuenta.EstaSuspendida) and not(Vehiculo.EsTagPerdido); //TASK_004_ECA_20160411
                btn_EximirFacturacion.Enabled  := btnEditarVehiculo.Enabled;
                btn_BonificarFacturacion.Enabled := btnEditarVehiculo.Enabled;
                btnEliminarVehiculo.Enabled    := not(Vehiculo.EsTagVendido);
                if Vehiculo.EsTagPerdido and not PuedeEliminarCuentaPerdida then
                    btnEliminarVehiculo.Enabled := False;

                if (Vehiculo.Cuenta.EstaSuspendida) then btn_Suspender.Caption:= CAPTION_BTN_ACTIVAR;

                btn_Suspender.Enabled          := not(Vehiculo.EsTagPerdido);

                btn_Cambiar.Enabled := not (Vehiculo.Cuenta.EstaSuspendida);
                if Vehiculo.EsTagPerdido and not PuedeEliminarCuentaPerdida then
                    btn_Cambiar.Enabled := False;

                btn_CambioVehiculo.Enabled := not(Vehiculo.EsTagPerdido) and not(Vehiculo.EsCambioEstado);
                if Vehiculo.EsTagPerdido and not PuedeEliminarCuentaPerdida then
                    btn_Cambiar.Enabled := False;

                btn_Perdido.Enabled := not (Vehiculo.Cuenta.EstaSuspendida);
                if Vehiculo.EsTagPerdido then begin
                    btn_Perdido.Enabled := True;
                    btn_Perdido.Caption := '&Recuperar';
                end;

                btnSuscribirLista.Enabled         := True; //TASK_034_ECA_20160611
                btnDesuscribirLista.Enabled       := True; //TASK_034_ECA_20160611
            end;
        end else begin
            (* No hay veh�culos o el veh�culo est� dado de baja *)
            btnAgregarVehiculo.Enabled        := True;
            btnEditarVehiculo.Enabled         := False;
            btnEliminarVehiculo.Enabled       := False;
            btn_Cambiar.Enabled               := False;
            btn_CambioVehiculo.Enabled        := False;
            btn_Suspender.Enabled             := False;
            btn_Perdido.Enabled               := False;
            btn_EximirFacturacion.Enabled     := False;
            btn_BonificarFacturacion.Enabled  := False;
            btnConvFactVehiculo.Enabled       := False; //TASK_004_ECA_20160411
            btnSuscribirLista.Enabled         := False; //TASK_034_ECA_20160611
            btnDesuscribirLista.Enabled       := False; //TASK_034_ECA_20160611
        end;
    end else begin
        (* No es Convenio de CN. *)
        btnAgregarVehiculo.Enabled        := False;
        btnEditarVehiculo.Enabled         := False;
        btnEliminarVehiculo.Enabled       := False;
        btn_Cambiar.Enabled               := False;
        btn_CambioVehiculo.Enabled        := False;
        btn_Suspender.Enabled             := False;
        btn_Perdido.Enabled               := False;
        btn_EximirFacturacion.Enabled     := True;
        btn_BonificarFacturacion.Enabled  := True;
        btnConvFactVehiculo.Enabled       := False; //TASK_004_ECA_20160411
        btnSuscribirLista.Enabled         := True; //TASK_034_ECA_20160611
        btnDesuscribirLista.Enabled       := True; //TASK_034_ECA_20160611
    end;

end;

{-------------------------------------------------------------------------------
Procedure Name	: HabilitarControles
 Author 		: dcepeda       (Nelson Droguett Sierra)
 Date Created	: 03-Mayo-2010  (19-Abril-2010)
 Description	: Habilitar los controles de la interfaz, segun los datos.

 Revision: 6
    Author : pdomniguez
    Date   : 16/07/2010
    firma   : SS-887-PDO-20100715
    Description : SS 887 -  Mejorar Performance
        - Se Revisa la l�gica del estado de los controles.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.HabilitarControles(habilita: boolean = true);
var
	AuxDomicilio:Boolean;
begin
    EnableControlsInContainer(FreDatoPersona, habilita and Assigned(FDatosConvenio) and ExisteAcceso('datos_cliente')); //and FDatosConvenio.EsConvenioCN;
    {
    FreDatoPersona.Panel1.Enabled 				:= true;
    FreDatoPersona.cbSexo.Enabled 				:= habilita;
    FreDatoPersona.txt_Nombre.Enabled 			:= habilita;
    FreDatoPersona.txt_Apellido.Enabled			:= habilita;
    FreDatoPersona.txt_ApellidoMaterno.Enabled 	:= habilita;
    FreDatoPersona.txtDocumento.Enabled 		:= not(habilita) and Assigned(FDatosConvenio);
    }
    FreDatoPersona.txtDocumento.ReadOnly        := True;
    {
    FreDatoPersona.cbPersoneria.Enabled 		:= not(habilita) and Assigned(FDatosConvenio);
    FreDatoPersona.lbl_Personeria.Enabled 		:= not(habilita) and Assigned(FDatosConvenio);
    FreDatoPersona.lblRutRun.Enabled 			:= not(habilita) and Assigned(FDatosConvenio);

    with FreDatoPersona do begin
        cboTipoCliente.Enabled := true;
        cboTipoCliente.ListSource.DataSet.EnableControls;
    end;
    }
    if Assigned(FDatosConvenio) and (FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA) then  begin
        btn_RepresentanteLegal.Enabled := True;
    end;
    {
    if ExisteAcceso('datos_medio_comunicacion') then begin
        EnableControlsInContainer(FreTelefonoPrincipal, habilita and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT)));
        EnableControlsInContainer(FreTelefonoSecundario, habilita and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT)));
        lblEmail.Enabled := habilita;
        txtEmailParticular.ReadOnly := not habilita;
        txtEMailContacto.ReadOnly := not habilita;
    end else EnableControlsInContainer(GBMediosComunicacion, False);
    }
    EnableControlsInContainer(GBMediosComunicacion, Habilita and ExisteAcceso('datos_medio_comunicacion'));
    //EnableControlsInContainer(FreTelefonoPrincipal, habilita and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT)));			//SS_1147_MCA_20140408
    EnableControlsInContainer(FreTelefonoPrincipal, habilita and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioNativo or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT)));         //SS_1147_MCA_20140408
    //EnableControlsInContainer(FreTelefonoSecundario, habilita and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT)));          //SS_1147_MCA_20140408
    EnableControlsInContainer(FreTelefonoSecundario, habilita and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioNativo or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT)));        //SS_1147_MCA_20140408

    if FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT then                //TASK_038_ECA_20160625
    begin                                                                       //TASK_038_ECA_20160625
       GBMediosComunicacion.Enabled:=True;                                      //TASK_038_ECA_20160625
    end;

    if ExisteAcceso('datos_domicilios') then begin
        if not(habilita) then AuxDomicilio := False
        else begin
            if ((FMDomicilioPrincipal.EsCalleNormalizada) and not(FMDomicilioPrincipal.cb_Comunas.Enabled))  then AuxDomicilio := False
            else AuxDomicilio := True;
        end;
        //EnableControlsInContainer(GBDomicilios, habilita and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaDomicilioRNUT)));						//SS_1147_MCA_20140408
        EnableControlsInContainer(GBDomicilios, habilita and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioNativo or (FDatosConvenio.Cliente.InactivaDomicilioRNUT)));						//SS_1147_MCA_20140408
        //FMDomicilioPrincipal.cb_Comunas.Enabled := AuxDomicilio and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaDomicilioRNUT));					//SS_1147_MCA_20140408
        FMDomicilioPrincipal.cb_Comunas.Enabled := AuxDomicilio and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioNativo or (FDatosConvenio.Cliente.InactivaDomicilioRNUT));				//SS_1147_MCA_20140408
        //FMDomicilioPrincipal.txt_CodigoPostal.Enabled := AuxDomicilio and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaDomicilioRNUT));			//SS_1147_MCA_20140408
        FMDomicilioPrincipal.txt_CodigoPostal.Enabled := AuxDomicilio and Assigned(FDatosConvenio) and (FDatosConvenio.EsConvenioNativo or (FDatosConvenio.Cliente.InactivaDomicilioRNUT));			//SS_1147_MCA_20140408
    end else EnableControlsInContainer(GBDomicilios, False);
    BtnEditarDomicilioEntrega.enabled := RBDomicilioEntregaOtro.checked and habilita;


    if ExisteAcceso('datos_otros') then begin
        EnableControlsInContainer(GBOtrosDatos, habilita);
        if habilita then begin
            cb_Observaciones.Enabled := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or (not cb_Observaciones.Checked) ;
            cb_RecInfoMail.Enabled := Assigned(FDatosConvenio.Cliente.EMail) and
                                (FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion > 0);
            //INICIO: TASK_038_ECA_20160625
            //chkInactivaDomicilioRNUT.Enabled := not FDatosConvenio.EsConvenioNativo and ExisteAcceso('BIT_Inactiva_RNUT') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); 	//SS_1147_MCA_20140408 // SS_974_PDO_20110810
            //chkInactivaMediosComunicacionRNUT.Enabled := not FDatosConvenio.EsConvenioNativo and ExisteAcceso('BIT_Inactiva_RNUT') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); 	//SS_1147_MCA_20140408 // SS_974_PDO_20110810
            //chkConvenioSinCuenta.Enabled := ExisteAcceso('BIT_Convenio_Sin_Cuenta');   // SS_628_PDO_20120103  // TASK_004_ECA_20160411
            chkInactivaDomicilioRNUT.Enabled :=  ExisteAcceso('BIT_Inactiva_RNUT') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); 	//SS_1147_MCA_20140408 // SS_974_PDO_20110810
            chkInactivaMediosComunicacionRNUT.Enabled := ExisteAcceso('BIT_Inactiva_RNUT') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); 	//SS_1147_MCA_20140408 // SS_974_PDO_20110810
            //FIN: TASK_038_ECA_20160625
        end;
    end else EnableControlsInContainer(GBOtrosDatos, False);

    cb_Observaciones.Enabled := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or (not cb_Observaciones.Checked and habilita) ;
    btn_VerObservaciones.Enabled := (FEstaConvenioDeBaja and (not cb_Observaciones.Checked)) or (cb_Observaciones.Checked and habilita);
    vcbTipoDocumentoElectronico.Enabled := Habilita and ExisteAcceso('Combo_Modificar_Documento_Electronico'); // SS-887-PDO-20100715
    if ExisteAcceso('datos_medio_pago') then begin
        EnableControlsInContainer(cb_PagoAutomatico, habilita);
        EnableControlsInContainer(btn_EditarPagoAutomatico, habilita);
    end else begin
        EnableControlsInContainer(cb_PagoAutomatico, False);
        EnableControlsInContainer(btn_EditarPagoAutomatico, False);
        btn_EditarPagoAutomatico.Caption := CAPTION_MEDIO_PAGO_VER;
    end;
    cb_PagoAutomatico.Enabled := True;
    btn_EditarPagoAutomatico.Enabled := True;
    lbl_DescripcionMedioPago.Enabled := True;
    if habilita then btn_EditarPagoAutomatico.Enabled := cb_PagoAutomatico.Value > TPA_NINGUNO;

    //EnableControlsInContainer(pnl_BotonesVehiculo, habilita and (Assigned(FDatosConvenio) and FDatosConvenio.EsConvenioCN and not FDatosConvenio.SinCuenta));                    //SS_1147_MCA_20140408 //SS_628_ALA_20111202
    EnableControlsInContainer(pnl_BotonesVehiculo, habilita and (Assigned(FDatosConvenio) and FDatosConvenio.EsConvenioNativo and not FDatosConvenio.SinCuenta));                  //SS_1147_MCA_20140408
    //pnl_BotonesVehiculo.Enabled := habilita and (Assigned(FDatosConvenio) and FDatosConvenio.EsConvenioCN); // SS-887-PDO-20100715
    pnl_BotonesVehiculo.Enabled := habilita and Assigned(FDatosConvenio);  // SS_1098_MDI_20130322
    EnableControlsInContainer(pnl_RepLegal, habilita);

    //if (self.Active) and (FreDatoPersona.txtDocumento.Enabled) then begin
     if (self.Active) and (not FreDatoPersona.txtDocumento.ReadOnly) then begin  // SS-887-PDO-20100715
        FreDatoPersona.txtDocumento.SetFocus;
        FreDatoPersona.txtDocumento.SelStart := length(FreDatoPersona.txtDocumento.Text);
        FreDatoPersona.txtDocumento.SelLength := 0;
    end;
    // SS-887-PDO-20100715
    //cb_HabilitadoAMB.Enabled := Habilita and ExisteAcceso('BIT_Habilitado_Lista_Blanca_AMB') and (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE);     // SS-1006-NDR-20111105

    btn_blanquearClaveModif.Enabled := habilita;
    btn_Baja_Contrato.Enabled := habilita;
    btn_ReImprmirModif.Enabled := habilita and (not FSeModificaronDatos);     //SS_1001_ALA_20111020    //SS_1001_ALA_20111220            // SS_960_PDO_20120115
    //btn_ReImprmirModif.Enabled := habilita and (not FSeModificaronDatos) and (Assigned(FDatosConvenio)) and (FDatosConvenio.EsConvenioCN);  // SS_960_PDO_20120115
    btn_Guardar.Enabled := habilita and FSeModificaronDatos;

    cbMostrarVehiculosEliminados.Enabled := habilita;
    dblVehiculos.Enabled := habilita;
    // Fin SS-887-PDO-20100715

    //INICIO BLOQUE SS_1001_ALA_20111220
    if FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE then begin
        //if FDatosConvenio.EsConvenioCN then									//SS_1147_MCA_20140408
        if FDatosConvenio.EsConvenioNativo then begin  							//SS_1147_MCA_20140408
            btn_Baja_Contrato.Enabled                   := True;
        end
        else begin //No es convenio CN pero Vigente
            btn_Baja_Contrato.Enabled                   := False;
            //chkConvenioSinCuenta.Enabled                := False;               //SS_628_ALA_20111207 //TASK_004_ECA_20160411
        end;
    end
    else begin  //No es convenio Vigente
        btn_Baja_Contrato.Enabled                       := False;
        //chkConvenioSinCuenta.Enabled                    := False;               //SS_628_ALA_20111207 //TASK_004_ECA_20160411
        btn_ConvenioFacturacion.Enabled                 := False;               //TASK_008_ECA_20160502
        btn_RUTFacturacion.Enabled                      := False;               //TASK_008_ECA_20160502
        GBOtrosDatos.Enabled                            := False;               //TASK_037_ECA_20160622
        btnSuscribirLista.Visible                       := False;               //TASK_037_ECA_20160622
        btnDesuscribirLista.Visible                     := False;               //TASK_037_ECA_20160622
        FreDatoPersona.cboSemaforo1.Enabled             := False;               //TASK_037_ECA_20160622
        FreDatoPersona.cboSemaforo3.Enabled             := False;               //TASK_037_ECA_20160622
        FreDatoPersona.cboTipoCliente.Enabled           := False;               //TASK_037_ECA_20160622
        btn_RepresentanteLegal.Enabled                  := False;               //TASK_037_ECA_20160622
    end;
    {
    //INICIO BLOQUE SS_1001_ALA_20111020
    FreDatoPersona.cbPersoneria.Enabled    := False;
    //
    if FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE then begin
        if FDatosConvenio.EsConvenioCN then begin
            FreDatoPersona.txt_Nombre.Enabled           := True;
            FreDatoPersona.txt_Apellido.Enabled         := True;
            FreDatoPersona.txt_ApellidoMaterno.Enabled  := True;
            FreDatoPersona.cbSexo.Enabled               := True;
            FreDatoPersona.txtFechaNacimiento.Enabled   := True;
            txtEMailContacto.Enabled                    := True;
            txtEmailParticular.Enabled                  := True;

            btn_blanquearClaveModif.Enabled             := True;
            btn_Baja_Contrato.Enabled                   := True;
            btn_ReImprmirModif.Enabled                  := True;
        end
        else begin //No es convenio CN pero Vigente
            if chkInactivaMediosComunicacionRNUT.Checked then begin
                txtEMailContacto.Enabled                := True;
                txtEmailParticular.Enabled              := True;
            end
            else begin
                txtEMailContacto.Enabled                := False;
                txtEmailParticular.Enabled              := False;
            end;
            FreDatoPersona.txt_Nombre.Enabled           := False;
            FreDatoPersona.txt_Apellido.Enabled         := False;
            FreDatoPersona.txt_ApellidoMaterno.Enabled  := False;
            FreDatoPersona.cbSexo.Enabled               := False;
            FreDatoPersona.txtFechaNacimiento.Enabled   := False;

            btn_blanquearClaveModif.Enabled             := False;
            btn_Baja_Contrato.Enabled                   := False;
            btn_ReImprmirModif.Enabled                  := False;

            btn_EximirFacturacion.Enabled               := True;
            btn_BonificarFacturacion.Enabled            := True;

            chkConvenioSinCuenta.Enabled                := False;               //SS_628_ALA_20111207
        end;
    end
    else begin  //No es convenio Vigente
        FreDatoPersona.txt_Nombre.Enabled               := False;
        FreDatoPersona.txt_Apellido.Enabled             := False;
        FreDatoPersona.txt_ApellidoMaterno.Enabled      := False;
        FreDatoPersona.cbSexo.Enabled                   := False;
        FreDatoPersona.txtFechaNacimiento.Enabled       := False;
        txtEMailContacto.Enabled                        := False;
        txtEmailParticular.Enabled                      := False;
        
        btn_blanquearClaveModif.Enabled                 := False;
        btn_Baja_Contrato.Enabled                       := False;
        btn_ReImprmirModif.Enabled                      := False;

        btn_EximirFacturacion.Enabled                   := False;
        btn_BonificarFacturacion.Enabled                := False;

        chkConvenioSinCuenta.Enabled                    := False;               //SS_628_ALA_20111207
    end;
    //TERMINO BLOQUE SS_1001_ALA_20111020
    //TERMINO BLOQUE SS_1001_ALA_20111220
    }


end;


{-------------------------------------------------------------------------------
Function Name   : BloquearConvenio
Author          : Nelson Droguett Sierra
Date Created    : 19-Abril-2010
Description     : se bloquea o desbloquea un convenio , esto se indica por el parametro
                   bloqueado
                   bloqueado = 0 = se pide desbloquearlo .
                   bloqueado = 1 = se pide bloquearlo.
                   el spConvenio_BloquearConvenio inserta un registro enm la tabla de bloqueos
                   asi nos aseguramos q solo se edite un determinado comvenio por un solo usuario
                   en caso de que el convenio este bloqueado nos retorna el usuario y en que fecha hora
                   se bloqueo.
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.BloquearConvenio(CodigoConvenio,Bloqueado : integer;Usuario : string;var DescriError : string):boolean;
resourcestring
    MSG_ERROR_DE_BLOQUEO = 'A ocurrido un error durante el bloqueo del Convenio ';
    MSG_CONVENIO_EN_MODIFICACION = 'El convenio est� en edici�n desde %s por el usuario: %s.';
    MSG_MODIFICACION_NO_PERMITIDA = 'No se permite editar el convenio';
var
   UsuarioBloqueo  : string;
   FechaBloqueo : TdateTime;
begin
    DescriError := EmptyStr;
    try
        // paso el codigo de convenio y el estado que se le asignara
        //bloqueado o desbloqueado .
        spConvenio_BloquearConvenio.Close;
        spConvenio_BloquearConvenio.Parameters.Refresh;
        spConvenio_BloquearConvenio.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spConvenio_BloquearConvenio.Parameters.ParamByName('@Bloqueado').Value := Bloqueado;
        spConvenio_BloquearConvenio.Parameters.ParamByName('@Usuario').Value := Usuario;
        spConvenio_BloquearConvenio.Parameters.ParamByName('@UsuarioBloqueo').Value := '';
        spConvenio_BloquearConvenio.Parameters.ParamByName('@FechaBloqueo').Value := Date;
        spConvenio_BloquearConvenio.ExecProc;

        if (spConvenio_BloquearConvenio.Parameters.ParamByName('@RETURN_VALUE').Value = 0) then begin
            //si @RETURN_VALUE retorna 0 es que la operacion no se realizo entonces informo el
            //estado bloqueado del convenio y el usuario y desde que fecha esta bloqueado
            UsuarioBloqueo := spConvenio_BloquearConvenio.Parameters.ParamByName('@UsuarioBloqueo').Value;
            FechaBloqueo := spConvenio_BloquearConvenio.Parameters.ParamByName('@FechaBloqueo').Value;
            MsgBox(Format(MSG_CONVENIO_EN_MODIFICACION ,
                         [FormatDatetime('dd/mm/yyyy hh:mm:ss' , FechaBloqueo) ,
                         Trim(UsuarioBloqueo)]) + CRLF + MSG_MODIFICACION_NO_PERMITIDA,
                         Self.Caption, MB_ICONINFORMATION);

            pnlDatosContacto.Enabled:=False;                                    //SS_390_NDR_20131213
            btn_Guardar.Enabled:=False;                                         //SS_390_NDR_20131213
            btn_ReImprmirModif.Enabled:=False;                                  //SS_390_NDR_20131213
        end;
        Result := (spConvenio_BloquearConvenio.Parameters.ParamByName('@RETURN_VALUE').Value = 1);
    except
         on e: Exception do begin
            DescriError := e.Message;
            Result := False;
         end;
    end;
end;


{------------------------------------------------------------------------------
Procedure Name	: ActualizarCalses
 Author 		: dcepeda       (Nelson Droguett Sierra)
 Date Created	: 03-Mayo-2010  (19-Abril-2010)
 Description	: Actualiza variables de atributos de Clase
-------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.ActualizarClases;
var
    AuxTelefono:TTipoMedioComunicacion;
	AuxDomicilio: TDatosDomicilio;
	unIndiceMedio: integer;
begin
    if (FDatosConvenio<>nil) and (FDatosConvenio.Cliente<>nil) then begin
        // datos cliente
        //if (FDatosConvenio.EsConvenioCN or (FDatosConvenio.Cliente.InactivaDomicilioRNUT or FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT)) then begin			//SS_1147_MCA_20140408
        if (FDatosConvenio.EsConvenioNativo or (FDatosConvenio.Cliente.InactivaDomicilioRNUT or FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT)) then begin		//SS_1147_MCA_20140408
            FDatosConvenio.Cliente.Datos:= FreDatoPersona.RegistrodeDatos;

            // actualizo los dantos del mandante si es el mismo.
            if  (FDatosConvenio.MedioPago.TieneMandante) and
                (FDatosConvenio.Cliente.Datos.NumeroDocumento = FDatosConvenio.MedioPago.Mandante.Datos.NumeroDocumento) and
                (FDatosConvenio.Cliente.Datos.TipoDocumento = FDatosConvenio.MedioPago.Mandante.Datos.TipoDocumento) then
                    FDatosConvenio.MedioPago.Mandante.Datos := FDatosConvenio.Cliente.Datos;

            //telefono principal
            if trim(FreTelefonoPrincipal.NumeroTelefono)<>'' then begin
                with AuxTelefono do begin
                    CodigoTipoMedioContacto := FreTelefonoPrincipal.CodigoTipoTelefono;
                    CodigoArea := FreTelefonoPrincipal.CodigoArea;
                    Valor := FreTelefonoPrincipal.NumeroTelefono;
                    Anexo := FreTelefonoPrincipal.Anexo;
                    HoraDesde := FreTelefonoPrincipal.HorarioDesde;
                    HoraHasta := FreTelefonoPrincipal.HorarioHasta;
                    Principal := True;
                    if FDatosConvenio.Cliente.Telefonos.TieneMedioPrincipal then
                        Indice := FDatosConvenio.Cliente.Telefonos.MediosComunicacion[FDatosConvenio.Cliente.Telefonos.IndiceMedioPrincipal].Indice
                    else
                        Indice := -1;
                end;
                if FDatosConvenio.Cliente.Telefonos.TieneMedioPrincipal then
                    FDatosConvenio.Cliente.Telefonos.MediosComunicacion[FDatosConvenio.Cliente.Telefonos.IndiceMedioPrincipal] := AuxTelefono
                else
                    FDatosConvenio.Cliente.Telefonos.AgregarMediocomunicacion(AuxTelefono);
            end else
                    if FDatosConvenio.Cliente.Telefonos.CantidadMediosComunicacion >= 1 then FDatosConvenio.Cliente.Telefonos.QuitarMediocomunicacion(FDatosConvenio.Cliente.Telefonos.IndiceMedioPrincipal);
            //telefono secudnario
            if trim(FreTelefonoSecundario.NumeroTelefono)<>'' then begin
                with AuxTelefono do begin
                    CodigoTipoMedioContacto := FreTelefonoSecundario.CodigoTipoTelefono;
                    CodigoArea := FreTelefonoSecundario.CodigoArea;
                    Valor := FreTelefonoSecundario.NumeroTelefono;
                    Anexo := FreTelefonoSecundario.Anexo;
                    HoraDesde := FreTelefonoSecundario.HorarioDesde;
                    HoraHasta := FreTelefonoSecundario.HorarioHasta;
                    Principal := False;
                    if FDatosConvenio.Cliente.Telefonos.TieneMedioSecundario then
                        Indice := FDatosConvenio.Cliente.Telefonos.MediosComunicacion[FDatosConvenio.Cliente.Telefonos.IndiceMedioSecundario].Indice
                    else
                        Indice := -1;
                end;
                if FDatosConvenio.Cliente.Telefonos.TieneMedioSecundario then
                    FDatosConvenio.Cliente.Telefonos.MediosComunicacion[FDatosConvenio.Cliente.Telefonos.IndiceMedioSecundario] := AuxTelefono
                else
                    FDatosConvenio.Cliente.Telefonos.AgregarMediocomunicacion(AuxTelefono);
            end else   //Lo reemplazo para que borre el telefono correcto.
                    if FDatosConvenio.Cliente.Telefonos.CantidadMediosComunicacion >= 2 then begin
                        //Me fijo si existe un indice para un telefono secunadario y lo borro.
                        unIndiceMedio := FDatosConvenio.Cliente.Telefonos.RetornarIndiceTelefonoSecundario;
                        if unIndiceMedio <> -1 then
                            FDatosConvenio.Cliente.Telefonos.QuitarMediocomunicacion(unIndiceMedio);
                    end;
            // domicilio principal
            AuxDomicilio := FMDomicilioPrincipal.RegDatosDomicilio;
            AuxDomicilio.Principal := True;
            AuxDomicilio.DomicilioEntrega :=  FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.DomicilioEntrega;
            FDatosConvenio.Cliente.Domicilios.EditarDomicilio(0,AuxDomicilio);
        end;

        // mail
        if trim(iif(FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA, TxtEmailParticular.Text, txtEMailContacto.Text))<>'' then begin
            with AuxTelefono do begin
                CodigoTipoMedioContacto := TIPO_MEDIO_CONTACTO_E_MAIL;
                Valor := iif(FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA, TxtEmailParticular.Text, txtEMailContacto.Text);
                Principal := False;
                if FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion > 0 then
                    Indice := FDatosConvenio.Cliente.EMail.MediosComunicacion[0].Indice
                else
                    indice := -1;
            end;
            if FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion>0 then
                FDatosConvenio.Cliente.EMail.MediosComunicacion[0] := AuxTelefono
            else
                FDatosConvenio.Cliente.EMail.AgregarMediocomunicacion(AuxTelefono);
        end else
            if FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion >= 1 then FDatosConvenio.Cliente.EMail.QuitarMediocomunicacion(0);
        // Otros
        FDatosConvenio.UbicacionFisicaCarpeta   := txt_UbicacionFisicaCarpeta.Text;
        FDatosConvenio.NoGenerarIntereses       := cb_NoGenerarIntereses.Checked;
        FDatosConvenio.RecibirInfoPorMail       := cb_RecInfoMail.Checked;
        FDatosConvenio.TipoComprobanteFiscal    := vcbTipoDocumentoElectronico.Value;
        FDatosConvenio.SinCuenta                := IIf((Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_INFRACTOR) or (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL),True,False) //chkConvenioSinCuenta.Checked;    //SS_628_ALA_20111202
    end;
end;


{------------------------------------------------------------------------------
Procedure Name	: DarCaptionMensaje
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.DarCaptionMensaje: String;
begin
	result:=STR_CONTRATO;
end;


{------------------------------------------------------------------------------
Procedure Name	: FormClose
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Accion al cerrar el formulario
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.FormClose(Sender: TObject; var Action: TCloseAction);
    var
        fFacturacionManual: TFacturacionManualForm;
begin
    try
        try
            if not FEsConvenioGuardadoOK then begin                                                                                                                        // SS_964_PDO_20110531
                RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, '', 0, '' , FPuntoEntrega, UsuarioSistema, 'Cancelar');    // SS_964_PDO_20110531
            end;                                                                                                                                                           // SS_964_PDO_20110531

            if (not BloquearConvenio(FDatosConvenio.CodigoConvenio , 0 , UsuarioSistema , DescriError)) then begin
                if  DescriError <> EmptyStr then begin
                    MsgBoxErr( MSG_ERROR_BLOQUEO_CONVENIO, DescriError , Self.Caption , MB_ICONSTOP);
                end;
            end;

            if FEsConvenioGuardadoOK then begin                                                                                                                                // SS_964_PDO_20110531
                with spExistenMovimientosFacturacionInmediata do begin                                                                                                         // SS_964_PDO_20110531
                    if Active then Close;                                                                                                                                      // SS_964_PDO_20110531
                                                                                                                                                                               // SS_964_PDO_20110531
                    Parameters.Refresh;                                                                                                                                        // SS_964_PDO_20110531
                    Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;                                                                          // SS_964_PDO_20110531
                    Parameters.ParamByName('@TieneMovimientos').Value := NULL;                                                                                                 // SS_964_PDO_20110531
                    ExecProc;                                                                                                                                                  // SS_964_PDO_20110531
                                                                                                                                                                               // SS_964_PDO_20110531
                    if Parameters.ParamByName('@TieneMovimientos').Value = True then begin                                                                                     // SS_964_PDO_20110531
                        if FindFormOrCreate(TFacturacionManualForm, fFacturacionManual) then begin                                                                             // SS_964_PDO_20110531
                            fFacturacionManual.Release                                                                                                                         // SS_964_PDO_20110531
                        end                                                                                                                                                    // SS_964_PDO_20110531
                        else FindFormOrCreate(TFacturacionManualForm, fFacturacionManual);                                                                                     // SS_964_PDO_20110531
                                                                                                                                                                               // SS_964_PDO_20110531
                        if FFacturacionManual.Inicializar(TC_FACTURACION_INMEDIATA, False ) then begin                                                                         // SS_964_PDO_20110531
                            FFacturacionManual.peRUTCliente.Text := FDatosConvenio.Cliente.Datos.NumeroDocumento;                                                              // SS_964_PDO_20110531
                            FFacturacionManual.peRUTCliente.OnChange(nil);                                                                                                     // SS_964_PDO_20110531
                            FFacturacionManual.btnBuscar.Click;                                                                                                                // SS_964_PDO_20110531
                            FFacturacionManual.cbConveniosCliente.ItemIndex := FFacturacionManual.cbConveniosCliente.Items.IndexOfValue(FDatosConvenio.CodigoConvenio);        // SS_964_PDO_20110531
                            FFacturacionManual.cbConveniosCliente.OnChange(nil);                                                                                               // SS_964_PDO_20110531
                            FFacturacionManual.Show;                                                                                                                           // SS_964_PDO_20110531
                        end;                                                                                                                                                   // SS_964_PDO_20110531
                    end;                                                                                                                                                       // SS_964_PDO_20110531
                end;                                                                                                                                                           // SS_964_PDO_20110531
            end;                                                                                                                                                               // SS_964_PDO_20110531

            if FEsConvenioDadoBajaOK  and TieneUltimosConsumosSinFacturar(FDatosConvenio.CodigoConvenio) then begin
                if FindFormOrCreate(TFacturacionManualForm, fFacturacionManual) then begin                                                                                     // SS_964_PDO_20110531
                    fFacturacionManual.Release                                                                                                                                 // SS_964_PDO_20110531
                end                                                                                                                                                            // SS_964_PDO_20110531
                else FindFormOrCreate(TFacturacionManualForm, fFacturacionManual);                                                                                             // SS_964_PDO_20110531
                                                                                                                                                                               // SS_964_PDO_20110531
                if FFacturacionManual.Inicializar(TC_FACTURACION_POR_BAJA, False ) then begin                                                                                  // SS_964_PDO_20110531
                    FFacturacionManual.peRUTCliente.Text := FDatosConvenio.Cliente.Datos.NumeroDocumento;                                                                      // SS_964_PDO_20110531
                    FFacturacionManual.peRUTCliente.OnChange(nil);                                                                                                             // SS_964_PDO_20110531
                    FFacturacionManual.btnBuscar.Click;                                                                                                                        // SS_964_PDO_20110531
                    FFacturacionManual.cbConveniosCliente.ItemIndex := FFacturacionManual.cbConveniosCliente.Items.IndexOfValue(FDatosConvenio.CodigoConvenio);                // SS_964_PDO_20110531
                    FFacturacionManual.cbConveniosCliente.OnChange(nil);                                                                                                       // SS_964_PDO_20110531
                    FFacturacionManual.Show;                                                                                                                                   // SS_964_PDO_20110531
                end;                                                                                                                                                           // SS_964_PDO_20110531
            end;                                                                                                                                                               // SS_964_PDO_20110531
        except                                                                                                                                                                 // SS_964_PDO_20110531
            on e:Exception do begin                                                                                                                                            // SS_964_PDO_20110531
                ShowMsgBoxCN(e, Self);                                                                                                                                         // SS_964_PDO_20110531
            end;                                                                                                                                                               // SS_964_PDO_20110531
        end;                                                                                                                                                                   // SS_964_PDO_20110531
    finally                                                                                                                                                                    // SS_964_PDO_20110531
        FreeAndNil(FDatosConvenio);
        FreeAndNil(FDatosConvenioOriginal);
        FreeAndNil(FConvenioFactory);

        if FormStyle <> fsNormal then Action :=  caFree;
    end;                                                                                                                                                                       // SS_964_PDO_20110531
end;


{-------------------------------------------------------------------------------
Procedure Name	: btn_SalirClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_SalirClick(Sender: TObject);
begin
    if FormStyle = fsNormal then
        ModalResult := mrCancel
    else
        close;
end;


{------------------------------------------------------------------------------
Procedure Name	: btn_ReImprmirModifClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_ReImprmirModifClick(Sender: TObject);
var
    f : TFrmSeleccionarReimpresion;
begin                              
    Application.CreateForm(TFrmSeleccionarReimpresion,f);
    if f.Inicializar(Caption, FDatosConvenio, FDatosConvenioOriginal, PuntoEntrega, nil, True) and (f.ShowModal = mrOK) then begin
        if f.MotivoCancelacionImpresion = ERROR then MsgBox(MSG_ERROR_IMPRIMIR_CONVENIO, FMsgBoxCaption, MB_ICONSTOP);              //SS-1006-NDR-20120727
        if f.MotivoCancelacionImpresion = OK then
        begin
{INICIO: TASK_011_JMA_20160505
            //MsgBox(MSG_CAPTION_IMPRESION_OK, FMsgBoxCaption, MB_ICONINFORMATION);             //SS-1006-NDR-20120727
}
            self.Close;
{TERMINO: TASK_011_JMA_20160505}
        end;
	end;

    f.Release;
end;

{------------------------------------------------------------------------------
Procedure Name	: btn_BlanquearClaveConsultaClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_BlanquearClaveConsultaClick(Sender: TObject);
var
	Clave : string;
    DatosAux : TDatosPersonales;

begin

    if MsgBox(MSG_QUETION_BLANQUEAR_CLAVE,FMsgBoxCaption,MB_ICONQUESTION or MB_YESNO) = IDYES then begin
        try
            with BlanquearPasswordPersona do begin
                Parameters.ParamByName('@CodigoDocumento').Value := FDatosConvenio.Cliente.Datos.TipoDocumento;
                Parameters.ParamByName('@NumeroDocumento').Value := FDatosConvenio.Cliente.Datos.NumeroDocumento;
                Parameters.ParamByName('@Password').Value        := ''; // es un parametro de salida
                ExecProc;
                Clave := Trim(Parameters.ParamByName('@Password').Value);
                DatosAux := FreDatoPersona.RegistrodeDatos;
                DatosAux.Password := Clave;
                FreDatoPersona.RegistrodeDatos := DatosAux;
                MsgBox(format(MSG_CAPTION_BLANQUEAR_CLAVE,[Clave]),FMsgBoxCaption,MB_ICONINFORMATION);
            end;
        Except
            on e:Exception do MsgBoxErr(MSG_ERROR_BLANQUEAR_CLAVE,e.Message,FMsgBoxCaption,MB_ICONSTOP);
        end;
        BlanquearPasswordPersona.close;
    end;
end;


{-------------------------------------------------------------------------------
Procedure Name	: debugerClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.debugerClick(Sender: TObject);
resourcestring
    STR_DOC  = 'DOC: ';
    STR_INFO = ' - info ';
    STR_CANT = ' - Cant ';
var
    i:Integer;
begin
    ActualizarClases;
    FDatosConvenio.ObtenerCambios(FDatosConvenioOriginal);
	with FDatosConvenio.DocumentacionConvenio do
        for i := 0 to CantidadDocumentacion do
            with DarDocumentacion(i) do
                ShowMessage(STR_DOC + inttostr(CodigoDocumentacionRespaldo) + STR_INFO + InformacionExtra + ' - Fecha Baja ' + FormatDateTime('dd/mm/yy',FechaBaja)+ STR_CANT + IntToStr(CantidadDocumentacion + 1));
end;


{$ENDREGION}






{-------------------------------------------------------------------------------
Procedure Name	: mnu_EnviarMailClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.mnu_EnviarMailClick(Sender: TObject);
begin
    if not InvocarCorreoPredeterminado(TEdit(PopupMenu.PopupComponent), TEdit(PopupMenu.PopupComponent).Text, '', '') then begin
        MsgBoxErr(MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO, MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO, MSG_CAPTION_ENVIO, MB_ICONERROR);
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: PopupMenuPopup
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.PopupMenuPopup(Sender: TObject);
begin
     mnu_EnviarMail.Enabled := (Trim(TEdit(TPopupMenu(Sender).PopupComponent).Text) <> '') and IsValidEMail(Trim(TEdit(TPopupMenu(Sender).PopupComponent).Text));
end;

{-------------------------------------------------------------------------------
Procedure Name	: lblEnviarMouseEnter
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.lblEnviarMouseEnter(Sender: TObject);
begin
    if NBook_Email.PageIndex = 0 then begin
        if ((Trim(txtEMailContacto.Text) = '') or not IsValidEMail(Trim(txtEMailContacto.Text))) then Exit;
    end
    else
		if ((Trim(txtEmailParticular.Text) = '') or not IsValidEMail(Trim(txtEmailParticular.Text))) then Exit;

    lblEnviar.Cursor := crHandPoint;
    lblEnviar.Font.Style := lblEnviar.Font.Style + [fsUnderline];
    lblEnviar.Font.Color := clblue;
end;

{------------------------------------------------------------------------------
Procedure Name	: lblEnviarMouseLeave
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.lblEnviarMouseLeave(Sender: TObject);
begin
    lblEnviar.Cursor := crDefault;
    lblEnviar.Font.Style := lblEnviar.Font.Style - [fsUnderline];
    lblEnviar.Font.Color := clBlack;
end;


{-------------------------------------------------------------------------------
Procedure Name	: lblEnviarClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.lblEnviarClick(Sender: TObject);
begin
    if lblEnviar.Cursor = crHandPoint then begin
        if NBook_Email.PageIndex = 0 then begin
            if not InvocarCorreoPredeterminado(txtEMailContacto, txtEMailContacto.Text, '', '') then begin
                MsgBoxErr(MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO, MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO, MSG_CAPTION_ENVIO, MB_ICONERROR);
            end;
        end
        else begin
            if not InvocarCorreoPredeterminado(txtEmailParticular, txtEmailParticular.Text, '', '') then begin
                MsgBoxErr(MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO, MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO, MSG_CAPTION_ENVIO, MB_ICONERROR);
            end;
        end;
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: FormActivate
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.FMDomicilioPrincipalcb_ComunasChange(Sender: TObject);
begin
    FMDomicilioPrincipal.cb_ComunasChange(Sender);
    ActualizaControlesPorModificacion(FMDomicilioPrincipal.Tag = 1);    
end;

procedure TFormModificacionConvenio.FMDomicilioPrincipalcb_RegionesChange(Sender: TObject);
begin
    FMDomicilioPrincipal.cb_RegionesChange(Sender);
    ActualizaControlesPorModificacion(FMDomicilioPrincipal.Tag = 1);
end;

procedure TFormModificacionConvenio.FormActivate(Sender: TObject);
begin
    DispararObservaciones;
    FCambioDomicilio := RBDomicilioEntrega.Checked;
    FCambioDomicilioOtro := RBDomicilioEntregaOtro.checked;
end;

{-------------------------------------------------------------------------------
Procedure Name	: DispararObservaciones
 Author 		: Nelson Droguett Sierra (neFernandez)
 Date Created	: 19-Abril-2010
 Description	:Se muestra un aviso con las observaciones de las cuentas
    del Cliente (RUT) en cuesti�n.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.DispararObservaciones;
resourcestring                                                                                        //SS_660_MVI_20130909
    MSG_ALERTA ='Validaci�n Cliente';                                                                 //SS_660_MVI_20130909
    MSG_ALERTA_CLIENTE_LISTA_AMARILLA='EL Cliente posee al menos un convenio en lista amarilla';      //SS_660_MVI_20130909
    SQLValidarClienteListaAmarilla  = 'SELECT dbo.EstaPersonaEnListaAmarilla(%d)';                    //SS_660_MVI_20130909
var
    Texto: AnsiString;
    PersonaEnListaAmarilla: Integer;                                                                  //SS_660_MVI_20130909
begin
    if ( FPrimeraVez and (ModoPantalla = scModi) ) then begin

        EstaClienteConMensajeEspecial(DMCOnnections.BaseCAC, Trim(FreDatoPersona.txtDocumento.Text));                               //SS_1408_MCA_20151027

        Texto := MensajeRUTConvenioCuentas(DMCOnnections.BaseCAC, Trim(FreDatoPersona.txtDocumento.Text));
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);

        PersonaEnListaAmarilla := QueryGetIntegerValue(DMConnections.BaseCAC, Format(SQLValidarClienteListaAmarilla, [FDatosConvenio.CodigoPersona]));  //SS_660_MVI_20130909
        if PersonaEnListaAmarilla = 1 then begin                                                                                                        //SS_660_MVI_20130909
           ShowMsgBoxCN(MSG_ALERTA, MSG_ALERTA_CLIENTE_LISTA_AMARILLA, MB_ICONWARNING, Self);                                                           //SS_660_MVI_20130909
            end;                                                                                                                                        //SS_660_MVI_20130909
    end;

    if FPrimeraVez and (FDatosConvenio.ListObservacionConvenio.TieneObservaciones or
                        FDatosConvenio.CheckListConvenio.TieneMarcados or
                        FDatosConvenio.ChekListCuenta.TieneItems) then
        btn_VerObservaciones.Click;
    FPrimeraVez := False;
end;

{-------------------------------------------------------------------------------
Procedure Name	: ActualizarTipoCliente
 Author 		: Nelson Droguett Sierra (lCanteros)
 Date Created	: 19-Abril-2010
 Description	: Actualiza el tipo de cliente en la tabla personas lo uso cuando el convenio se dio de baja
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.ActualizarTipoCliente;
    var
        spActualizar: TADOStoredProc;
begin
    spActualizar := TADOStoredProc.Create(nil);
    try
        with spActualizar, Parameters do begin
          Connection := DMConnections.BaseCAC;
          ProcedureName := 'TipoClienteModificarTipo';
          Refresh;
          ParamByName('@Tipo').Value := FreDatoPersona.RegistrodeDatos.TipoCliente;
          ParamByName('@Semaforo1').Value := FDatosConvenio.Cliente.Datos.Semaforo1; // SS 690
          ParamByName('@Semaforo3').Value := FDatosConvenio.Cliente.Datos.Semaforo3; // SS 690
          ParamByName('@Codigo').Value := FDatosConvenio.Cliente.Datos.CodigoPersona;
          ParamByName('@CodigoUsuario').Value := UsuarioSistema;                     //SS_1419_NDR_20151130
          ExecProc;
        end;
    finally
      spActualizar.Close;
      spActualizar.Free;
    end;
end;


{-------------------------------------------------------------------------------
Procedure Name	: cbMostrarVehiculosEliminadosClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.cbbTipoClienteChange(Sender: TObject);
begin
    btn_Guardar.Enabled := True; //TASK_040_ECA_20160628
end;

procedure TFormModificacionConvenio.cbMostrarVehiculosEliminadosClick(
  Sender: TObject);
begin
  //Rev.5 / 09-Junio-2010-----------------------------------------------------
  try
    DSVehiculosContacto.DataSet := Nil;
    cdsVehiculosContacto.DisableControls;
    FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
  finally
    cdsVehiculosContacto.EnableControls;
    DSVehiculosContacto.DataSet := cdsVehiculosContacto;
  end;
  //Rev.5---------------------------------------------------------------------
end;

{-------------------------------------------------------------------------------
Procedure Name	: dblVehiculosColumnsHeaderClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.dblVehiculosColumnsHeaderClick(
  Sender: TObject);
var
    i: Integer;
begin
    if not cdsVehiculosContacto.Active then
        Exit;

    for i := 0 to dblVehiculos.Columns.Count-1 do
        if dblVehiculos.Columns[i] <> Sender then
			dblVehiculos.Columns[i].Sorting := csNone;

    if TDBListExColumn(Sender).Sorting = csAscending then
        TDBListExColumn(Sender).Sorting := csDescending
    else
        TDBListExColumn(Sender).Sorting := csAscending;

    with cdsVehiculosContacto do begin
        DisableControls;
        try
            if IndexName <> EmptyStr then
                DeleteIndex('xOrder');
            AddIndex('xOrder', TDBListExColumn(Sender).FieldName, [],
                     iif(TDBListExColumn(Sender).Sorting = csDescending, TDBListExColumn(Sender).FieldName, EmptyStr));
            IndexName := 'xOrder';
        finally
            EnableControls;
        end;
    end;
end;

{Procedure Name	: btnAgregarVehiculoClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
 Parameters		:

Revision 		: 1
Author      : mpiazza
Date        : 23/03/2010
Description : ss-870 : se agrega validacion de turno abierto
}
procedure TFormModificacionConvenio.btnAgregarVehiculoClick(
  Sender: TObject);
ResourceString
    MSG_ERROR_EDITAR_VEHICULO      = 'No se pudieron modificar los datos del veh�culo';
    MSG_EDITAR_VEHICULO_CAPTION    = 'Actualizar datos del veh�culo';
    MSG_DADO_BAJA                  = 'El veh�culo que est� dando de alta, ya posee una operaci�n de baja en esta ventana. No se puede editar la fecha de alta';
        MSG_ES_SIN_CUENTA          = 'No puede agregar Cuentas a este Convenio ya que se encuentra marcado como Sin Cuenta';      //SS_628_ALA_20111202
var
    f : TFormSCDatosVehiculo;
    Cadena, CadenaTags: TStringList;
    auxVehiculo:TVehiculosConvenio;
    IDMotivoMovCuentaTelevia,
    ConceptoIndemnizacion           : integer;
    DescripcionConceptoIndemnizacion: string;
    DescriError                     : string;
    Index                           : integer;
    ListaVouchers                   : TStringList;

begin
    //INICIO: TASK_005_ECA_20160420
    if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL then
    begin
        MostrarVehiculosRUT();
        Exit;
    end;
    //FIN: TASK_005_ECA_20160420

    if not EsConvenioSinCuenta then begin                                                                                           //SS_628_ALA_20111202
        //MostrarTagsVencidos(Trim(FreDatoPersona.txtDocumento.Text), DMConnections.BaseCAC); //SS-960-ALA-20110411                 //SS_960_ALA_20111011

        // Aviso que el RUT tiene alguna cuenta suspendida
        if (FDatosConvenio.TieneCuentasSuspendidas) and
            (MsgBox(MSG_QUESTION_ALTA_TAG, Caption, MB_YESNO or MB_ICONQUESTION) <> idYES) then exit;

        //Controlamos que el turno este abierto
         if GNumeroTurno < 1 then begin
            MsgBox(MSG_ERROR_TURNO_MODIF_CONVENIO, FMsgBoxCaption, MB_ICONSTOP);
            Exit;
        end;

        // Aviso si el cliente tiene un tag vendido y sin usar
        if FDatosConvenio.TieneTagVendidoSinUsar > 0 then
            MsgBox(format(MSG_TAG_SIN_USAR,[FDatosConvenio.TieneTagVendidoSinUsar]), FMsgBoxCaption, MB_ICONINFORMATION);

        // Alta propiamente dicha
        Application.CreateForm(TFormSCDatosVehiculo, f);
        Cadena := TStringList.Create;
        CadenaTags := TStringList.Create;
        //Rev.5 / 09-Junio-2010 / Nelson Droguett Sierra ----------------------------
        //ArmarListaPatentes(Cadena);
        //ArmarListaTags(CadenaTags);
        ArmarListaPatentesTags(Cadena,
                               CadenaTags
                              );
        //FinRev.5-------------------------------------------------------------------
        ListaVouchers := TStringList.Create;
        FDatosConvenio.ArmarListaVouchers(ListaVouchers);

        if f.Inicializar(MSG_CAPTION_AGREGAR_VEHICULO,
                        FPuntoEntrega,
                        1,//iif(cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger = 0, 1,cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger),
                        -1,
                        Cadena,
                        true,
                        CadenaTags,
                        -1,
                        FDatosConvenio.CodigoConvenio,
                        cdsVehiculosContacto,
                        FDatosConvenio.Cliente.Datos.CodigoPersona,
                        Peatypes.ConvenioAltaVehiculo,
                        ListaVouchers,
                        FDatosConvenio)     // SS_916_PDO_20120109
        and (f.ShowModal = mrOK) then begin
            //Cargo el Vehiculo en la Lista.
            try
                if not RegistrarLogOperacionConvenio( 	FreDatoPersona.txtDocumento.Text,
                                                        FDatosConvenio.CodigoConvenio,
                                                        f.DatoCuenta.Vehiculo.Patente,
                                                        f.DatoCuenta.IndiceVehiculo,
                                                        f.NumeroTag, FPuntoEntrega,
                                                        UsuarioSistema,
                                                        'Alta')  then
                    raise exception.Create('Error registrando auditoria');

                auxVehiculo.Cuenta := f.DatoCuenta;

                AuxVehiculo.EsVehiculoRobado        := f.DatoCuenta.Vehiculo.Robado;
                AuxVehiculo.EsVehiculoRecuperado    := f.DatoCuenta.Vehiculo.Recuperado;
                auxVehiculo.EsForaneo               := f.DatoCuenta.Vehiculo.Foraneo; // 20160603 MGO

                auxVehiculo.Cuenta.FechaAltaTag             := f.FechaAlta;     // SS_916_PDO_20120109
                auxVehiculo.Cuenta.FechaCreacion            := f.FechaAlta;     // SS_916_PDO_20120109
                auxVehiculo.Cuenta.TieneFechaRetroactiva    := f.CambioFecha;   // SS_916_PDO_20120109

                auxVehiculo.Cuenta.CodigoAlmacenDestino := f.AlmacenDestino ;
                auxVehiculo.Documentacion               := nil;

                //Tengo que guardar el tipo de asignacion porque despu�s lo necesitar� para grabar la fecha vencimiento.
                auxVehiculo.Cuenta.TipoAsignacionTag := f.TipoAsignacionTag;
                auxVehiculo.Cuenta.FechaVencimientoTag := f.FechaVencimientoTag;        //REV.8

                //INICIO: TASK_009_ECA_20160506
                auxVehiculo.Cuenta.CodigoConvenioFacturacion:=FDatosConvenio.CodigoConvenioFacturacion;
                auxVehiculo.Cuenta.NumeroConvenioFacturacion:=FDatosConvenio.NumeroConvenio;
                //FIN: TASK_009_ECA_20160506
                auxVehiculo.Cuenta.Vehiculo.CodigoColor:= f.Color;
                //AuxVehiculo.Cuenta.AdheridoPA := FDatosConvenio.AdheridoPA;         //SS-1006-NDR-20120715 TASK_004_ECA_20160411

                //Bajo esta parte para que se refresque la fecha de vencimiento de la garantia del tag en la grilla.
                FDatosConvenio.Cuentas.AgregarVehiculo(auxVehiculo);
                //FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);

                //esta en la fecha de vencimiento de la garantia
                //comentado en REV.8 
                //REV.8 if FDatosConvenio.Cuentas.CalcularVencimientoGarantiaTag(FDatosConvenio.Cuentas.BuscarIndice(auxVehiculo.Cuenta.Vehiculo.Patente) , f.TipoAsignacionTag , DescriError) then begin
                    f.cdConceptos.First;
                    while not f.cdConceptos.eof do begin
                        ConceptoIndemnizacion				:= f.cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                        DescripcionConceptoIndemnizacion    := f.cdConceptos.FieldByName('DescripcionMotivo').AsString;
                        IDMotivoMovCuentaTelevia			:= f.cdConceptos.FieldByName('IDMotivoMovCuentaTelevia').AsInteger;
                        Index                               := FDatosConvenio.Cuentas.BuscarIndice(auxVehiculo.Cuenta.Vehiculo.Patente) ;
                        IDMotivoMovCuentaTelevia            := ConceptoIndemnizacion;
                        FDatosConvenio.Cuentas.AgregarVehiculoTagACobrar(Index, IDMotivoMovCuentaTelevia, ConceptoIndemnizacion, f.NumeroVoucher, f.AlmacenDestino, DescripcionConceptoIndemnizacion, f.TipoAsignacionTag);
                        f.cdConceptos.Next;
                    end;
                    FPuedeDarBaja := False;
                //REV.8 end else begin
                //REV.8    MsgBoxErr( MSG_ERROR_EDITAR_VEHICULO, DescriError, MSG_EDITAR_VEHICULO_CAPTION, MB_ICONSTOP);
                //REV.8    dsVehiculosContacto.DataSet.Cancel;
                //REV.8 end;
            
                //Rev.5 / 09-Junio-2010-----------------------------------------------------
                try
                  DSVehiculosContacto.DataSet := Nil;
                  cdsVehiculosContacto.DisableControls;
                  FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                finally
                  cdsVehiculosContacto.EnableControls;
                  DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                end;
                //Rev.5---------------------------------------------------------------------
                ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
            except
                On E: Exception do begin
                    MsgBoxErr( MSG_ERROR_EDITAR_VEHICULO, E.message, MSG_EDITAR_VEHICULO_CAPTION, MB_ICONSTOP);
                    dsVehiculosContacto.DataSet.Cancel;
                end;
            end;
        end;
        Cadena.Free;
        CadenaTags.Free;
        ListaVouchers.Free;
        f.Release;
    end                                                                                                              //SS_628_ALA_20111202
    else begin                                                                                                       //SS_628_ALA_20111202
        MsgBoxBalloon(MSG_ES_SIN_CUENTA, Self.Caption, MB_ICONSTOP, cbbTipoConvenio);                           //SS_628_ALA_20111202
    end;                                                                                                             //SS_628_ALA_20111202
end;




{-------------------------------------------------------------------------------
Procedure Name	: btnEliminarVehiculoClick
 Author 		: Nelson Droguett Sierra (lgisuk)
 Date Created	: 19-Abril-2010
 Description	: Ahora si es el ultimo vehiculo y el convenio
                  tiene saldo, informo que la cuenta no deberia ser dada de baja,
                  pero si el usuario acepta permito realizarlo igual.
                   Dejo el valor en Suspendida, ya que para saber si est� o no suspendida
    nos basamos en la propiedad booleana EstaSuspendida.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btnEliminarVehiculoClick(Sender: TObject);
ResourceString
    MSG_DELETE_VEHICULO_QUESTION   = '�Desea eliminar este Veh�culo?';
    MSG_DELETE_VEHICULO_ERROR      = 'No se ha podido eliminar el Veh�culo';
    MSG_QUESTION_BAJA_ULT_CUENTA_WITH_DEBT = 'El Convenio tiene montos adeudados. Recuerde despu�s de dar de baja el convenio facturar la totalidad de los montos';
    MSG_BAJA_CUENTA_ULTIMOS_CONSUMOS_SIN_FACTURAR = 'El Convenio tiene �ltimos consumos sin facturar. Recuerde despu�s de dar de baja el convenio facturar la totalidad de los montos.';
    MSG_DELETE_VEHICULO_CAPTION    = 'Eliminar Veh�culo';
    MSG_INICIO_PANTALLA 	   		= 'No se pudo inicializar la pantalla';
    MSG_QUESTION_BAJA_VEHICULO_CTACOMERCIAL= '�Est� seguro de querer dar de baja esta %s del Convenio Cuenta Comercial?';  //TASK_019_20160531
var
    Vehiculo: TVehiculosConvenio;
    f: TfrmEliminarCuenta;
    Indice, I: Integer;                                                         // TASK_070_MGO_20160907
    CargarObservacion: Boolean;
    TieneSaldo: Boolean;
    ConceptoIndemnizacion: integer;
    DescripcionConceptoIndemnizacion: string;
    frmEliminarTag: TFormEliminarTag;
    formAltaForzada: TFormAltaForzada;                                          // TASK_070_MGO_20160907
begin
    //INICIO: TASK_005_ECA_20160420
    if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL then
    begin
        if MsgBox(format(MSG_QUESTION_BAJA_VEHICULO_CTACOMERCIAL,[FLD_CUENTA]), FMsgBoxCaption, MB_YESNO or MB_ICONWARNING) = IDYES then //TASK_019_20160531
         EliminarVehiculosCuentaComercial();                                                                            //TASK_019_20160531
        Exit;                                                                                                           //TASK_019_20160531
    end;
    //FIN: TASK_005_ECA_20160420

    CargarObservacion := False;
    if FDatosConvenio.Cuentas.CantidadVehiculos = 0 then
        Exit;

    //Obtengo si tiene saldo el convenio
    TieneSaldo := QueryGetValueINT(DMConnections.BaseCAC,'SELECT DBO.ObtenerSaldoDelConvenio('+ IntToStr(FDatosConvenio.CodigoConvenio) +')') > 0;
    //si es el ultimo vehiculo y el convenio tiene saldo, informo que la cuenta no deberia ser dada de baja, pero si el usuario acepta permito realizarlo igual.
    if (FDatosConvenio.Cuentas.CantidadVehiculos = 1) and TieneSaldo then
        MsgBox(MSG_QUESTION_BAJA_ULT_CUENTA_WITH_DEBT, FMsgBoxCaption, MB_ICONWARNING);

    //si es el ultimo vehiculo y el convenio tiene ultimos consumos, informo que la cuenta no deberia ser dada de baja, pero si el usuario acepta permito realizarlo igual.
    if (FDatosConvenio.Cuentas.CantidadVehiculos = 1) and TieneUltimosConsumosSinFacturar(FDatosConvenio.CodigoConvenio) then
        MsgBox(MSG_BAJA_CUENTA_ULTIMOS_CONSUMOS_SIN_FACTURAR, FMsgBoxCaption, MB_ICONWARNING);

    if FDatosConvenio.Cuentas.CantidadVehiculos = 1 then begin
          btn_Baja_Contrato.Click;
          exit;
    end;

    Indice := FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString);
    Vehiculo := FDatosConvenio.Cuentas.Vehiculos[Indice];

    if Vehiculo.EsTagPerdido then begin

        if MsgBox(format(MSG_QUESTION_BAJA_ESTA,[FLD_CUENTA]), FMsgBoxCaption, MB_YESNO or MB_ICONWARNING) = IDYES then begin
            if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, Vehiculo.Cuenta.Vehiculo.Patente,
              Vehiculo.Cuenta.IndiceVehiculo, SerialNumberToEtiqueta(Vehiculo.Cuenta.ContactSerialNumber), FPuntoEntrega, UsuarioSistema, 'Baja Perdido')  then
                raise Exception.create('Error registrando auditoria');

            FDatosConvenio.ChekListCuenta.ElimnarItems(Vehiculo.Cuenta.IndiceVehiculo);
            FDatosConvenio.Cuentas.QuitarVehiculo(Indice);

            try
              DSVehiculosContacto.DataSet := Nil;
              cdsVehiculosContacto.DisableControls;
              FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
            finally
              cdsVehiculosContacto.EnableControls;
              DSVehiculosContacto.DataSet := cdsVehiculosContacto;
            end;

            CargarObservacion := True;
            ActualizaControlesPorModificacion(True);
        end;
    end
    else if Vehiculo.EsVehiculoNuevo then begin


            if MsgBox(format(MSG_QUESTION_BAJA_ESTA,[FLD_CUENTA]), FMsgBoxCaption, MB_YESNO or MB_ICONWARNING) = IDYES then begin
                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, Vehiculo.Cuenta.Vehiculo.Patente,
                  Vehiculo.Cuenta.IndiceVehiculo, SerialNumberToEtiqueta(Vehiculo.Cuenta.ContactSerialNumber), FPuntoEntrega, UsuarioSistema, 'Baja Nuevo')  then
                    raise Exception.create('Error registrando auditoria');
                FDatosConvenio.Cuentas.DestruirVehiculo(Indice);
                try
                  DSVehiculosContacto.DataSet := Nil;
                  cdsVehiculosContacto.DisableControls;
                  FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                finally
                  cdsVehiculosContacto.EnableControls;
                  DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                end;
                ActualizaControlesPorModificacion(True);
            end;
    end
    else begin

        if GNumeroTurno < 1 then begin
            MsgBox(MSG_ERROR_TURNO_MODIF_CONVENIO, FMsgBoxCaption, MB_ICONSTOP);
            Exit;
        end;

        Application.CreateForm(TFormEliminarTag, frmEliminarTag);
        try
            try
                if frmEliminarTag.Inicializar(FMsgBoxCaption,Vehiculo) then begin

                    if frmEliminarTag.ShowModal = mrOK then begin
                        if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, Vehiculo.Cuenta.Vehiculo.Patente,
                          Vehiculo.Cuenta.IndiceVehiculo, SerialNumberToEtiqueta(Vehiculo.Cuenta.ContactSerialNumber), FPuntoEntrega, UsuarioSistema, 'Baja')  then
                            raise Exception.create('Error registrando auditoria');

                        frmEliminarTag.cdConceptos.First;
                        while not frmEliminarTag.cdConceptos.eof do begin
                            if (frmEliminarTag.cdConceptos.FieldByName('Patente').asString) = (frmEliminarTag.cdsEliminarTelevia.FieldByName('Patente').AsString) then begin
                                ConceptoIndemnizacion := frmEliminarTag.cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                                DescripcionConceptoIndemnizacion := frmEliminarTag.cdConceptos.FieldByName('DescripcionMotivo').AsString;
                                FDatosConvenio.Cuentas.AgregarVehiculoTagACobrar(Indice, ConceptoIndemnizacion, DescripcionConceptoIndemnizacion, 0, 0, '');
                            end;
                            frmEliminarTag.cdConceptos.Next;
                        end;

                        FDatosConvenio.Cuentas.CambiarEstadoConservacionTAG(Indice, frmEliminarTag.EstadoConservacion);

                        // Actualizao los datos del vehiculo
                        Vehiculo.Cuenta.Vehiculo.Robado := frmEliminarTag.Robado;

                        Vehiculo.Cuenta.EstadoConservacionTAG := frmEliminarTag.EstadoConservacion;
                        Vehiculo.EsTagVendido :=  frmEliminarTag.TagVendido;
                        Vehiculo.EsTagPerdido := False;

                        Vehiculo.Cuenta.CodigoMotivoBaja :=  frmEliminarTag.MotivodeBaja;

                        Vehiculo.Cuenta.DescripcionMotivoBaja :=  frmEliminarTag.DescripcionMotivodeBaja;
                        Vehiculo.Cuenta.DescripcionEstadoTAG :=  frmEliminarTag.DescripcionEstadoTAG;
                        Vehiculo.Cuenta.SerialNumberaMostrar := frmEliminarTag.cdsEliminarTelevia.FieldByName('Televia').AsString;

                        //Le comento esta parte porque necesito el valor en el mismo registro.
                        //Adem�s, ahora lo que importa es el estado para saber sie sst� suspendida.
                        Vehiculo.Cuenta.EstaSuspendida := False;
                        Vehiculo.Cuenta.SuspensionForzada := False;
                        Vehiculo.Cuenta.DeadlineSuspension := NullDate;
                        Vehiculo.Cuenta.FechaBajaCuenta := NowBaseSmall(DMConnections.BaseCAC);

                        //Se agrega al listado de bajas forzadas por deshacer si es que tiene           // TASK_070_MGO_20160907
                        FDatosConvenio.BajasForzadas.AgregarVehiculo(Vehiculo.Cuenta.IndiceVehiculo);   // TASK_070_MGO_20160907

                        ChequearRVM(Vehiculo);
                        FDatosConvenio.Cuentas.EditarVehiculo(Indice, Vehiculo);
                        FDatosConvenio.Cuentas.QuitarVehiculo(Indice);
                        FDatosConvenio.ChekListCuenta.ElimnarItems(Vehiculo.Cuenta.IndiceVehiculo);
                        CargarObservacion := True;

                        try
                          DSVehiculosContacto.DataSet := Nil;
                          cdsVehiculosContacto.DisableControls;
                          FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                        finally
                          cdsVehiculosContacto.EnableControls;
                          DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                        end;
                            
                        ActualizaControlesPorModificacion(True);
                    end;
                end
                else
                    MsgBox(MSG_INICIO_PANTALLA, Caption, MB_ICONSTOP);
            Except
                On E: Exception do begin
                    MsgBoxErr(MSG_DELETE_VEHICULO_ERROR, e.message, MSG_DELETE_VEHICULO_CAPTION, MB_ICONSTOP);
                end;
            end;
        finally
            frmEliminarTag.Release;
        end;

        //INICIO : TASK_070_MGO_20160907
        Application.CreateForm(TFormAltaForzada, formAltaForzada);
        try
            try
                if formAltaForzada.Inicializar(FDatosConvenio.BajasForzadas) then begin
                    if formAltaForzada.ShowModal = mrOk then
                        for I := 0 to formAltaForzada.FBajasForzadasConvenio.CantidadBajasForzadas - 1 do
                            FDatosConvenio.BajasForzadas.MarcarBajaForzadaDeshacer(
                                formAltaForzada.FBajasForzadasConvenio.BajasForzadas[I].CodigoBajaForzada,
                                formAltaForzada.FBajasForzadasConvenio.BajasForzadas[I].Deshacer);
                end;
            except
                On E: Exception do begin
                    MsgBoxErr(MSG_DELETE_VEHICULO_ERROR, e.message, MSG_DELETE_VEHICULO_CAPTION, MB_ICONSTOP);
                end;
            end;
        finally
            formAltaForzada.Release;
        end;
        // FIN : TASK_070_MGO_20160907
    end;

    HabilitarBotonesVehiculos;
    Screen.Cursor   := crDefault;
    if CargarObservacion then begin
        cb_Observaciones.Checked := True;
        cb_Observaciones.OnClick(cb_Observaciones);
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_CambiarClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.btn_CambiarClick(Sender: TObject);
ResourceString
	MSG_CAMBIAR_VEHICULO_ERROR      = 'No se ha podido cambiar el televia del Veh�culo';
	MSG_CAMBIAR_VEHICULO_CAPTION    = 'Cambio de Televia';
	MSG_CANT_INITIALIZE_SCREEN 		= 'No se pudo inicializar la pantalla';
	MSG_CAPTION_ACAMBIARTAG_MSG     = 'Cambiar Tag de';
	MSG_ERROR_FECHAGARANTIAVENCIDA  = 'No se pudo realizar el cambio de telev�a.'+CRLF+'La fecha de garant�a del Telev�a esta vencida';  //SS_1006_1015_NDR_20121130

var
    f: TformCambiarTag;
    Vehiculo,                               // SS_916_PDO_20120109
    VehiculoNew: TVehiculosConvenio;        // SS_916_PDO_20120109
    CargarObservacion: Boolean;
    ConceptoIndemnizacion: integer;
    DescripcionConceptoIndemnizacion: string;
    Patente: string;
begin
  if FDatosConvenio.TurnoAbierto then begin
    CargarObservacion := False;
    if cdsVehiculosContacto.RecordCount = 0 then Exit;

    if GNumeroTurno < 1 then begin
        MsgBox(MSG_ERROR_TURNO_MODIF_CONVENIO, FMsgBoxCaption, MB_ICONSTOP);
        Exit;
    end;

    if cdsvehiculoscontacto.FieldByName('FechaVencimientoGarantia').AsDateTime<NowBaseSmall(DMConnections.BaseCAC) then     //SS_1006_1015_NDR_20121130
    begin                                                                                                                   //SS_1006_1015_NDR_20121130
        ShowMsgBoxCN(MSG_CAMBIAR_VEHICULO_CAPTION, MSG_ERROR_FECHAGARANTIAVENCIDA, MB_ICONERROR, Application.MainForm);     //SS_1006_1015_NDR_20121130
        Exit;                                                                                                               //SS_1006_1015_NDR_20121130
    end;                                                                                                                    //SS_1006_1015_NDR_20121130

    if (FDatosConvenio.Cuentas.TieneCuentasSuspendidas) and
        (MsgBox(MSG_QUESTION_ALTA_TAG, Caption, MB_YESNO or MB_ICONQUESTION) <> idYES) then exit;

    if FDatosConvenio.TieneTagVendidoSinUsar > 0 then
        MsgBox(format(MSG_TAG_SIN_USAR,[FDatosConvenio.TieneTagVendidoSinUsar]), FMsgBoxCaption, MB_ICONINFORMATION);

    Screen.Cursor   := crHourGlass;
    Application.CreateForm(TformCambiarTag, f);
    Patente := cdsVehiculosContacto.FieldByName('Patente').AsString ;
    try
        try
            if f.Inicializar(FDatosConvenio.Cuentas.ObtenerVehiculo(Patente), True, FPuntoEntrega) then begin
                if f.ShowModal = mrOK then begin
                    if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, Patente,
                      FDatosConvenio.Cuentas.ObtenerVehiculo(Patente).Cuenta.IndiceVehiculo, cdsVehiculosContacto.FieldByName('SerialNumberAMostrar').AsString , FPuntoEntrega, UsuarioSistema, 'BajaxCambioTag')  then
                        raise exception.Create('Error registrando auditoria');
                    f.cdConceptos.First;
                    while not f.cdConceptos.eof do begin
                        ConceptoIndemnizacion := f.cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                        DescripcionConceptoIndemnizacion := f.cdConceptos.FieldByName('DescripcionMotivo').AsString;
                        FDatosConvenio.Cuentas.AgregarVehiculoTagACobrar(FDatosConvenio.Cuentas.BuscarIndice(Patente), ConceptoIndemnizacion, DescripcionConceptoIndemnizacion, 0, 0, '');
                        f.cdConceptos.Next;
                    end;
                    FDatosConvenio.Cuentas.CambiarEstadoConservacionTAG(FDatosConvenio.Cuentas.BuscarIndice(Patente), f.EstadoConservacion);
                    Vehiculo := FDatosConvenio.Cuentas.ObtenerVehiculo(Patente);                                        // SS_916_PDO_20120109
                    Vehiculo.Cuenta.FechaBajaCuenta := NowBaseSmall(DMConnections.BaseCAC);                             // SS_916_PDO_20120109
                    FDatosConvenio.Cuentas.EditarVehiculo(Patente, Vehiculo);                                           // SS_916_PDO_20120109
                    FDatosConvenio.Cuentas.QuitarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(Patente));
                    FDatosConvenio.Cuentas.AgregarVehiculo(f.Vehiculo);
                    VehiculoNew := FDatosConvenio.Cuentas.ObtenerVehiculo(f.Vehiculo.Cuenta.Vehiculo.Patente);          // SS_916_PDO_20120109
                    VehiculoNew.Cuenta.FechaCreacion := IncMinute(Vehiculo.Cuenta.FechaBajaCuenta, 1);                  // SS_916_PDO_20120109
                    VehiculoNew.EsCambioTag := True;                                                                    // SS_916_PDO_20120109
                    VehiculoNew.Cuenta.TipoAsignacionTag := CONST_OP_TRANSFERENCIA_TAG_NUEVO;                           // SS_916_PDO_20120109
                    VehiculoNew.Cuenta.CodigoAlmacenDestino := f.AlmacenDestino;                                        // SS_916_PDO_20120109
                    VehiculoNew.Cuenta.TieneFechaRetroactiva := False;                                                  // SS_916_PDO_20120109
                    FDatosConvenio.Cuentas.EditarVehiculo(VehiculoNew.Cuenta.Vehiculo.Patente, VehiculoNew);            // SS_916_PDO_20120109
                    FDatosConvenio.ChekListCuenta.ElimnarItems(f.Vehiculo.Cuenta.IndiceVehiculo);
                    CargarObservacion := True;

                    //Rev.5 / 09-Junio-2010-----------------------------------------------------
                    try
                      DSVehiculosContacto.DataSet := Nil;
                      cdsVehiculosContacto.DisableControls;
                      FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                    finally
                      cdsVehiculosContacto.EnableControls;
                      DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                    end;
                    //Rev.5---------------------------------------------------------------------

                    if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, Patente,
                      0,SerialNumbertoEtiqueta(f.Vehiculo.Cuenta.ContactSerialNumber) , FPuntoEntrega, UsuarioSistema, 'AltaxCambioTag')  then
                        raise exception.Create('Error registrando auditoria');
                    FDatosConvenio.ObtenerCambios(FDatosConvenioOriginal);
                    FDatosConvenio.DocumentacionConvenio.ActualizarEstadoDoc(f.Vehiculo.Cuenta.Vehiculo.Patente);
                    FPuedeDarBaja := False;
                    
                    ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
                end;
            end else MsgBox(MSG_CANT_INITIALIZE_SCREEN, Caption, MB_ICONSTOP);
        Except
            On E: Exception do begin
                MsgBoxErr( MSG_CAMBIAR_VEHICULO_ERROR, e.message, MSG_CAMBIAR_VEHICULO_CAPTION, MB_ICONSTOP);
            end;
        end;
    finally
        f.Release;
    end;

	Screen.Cursor   := crDefault;
    if CargarObservacion then begin
        cb_Observaciones.Checked := True;
        cb_Observaciones.OnClick(cb_Observaciones);
    end;
  end else begin
        MsgBox(format (MSG_ERROR_TURNO_SUSPENDER_VEHICULO, [MSG_CAPTION_ACAMBIARTAG_MSG]), Self.Caption, MB_ICONSTOP);
        Exit;
  end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_CambioVehiculoClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Agrego que si es cambio de tag setee la fecha baja ahora, ya
    que la de la cuenta la setea por codigo y no en los storeds entonces se
    produc�an solapamientos.
 Parameters		:
Revision 		: 1
Author      : mpiazza
Date        : 23/03/2010
Description : ss-870 : se agrega validacion de turno abierto
}
procedure TFormModificacionConvenio.btn_CambioVehiculoClick(
  Sender: TObject);
ResourceString
    MSG_ERROR_EDITAR_VEHICULO      = 'No se pudieron modificar los datos del veh�culo';
    MSG_EDITAR_VEHICULO_CAPTION    = 'Actualizar datos del veh�culo';
    MSG_CAPTION_CAMBIO_MSG         = 'Cambio de';
	MSG_ERROR_FECHAGARANTIAVENCIDA  = 'No se pudo realizar el cambio de veh�culo.'+CRLF+'La fecha de garant�a del Telev�a esta vencida';  //SS_1006_1015_NDR_20121130

var
    f : TFormSCDatosVehiculo;
    Cadena, CadenaTags: TStringList;
    dondeEstaba: TBookmark;
    NoCargar: string;
    auxVehiculo, auxVehiculoModif: TVehiculosConvenio;
    ConceptoIndemnizacion: integer;
    DescripcionConceptoIndemnizacion: string;
begin
  if FDatosConvenio.TurnoAbierto then begin
    if dblVehiculos.DataSource.DataSet.RecordCount = 0 then Exit;

    //Controlamos que el turno este abierto
     if GNumeroTurno < 1 then begin
        MsgBox(MSG_ERROR_TURNO_MODIF_CONVENIO, FMsgBoxCaption, MB_ICONSTOP);
        Exit;
    end;

    if cdsvehiculoscontacto.FieldByName('FechaVencimientoGarantia').AsDateTime<NowBaseSmall(DMConnections.BaseCAC) then                 //SS_1006_1015_NDR_20121130
    begin                                                                                                                               //SS_1006_1015_NDR_20121130
        ShowMsgBoxCN(MSG_EDITAR_VEHICULO_CAPTION, MSG_ERROR_FECHAGARANTIAVENCIDA, MB_ICONERROR, Application.MainForm);                  //SS_1006_1015_NDR_20121130
        Exit;                                                                                                                           //SS_1006_1015_NDR_20121130
    end;                                                                                                                                //SS_1006_1015_NDR_20121130


    Cadena := TStringList.Create;
    CadenaTags := TStringList.Create;
	dondeEstaba := cdsVehiculosContacto.GetBookmark;
    if (cdsVehiculosContacto.fieldbyname('ContractSerialNumber').IsNull) then
        NoCargar := ''
    else
        NoCargar := cdsVehiculosContacto.fieldbyname('ContractSerialNumber').Value;

    //Rev.5 / 09-Junio-2010 / Nelson Droguett Sierra ----------------------------
    //ArmarListaPatentes(Cadena, Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString), Trim(dsVehiculosContacto.DataSet.FieldByName('PatenteRVM').AsString));
    //ArmarListaTags(CadenaTags, Trim(NoCargar));
    ArmarListaPatentesTags(Cadena,
                           CadenaTags,
                           Trim(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString),
                           Trim(dsVehiculosContacto.DataSet.FieldByName('PatenteRVM').AsString),
                           Trim(NoCargar)
                          );
    //FinRev.5-------------------------------------------------------------------
    cdsVehiculosContacto.GotoBookmark(dondeEstaba);
    cdsVehiculosContacto.FreeBookmark(dondeEstaba);
    Application.CreateForm(TFormSCDatosVehiculo, f);
    if f.Inicializar(
        MSG_CAPTION_CAMBIAR_VEHICULO,
        PuntoEntrega,
        Trim(cdsVehiculosContacto.FieldByName('TipoPatente').AsString),
        Trim(cdsVehiculosContacto.FieldByName('Patente').AsString),
        Trim(cdsVehiculosContacto.FieldByName('PatenteRVM').AsString),
        cdsVehiculosContacto.FieldByName('Modelo').AsString,
        cdsVehiculosContacto.FieldByName('CodigoMarca').AsInteger,
        cdsVehiculosContacto.FieldByName('AnioVehiculo').AsInteger,
        cdsVehiculosContacto.FieldByName('CodigoTipoVehiculo').AsInteger,
        cdsVehiculosContacto.FieldByName('CodigoColor').AsInteger,
        Trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatente').AsString),
        Trim(dsVehiculosContacto.DataSet.FieldByName('DigitoVerificadorPatenteRVM').AsString),
		cdsVehiculosContacto.FieldByName('CodigoVehiculo').AsInteger,
        cdsVehiculosContacto.FieldByName('Observaciones').AsString,
        -1,
        Cadena, true, CadenaTags,
        cdsVehiculosContacto.FieldByName('ContextMark').AsInteger,
        cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString,
        False,
        FAdministrador,
        FDatosConvenio.CodigoConvenio,
        cdsVehiculosContacto,
        cdsVehiculosContacto.FieldByName('Vendido').AsBoolean or (Trim(cdsVehiculosContacto.FieldByName('Patente').AsString) = ''),
        FDatosConvenio.Cliente.Datos.CodigoPersona,
        Peatypes.ConvenioCambiarVehiculo
        )
        and (f.ShowModal = mrOK) then begin
        try
            if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, cdsVehiculosContacto.fieldbyname('Patente').Value,
              FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).Cuenta.IndiceVehiculo,
              cdsVehiculosContacto.FieldByName('SerialNumberAMostrar').AsString , FPuntoEntrega, UsuarioSistema, 'BajaxCambioVehiculo')  then
                raise exception.Create('Error registrando auditoria');

            with FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).Cuenta do begin
                auxVehiculo.Cuenta := f.DatoCuentaNueva;
                auxVehiculo.Cuenta.IndiceVehiculo := -1;
                auxVehiculo.Cuenta.FechaAltaTag := FechaAltaTag;
                auxVehiculo.Cuenta.FechaBajaTag := FechaBajaTag;
                auxVehiculo.Cuenta.FechaVencimientoTag := FechaVencimientoTag;
                auxVehiculo.EsVehiculoModificado := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).EsVehiculoModificado;
                auxVehiculo.Documentacion := nil;
                auxVehiculo.EsTagVendido := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).EsTagVendido;
                auxVehiculo.Cuenta.FechaAltaTag := FechaAltaTag;
                //auxVehiculo.Cuenta.AdheridoPA := FDatosConvenio.AdheridoPA;		// SS_1006_PDO_20120820 TASK_004_ECA_20160411

                //Rev.7 / 26-Julio-2010 / Nelson Droguett Sierra ---------------------------------------------------------
                //if cdsVehiculosContacto.FieldByName('Almacen').AsString = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS then begin
                //	auxVehiculo.Cuenta.CodigoAlmacenDestino := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
                //end
                //else auxVehiculo.Cuenta.CodigoAlmacenDestino := f.AlmacenDestino ;
                if cdsVehiculosContacto.FieldByName('Almacen').AsString = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS then
                	auxVehiculo.Cuenta.CodigoAlmacenDestino := CONST_ALMACEN_ENTREGADO_EN_CUOTAS
                else if cdsVehiculosContacto.FieldByName('Almacen').AsString = CONST_STR_ALMACEN_ARRIENDO then
                	auxVehiculo.Cuenta.CodigoAlmacenDestino := CONST_ALMACEN_ARRIENDO
                else if cdsVehiculosContacto.FieldByName('Almacen').AsString = CONST_STR_ALMACEN_COMODATO then
                	auxVehiculo.Cuenta.CodigoAlmacenDestino := CONST_ALMACEN_EN_COMODATO
                else auxVehiculo.Cuenta.CodigoAlmacenDestino := f.AlmacenDestino ;
                //FinRev.7-------------------------------------------------------------------------------------------------

                auxVehiculo.Cuenta.TieneFechaRetroactiva := False;
                auxVehiculo.Cuenta.TipoAsignacionTag := CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA;
                auxVehiculoModif := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString);
                auxVehiculoModif.EsCambioVehiculo := True;
                //auxVehiculoModif.Cuenta.FechaBajaCuenta := IncMinute(NowBaseSmall(DMConnections.BaseCAC), -1);                    // SS_916_PDO_20120109
                auxVehiculoModif.Cuenta.FechaBajaCuenta := NowBaseSmall(DMConnections.BaseCAC);                                     // SS_916_PDO_20120109
                ChequearRVM(auxVehiculoModif);
                FDatosConvenio.Cuentas.EditarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString), auxVehiculoModif);
                FDatosConvenio.Cuentas.QuitarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString));
                AuxVehiculo.EsVehiculoRobado := f.DatoCuentaNueva.Vehiculo.Robado;
                AuxVehiculo.EsVehiculoRecuperado := f.DatoCuentaNueva.Vehiculo.Recuperado;

                auxVehiculo.Cuenta.Vehiculo.CodigoColor:=f.FreDatosVehiculoNuevo.Color; //TASK_016_ECA_20160526   

                FDatosConvenio.Cuentas.AgregarCambioVehiculo(auxVehiculo, IncMinute(auxVehiculoModif.Cuenta.FechaBajaCuenta, 1));   // SS_916_PDO_20120109
                //Rev.5 / 09-Junio-2010-----------------------------------------------------
                try
                  DSVehiculosContacto.DataSet := Nil;
                  cdsVehiculosContacto.DisableControls;
                  FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                finally
                  cdsVehiculosContacto.EnableControls;
                  DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                end;
                //Rev.5---------------------------------------------------------------------

            if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, f.DatoCuentaNueva.Vehiculo.Patente,
              FDatosConvenio.Cuentas.ObtenerVehiculo(auxVehiculo.Cuenta.Vehiculo.Patente).Cuenta.IndiceVehiculo ,
              SerialNumberToEtiqueta(f.DatoCuentaNueva.ContactSerialNumber) , FPuntoEntrega, UsuarioSistema, 'AltaxCambioVehiculo')  then
                raise exception.Create('Error registrando auditoria');

                f.cdConceptos.First;
                while not f.cdConceptos.eof do begin
                    ConceptoIndemnizacion := f.cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                    DescripcionConceptoIndemnizacion := f.cdConceptos.FieldByName('DescripcionMotivo').AsString;
                    FDatosConvenio.Cuentas.AgregarVehiculoTagACobrar(FDatosConvenio.Cuentas.BuscarIndice(auxVehiculo.Cuenta.Vehiculo.Patente), ConceptoIndemnizacion, DescripcionConceptoIndemnizacion, 0, F.AlmacenDestino, '');
                    f.cdConceptos.Next;
                end;
            end;
            FPuedeDarBaja := False;

            ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
            HabilitarBotonesVehiculos;  // SS_916_PDO_20120109         
        except
            On E: Exception do begin
            	MsgBoxErr( MSG_ERROR_EDITAR_VEHICULO, E.message, MSG_EDITAR_VEHICULO_CAPTION, MB_ICONSTOP);
            end;
        end;
    end;
    Cadena.Free;
    CadenaTags.Free;
    f.Release;
  end else begin
        MsgBox(format (MSG_ERROR_TURNO_SUSPENDER_VEHICULO, [MSG_CAPTION_CAMBIO_MSG]), Self.Caption, MB_ICONSTOP);
        Exit;
  end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_SuspenderClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
 Parameters		:

Revision 		: 1
Author      : mpiazza
Date        : 09/03/2010
Description : ss-870 : se agrega validacion de turno abierto
}
procedure TFormModificacionConvenio.btn_SuspenderClick(Sender: TObject);
var
    f: TfrmSuspensionCuenta;
    FActivar: TfrmActivarCuenta;
    AuxVehiculo: TVehiculosConvenio;
    dondeEstaba: TBookmark;
    CadenaTags, Cadena: TStringList;
    CargarObservacion: Boolean;
    PatenteSeleccion, DigitoSeleccion: string;

    ConceptoIndemnizacion,  IDMotivoMovCuentaTelevia, Index : integer;
    DescripcionConceptoIndemnizacion : string;
resourcestring
	MSG_CAPTION_SUSPENDER = 'Suspender Cuenta';
	MSG_CAPTION_SUSPENDER_MSG = 'Suspender';
	MSG_CAPTION_ACTIVAR_MSG = 'Activar';
	MSG_QUESTION_SUSPENDER = '�Desea supender la cuenta?';
	MSG_CANT_SUSPEND = 'No puede suspender una cuenta que es nueva';
	STR_FORM_SUSP = 'Formularion de Suspensi�n';
begin
  if FDatosConvenio.TurnoAbierto then begin
    CargarObservacion := False;
    AuxVehiculo := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString);

	if AuxVehiculo.EsVehiculoNuevo then MsgBox(MSG_CANT_SUSPEND, FMsgBoxCaption, MB_ICONSTOP)
    else begin
        dondeEstaba := cdsVehiculosContacto.GetBookmark;

        if (cdsVehiculosContacto.fieldbyname('Suspendido').AsBoolean) then begin
            try
                Application.CreateForm(TfrmActivarCuenta, FActivar);
                Cadena := TStringList.Create;
                CadenaTags := TStringList.Create;

                //Rev.5 / 09-Junio-2010 / Nelson Droguett Sierra ----------------------------
                //ArmarListaPatentes(Cadena, Trim(AuxVehiculo.Cuenta.Vehiculo.Patente), Trim(AuxVehiculo.Cuenta.Vehiculo.PatenteRVM));
                //ArmarListaTags(CadenaTags, Trim(FloatToStr(AuxVehiculo.Cuenta.ContactSerialNumber)));
                ArmarListaPatentesTags(Cadena,
                                       CadenaTags,
                                       Trim(AuxVehiculo.Cuenta.Vehiculo.Patente),
                                       Trim(AuxVehiculo.Cuenta.Vehiculo.PatenteRVM),
                                       Trim(FloatToStr(AuxVehiculo.Cuenta.ContactSerialNumber))
                                      );
                //FinRev.5-------------------------------------------------------------------


                if not FActivar.Inicializar(MSG_CAPTION_ACTIVAR_CUENTA,
                                            AuxVehiculo,
                                            FPuntoEntrega,
                                            FDatosConvenio.CodigoConvenio,
                                            FDatosConvenio.Cliente.CodigoPersona,
                                            Cadena,
                                            CadenaTags,nil,
                                            cdsVehiculosContacto)
                                            then MsgBoxErr(Format(MSG_ERROR_INICIALIZACION,['Activaci�n de Cuenta']),Format(MSG_ERROR_INICIALIZACION,['Formularion de Suspensi�n']), 'Activaci�n', MB_ICONSTOP)
                else begin
                    if FActivar.ShowModal = mrOK then begin
                        if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text,
                                                              FDatosConvenio.CodigoConvenio,
                                                              AuxVehiculo.Cuenta.Vehiculo.Patente,
                                                              AuxVehiculo.Cuenta.IndiceVehiculo,
                                                              SerialNumberToEtiqueta(AuxVehiculo.Cuenta.ContactSerialNumber) ,
                                                              FPuntoEntrega,
                                                              UsuarioSistema,
                                                              'Activar')  then begin
                        	raise exception.Create('Error registrando auditoria');
                        end;

                        AuxVehiculo:= FActivar.Vehiculo;
                        AuxVehiculo.Cuenta.FechaCreacion := IncMinute(dsVehiculosContacto.DataSet.FieldByName('FechaBajaCuenta').AsDateTime,1);
                       // AuxVehiculo.Cuenta.AdheridoPA := FDatosConvenio.AdheridoPA;         //SS-1006-NDR-20120715 TASK_004_ECA_20160411
                        FDatosConvenio.Cuentas.AgregarCuentaActivada(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString), AuxVehiculo);

                        FActivar.cdConceptos.First;
                        while not FActivar.cdConceptos.eof do begin
                            ConceptoIndemnizacion				:= FActivar.cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                            DescripcionConceptoIndemnizacion	:= FActivar.cdConceptos.FieldByName('DescripcionMotivo').AsString;
                            IDMotivoMovCuentaTelevia			:= FActivar.cdConceptos.FieldByName('IDMotivoMovCuentaTelevia').AsInteger;
                            Index := FDatosConvenio.Cuentas.BuscarIndice(AuxVehiculo.Cuenta.Vehiculo.Patente) ;
                            FDatosConvenio.Cuentas.AgregarVehiculoTagACobrar(Index, ConceptoIndemnizacion, ConceptoIndemnizacion, 0, FActivar.AlmacenDestino, DescripcionConceptoIndemnizacion, FActivar.TipoAsignacionTag);
                            FActivar.cdConceptos.Next;
                        end;

                        ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
                    end;
                end;
            finally
                FActivar.Release;
            end;
        end else begin
            try
                Application.CreateForm(TfrmSuspensionCuenta, f);
                PatenteSeleccion := dblVehiculos.DataSource.DataSet.FieldByName('Patente').AsString;
                DigitoSeleccion := dblVehiculos.DataSource.DataSet.FieldByName('DigitoVerificadorPatente').AsString;
                if not f.Inicializar(PatenteSeleccion, DigitoSeleccion) then MsgBoxErr(Format(MSG_ERROR_INICIALIZACION,[STR_FORM_SUSP]),Format(MSG_ERROR_INICIALIZACION,['Formularion de Suspensi�n']), 'Suspensi�n', MB_ICONSTOP)
                else begin
                    if f.ShowModal = mrOK then begin
                        if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, AuxVehiculo.Cuenta.Vehiculo.Patente,
                          AuxVehiculo.Cuenta.IndiceVehiculo, SerialNumberToEtiqueta(AuxVehiculo.Cuenta.ContactSerialNumber) , FPuntoEntrega, UsuarioSistema, 'Suspender')  then
                            raise exception.Create('Error registrando auditoria');
                        FDatosConvenio.Cuentas.Suspender(FDatosConvenio.Cuentas.BuscarIndice(dsVehiculosContacto.DataSet.FieldByName('Patente').AsString),f.FechaLimite, f.NotificadoPorCliente);
                        CargarObservacion := True;

                        ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715                        
                    end;
                end;
            finally
                f.Release;
            end

        end;

        //Rev.5 / 09-Junio-2010-----------------------------------------------------
        try
          DSVehiculosContacto.DataSet := Nil;
          cdsVehiculosContacto.DisableControls;
          FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
        finally
          cdsVehiculosContacto.EnableControls;
          DSVehiculosContacto.DataSet := cdsVehiculosContacto;
        end;
        //Rev.5---------------------------------------------------------------------
        cdsVehiculosContacto.GotoBookmark(dondeEstaba);
        cdsVehiculosContacto.FreeBookmark(dondeEstaba);

        if CargarObservacion then begin
            cb_Observaciones.Checked := True;
            cb_Observaciones.OnClick(cb_Observaciones);
        end;
    end;
  end else begin
        if (cdsVehiculosContacto.fieldbyname('Suspendido').AsBoolean) then begin
            MsgBox(format (MSG_ERROR_TURNO_SUSPENDER_VEHICULO, [MSG_CAPTION_ACTIVAR_MSG]), Self.Caption, MB_ICONSTOP);
        end else begin
            MsgBox(format (MSG_ERROR_TURNO_SUSPENDER_VEHICULO, [MSG_CAPTION_SUSPENDER_MSG]), Self.Caption, MB_ICONSTOP);
        end;

        Exit;
  end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_PerdidoClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
 Parameters		:
Revision 		: 1
Author      : mpiazza
Date        : 23/03/2010
Description : ss-870 : se agrega validacion de turno abierto
}
procedure TFormModificacionConvenio.btn_PerdidoClick(Sender: TObject);
ResourceString
    MSG_CAPTION_PERDIDO_MSG         = ' dar por Perdido ';
var
    f: Tfrm_PerdidaTelevia;
    FActivar: TfrmReacuperacionTag;
    auxVehiculo: TVehiculosConvenio;
    indice: Integer;
    ConceptoIndemnizacion: integer;
    DescripcionConceptoIndemnizacion: string;
begin
  if FDatosConvenio.TurnoAbierto then begin
    if cdsVehiculosContacto.RecordCount = 0 then Exit;
    indice := FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString);
    auxVehiculo := FDatosConvenio.Cuentas.Vehiculos[indice];
    if not cdsVehiculosContacto.FieldByName('Perdido').AsBoolean then begin
        Application.CreateForm(Tfrm_PerdidaTelevia, f);
        try
            if f.Inicializar( auxVehiculo ) and (f.ShowModal = mrOK) then begin
                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, AuxVehiculo.Cuenta.Vehiculo.Patente,
                AuxVehiculo.Cuenta.IndiceVehiculo, SerialNumberToEtiqueta(AuxVehiculo.Cuenta.ContactSerialNumber) , FPuntoEntrega, UsuarioSistema, 'Perdido')  then
                    raise exception.Create('Error registrando auditoria');
                f.cdConceptos.First;
                while not f.cdConceptos.eof do begin
                    ConceptoIndemnizacion := f.cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                    DescripcionConceptoIndemnizacion := f.cdConceptos.FieldByName('DescripcionMotivo').AsString;
                    FDatosConvenio.Cuentas.AgregarVehiculoTagACobrar(Indice, ConceptoIndemnizacion, DescripcionConceptoIndemnizacion, 0, 0, '');
                    f.cdConceptos.Next;
                end;
                if f.VehiculoRobado then auxVehiculo.Cuenta.Vehiculo.Robado := True;
                auxVehiculo.EsTagPerdido := True;
                auxVehiculo.Cuenta.Suspendida := NullDate;
                auxVehiculo.Cuenta.EstaSuspendida := False;
                auxVehiculo.Cuenta.SuspensionForzada := False;
                auxVehiculo.Cuenta.DeadlineSuspension := NullDate;
                auxVehiculo.Cuenta.FechaBajaCuenta := NowBaseSmall(DMConnections.BaseCAC);      // SS_916_PDO_20120214

                //auxVehiculo.Cuenta.AdheridoPA := False;                         //SS-1006-NDR-20120715 //TASK_004_ECA_20160411

                FDatosConvenio.Cuentas.CambiarEstadoConservacionTAG(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString), ESTADO_CONSERVACION_NO_DEVUELTO);
                FDatosConvenio.Cuentas.EditarVehiculo(indice, auxVehiculo);
                //Rev.5 / 09-Junio-2010-----------------------------------------------------
                try
                  DSVehiculosContacto.DataSet := Nil;
                  cdsVehiculosContacto.DisableControls;
                  FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                finally
                  cdsVehiculosContacto.EnableControls;
                  DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                end;
                //Rev.5---------------------------------------------------------------------
                FDatosConvenio.ChekListCuenta.Agregar(CONST_ENTREGAR_DENUNCIA, auxVehiculo, f.FechaLimiteEnvioDenuncia);

                //ahora eliminamos el vehiculo desde ac�
                FDatosConvenio.Cuentas.QuitarVehiculo(Indice);
                //Rev.5 / 09-Junio-2010-----------------------------------------------------
                try
                  DSVehiculosContacto.DataSet := Nil;
                  cdsVehiculosContacto.DisableControls;
                  FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                finally
                  cdsVehiculosContacto.EnableControls;
                  DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                end;
                //Rev.5---------------------------------------------------------------------

                FCargando := True;
                cb_Observaciones.Checked := True;
                FCargando := False;

                ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715

                btn_VerObservaciones.Click;
                FPuedeDarBaja := False;
            end;
        finally
            f.Release;
        end;
    end else begin //Recupero
        Application.CreateForm(TfrmReacuperacionTag, FActivar);
        try
            if FActivar.Inicializa(auxVehiculo) and (FActivar.ShowModal = mrOK)  then begin
                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, AuxVehiculo.Cuenta.Vehiculo.Patente,
                AuxVehiculo.Cuenta.IndiceVehiculo,  SerialNumberToEtiqueta(AuxVehiculo.Cuenta.ContactSerialNumber) , FPuntoEntrega, UsuarioSistema, 'Recuperado') then
                    raise exception.Create('Error registrando auditoria');

                auxVehiculo.EsTagPerdido := False;
                auxVehiculo.Cuenta.Vehiculo.Robado := not FActivar.Recuperado;
                auxVehiculo.EsTagRecuperado := True;
                auxVehiculo.Cuenta.TipoAsignacionTag := CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA;
                auxVehiculo.Cuenta.TieneFechaRetroactiva := False;
                FDatosConvenio.Cuentas.EditarVehiculo(indice, auxVehiculo);
                FDatosConvenio.Cuentas.QuitarVehiculo(indice);
                auxVehiculo.Cuenta.EstadoConservacionTAG := ESTADO_CONSERVACION_BUENO;
                FDatosConvenio.Cuentas.AgregarVehiculo(auxVehiculo);
                FDatosConvenio.ChekListCuenta.ElimnarItems(auxVehiculo.Cuenta.IndiceVehiculo);

                FCargando := True;
                cb_Observaciones.Checked := FDatosConvenio.TieneObservacion;
                FCargando := False;

                //Rev.5 / 09-Junio-2010-----------------------------------------------------
                try
                  DSVehiculosContacto.DataSet := Nil;
                  cdsVehiculosContacto.DisableControls;
                  FDatosConvenio.Cuentas.ObtenerVehiculos(cdsVehiculosContacto, cbMostrarVehiculosEliminados.Checked);
                finally
                  cdsVehiculosContacto.EnableControls;
                  DSVehiculosContacto.DataSet := cdsVehiculosContacto;
                end;
                //Rev.5---------------------------------------------------------------------

                ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715                
            end;
        finally
            FActivar.Release;
        end;

    end;
  end else begin
        MsgBox(format (MSG_ERROR_TURNO_SUSPENDER_VEHICULO, [MSG_CAPTION_PERDIDO_MSG]), Self.Caption, MB_ICONSTOP);
        Exit;
  end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_EximirFacturacionClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
 Parameters		:
Revision 		: 1
Author      : mpiazza
Date        : 23/03/2010
Description : ss-870 : se agrega validacion de turno abierto
}
procedure TFormModificacionConvenio.btn_EximirFacturacionClick(Sender: TObject);
resourcestring
    MSG_ERROR_EXIMIR_FACTURACION = 'No se pudieron modificar los datos del veh�culo';
  	MSG_CAPTION_EXIMIR_MSG = 'Eximir';
var
    f: TFormNoFacturarTransitosDeCuenta;
    Vehiculo: TVehiculosConvenio;
begin
  if FDatosConvenio.TurnoAbierto then begin
    if dblVehiculos.DataSource.DataSet.RecordCount = 0 then Exit;

    Application.CreateForm(TFormNoFacturarTransitosDeCuenta, f);
    try
        (* Obtener el vehiculo seleccionado en la grilla. *)
        Vehiculo := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString);
        if f.Inicializar(ModoPantalla,
                FDatosConvenio.CodigoConvenio,
                Vehiculo.Cuenta.IndiceVehiculo,
                Vehiculo.Cuenta.EximirFacturacion,
                Vehiculo.Cuenta.BonificarFacturacion,
                Trim(cdsVehiculosContacto.FieldByName('Patente').AsString),
                Trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatente').AsString),
                cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString)
                and (f.ShowModal = mrOK) then begin

            try
                (* Obtener los datos modificados y asigarlos al vehiculo. *)
                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, Vehiculo.Cuenta.Vehiculo.Patente,
                Vehiculo.Cuenta.IndiceVehiculo,  SerialNumberToEtiqueta(Vehiculo.Cuenta.ContactSerialNumber) , FPuntoEntrega, UsuarioSistema, 'Eximir') then
                    raise exception.Create('Error registrando auditoria');

                Vehiculo.Cuenta.EximirFacturacion := f.GetEximirFacturacion;
                (* Actualizar los datos del vehiculo. *)
                FDatosConvenio.Cuentas.EditarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString), Vehiculo);

                ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715
            except
                On E: Exception do begin
                    MsgBoxErr(MSG_ERROR_EXIMIR_FACTURACION, E.Message,
                        Caption, MB_ICONSTOP);
                end;
            end; // except
        end; // if Inicializar
    finally
        f.Release;
    end; // finally

  //    HabilitarBotonesVehiculos;
  end else begin
        MsgBox(format (MSG_ERROR_TURNO_SUSPENDER_VEHICULO, [MSG_CAPTION_EXIMIR_MSG]), Self.Caption, MB_ICONSTOP);
        Exit;
  end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: btn_BonificarFacturacionClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	:
 Parameters		:
Revision 		: 1
Author      : mpiazza
Date        : 23/03/2010
Description : ss-870 : se agrega validacion de turno abierto
}
procedure TFormModificacionConvenio.btn_BonificarFacturacionClick(
  Sender: TObject);
resourcestring
    MSG_ERROR_BONIFICAR_FACTURACION = 'No se pudieron modificar los datos del veh�culo';
    MSG_CAPTION_BONIFICAR_MSG       = 'Bonificar';
var
    f: TFormBonificacionFacturacionDeCuenta;
    Vehiculo: TVehiculosConvenio;
begin
  if FDatosConvenio.TurnoAbierto then begin

    if dblVehiculos.DataSource.DataSet.RecordCount = 0 then Exit;

    Application.CreateForm(TFormBonificacionFacturacionDeCuenta, f);
    try
        (* Obtener el vehiculo seleccionado en la grilla. *)
        Vehiculo := FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString);
        if f.Inicializar(ModoPantalla,
                FDatosConvenio.CodigoConvenio,
                Vehiculo.Cuenta.IndiceVehiculo,
                Vehiculo.Cuenta.BonificarFacturacion,
                Vehiculo.Cuenta.EximirFacturacion,
                Trim(cdsVehiculosContacto.FieldByName('Patente').AsString),
                Trim(cdsVehiculosContacto.FieldByName('DigitoVerificadorPatente').AsString),
                cdsVehiculosContacto.FieldByName('SerialNumberaMostrar').AsString)
                and (f.ShowModal = mrOK) then begin

            try
                (* Obtener los datos modificados y asigarlos al vehiculo. *)
                if not RegistrarLogOperacionConvenio( FreDatoPersona.txtDocumento.Text, FDatosConvenio.CodigoConvenio, Vehiculo.Cuenta.Vehiculo.Patente,
                  Vehiculo.Cuenta.IndiceVehiculo, SerialNumberToEtiqueta(Vehiculo.Cuenta.ContactSerialNumber) , FPuntoEntrega, UsuarioSistema, 'Bonificar') then
                    raise exception.Create('Error registrando auditoria');
                Vehiculo.Cuenta.BonificarFacturacion := f.GetBonificarFacturacion;
                (* Actualizar los datos del vehiculo. *)
                FDatosConvenio.Cuentas.EditarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString), Vehiculo);

                ActualizaControlesPorModificacion(True); // SS-887-PDO-20100715                
            except
                On E: Exception do begin
                    MsgBoxErr(MSG_ERROR_BONIFICAR_FACTURACION, E.Message,
                        Caption, MB_ICONSTOP);
                end;
            end; // except
        end; // if Inicializar
    finally
        f.Release;
    end; // finally

  //    HabilitarBotonesVehiculos;
  end else begin
        MsgBox(format (MSG_ERROR_TURNO_SUSPENDER_VEHICULO, [MSG_CAPTION_BONIFICAR_MSG]), Self.Caption, MB_ICONSTOP);
        Exit;
  end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: AcomodarBotonesVehiculo
 Author 		: Nelson Droguett Sierra (ggomez)
 Date Created	: 20-Abril-2010
 Description	: Acomoda los botones para las acciones sobre veh�culos.
                Se utiliza para que al colocar invisible un bot�n no queden huecos entre
                ellos.
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.AcomodarBotonesVehiculo;
begin
    if not btnAgregarVehiculo.Visible then begin
        btnEditarVehiculo.Left          := btnEditarVehiculo.Left - btnAgregarVehiculo.Width;
        btnEliminarVehiculo.Left        := btnEliminarVehiculo.Left - btnAgregarVehiculo.Width;
        btn_Cambiar.Left                := btn_Cambiar.Left - btnAgregarVehiculo.Width;
        btn_CambioVehiculo.Left         := btn_CambioVehiculo.Left - btnAgregarVehiculo.Width;
        //btn_Suspender.Left              := btn_Suspender.Left - btnAgregarVehiculo.Width; //TASK_041_ECA_20160629
        btn_Perdido.Left                := btn_Perdido.Left - btnAgregarVehiculo.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btnAgregarVehiculo.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btnAgregarVehiculo.Width;
    end;
    if not btnEditarVehiculo.Visible then begin
        btnEliminarVehiculo.Left        := btnEliminarVehiculo.Left - btnEditarVehiculo.Width;
        btn_Cambiar.Left                := btn_Cambiar.Left - btnEditarVehiculo.Width;
        btn_CambioVehiculo.Left         := btn_CambioVehiculo.Left - btnEditarVehiculo.Width;
        //btn_Suspender.Left              := btn_Suspender.Left - btnEditarVehiculo.Width; //TASK_041_ECA_20160629
        btn_Perdido.Left                := btn_Perdido.Left - btnEditarVehiculo.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btnEditarVehiculo.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btnEditarVehiculo.Width;
    end;
    if not btnEliminarVehiculo.Visible then begin
        btn_Cambiar.Left                := btn_Cambiar.Left - btnEliminarVehiculo.Width;
        btn_CambioVehiculo.Left         := btn_CambioVehiculo.Left - btnEliminarVehiculo.Width;
        //btn_Suspender.Left              := btn_Suspender.Left - btnEliminarVehiculo.Width; //TASK_041_ECA_20160629
        btn_Perdido.Left                := btn_Perdido.Left - btnEliminarVehiculo.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btnEliminarVehiculo.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btnEliminarVehiculo.Width;
    end;
    if not btn_Cambiar.Visible then begin
        btn_CambioVehiculo.Left         := btn_CambioVehiculo.Left - btn_Cambiar.Width;
        //btn_Suspender.Left              := btn_Suspender.Left - btn_Cambiar.Width; //TASK_041_ECA_20160629
        btn_Perdido.Left                := btn_Perdido.Left - btn_Cambiar.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btn_Cambiar.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btn_Cambiar.Width;
    end;
    if not btn_CambioVehiculo.Visible then begin
        //btn_Suspender.Left              := btn_Suspender.Left - btn_CambioVehiculo.Width; //TASK_041_ECA_20160629
        btn_Perdido.Left                := btn_Perdido.Left - btn_CambioVehiculo.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btn_CambioVehiculo.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btn_CambioVehiculo.Width;
    end;
{*    if not btn_Suspender.Visible then begin
        btn_Perdido.Left                := btn_Perdido.Left - btn_Suspender.Width;
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btn_Suspender.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btn_Suspender.Width;
    end;*}
    if not btn_Perdido.Visible then begin
        btn_EximirFacturacion.Left      := btn_EximirFacturacion.Left - btn_Perdido.Width;
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btn_Perdido.Width;
    end;
    if not btn_EximirFacturacion.Visible then begin
        btn_BonificarFacturacion.Left   := btn_BonificarFacturacion.Left - btn_EximirFacturacion.Width;
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: TieneUltimosConsumosSinFacturar
 Author 		: Nelson Droguett Sierra (lgisuk)
 Date Created	: 20-Abril-2010
 Description	:  Obtengo si tiene ultimos consumos sin facturar
 					Ahora sumo todos los consumos si el total da positivo entonces
              		tiene ultimos consumos que adeuda.
--------------------------------------------------------------------------------}
Function TFormModificacionConvenio.TieneUltimosConsumosSinFacturar (CodigoConvenio : Integer): Boolean;
resourcestring
    MSG_ERROR = 'Error al obtener si tiene ultimos consumos sin facturar';
var
    Total : INT64;
begin
    Result := False;
    try
        with spObtenerUltimosConsumos do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            Parameters.ParamByName('@IndiceVehiculo').Value := NULL;
            Open;
            //Inicializo
            Total := 0;
            //Sumo todos los ultimos consumos
            while not Eof do begin
                Total := Total + FieldByName('Importe').asVariant;
                Next;
            end;
            //si el total es positivo, entonces tiene ultimos consumos
            if Total > 0 then Result := True;
        end;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, Caption, MB_ICONSTOP);
        end;
    end;
end;


{-------------------------------------------------------------------------------
Procedure Name	: RegistrarLogOperacionConvenio
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 20-Abril-2010
 Description	:
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.RegistrarLogOperacionConvenio( NumeroDocumento: string ; CodigoConvenio: integer ; Patente: string ;  IndiceVehiculo: integer; Etiqueta: string ; CodigoPuntoEntrega: integer; Usuario: string ; Accion: string): boolean;
ResourceString
    MSG_ERROR_AUDITORIA_CAC = 'Error guardando operaciones en Convenios CAC';
begin
    Result := False ;
    try
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Operacion').Value       :=  'M' ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@NumeroDocumento').Value :=  NumeroDocumento ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@CodigoConvenio').Value  :=  CodigoConvenio ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Etiqueta').Value        :=  Etiqueta       ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Patente').Value         :=  Patente        ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@IndiceVehiculo').Value  :=  IndiceVehiculo       ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@CodigoPuntoEntrega').Value := CodigoPuntoEntrega ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Usuario').Value         := Usuario               ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@HostName').Value        := GetMachineName        ;
        SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@Accion').Value          := Accion                ;
        SPAgregarAuditoriaConveniosCAC.ExecProc;
        if SPAgregarAuditoriaConveniosCAC.Parameters.ParamByName('@RETURN_VALUE').Value = 0 then Result := True;
    except
        On e: exception do begin
            MsgBoxErr(MSG_ERROR_AUDITORIA_CAC, E.message, MSG_ERROR_AUDITORIA_CAC, MB_ICONSTOP);
        end;
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: ChequearRVM
 Author 		: Nelson Droguett Sierra (vpaszkowicz)
 Date Created	: 20-Abril-2010
 Description	:
-------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.ChequearRVM(var unVehiculo: TVehiculosConvenio);
begin
    if Trim(TVehiculos(unVehiculo.Cuenta.Vehiculo).PatenteRVM) <> '' then begin
        TVehiculos(unVehiculo.Cuenta.Vehiculo).PatenteRVM := '';
        TVehiculos(unVehiculo.Cuenta.Vehiculo).DigitoPatenteRVM := '';
        unVehiculo.EsVehiculoModificado := True;
    end;
end;
{-------------------------------------------------------------------------------
Procedure Name	: EsGiroValido
 Author 		: Nelson Droguett Sierra (mbecerra)
 Date Created	: 20-Abril-2010
 Description	: (Ref. SS 833)
                Valida si el Giro indicado para el usuario persona es v�lido
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.EsGiroValido;
resourcestring
	MSG_ERROR_GIRO_INVALIDO		= 'El Giro ingresado es inv�lido';
    MSG_ERROR_GIRO_CARACTERES	= 'El Giro contiene caracteres inv�lidos';

var
    GiroAux, Mensaje : string;
    i, Largo : integer;
begin
	GiroAux := Trim(Giro);
    Largo	:= Length(GiroAux);
    Mensaje := '';
	if Largo < 5 then Mensaje := MSG_ERROR_GIRO_INVALIDO;

    i := 1;
    while (Mensaje = '') and (i < Largo) do begin
        if not (GiroAux[i] in ['0'..'9', 'a'..'z','A'..'Z', '�', '�', ',', '.', ' ']) then begin
        	Mensaje := MSG_ERROR_GIRO_CARACTERES;
        end;

        Inc(i);
    end;
    Result := (Mensaje = '');
    if MostrarMensaje and (not Result) then MsgBox(Mensaje, Caption, MB_ICONERROR);

end;

{-------------------------------------------------------------------------------
Procedure Name	: PuedeGuardarTipoDocumentoElectronico
 Author 		: Nelson Droguett Sierra (mbecerra)
 Date Created	: 20-Abril-2010
 Description	: (Ref. SS 833)
            Realiza la validaci�n sobre el tipo de documento electr�nico
            seleccionado, y sobre el giro del cliente
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.PuedeGuardarTipoDocumentoElectronico;
resourcestring
    MSG_FALTA_DOCUMENTO_E	= 'Debe seleccionar el documento electr�nico';
    MSG_PEDIR_GIRO			= 'Debe ingresar un Giro para emitir factura al contribuyente persona natural';
    MSG_INGRESAR_GIRO		= 'Ingresar Giro';
    MSG_GIRO				= 'Giro : ';

var
	Giro : string;
begin
	Result := ValidateControls(	[vcbTipoDocumentoElectronico],
    							[vcbTipoDocumentoElectronico.ItemIndex >= 0],
                                Caption,
                                [MSG_FALTA_DOCUMENTO_E]);

    if Result then begin
        //Si es persona natural, y pide factura, validar que est� el Giro, o sino solicitarlo
        if	FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA then begin
            if vcbTipoDocumentoElectronico.Value = TC_FACTURA then begin
                if not EsGiroValido(FDatosConvenio.Cliente.Datos.Giro, False) then begin
                    MsgBox(MSG_PEDIR_GIRO, Caption, MB_ICONEXCLAMATION);
                    Giro := '';
                    if InputQuery(MSG_INGRESAR_GIRO, MSG_GIRO, Giro) then begin
                        if EsGiroValido(Giro, True) then FDatosConvenio.Cliente.SetGiro(Giro)
                        else begin
                            Result := False;
                        end;
                    end
                    else begin
                        Result := False;
                    end;
                end;
            end
            else FDatosConvenio.Cliente.SetGiro('');		// si pide boleta, limpiar el giro
        end;
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: vcbTipoDocumentoElectronicoChange
 Author 		: Nelson Droguett Sierra (mbecerra)
 Date Created	: 20-Abril-2010
 Description	: 	(Ref. SS 833)
                Permite asignar el tipo de comprobante fiscal al Convenio
--------------------------------------------------------------------------------}
procedure TFormModificacionConvenio.vcbTipoDocumentoElectronicoChange(Sender: TObject);
resourcestring
	MSG_TITULO_GIRO		= 'Para Clientes que emiten factura';
    MSG_INGRESE_GIRO	= 'Ingrese Giro:';

var
	Giro, Comprobante, EMail : string;
begin
	if (ActiveControl = Sender) and (vcbTipoDocumentoElectronico.ItemIndex >= 0) then begin
    	Comprobante := vcbTipoDocumentoElectronico.Value;
        if (Comprobante = TC_FACTURA) and (FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA) then begin
            Giro := FDatosConvenio.Cliente.Datos.Giro;
            if InputQuery(MSG_TITULO_GIRO, MSG_INGRESE_GIRO, Giro) then begin
            	if EsGiroValido(Giro, True) then begin
            		FDatosConvenio.TipoComprobanteFiscal	:= Comprobante;
                    FDatosConvenio.Cliente.SetGiro(Giro);
                    EMail := txtEmailParticular.Text;			//para evitar que se pierda el correo en el evento FreDatoPersonacbPersoneriaChange
                    FreDatoPersona.RegistrodeDatos := FDatosConvenio.Cliente.Datos;
                    txtEmailParticular.Text := EMail;
            	end  //forzar que sea boleta
            	else vcbTipoDocumentoElectronico.ItemIndex := vcbTipoDocumentoElectronico.Items.IndexOfValue(TC_BOLETA);
            end  //forzar que sea boleta
            else vcbTipoDocumentoElectronico.ItemIndex := vcbTipoDocumentoElectronico.Items.IndexOfValue(TC_BOLETA);
        end;
        ActualizaControlesPorModificacion(True);  // SS-887-PDO-20100715        
    end;
end;

{
    Procedure Name : ActualizaControlesPorModificacion
    Parameters : SeModificaronDatos: Boolean
    Author : pdominguez
    Date   : 16/07/2010
    firma   :   SS-887-PDO-20100715
    Description: SS 887 - Mejorar Performance
        - Se actualizan los Botones de Reimpresi�n y Guardar, dependiendo de si se modific� o
        no alg�n dato del Convenio.
}
procedure TFormModificacionConvenio.ActualizaControlesPorModificacion(SeModificaronDatos: Boolean);
begin
    if not FCargando then begin
        btn_Guardar.Enabled        := SeModificaronDatos;
        btn_ReImprmirModif.Enabled := not SeModificaronDatos;// and (Assigned(FDatosConvenio) and FDatosConvenio.EsConvenioCN);            //SS_1001_ALA_20111020        //SS_1001_ALA_20111207      //SS_1001_ALA_20111220   // SS_960_PDO_20120115
        //btn_ReImprmirModif.Enabled := not SeModificaronDatos and (Assigned(FDatosConvenio) and FDatosConvenio.EsConvenioCN);    // SS_960_PDO_20120115
        FSeModificaronDatos        := SeModificaronDatos;
    end;
end;



{
    Procedure Name : OnChangeGenerico
    Parameters : Sender: TObject
    Author : pdominguez
    Date   : 16/07/2010
    firma   :   SS-887-PDO-20100715
    Description: SS 887 - Mejorar Performance
        - Se actualizan los controles por Modificaci�n.
}
procedure TFormModificacionConvenio.OnChangeGenerico(Sender: TObject);
begin
    ActualizaControlesPorModificacion(True);
end;

{-------------------------------------------------------------------------------
Procedure Name	: ValidaCambioDomicilios
 Author 		: Jose Jofre
 Date Created	: 01-Agosto-2010
 Description	: valida si cambio domicilio o el bit
            InactivaDomicilioRNUT y registra la auditoria
--------------------------------------------------------------------------------}

procedure TFormModificacionConvenio.ValidaCambioDomicilios ;
begin
    if (FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoComuna  <> FDatosConvenioOriginal.Cliente.Domicilios.DomicilioFacturacion.CodigoComuna)
        or (FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoRegion <> FDatosConvenioOriginal.Cliente.Domicilios.DomicilioFacturacion.CodigoRegion )
            or (FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoCiudad <> FDatosConvenioOriginal.Cliente.Domicilios.DomicilioFacturacion.CodigoCiudad)
                or (FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.NumeroCalle <> FDatosConvenioOriginal.Cliente.Domicilios.DomicilioFacturacion.NumeroCalle)
                    or (Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPostal) <> Trim(FDatosConvenioOriginal.Cliente.Domicilios.DomicilioFacturacion.CodigoPostal))
                        or (Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto) <> Trim(FDatosConvenioOriginal.Cliente.Domicilios.DomicilioFacturacion.Dpto))
                            or (Trim(UpperCase(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Descripcion)) <> Trim(UpperCase(FDatosConvenioOriginal.Cliente.Domicilios.DomicilioFacturacion.Descripcion)))
                                or (FDatosConvenio.Cliente.InactivaDomicilioRNUT and FDatosConvenio.Cliente.InactivaDomicilioRNUT <> FDatosConvenioOriginal.Cliente.InactivaDomicilioRNUT)
    then RegistraAuditoriaDomicilio (FDatosConvenio.CodigoConvenio,FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoDomicilio , FDatosConvenio.Cliente.InactivaDomicilioRNUT );

    if (FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoDomicilio <> FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoDomicilio) then begin

        if (FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoComuna  <> FDatosConvenioOriginal.Cliente.Domicilios.DomicilioPrincipal.CodigoComuna)
            or (FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoRegion <> FDatosConvenioOriginal.Cliente.Domicilios.DomicilioPrincipal.CodigoRegion )
                or (FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoCiudad <> FDatosConvenioOriginal.Cliente.Domicilios.DomicilioPrincipal.CodigoCiudad)
                    or (FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.NumeroCalle <> FDatosConvenioOriginal.Cliente.Domicilios.DomicilioPrincipal.NumeroCalle)
                        or (Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPostal) <> Trim(FDatosConvenioOriginal.Cliente.Domicilios.DomicilioPrincipal.CodigoPostal))
                            or (Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.Dpto) <> Trim(FDatosConvenioOriginal.Cliente.Domicilios.DomicilioPrincipal.Dpto))
                                or (Trim(UpperCase(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.Descripcion)) <> Trim(UpperCase(FDatosConvenioOriginal.Cliente.Domicilios.DomicilioPrincipal.Descripcion)))
                                    or (FDatosConvenio.Cliente.InactivaDomicilioRNUT and FDatosConvenio.Cliente.InactivaDomicilioRNUT <> FDatosConvenioOriginal.Cliente.InactivaDomicilioRNUT)

        then RegistraAuditoriaDomicilio (FDatosConvenio.CodigoConvenio, FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoDomicilio , FDatosConvenio.Cliente.InactivaDomicilioRNUT );
    end;
end;


{-------------------------------------------------------------------------------
Procedure Name	: ValidarCambioMediosDeContacto
 Author 		: Jose Jofre
 Date Created	: 01-Agosto-2010
 Description	: valida si cambio algun medio de contacto o el bit
            InactivaMedioComunicacionRNUT y registra la auditoria
--------------------------------------------------------------------------------}

procedure TFormModificacionConvenio.ValidarCambioMediosDeContacto;
begin

    if (FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoArea <> FDatosConvenioOriginal.Cliente.Telefonos.MedioPrincipal.CodigoArea)
        or (Trim(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.Valor) <> Trim(FDatosConvenioOriginal.Cliente.Telefonos.MedioPrincipal.Valor))
            or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT and FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT <> FDatosConvenioOriginal.Cliente.InactivaMedioComunicacionRNUT)
    then RegistraAuditoriaMedioContacto(FDatosConvenio.CodigoConvenio, FDatosConvenio.Cliente.Telefonos.MedioPrincipal.Indice, FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT);

    if (FDatosConvenio.Cliente.Telefonos.CantidadMediosComunicacion > 1) then begin

        if FDatosConvenioOriginal.Cliente.Telefonos.CantidadMediosComunicacion > 1 then begin

            if (FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoArea <> FDatosConvenioOriginal.Cliente.Telefonos.MedioSecundario.CodigoArea)
                or (Trim(FDatosConvenio.Cliente.Telefonos.MedioSecundario.Valor) <> Trim(FDatosConvenioOriginal.Cliente.Telefonos.MedioSecundario.Valor))
                    or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT and FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT <> FDatosConvenioOriginal.Cliente.InactivaMedioComunicacionRNUT)
            then RegistraAuditoriaMedioContacto(FDatosConvenio.CodigoConvenio, FDatosConvenio.Cliente.Telefonos.MedioSecundario.Indice, FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT);

        end else RegistraAuditoriaMedioContacto(FDatosConvenio.CodigoConvenio, FDatosConvenio.Cliente.Telefonos.MedioSecundario.Indice, FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT);
    end;

    IF FDatosConvenio.Cliente.EMail.CantidadMediosComunicacion > 0 then begin

       IF FDatosConvenioOriginal.Cliente.EMail.CantidadMediosComunicacion > 0 then begin
           if (Trim( FDatosConvenio.Cliente.EMail.MediosComunicacion[0].Valor) <> Trim(FDatosConvenioOriginal.Cliente.EMail.MediosComunicacion[0].Valor))
                or (FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT and FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT <> FDatosConvenioOriginal.Cliente.InactivaMedioComunicacionRNUT)
           then RegistraAuditoriaMedioContacto(FDatosConvenio.CodigoConvenio, FDatosConvenio.Cliente.EMail.MediosComunicacion[0].Indice, FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT);
       end else RegistraAuditoriaMedioContacto(FDatosConvenio.CodigoConvenio, FDatosConvenio.Cliente.EMail.MediosComunicacion[0].Indice, FDatosConvenio.Cliente.InactivaMedioComunicacionRNUT);
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: RegistraAuditoriaMedioContacto
 Author 		: Jose Jofre
 Date Created	: 01-Agosto-2010
 Description	: Registra la Auditoria de Domicilios
--------------------------------------------------------------------------------}

function TFormModificacionConvenio.RegistraAuditoriaDomicilio( CodigoConvenio: Integer ; CodigoDomicilio: Integer ; InactivaDomicilioRNUT : Boolean ): boolean;
ResourceString
    MSG_ERROR_AUDITORIA_DOMICILIOS = 'Error guardando Auditoria Domicilios en Convenios CAC';
begin
    Result := False ;
    try
        spRegistraAuditoriaDomicilios.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio ;
        spRegistraAuditoriaDomicilios.Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilio ;
        spRegistraAuditoriaDomicilios.Parameters.ParamByName('@InactivaDomicilioRNUT').Value := InactivaDomicilioRNUT ;
        spRegistraAuditoriaDomicilios.parameters.ParamByName('@UsuarioSistema').value := UsuarioSistema;
        spRegistraAuditoriaDomicilios.ExecProc;
        Result := True ;
    except
        On e: exception do begin
            MsgBoxErr(MSG_ERROR_AUDITORIA_DOMICILIOS, E.message, MSG_ERROR_AUDITORIA_DOMICILIOS, MB_ICONSTOP);
        end;
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: RegistraAuditoriaMedioContacto
 Author 		: Jose Jofre
 Date Created	: 01-Agosto-2010
 Description	: Registra la Auditoria Medios de Contacto
--------------------------------------------------------------------------------}
function TFormModificacionConvenio.RegistraAuditoriaMedioContacto( CodigoConvenio: Integer ; CodigoMedioContacto: Integer ; InactivaMedioContactoRNUT : Boolean ): boolean;
ResourceString
    MSG_ERROR_AUDITORIA_MEDIOS_CONTACTO = 'Error guardando Auditoria Medios de Contacto en Convenios CAC';
begin
    Result := False ;
    try
        spRegistraAuditoriaMediosDeContacto.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio ;
        spRegistraAuditoriaMediosDeContacto.Parameters.ParamByName('@CodigoMedioComunicacion').Value := CodigoMedioContacto ;
        spRegistraAuditoriaMediosDeContacto.Parameters.ParamByName('@InactivaMedioContactoRNUT').Value := InactivaMedioContactoRNUT ;
        spRegistraAuditoriaMediosDeContacto.Parameters.ParamByName('@UsuarioSistema').value := UsuarioSistema;
        spRegistraAuditoriaMediosDeContacto.ExecProc;
        Result := True ;
    except
        On e: exception do begin
            MsgBoxErr(MSG_ERROR_AUDITORIA_MEDIOS_CONTACTO, E.message, MSG_ERROR_AUDITORIA_MEDIOS_CONTACTO, MB_ICONSTOP);
        end;
    end;
end;




function TFormModificacionConvenio.EsConvenioSinCuenta: Boolean;
begin
    Result :=  IIf((Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_INFRACTOR) or (Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL),True,False); //  chkConvenioSinCuenta.Checked; //TASK_004_ECA_20160411
end;




//INICIO BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------
procedure TFormModificacionConvenio.btnAdherirPorCuentaClick(Sender: TObject);
resourcestring                                                                                                   //SS-1006-NDR-20120727
    MSG_ERROR_TURNO_MODIF_ADHERIDO  = 'Para modificar la Adhesi�n de un convenio debe abrir Turno.';             //SS-1006-NDR-20120727
var
    f: TFormHabilitarCuentasListasAcceso;                                           
    //HayMarcados:Boolean;                                                        //SS_1006_1015_MCO_20121003
    auxVehiculo:TVehiculosConvenio;
begin
    if GNumeroTurno < 1 then begin                                              //SS-1006-NDR-20120727
        MsgBox(MSG_ERROR_TURNO_MODIF_ADHERIDO, FMsgBoxCaption, MB_ICONSTOP);    //SS-1006-NDR-20120727
        Exit;
    end;                                                                        //SS-1006-NDR-20120727

    if ModoPantalla <> scNormal then ActualizarClases;

    Application.CreateForm(TFormHabilitarCuentasListasAcceso, f);

    //HayMarcados:=False;
    if f.Inicializar(FDatosConvenio.CodigoConvenio) then
    begin
//        With f.spObtenerCuentas do                                            //SS_1006_1015_MCO_20121003
        With f.dsObtenerCuentas.Dataset do                                             //SS_1006_1015_MCO_20121003
        begin
          First;
          while not eof do
          begin                                                                 //SS_1006_1015_MCO_20121003
            if (cdsVehiculosContacto.Locate('CodigoVehiculo',FieldByName('CodigoVehiculo').AsInteger,[])) then
//                if (cdsVehiculosContacto.FieldByName('AdheridoPA').AsBoolean=True) then begin     //SS_1006_1015_MCO_20121003
                if (cdsVehiculosContacto.FieldByName('AdheridoPA').AsBoolean=False) then begin       //SS_1006_1015_MCO_20121003
                   //Include(f.FCuentasElegidas, f.cdsObtenerCuentas.RecNo);            //SS_1006_1015_MCO_20121003
                    Edit;                                    //SS_1006_1015_MCO_20121003
                    FieldByName('Chequeado').AsBoolean:= not FieldByName('Chequeado').AsBoolean; //SS_1006_1015_MCO_20121003
                    Post;                                    //SS_1006_1015_MCO_20121003
                end;
            Next;
          end;
          First;
        end;

        if (f.ShowModal = mrok) then
        begin
//          With f.spObtenerCuentas do                                            //SS_1006_1015_MCO_20121003
          With f.dsObtenerCuentas.Dataset do                                             //SS_1006_1015_MCO_20121003
          begin
            First;
            while not eof do
            begin
              if cdsVehiculosContacto.Locate('CodigoVehiculo',FieldByName('CodigoVehiculo').AsInteger,[]) then
              begin
                cdsVehiculosContacto.Edit;
//              cdsVehiculosContacto.FieldByName('AdheridoPA').AsBoolean:=(RecNo in f.FCuentasElegidas);     //SS_1006_1015_MCO_20121003                                                 //SS-1006-NDR-20120614
                //cdsVehiculosContacto.FieldByName('AdheridoPA').AsBoolean:= FieldByName('Chequeado').AsBoolean;  //SS_1006_1015_MCO_20121003 // TASK_004_ECA_20160411
                cdsVehiculosContacto.Post;
                //if (RecNo in f.FCuentasElegidas) then HayMarcados:=True;      //SS_1006_1015_MCO_20121003
              end;
              auxVehiculo:=FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString);
              //auxVehiculo.AdheridoPA:=cdsVehiculosContacto.FieldByName('AdheridoPA').AsBoolean;                                                             //SS-1006-NDR-20120614 TASK_004_ECA_20160411
              //auxVehiculo.Cuenta.AdheridoPA:=cdsVehiculosContacto.FieldByName('AdheridoPA').AsBoolean;                                                        //SS-1006-NDR-20120614 TASK_004_ECA_20160411
              FDatosConvenio.Cuentas.EditarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString), auxVehiculo);
              Next;
            end;
          end;
          //cb_AdheridoPA.Checked := HayMarcados;                               //SS-1006-NDR-20120715
          ActualizaControlesPorModificacion(True);
        end;
    end;
    f.Release;
end;
//FIN BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------






procedure TFormModificacionConvenio.cb_AdheridoPAEnter(Sender: TObject);
	resourcestring
	    MSG_ERROR_TURNO_MODIF_ADHERIDO  = 'Para modificar la Adhesi�n de un convenio debe abrir Turno.';    //SS-1006-NDR-20120727
        MSG_ERROR_REPRESENTANTE_LEGAL_TITULO = 'Validaci�n Representante Legal';
        MSG_ERROR_REPRESENTANTE_LEGAL_MENSAJE = 'Se requiere un Representante Legal para poder realizar la adhesi�n al servicio Arauco TAG.';

begin
    if GNumeroTurno < 1 then begin                                              //SS-1006-NDR-20120727
        MsgBox(MSG_ERROR_TURNO_MODIF_ADHERIDO, FMsgBoxCaption, MB_ICONSTOP);    //SS-1006-NDR-20120727
    	FreDatoPersona.txtDocumento.SetFocus;
        Abort;
    end                                                                        //SS-1006-NDR-2012072
    else if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA then begin
    	if TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes = 0 then begin
        	MsgBoxBalloon(
            	MSG_ERROR_REPRESENTANTE_LEGAL_MENSAJE,
                MSG_ERROR_REPRESENTANTE_LEGAL_TITULO,
                MB_ICONERROR,
                btn_RepresentanteLegal);
            btn_RepresentanteLegal.SetFocus;
            Abort;
        end;
    end;
end;

procedure TFormModificacionConvenio.dblVehiculosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);    // SS_1214_CQU_20140917
begin                                                                                                           // SS_1214_CQU_20140917
    if (Column.FieldName = 'ImagenReferencia') then begin                                                       // SS_1214_CQU_20140917
        MostrarVentanaImagenReferencia(Self, Trim(cdsVehiculosContacto.FieldByName('Patente').AsString));       // SS_1214_CQU_20140917
    end;                                                                                                        // SS_1214_CQU_20140917
end;

procedure TFormModificacionConvenio.CargarDatosFacturacion(CodigoConvenio: Integer);
var
    sp: TADOStoredProc;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'ConvenioFacturacion_SELECT';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
    sp.Open;
    if not sp.IsEmpty then
    begin
        txt_NumeroConvenioFacturacion.Text:= sp.FieldByName('NumeroConvenio').AsString;
        txt_RUTFacturacion.Text:=sp.FieldByName('NumeroDocumento').AsString;
        lbl_NombreRutFacturacion.Caption:=sp.FieldByName('NombreConvenioFacturacion').AsString;
    end;
    sp.Close;
end;

procedure TFormModificacionConvenio.CargarTipoConvenios;
var
    sp: TADOStoredProc;
    i: Integer;
    cbindex: Integer;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'CRM_ConvenioTipos_SELECT';
    sp.Open;
    if not sp.IsEmpty then
    begin
         cbindex:=0;
         for i := 0 to sp.RecordCount - 1 do
         begin
            if CodigoTipoConvenio=sp.FieldByName('CodigoTipoConvenio').AsInteger then
               cbindex:=i;
            cbbTipoConvenio.Items.Add(sp.FieldByName('Descripcion').AsString,sp.FieldByName('CodigoTipoConvenio').AsString);
            sp.Next;
         end;
         cbbTipoConvenio.ItemIndex:=cbindex;
    end;
    sp.Close;

    if Integer(cbbTipoConvenio.Value)<>CODIGO_TIPO_CONVENIO_RNUT then
    begin
        btnAgregarVehiculo.Enabled:=False;
        btn_ConvenioFacturacion.Enabled:=False;
        btn_Baja_Contrato.Enabled:=False;
        btnEditarVehiculo.Enabled         := False;
        btnEliminarVehiculo.Enabled       := False;
        btn_Cambiar.Enabled               := False;
        btn_CambioVehiculo.Enabled        := False;
        btn_Suspender.Enabled             := False;
        btn_Perdido.Enabled               := False;
        btn_EximirFacturacion.Enabled     := False;
        btn_BonificarFacturacion.Enabled  := False;
        btnConvFactVehiculo.Enabled       := False;
        btnSuscribirLista.Enabled         := False; //TASK_034_ECA_20160611
        btnDesuscribirLista.Enabled       := False; //TASK_034_ECA_20160611
        if Integer(cbbTipoConvenio.Value)=CODIGO_TIPO_CONVENIO_CTA_COMERCIAL then
        begin
            btnAgregarVehiculo.Enabled:=True;
            if cdsVehiculosContacto.RecordCount>0 then
            begin
                btnEliminarVehiculo.Enabled:= True;
                btn_EximirFacturacion.Enabled:=True;
                btn_BonificarFacturacion.Enabled:=True;
            end;
        end;
    end;
   cbbTipoConvenio.Enabled:=False;
end;

procedure TFormModificacionConvenio.OnClickConvenioSelected(NumeroConvenio: String; CodigoConvenio: Integer);
begin
    txt_NumeroConvenioFacturacion.Text:= NumeroConvenio;
    CodigoConvenioFacturacion:= CodigoConvenio;
end;

procedure TFormModificacionConvenio.btn_ConvenioFacturacionClick(Sender: TObject);
var
    frm : TformCodigoConvenioFacturacion;
    Personeria : string;
    auxConvenioFacturacion: Integer;
begin
    auxConvenioFacturacion:= CodigoConvenioFacturacion;

    Application.CreateForm(TformCodigoConvenioFacturacion, frm);

    if not frm.Inicializa(FreDatoPersona.txtDocumento.Text)
    then
        frm.Release
    else
    begin
        Personeria:= Trim(StrRight(FreDatoPersona.cbPersoneria.Text, 10));
        frm.txt_Personeria.Text:= FreDatoPersona.cbPersoneria.Text;
        frm.txt_RUT.Text:=FreDatoPersona.txtDocumento.Text;
        if (Personeria=PERSONERIA_JURIDICA) then
        begin
            frm.txt_Nombre.Text:=  FreDatoPersona.txtNombre_Contacto.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txtApellido_Contacto.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txtApellidoMaterno_Contacto.Text;
            frm.txt_RazonSocial.Text:=FreDatoPersona.txtRazonSocial.Text;
            frm.txt_RazonSocial.Visible:=True;
            frm.lblRazonSocial.Visible:=True;
        end
        else
        begin
            frm.txt_Nombre.Text:= FreDatoPersona.txt_Nombre.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txt_Apellido.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txt_ApellidoMaterno.Text;
        end;
        frm.OnClickRegSeleccionado:= OnClickConvenioSelected;
        frm.ShowModal;
        frm.Free;
    end;

    if auxConvenioFacturacion<>CodigoConvenioFacturacion then
       btn_Guardar.Enabled:= True;
end;

procedure TFormModificacionConvenio.OnClickVehiculoSelected(NumeroConvenio: String; CodigoConvenio: Integer);
begin
    CodigoConvFactVehiculo := CodigoConvenio;
    NumeroConvFactVehiculo   := NumeroConvenio;
end;

procedure TFormModificacionConvenio.btnConvFactVehculoClick(Sender: TObject);
var
    frm : TformCodigoConvenioFacturacion;
    Personeria : string;
    AuxNumeroConvFactVehiculo : string;
    auxVehiculo: TVehiculosConvenio;
begin
    //if DSVehiculosContacto.DataSource.DataSet.RecordCount = 0 then Exit;

    AuxNumeroConvFactVehiculo:= cdsVehiculosContacto.FieldByName('NumeroConvenioFacturacion').Value;
    NumeroConvFactVehiculo:= AuxNumeroConvFactVehiculo;
    Application.CreateForm(TformCodigoConvenioFacturacion, frm);

    if not frm.Inicializa(FreDatoPersona.txtDocumento.Text)
    then
        frm.Release
    else
    begin
        Personeria:= Trim(StrRight(FreDatoPersona.cbPersoneria.Text, 10));
        frm.txt_Personeria.Text:= FreDatoPersona.cbPersoneria.Text;
        frm.txt_RUT.Text:=FreDatoPersona.txtDocumento.Text;
        if (Personeria=PERSONERIA_JURIDICA) then
        begin
            frm.txt_Nombre.Text:=  FreDatoPersona.txtNombre_Contacto.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txtApellido_Contacto.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txtApellidoMaterno_Contacto.Text;
            frm.txt_RazonSocial.Text:=FreDatoPersona.txtRazonSocial.Text;
            frm.txt_RazonSocial.Visible:=True;
            frm.lblRazonSocial.Visible:=True;
        end
        else
        begin
            frm.txt_Nombre.Text:= FreDatoPersona.txt_Nombre.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txt_Apellido.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txt_ApellidoMaterno.Text;
        end;
        frm.OnClickRegSeleccionado:= OnClickVehiculoSelected;
        frm.ShowModal;
        frm.Free;

        if AuxNumeroConvFactVehiculo<>NumeroConvFactVehiculo then
        begin
            with cdsVehiculosContacto do begin
               Edit;
               FieldByName('CodigoConvenioFacturacion').Value := CodigoConvFactVehiculo;
               FieldByName('NumeroConvenioFacturacion').Value := NumeroConvFactVehiculo;
               post;
            end;
            dblVehiculos.Refresh;

            auxVehiculo.Cuenta:= FDatosConvenio.Cuentas.ObtenerVehiculo(cdsVehiculosContacto.FieldByName('Patente').AsString).Cuenta;
            auxVehiculo.Cuenta.CodigoConvenioFacturacion:= CodigoConvFactVehiculo;
            FDatosConvenio.Cuentas.EditarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(cdsVehiculosContacto.FieldByName('Patente').AsString), auxVehiculo);
            HabilitarBotonesVehiculos;
            ActualizaControlesPorModificacion(True);
        end;
    end;
end;

function TFormModificacionConvenio.BuscarNombreRUT(CodigoPersona: Integer): string;
var
    sp: TADOStoredProc;
begin
    Result:='';
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'ObtenerDatosCliente';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoDocumento').Value := null;
    sp.Parameters.ParamByName('@NumeroDocumento').Value := null;
    sp.Parameters.ParamByName('@CodigoCliente').Value := CodigoPersona;
    sp.Open;
    if not sp.IsEmpty then
    begin
        Result:=Trim(sp.FieldByName('Nombre').AsString);
    end;
    sp.Close;
end;

procedure TFormModificacionConvenio.btn_RUTFacturacionClick(Sender: TObject);
var
	f: TFormBuscaClientes;
    AuxCodigoCliente: Integer;
begin
    AuxCodigoCliente:=CodigoClienteFacturacion;
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
            FUltimaBusqueda := F.UltimaBusqueda;
			txt_RUTFacturacion.Text := Trim(f.Persona.NumeroDocumento);
            CodigoClienteFacturacion:= QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT CodigoPersona FROM Personas (NOLOCK) WHERE CodigoDocumento = ''RUT'' AND NumeroDocumento = ''%s''',
            [PadL(Trim(txt_RUTFacturacion.Text), 9, '0')]));
            lbl_NombreRutFacturacion.Caption:=BuscarNombreRUT(CodigoClienteFacturacion);
	   	end;
	end;
    f.Release;
    if AuxCodigoCliente<>CodigoClienteFacturacion then
         btn_Guardar.Enabled:= True;
end;

procedure TFormModificacionConvenio.OnClickVehiculoRUTSelected(CodigoConvenio: Integer; CodigoVehiculo: Integer );
begin
    CodigoConvenioRUT:= CodigoConvenio;
    CodigoVehiculoRUT:= CodigoVehiculo;
end;

procedure TFormModificacionConvenio.MostrarVehiculosRUT;
var
    frm: TformConvenioCuentasRUT;
    Personeria, vehiculosAgregados : string;
begin
   Application.CreateForm(TformConvenioCuentasRUT, frm);

    while not cdsVehiculosContacto.eof do
    begin
        vehiculosAgregados:= vehiculosAgregados + Trim(cdsVehiculosContacto.FieldByName('Patente').AsString) + ',';
        cdsVehiculosContacto.Next;
    end;

    if cdsVehiculosContacto.RecordCount>0 then
       cdsVehiculosContacto.First;

    if not frm.Inicializar(Trim(FreDatoPersona.txtDocumento.Text),CodigoConvenioFacturacion,vehiculosAgregados)    then
        frm.Release
    else
    begin
        CodigoConvenioRUT:= 0;
        CodigoVehiculoRUT:= 0;
        Personeria:= Trim(StrRight(FreDatoPersona.cbPersoneria.Text, 10));
        frm.txt_Personeria.Text:= FreDatoPersona.cbPersoneria.Text;
        frm.txt_RUT.Text:=FreDatoPersona.txtDocumento.Text;
        if (Personeria=PERSONERIA_JURIDICA) then
        begin
            frm.txt_Nombre.Text:=  FreDatoPersona.txtNombre_Contacto.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txtApellido_Contacto.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txtApellidoMaterno_Contacto.Text;
            frm.txt_RazonSocial.Text:=FreDatoPersona.txtRazonSocial.Text;
            frm.txt_RazonSocial.Visible:=True;
            frm.lblRazonSocial.Visible:=True;
        end
        else
        begin
            frm.txt_Nombre.Text:= FreDatoPersona.txt_Nombre.Text;
            frm.txt_Apellido.Text:= FreDatoPersona.txt_Apellido.Text;
            frm.txt_ApellidoMaterno.Text:= FreDatoPersona.txt_ApellidoMaterno.Text;
        end;
        frm.OnClickRegSeleccionado:= OnClickVehiculoRUTSelected;
        frm.ShowModal;
        //if CodigoConvenioRUT<>0 then     //TASK_051_ECA_20160707
        AgregarVehiculosCuentaComercial(frm.Vehiculo);
        frm.Free;
    end;
end;

procedure TFormModificacionConvenio.AgregarVehiculosCuentaComercial(Vehiculo: array of TCuentaVehiculo);
var
 almacen: string;
 i: Integer;
begin
    if cdsVehiculosContacto.RecordCount>0 then
       cdsVehiculosContacto.Last;

     for i := 0 to Length(Vehiculo)-1 do
    begin
        //almacen:=iif(StrToInt(Vehiculo[9])= CONST_ALMACEN_EN_COMODATO,CONST_STR_ALMACEN_COMODATO,iif(StrToInt(Vehiculo[9]) = CONST_ALMACEN_ARRIENDO,CONST_STR_ALMACEN_ARRIENDO,iif(StrToInt(Vehiculo[9])= CONST_ALMACEN_ENTREGADO_EN_CUOTAS,CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS,'')));
        almacen:=iif(Vehiculo[i].CodigoAlmacenDestino= CONST_ALMACEN_EN_COMODATO,CONST_STR_ALMACEN_COMODATO,iif(Vehiculo[i].CodigoAlmacenDestino = CONST_ALMACEN_ARRIENDO,CONST_STR_ALMACEN_ARRIENDO,
        iif(Vehiculo[i].CodigoAlmacenDestino= CONST_ALMACEN_ENTREGADO_EN_CUOTAS,CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS,'')));

        //INICIO: TASK_050_ECA_20160706
        with cdsVehiculosContacto do begin
            Append;
            FieldByName('TipoPatente').AsString             := '';
            FieldByName('CodigoVehiculo').Value             := Vehiculo[i].Vehiculo.CodigoVehiculo;;
            FieldByName('Patente').AsString                 := Vehiculo[i].Vehiculo.Patente;
            FieldByName('Modelo').AsString                  := Vehiculo[i].Vehiculo.Modelo;
            FieldByName('CodigoMarca').Value                := 0;
            FieldByName('DescripcionMarca').AsString        := Vehiculo[i].Vehiculo.Marca;
            FieldByName('CodigoColor').AsInteger            := 0;
            FieldByName('DescripcionColor').AsString        := '';
            FieldByName('AnioVehiculo').Value               := Vehiculo[i].Vehiculo.Anio;
            FieldByName('CodigoConvenio').Value             := Vehiculo[i].CodigoConvenioRNUT;
            FieldByName('DescripcionTipoVehiculo').AsString := Vehiculo[i].Vehiculo.Tipo;
            FieldByName('Observaciones').AsString           := '';
            FieldByName('ContextMark').Value                := 0;
            FieldByName('ContractSerialNumber').Value       := Vehiculo[i].ContactSerialNumber;
            FieldByName('SerialNumberaMostrar').Value       := Vehiculo[i].SerialNumberaMostrar;
            FieldByName('TieneAcoplado').Value              := null;
            FieldByName('DigitoVerificadorPatente').Value   := '';
            FieldByName('DigitoVerificadorValido').Value    := 0;
            FieldByName('PatenteRVM').Value                 := Vehiculo[i].Vehiculo.Patente;
            FieldByName('DigitoVerificadorPatenteRVM').Value:= '';
            FieldByName('TeleviaNuevo').Value               := True;
            FieldByName('FechaCreacion').Value:= Vehiculo[i].FechaCreacion;
            FieldByName('Robado').Value := False;
            FieldByName('Almacen').Value := almacen;
            FieldByName('Estado').Value := 'Nuevo';
            FieldByName('FechaVencimientoGarantia').Value := Vehiculo[i].FechaBajaTag;
            FieldByName('CodigoConvenioFacturacion').Value := 0;
            FieldByName('NumeroConvenioFacturacion').Value := '';

            Post;
        end;
    end;

    cdsVehiculosContacto.First;
    btn_Guardar.Enabled:=True;
    btnEliminarVehiculo.Enabled:=True;
end;

procedure TFormModificacionConvenio.EliminarVehiculosCuentaComercial;
resourcestring
    MSG_ELIMINAR_CTA_OK='Cambio Exitoso del estado de la cuenta a "Eliminado", sera efectivo al guardar los datos' ;  //TASK_019_20160531
begin
   with cdsVehiculosContacto do begin
       Edit;
       FieldByName('Estado').Value := 'Eliminado';
       post;
    end;
   btn_Guardar.Enabled:=True;
   MsgBox(MSG_ELIMINAR_CTA_OK, FMsgBoxCaption, MB_ICONINFORMATION);  //TASK_019_20160531
end;

procedure TFormModificacionConvenio.GuardarVehiculosCuentaComercial;
begin
    if cdsVehiculosContacto.RecordCount>0 then
       cdsVehiculosContacto.First;

    while not cdsVehiculosContacto.eof do
    begin
        if cdsVehiculosContacto.FieldByName('Estado').Value= 'Nuevo' then
            ActualizarVehiculosCuentaComercial(cdsVehiculosContacto.FieldByName('CodigoConvenio').AsInteger,
                                               CodigoConvenioFacturacion,
                                               cdsVehiculosContacto.FieldByName('CodigoVehiculo').AsInteger,
                                               UsuarioSistema,'I');

        if cdsVehiculosContacto.FieldByName('Estado').Value= 'Eliminado' then
            ActualizarVehiculosCuentaComercial(cdsVehiculosContacto.FieldByName('CodigoConvenio').AsInteger,
                                               CodigoConvenioFacturacion,
                                               cdsVehiculosContacto.FieldByName('CodigoVehiculo').AsInteger,
                                               UsuarioSistema,'E');

        cdsVehiculosContacto.Next;
    end;
end;

procedure TFormModificacionConvenio.ActualizarVehiculosCuentaComercial(CodigoConvenioRUT,CodigoConvenioFacturacion,CodigoVehiculoRUT: Integer; Usuario, Opcion: string);
var
    sp: TADOStoredProc;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'ConvenioCuentasRUT_UPDATE';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenioRUT;
    sp.Parameters.ParamByName('@CodigoConvenioFacturacion').Value := FDatosConvenio.CodigoConvenio;
    sp.Parameters.ParamByName('@CodigoVehiculo').Value := CodigoVehiculoRUT;
    sp.Parameters.ParamByName('@Usuario').Value := Usuario;
    sp.Parameters.ParamByName('@opcion').Value := Opcion;
    sp.ExecProc;
end;

procedure TFormModificacionConvenio.ActualizarPersonaCuentaComercial(CodigoPersona,CuentaComercial: Integer);
var
    sp: TADOStoredProc;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'CRM_PersonasCuentaComercial_UPDATE';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
    sp.Parameters.ParamByName('@CuentaComercial').Value := CuentaComercial;
    sp.ExecProc;
end;

function TFormModificacionConvenio.ValidarRestriccionesSuscripcionListaBlanca(CodigoPersona: Integer): Boolean;
var
    sp: TADOStoredProc;
begin
    CambiarEstadoCursor(CURSOR_RELOJ);
    Result:=False;

    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'ValidarRestriccionesAdhesionListaBlanca';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
    sp.Parameters.ParamByName('@ConDetalle').Value := 1;
    sp.Open;

    if sp.Parameters.ParamByName('@PuedeAdherirse').Value = True then
       Result:=True
    else
    begin
         if FCodigoConcesionariaNativa = FDatosConvenio.CodigoConcesionaria then
         begin
            if sp.FieldByName('Codigo').AsString='2' then
               Result:=True
         end
    end;

    sp.Close;
    FreeAndNil(sp);
    CambiarEstadoCursor(CURSOR_DEFECTO);
end;

procedure TFormModificacionConvenio.CargarConvenioClienteTipo;
var
    sp: TADOStoredProc;
    i: Integer;
    cbindex: Integer;
begin
    Screen.Cursor   := crHourGlass;
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'CRM_ConvenioClienteTipo_SELECT';
    sp.Open;
    if not sp.IsEmpty then
    begin
        cbbTipoCliente.Items.Clear;
        cbindex:=0;
        for i := 0 to sp.RecordCount - 1 do
        begin
           if FDatosConvenio.CodigoTipoCliente=sp.FieldByName('CodigoTipoCliente').AsInteger then
              cbindex:=i;
           cbbTipoCliente.Items.Add(sp.FieldByName('Descripcion').AsString,sp.FieldByName('CodigoTipoCliente').AsString);
           sp.Next;
        end;
        cbbTipoCliente.ItemIndex:=cbindex;
    end;
    sp.Close;
    sp.Free;
    Screen.Cursor   := crDefault;
end;







{TASK_056_JMA_20161012 - ESTOS PROCEDIMIENTOS/EVENTOS NUNCA SE USAN

-------------------------------------------------------------------------------
Procedure Name	: btn_editarClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Accion al hacer click en el boton editar
--------------------------------------------------------------------------------
procedure TFormModificacionConvenio.btn_editarClick(Sender: TObject);
begin
    Close;
end;                

{-------------------------------------------------------------------------------
Procedure Name	: btnSalirClick
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Accion al evento click en el boton btnSalirClick(
--------------------------------------------------------------------------------
procedure TFormModificacionConvenio.btnSalirClick(Sender: TObject);
begin
    Release;
end;

//INICIO BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------
procedure TFormModificacionConvenio.cb_InhabilitadoPAClick(Sender: TObject);
begin
  FCambioInhabilitado  := Not(FCambioInhabilitado);
  HabilitarBitsListaDeAcceso(True);
//  ActualizaControlesPorModificacion(FCambioAdheridoPA or (FCambioInhabilitado and (cb_MotivosInhabilitacion.ItemIndex>0))); TASK_004_ECA_20160411
  ActualizaControlesPorModificacion(FCambioAdheridoPA or FCambioInhabilitado);  //TASK_004_ECA_20160411
end;
//FIN BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------

{-------------------------------------------------------------------------------
Procedure Name  : ArmarListaPatentes
Author          : Nelson Droguett Sierra
Date Created    : 19-Abril-2010
Description     : Arma una lista con las patentes del convenio
--------------------------------------------------------------------------------
procedure TFormModificacionConvenio.ArmarListaPatentes(var Cadena: TStringList; sacarPatente: AnsiString = ''; sacarPatenteRVM: AnsiString = '');
var
    Book: TBookmark;
begin
    if not (cdsVehiculosContacto.IsEmpty) then begin
        book := cdsVehiculosContacto.GetBookmark;
        cdsVehiculosContacto.First;
        while not cdsVehiculosContacto.Eof do begin
            if not cdsVehiculosContacto.FieldByName('VehiculoEliminado').Value then begin
                if sacarPatente <> trim(cdsVehiculosContacto.FieldByName('Patente').Value) then begin
                    Cadena.Add(cdsVehiculosContacto.FieldByName('Patente').Value);
                end;

                if sacarPatenteRVM <> trim(cdsVehiculosContacto.FieldByName('PatenteRVM').Value) then begin
                    Cadena.Add(cdsVehiculosContacto.FieldByName('PatenteRVM').Value);
                end;
            end;

            cdsVehiculosContacto.Next;
        end;
        cdsVehiculosContacto.GotoBookmark(book);
        cdsVehiculosContacto.FreeBookmark(book);
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: ArmarListaTags
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Arma una lista con los TAGS del convenio
--------------------------------------------------------------------------------
procedure TFormModificacionConvenio.ArmarListaTags(var Cadena: TStringList; sacarTag: AnsiString = '');
var
    NroTag: string;
    Book: TBookmark;
begin
     if not (cdsVehiculosContacto.IsEmpty) then begin
        Book := cdsVehiculosContacto.GetBookmark;
        cdsVehiculosContacto.First;
        cdsVehiculosContacto.First;
        while not cdsVehiculosContacto.Eof do begin
            if not cdsVehiculosContacto.FieldByName('VehiculoEliminado').Value then begin
                if (cdsVehiculosContacto.fieldbyname('ContractSerialNumber').IsNull) then
                    NroTag := ''
                else
                    NroTag := cdsVehiculosContacto.fieldbyname('ContractSerialNumber').Value;

                if (trim(NroTag)<>'') and (sacarTag <> NroTag) then begin
                    Cadena.Add(cdsVehiculosContacto.FieldByName('ContractSerialNumber').Value);
                end;
            end;
            cdsVehiculosContacto.Next;
        end;
        cdsVehiculosContacto.GotoBookmark(Book);
        cdsVehiculosContacto.FreeBookmark(Book);
    end;
end;

{-------------------------------------------------------------------------------
Procedure Name	: IniciarCargaConRut
 Author 		: Nelson Droguett Sierra
 Date Created	: 19-Abril-2010
 Description	: Inicia la carga de datos con el RUT
--------------------------------------------------------------------------------
procedure TFormModificacionConvenio.IniciarCargaConRut;
Var
	ValidarRut:integer;
    DatoPersona: TDatosPersonales;
begin
    //valido estos datos antes, asi no llamo al SP
    DatoPersona := FreDatoPersona.RegistrodeDatos;
    if (length(Trim(DatoPersona.NumeroDocumento)) < 8) then begin
        if FMDomicilioPrincipal.cb_BuscarCalle.Enabled then HabilitarControles(false);
        Exit;
    end;

    ValidarRut := QueryGetValueInt(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
          StrLeft(DatoPersona.NumeroDocumento, (Length(DatoPersona.NumeroDocumento) -1)) +
          ''',''' +
          DatoPersona.NumeroDocumento[Length(DatoPersona.NumeroDocumento)] + '''');

    if ValidarRut = 0  then begin
        exit;
    end;
end;

//INICIO BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------
procedure TFormModificacionConvenio.HabilitarBitsListaDeAcceso(Habilita:Boolean=False);
resourcestring
    SQL_CONVENIOINHABILITADO    = 'SELECT dbo.ConvenioInhabilitadoEnListaAcceso(%d,%d)';
    SQL_CONCESIONARIA           = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()';
    SQL_INHABILITACION_MOROSIDAD= 'SELECT dbo.CONST_CODIGO_MOTIVO_INHABILITACION_POR_MOROSIDAD()';
var
  iCodigoMotivoInhabilitacion:Integer;
begin
  if not Habilita then
  begin
    //cb_AdheridoPA.Enabled:=False; TASK_004_ECA_20160411
    //btnAdherirPorCuenta.Enabled:=False;    //TASK_004_ECA_20160411

    //cb_InhabilitadoPA.Enabled:=False; //TASK_004_ECA_20160411
    //cb_InhabilitadoPA.Visible:=False; //TASK_004_ECA_20160411

    //cb_MotivosInhabilitacion.Enabled:=False; TASK_004_ECA_20160411
    //cb_MotivosInhabilitacion.Visible:=False; TASK_004_ECA_20160411
  end
  else
  begin
    //cb_AdheridoPA.Enabled := ExisteAcceso('BIT_Adherido_Lista_Acceso_PA') and                 TASK_004_ECA_20160411
    //                         (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE); TASK_004_ECA_20160411
    //btnAdherirPorCuenta.Enabled:=cb_AdheridoPA.Checked and cb_AdheridoPA.Enabled; //TASK_004_ECA_20160411
    if FDatosConvenioOriginal.AdheridoPA then
    begin
      iCodigoMotivoInhabilitacion := QueryGetValueInt(  DMConnections.BaseCAC,
                                                        Format( SQL_CONVENIOINHABILITADO,
                                                                [ FDatosConvenio.CodigoConvenio,
                                                                  QueryGetValueInt( DMConnections.BaseCAC,
                                                                                    SQL_CONCESIONARIA
                                                                                  )
                                                                ]
                                                              )
                                                      );
       //INICIO: TASK_004_ECA_20160411
//       if (iCodigoMotivoInhabilitacion=1) then
//       begin
//         cb_InhabilitadoPA.Font.Style := [fsBold,fsUnderline];
//         cb_InhabilitadoPA.Font.Color:=clRed;
//       end
//       else
//       begin
//         cb_InhabilitadoPA.Font.Style := [];
//         cb_InhabilitadoPA.Font.Color:=clWindowText;
//       end;
//
//       cb_InhabilitadoPA.Visible    :=  False; //cb_AdheridoPA.Checked; TASK_004_ECA_20160411
//       //cb_InhabilitadoPA.Enabled    :=  ExisteAcceso('BIT_Adherido_Lista_Acceso_PA_Inhabilitar') and   TASK_004_ECA_20160411
//                                        //cb_AdheridoPA.Checked and             //SS_1178_NDR_20140430
//                                        //(iCodigoMotivoInhabilitacion<>1);     //SS_1178_NDR_20140430
//       //                                 cb_AdheridoPA.Checked;                  //SS_1178_NDR_20140430 TASK_004_ECA_20160411
       //FIN: TASK_004_ECA_20160411
      // cb_MotivosInhabilitacion.Value:= IfThen(cb_MotivosInhabilitacion.Value<>-1,cb_MotivosInhabilitacion.Value,IfThen(iCodigoMotivoInhabilitacion>0,iCodigoMotivoInhabilitacion,-1)); TASK_004_ECA_20160411
       //cb_MotivosInhabilitacion.Font.Style:=cb_InhabilitadoPA.Font.Style; TASK_004_ECA_20160411
       //cb_MotivosInhabilitacion.Visible:=cb_InhabilitadoPA.Checked and cb_InhabilitadoPA.Visible; TASK_004_ECA_20160411
       //cb_MotivosInhabilitacion.Enabled := ExisteAcceso('BIT_Adherido_Lista_Acceso_PA_Inhabilitar') and FCambioInhabilitado and cb_InhabilitadoPA.Checked; TASK_004_ECA_20160411
    end;
  end;
end;


procedure TFormModificacionConvenio.ImprimirContratoAdhesionPA;
	resourcestring
    	rsTituloActivacion  = 'Activaci�n Servicio Arauco TAG';
        rsMensajeActivacion = '� Firm� el Cliente el contrato del Servicio Arauco TAG ?';
        rsMensajeActivado   = 'El Servicio Arauco TAG, se activ� correctamente para el Cliente.';
    	rsTituloImpresion   = 'Impresi�n Contrato Servicio Arauco TAG';
        rsMensajeImpresion  = '� Se imprimi� el contrato del Servicio Arauco TAG correctamente ?';
	var
        EMailPersona,
        NombrePersona,
        Firma,
        NumeroDocumento: string;
        ClienteAdheridoEnvioEMail: Boolean;
        ReporteContratoAdhesionPAForm: TReporteContratoAdhesionPAForm;
        FechaAdhesion:TDateTime;
        RBInterfaceConfig: TRBConfig;
        spValidarRestriccionesAdhesionPAKAnexo: TADOStoredProc;			// SS_1006_PDO_20121122
        SituacionesARegularizar: AnsiString;							// SS_1006_PDO_20121122

    const
    	cSQLObtenerEmailPersona                  = 'SELECT dbo.ObtenerEmailPersona(%d)';
        cSQLFechaAdhesion                        = 'SELECT dbo.ObtenerFechaAdhesionPA(%d)';
        cSQLValidarConveniosPersonaEnvioPorEMail = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';
        cSQLObtenerNombrePersona				 = 'SELECT dbo.ObtenerNombrePersona(%d)';
        cSQLArmarRazonSocialyRepLegalEmpresa     = 'SELECT dbo.ArmarRazonSocialyRepLegalEmpresa(%d)';
begin
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	try
        	// SS_1006_PDO_20121122 Inicio Bloque
        	spValidarRestriccionesAdhesionPAKAnexo := TADOStoredProc.Create(nil);
            with spValidarRestriccionesAdhesionPAKAnexo do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'ValidarRestriccionesAdhesionPAKAnexo';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := FDatosConvenio.Cliente.CodigoPersona;
                Parameters.ParamByName('@DevolverValidaciones').Value := 1;
                Open;

                if RecordCount > 0 then begin
                	while not Eof do begin

                        if SituacionesARegularizar <> EmptyStr then begin
                        	SituacionesARegularizar := SituacionesARegularizar + '\par \par ';
                        end;

                        SituacionesARegularizar := SituacionesARegularizar + ' \tab ' + '- ' + AnsiUpperCase(FieldByName('Descripcion').AsString);

                        Next;
                    end;
                end;
            end;
            // SS_1006_PDO_20121122 Fin Bloque

            FechaAdhesion:= QueryGetDateValue(DMConnections.BaseCAC, Format(cSQLFechaAdhesion, [FDatosConvenio.CodigoConvenio]));
            EMailPersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerEmailPersona, [FDatosConvenio.Cliente.CodigoPersona]));
            ClienteAdheridoEnvioEMail := QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [FDatosConvenio.Cliente.CodigoPersona]));

            NumeroDocumento := FDatosConvenio.Cliente.Datos.NumeroDocumento;

            if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA then begin
				NombrePersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLArmarRazonSocialyRepLegalEmpresa, [FDatosConvenio.CodigoConvenio]));
	            Firma := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[0].CodigoPersona]));
            end
            else begin
				NombrePersona := QueryGetStringValue(DMConnections.BaseCAC, Format(cSQLObtenerNombrePersona, [FDatosConvenio.Cliente.CodigoPersona]));
            	Firma := NombrePersona;
            end;


            ReporteContratoAdhesionPAForm := TReporteContratoAdhesionPAForm.Create(nil);

            with ReporteContratoAdhesionPAForm do
            begin
                Inicializar(  NombrePersona,
                              Firma,	
                              Trim(NumeroDocumento),
                              EMailPersona,
                              FechaAdhesion,
                              ClienteAdheridoEnvioEMail,
                              False,						// SS_1006_PDO_20121122
                              False,                        // SS_1006_PDO_20121122
                              SituacionesARegularizar       // SS_1006_PDO_20121122
                           );

                RBInterface.ModalPreview := False;
                RBInterfaceConfig := RBInterface.GetConfig;

                with RBInterfaceConfig do begin
	                Copies       := 2;
                    MarginTop    := 0;
                    MarginLeft   := 0;
                    MarginRight  := 0;
                    MarginBottom := 0;
                    Orientation  := poPortrait;
	                {$IFDEF DESARROLLO or TESTKTC
                    	PaperName    := 'Letter';
                    {$ELSE
                    	PaperName    := 'Carta';
                    {$ENDIF
                    ShowUser     := False;
                    ShowDateTime := False;
                end;

                RBInterface.SetConfig(RBInterfaceConfig);
                RBInterface.Execute(True);
//                if RBInterface.Execute(True) then
//                begin
//
//                    while ShowMsgBoxCN(rsTituloImpresion, rsMensajeImpresion, MB_ICONQUESTION + MB_YESNO, Self) <> mrOk do begin
//	                    RBInterface.Execute(True);
//                    end;
//
//                    if ShowMsgBoxCN(rsTituloActivacion, rsMensajeActivacion, MB_ICONQUESTION + MB_YESNO, Self) = mrOk then begin
//                        ShowMsgBoxCN(rsTituloActivacion, rsMensajeActivado, MB_ICONINFORMATION, Self);
//                    end;
//                end;
            end;
        except
        	on e: Exception do
          begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	if Assigned(ReporteContratoAdhesionPAForm) then FreeAndNil(ReporteContratoAdhesionPAForm);
        // SS_1006_PDO_20121122 Inicio Bloque
        if Assigned(spValidarRestriccionesAdhesionPAKAnexo) then begin
        	if spValidarRestriccionesAdhesionPAKAnexo.Active then spValidarRestriccionesAdhesionPAKAnexo.Close;
            FreeAndNil(spValidarRestriccionesAdhesionPAKAnexo);
        end;
        // SS_1006_PDO_20121122 Fin BLoque
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

//FIN BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------

}


//INICIO : TASK_004_ECA_20160411
//procedure TFormModificacionConvenio.chkConvenioSinCuentaClick(Sender: TObject);
//resourcestring
//    MSG_NO_ES_SIN_CUENTA = 'No puede marcar este convenio como sin cuenta ya que ha agregado Cuentas';
//begin
//    if (chkConvenioSinCuenta.Checked) and (cdsVehiculosContacto.RecordCount > 0)  then begin
//        chkConvenioSinCuenta.Checked := false;
//        MsgBoxBalloon(MSG_NO_ES_SIN_CUENTA, Self.Caption, MB_ICONSTOP, dblVehiculos);
//    end
//    else
//        ActualizaControlesPorModificacion(True);                                //SS_628_ALA_20111207
//end;
//FIN: TASK_004_ECA_20160411

//function TFormModificacionConvenio.ValidarEstadoTAGvsMotivo(): Boolean;  TASK_012_ECA_20160514
//begin                                                                    TASK_012_ECA_20160514
//    Result:=True;                                                        TASK_012_ECA_20160514
//end;                                                                     TASK_012_ECA_20160514
//FIN: TASK_005_ECA_20160420


//INICIO: TASK_004_ECA_20160411
//INICIO BLOQUE SS-1006-NDR-20120614--------------------------------------------
{Procedure Name	: cb_AdheridoPAClick
 Author 		: Nelson Droguett Sierra ()
 Date Created	: 19-Abril-2010
 Description	: Al hacer click en el chkBox, se cambian Flags que influyen despues
                	en la grabacion.
--------------------------------------------------------------------------------}
//procedure TFormModificacionConvenio.cb_AdheridoPAClick(Sender: TObject);
//resourcestring
//    SQL_CONVENIOINHABILITADO        = 'SELECT dbo.ConvenioInhabilitadoEnListaAcceso(%d,%d)';
//    SQL_CONCESIONARIA               = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_PARQUE_ARAUCO()';
//var
//    auxVehiculo: TVehiculosConvenio;
//    spValidarRestriccionesAdhesionPAKAnexo: TADOStoredProc;
//	SituacionesARegularizar: Boolean;
//begin
//
//    dblVehiculos.DataSource.DataSet.DisableControls;                                                                    
//    with cdsVehiculosContacto do                                                                                        
//    begin                                                                                                               
//      First;                                                                                                            
//      while not eof do                                                                                                  
//      begin
//        if Pos('-B',FieldByName('Patente').AsString)=0 then                                                 //SS_1006_NDR_20121003
//        begin                                                                                               //SS_1006_NDR_20121003
//        Edit;                                                                                                           
//        FieldByName('AdheridoPA').AsBoolean := StrToBool(IfThen(  False,   //cb_AdheridoPA.Checked, TASK_004_ECA_20160411
//                                                                  IfThen( FCambioAdheridoPA,                            
//                                                                          FieldByName('AdheridoPAOriginal').AsString,   
//                                                                          '1'                                           
//                                                                        ),                                              
//                                                                  '0'                                                   
//                                                                )                                                       
//                                                         );                                                             
//        Post;                                                                                                           
//        auxVehiculo:=FDatosConvenio.Cuentas.ObtenerVehiculo(FieldByName('Patente').AsString);                           
//        auxVehiculo.AdheridoPA:=FieldByName('AdheridoPA').AsBoolean;                                                    
//        auxVehiculo.Cuenta.AdheridoPA:=FieldByName('AdheridoPA').AsBoolean;                                             
//        FDatosConvenio.Cuentas.EditarVehiculo(FDatosConvenio.Cuentas.BuscarIndice(FieldByName('Patente').AsString), auxVehiculo);
//        end;                                                                                                //SS_1006_NDR_20121003
//        Next;
//      end;                                                                                                              
//      First;                                                                                                            
//    end;                                                                                                                
//    dblVehiculos.DataSource.DataSet.EnableControls;
//    FCambioAdheridoPA := Not(FCambioAdheridoPA);
//    FDatosConvenio.AdheridoPA := False;//cb_AdheridoPA.Checked; TASK_004_ECA_20160411
//    HabilitarBitsListaDeAcceso(True);
//    ActualizaControlesPorModificacion(FCambioAdheridoPA or (FCambioInhabilitado and (cb_MotivosInhabilitacion.ItemIndex>0)));
//
//    if cb_AdheridoPA.Checked then try
//        try
//        	spValidarRestriccionesAdhesionPAKAnexo := TADOStoredProc.Create(nil);
//
//            with spValidarRestriccionesAdhesionPAKAnexo do begin
//                Connection := DMConnections.BaseCAC;
//                ProcedureName := 'ValidarRestriccionesAdhesionPAKAnexo';
//                Parameters.Refresh;
//                Parameters.ParamByName('@CodigoPersona').Value := FDatosConvenio.CodigoPersona;
//                Parameters.ParamByName('@DevolverValidaciones').Value := 0;
//                Parameters.ParamByName('@InfraccionesPendientes').Value := 0;
//                Parameters.ParamByName('@TAGsVencidos').Value := 0;
//                Parameters.ParamByName('@CuentasSuspendidas').Value := 0;
//                ExecProc;
//
//                SituacionesARegularizar :=
//	                (Parameters.ParamByName('@InfraccionesPendientes').Value > 0)
//	                or (Parameters.ParamByName('@TAGsVencidos').Value > 0)
//	                or (Parameters.ParamByName('@CuentasSuspendidas').Value > 0);
//            end;
//
//        	if SituacionesARegularizar then try
//                try
//                    FormConsultaAltaPAraucoSoloConsulta := TFormConsultaAltaPArauco.ModalCreate(nil);
//                    if FormConsultaAltaPAraucoSoloCOnsulta.Inicializar(FDatosConvenio.Cliente.Datos.NumeroDocumento) then begin
//                        FormConsultaAltaPAraucoSoloCOnsulta.ShowModal;
//                    end;
//                except
//                    on e: Exception do begin
//                        ShowMsgBoxCN(e, Self);
//                    end;
//                end;
//            finally
//                if Assigned(FormConsultaAltaPAraucoSoloCOnsulta) then FreeAndNil(FormConsultaAltaPAraucoSoloCOnsulta);
//            end;
//        except
//            on e: Exception do begin
//                ShowMsgBoxCN(e, Self);
//            end;
//        end;
//finally
//        if Assigned(spValidarRestriccionesAdhesionPAKAnexo) then FreeAndNil(spValidarRestriccionesAdhesionPAKAnexo);
//end;
//end;

//FIN BLOQUE SS-1006-NDR-20120614--------------------------------------------
//FIN: TASK_004_ECA_20160411


//INICIO: TASK_004_ECA_20160411
////INICIO BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------
//function TFormModificacionConvenio.CargarComboMotivosInhabilitacion: Boolean;
//var
//    ADOStoredProc: TADOStoredProc;
//begin
//  ADOStoredProc := TADOStoredProc.Create(nil);
//  try
//    with ADOStoredProc do
//    begin
//        Connection := DMConnections.BaseCAC;
//        ProcedureName := 'ObtenerMotivosInhabilitacion';
//        Open;
//        cb_MotivosInhabilitacion.Clear;
//        cb_MotivosInhabilitacion.Items.Add('Seleccione Motivo', -1);
//        while not Eof do begin
//            cb_MotivosInhabilitacion.Items.Add( FieldByName('Descripcion').AsString,
//                                                FieldByName('CodigoMotivoInhabilitacion').AsInteger
//                                              );
//            Next;
//        end;
//        cb_MotivosInhabilitacion.ItemIndex := 0;
//    end;
//  finally
//    if Assigned(ADOStoredProc) then
//    begin
//      if ADOStoredProc.Active then ADOStoredProc.Close;
//      FreeAndNil(ADOStoredProc);
//    end;
//  end;
//end;
////FIN BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------
//FIN: TASK_004_ECA_20160411



//INICIO: TASK_004_ECA_20160411
//INICIO BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------
//procedure TFormModificacionConvenio.cb_MotivosInhabilitacionChange(
//  Sender: TObject);
//resourcestring
//    SQL_INHABILITACION_MOROSIDAD= 'SELECT dbo.CONST_CODIGO_MOTIVO_INHABILITACION_POR_MOROSIDAD()';    //SS-1006-NDR-20120614
//begin
//  if cb_MotivosInhabilitacion.Value=QueryGetValueInt(DMConnections.BaseCAC,SQL_INHABILITACION_MOROSIDAD) then
//  begin
//    ShowMessage('No se puede elegir MOROSIDAD en forma manual');
//    cb_MotivosInhabilitacion.ItemIndex:=0;
//  end;
//  ActualizaControlesPorModificacion(cb_MotivosInhabilitacion.ItemIndex>0); // SS-887-PDO-20100715
//end;
//FIN BLOQUE : SS-1006-NDR-20120614------------------------------------------------------------------------
//FIN: TASK_004_ECA_20160411
end.
