object formFirmarConvenio: TformFirmarConvenio
  Left = 278
  Top = 200
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Firmar Convenio'
  ClientHeight = 91
  ClientWidth = 322
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    322
    91)
  PixelsPerInch = 96
  TextHeight = 13
  object txtEstado: TLabel
    Left = 7
    Top = 16
    Width = 306
    Height = 20
    Alignment = taCenter
    AutoSize = False
    Caption = 'Seleccione una opci'#243'n'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnFirmarContrato: TButton
    Left = 30
    Top = 59
    Width = 120
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Confirmar Convenio'
    Default = True
    TabOrder = 0
    OnClick = btnFirmarContratoClick
  end
  object btnCancelar: TButton
    Left = 171
    Top = 59
    Width = 120
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = '&Modificar'
    ModalResult = 2
    TabOrder = 1
    OnClick = btnCancelarClick
  end
end
