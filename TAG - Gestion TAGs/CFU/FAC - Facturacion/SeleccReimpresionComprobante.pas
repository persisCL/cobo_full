{-----------------------------------------------------------------------------
 Unit Name: SeleccReimpresionComprobante.pas
 Author:    None
 Date Created: 23/03/2005
 Language: ES-AR
 Description: Formulario que se usa para seleccionar el modo de impresion del
              comprobante
-----------------------------------------------------------------------------
Revision 1:
    Author : lgisuk
    Date : 03/11/2006
    Description :
        - El boton "Imprimir" ahora debe decir "Imprimir con Cobro"
        - Si se hace click en el boton imprimir gratis se debera descontar la
          impresion sin hacer un movimiento de cuenta. cambie de TRI_GRATIS
          a TRI_DESCONTAR
-----------------------------------------------------------------------------}
unit SeleccReimpresionComprobante;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UtilDB, DMConnection, ConstParametrosGenerales;

type


  TfrmSeleccReimpresion = class(TForm)
    lblTipoImpresion: TLabel;
    lblDescontarGratis: TLabel;
    lblDetalle: TLabel;
    btnImprimir: TButton;
    btnImprimirGratis: TButton;
    btnCancelar: TButton;
    procedure btnImprimirGratisClick(Sender: TObject);
  private
    { Private declarations }
    FTipoReimpresion: Integer;
  public
    { Public declarations }
    Function Inicializar(TipoComprobante:String; NumeroComprobante:Int64): boolean;
    property TipoReimpresion: integer read FTipoReimpresion;
  end;

const
  TRI_GRATIS    =  -1;
  TRI_PAGA      =   1;
  TRI_DESCONTAR =   0;
  TRI_ORIGINAL  =   2;

var
  frmSeleccReimpresion: TfrmSeleccReimpresion;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    None
  Date Created: 23/03/2005
  Description: Inicializaci�n de Este formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
Revision 1:
    Author : lgisuk
    Date : 03/11/2006
    Description :
        - El boton "Imprimir" ahora debe decir "Imprimir con Cobro"
-----------------------------------------------------------------------------
Revision 2:
    Author : lgisuk
    Date : 06/11/2006
    Description :
        - Ahora cuando el cliente tiene que pagar por las reimpresiones, se
        muestra el mismo cartel que cuando tiene impresiones gratis, solo que
        informa que la cantidad de impresiones gratis es cero.
        - Cambie nombre resourcestring MSG_FREE_REPRINT a MSG_REPRINTING_VOUCHER
        - Elimine el resourcestring MSG_CHARGED_REPRINT
-----------------------------------------------------------------------------}
Function TfrmSeleccReimpresion.Inicializar(TipoComprobante:String; NumeroComprobante:Int64): boolean;
resourcestring
    MSG_REPRINTING_VOUCHER                 = 'Reimpresi�n de Comprobantes';
    MSG_FREE_DISCOUNTED			    = 'Ser� descontada de sus reimpresiones gratis';
    MSG_REMAINING_FREE_REPRINTS	    = 'Cantidad de reimpresiones gratis restantes: %d';
    MSG_REPRINTING_CHARGE			= 'La reimpresi�n del comprobante tiene un costo de $ %d';
    MSG_PRINTING_ORIGINAL           = 'Es la impresi�n del ORIGINAL';
    MSG_PRINTING_ORIGINAL_DESC      = 'Se imprimir� un Original. Es sin cargo para el cliente.';
    MSG_PRINTING_ORIGINAL_WARNING   = 'Esta acci�n har� que el Comprobante no se envie a la imprenta.';

var
    CantMaxGratis, CantReimpresiones, CostoReimpresion: integer;
begin
    CantReimpresiones := QueryGetValueInt(DMConnections.BaseCAC,
                                        Format(' SELECT CantidadReimpresiones FROM Comprobantes WITH(NOLOCK) '+
                                               ' WHERE TipoComprobante = ''%s'' AND NumeroComprobante = %d'
                                        ,[TipoComprobante, NumeroComprobante]));

    ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_REIMPRESION_COMPROBANTE_GRATIS',CantMaxGratis);

    if CantReimpresiones < cantMaxGratis then begin
        // El cliente tiene reimpresiones gratuitas disponibles
        if ( QueryGetValueInt(DMConnections.BaseCAC, 'SELECT ISNULL('+
                                  '(Select 1 From Comprobantes WITH(NOLOCK) '+
                                  Format(' Where TipoComprobante = ''%s'' and NumeroComprobante = %d ',
                                  [TipoComprobante, NumeroComprobante])+
                                  ' AND FechaHoraImpreso IS NOT NULL )'+
                                  ',0)') = 0 ) then begin
            // era null --> es original
            lblTipoImpresion.Caption := MSG_PRINTING_ORIGINAL;
            lblDescontarGratis.Caption := MSG_PRINTING_ORIGINAL_DESC;
            lblDetalle.Caption := MSG_PRINTING_ORIGINAL_WARNING;
            FTipoReimpresion := TRI_ORIGINAL;

        end else
        begin
            lblTipoImpresion.Caption := MSG_REPRINTING_VOUCHER;
            lblDescontarGratis.Caption := MSG_FREE_DISCOUNTED;
            lblDetalle.Caption := Format(MSG_REMAINING_FREE_REPRINTS, [(CantMaxGratis - CantReimpresiones)]);
            FTipoReimpresion := TRI_DESCONTAR;
            //btnImprimir.Enabled := False;
        end;
    end
    else begin
        // El cliente tiene que pagar por las reimpresiones
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'PRECIO_REIMPRESION_COMPROBANTE',CostoReimpresion) ;
        CostoReimpresion := CostoReimpresion div 100;
        lblTipoImpresion.Caption := MSG_REPRINTING_VOUCHER; //Modificado en revision 2
        lblDescontarGratis.Caption := Format(MSG_REMAINING_FREE_REPRINTS, [(0)]);  
        lblDetalle.Caption := Format(MSG_REPRINTING_CHARGE, [CostoReimpresion]);
        FTipoReimpresion := TRI_PAGA;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btnImprimirGratisClick
  Author:    None
  Date Created:  23/03/2005
  Description: Imprimir Gratis comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
Revision 1:
    Author : lgisuk
    Date : 03/11/2006
    Description :
        - Si se hace click en el boton imprimir gratis se debera descontar la
          impresion sin hacer un movimiento de cuenta. cambie de TRI_GRATIS
          a TRI_DESCONTAR
-----------------------------------------------------------------------------}
procedure TfrmSeleccReimpresion.btnImprimirGratisClick(Sender: TObject);
begin
    //Ahora al hacer click contara la impresion,
    //sin hacer un movimiento de cuenta.
    FTipoReimpresion :=  TRI_DESCONTAR;
end;

end.
