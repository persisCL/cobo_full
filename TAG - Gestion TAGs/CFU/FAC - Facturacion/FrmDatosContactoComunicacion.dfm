object FormDatosContactoComunicacion: TFormDatosContactoComunicacion
  Left = 211
  Top = 205
  BorderStyle = bsDialog
  Caption = 'Datos del Contacto'
  ClientHeight = 206
  ClientWidth = 662
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 10
    Top = 11
    Width = 69
    Height = 13
    Caption = 'Documento:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object pnl_Botones: TPanel
    Left = 0
    Top = 165
    Width = 662
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object btnAceptar: TButton
      Left = 458
      Top = 8
      Width = 97
      Height = 25
      Caption = '&Asociar Datos'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnSalir: TButton
      Left = 559
      Top = 8
      Width = 92
      Height = 25
      Cancel = True
      Caption = '&Salir'
      ModalResult = 2
      TabOrder = 1
      OnClick = btnSalirClick
    end
  end
  object txt_Rut: TEdit
    Left = 112
    Top = 8
    Width = 121
    Height = 21
    Color = 16444382
    MaxLength = 9
    TabOrder = 0
    OnChange = txt_RutChange
    OnKeyPress = txt_RutKeyPress
  end
  object pnl_DatosPersona: TPanel
    Left = 5
    Top = 29
    Width = 660
    Height = 137
    BevelOuter = bvNone
    TabOrder = 1
    object lblApellido: TLabel
      Left = 6
      Top = 9
      Width = 50
      Height = 13
      Caption = 'Apellido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblApellidoMaterno: TLabel
      Left = 347
      Top = 8
      Width = 81
      Height = 13
      Caption = 'Apellido materno:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 497
      Top = 60
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object lblNombre: TLabel
      Left = 6
      Top = 35
      Width = 62
      Height = 13
      Caption = 'Nombre(s):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 7
      Top = 60
      Width = 45
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 7
      Top = 89
      Width = 91
      Height = 13
      Caption = 'Tel'#233'fono particular:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label21: TLabel
      Left = 10
      Top = 112
      Width = 28
      Height = 13
      Caption = 'Email:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txt_apellido: TEdit
      Left = 107
      Top = 6
      Width = 229
      Height = 21
      Color = 16444382
      TabOrder = 0
    end
    object txt_apellidoMaterno: TEdit
      Left = 433
      Top = 4
      Width = 215
      Height = 21
      TabOrder = 1
    end
    object txtNumeroCalle: TEdit
      Left = 545
      Top = 56
      Width = 98
      Height = 21
      MaxLength = 20
      TabOrder = 2
    end
    object txtEmailParticular: TEdit
      Left = 107
      Top = 108
      Width = 228
      Height = 21
      Hint = 'direcci'#243'n de e-mail'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object txtTelefonoParticular: TEdit
      Left = 107
      Top = 82
      Width = 121
      Height = 21
      Hint = 'N'#250'mero de tel'#233'fono'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
    end
    object txtDomicilio: TEdit
      Left = 107
      Top = 57
      Width = 378
      Height = 21
      MaxLength = 255
      TabOrder = 5
    end
    object txt_nombre: TEdit
      Left = 107
      Top = 31
      Width = 229
      Height = 21
      Color = 16444382
      TabOrder = 6
    end
  end
  object ObtenerDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 512
    Top = 8
  end
  object ObtenerMediosComunicacionPersonaFormato: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMediosComunicacionPersonaFormato'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Formato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 544
    Top = 8
  end
  object ObtenerDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomiciliosPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Principal'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 576
    Top = 8
  end
end
