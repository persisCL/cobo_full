object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 612
  ClientWidth = 894
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 96
    Top = 12
    Width = 55
    Height = 13
    Caption = 'Rut Emisor:'
  end
  object Label3: TLabel
    Left = 125
    Top = 65
    Width = 26
    Height = 13
    Caption = 'Folio:'
  end
  object Label4: TLabel
    Left = 80
    Top = 92
    Width = 71
    Height = 13
    Caption = 'Fecha Emisi'#243'n:'
  end
  object Label5: TLabel
    Left = 90
    Top = 119
    Width = 61
    Height = 13
    Caption = 'Monto Total:'
  end
  object Label6: TLabel
    Left = 87
    Top = 146
    Width = 64
    Height = 13
    Caption = 'Descrip Item:'
  end
  object Label7: TLabel
    Left = 83
    Top = 173
    Width = 68
    Height = 13
    Caption = 'Rut Receptor:'
  end
  object Label8: TLabel
    Left = 87
    Top = 200
    Width = 64
    Height = 13
    Caption = 'Raz'#243'n Social:'
  end
  object Label2: TLabel
    Left = 106
    Top = 38
    Width = 45
    Height = 13
    Caption = 'Tipo Doc:'
  end
  object Label9: TLabel
    Left = 57
    Top = 228
    Width = 94
    Height = 13
    Caption = 'Fecha Hora Timbre:'
  end
  object Label11: TLabel
    Left = 25
    Top = 334
    Width = 26
    Height = 13
    Caption = 'Folio:'
  end
  object imgTimbre: TImage
    Left = 399
    Top = 368
    Width = 396
    Height = 236
  end
  object lblArchivo: TLabel
    Left = 689
    Top = 287
    Width = 46
    Height = 13
    Caption = 'lblArchivo'
  end
  object edtRutEmi: TEdit
    Left = 157
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '076496130'
  end
  object edtDVEmi: TEdit
    Left = 284
    Top = 8
    Width = 29
    Height = 21
    TabOrder = 1
    Text = '7'
  end
  object edtFolio: TEdit
    Left = 157
    Top = 61
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '10'
  end
  object edtMonto: TEdit
    Left = 157
    Top = 115
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '10000'
  end
  object edtRutRecep: TEdit
    Left = 157
    Top = 169
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '12908094'
  end
  object edtDVRecep: TEdit
    Left = 284
    Top = 169
    Width = 25
    Height = 21
    TabOrder = 7
    Text = '9'
  end
  object edtRazonSocial: TEdit
    Left = 157
    Top = 196
    Width = 121
    Height = 21
    TabOrder = 8
    Text = 'Prueba'
  end
  object edtDescrip: TEdit
    Left = 157
    Top = 142
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'Producto 1'
  end
  object cbxTipoDoc: TComboBox
    Left = 157
    Top = 34
    Width = 156
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 2
    Text = 'Factura'
    Items.Strings = (
      'Factura'
      'FacturaExenta'
      'Boleta'
      'Boleta Exenta'
      'Guia'
      'Nota D'#233'bito'
      'Nota Cr'#233'dito')
  end
  object Button1: TButton
    Left = 168
    Top = 251
    Width = 75
    Height = 25
    Caption = 'Timbrar'
    TabOrder = 9
    OnClick = Button1Click
  end
  object mmoLog: TMemo
    Left = 8
    Top = 368
    Width = 385
    Height = 217
    Lines.Strings = (
      'mmoLog')
    ScrollBars = ssBoth
    TabOrder = 10
  end
  object Button2: TButton
    Left = 143
    Top = 329
    Width = 75
    Height = 25
    Caption = 'Pedir Folio'
    TabOrder = 11
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 292
    Top = 87
    Width = 90
    Height = 25
    Caption = 'Limpiar Datos'
    TabOrder = 12
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 224
    Top = 329
    Width = 105
    Height = 25
    Caption = 'Confirmar Folio'
    TabOrder = 13
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 335
    Top = 329
    Width = 102
    Height = 25
    Caption = 'Reversar Folio'
    TabOrder = 14
    OnClick = Button5Click
  end
  object edtFolioNuevo: TEdit
    Left = 57
    Top = 331
    Width = 80
    Height = 21
    TabOrder = 15
    Text = '100'
  end
  object dtpFechaEmision: TDateTimePicker
    Left = 157
    Top = 88
    Width = 129
    Height = 21
    Date = 39877.450232384260000000
    Time = 39877.450232384260000000
    TabOrder = 16
  end
  object dtpFechaTimbre: TDateTimePicker
    Left = 157
    Top = 224
    Width = 129
    Height = 21
    Date = 39877.450373078690000000
    Time = 39877.450373078690000000
    TabOrder = 17
  end
  object Button6: TButton
    Left = 410
    Top = 194
    Width = 264
    Height = 25
    Caption = 'Timbrar Din'#225'mico'
    TabOrder = 18
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 410
    Top = 164
    Width = 264
    Height = 25
    Caption = 'Cargar DLL'
    TabOrder = 19
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 410
    Top = 252
    Width = 264
    Height = 25
    Caption = 'LiberarDLL'
    TabOrder = 20
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 410
    Top = 282
    Width = 264
    Height = 25
    Caption = 'usar egate_pdf417.exe'
    TabOrder = 21
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 410
    Top = 135
    Width = 264
    Height = 25
    Caption = 'Configurar EGATE_HOME por par'#225'metro general'
    TabOrder = 22
    OnClick = Button10Click
  end
  object Button11: TButton
    Left = 410
    Top = 104
    Width = 264
    Height = 25
    Caption = 'Config EGATE_HOME a mano'
    TabOrder = 23
    OnClick = Button11Click
  end
  object Button12: TButton
    Tag = 10
    Left = 410
    Top = 223
    Width = 264
    Height = 25
    Caption = 'Timbrar y obtener Imagen JPG'
    TabOrder = 24
    OnClick = Button6Click
  end
end
