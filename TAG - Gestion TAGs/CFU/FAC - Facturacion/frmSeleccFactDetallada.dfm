object fSeleccFactDetallada: TfSeleccFactDetallada
  Left = 390
  Top = 287
  BorderIcons = [biSystemMenu]
  Caption = 'Impresi'#243'n Detallada'
  ClientHeight = 141
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblTipoImpresion: TLabel
    Left = 24
    Top = 16
    Width = 321
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Reimpresi'#243'n Detalle de Transacciones'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblDescontarGratis: TLabel
    Left = 24
    Top = 40
    Width = 321
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Ser'#225' descontada de sus impresiones gratis'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblDetalle: TLabel
    Left = 24
    Top = 64
    Width = 321
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Cantidad de impresiones gratis :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btnImprimir: TButton
    Left = 12
    Top = 110
    Width = 113
    Height = 25
    Caption = '&Imprimir con Cobro'
    ModalResult = 1
    TabOrder = 0
    OnClick = btnImprimirClick
  end
  object btnImprimirGratis: TButton
    Left = 127
    Top = 110
    Width = 113
    Height = 25
    Caption = 'Imprimir &Gratis'
    ModalResult = 4
    TabOrder = 1
    OnClick = btnImprimirGratisClick
  end
  object btnCancelar: TButton
    Left = 243
    Top = 110
    Width = 113
    Height = 25
    Caption = '&Cancelar'
    Default = True
    ModalResult = 2
    TabOrder = 2
  end
  object cbOpciones: TCheckBox
    Left = 16
    Top = 88
    Width = 225
    Height = 17
    Caption = 'Ver Opciones de Impresi'#243'n y Vista Previa.'
    TabOrder = 3
  end
  object spObtenerDatosFacturacionDetallada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosFacturacionDetallada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 330
    Top = 57
  end
end
