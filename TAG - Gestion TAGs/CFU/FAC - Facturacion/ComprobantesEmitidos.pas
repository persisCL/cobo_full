{-----------------------------------------------------------------------------
 File Name: ComprobantesEmitidos.pas
 Description:   Permite buscar un comprobante por tipo/numero, o seleccionando
                un RUT, permite seleccionar los convenios del mismo y ver
                sus comprobantes.
                Una ves seleccionado un comprobante se visualizan en las listas
                inferiores los movimientos de cuenta y los transitos facturados.

 Revision 1
 Author: jjofre
 Date: 26-Febrero-2010
 Description:    SS 830 Time Out Caf Faltante  .
                - Se agrega un Try a la funcion ImprimirComprobanteElectronico
                para rescatar el raiser de  que se pueda provocar
                en la funcion Preparar() llamada desde el beforePrint
                (ambas funciones de frmReporteBoletaFacturaElectronica)
                y emitir el MSgBOX que antes se emitia de beforePrint lo
                que provocaba Time out desde la WEB.

Revision	:2
Author		: Nelson Droguett Sierra
Date		: 23-Abril-2010
Description	: Se quita el menu contextual de la grilla de los transitos en la solapa
        	de transacciones. Este menu permite hacer un reclamo de los transitos
            pero esta grilla no permite seleccionar (checkbox) el transito a reclamar.
            La misma funcionalidad esta en la cartola de Transitos en el InicioConsultaConvenio
            y ahi si es posible hacer el reclamo.

Revision 3
Author: mbecerra
Date: 21-Junio-2010
Description:		(Ref Fase II)
            	Se agrega un nuevo TabSheet estacionamientos para el despliegue
                de las transacciones de estacionamiento del comprobante

Revision	: 3
Author		: Javier De Barbieri
Date		: 16 - Noviembre- 2010
Description	: En la funci�n MostrarDetalleOtrosComprobantes se verifica el tipo de
            comprobante seleccionado: Si corresponde a Nota De Credito a Nota De Cobro,
            muestra la suma de los conceptos, si corresponde a cualquier otro tipo de comprobante,
            muestra el Total a Cobrar.

Firma		: SS_959_MBE_20110617
Description	: Se agrega el comprobante devoluci�n de dinero

Firma       : SS_1012_ALA_20111121
Description : Se modifica el TADOStoredProc �spListadoCargos� de
              ObtenerListadoCargosFacturaAImprimir a ObtenerDetalleCuentaFacturaAImprimir.
              Adem�s se reemplaza el total a pagar por el total del documento.

Firma       : SS_1006_HUR_20120213
Descripcion : Se modifica procedure CargarConcesionarias para ditinguir concesionarias
              de peaje y estacionamientos.
              Se habilita la solapa "Estacionamientos".

Firma       : SS_1006_HUR_20120215
Descripcion : Se modifica campo a desplegar como importe por DescriImporte con el
              valor de Importe con formato

Firma       : SS_1015_HUR_20120413
Descripcion : Se agregan los tipos de comprobante Saldo Inicial Oc y Anula Saldo Inicial
              al combo Tipo Comprobantes para desplegar el detalle.

Firma       : SS_1015_HUR_20120417
Descripcion : Se modifican los tipos de comprobantes de Saldo Inicial

Firma       : SS-1015-NDR-20120510
Descripcion : Se agregan los tipos de Comprobantes Cuotas Otras Concesionarias (CO)
              y Anula Cuotas Otras Concesionarias (AC)

Firma       : SS_660_CQU_20121010
Descripcion : Los comprobantes generados en el proceso de facturaci�n masivo de morosos
              pueden contener conceptos de infraccion y transitos/peajes/cuotas/etc
              por lo tanto se deben imprimir ambos detalles si esto ocurre.

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.
              Se corrige el n�mero de la columna imagen de 7 a 9, que es lo que corresponde.

Firma       : SS_1120_MVI_20130820
Descripcion : Se cambia estilo del comboBox a: vcsOwnerDrawFixed, se crear el evento DrawItem y se llama al procedimeinto ItemComboBoxColor.

Firma		:	SS_1291_MBE_20150703
Description	:	Se agrega que ponga en color rojo los tr�nsitos anulados.

Fecha       :   19/08/2015
Firma       :   SS_1246_CQU_20150819
Descripcion :   Se pide: En la ventana de comprobantes emitidos, en el CAC, Eliminar el bot�n de �Imprimir con Cobro�
                        y cambiar la etiqueta del bot�n "Imprimir gratis" por "imprimir".
                        A partir de ahora todas las impresiones son gratis.
                Se hizo: Se elimin� la llamda a TfrmSeleccReimpresion ya que ese formulario NO HACE nada.

Fecha       :   06/10/2015
Firma       :   SS_1246_CQU_20151006
Descripcion :   Lo que se pidi� es quitar el cobro por reimpresi�n de comprobantes en TODAS partes,
                no s�lo el bot�n de imprimir gratis.

Firma       :   SS_1361_MCA_20151006
Descripcion :   se corrigen algunas funcionalidades del detalle de transitos y estacionamientos.

Fecha       :   23/10/2015
Firma       :   SS_1402_MBE_20151023
Descripcion :   Se corrige un error de asignaci�n en la impresi�n de comprobantes.

Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

Etiqueta    :   20160802 MGO
Descripci�n :   Hotfix facturas reimprimibles
 -----------------------------------------------------------------------------}

unit ComprobantesEmitidos;

interface

uses
	frmReporteBoletaFacturaElectronica,
    frmReporteNotaCreditoElectronica,
    frmReporteNotaDebitoElectronica,
    frmRptDetInfraccionesAnuladas,		//REV.15
    frmReporteCompDevDinero,          				//SS_959_MBE_20110617
    Windows, Messages, SysUtils, Variants, Classes, Controls, Forms, Dialogs,
    Dateedit, Buttons, ExtCtrls, ADODB, VariantComboBox, DmiCtrls, UtilProc,
    UtilDB, util, DMConnection, BuscaClientes, peaTypes, peaProcs, DPSControls,
    ImagenesTransitos, MenuesCOntextuales, Navigator, ComCtrls, DB, ListBoxEx,
    DBListEx, StdCtrls, ReporteFactura, DBClient, Graphics,
    frmReporteFacturacionDetallada, ConstParametrosGenerales,SeleccReimpresionComprobante,
	TimeEdit, Validate, DateUtils, ReporteCK, ReporteNC, CSVUtils;

type
  TFormComprobantesEmitidos = class(TForm)
    dsFacturas: TDataSource;
    dsTransitosPorComprobante: TDataSource;
    ObtenerTransitosPorComprobante: TADOStoredProc;
    gbDatosCliente: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    gbCliente: TGroupBox;
    Label1: TLabel;
    peNumeroDocumento: TPickEdit;
    spObtenerListaComprobantes: TADOStoredProc;
    gbComprobante: TGroupBox;
    Label5: TLabel;
    cbTipoComprobante: TVariantComboBox;
    Label2: TLabel;
    neNumeroComprobante: TNumericEdit;
    Label10: TLabel;
    Label3: TLabel;
    cbConvenios: TVariantComboBox;
    dblComprobantes: TDBListEx;
    Label4: TLabel;
    pgDetalles: TPageControl;
    tsDetalleComprobante: TTabSheet;
    tsTransacciones: TTabSheet;
    dblTransitos: TDBListEx;
    btnReimprimir: TButton;
    tsMensajes: TTabSheet;
    DBListEx1: TDBListEx;
    dblDetalle: TDBListEx;
    Detalle: TClientDataSet;
    dsDetalle: TDataSource;
    spListadoCargos: TADOStoredProc;
    dsMensajes: TDataSource;
    Label7: TLabel;
    lblConvenio: TLabel;
    lblRUT: TLabel;
    Label11: TLabel;
    spObtenerDetalleConceptosComprobante: TADOStoredProc;
    tmrConsultaRUT: TTimer;
    btnImprimirFacturacionDetallada: TButton;
    spMensajes: TADOStoredProc;
    spMensajesTexto: TStringField;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    lblverConvenio: TLabel;
    Label8: TLabel;
    lblConcesionaria: TLabel;
    Panel1: TPanel;
    btnTransitosFiltrar: TButton;
    lbl_TransitosDesde: TLabel;
    de_TransitosDesde: TDateEdit;
    lbl_TransitosHasta: TLabel;
    de_TransitosHasta: TDateEdit;
    lbl_Patente: TLabel;
    txt_Patente: TEdit;
    lbl_TransitosReestablecer: TLabel;
    lblImpresoraFiscal: TLabel;
    cbImpresorasFiscales: TVariantComboBox;
    spObtenerNumeroTipoComprobanteInterno: TADOStoredProc;
    spObtenerNumeroTipoInternoComprobanteFiscal: TADOStoredProc;
    StringField1: TStringField;
    LargeintField1: TLargeintField;
    StringField2: TStringField;
    WordField1: TWordField;
    BTNBuscarComprobante: TButton;
    spCobrar_o_DescontarReimpresion: TADOStoredProc;
    cbConcesionaria: TVariantComboBox;
    spObtenerConcesionarias: TADOStoredProc;
    tsEstacionamientos: TTabSheet;
    spObtenerEstacionamientos: TADOStoredProc;
    dsEstacionamientos: TDataSource;
    pnlEstacionamiento: TPanel;
    lblEstacConceaionaria: TLabel;
    vcbConcesionariasEstacionamiento: TVariantComboBox;
    btnEstacBuscar: TButton;
    dblEstacionamientos: TDBListEx;
    lbl_EstacionamientosDesde: TLabel;
    lbl_EstacionamientosHasta: TLabel;
    de_EstacionamientosDesde: TDateEdit;
    de_EstacionamientosHasta: TDateEdit;
    spObtenerFechasTransitosPorComprobante: TADOStoredProc;
    lblCSV: TLabel;
    sdGuardarCSV: TSaveDialog;
    spObtenerFechasEstacionamientosComprobante: TADOStoredProc;
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure cbTipoComprobanteChange(Sender: TObject);
    procedure cbConveniosChange(Sender: TObject);
    procedure dblComprobantesDrawText(Sender: TCustomDBListEx;
    Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
    var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
    var DefaultDraw: Boolean);
    procedure dsFacturasDataChange(Sender: TObject; Field: TField);
    procedure dblTransitosContextPopup(Sender: TObject; MousePos: TPoint;
    var Handled: Boolean);
    procedure dblTransitosLinkClick(Sender: TCustomDBListEx;
    Column: TDBListExColumn);
    procedure dblTransitosCheckLink(Sender: TCustomDBListEx;
    Column: TDBListExColumn; var IsLink: Boolean);
    procedure dblComprobantesContextPopup(Sender: TObject;
    MousePos: TPoint; var Handled: Boolean);
    procedure btnReimprimirClick(Sender: TObject);
    procedure spObtenerListaComprobantesAfterClose(DataSet: TDataSet);
    procedure spObtenerListaComprobantesAfterOpen(DataSet: TDataSet);
    procedure dblDetalleDrawText(Sender: TCustomDBListEx;
    Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
    var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
    var DefaultDraw: Boolean);
    procedure spObtenerListaComprobantesAfterScroll(DataSet: TDataSet);
    procedure dblTransitosColumns0HeaderClick(Sender: TObject);
    procedure dblComprobantesColumns0HeaderClick(Sender: TObject);
    procedure tmrConsultaRUTTimer(Sender: TObject);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure tmrConsultaComprobanteTimer(Sender: TObject);
    procedure btnImprimirFacturacionDetalladaClick(Sender: TObject);
    procedure lblverConvenioClick(Sender: TObject);
    procedure dblTransitosDrawText(Sender: TCustomDBListEx;
    Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
    var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
    var DefaultDraw: Boolean);
    procedure btnTransitosFiltrarClick(Sender: TObject);
    //procedure neCantidadTransitosExit(Sender: TObject);                       //SS 1006 / 1015_MCO_20120828
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbl_TransitosReestablecerClick(Sender: TObject);
    procedure BTNBuscarComprobanteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbConcesionariaChange(Sender: TObject);
    procedure OrdenarColumna(Sender: TObject);
    procedure btnEstacBuscarClick(Sender: TObject);
    procedure vcbConcesionariasEstacionamientoChange(Sender: TObject);
    procedure de_TransitosDesdeChange(Sender: TObject);
    procedure de_TransitosHastaChange(Sender: TObject);
    procedure de_EstacionamientosDesdeChange(Sender: TObject);
    procedure de_EstacionamientosHastaChange(Sender: TObject);
    procedure lblCSVClick(Sender: TObject);
    procedure cbConveniosDrawItem(Control: TWinControl; Index: Integer;                           //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);
    procedure dblEstacionamientosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);                                                       //SS_1120_MVI_20130820

    protected
    { Protected declarations }
  private
    { Private declarations }
    FUltimaBusqueda : TBusquedaCliente;
    FConvenio : integer;

    //Filtro de Transitos
    FTransitosDesde, FTransitosHasta : TDateTime;
    FEstacionamientosDesde, FEstacionamientosHasta : TDateTime;
    FBoletaAfectaElectronica, FBoletaExentaElectronica,
    FFacturaAfectaElectronica, FFacturaExentaElectronica,
    FTipoNotaDeCobro, FNotaCreditoElectronica, FNotaDebitoElectronica,
    FUltimoNumeroDocumento: String;
    procedure CargarComprobantes;
    function  ImprimirComprobante(TipoComprobante: String; NroComprobante: Int64): Boolean;
    function  ImprimirComprobanteElectronico(TipoComprobante: String; NroComprobante: Int64): Boolean;
    procedure MostrarDetalleNotaCobro;
    procedure MostrarDetalleOtrosComprobantes;
    procedure MostrarDomicilioFacturacion(CodigoConvenio : integer);
    Function VerificarReimpresion(TipoComprobante: string; NumeroComprobante: int64):Boolean;
    //Transitos
    procedure EstablecerFechasTransitos(var Inicial: TDateTime; var Final: TDateTime);
    function  ValidarFiltroTransitos : Boolean;
    function  ValidarFiltroEstacionamientos : Boolean; //SS 1006 / 1015_MCO_20120828
    procedure FiltrarTransitos;
    function ImprimirCreditoCK(TipoComprobante: string; NroComprobante: int64): Boolean;
    function ImprimirCreditoNC(TipoComprobante: string; NroComprobante: int64): Boolean;
    procedure MostrarControlesSegunComprobante(TipoComprobante: String);
    procedure MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
    function ImprimirNotaCreditoElectronica(TipoComprobante:string; NroComprobante: int64): Boolean;
    function ImprimirNotaDebitoElectronica(TipoComprobante:string; NumeroComprobante: int64): Boolean;
    Procedure LiberarMemoria;   // SS_1017_PDO_20120514
  public
  { Public declarations }
    function Inicializar(CodigoConvenio: Integer; TipoComprobante: AnsiString = '';
        NumeroComprobante: Int64 = 0; NumeroComprobanteFiscal: Int64 = 0;
        CodigoImpresoraFiscal: Integer = 0): Boolean;
    // Rev.16 / 07-Mayo-2010 / Nelson Droguett Sierra
    procedure CargarConcesionarias(Conn: TADOConnection; Combo: TVariantComboBox; CodigoTipoConcesionaria:Integer ; OpcionTodas: boolean = True); // SS_1006_HUR_20120213
    function ImprimirDevolucionDinero( TipoComprobante : string; NumeroComprobante : Int64) : Boolean;
  end;


implementation

{$R *.dfm}
uses
    FrmInicioConsultaConvenio;

{-----------------------------------------------------------------------------
 Function/Procedure Name:  Inicializar
 Date: 19/04/2010
 Author: jjofre
 Description:  permite preparar e inicilizar todas las variables, funciones
                y stored involucrados para imprimir un comprobante
-----------------------------------------------------------------------------}
function TFormComprobantesEmitidos.Inicializar(CodigoConvenio: Integer;
    TipoComprobante: AnsiString = ''; NumeroComprobante: Int64 = 0;
    NumeroComprobanteFiscal: Int64 = 0; CodigoImpresoraFiscal: Integer = 0): Boolean;
resourcestring
	MSG_ALL_INVOICE_TYPES	= 'Todos';
    MSG_ERROR_INICIALIZAR	= 'Error al inicializar el formulario.';
    SQL_BOLETA_EXENTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA_EXENTA()';
    SQL_BOLETA_AFECTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA_AFECTA()';
    SQL_FACTURA_EXENTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA_EXENTA()';
    SQL_FACTURA_AFECTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA_AFECTA()';
    SQL_NOTA_COBRO			= 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_COBRO()';
    SQL_NOTA_CREDITO_ELECTRONICA = 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_CREDITO_ELECTRONICA()';
    SQL_NOTA_DEBITO_ELECTRONICA  = 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_DEBITO_ELECTRONICA()';
    TIPO_NOTA_DEBITO		= 'Nota D�bito Electr�nica';
var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

    //cargar los tipos de documentos electr�nicos
    FFacturaAfectaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_FACTURA_AFECTA);
    FFacturaExentaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_FACTURA_EXENTA);
    FBoletaAfectaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_BOLETA_AFECTA);
    FBoletaExentaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_BOLETA_EXENTA);
    FTipoNotaDeCobro			:= QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_COBRO);
    FNotaCreditoElectronica     := QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_CREDITO_ELECTRONICA);
    FNotaDebitoElectronica		:= QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_DEBITO_ELECTRONICA);
    
    // Armamos la fecha de los transitos inicial y final, se usa para el caso que la NK no contenga transacciones (Transitos y Estacionamientos)
    //EstablecerFechasTransitos(FTransitosDesde, FTransitosHasta);					//SS_1361_MCA_20151006
    //EstablecerFechasTransitos(FEstacionamientosDesde, FEstacionamientosHasta);  	//SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828
    //de_TransitosDesde.Date            := FTransitosDesde;							//SS_1361_MCA_20151006
    //de_TransitosHasta.Date            := FTransitosHasta;							//SS_1361_MCA_20151006
    //de_EstacionamientosDesde.Date     :=   FEstacionamientosDesde;               	//SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828
    //de_EstacionamientosHasta.Date     :=   FEstacionamientosHasta;                //SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828
    //neCantidadTransitos.ValueInt    := 100;                                   	//SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828

  	lblConvenio.Caption             := EmptyStr;
    lblConcesionaria.Caption        := EmptyStr;
    lblRUT.Caption                  := EmptyStr;
    lblNombre.Caption               := EmptyStr;
    lblDomicilio.Caption            := EmptyStr;

    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_COBRO), TC_NOTA_COBRO);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_FACTURA), TC_FACTURA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_CREDITO), TC_NOTA_CREDITO);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_CREDITO_A_COBRO), TC_NOTA_CREDITO_A_COBRO);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_DEBITO), TC_NOTA_DEBITO);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_BOLETA), TC_BOLETA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_COMPROBANTE_CUOTA), TC_COMPROBANTE_CUOTA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_DEVOLUCION_DINERO), TC_DEVOLUCION_DINERO);		//SS_959_MBE_20110617

    // agrego los comprobantes electr�nicos
    cbTipoComprobante.Items.Add(DescriTipoComprobante(FFacturaAfectaElectronica), FFacturaAfectaElectronica);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(FFacturaExentaElectronica), FFacturaExentaElectronica);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(FBoletaAfectaElectronica), FBoletaAfectaElectronica);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(FBoletaExentaElectronica), FBoletaExentaElectronica);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(FNotaCreditoElectronica), FNotaCreditoElectronica);
    cbTipoComprobante.Items.Add(TIPO_NOTA_DEBITO, FNotaDebitoElectronica);
    //cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_SALDO_INICIA_OC), TC_SALDO_INICIA_OC);                     // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
    //cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_ANULA_SALDO_INICIAL_OC), TC_ANULA_SALDO_INICIAL_OC);       // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_SALDO_INICIAL_DEUDOR), TC_SALDO_INICIAL_DEUDOR);                       // SS_1015_HUR_20120417
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_ANULA_SALDO_INICIAL_DEUDOR), TC_ANULA_SALDO_INICIAL_DEUDOR);           // SS_1015_HUR_20120417
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_SALDO_INICIAL_ACREEDOR), TC_SALDO_INICIAL_ACREEDOR);                   // SS_1015_HUR_20120417
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_ANULA_SALDO_INICIAL_ACREEDOR), TC_ANULA_SALDO_INICIAL_ACREEDOR);       // SS_1015_HUR_20120417
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_CUOTAS_OTRAS_CONCESIONARIAS), TC_CUOTAS_OTRAS_CONCESIONARIAS);                   // SS-1015-NDR-20120510
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_ANULA_CUOTAS_OTRAS_CONCESIONARIAS), TC_ANULA_CUOTAS_OTRAS_CONCESIONARIAS);       // SS-1015-NDR-20120510


    cbTipoComprobante.Value := TC_NOTA_COBRO;

    // Se cargan las impresoras y se ejecuta el OnChange de los Tipos de Comprobantes para que muestre los componentes que son necesarios
    CargarImpresorasFiscales(DMConnections.BaseCAC, cbImpresorasFiscales, iif(CodigoImpresoraFiscal = 0, 0, CodigoImpresoraFiscal));
    MostrarControlesSegunComprobante(TipoComprobante);

	pgDetalles.ActivePageIndex := 0;

    if CodigoConvenio > 0 then begin
        ActiveControl := peNumeroDocumento;
        try
            peNumeroDocumento.Text := QueryGetValue(DMConnections.BaseCAC, Format(
                'SELECT Personas.NumeroDocumento FROM Personas WITH (NOLOCK) , Convenio  WITH (NOLOCK) ' +
                'WHERE Personas.CodigoPersona = Convenio.CodigoCliente ' +
                'AND Convenio.CodigoConvenio = %d', [CodigoConvenio]));
            cbConvenios.Value := CodigoConvenio;
        except
            on E: Exception do begin
                Result := False;
                MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, Self.Caption, MB_ICONERROR);
                Exit;
            end;
        end;

    end else begin
        ActiveControl := neNumeroComprobante;
        cbTipoComprobante.Value := iif( TipoComprobante <> EmptyStr, TipoComprobante, TC_NOTA_COBRO);
        if (TipoComprobante = TC_NOTA_CREDITO) or (TipoComprobante = TC_BOLETA) or (TipoComprobante = TC_FACTURA) then begin
            neNumeroComprobante.ValueInt := NumeroComprobanteFiscal;
        end else begin
            neNumeroComprobante.ValueInt := NumeroComprobante;
        end;


    end;
    // Rev.16 / 07-Mayo-2010 / Nelson Droguett Sierra

    //tsEstacionamientos.TabVisible := False;                                               // SS_1006_HUR_20120213
    CargarConcesionarias(DMConnections.BaseCAC,cbConcesionaria,1,True);                     // SS_1006_HUR_20120213
    CargarConcesionarias(DMConnections.BaseCAC,vcbConcesionariasEstacionamiento,2,True);    // SS_1006_HUR_20120213
    
    Result := True;
end;

procedure TFormComprobantesEmitidos.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
            tmrConsultaRUTTimer(nil);
        end;
	end;
    F.free;
end;

procedure TFormComprobantesEmitidos.cbTipoComprobanteChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    MostrarControlesSegunComprobante(cbTipoComprobante.Value);
	peNumeroDocumento.Value := 0;
    cbConvenios.Clear;
end;

procedure TFormComprobantesEmitidos.cbConveniosChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    Cargarcomprobantes;
end;
//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TFormComprobantesEmitidos.cbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.cbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------
{-----------------------------------------------------------------------------
  Revision: 1
  Author: nefernandez
  Date: 11/02/2008
  Description: SS 631: Cuando se recuperan los datos del cliente, se verifican
  si existen observaciones a mostrar para las cuentas de dicho RUT
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.CargarComprobantes;
resourcestring
    MSG_ERROR_SP = 'Se produjo un error al ejecutar el procedimiento almacenado ObtenerDatosComprobanteParaTimbreElectronico';
    MSG_NO_EXISTEN_COMPROBANTE='El Comprobante %d no existe';						//SS_1361_MCA_20151006
Var
    Persona: Integer;
    TipoComprobante: string;
    NumeroComprobante: int64;
begin

    // guardo los valores de los controles en variables
    NumeroComprobante   := neNumeroComprobante.ValueInt;
    TipoComprobante     := cbTipoComprobante.Value;

    // si es un comprobante fiscal (electr�nico) obtengo su numeracion interna
    if	(cbTipoComprobante.Value = TC_FACTURA_AFECTA) or (cbTipoComprobante.Value = TC_FACTURA_EXENTA) or
    	(cbTipoComprobante.Value = TC_BOLETA_AFECTA) or (cbTipoComprobante.Value = TC_BOLETA_EXENTA) or
        (cbTipoComprobante.Value = TC_NOTA_CREDITO_ELECTRONICA) or
        (cbTipoComprobante.Value = FNotaDebitoElectronica) then begin
        try
	        spObtenerNumeroTipoInternoComprobanteFiscal.Parameters.Refresh;
	        spObtenerNumeroTipoInternoComprobanteFiscal.Parameters.ParamByName('@TipoComprobanteFiscal').Value  := cbTipoComprobante.Value;
	        spObtenerNumeroTipoInternoComprobanteFiscal.Parameters.ParamByName('@NumeroComprobanteFiscal').Value:= neNumeroComprobante.ValueInt;
	        spObtenerNumeroTipoInternoComprobanteFiscal.ExecProc;
            NumeroComprobante   := spObtenerNumeroTipoInternoComprobanteFiscal.Parameters.ParamByName('@NumeroComprobante').Value;
            TipoComprobante     := spObtenerNumeroTipoInternoComprobanteFiscal.Parameters.ParamByName('@TipoComprobante').Value;
        except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_SP, E.Message, Self.Caption, MB_ICONSTOP);
            end
        end;
    end;


    spObtenerListaComprobantes.Close;
    if cbConvenios.ItemIndex >= 0 then begin
        spObtenerListaComprobantes.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
        spObtenerListaComprobantes.Parameters.ParamByName('@TipoComprobante').Value := NULL;
        spObtenerListaComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := NULL;
        spObtenerListaComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := NULL;
        spObtenerListaComprobantes.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := NULL;
        FConvenio := cbConvenios.Value;
        Persona := QueryGetValueInt(DMConnections.BaseCAC, Format(
          'SELECT CodigoCliente FROM Convenio  WITH (NOLOCK) WHERE CodigoConvenio = %d', [FConvenio]));
        spObtenerListaComprobantes.Open;
        lblRUT.Caption :=  QueryGetValue(DMConnections.BaseCAC, Format(
                              'SELECT dbo.ObtenerRUTPersona(%d)', [Persona]));
        lblConvenio.Caption :=  QueryGetValue(DMConnections.BaseCAC, Format(
                              'SELECT dbo.FormatearNumeroConvenio(dbo.ObtenerNumeroConvenio(%d))', [FConvenio]));
		lblConcesionaria.Caption := QueryGetValue(DMConnections.BaseCAC,
                                    Format('SELECT dbo.ObtenerConcesionariaConvenio(%d)',[FConvenio]));
        lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
                              'SELECT dbo.ObtenerNombrePersona(%d)', [Persona]));
    end else begin

        spObtenerListaComprobantes.Parameters.ParamByName('@CodigoConvenio').Value := NULL;
        spObtenerListaComprobantes.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;//cbTipoComprobante.Value; REV 4

        if	(TipoComprobante = TC_BOLETA) or (TipoComprobante = TC_NOTA_CREDITO) or
        	(TipoComprobante = TC_DEVOLUCION_DINERO) or										//SS_959_MBE_20110617
        	(TipoComprobante = TC_NOTA_CREDITO_A_COBRO) or (TipoComprobante = TC_NOTA_COBRO) or                                 // SS_1015_HUR_20120413
          //(TipoComprobante = TC_SALDO_INICIA_OC) or (TipoComprobante = TC_ANULA_SALDO_INICIAL_OC) then begin                  // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
          (TipoComprobante = TC_SALDO_INICIAL_DEUDOR) or (TipoComprobante = TC_ANULA_SALDO_INICIAL_DEUDOR) or                   // SS_1015_HUR_20120417
          (TipoComprobante = TC_SALDO_INICIAL_ACREEDOR) or (TipoComprobante = TC_ANULA_SALDO_INICIAL_ACREEDOR) or               // SS_1015_HUR_20120417 SS-1015-NDR-20120510
          (TipoComprobante = TC_CUOTAS_OTRAS_CONCESIONARIAS) or (TipoComprobante = TC_ANULA_CUOTAS_OTRAS_CONCESIONARIAS) then begin //SS-1015-NDR-20120510
            spObtenerListaComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            spObtenerListaComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := NULL;//neNumeroComprobante.ValueInt; REV 4
        end else begin
            spObtenerListaComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := NULL;//neNumeroComprobante.ValueInt; REV 4
            spObtenerListaComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := NumeroComprobante;
        end;

        spObtenerListaComprobantes.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := iif(cbTipoComprobante.Value = TC_BOLETA, cbImpresorasFiscales.Value, NULL);
        spObtenerListaComprobantes.Open;
        Persona := spObtenerListaComprobantes.FieldByName('CodigoPersona').AsInteger;
        lblConvenio.Caption := spObtenerListaComprobantes.FieldByName('NumeroConvenio').AsString;
        lblRUT.Caption := spObtenerListaComprobantes.FieldByName('NumeroDocumento').AsString;
        FConvenio := spObtenerListaComprobantes.FieldByName('CodigoConvenio').AsInteger;
		lblConcesionaria.Caption := QueryGetValue(DMConnections.BaseCAC,
                                    Format('SELECT dbo.ObtenerConcesionariaConvenio(%d)',[FConvenio]));
        lblverConvenio.Enabled := not spObtenerListaComprobantes.IsEmpty;
    end;
    if spObtenerListaComprobantes.IsEmpty then																		//SS_1361_MCA_20151006
               MsgBox(format(MSG_NO_EXISTEN_COMPROBANTE, [NumeroComprobante]), Self.Caption, MB_ICONINFORMATION);	//SS_1361_MCA_20151006

    if Persona <> 0 then                                                        //SS_1408_MCA_20151027
        EstaClienteConMensajeEspecial(DMConnections.BaseCAC, Trim(lblRUT.Caption));                           //SS_1408_MCA_20151027


    lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNombrePersona(%d)', [Persona]));
    MostrarDomicilioFacturacion(FConvenio);
    if FUltimoNumeroDocumento <> Trim(lblRUT.Caption) then begin
        FUltimoNumeroDocumento := Trim(lblRUT.Caption);
        MostrarMensajeRUTConvenioCuentas(FUltimoNumeroDocumento);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarMensajeRUTConvenioCuentas
  Author: nefernandez
  Date Created: 11/02/2008
  Description:  SS 631: Se muestra un aviso con las observaciones de las cuentas
  del Cliente (RUT) en cuesti�n.
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeRUTConvenioCuentas(DMCOnnections.BaseCAC, NumeroDocumento);
        if Texto <> '' then MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

procedure TFormComprobantesEmitidos.MostrarDomicilioFacturacion(CodigoConvenio : integer);
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
	with spObtenerDomicilioFacturacion, Parameters do begin
    	// Mostramos el domicilio de Facturaci�n
        Close;
    	ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        try
        	ExecProc;
    		lblDomicilio.Caption := ParamByName('@Domicilio').Value + ' - ' + ParamByName('@Comuna').Value;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;


procedure TFormComprobantesEmitidos.dblComprobantesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
resourcestring
    MSG_NO = 'No';
    MSG_YES = 'S�';
    MSG_JUDICIAL = 'Judicial';
begin
    if Column = dblComprobantes.Columns[8] then begin
        if spObtenerListaComprobantes['EstadoPago'] = 'I' then Text := MSG_NO
        else if spObtenerListaComprobantes['EstadoPago'] = 'P' then Text := MSG_YES
        else if spObtenerListaComprobantes['EstadoPago'] = 'J' then Text := MSG_JUDICIAL;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dsFacturasDataChange
  Author:    (probably) ndonadio
  Date Created: UNKNOWN
  Description:
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.dsFacturasDataChange(Sender: TObject; Field: TField);
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
    ERROR_GETTING_INVOICE_TRANSITS = 'Ocurri� un error obteniendo la lista de tr�nsitos del comprobante.';
    ERROR_GETTING_INVOICE_MESSAGES = 'Ocurri� un error obteniendo la lista de mensajes del comprobante.';
var NumComprobante : Integer;
var TipoComprobante : string;
begin
	Screen.Cursor := crHourglass;

  NumComprobante := iif(spObtenerListaComprobantes.fieldByName('NumeroComprobante').AsInteger>0, spObtenerListaComprobantes.fieldByName('NumeroComprobante').AsInteger, 0); //SS 1006 / 1015_MCO_20120828
  TipoComprobante := spObtenerListaComprobantes.fieldByName('TipoComprobante').AsString;//SS 1006 / 1015_MCO_20120828

    if ( TipoComprobante = TC_NOTA_COBRO ) then                                         //SS 1006 / 1015_MCO_20120828
        MostrarDetalleNotaCobro
    else MostrarDetalleOtrosComprobantes;

    try

      if (NumComprobante > 0) and (TipoComprobante = TC_NOTA_COBRO) then begin                        //SS 1006 / 1015_MCO_20120828
        with spObtenerFechasTransitosPorComprobante do begin                                          //SS 1006 / 1015_MCO_20120828
            Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;                      //SS 1006 / 1015_MCO_20120828
            Parameters.ParamByName('@NumeroComprobante').Value := NumComprobante;                     //SS 1006 / 1015_MCO_20120828
            Open;                                                                                     //SS 1006 / 1015_MCO_20120828

            FTransitosDesde         := spObtenerFechasTransitosPorComprobante.fieldByName('PeriodoInicial').AsDateTime;   //SS 1006 / 1015_MCO_20120828
            FTransitosHasta         := spObtenerFechasTransitosPorComprobante.fieldByName('PeriodoFinal').AsDateTime;     //SS 1006 / 1015_MCO_20120828
            Close;                                                                                    //SS 1006 / 1015_MCO_20120828
        end;

        with spObtenerFechasEstacionamientosComprobante do begin                                      //SS 1006 / 1015_MCO_20120828
            Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;                      //SS 1006 / 1015_MCO_20120828
            Parameters.ParamByName('@NumeroComprobante').Value := NumComprobante;                     //SS 1006 / 1015_MCO_20120828
            Open;
                                                                                                 //SS 1006 / 1015_MCO_20120828
            FEstacionamientosDesde         := spObtenerFechasEstacionamientosComprobante.fieldByName('PeriodoInicial').AsDateTime;   //SS 1006 / 1015_MCO_20120828
            FEstacionamientosHasta         := spObtenerFechasEstacionamientosComprobante.fieldByName('PeriodoFinal').AsDateTime;     //SS 1006 / 1015_MCO_20120828
            Close;                                                                                    //SS 1006 / 1015_MCO_20120828
        end;
      end;

      if (FTransitosDesde = 0) and (FTransitosHasta = 0) then EstablecerFechasTransitos(FTransitosDesde, FTransitosHasta);    //SS 1006 / 1015_MCO_20120828
      if (FEstacionamientosDesde = 0) and (FEstacionamientosHasta = 0) then EstablecerFechasTransitos(FEstacionamientosDesde, FEstacionamientosHasta);  //SS 1006 / 1015_MCO_20120828

      //de_TransitosDesde.Date       := FTransitosDesde;                //SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828
      //de_TransitosHasta.Date       := FTransitosHasta;                //SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828
      //de_EstacionamientosDesde.Date    :=   FEstacionamientosDesde;   //SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828
      //de_EstacionamientosHasta.Date    :=   FEstacionamientosHasta;   //SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828

      //neNumeroComprobante.ValueInt := NumComprobante;               	//SS_1361_MCA_20151006 //SS 1006 / 1015_MCO_20120828

      FiltrarTransitos;
      btnEstacBuscar.Click;                                         //SS 1006 / 1015_MCO_20120828

    except
        on e: exception do begin
            msgBoxErr(ERROR_GETTING_INVOICE_TRANSITS, e.Message, caption, MB_ICONSTOP);
        end;
    end;

    try
        spMensajes.Close;
        spMensajes.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
        spMensajes.Parameters.ParamByName('@NumeroComprobante').Value := NumComprobante;
        spMensajes.Open;
    except
        on e: exception do begin
            msgBoxErr(ERROR_GETTING_INVOICE_MESSAGES, e.Message, caption, MB_ICONSTOP);
        end;
    end;

	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDetalleNotaCobro
  Author:    flamas
  Date Created: 02/02/2005
  Description: Muestra el Detalle de una Nota de Cobro
 -----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.MostrarDetalleNotaCobro;
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
    ERROR_GETTING_INVOICE_TRANSITS = 'Ocurri� un error obteniendo la lista de tr�nsitos del comprobante.';
begin
    try
        Detalle.Close;
        spListadoCargos.Close;
        spListadoCargos.Parameters.Refresh;                                                                                                                                                         //SS_1012_ALA_20111121
        spListadoCargos.Parameters.ParamByName('@TipoComprobante').Value := spObtenerListaComprobantes.fieldByName('TipoComprobante').AsString;
        spListadoCargos.Parameters.ParamByName('@NumeroComprobante').Value := spObtenerListaComprobantes.fieldByName('NumeroComprobante').asInteger;
        spListadoCargos.Open;
        Detalle.CreateDataSet;
        while not spListadoCargos.Eof do begin
            Detalle.Append;
            Detalle.FieldByName('Descripcion').Assign(spListadoCargos.FieldByName('Descripcion'));
            Detalle.FieldByName('Importe').Assign(spListadoCargos.FieldByName('DescriImporte')); //iif(spListadoCargos.FieldByName('Importe').AsFloat > 0, FormatFloat(FORMATO_IMPORTE, spListadoCargos.FieldByName('Importe').AsFloat), '');   //SS_1012_ALA_20111121 // SS_1006_HUR_20120215
            //rev 4 saca
            //Detalle.FieldByName('CodigoConcepto').AsString := spListadoCargos.FieldByName('CodigoConcepto').AsString;
            Detalle.Post;
            spListadoCargos.Next;
        end;
        spListadoCargos.Close;

        if not spObtenerListaComprobantes.IsEmpty then begin
        	Detalle.Append;
        	Detalle.FieldByName('Descripcion').AsString := 'TOTAL A PAGAR';
        	Detalle.FieldByName('Importe').AsString := spObtenerListaComprobantes.FieldByName('DescriTotal').AsString;                                                                               //SS_1012_ALA_20111121
        	Detalle.Post;
        end;
        Detalle.First;
    except
        on e: exception do begin
            msgBoxErr(ERROR_GETTING_INVOICE_DETAIL, e.Message, caption, MB_ICONSTOP);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDetalleOtrosComprobantes
  Author:    flamas
  Date Created: 02/02/2005
  Description: Muestra el Detalle de los Comprobantes distintos de Nota de Cobro
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.MostrarDetalleOtrosComprobantes;
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
begin
    try
        Detalle.Close;
        spObtenerDetalleConceptosComprobante.Close;
        spObtenerDetalleConceptosComprobante.Parameters.ParamByName('@TipoComprobante').Value := spObtenerListaComprobantes.fieldByName('TipoComprobante').AsString;
        spObtenerDetalleConceptosComprobante.Parameters.ParamByName('@NumeroComprobante').Value := spObtenerListaComprobantes.fieldByName('NumeroComprobante').asInteger;
        spObtenerDetalleConceptosComprobante.Open;
        Detalle.CreateDataSet;
        while not spObtenerDetalleConceptosComprobante.Eof do begin
            Detalle.Append;
            Detalle.FieldByName('Descripcion').Assign(spObtenerDetalleConceptosComprobante.FieldByName('Descripcion'));
            Detalle.FieldByName('Importe').AsString := spObtenerDetalleConceptosComprobante.FieldByName('DescImporte').AsString;
            Detalle.Post;
            spObtenerDetalleConceptosComprobante.Next;
        end;
        spObtenerDetalleConceptosComprobante.Close;
        if not spObtenerListaComprobantes.IsEmpty then begin
        	Detalle.Append;
            if (spObtenerListaComprobantes.FieldByName('TipoComprobante').AsString = TC_NOTA_CREDITO_A_COBRO)  then begin // Rev. 3 SS 877
                Detalle.FieldByName('Descripcion').AsString := 'TOTAL COMPROBANTE'; // Rev. 3 SS 877
            	Detalle.FieldByName('Importe').AsString := spObtenerListaComprobantes.FieldByName('DescriTotal').AsString;// Rev. 3 SS 877
            end
            else begin
            	Detalle.FieldByName('Descripcion').AsString := 'TOTAL A PAGAR'; // Rev. 3 SS 877
            	Detalle.FieldByName('Importe').AsString := spObtenerListaComprobantes.FieldByName('DescriAPagar').AsString;// Rev. 3 SS 877
            end;    // Rev. 3 SS 877
        	Detalle.Post;
        end;
        Detalle.First;
    except
        on e: exception do begin
            msgBoxErr(ERROR_GETTING_INVOICE_DETAIL, e.Message, caption, MB_ICONSTOP);
        end;
    end;
end;


procedure TFormComprobantesEmitidos.dblTransitosContextPopup(
  Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
//Rev.2 / 23-Abril-2010 / Nelson Droguett Sierra.
{    if not ObtenerTransitosPorComprobante.IsEmpty then
        Handled := MostrarMenuContextualTransito(dblTransitos.ClientToScreen(MousePos),
          ObtenerTransitosPorComprobante.FieldByName('NumCorrCA').asInteger);
}
end;

procedure TFormComprobantesEmitidos.dblTransitosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    //if Column = dblTransitos.Columns[7] then                                  // SS_1091_CQU_20130516
    if Column = dblTransitos.Columns[9] then                                    // SS_1091_CQU_20130516
        MostrarVentanaImagen(Self,
          ObtenerTransitosPorComprobante.FieldByName('NumCorrCA').asInteger,
          ObtenerTransitosPorComprobante.FieldByName('FechaHora').asDateTime,   // SS_1091_CQU_20130516
          ObtenerTransitosPorComprobante.FieldByName('EsItaliano').AsBoolean);  // SS_1091_CQU_20130516
end;


//SS 1006 / 1015_MCO_20120828 valida que la fecha desde del filtro de estacionamiento no sea inferior a la fecha de facturacion ni mayor a la fecha hasta
procedure TFormComprobantesEmitidos.de_EstacionamientosDesdeChange(
  Sender: TObject);
resourcestring
    MSG_INVALID_DATE            = 'Fecha Inv�lida';
    MSG_ERROR_DATE	            = 'La Fecha Ingresada Est� Fuera de Rango de Facturaci�n';
var FEstacionamientoDesde : TDateTime;
begin
   // FEstacionamientoDesde := iif(de_EstacionamientosDesde.Date <= 0, 0, de_EstacionamientosDesde.Date);				//SS_1361_MCA_20151006

    //if (FEstacionamientoDesde > Trunc(FEstacionamientosHasta)) or (FEstacionamientoDesde < Trunc(FEstacionamientosDesde)) then begin	//SS_1361_MCA_20151006
   //     MsgBoxBalloon(MSG_ERROR_DATE, MSG_INVALID_DATE, MB_ICONSTOP, de_EstacionamientosDesde);										//SS_1361_MCA_20151006
 // //      de_EstacionamientosDesde.Date := FEstacionamientosDesde;																	//SS_1361_MCA_20151006
 //   end;																																//SS_1361_MCA_20151006
end;

//SS 1006 / 1015_MCO_20120828 valida que la fecha Hasta del filtro de estacionamiento no sea Superior a la fecha de facturacion ni inferior a la fecha desde
procedure TFormComprobantesEmitidos.de_EstacionamientosHastaChange(
  Sender: TObject);
resourcestring
    MSG_INVALID_DATE            = 'Fecha Inv�lida';
    MSG_ERROR_DATE	            = 'La Fecha Ingresada Est� Fuera de Rango de Facturaci�n';
var FEstacionamientoHasta : TDateTime;
begin
   // FEstacionamientoHasta := iif(de_EstacionamientosHasta.Date <= 0, 0, de_EstacionamientosHasta.Date);									//SS_1361_MCA_20151006

   // if (FEstacionamientoHasta > Trunc(FEstacionamientosHasta)) or (FEstacionamientoHasta < TRUNC(FEstacionamientosDesde)) then begin		//SS_1361_MCA_20151006
  //      MsgBoxBalloon(MSG_ERROR_DATE, MSG_INVALID_DATE,MB_ICONSTOP,  de_EstacionamientosHasta);											//SS_1361_MCA_20151006
  //      de_EstacionamientosHasta.Date := FEstacionamientosHasta;																			//SS_1361_MCA_20151006
  //  end;																																	//SS_1361_MCA_20151006

end;

//SS 1006 / 1015_MCO_20120828 valida que la fecha desde del filtro de Transitos no sea inferior a la fecha de facturacion ni mayor a la fecha hasta
procedure TFormComprobantesEmitidos.de_TransitosDesdeChange(Sender: TObject);
resourcestring
    MSG_INVALID_DATE            = 'Fecha Inv�lida';
    MSG_ERROR_DATE	            = 'La Fecha Ingresada Est� Fuera de Rango de Facturaci�n';
var FTransitoDesde : TDateTime;
begin
   // FTransitoDesde := iif(de_TransitosDesde.Date <= 0, 0, de_TransitosDesde.Date);						//SS_1361_MCA_20151006

//    if (FTransitoDesde > Trunc(FTransitosHasta)) or (FTransitoDesde < Trunc(FTransitosDesde))then begin	//SS_1361_MCA_20151006
//        MsgBoxBalloon(MSG_ERROR_DATE, MSG_INVALID_DATE,MB_ICONSTOP, de_TransitosDesde);					//SS_1361_MCA_20151006
//        de_TransitosDesde.Date := FTransitosDesde;														//SS_1361_MCA_20151006
//    end;																									//SS_1361_MCA_20151006
end;

//SS 1006 / 1015_MCO_20120828 valida que la fecha desde del filtro de Transitos no sea SUPERIOR a la fecha de facturacion ni inferior a la fecha desde
procedure TFormComprobantesEmitidos.de_TransitosHastaChange(Sender: TObject);
resourcestring
    MSG_INVALID_DATE            = 'Fecha Inv�lida';
    MSG_ERROR_DATE	            = 'La Fecha Ingresada Est� Fuera de Rango de Facturaci�n';
var FTransitoHasta : TDateTime;
begin
    //FTransitoHasta := iif(de_TransitosHasta.Date <= 0, 0, de_TransitosHasta.Date);						//SS_1361_MCA_20151006

//    if (FTransitoHasta > Trunc(FTransitosHasta)) or (FTransitoHasta < Trunc(FTransitosDesde)) then begin	//SS_1361_MCA_20151006
//        MsgBoxBalloon(MSG_ERROR_DATE, MSG_INVALID_DATE, MB_ICONSTOP, de_TransitosHasta);					//SS_1361_MCA_20151006
//        de_TransitosHasta.Date := FTransitosHasta;														//SS_1361_MCA_20151006
//    end;																									//SS_1361_MCA_20151006
end;

procedure TFormComprobantesEmitidos.dblTransitosCheckLink(
  Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    if Column.FieldName = 'Imagen' then
        IsLInk := ObtenerTransitosPorComprobante.Active and
          (ObtenerTransitosPorComprobante.FieldByName('RegistrationAccessibility').AsInteger > 0)
    else
        IsLInk := false;
end;

procedure TFormComprobantesEmitidos.dblComprobantesContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin

    if not spObtenerListaComprobantes.IsEmpty then
        Handled := MostrarMenuContextualComprobante(dblComprobantes.ClientToScreen(MousePos),
        						spObtenerListaComprobantes.FieldByName('TipoComprobante').AsString,
          						spObtenerListaComprobantes.FieldByName('NumeroComprobante').AsInteger);
end;

{-----------------------------------------------------------------------------
  Function Name: btnReimprimirClick
  Author:    ndonadio
  Date Created: 06/01/2005
  Description: Lanza el proceso de Re Impresion del Comprobante
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.btnReimprimirClick(Sender: TObject);
var
  TipoComprobante, TipoFiscal : string ;
  NumeroComprobante : int64;
begin
    try
        Screen.Cursor := crHourglass;
        With spObtenerListaComprobantes do begin
           	TipoFiscal := FieldByName('TipoComprobanteFiscal').AsString;
            TipoComprobante := FieldByName('TipoComprobante').AsString;
            NumeroComprobante := FieldByName('NumeroComprobante').AsInteger;

            LiberarMemoria;     // SS_1017_PDO_20120514

            if	(TipoFiscal = FFacturaAfectaElectronica) or (TipoFiscal = FFacturaExentaElectronica) or
                (TipoFiscal = FBoletaAfectaElectronica) or (TipoFiscal = FBoletaExentaElectronica)then begin
                ImprimirComprobanteElectronico(TipoComprobante, NumeroComprobante);
            end
            else if (TipoComprobante = TC_NOTA_COBRO) and (TipoFiscal = FTipoNotaDeCobro) then ImprimirComprobante(TipoComprobante,NumeroComprobante)
            else if (TipoComprobante = TC_NOTA_CREDITO_A_COBRO) AND (TipoFiscal = TC_NOTA_CREDITO_A_COBRO) then ImprimirCreditoCK(TipoComprobante,NumeroComprobante)
            else if (TipoComprobante = TC_NOTA_CREDITO) AND  (TipoFiscal = TC_NOTA_CREDITO) then ImprimirCreditoNC(TipoComprobante,NumeroComprobante)
            else if (TipoComprobante = TC_DEVOLUCION_DINERO) then ImprimirDevolucionDinero(TipoComprobante, NumeroComprobante)												//SS_959_MBE_20110617
            else if (TipoFiscal = TC_NOTA_CREDITO_ELECTRONICA) then ImprimirNotaCreditoElectronica(TipoComprobante,NumeroComprobante)
            else if (TipoFiscal = FNotaDebitoElectronica) then ImprimirNotaDebitoElectronica(TipoComprobante, NumeroComprobante);
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{--------------------------------------------------------------------
                         ImprimirDevolucionDinero

Author		: mbecerra
Date		: 21-Junio-2011
Description	:		(REf SS 959)
            	Permite la reimpresi�n del comprobante de devoluci�n de dinero

--------------------------------------------------------------------}
function TFormComprobantesEmitidos.ImprimirDevolucionDinero(TipoComprobante: string; NumeroComprobante: Int64): boolean;
begin
    Application.CreateForm(TReporteCompDevDineroForm, ReporteCompDevDineroForm);
    Result := ReporteCompDevDineroForm.Inicializar(NumeroComprobante);
    ReporteCompDevDineroForm.Release;
end;


Function TFormComprobantesEmitidos.ImprimirCreditoCK(TipoComprobante:string; NroComprobante: int64): Boolean;
var
	f	   : TfrmReporteCK;
	sError : string;
begin
	Result := False;

	//Application.CreateForm(TFrmReporteCK,f);      // SS_1017_PDO_20120514
    f := TFrmReporteCK.Create(nil);                 // SS_1017_PDO_20120514
 	try
		if not f.Inicializar(DMConnections.BaseCAC,TipoComprobante, NroComprobante, sError) then begin
			ShowMessage( sError );
			Exit;
		end;
		Result := f.Ejecutar;
	finally
		//f.Release;                            // SS_1017_PDO_20120514
        if Assigned(f) then FreeAndNil(f);      // SS_1017_PDO_20120514
	end;
end;

Function TFormComprobantesEmitidos.ImprimirCreditoNC(TipoComprobante:string; NroComprobante: int64): Boolean;
var
	f	   : TfrmReporteNC;
	sError : string;
begin
	Result := False;

    //Application.CreateForm(TFrmReporteNC,f);      // SS_1017_PDO_20120514
    f := TFrmReporteNC.Create(nil);                 // SS_1017_PDO_20120514

    try
        if not f.Inicializar(DMConnections.BaseCAC,TipoComprobante, NroComprobante, sError) then begin
            ShowMessage( sError );
            Exit;
        end;
        Result := f.Ejecutar;
    finally
		//f.Release;                            // SS_1017_PDO_20120514
        if Assigned(f) then FreeAndNil(f);      // SS_1017_PDO_20120514
    end;
end;


Function TFormComprobantesEmitidos.ImprimirNotaCreditoElectronica(TipoComprobante:string; NroComprobante: int64): Boolean;
var
	f	   : TReporteNotaCreditoElectronicaForm;
	sError : string;
begin
	Result := False;

	//Application.CreateForm(TReporteNotaCreditoElectronicaForm,f);     // SS_1017_PDO_20120514
    f := TReporteNotaCreditoElectronicaForm.Create(nil);                // SS_1017_PDO_20120514
	try
		if not f.Inicializar(DMConnections.BaseCAC,TipoComprobante, NroComprobante, sError) then begin
			ShowMessage( sError );
			Exit;
		end;
		Result := f.Ejecutar;
	finally
		//f.Release;                            // SS_1017_PDO_20120514
        if Assigned(f) then FreeAndNil(f);      // SS_1017_PDO_20120514
	end;
end;

{-----------------------------------------------------------------------------
Function/Procedure Name:ImprimirNotaDebitoElectronica
Author: mbecerra
Date: 14-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Imprime la Nota de D�bito Electr�nica
-----------------------------------------------------------------------------}
function TFormComprobantesEmitidos.ImprimirNotaDebitoElectronica;
var
	f	   : TReporteNotaDebitoElectronicaForm;
	sError : string;
begin
	Result := False;

	//Application.CreateForm(TReporteNotaDebitoElectronicaForm,f);      // SS_1017_PDO_20120514
    f := TReporteNotaDebitoElectronicaForm.Create(nil);                 // SS_1017_PDO_20120514

	try
		if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NumeroComprobante, True) then begin
			Result := f.Imprimir(True);
        end;
	finally
		//f.Release;                            // SS_1017_PDO_20120514
        if Assigned(f) then FreeAndNil(f);      // SS_1017_PDO_20120514
	end;
end;


{-----------------------------------------------------------------------------
Function/Procedure Name: ImprimirComprobante
Author:    ndonadio
Date Created: 03/01/2005
-----------------------------------------------------------------------------}
Function TFormComprobantesEmitidos.ImprimirComprobante(TipoComprobante:string; NroComprobante: int64): Boolean;
resourcestring
    MSG_ERROR_COBRAR	= 'No se actualiz� el campo "ReImpresi�n" para el comprobante';
var
    f	   : TFrmReporteFactura;
    sError : string;
    ImFdo  : boolean;
begin
    Result := False;
    if NOT VerificarReimpresion(TipoComprobante,NroComprobante) then Exit;

    //Application.CreateForm(TFrmReporteFactura,f);     // SS_1017_PDO_20120514
    f := TFrmReporteFactura.Create(nil);                // SS_1017_PDO_20120514

    try
        ImFdo := True;
        if not f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, sError, ImFdo) then begin
            ShowMessage( sError );
            Exit;
        end;
        Result := f.Ejecutar;
        if f.FImprimioComprobante then begin
            //spCobrar_o_DescontarReimpresion.Close;                                    // SS_1246_CQU_20151006
            //with spCobrar_o_DescontarReimpresion do begin                             // SS_1246_CQU_20151006
            //	Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;    // SS_1246_CQU_20151006
            //    Parameters.ParamByName('@NumeroComprobante').Value := NroComprobante; // SS_1246_CQU_20151006
            //    Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;     // SS_1246_CQU_20151006
            //    ExecProc;                                                             // SS_1246_CQU_20151006
            //    if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin       // SS_1246_CQU_20151006
            //    	MsgBox(MSG_ERROR_COBRAR, Caption, MB_ICONEXCLAMATION);              // SS_1246_CQU_20151006
            //    end;                                                                  // SS_1246_CQU_20151006

            //end;                                                                      // SS_1246_CQU_20151006
        end;                                                                            
    finally
        //f.Free;                               // SS_1017_PDO_20120514
        if Assigned(f) then FreeAndNil(f);      // SS_1017_PDO_20120514
    end;
end;

{-----------------------------------------------------------------------------
Function/Procedure Name: ImprimirComprobanteElectronico
Author: mbecerra
Date: 12-Marzo-2009
Description:	(Ref. Facturaci�n Electr�nica)
                Imprime las boletas o facturas electr�nicas
-----------------------------------------------------------------------------}
Function TFormComprobantesEmitidos.ImprimirComprobanteElectronico;
resourcestring
    MSG_ERROR_PREPARANDO    = 'Se produjo un Error mientras se preparaba el Reporte de la Factura.';
    MSG_CAPTION_ERROR       = 'Preparando Reporte Boleta Factura Electr�nica.';
    MSG_ERROR_COBRAR	    = 'No se actualiz� el campo "ReImpresi�n" para el comprobante';
	MSG_SQL_NI			    = 'SELECT dbo.ComprobanteTieneConceptosInfractor(''%s'', %d)';
var
    f	   : TReporteBoletaFacturaElectronicaForm;
    inf	   : TRptDetInfraccionesAnuladas;
    Tiene  : integer;
    sError : string;
    ImFdo  : boolean;

begin
    try
        Result := False;
        if NOT VerificarReimpresion(TipoComprobante,NroComprobante) then Exit;

        //Application.CreateForm(TReporteBoletaFacturaElectronicaForm,f);    // SS_1017_PDO_20120514
        f   := TReporteBoletaFacturaElectronicaForm.Create(nil);             // SS_1017_PDO_20120514
        inf := TRptDetInfraccionesAnuladas.Create(nil);                     // SS_1402_MBE_20151023

        try
            ImFdo := True;
            if not f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, sError, ImFdo) then begin
                ShowMessage( sError );
                Exit;
            end;
            Result := f.Ejecutar;
            if f.FImprimioComprobante then begin
                //spCobrar_o_DescontarReimpresion.Close;                                    // SS_1246_CQU_20151006
                //with spCobrar_o_DescontarReimpresion do begin                             // SS_1246_CQU_20151006
            	//    Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;  // SS_1246_CQU_20151006
                //    Parameters.ParamByName('@NumeroComprobante').Value := NroComprobante; // SS_1246_CQU_20151006
                //    Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;     // SS_1246_CQU_20151006
                //    ExecProc;                                                             // SS_1246_CQU_20151006
                //    if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin       // SS_1246_CQU_20151006
                //	    MsgBox(MSG_ERROR_COBRAR, Caption, MB_ICONEXCLAMATION);              // SS_1246_CQU_20151006
                //    end;                                                                  // SS_1246_CQU_20151006
                //end;                                                                      // SS_1246_CQU_20151006

                //Reimprimir detalle de las infracciones
                Tiene := QueryGetValueInt(	DMConnections.BaseCAC,
            							Format(MSG_SQL_NI, [TipoComprobante, NroComprobante]));
                if Tiene = 1 then begin
                    //Application.CreateForm(TRptDetInfraccionesAnuladas, inf);		// SS_1017_PDO_20120514
                    //inf := TRptDetInfraccionesAnuladas.Create(nil);				// SS_1402_MBE_20151023	// SS_1017_PDO_20120514
                	if not inf.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, sError) then inf.Release;
                end;
            end;

        finally
            //f.Free;
            if Assigned(f) then begin
                FreeAndNil(f);          // SS_1017_PDO_20120514
            end;

            if Assigned(inf) then begin
                FreeAndNil(inf);      // SS_1017_PDO_20120514
            end;
        end;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_PREPARANDO,e.Message,MSG_CAPTION_ERROR,MB_ICONSTOP)
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerListaComprobantesAfterClose
               y ObtenerListaComprobantesAfterOpen
  Author:    ndonadio
  Date Created: 04/01/2005
  Description: Controlan el comportamiento del Boton de Imprimir Comprobante
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.spObtenerListaComprobantesAfterClose(
  DataSet: TDataSet);
begin
    btnReimprimir.Enabled := False;
    btnImprimirFacturacionDetallada.Enabled := False;
end;

procedure TFormComprobantesEmitidos.spObtenerListaComprobantesAfterOpen(
  DataSet: TDataSet);
begin
   btnReimprimir.Enabled := not spObtenerListaComprobantes.IsEmpty;
   btnImprimirFacturacionDetallada.Enabled := not ObtenerTransitosPorComprobante.IsEmpty ;
end;

procedure TFormComprobantesEmitidos.dblDetalleDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Assigned(Column) then begin
        if Pos('TOTAL', UpperCase(Text)) <> 0 then
          dblDetalle.Canvas.Font.Style := [fsBold];
    end;
end;

procedure TFormComprobantesEmitidos.dblEstacionamientosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
	if spObtenerEstacionamientos.FieldByName('EstacionamientoAnulado').AsString <> '' then begin      	  // SS_1291_MBE_20150703
    	Sender.Canvas.Font.Color := clRed;                                                				  // SS_1291_MBE_20150703
    end;                                                                                  				  // SS_1291_MBE_20150703

end;

procedure TFormComprobantesEmitidos.spObtenerListaComprobantesAfterScroll(DataSet: TDataSet);
var
	TipoComprobante : string;														//SS_959_MBE_20110617
	EsComprobanteDevDinero, 														//SS_959_MBE_20110617
  EsNotaDebito, EsNotaCobro, EsCreditoCK, EsCreditoNC, EstaAnulado, EsFactura, EsBoleta: Boolean;               //20160802 MGO
begin
	TipoComprobante			:= spObtenerListaComprobantes.FieldByName('TipoComprobante').AsString;
	EsNotaCobro				:= (TipoComprobante = TC_NOTA_COBRO) ;                                          
	EsCreditoCK				:= (TipoComprobante = TC_NOTA_CREDITO_A_COBRO);                                 
	EsCreditoNC				:= (TipoComprobante = TC_NOTA_CREDITO);                                         
    EsNotaDebito			:= (TipoComprobante = TC_NOTA_DEBITO);                                          
    EsComprobanteDevDinero	:= (TipoComprobante = TC_DEVOLUCION_DINERO);                                    
    EsFactura               := (TipoComprobante = TC_FACTURA_EXENTA) or (TipoComprobante = TC_FACTURA_AFECTA);  //20160802 MGO
    EsBoleta                := (TipoComprobante = TC_BOLETA_EXENTA) or (TipoComprobante = TC_BOLETA_AFECTA);  //20160802 MGO
    EstaAnulado 			:= (spObtenerListaComprobantes.FieldByName('EstadoPago').AsString = 'A');
	btnReimprimir.Enabled 	:= EsNotaCobro or EsCreditoCK or EsCreditoNC or EsNotaDebito or EsComprobanteDevDinero or EsFactura or EsBoleta; //20160802 MGO
    tsTransacciones.Enabled := EsNotaCobro;
    btnImprimirFacturacionDetallada.Enabled := EsNotaCobro and not(EstaAnulado);
end;

procedure TFormComprobantesEmitidos.dblTransitosColumns0HeaderClick(Sender: TObject);
begin
    if NOT ObtenerTransitosPorComprobante.Active then Exit;
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;
    ObtenerTransitosPorComprobante.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{-----------------------------------------------------------------------------
  Function Name: dblComprobantesColumns0HeaderClick
  Author:    flamas
  Date Created: 25/02/2005
  Description: Ordena por Columna
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.dblComprobantesColumns0HeaderClick(Sender: TObject);
begin
   if NOT  spObtenerListaComprobantes.Active then Exit;
   if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   else TDBListExColumn(sender).Sorting := csAscending;
   spObtenerListaComprobantes.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 08/03/2005
  Description: Resetea el Timer para que se ejecute la consulta
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.peNumeroDocumentoChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
 	tmrConsultaRUT.Enabled := False;
	tmrConsultaRUT.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: tmrConsultaTimer
  Author:    flamas
  Date Created: 08/03/2005
  Description: Ejecuta la consulta
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.tmrConsultaRUTTimer(Sender: TObject);
begin
	Screen.Cursor := crHourglass;
    try
		tmrConsultaRUT.Enabled := False;
	    CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', PadL(Trim(peNumeroDocumento.Text), 9, '0'));
    	if cbConvenios.Items.Count > 0 then cbConvenios.ItemIndex := 0;
	    neNumeroComprobante.Value := 0;
        lblverConvenio.Enabled := (cbConvenios.Items.Count > 0);
	    CargarComprobantes;
    finally
		Screen.Cursor := crDefault;
    end;
end;

procedure TFormComprobantesEmitidos.OrdenarColumna(Sender: TObject);
begin
    if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumna(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName);
end;



{-----------------------------------------------------------------------------
  Function Name: tmrConsultaComprobanteTimer
  Author:    flamas
  Date Created: 08/03/2005
  Description: Dispara la Consulta por Comprobante
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.tmrConsultaComprobanteTimer(Sender: TObject);
begin
	try
		Screen.Cursor := crHourglass;
    	cbConvenios.Clear;
		CargarComprobantes;
    finally
		Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
Procedure: TFormComprobantesEmitidos.btnImprimirFacturacionDetalladaClick
Author:    ndonadio
Date:      09-Mar-2005
Description :   La variable ImporteTr�nstos es para poder
                utilizar como par�metro en la funci�n StringReplace, para borrar los signos "$" y "."
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.BTNBuscarComprobanteClick(Sender: TObject);
begin
    cbConvenios.Clear;
    txt_Patente.Clear;                                                          //SS_1361_MCA_20151006
    de_TransitosDesde.Clear;                                                    //SS_1361_MCA_20151006
	de_TransitosHasta.Clear;                                                    //SS_1361_MCA_20151006
    de_EstacionamientosDesde.Clear;                                             //SS_1361_MCA_20151006
    de_EstacionamientosHasta.Clear;                                             //SS_1361_MCA_20151006
    if ObtenerTransitosPorComprobante.Active then ObtenerTransitosPorComprobante.Close; //SS_1361_MCA_20151006
    if spObtenerEstacionamientos.Active then spObtenerEstacionamientos.Close;   //SS_1361_MCA_20151006

    CargarComprobantes;
end;


{******************************************************************************
						btnEstacBuscarClick
Author          :  mbecerra
Date Created    :  21-Junio-2010
Description     :   Despliega los estacionamientos asociados a un convenio
*******************************************************************************}
procedure TFormComprobantesEmitidos.btnEstacBuscarClick(Sender: TObject);
resourcestring																								//SS_1361_MCA_20151006
    MSG_NO_EXISTEN_ESTACIONAMIENTOS='No existen estacionamiento para la Nota de Cobro seleccionada.';		//SS_1361_MCA_20151006
begin
    //if not ValidarFiltroEstacionamientos then Exit; //SS 1006 / 1015_MCO_20120828 Se agrega validacion para las fechas del filtro de los estacionamientos	//SS_1361_MCA_20151006

	with spObtenerEstacionamientos do begin
    	Close;
        Parameters.Refresh;																						// SS_1006_HUR_20120213
        Parameters.ParamByName('@TipoComprobante').Value :=  cbTipoComprobante.Value;
        Parameters.ParamByName('@NumeroComprobante').Value := neNumeroComprobante.Value;
        Parameters.ParamByName('@CodigoConcesionaria').Value := Null;											// SS_1006_HUR_20120213
																												// SS_1006_HUR_20120213
        Parameters.ParamByName('@FechaInicial').Value  := iif(de_EstacionamientosDesde.Date <= 0, Null, de_EstacionamientosDesde.Date); //SS_1361_MCA_20151006
        Parameters.ParamByName('@FechaFinal').Value := iif(de_EstacionamientosHasta.Date <= 0, Null, de_EstacionamientosHasta.Date);    //SS_1361_MCA_20151006
        // Me falta Agregar el filtro para los estacionamientos
        if not(vcbConcesionariasEstacionamiento.Value = -1) then												// SS_1006_HUR_20120213
          Parameters.ParamByName('@CodigoConcesionaria').Value := vcbConcesionariasEstacionamiento.Value;		// SS_1006_HUR_20120213

        Open;
        if spObtenerEstacionamientos.IsEmpty and (ActiveControl = btnEstacBuscar) then		//SS_1361_MCA_20151006
               MsgBox(MSG_NO_EXISTEN_ESTACIONAMIENTOS, Self.Caption, MB_ICONINFORMATION);	//SS_1361_MCA_20151006
    end;
end;

procedure TFormComprobantesEmitidos.btnImprimirFacturacionDetalladaClick(Sender: TObject);
resourcestring
    MSG_NO_EXISTEN_TRANSITOS='No existen tr�nsitos para la Nota de Cobro seleccionada.';	//SS_1361_MCA_20151006
	MSG_SQL_NI			= 'SELECT dbo.ComprobanteTieneConceptosInfractor(''%s'', %d)';
    SQL_NI_NK               = 'SELECT dbo.ComprobanteTieneConceptosInfractoresYNormales(''%s'', %d)';   // SS_660_CQU_20121010
var
    f: TformReporteFacturacionDetallada;
    Error: String;
    inf : TRptDetInfraccionesAnuladas;
    Tiene : integer;
    TipoComprobante : string;
    NumeroComprobante :int64;
begin
	TipoComprobante := spObtenerListaComprobantes.FieldByName('TipoComprobante').AsString;
    NumeroComprobante := spObtenerListaComprobantes.FieldByName('NumeroComprobante').AsInteger;
	if ( TipoComprobante = TC_NOTA_CREDITO_A_COBRO) then exit;

    if QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_NI_NK, [TipoComprobante, NumeroComprobante])) = 0 then begin  // SS_660_CQU_20121010
    
        //Ver si debe Reimprimir detalle de las infracciones
        Tiene := QueryGetValueInt( DMConnections.BaseCAC,	Format(MSG_SQL_NI, [TipoComprobante, NumeroComprobante]));
        if Tiene = 1 then begin
            Application.CreateForm(TRptDetInfraccionesAnuladas, inf);
            if not inf.Inicializar(DMConnections.BaseCAC, TipoComprobante, NumeroComprobante, Error) then inf.Release;
        end
        else begin
            if QueryGetValueInt(DMConnections.BaseCAC,
                                Format('select dbo.ObtenerImportePeajeComprobante(''%s'',%d)',
                                        [TipoComprobante, NumeroComprobante])) = 0 then begin
                MsgBox(MSG_NO_EXISTEN_TRANSITOS);
                Exit;
            end;

            // Lanza el reporte de Facturacion detallada...
            screen.Cursor := crHourGlass;
            With spObtenerListaComprobantes do begin
                Application.CreateForm(TformreportefacturacionDetallada, f);
                try
                    if f.Inicializar(DMConnections.BaseCAC,
                        FieldByName('TipoComprobante').AsString,
                        FieldByName('NumeroComprobante').AsInteger, Error) then f.Ejecutar;
                finally
                    f.Release;
                    Screen.Cursor := crDefault;
                end;
            end;
        end;{else}
    end else begin                                                                                                      // SS_660_CQU_20121010
        screen.Cursor := crHourGlass;                                                                                   // SS_660_CQU_20121010
        With spObtenerListaComprobantes do begin                                                                        // SS_660_CQU_20121010
            f := TformreportefacturacionDetallada.Create(nil);                                                          // SS_660_CQU_20121010
            inf := TRptDetInfraccionesAnuladas.Create(nil);                                                             // SS_660_CQU_20121010
            try                                                                                                         // SS_660_CQU_20121010
                // El detalle de la NK                                                                                  // SS_660_CQU_20121010
                if f.Inicializar(DMConnections.BaseCAC,                                                                 // SS_660_CQU_20121010
                                FieldByName('TipoComprobante').AsString,                                                // SS_660_CQU_20121010
                                FieldByName('NumeroComprobante').AsInteger, Error)                                      // SS_660_CQU_20121010
                then f.Ejecutar;                                                                                        // SS_660_CQU_20121010
                                                                                                                        // SS_660_CQU_20121010
                // El Detalle de las Infracciones                                                                       // SS_660_CQU_20121010
                if not inf.Inicializar(DMConnections.BaseCAC,                                                           // SS_660_CQU_20121010
                                FieldByName('TipoComprobante').AsString,                                                // SS_660_CQU_20121010
                                FieldByName('NumeroComprobante').AsInteger, Error)                                      // SS_660_CQU_20121010
                then inf.Release;                                                                                       // SS_660_CQU_20121010
            finally                                                                                                     // SS_660_CQU_20121010
                if Assigned(f) then FreeAndNil(f);                                                                      // SS_660_CQU_20121010
                if Assigned(inf) then FreeAndNil(inf);                                                                  // SS_660_CQU_20121010
                Screen.Cursor := crDefault;                                                                             // SS_660_CQU_20121010
            end;                                                                                                        // SS_660_CQU_20121010
        end;                                                                                                            // SS_660_CQU_20121010
    end;                                                                                                                // SS_660_CQU_20121010
end;

{-----------------------------------------------------------------------------
  Function Name: gbDatosClienteClick
  Author:    mcabello
  Date Created: 30/08/2012
  Description: Funcionalidad que permites exporta a excel las transacciones.
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.lblCSVClick(Sender: TObject);
resourcestring
    MSG_TITLE   = 'Exportar CSV';
    MSG_ERROR   = 'ERROR';
    MSG_SUCCESS = 'El archivo %s fu� creado exitosamente';
var
    FileBuffer, Error: String;
    NombreArchivo: String;
begin
    if dblTransitos.DataSource.DataSet.IsEmpty then Exit;
    
    if DatasetToExcelCSV(dblTransitos.DataSource.DataSet, FileBuffer,
        Error) then begin

        NombreArchivo := peNumeroDocumento.Text
            + '_' + cbConvenios.Text
            + '_Transacciones_del_'
            + FormatDateTime ('yyyyMMdd', de_TransitosDesde.Date)
            + '_al_' + FormatDateTime ('yyyyMMdd', de_TransitosHasta.Date)
            + '.csv';
            sdGuardarCSV.FileName := NombreArchivo;

        if sdGuardarCSV.Execute then begin
            StringToFile(FileBuffer, sdGuardarCSV.FileName);
            MsgBox(Format(MSG_SUCCESS, [sdGuardarCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
        end;
    end else begin
        MsgBoxErr(MSG_ERROR, Error, MSG_ERROR, MB_ICONERROR);
    end;
end;

procedure TFormComprobantesEmitidos.lblverConvenioClick(Sender: TObject);
var
	f: TFormInicioConsultaConvenio;
begin
	if FindFormOrCreate(TFormInicioConsultaConvenio, f) then
		f.Show
	else begin
		if f.Inicializar(FConvenio, -1, -1) then begin
            f.Show;
        end
        else f.Release;
	end;
end;

procedure TFormComprobantesEmitidos.dblTransitosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column = dblTransitos.Columns[0] then
       Text := FormatDateTime('dd-mm-yyyy hh:nn', ObtenerTransitosPorComprobante.FieldByName('FechaHora').AsDateTime)
    else if Column = dblTransitos.Columns[7] then
       Text := RStr(ObtenerTransitosPorComprobante.FieldByName('Kms').AsFloat, 10, 2);

    if ObtenerTransitosPorComprobante.FieldByName('TransitoAnulado').AsString <> '' then begin            // SS_1291_MBE_20150703
    	Sender.Canvas.Font.Color := clRed;                                                				  // SS_1291_MBE_20150703
    end;                                                                                  				  // SS_1291_MBE_20150703

end;

{-----------------------------------------------------------------------------
  Function Name: VerificarReimpresion
  Author:    ndonadio
  Date Created: 04/05/2005
  Description: Verifica en que estado de reimpresion est�...
                    y arma el dialogo pertinente.
-----------------------------------------------------------------------------}
Function TFormComprobantesEmitidos.VerificarReimpresion(TipoComprobante: string; NumeroComprobante: int64):Boolean;
//var                                                                           // SS_1246_CQU_20150819
//   f: TfrmSeleccReimpresion;                                                  // SS_1246_CQU_20150819
begin
        Result := True;                                                         // SS_1246_CQU_20150819
        //Application.CreateForm(TfrmSeleccReimpresion,f);                      // SS_1246_CQU_20150819
        //try                                                                   // SS_1246_CQU_20150819
        //    if f.Inicializar(TipoComprobante, NumeroComprobante) then begin   // SS_1246_CQU_20150819
        //    	Result := (f.ShowModal <> mrCancel);                            // SS_1246_CQU_20150819
        //    end                                                               // SS_1246_CQU_20150819
        //    else begin                                                        // SS_1246_CQU_20150819
        //        Result := False;                                              // SS_1246_CQU_20150819
        //    end;                                                              // SS_1246_CQU_20150819
        //                                                                      // SS_1246_CQU_20150819
        //finally                                                               // SS_1246_CQU_20150819
        //    f.release;                                                        // SS_1246_CQU_20150819
        //end;                                                                  // SS_1246_CQU_20150819
end;


{-----------------------------------------------------------------------------
  Function Name: btnTransitosFiltrarClick
  Author:    ndonadio
  Date Created: 24/06/2005
  Description: Aplica los filtros al listado de transitos del comprobante...
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.btnTransitosFiltrarClick(
  Sender: TObject);
resourcestring
    ERROR_GETTING_INVOICE_TRANSITS = 'Ocurri� un error obteniendo la lista de tr�nsitos del comprobante.';
begin
   // if not ValidarFiltroTransitos then Exit;			//SS_1361_MCA_20151006

    Cursor := crHourglass;
    try
        if  (NOT spObtenerListaComprobantes.Active) OR
            (spObtenerListaComprobantes.IsEmpty) then Exit;
        try
            FiltrarTransitos;
        except
            on e: exception do begin
                msgBoxErr(ERROR_GETTING_INVOICE_TRANSITS, e.Message, caption, MB_ICONSTOP);
            end;
        end;
    finally
        Cursor := crDefault;
    end;
end;

//SS 1006 / 1015_MCO_20120828 se comenta debido a que se elimino el txt_CantidadTransitos
      {
procedure TFormComprobantesEmitidos.neCantidadTransitosExit(
  Sender: TObject);
resourcestring
    CAP_VALIDATION_ERROR    = 'Filtro Incorrecto';
    MSG_QTY_INVALID         = 'La cantidad a mostrar debe ser mayor o igual a uno (1).';
begin
   if NOT ValidateControls( [neCantidadTransitos], [(neCantidadTransitos.ValueInt > 0)], CAP_VALIDATION_ERROR, [MSG_QTY_INVALID]) then Exit;
end;
       }
procedure TFormComprobantesEmitidos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormComprobantesEmitidos.FormCreate(Sender: TObject);
var
  iAltoTotal:Integer;
begin
  iAltoTotal := dblComprobantes.ClientHeight+pgDetalles.ClientHeight;
  dblComprobantes.ClientHeight := Round(iAltoTotal*0.1);
  pgDetalles.ClientHeight := iAltoTotal -dblComprobantes.ClientHeight;
end;


{-----------------------------------------------------------------------------
  Function Name: EstablecerFechasTransitos
  Author:    gcasais
  Date Created: 21/04/2005
  Description: Establece un fecha Inicial y Final para el filtro de busqueda
               de Transitos
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.EstablecerFechasTransitos(var Inicial,
  Final: TDateTime);
const
    VALOR = -2;
var
    Year, Month, Day : Word;
begin
    //Fecha Incial: 2 meses Atras
    DecodeDate(NowBase(DMConnections.BaseCAC), Year, Month, Day);
    IncAMonth(Year, Month, Day, VALOR);
    Inicial := EncodeDate(Year, Month, Day);
    //Fecha Final: Hoy.
    Final := NowBase(DMConnections.BaseCAC);
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarFiltroTransitos
  Author:    gcasais
  Date Created: 22/04/2005
  Description: Valida los par�metros aplicados al filtro de tr�nsitos
-----------------------------------------------------------------------------}
function TFormComprobantesEmitidos.ValidarFiltroTransitos: Boolean;
resourcestring
    MSG_INVALID_DATE            = 'Fecha Inv�lida';
    MSG_INVALID_DATE_DESC       = 'La Fecha Indicada No Es V�lida';
    MSG_INVALID_ROW_COUNT       = 'Cantidad de Tr�nsitos Inv�lida';
    MSG_INVALID_ROW_COUNT_DESC  = 'La Cantidad de Tr�nsitos a Mostrar No Puede ser Inferior a 1';
var
    Year, Month, Day: Word;
begin
    Result := False;

    DecodeDate(de_TransitosDesde.Date, Year, Month, Day);
    if not ValidateControls([de_TransitosDesde],
            [IsValidDate(Year, Month, Day)],
            MSG_INVALID_DATE,
            [MSG_INVALID_DATE_DESC ]) then Exit;

    DecodeDate(de_TransitosHasta.Date, Year, Month, Day);
    if not ValidateControls([de_TransitosHasta], [IsValidDate(Year, Month, Day)],
        MSG_INVALID_DATE, [MSG_INVALID_DATE_DESC ]) then Exit;

    {if not ValidateControls([neCantidadTransitos], [neCantidadTransitos.ValueInt > 0],  //SS 1006 / 1015_MCO_20120828
        MSG_INVALID_ROW_COUNT, [MSG_INVALID_ROW_COUNT_DESC]) then Exit;         }

    Result := True;
end;

function TFormComprobantesEmitidos.ValidarFiltroEstacionamientos: Boolean;
resourcestring
    MSG_INVALID_DATE            = 'Fecha Inv�lida';
    MSG_INVALID_DATE_DESC       = 'La Fecha Indicada No Es V�lida';
var
    Year, Month, Day: Word;
begin
    Result := False;

    DecodeDate(de_EstacionamientosDesde.Date, Year, Month, Day);
    if not ValidateControls([de_EstacionamientosDesde],
            [IsValidDate(Year, Month, Day)],
            MSG_INVALID_DATE,
            [MSG_INVALID_DATE_DESC ]) then Exit;

    DecodeDate(de_EstacionamientosHasta.Date, Year, Month, Day);
    if not ValidateControls([de_EstacionamientosHasta], [IsValidDate(Year, Month, Day)],
        MSG_INVALID_DATE, [MSG_INVALID_DATE_DESC ]) then Exit;

    Result := True;
end;


procedure TFormComprobantesEmitidos.lbl_TransitosReestablecerClick(
  Sender: TObject);
begin
    //tablecerFechasTransitos(FTransitosDesde, FTransitosHasta); //SS 1006 / 1015_MCO_20120828 Se comenta para obtener las fechas de las NK por default
    //de_TransitosDesde.Date := FTransitosDesde;		//SS_1361_MCA_20151006
    //de_TransitosHasta.Date := FTransitosHasta;		//SS_1361_MCA_20151006
end;

{-----------------------------------------------------------------------------
  Procedure: TFormComprobantesEmitidos.FiltrarTransitos
  Author:    ggomez
  Date:      29-Jun-2005
  Description: Obtiene el listado de tr�nsitos seg�n los filtros seleccionados.
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidos.FiltrarTransitos;
resourcestring																					//SS_1361_MCA_20151006
        MSG_NO_EXISTEN_TRANSITOS='No existen tr�nsitos para la nota de cobro seleccionada.';	//SS_1361_MCA_20151006
begin

    ObtenerTransitosPorComprobante.Close;
    ObtenerTransitosPorComprobante.Parameters.Clear;
    ObtenerTransitosPorComprobante.Parameters.Refresh;
    ObtenerTransitosPorComprobante.Parameters.ParamByName('@TipoComprobante').Value     := spObtenerListaComprobantes.FieldByName('TipoComprobante').AsString;
    ObtenerTransitosPorComprobante.Parameters.ParamByName('@NumeroComprobante').Value   := spObtenerListaComprobantes.FieldByName('NumeroComprobante').AsInteger;
    //ObtenerTransitosPorComprobante.Parameters.ParamByName('@Cantidad').Value            := neCantidadTransitos.ValueInt;//SS 1006 / 1015_MCO_20120828
    ObtenerTransitosPorComprobante.Parameters.ParamByName('@FechaInicial').Value        := iif(de_TransitosDesde.Date <= 0, Null, de_TransitosDesde.Date);
    ObtenerTransitosPorComprobante.Parameters.ParamByName('@FechaFinal').Value          := iif(de_TransitosHasta.Date <= 0, Null, de_TransitosHasta.Date);
    ObtenerTransitosPorComprobante.Parameters.ParamByName('@Patente').Value             := iif(Trim(txt_Patente.Text) = EmptyStr, Null, Trim(txt_Patente.Text));
    //Rev.22 / 07-Mayo-2010 / Nelson Droguett Sierra
    if cbConcesionaria.ItemIndex>0 then
    	ObtenerTransitosPorComprobante.Parameters.ParamByName('@CodigoConcesionaria').Value := cbConcesionaria.Items[cbConcesionaria.ItemIndex].Value;
    ObtenerTransitosPorComprobante.Open;

    if ObtenerTransitosPorComprobante.IsEmpty and (ActiveControl = btnTransitosFiltrar) then   //SS_1361_MCA_20151006
               MsgBox(MSG_NO_EXISTEN_TRANSITOS, Self.Caption, MB_ICONINFORMATION);		       //SS_1361_MCA_20151006
end;

{******************************** Function Header ******************************
Function Name: MostrarControlesSegunComprobante
Author : mlopez
Date Created : 21/07/2006
Description :
*******************************************************************************}
procedure TFormComprobantesEmitidos.MostrarControlesSegunComprobante(TipoComprobante: String);
begin
    if TipoComprobante = TC_BOLETA then begin
        lblImpresoraFiscal.Visible := True;
        cbImpresorasFiscales.Visible := True;
        cbTipoComprobante.Width := 122;
    end else begin
        lblImpresoraFiscal.Visible := False;
        cbImpresorasFiscales.Visible := False;
        cbTipoComprobante.Width := 297;
    end;
end;


{
Procedure Name	: Cargar Concesionarias
Author			: Nelson Droguett Sierra
Date			: 07-Mayo-2010
Description		: Llena un combobox con las concesionarias
}
procedure TFormComprobantesEmitidos.CargarConcesionarias(Conn: TADOConnection;
  Combo: TVariantComboBox; CodigoTipoConcesionaria: Integer; OpcionTodas: boolean);		// SS_1006_HUR_20120213
resourcestring
    ERROR_NO_HAY_CONCESIONES    = 'No hay concesionarias definidas.';
    ERROR_NO_SE_CARGO_COMBO     = 'Fall� la carga del combo de concesionarias.';
Var
    Concesionaria: AnsiString;
    Codigo:          Integer;
begin
    try
        try
            // vacio el combo
            Combo.Items.Clear;																									// SS_1006_HUR_20120213
																																// SS_1006_HUR_20120213
            spObtenerConcesionarias.Parameters.Refresh;																			// SS_1006_HUR_20120213
            spObtenerConcesionarias.Parameters.ParamByName('@CodigoTipoConcesionaria').Value := Null;							// SS_1006_HUR_20120213
																																// SS_1006_HUR_20120213
            if (CodigoTipoConcesionaria > 0) then begin																			// SS_1006_HUR_20120213
              spObtenerConcesionarias.Parameters.ParamByName('@CodigoTipoConcesionaria').Value := CodigoTipoConcesionaria;		// SS_1006_HUR_20120213

            // Obtengo las concesionarias
            spObtenerConcesionarias.Open;
            // Si no hay ninguna, levanto una excepcion, para manejar los errores todos en el mismo lugar...
            if spObtenerConcesionarias.IsEmpty then raise exception.Create(ERROR_NO_HAY_CONCESIONES);							// SS_1006_HUR_20120213
																																// SS_1006_HUR_20120213
            if (OpcionTodas) then Combo.Items.Add('Todas las Concesionarias', -1);												// SS_1006_HUR_20120213

            While not spObtenerConcesionarias.EOF do begin
                Concesionaria := spObtenerConcesionarias.FieldByName('Descripcion').asString;
                Codigo        := spObtenerConcesionarias.FieldByName('CodigoConcesionaria').asInteger;

                Combo.Items.Add(Concesionaria, Codigo);

                spObtenerConcesionarias.Next;
            end;

            if Combo.Items.Count = 0 then raise exception.create(ERROR_NO_SE_CARGO_COMBO);

            Combo.ItemIndex := 0;																								// SS_1006_HUR_20120213
        end;
        except
            // Si surgi� un error, cargo la var Error y salgo...
            on e:exception do begin
                Exit;
            end;
        end;
    finally
        spObtenerConcesionarias.Close;
    end;
end;


{
Procedure Name	: cbConcesionariaChange
Author			: Nelson Droguett Sierra
Date			: 08-Mayo-2010
Description		: Si cambia el valor del combo, realiza el filtro nuevamente.
}
procedure TFormComprobantesEmitidos.cbConcesionariaChange(Sender: TObject);
begin
	btnTransitosFiltrar.Click;
end;
procedure TFormComprobantesEmitidos.vcbConcesionariasEstacionamientoChange(Sender: TObject);
begin
//PAR00133-FASE2-NDR-20110624
  btnEstacBuscar.Click;                                                         
end;

// SS_1017_PDO_20120514 Inicio bloque
procedure TFormComprobantesEmitidos.LiberarMemoria;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
end;
// SS_1017_PDO_20120514 Fin bloque

end.
