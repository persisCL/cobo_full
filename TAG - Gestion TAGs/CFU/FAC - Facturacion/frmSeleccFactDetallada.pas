{-----------------------------------------------------------------------------
 Unit Name: FormConfirmarBajaModifConvenio.pas
 Author:    mtraversa
 Date Created: 23/03/2005
 Language: ES-AR
 Description: Formulario que se usa para la confirmacion de las bajas y de las
              modificaciones realizadas sobre convenios
-----------------------------------------------------------------------------
Revision 1:
    Author : lgisuk
    Date : 03/11/2006
    Description :
        - El boton "Imprimir" ahora debe decir "Imprimir con Cobro"
        - el mensaje "la facturacion detallada tiene un costo de $xx" ahora debe
          decir "El detalle de Transacciones tiene un costo de $xx"
-----------------------------------------------------------------------------}

unit frmSeleccFactDetallada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DMConnection, DB, ADODB, PeaProcs, UtilProc;
type
  TfSeleccFactDetallada = class(TForm)
    btnImprimir: TButton;
    btnImprimirGratis: TButton;
    btnCancelar: TButton;
    spObtenerDatosFacturacionDetallada: TADOStoredProc;
    lblTipoImpresion: TLabel;
    lblDescontarGratis: TLabel;
    lblDetalle: TLabel;
    cbOpciones: TCheckBox;
    procedure btnImprimirGratisClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
    FImpresionGratis : boolean;
    FDescontarGratis : boolean;
	FCobrar : boolean;
    FOpciones: boolean;

  public
    function Inicializar(FTipoComprobante: string; FNumeroComprobante: int64 ): boolean;
    property ImpresionGratis : boolean read FImpresionGratis;
    property DescontarGratis : boolean read FDescontarGratis;
    property Cobrar : boolean read FCobrar;
    property Opciones: boolean read FOpciones;
  end;

var
  fSeleccFactDetallada: TfSeleccFactDetallada;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 29/03/2005
  Description: Inicializa el Form de Selecci�n de Impresi�n Detallada
  Parameters: FTipoComprobante: string; FNumeroComprobante : int64
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfSeleccFactDetallada.Inicializar(FTipoComprobante: string; FNumeroComprobante : int64 ): boolean;
resourcestring
	  MSG_ERROR_GETTING_DETAIL 	= 'Error obteniendo datos de Facturaci�n Detallada';
    MSG_FREE_DETAILED_PRINT     = 'La impresi�n detallada es gratuita';
    MSG_FREE_DISCOUNTED			= 'Ser� descontada de sus impresiones gratis';
    MSG_REMAINING_FREE_PRINTS	= 'Cantidad de impresiones gratis restantes: %d';
    MSG_CHARGED_DETAILED_PRINT  = 'Reimpresi�n Detalle de Transacciones';
    MSG_PRINTING_CHARGE			= 'El Detalle de Transacciones tiene un costo de $ %d';
begin
	result := True;
	with spObtenerDatosFacturacionDetallada, Parameters do begin
    	FImpresionGratis	:= False;
    	FDescontarGratis 	:= False;
		FCobrar 			:= False;

    	ParamByName('@TipoComprobante').Value := FTipoComprobante;
        ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        try
        	Open;
            if ((FieldByName('ImpresionGratis').AsBoolean) or
            	(FieldByName('CantidadGratis').AsInteger = -1))then begin
                lblTipoImpresion.Caption := MSG_FREE_DETAILED_PRINT;
                lblDescontarGratis.Caption := '';
                lblDetalle.Caption := '';
                btnImprimirGratis.Enabled := False;
                FOpciones := cbOpciones.Checked;
                FImpresionGratis := True;
            end else if (FieldByName('CantidadGratis').AsInteger > 0) then begin
                lblTipoImpresion.Caption := MSG_FREE_DETAILED_PRINT;
                lblDescontarGratis.Caption := MSG_FREE_DISCOUNTED;
                lblDetalle.Caption := Format(MSG_REMAINING_FREE_PRINTS, [FieldByName('CantidadGratis').AsInteger]);
                btnImprimirGratis.Enabled := True;
                FDescontarGratis := True;
                FOpciones := cbOpciones.Checked;
            end else begin
                lblTipoImpresion.Caption := MSG_CHARGED_DETAILED_PRINT;
                lblDescontarGratis.Caption := '';
                lblDetalle.Caption := Format(MSG_PRINTING_CHARGE, [FieldByName('PrecioFacturacion').AsInteger div 100]);
                btnImprimirGratis.Enabled := True;
                FCobrar := True;
                FOpciones := cbOpciones.Checked;
            end;

            spObtenerDatosFacturacionDetallada.Close;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_DETAIL, e.Message, Caption, MB_ICONSTOP);
            	result := False;
            end;
        end;
    end;
end;

procedure TfSeleccFactDetallada.btnImprimirGratisClick(Sender: TObject);
begin
	FImpresionGratis := True;
    FCobrar := False;
    FOpciones := cbOpciones.Checked;
    ModalResult := mrOk;
end;

procedure TfSeleccFactDetallada.btnImprimirClick(Sender: TObject);
begin
        FOpciones := cbOpciones.Checked;
end;

end.
