object frmMainForm: TfrmMainForm
  Left = 0
  Top = 0
  Caption = 'Servicio de Generacion de archivos PDF'
  ClientHeight = 90
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Service: TDPSService
    ServiceName = 'KTCGeneracionPDF_VS'
    DisplayName = 'KAPSCH TraffiCom SCL - Servicio de Generaci'#243'n de archivos PDF VS'
    OnStart = ServiceStart
    OnStop = ServiceStop
    OnAfterInstall = ServiceAfterInstall
    OnAfterUninstall = ServiceAfterUninstall
    Left = 20
    Top = 32
  end
  object tmrReporte: TTimer
    Enabled = False
    OnTimer = tmrReporteTimer
    Left = 108
    Top = 24
  end
  object spReporte: TADOStoredProc
    CommandTimeout = 6000
    Parameters = <>
    Left = 208
    Top = 32
  end
end
