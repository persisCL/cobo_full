{********************************** File Header ********************************
Author      : Nelson Droguett Sierra
Date        : 10-Febrero-2010
Description : Servicio de creacion de archivos PDF de los
                comprobantes para ser enviados por el ServicioEnvioMail

Revision  :2
Author      : Nelson Droguett Sierra
Date        : 22-Febrero-2010
Description : Se modifica la carga del flag de LOG, para que solo lea el .INI al
              momento de iniciar el servicio y no cada vez que se quiera escribir
              en el log, esto para disminuir el acceso a la unidad de red donde se
             guarda el LOG.
             Se agrega ademas el evento ServiceStop , para realizar tareas en la
             detencion del servicio. (Cerrar el archivo de log)

Revision  :3
Author      : Nelson Droguett Sierra
Date        : 17-Abril-2010
Description : Se obtiene el intervalo en segundos para la ejecucion desde el
        	parametro general GENERACIONPDF_INTERVALOSEGUNDOS


Revision  	: 4
Author      : Nelson Droguett Sierra
Date        : 28-Abril-2010
Description : Informar en el EVENT VIEWER no solo los errores sino tambien los
                exitos (ej, cuando parte el servicio)

Revision	: 5
Author		: Nelson Droguett Sierra
Date		: 07-Junio-2010
Description	: Se agrega registro en el log de eventos cada vez que se inicia y
            	se detiene el servicio por parte del usuario.
                Se registra en el visor de eventos el inicio del servicio cuando
                se ejecuta el evento ServiceStart

Revision	: 6
Author		: Nelson Droguett Sierra
Date		: 13-Agosto-2010
Description	: Se agrega una rutina que borra los JPGs generados por el timbre
              electronico, cuya fecha de archivo sea de X dias hacia atras (X debe
              salir de un parametro general CANT_DIAS_BORRAR_JPG_TIMBRE)
              Se agrega una rutina que borra los TEDs generados por el timbre
              electronico, cuya fecha de archivo sea de X dias hacia atras (X debe
              salir de un parametro general CANT_DIAS_BORRAR_JPG_TIMBRE)
Revision	: 7
Author    	: Nelson Droguett Sierra
Date		: 22-Septiembre-2010
Description	: Se modifica la eliminacion de archivos, ya que pasaba por alto el
              primer archivo del criterio buscado.
Firma		: SS-601-NDR-20100922

Revision	: 8
Author		: Nelson Droguett Sierra
Date		: 16-Noviembre-2010
Description	: Se registra en el visor de eventos los errores que caen en el Except
Firma		: SS-601-NDR-20101116

Firma : SS_995_PDO_20111006

Firma       : SS_1307_NDR_20150804
Descripcion : Separacion de balanceo de carga.
              El servicio podria configurarse para procesar ciertos mensajes basados en el ultimo digito de su ID
              Se hacen tareas de mantenimiento como desconectar la BD en cada ciclo, liberar memoria.
              Se debe hacer que los archivos no se generen en carpetas que requieran permisos de Windows

Etiqueta    : 20160621 MGO
Descripci�n : Se reemplaza el par�metro DIR_TIMBRE_ELECTRONICO por DIR_TIMBRE_ELECTRONICO_GENPDF

*******************************************************************************}
unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  DpsServ, DMConnection, UtilProc, Util,  EventLog, ExtCtrls, ADODB,
  DB, PeaTypes,  ConstParametrosGenerales, UtilDB, RBSetUp,
  frmReporteNotaCreditoElectronica,
  frmReporteBoletaFacturaElectronica,
  ppTypes, DateUtils, UtilRB, ReporteFactura, ReporteCK, ReporteNC, 
  StrUtils,                                                               //SS_1307_NDR_20150804
  ppPDFDevice, ppReport, frmReporteContratoAdhesionPA;                    // SS_995_PDO_20111006
type
  TfrmMainForm = class(TForm)
    Service: TDPSService;
    tmrReporte: TTimer;
    spReporte: TADOStoredProc;
    procedure tmrReporteTimer(Sender: TObject);
    procedure ServiceStart(Sender: TObject; var Started: Boolean; var ExitCode,
      SpecificExitCode: Cardinal);
    procedure ServiceAfterInstall(Sender: TObject);
    procedure ServiceAfterUninstall(Sender: TObject);
    procedure ServiceStop(Sender: TObject);
  private
    FServiceName: String;
    FIntervalo:  Integer;
    FDirectorio : String;
    FProcesar : string;                                                         				//SS_1307_NDR_20150804
    // Rev.2 / 22-Febrero-2010 / Nelson Droguett Sierra -------------------------------
    bDebeEscribirLog : Boolean;
    LogFile: TextFile;
    // --------------------------------------------------------------------------------
    FInstancia: Integer;		// SS_1034_PDO_20120420
    function ConectarBase: Boolean;
    function GenerarReporte(const TipoComprobante: string; NumeroComprobante: Int64; Out NombreArchivo: AnsiString):Boolean;
    function GenerarContratoPAK(pCodigoPersona: Integer; pNombreCliente, pNumeroDocumento, pEMail: string; pFechaContrato: TDateTime; pAdheridoEnvioEMail: Boolean; var pNombreArchivo: string): Boolean;
    function ConfigurarParametros: Boolean;
    function ProcesarReportes: Boolean;
    procedure ActualizarReporteGenerado(const CodigoMensaje: Integer; const NombreArchivo: AnsiString);
    function ImprimirDisco(TipoComprobante,TipoComprobanteFiscal: string; NumeroComprobante: Int64; const Reporte: TppReport; Out NombreArchivo: AnsiString): Boolean;	// SS_995_PDO_20111006
    function SumarReintento(const CodigoMensaje: Int64):Boolean;
    procedure LogAction(Mensaje: string);
    //Rev.6 / 13-Agosto-2010 / Nelson Droguett Sierra --------------------------------
    procedure BorrarJPGs();
    procedure BorrarTEDs();
    procedure LiberarMemoria;	// SS_1034_PDO_20120420
    function DesconectarBase: Boolean;                                                         //SS_1307_NDR_20150804   
  public
    function Inicializar: Boolean;
  end;

const
    // Formato de nombre de archivo a generar para cualquier reporte a disco
    // El formato es: (Dir de ejecuci�n del servicio) + TipoComprobante.NumeroComprobante.FechaHoraCreaci�n.pdf'
    FILE_NAME_TEMPLATE     =  '%s.%d.%s.pdf';
	FILE_NAME_TEMPLATE_PAK =  'PAK.%s.%s.pdf';
var
  frmMainform: TfrmMainForm;
  //Rev.6 / 13-Agosto-2010 / Nelson Droguett Sierra
  UltimaFechaEjecucionBorradoJPG : TDate;
  UltimaFechaEjecucionBorradoTED : TDate;
  ParametrosConfigurados:boolean; //SS_1307_NDR_20150804

implementation

{$R *.dfm}


procedure TfrmMainForm.ActualizarReporteGenerado(const CodigoMensaje: Integer; const NombreArchivo: AnsiString);
resourcestring
    MSG_REP_UPDATE_ERROR = 'Ha ocurrido un error marcando el reporte como creado para el mensaje %d. Detalle: %s';
const
    SQL_U = 'UPDATE Email_Comprobantes SET Reporte = 1, NombreArchivo = %s, EliminarArchivo = 1, Reintentos = 0  WHERE CODIGOMENSAJE = %d';
begin
    try
        QueryExecute(DMConnections.BaseCAC, Format(SQL_U, [QuotedStr(ExtractFileName(NombreArchivo)), CodigoMensaje]));
    except
        on e: Exception do begin
            LogAction(Format(MSG_REP_UPDATE_ERROR, [CodigoMensaje, e.Message]));
            EventLogReportEvent(elError, Format(MSG_REP_UPDATE_ERROR, [CodigoMensaje, e.Message]), '');
        end;
    end;
end;

//Rev.6 / 13-Agosto-2010 / Nelson Droguett Sierra ------------------------------------
{
Function Name	: BorrarJPGs
Author			: Nelson Droguett Sierra
Date			: 13-Agosto-2010
Description		: Borra los archivos JPG del directorio que se especifica en el parametro
                  general DIR_TIMBRE_ELECTRONICO + 'out\html'. Borra los archivos cuya
                  fecha sea anterior a la fecha de hoy menos la cantidad de dias definida
                  en el parametrogeneral CANT_DIAS_BORRAR_JPG_TIMBRE
}
procedure TfrmMainForm.BorrarJPGs;
var
  diasHaciaAtras:Integer;
  CarpetaSuite:String;
  FileSearch:  TSearchRec;
  FechaActual:TDateTime;
  CantidadLimiteABorrar:Integer;
  borrados:Integer;
  bHayArchivos:Boolean;			//SS-601-NDR-20100922
//Rev.8 / 16-Noviembre-2010 / Nelson Droguett Sierra-----------------------------------------------------------
resourcestring
    MSG_BORRARJPG_ERROR = 'Ha ocurrido un error borrando los archivos JPG del timbraje electr�nico. Detalle : %s';		//SS-601-NDR-20101116
begin
  try                                                                                                                  //SS-601-NDR-20101116
    borrados:=0;
    FechaActual:=StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()'));
    ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_DIAS_BORRAR_JPG_TIMBRE', diasHaciaAtras);
    ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_LIMITE_A_BORRAR_JPG', CantidadLimiteABorrar);
    if DaysBetween(FechaActual,UltimaFechaEjecucionBorradoJPG)>0 then begin
      UltimaFechaEjecucionBorradoJPG:=FechaActual;
      { INICIO : 20160621 MGO
      ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_TIMBRE_ELECTRONICO' , CarpetaSuite);
      }
      ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_TIMBRE_ELECTRONICO_GENPDF' , CarpetaSuite);
      // FIN : 20160621 MGO
      ChDir(GoodDir(CarpetaSuite)+'out\html');
      bHayArchivos := (FindFirst ('*.jpg', faAnyFile-faDirectory, FileSearch)=0);	//SS-601-NDR-20100922
      while (borrados<CantidadLimiteABorrar) and (bHayArchivos) do begin       //SS-601-NDR-20100922
          if FileSearch.Attr <> faDirectory then begin
             if FileDateToDateTime(FileSearch.Time) <= IncDay(Now,diasHaciaAtras*-1) then begin
                DeleteFile(FileSearch.Name);
                Inc(borrados);
             end;
          end;
          bHayArchivos := (FindNext(FileSearch)=0);			//SS-601-NDR-20100922
      end;
      FindClose(FileSearch);
    end;
  except                                                                                                             //SS-601-NDR-20101116
      on e: Exception do begin                                                                                       //SS-601-NDR-20101116
          LogAction(Format(MSG_BORRARJPG_ERROR, [e.Message]));                                                       //SS-601-NDR-20101116
          EventLogReportEvent(elError, Format(MSG_BORRARJPG_ERROR, [e.Message]), '');                                //SS-601-NDR-20101116
      end;                                                                                                           //SS-601-NDR-20101116
  end;                                                                                                               //SS-601-NDR-20101116
end;


{
Function Name	: BorrarTEDs
Author			: Nelson Droguett Sierra
Date			: 07-Septiembre-2010
Description		: Borra los archivos TED del directorio que se especifica en el parametro
                  general DIR_TIMBRE_ELECTRONICO + 'in\ted'. Borra los archivos cuya
                  fecha sea anterior a la fecha de hoy menos la cantidad de dias definida
                  en el parametrogeneral CANT_DIAS_BORRAR_JPG_TIMBRE
}
procedure TfrmMainForm.BorrarTEDs;
var
  diasHaciaAtras:Integer;
  CarpetaSuite:String;
  FileSearch:  TSearchRec;
  FechaActual:TDateTime;
  CantidadLimiteABorrar:Integer;
  borrados:Integer;
  bHayArchivos:Boolean;   													 //SS-601-NDR-20100922
//Rev.8 / 16-Noviembre-2010 / Nelson Droguett Sierra-----------------------------------------------------------
resourcestring
    MSG_BORRARTED_ERROR = 'Ha ocurrido un error borrando los archivos TED del timbraje electr�nico. Detalle : %s';		//SS-601-NDR-20101116
begin
  try                                                                                                                  //SS-601-NDR-20101116
    borrados:=0;
    FechaActual:=StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()'));
    ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_DIAS_BORRAR_JPG_TIMBRE', diasHaciaAtras);
    ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_LIMITE_A_BORRAR_JPG', CantidadLimiteABorrar);
    if DaysBetween(FechaActual,UltimaFechaEjecucionBorradoTED)>0 then begin
      UltimaFechaEjecucionBorradoTED:=FechaActual;
      { INICIO : 20160621 MGO
      ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_TIMBRE_ELECTRONICO' , CarpetaSuite);
      }
      ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_TIMBRE_ELECTRONICO_GENPDF' , CarpetaSuite);
      // FIN : 20160621 MGO
      ChDir(GoodDir(CarpetaSuite)+'in\ted');
      bHayArchivos := (FindFirst ('*.ted', faAnyFile-faDirectory, FileSearch)=0);           //SS-601-NDR-20100922
      while (borrados<CantidadLimiteABorrar) and (bHayArchivos) do begin      //SS-601-NDR-20100922
          if FileSearch.Attr <> faDirectory then begin
             if FileDateToDateTime(FileSearch.Time) <= IncDay(Now,diasHaciaAtras*-1) then begin
                DeleteFile(FileSearch.Name);
                Inc(borrados);
             end;
          end;
          bHayArchivos := (FindNext(FileSearch)=0);			               //SS-601-NDR-20100922
      end;
      FindClose(FileSearch);
    end;
  except                                                                                                             //SS-601-NDR-20101116
      on e: Exception do begin                                                                                       //SS-601-NDR-20101116
          LogAction(Format(MSG_BORRARTED_ERROR, [e.Message]));                                                       //SS-601-NDR-20101116
          EventLogReportEvent(elError, Format(MSG_BORRARTED_ERROR, [e.Message]), '');                                //SS-601-NDR-20101116
      end;                                                                                                           //SS-601-NDR-20101116
  end;                                                                                                               //SS-601-NDR-20101116
end;
//Rev.6-------------------------------------------------------------------------------

function TfrmMainForm.ConectarBase: Boolean;
resourcestring
    MSG_CONNECT_ERROR = 'Ha ocurrido un error conectando a la base de datos. Detalle: %s';
begin
    Result := False;
    try
        if not DMConnections.BaseCAC.Connected then begin
            DMConnections.SetUp;
            DMConnections.BaseCAC.Connected := True;
        end;
        Result := True;
    except
        on e: Exception do begin
            LogAction(Format(MSG_CONNECT_ERROR, [e.Message]));
            EventLogReportEvent(elError, Format(MSG_CONNECT_ERROR, [e.Message]), '');
        end;
    end;
end;


function TfrmMainForm.ConfigurarParametros: Boolean;
resourcestring
//Rev.8 / 16-Noviembre-2010 / Nelson Droguett Sierra-----------------------------------------------------------
    MSG_DIRECTORY_ERROR = 'El Directorio %s que se va a utlizar para guardar los archivos generados no se encuentra o no est� accesible';
begin
    try                                                                                                             //SS-601-NDR-20101116
      Result := False;
	  //BEGIN : SS_1307_NDR_20150804-------------------------------------------------------------------------------------
      if not PArametrosConfigurados then
      begin
        // Rev.3 / 16-Abril-2010 / Nelson Droguett Sierra
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'GENERACIONPDF_INTERVALOSEGUNDOS', FIntervalo) then Exit;
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_EMAILS_ADJUNTOS', FDirectorio) then Exit;
        if FDirectorio = ''  then FDirectorio := '.';
        FDirectorio := GoodDir(FDirectorio);
        if not (DirectoryExists(FDirectorio)) then begin
            LogAction(Format(MSG_DIRECTORY_ERROR, [FDirectorio]));
            EventLogReportEvent(elError, Format(MSG_DIRECTORY_ERROR, [FDirectorio]), '');
            Exit;
        end;
        ParametrosConfigurados:=True;
      end;
	  //END : SS_1307_NDR_20150804-------------------------------------------------------------------------------------
      Result := True;                                                                                               //SS-601-NDR-20101116
    except                                                                                                          //SS-601-NDR-20101116
        on e: Exception do begin                                                                                    //SS-601-NDR-20101116
            LogAction(Format(MSG_DIRECTORY_ERROR, [e.Message]));                                                    //SS-601-NDR-20101116
            EventLogReportEvent(elError, Format(MSG_DIRECTORY_ERROR, [e.Message]), '');                             //SS-601-NDR-20101116
        end;                                                                                                        //SS-601-NDR-20101116
    end;                                                                                                            //SS-601-NDR-20101116
end;


function TfrmMainForm.GenerarReporte(const TipoComprobante: string;
  NumeroComprobante: Int64; Out NombreArchivo: AnsiString): Boolean;
resourcestring
    MSG_REP_ERROR		= 'Ha ocurrido un error generando el reporte tipo %s y n�mero %d : Error : %s';		//SS_1307_NDR_20150804
    MSG_UNKNOWN_REPORT	= 'El reporte de tipo %s no est� soportado por este servicio';
    MSG_SQL_FISCAL		= 	'SELECT TipoComprobanteFiscal FROM Comprobantes (NOLOCK) ' +
    						'WHERE TipoComprobante = ''%s'' AND NumeroComprobante = %d';
var
    ReporteNK: TfrmReporteFactura;
    ReporteNC: TfrmReporteNC;
    ReporteCK: TfrmReporteCK;
    ReporteNKElectronica: TReporteBoletaFacturaElectronicaForm;
    ReporteCKElectronica: TReporteNotaCreditoElectronicaForm;
    TipoComprobanteFiscal, DescriErr, NombreArchivoConsumos: String;
    ContenidoArchivo:String;														//SS_1307_NDR_20150804
    ArchivoPDF : TStringList;														//SS_1307_NDR_20150804
begin
    Result := False;
    //NombreArchivo := EmptyStr;																			//SS_1307_NDR_20150804
	NombreArchivo := Format(FDirectorio + FILE_NAME_TEMPLATE, 												//SS_1307_NDR_20150804
							[TipoComprobante, NumeroComprobante, FormatDateTime('ddmmyyyyhhnnss', Now)]		//SS_1307_NDR_20150804
							);																				//SS_1307_NDR_20150804

    TipoComprobanteFiscal := QueryGetValue(	DMConnections.BaseCAC,
                                            Format(MSG_SQL_FISCAL,
                                            [TipoComprobante, NumeroComprobante]));
    if	(TipoComprobanteFiscal = TC_FACTURA_EXENTA ) or
    	(TipoComprobanteFiscal = TC_FACTURA_AFECTA ) or
        (TipoComprobanteFiscal = TC_BOLETA_EXENTA  ) or
        (TipoComprobanteFiscal = TC_BOLETA_AFECTA  ) then
    begin
        try
            //Application.CreateForm(TReporteBoletaFacturaElectronicaForm, ReporteNKElectronica);   // SS_1034_PDO_20120420
            ReporteNKElectronica := TReporteBoletaFacturaElectronicaForm.Create(Nil);               // SS_1034_PDO_20120420

            //if not ReporteNKElectronica.Inicializar(DMConnections.BaseCAC, TipoComprobante, NumeroComprobante, DescriErr, True, False) then begin
            if not ReporteNKElectronica.Inicializar(  DMConnections.BaseCAC,
                                                     TipoComprobante,
                                                     NumeroComprobante,
                                                     DescriErr,
                                                     True,
                                                     False,                                         //SS_1307_NDR_20150804
                                                     oiServicio) then                               //SS_1307_NDR_20150804
            begin
               EventLogReportEvent(elError, Format(MSG_REP_ERROR, [TC_NOTA_COBRO, NumeroComprobante, DescriErr]), EmptyStr);
               Exit;
            end;
      		//BEGIN : SS_1307_NDR_20150804-------------------------------------------------------------------------------------
			//Result := ImprimirDisco(TipoComprobante,TipoComprobanteFiscal, NumeroComprobante, ReporteNKElectronica.rptReporteFactura, NombreArchivo);	// SS_995_PDO_20111006
            try
              try
                  if ReporteNKElectronica.EjecutarPDF(ContenidoArchivo, DescriErr) then
                  begin
                      ArchivoPDF := TStringList.Create;
                      // Cargo el pdf
                      ArchivoPDF.Add(ContenidoArchivo);
                      // Grabo el archivo PDF
                      ArchivoPDF.SaveToFile(NombreArchivo);
                      // Libero el objeto
                      ArchivoPDF.Free;
                      // Verifico que el archivo se haya creado
                      if FileExists(NombreArchivo)
                      then
                      begin
                          DescriErr := NombreArchivo;
                          Result:=True;
                      end
                      else
                      begin
                          DescriErr := 'No se pudo obtener el comprobante';
                      end;
                  end
                  else
                  begin
                      DescriErr := Format(MSG_REP_ERROR, [TipoComprobante, NumeroComprobante, DescriErr]);
                      EventLogReportEvent(elError, Format(MSG_UNKNOWN_REPORT, [TipoComprobante]), EmptyStr);
                  end;
              except
                  on E : Exception do begin
                      DescriErr := Format(MSG_REP_ERROR, [TipoComprobante, NumeroComprobante, E.Message]);
                      EventLogReportEvent(elError, Format(MSG_UNKNOWN_REPORT, [TipoComprobante]), EmptyStr);
                  end;
              end;
              //END : SS_1307_NDR_20150804-------------------------------------------------------------------------------------


              if FileExists(NombreArchivo) then
              begin
                LogAction('Validando que exista archivo ' + NombreArchivo + ' : OK');
                NombreArchivo := ExtractFileName(NombreArchivo);

                if ReporteNKElectronica.MostrarConsumoAparte then
                begin
	                NombreArchivoConsumos := EmptyStr;
	                Result := ImprimirDisco(TipoComprobante,TipoComprobanteFiscal, NumeroComprobante, ReporteNKElectronica.rptReporteConsumo, NombreArchivoConsumos);

	                if FileExists(NombreArchivoConsumos) then
	                begin
	                  LogAction('Validando que exista archivo ' + NombreArchivoConsumos + ' : OK');
	                  NombreArchivoConsumos := ExtractFileName(NombreArchivoConsumos);
	                  NombreArchivo := NombreArchivo + ';' + NombreArchivoConsumos;
	                end
	                else
                    begin
	                  Result := False;
	                end;
	              end;
	            end
              else
              begin
                Result := False;
              end;
            except
              Result := False;
            end;
        finally
            //ReporteNKElectronica.Release;                                             // SS_1034_PDO_20120420
            if Assigned(ReporteNKElectronica) then FreeAndNil(ReporteNKElectronica);    // SS_1034_PDO_20120420
        end;
    end
    else if TipoComprobanteFiscal = TC_NOTA_COBRO then begin
        LogAction('ProcesarReportes GenerarReporte Crear Form START');
         try
             //Application.CreateForm(TfrmReporteFactura, ReporteNK);   // SS_1034_PDO_20120420
             ReporteNK := TfrmReporteFactura.Create(Nil);               // SS_1034_PDO_20120420

             if not ReporteNK.Inicializar(DMConnections.BaseCAC, TipoComprobante, NumeroComprobante, DescriErr, True, False) then begin
                EventLogReportEvent(elError, Format(MSG_REP_ERROR, [TC_NOTA_COBRO, NumeroComprobante, DescriErr]), EmptyStr);
                Exit;
             end;
             Result := ImprimirDisco(TipoComprobante,TipoComprobanteFiscal, NumeroComprobante, ReporteNK.rptReporteFactura, NombreArchivo);		// SS_995_PDO_20111006
             if FileExists(  NombreArchivo) then LogAction('Validando que exista archivo ' +  NombreArchivo + ' : OK')
             else begin
                Result := False;
             end;
         finally
            //ReporteNK.Release;                                    // SS_1034_PDO_20120420
            if Assigned(ReporteNK) then FreeAndNil(ReporteNK);      // SS_1034_PDO_20120420
         end;
    end else if TipoComprobanteFiscal = TC_NOTA_CREDITO then begin
         try
             //Application.CreateForm(TfrmReporteNC, ReporteNC);    // SS_1034_PDO_20120420
             ReporteNC := TfrmReporteNC.Create(nil);                // SS_1034_PDO_20120420

             if not ReporteNC.Inicializar(DMConnections.BaseCAC, TipoComprobante, NumeroComprobante, DescriErr) then begin
                EventLogReportEvent(elError, Format(MSG_REP_ERROR, [TC_NOTA_CREDITO, NumeroComprobante, DescriErr]), EmptyStr);
                Exit;
             end;
             Result := ImprimirDisco(TipoComprobante,TipoComprobanteFiscal, NumeroComprobante, ReporteNC.rptNC, NombreArchivo);		// SS_995_PDO_20111006
             if FileExists( NombreArchivo) then LogAction('Validando que exista archivo ' + NombreArchivo + ' : OK')
             else begin
                Result := False;
             end;
         finally
            //ReporteNC.Release;                                    // SS_1034_PDO_20120420
            if Assigned(ReporteNC) then FreeAndNil(ReporteNC);      // SS_1034_PDO_20120420
         end;
    end else if TipoComprobanteFiscal = TC_NOTA_CREDITO_A_COBRO then begin
         try
             //Application.CreateForm(TfrmReporteCK, ReporteCK);    // SS_1034_PDO_20120420
             ReporteCK := TfrmReporteCK.Create(nil);                // SS_1034_PDO_20120420

             if not ReporteCK.Inicializar(DMConnections.BaseCAC, TipoComprobante, NumeroComprobante, DescriErr) then begin
                EventLogReportEvent(elError, Format(MSG_REP_ERROR, [TC_NOTA_CREDITO_A_COBRO, NumeroComprobante, DescriErr]), EmptyStr);
                Exit;
             end;
             Result := ImprimirDisco(TipoComprobante,TipoComprobanteFiscal, NumeroComprobante, ReporteCK.rptCK, NombreArchivo);		// SS_995_PDO_20111006
             if FileExists( NombreArchivo) then LogAction('Validando que exista archivo ' + NombreArchivo + ' : OK')
             else begin
                Result := False;
             end;
         finally
            //ReporteCK.Release;                                    // SS_1034_PDO_20120420
            if Assigned(ReporteCK) then FreeAndNil(ReporteCK);      // SS_1034_PDO_20120420

         end;
    end else if TipoComprobanteFiscal = TC_NOTA_CREDITO_ELECTRONICA {REV 6 TipoComprobante = TC_NOTA_CREDITO_A_COBRO} then begin
         try
             //Application.CreateForm(TReporteNotaCreditoElectronicaForm, ReporteCKElectronica);    // SS_1034_PDO_20120420
             ReporteCKElectronica := TReporteNotaCreditoElectronicaForm.Create(nil);                // SS_1034_PDO_20120420

             if not ReporteCKElectronica.Inicializar(DMConnections.BaseCAC, TipoComprobante, NumeroComprobante, DescriErr) then begin
                EventLogReportEvent(elError, Format(MSG_REP_ERROR, [TC_NOTA_CREDITO_A_COBRO, NumeroComprobante, DescriErr]), EmptyStr);
                Exit;
             end;
             Result := ImprimirDisco(TipoComprobante,TipoComprobanteFiscal, NumeroComprobante, ReporteCKElectronica.rptCK, NombreArchivo);		// SS_995_PDO_20111006
             if FileExists( NombreArchivo) then LogAction('Validando que exista archivo ' + NombreArchivo + ' : OK')
             else begin
                Result := False;
             end;
         finally
            //ReporteCKElectronica.Release;                                                 // SS_1034_PDO_20120420
            if Assigned(ReporteCKElectronica) then FreeAndNil(ReporteCKElectronica);        // SS_1034_PDO_20120420
            
         end;
    end else begin
        LogAction(Format(MSG_UNKNOWN_REPORT, [TipoComprobante]));
        EventLogReportEvent(elError, Format(MSG_UNKNOWN_REPORT, [TipoComprobante]), EmptyStr);
    end;
end;


function TfrmMainForm.ImprimirDisco(TipoComprobante,TipoComprobanteFiscal: string; NumeroComprobante: Int64; const Reporte: TppReport; Out NombreArchivo: AnsiString): Boolean;		// SS_995_PDO_20111006
resourcestring
    MSG_JOB_ERROR = 'Ha ocurrido un error generando el archivo adjunto tipo comprobante %s de n�mero %d. Detalle: %s';
    MSG_TIMBRAR_ERROR = 'No se pudo Timbrar el Comprobante.';	// SS_995_PDO_20111006
var
    DescriErr: String;
  	FNombreImagenFondo: string;
    Exportador: TppPDFDevice;	// SS_995_PDO_20111006

begin
    Result := False;
    NombreArchivo := Format(FDirectorio + FILE_NAME_TEMPLATE, [TipoComprobante, NumeroComprobante, FormatDateTime('ddmmyyyyhhnnss', Now)]);
    try

        if Reporte.Name = 'rptReporteConsumo' then begin
            NombreArchivo := Format(FDirectorio + FILE_NAME_TEMPLATE, ['DC', NumeroComprobante, FormatDateTime('ddmmyyyyhhnnss', Now)]);
        end;

        if (TipoComprobanteFiscal = TC_NOTA_CREDITO)                                                                                                                        // SS_995_PDO_20111006
            or (TipoComprobanteFiscal = TC_NOTA_CREDITO_ELECTRONICA)                                                                                                        // SS_995_PDO_20111006
            or (TipoComprobanteFiscal = TC_NOTA_CREDITO_A_COBRO)                                                                                                            // SS_995_PDO_20111006
            or (TipoComprobanteFiscal = TC_FACTURA_EXENTA)                                                                                                                  // SS_995_PDO_20111006
            or (TipoComprobanteFiscal = TC_FACTURA_AFECTA)                                                                                                                  // SS_995_PDO_20111006
            or (TipoComprobanteFiscal = TC_BOLETA_EXENTA)                                                                                                                   // SS_995_PDO_20111006
            or (TipoComprobanteFiscal = TC_BOLETA_AFECTA)                                                                                                                   // SS_995_PDO_20111006
            or (TipoComprobanteFiscal = TC_NOTA_COBRO) then try                                                                                                             // SS_995_PDO_20111006
                                                                                                                                                                            // SS_995_PDO_20111006
            if TipoComprobanteFiscal = TC_NOTA_CREDITO_ELECTRONICA then begin                                                                                               // SS_995_PDO_20111006
                // Este tipo de documento se llama en forma diferente porque el reporte                                                                                     // SS_995_PDO_20111006
                // esta construido distinto al reporte de boletaFacturaElectronica. Levantaba                                                                               // SS_995_PDO_20111006
                // una pantalla de visualizacion, lo que para una ejecucion como servicio                                                                                   // SS_995_PDO_20111006
                // no corresponde.                                                                                                                                          // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).spComprobantes.Close;                                                                                     // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).spComprobantes.Parameters.ParamByName('@TipoComprobante').Value:= TipoComprobante;                        // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value:= NumeroComprobante;                    // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).spComprobantes.Open;                                                                                      // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).spObtenerLineasImpresionCK.Close;                                                                         // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).spObtenerLineasImpresionCK.Parameters.ParamByName('@TipoComprobante').Value:= TipoComprobante;            // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).spObtenerLineasImpresionCK.Parameters.ParamByName('@NumeroComprobante').Value:= NumeroComprobante;        // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).spObtenerLineasImpresionCK.Open;                                                                          // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).ppTipoDocumentoElectronico.Text:='NOTA DE CR�DITO ELECTR�NICA';                                           // SS_995_PDO_20111006
                TReporteNotaCreditoElectronicaForm(Reporte.Owner).ppImageFondo.Picture.LoadFromFile(TReporteNotaCreditoElectronicaForm(Reporte.Owner).FNombreImagenFondo);  // SS_995_PDO_20111006
                if not TReporteNotaCreditoElectronicaForm(Reporte.Owner).ObtenerTimbreElectronico then begin                                                                // SS_995_PDO_20111006
                    raise Exception.Create(MSG_TIMBRAR_ERROR);                                                                                                              // SS_995_PDO_20111006
                end;                                                                                                                                                        // SS_995_PDO_20111006
            end;
                                                                                                                                                                            // SS_995_PDO_20111006
            Exportador := TppPDFDevice.Create(nil);                                                                                                                         // SS_995_PDO_20111006
                                                                                                                                                                            // SS_995_PDO_20111006
            TppPDFDevice(Exportador).PDFSettings.OptimizeImageExport := False;                                                                                              // SS_995_PDO_20111006
            TppPDFDevice(Exportador).PDFSettings.ScaleImages := False;                                                                                                      // SS_995_PDO_20111006
            TppPDFDevice(Exportador).FileName := NombreArchivo;                                                                                                             // SS_995_PDO_20111006
                                                                                                                                                                            // SS_995_PDO_20111006
            Exportador.Publisher := Reporte.Publisher;                                                                                                                      // SS_995_PDO_20111006
            Exportador.Reset;                                                                                                                                               // SS_995_PDO_20111006
            Reporte.PrintToDevices;                                                                                                                                         // SS_995_PDO_20111006
                                                                                                                                                                            // SS_995_PDO_20111006
        finally                                                                                                                                                             // SS_995_PDO_20111006
            if Assigned(Exportador) then FreeAndNil(Exportador);                                                                                                            // SS_995_PDO_20111006
        end;                                                                                                                                                                // SS_995_PDO_20111006

        Result := True;
    except
        on e: Exception do begin
            LogAction(Format(MSG_JOB_ERROR, [TipoComprobante, NumeroComprobante, e.Message]));
            EventLogReportEvent(elError, Format(MSG_JOB_ERROR, [TipoComprobante, NumeroComprobante, e.Message]), EmptyStr);
        end;
    end;
end;

function TfrmMainForm.Inicializar: Boolean;
resourcestring
    MSG_INSTALL                         = 'Instalar';
    MSG_UNINSTALL                       = 'Desinstalar';
    MSG_SERVICE_SUCCESS_INSTALLING      = 'Servicio instalado con �xito';
    MSG_SERVICE_ERROR_INSTALLING        = 'Error al instalar el Servicio %s:' + CRLF + '%s';
    MSG_SERVICE_SUCCESS_UNINSTALLING    = 'Servicio desinstalado con �xito';
    MSG_SERVICE_ERROR_UNINSTALLING      = 'Error al desinstalar el Servicio %s:' + CRLF + '%s';
    MSG_SERVICE_CANNOT_START            = 'El servicio no pudo iniciarse debido al siguiente Error: %s';
var
    ErrorCode: DWORD;
    NumeroComprobante: Int64;
    DescriErr: string;
begin
    ParametrosConfigurados:=False;																	// SS_1307_NDR_20150804
    //Rev.6 / 13-Agosto-2010 / Nelson Droguett Sierra---------------------------------
    UltimaFechaEjecucionBorradoJPG := StartOfTheYear(Now);
    UltimaFechaEjecucionBorradoTED := StartOfTheYear(Now);
    //Rev.6---------------------------------------------------------------------------
    DMConnections.SetUp;
    FInstancia          := InstallIni.ReadInteger('General','Instancia',0);                       // SS_1034_PDO_20120419
    FProcesar           := InstallIni.ReadString('General','Procesar','1,2,3,4,5,6,7,8,9,0');     // SS_1307_NDR_20150804
    FServiceName        := Service.ServiceName + '_' + IntToStr(FInstancia);                          // SS_1307_NDR_20150804 SS_1034_PDO_20120419
    //FServiceName        := Service.ServiceName + '_' + ReplaceStr(FProcesar,',','');              // SS_1307_NDR_20150804
    Service.ServiceName := FServiceName;                                                          // SS_1034_PDO_20120419
    //Service.DisplayName := Service.DisplayName + IIf(FInstancia = 0, ' [Pares]', ' [Impares]'); // SS_1307_NDR_20150804 SS_1034_PDO_20120419 SS_1034_PDO_20120419
//    Service.DisplayName := Service.DisplayName + '['+ReplaceStr(FProcesar,',','')+']';            // SS_1307_NDR_20150804

    spReporte.Connection := DMConnections.BaseCAC;
    spReporte.ProcedureName := 'EMAIL_ObtenerComprobantesAImprimir';
	Result := False;
	if FindCmdLineSwitch('install', ['/', '-'], True) then begin
		// Instalaci�n del Servicio
		if InstallServices(ErrorCode, FServiceName) then begin
			MsgBox(MSG_SERVICE_SUCCESS_INSTALLING, MSG_INSTALL, MB_ICONINFORMATION);
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_INSTALLING , '');
		end else begin
			MsgBox(Format(MSG_SERVICE_ERROR_INSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]),
			  MSG_INSTALL, MB_ICONSTOP);
            EventLogReportEvent(elError, Format(MSG_SERVICE_ERROR_INSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]) , '');
		end;
	end else if FindCmdLineSwitch('uninstall', ['/', '-'], True) then begin
		// Desinstalaci�n del Servicio
		if UninstallServices(ErrorCode, FServiceName) then begin
			MsgBox(MSG_SERVICE_SUCCESS_UNINSTALLING, MSG_UNINSTALL, MB_ICONINFORMATION);
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_UNINSTALLING , '');
		end else begin
			MsgBox(Format(MSG_SERVICE_ERROR_UNINSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]),
			  MSG_UNINSTALL, MB_ICONSTOP);
            EventLogReportEvent(elError, Format(MSG_SERVICE_ERROR_UNINSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]) , '');
		end;
	end else begin
		// Ejecuci�n del Servicio
		Result := StartServices;
		if Result then begin
			if not IsServiceApplication then Show;
		end else begin
			EventLogReportEvent(elError,
              Format(MSG_SERVICE_CANNOT_START, [SysErrorMessage(GetLastError())]), '');
		end;
    end;
end;


function TfrmMainForm.ProcesarReportes: Boolean;
resourcestring
    MSG_GET_REPORT_JOB_ERROR = 'Ha ocurrido un error obteniendo reportes a generar para el comprobante tipo %s y n�mero %d: Detalle: %s';
var
    CodigoMensaje, NumeroComprobante: Int64;
    TipoComprobante, NombreArchivo : String;
    ReportesProcesados: Integer;	// SS_1034_PDO_20120420
    NombreCliente,
    NumeroDocumento,
    EMail: string;
    CodigoPersona: Integer;
    FechaContrato: TDateTime;
    AdheridoEnvioEMail: Boolean;
    spEMAIL_ActualizarContratoAdhesionPAKAImprimir: TADOStoredProc;
begin
    //Rev.6 / 13-Agosto-2010 / Nelson Droguett Sierra
    //BorrarJPGs();
    //BorrarTEDs();
    //FinRev.6---------------------------------------
    Result := False;
    TipoComprobante := '';
    NumeroComprobante := 0;
    LogAction('ProcesarReportes START');
    try
      spReporte.Close;
      spReporte.ProcedureName := 'EMAIL_ObtenerComprobantesAImprimir';
      spReporte.Parameters.Refresh;
      spReporte.Parameters.ParamByName('@Instancia').Value := FInstancia;
      spReporte.Open;
      spReporte.First;                                                   
      //Result := not spReporte.IsEmpty;
      //if not Result then Exit;

      ReportesProcesados := 0;	// SS_1034_PDO_20120419

      while not spReporte.Eof do
      begin
        CodigoMensaje       := spReporte.FieldByName('CodigoMensaje').AsInteger;
        TipoComprobante     := spReporte.FieldByName('TipoComprobante').AsString;
        NumeroComprobante   := spReporte.FieldByName('NumeroComprobante').AsInteger;

        if NumeroComprobante <= 0 then
            raise Exception.Create('El n�mero de comprobante a imprimir no es v�lido, es menor o igual a cero');
        LogAction('ProcesarReportes GenerarReporte START para el comprobante: ' +   TipoComprobante + ' ' +
                                                                                    IntToStr(NumeroComprobante) + ' ' +
                                                                                    NombreArchivo);
        if GenerarReporte(TipoComprobante, NumeroComprobante, NombreArchivo)then
		    begin
          LogAction('ProcesarReportes GenerarReporte END');
          LogAction('ProcesarReportes ActualizarReporteGenerado START');
          ActualizarReporteGenerado(CodigoMensaje, NombreArchivo);
          LogAction('ProcesarReportes ActualizarReporteGenerado END');
        end
        else
        begin
          SumarReintento(CodigoMensaje);
        end;

        Inc(ReportesProcesados);              // SS_1034_PDO_20120419
        if ReportesProcesados = 5 then        // SS_1034_PDO_20120419
        begin                                 // SS_1034_PDO_20120419
            LiberarMemoria;                   // SS_1034_PDO_20120419
            ReportesProcesados := 0;          // SS_1034_PDO_20120419
        end;                                  // SS_1034_PDO_20120419

        spReporte.Next;

        if spReporte.Eof then begin

            spReporte.Close;
            spReporte.Open;
        end;

      end;

      spReporte.Close;
//      spReporte.ProcedureName := 'EMAIL_ObtenerContratoAdhesionPAKAImprimir';
//      spReporte.Parameters.Refresh;
//      spReporte.Parameters.ParamByName('@Instancia').Value := FInstancia;
//      spReporte.Open;
//
//      Result := not spReporte.IsEmpty;
//      if not Result then Exit;
//
//      ReportesProcesados := 0;
//
//      while not spReporte.Eof do
//      begin
//        CodigoMensaje      := spReporte.FieldByName('CodigoMensaje').AsInteger;
//        CodigoPersona	   := spReporte.FieldByName('CodigoPersona').AsInteger;
//        NombreCliente      := spReporte.FieldByName('NombreCompleto').AsString;
//        NumeroDocumento    := spReporte.FieldByName('NumeroDocumento').AsString;
//        EMail              := spReporte.FieldByName('Email').AsString;
//        FechaContrato      := spReporte.FieldByName('FechaContrato').AsDateTime;
//        AdheridoEnvioEMail := spReporte.FieldByName('AdheridoEnvioEMail').AsBoolean;
//
//        if GenerarContratoPAK(CodigoPersona, NombreCliente, NumeroDocumento, EMail, FechaContrato, AdheridoEnvioEMail, NombreArchivo) then
//        try
//        	try
//            spEMAIL_ActualizarContratoAdhesionPAKAImprimir := TADOStoredProc.Create(nil);
//
//            with spEMAIL_ActualizarContratoAdhesionPAKAImprimir do
//            begin
//              Connection    := DMConnections.BaseCAC;
//              ProcedureName := 'EMAIL_ActualizarContratoAdhesionPAKAImprimir';
//              Parameters.Refresh;
//              Parameters.ParamByName('@CodigoMensaje').Value := CodigoMensaje;
//              Parameters.ParamByName('@NombreArchivo').Value := NombreArchivo;
//              ExecProc;
//
//              if Parameters.ParamByName('@RETURN_VALUE').Value = -1 then
//              begin
//                raise Exception.Create('Error');
//              end;
//            end;
//          except
//            on e: exception do
//            begin
//            	raise Exception.Create(Format('Error al actualizar la generaci�n del Contrato PAK. C�digo Mensaje: %d', [CodigoMensaje]));
//            end;
//          end;
//        finally
//        	if Assigned(spEMAIL_ActualizarContratoAdhesionPAKAImprimir) then FreeAndNil(spEMAIL_ActualizarContratoAdhesionPAKAImprimir);
//        end;
//
//
//        Inc(ReportesProcesados);
//        if ReportesProcesados = 5 then
//        begin
//          LiberarMemoria;
//          ReportesProcesados := 0;
//        end;
//
//      	spReporte.Next;
//      end;


      Result := True;
    except
      on e: Exception do
      begin
          LogAction(Format(MSG_GET_REPORT_JOB_ERROR, [TipoComprobante, NumeroComprobante, e.Message]));
          EventLogReportEvent(elError, Format(MSG_GET_REPORT_JOB_ERROR, [TipoComprobante, NumeroComprobante, e.Message]), '');
      end;
    end;
end;


procedure TfrmMainForm.ServiceAfterInstall(Sender: TObject);
begin
    EventLogRegisterSource;
end;


procedure TfrmMainForm.ServiceAfterUninstall(Sender: TObject);
begin
    EventLogUnregisterSource;
end;


procedure TfrmMainForm.ServiceStart(Sender: TObject; var Started: Boolean;
  var ExitCode, SpecificExitCode: Cardinal);
//Rev.5 / 07-Junio-2010 / Nelson Droguett Sierra-----------------------------------
resourcestring
    MSG_SERVICE_SUCCESS_STARTED = 'Servicio iniciado con �xito';
//FinRev.5-------------------------------------------------------------------------
begin
    MoveToMyOwnDir;
    tmrReporte.Enabled := True;
    Started := True;
    // Rev.2 / 22-Febrero-2010 / Nelson Droguett Sierra -------------------------------
    bDebeEscribirLog:=InstallIni.ReadInteger('LogFile', 'LogToFile', 1) = 1;
    AssignFile( LogFile , 'GeneracionPDF.Log');
    if not FileExists('GeneracionPDF.Log') then ReWrite(LogFile);
    Append(LogFile);
    //---------------------------------------------------------------------------------
    //Rev.5 / 07-Junio-2010 / Nelson Droguett Sierra-----------------------------------
    LogAction(MSG_SERVICE_SUCCESS_STARTED);
	EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_STARTED , '');
    //FinRev.5-------------------------------------------------------------------------
end;

procedure TfrmMainForm.ServiceStop(Sender: TObject);
//Rev.5 / 07-Junio-2010 / Nelson Droguett Sierra-----------------------------------
resourcestring
    MSG_SERVICE_SUCCESS_STOPED = 'Servicio detenido con �xito';
//FinRev.5-------------------------------------------------------------------------
begin
    //Rev.5 / 07-Junio-2010 / Nelson Droguett Sierra-----------------------------------
    LogAction(MSG_SERVICE_SUCCESS_STOPED);
    CloseFile(LogFile);
	EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_STOPED , '');
    //FinRev.5-------------------------------------------------------------------------
    // Rev.2 / 22-Febrero-2010 / Nelson Droguett Sierra -------------------------------

    //---------------------------------------------------------------------------------
end;

function TfrmMainForm.SumarReintento(const CodigoMensaje: Int64): Boolean;
resourcestring
    MSG_REP_UPDATE_RETRY_ERROR = 'Ha ocurrido un error actualizando los reintentos para el e-mail de c�digo %d. Detalle: %s';
const
    SQL_R = 'UPDATE EMAIL_COMPROBANTES SET REINTENTOS = REINTENTOS + 1 WHERE CODIGOMENSAJE = %d';
begin
    Result := False;
    try
        QueryExecute(DMConnections.BaseCAC, Format(SQL_R, [CodigoMensaje]));
        Result := True;
    except
        on e: Exception do begin
            LogAction(Format(MSG_REP_UPDATE_RETRY_ERROR, [CodigoMensaje, e.Message]));;
            EventLogReportEvent(elError, Format(MSG_REP_UPDATE_RETRY_ERROR, [CodigoMensaje, e.Message]), '');
        end;
    end;
end;


procedure TfrmMainForm.tmrReporteTimer(Sender: TObject);
begin
    LogAction('Inicio de la generaci�n de PDF');
    tmrReporte.Enabled  := False;
    try
        if not ConectarBase         then Exit;
        if not ConfigurarParametros then Exit;
        if Fintervalo <> 0          then tmrReporte.Interval := FIntervalo * 1000;
        if not ProcesarReportes()   then LogAction('ProcesarReportes retorn� Falso');                 //SS_1307_NDR_20150804
        //if not DesconectarBase      then Exit;                                                      //SS_1307_NDR_20150804
    finally
        tmrReporte.Enabled := True;
    end;
end;


procedure TfrmMainForm.LogAction(Mensaje:string);
var
    HoraYMensaje : string;
begin
    // Rev.2 / 22-Febrero-2010 / Nelson Droguett Sierra.-------------------------------
    // Solo se pregunta si escribe o no el Log, consultando una variable, que toma
    // valor en el inicio del servicio.
    if bDebeEscribirLog then begin
      HoraYMensaje := DateTimeToStr(now) + Mensaje ;
      WriteLn( LogFile , Pchar(HoraYMensaje));
    end;
end;

// Inicio Bloque SS_1034_PDO_20120419
procedure TfrmMainForm.LiberarMemoria;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
end;
// Fin Bloque SS_1034_PDO_20120419

function TfrmMainForm.GenerarContratoPAK(pCodigoPersona: Integer; pNombreCliente, pNumeroDocumento, pEMail: string; pFechaContrato: TDateTime; pAdheridoEnvioEMail: Boolean; var pNombreArchivo: string): Boolean;
	var
        ReporteContratoAdhesionPAForm: TReporteContratoAdhesionPAForm;
        NombreArchivo: string;
        spValidarRestriccionesAdhesionPAKAnexo: TADOStoredProc;			// SS_1006_PDO_20121122
        SituacionesARegularizar: AnsiString;							// SS_1006_PDO_20121122
begin
	try
    	Result := False;

    	try
    		NombreArchivo := Format(FDirectorio + FILE_NAME_TEMPLATE_PAK, [pNumeroDocumento, FormatDateTime('ddmmyyyyhhnnss', Now)]);

        	// SS_1006_PDO_20121122 Inicio Bloque
        	spValidarRestriccionesAdhesionPAKAnexo := TADOStoredProc.Create(nil);
            with spValidarRestriccionesAdhesionPAKAnexo do begin
                Connection := DMConnections.BaseCAC;
                ProcedureName := 'ValidarRestriccionesAdhesionPAKAnexo';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := pCodigoPersona;
                Parameters.ParamByName('@DevolverValidaciones').Value := 1;
                Open;

                if RecordCount > 0 then begin
                	while not Eof do begin

                        if SituacionesARegularizar <> EmptyStr then begin
                        	SituacionesARegularizar := SituacionesARegularizar + '\par \par ';
                        end;

                        SituacionesARegularizar := SituacionesARegularizar + ' \tab ' + '- ' + AnsiUpperCase(FieldByName('Descripcion').AsString);

                        Next;
                    end;
                end;
            end;
            // SS_1006_PDO_20121122 Fin Bloque


			ReporteContratoAdhesionPAForm := TReporteContratoAdhesionPAForm.Create(nil);

            with ReporteContratoAdhesionPAForm do  begin
                Inicializar(
                	pNombreCliente,
                    pNombreCliente,
                    pNumeroDocumento,
                    pEMail,
                    pFechaContrato,
                    pAdheridoEnvioEMail,
                    False,
                    True,
                    SituacionesARegularizar);

                ImprimirPDF(NombreArchivo);
            end;

            if not FileExists(NombreArchivo) then begin
            	raise Exception.Create(Format('Error al generar el contrato PAK. Contrato: %s', [NombreArchivo]));
            end;

            pNombreArchivo := ExtractFileName(NombreArchivo);
            Result := True;
        except
        	on e: Exception do begin
                raise Exception.Create(e.Message);
            end;
        end;
    finally
    	if Assigned(ReporteContratoAdhesionPAForm) then FreeAndNil(ReporteContratoAdhesionPAForm);
        // SS_1006_PDO_20121122 Inicio Bloque
        if Assigned(spValidarRestriccionesAdhesionPAKAnexo) then begin
        	if spValidarRestriccionesAdhesionPAKAnexo.Active then spValidarRestriccionesAdhesionPAKAnexo.Close;
            FreeAndNil(spValidarRestriccionesAdhesionPAKAnexo);
        end;
        // SS_1006_PDO_20121122 Fin BLoque        
    end;
end;


//-------------------------------------------------------------------------------------------------------------------
//BEGIN : SS_1307_NDR_20150804 --------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
function TfrmMainForm.DesconectarBase: Boolean;
resourcestring
    MSG_DISCONNECT_ERROR = 'Ha ocurrido un error desconectando la base de datos. Detalle: %s';
begin
    Result := False;
    try
        if DMConnections.BaseCAC.Connected then
        begin
            DMConnections.BaseCAC.Connected := False;
        end;
        Result := True;
    except
        on e: Exception do begin
            LogAction(Format(MSG_DISCONNECT_ERROR, [e.Message]));
            EventLogReportEvent(elError, Format(MSG_DISCONNECT_ERROR, [e.Message]), '');
        end;
    end;
end;
//-------------------------------------------------------------------------------------------------------------------
//END : SS_1307_NDR_20150804 --------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------


end.


