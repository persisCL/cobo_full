{********************************** File Header ********************************
Author      : Nelson Droguett Sierra
Date        : 10-Febrero-2010
Description : Servicio de Envio de Archivos Adjuntos, de comprobantes y de detalle
                de comprobantes.

Revision  :2
Author      : Nelson Droguett Sierra
Date        : 22-Febrero-2010
Description : Se modifica la carga del flag de LOG, para que solo lea el .INI al
              momento de iniciar el servicio y no cada vez que se quiera escribir
              en el log, esto para disminuir el acceso a la unidad de red donde se
             guarda el LOG.
             Se agrega ademas el evento ServiceStop , para realizar tareas en la
             detencion del servicio. (Cerrar el archivo de log)

Revision  	: 4
Author      : Nelson Droguett Sierra
Date        : 28-Abril-2010
Description : Informar en el EVENT VIEWER no solo los errores sino tambien los
                exitos (ej, cuando parte el servicio)

Revision	: 5
Author		: Nelson Droguett Sierra
Date		: 07-Junio-2010
Description	: Se agrega registro en el log de eventos cada vez que se inicia y
            	se detiene el servicio por parte del usuario.
            	Se elimina la funcion SetearReintentosMaximos, ya que no se usa.
                Se registra en el visor de eventos el inicio del servicio cuando
                se ejecuta el evento ServiceStart

Revision	: 6
Author		: Nelson Droguett Sierra
Date		: 09-Junio-2010
Description	: No adjuntar si es vacio , contenttype en el mensaje.

Revision	: 7
Author		: Nelson Droguett Sierra
Date		: 17-Agosto-2010
Description	: (Ref. SS-601 , SS-715)
              En las plantillas se deben poder usar parametros que son reemplazados
              en el cuerpo del mensaje
              Los correos pueden o no contener un archivo adejunto, esto se define
              por un Bit que se encuentra en la plantilla
              La plantilla puede ser texto plano o HTML, esto lo define un campo
              en la plantilla
              La plantilla puede usar o no una firma, esto lo define un campo
              en la plantilla
              Hay correos que se encuentran en la tabla EMAIL_Mensajes, que estan
              asociados a un convenio y no a un comprobante, el servicio tambien
              deberia despachar estos mensajes.
Revision	: 8
Author		: Nelson Droguett Sierra
Date		: 22-Septiembre-2010
Description	: (Ref. SS-601 , SS-715)
              Se separa el contenido de la plantilla de la firma de la plantilla
Firma		: SS-601-NDR-20100922

Revision	: 9
Author		: Nelson Droguett Sierra
Date		: 11-Noviembre-2010
Description	: (Ref. SS-601)
              Se corrige la grabacion de reintentos, ya que siempre lo hacia en
              la tabla de Email_Comprobantes y nunca en la tabla Email_Mensajes.
              Los reintentos se incrementaran en la tabla que corresponde.
              Se agrega el parametro Tabla:string a la funcion SumarReintento
              Escribir en el EventViewer la instalacion, inicio, parad ay desisntalacion
              del servicio. Ademas de los errores. Los exitos en el env�o no.
Firma		: SS-601-NDR-20101111

Revision	: 10
Author		: Nelson Droguett Sierra
Date		: 16-Noviembre-2010
Description	: Se registra en el visor de eventos los errores que caen en el Except
Firma		: SS-601-NDR-20101116


Firma       : SS_1075_MCO_20130131
Author      : Manuel Cabello O.
Date        : 31-Enero-2013
Description : Se agrega Campo Email Sender y Clave Sender definido en la plantilla. Si el FSender <> EmailSender, se
             realiza una nueva conexion SMTP para enviar el email.


Firma		:	SS_1274_MBE_20150518
Description	:	Se quita el "T�tulo" del Sender, pues dice "Costanera Norte"

    Firma       :   SS_1354_MBE_20150731
    Description :   Se agrega que devuelva el codigo de plantilla, para descubrir
					los errores de 'String de parametros malformados' (Comunes\MensajesEmail.pas)

Firma		: CAC-CLI-066_MGO_20160218
Description	: Se agrega proceso de env�o de correspondencia desde la tabla EMAIL_Correspondencia

Etiqueta    : 20160530 MGO
Descripci�n : Se agrega el par�metro @CodigoMensaje vac�o
              Se modifica el procesamiento de encabezado

Etiqueta    : 20160609 MGO
Descripci�n : Se elimina la Base espec�fica de dmMensajes
              Se elimina c�digo comentado antiguo
*******************************************************************************}
unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  IdMessage, IdMessageParts, IdBaseComponent, IdComponent, IdTCPConnection,
  IdMessageClient, IdSMTPBase,
  IdSMTP, DpsServ, DMConnection, UtilProc, Util, EventLog, ExtCtrls, ADODB,
  DB, IdAttachmentFile, PeaTypes,
  ConstParametrosGenerales, UtilDB, IdTCPClient, UtilRB, RBSetUp,
  MensajesEmail,StrUtils, Dialogs, IdText,
  dmMensaje,
  IdSSLOpenSSL, IdGlobal, IdExplicitTLSClientServerBase;
type
  TfrmMainForm = class(TForm)
    Service: TDPSService;
    tmrReporte: TTimer;
    spMails: TADOStoredProc;
    spObtenerDatosPersona: TADOStoredProc;
    spObtenerDomicilioConvenio: TADOStoredProc;
    procedure tmrReporteTimer(Sender: TObject);
    procedure ServiceStart(Sender: TObject; var Started: Boolean; var ExitCode,
      SpecificExitCode: Cardinal);
    procedure ServiceAfterInstall(Sender: TObject);
    procedure ServiceAfterUninstall(Sender: TObject);
    procedure ServiceStop(Sender: TObject);
  private
    FSMTP:TIdSMTP;
    FMsg: TIdMessage;
    SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
    FServiceName: String;
    FIntervalo:  Integer;
    FInstancia: Integer;
    FTipoInstancia: Integer;
    FSender: string;
    FSMTPServer: string;
    FSMTPPort: Integer; // 20160609 MGO
    FUsuarioSMTP: string;
    FClaveSMTP: string;
    FBorrar : integer;
    FDirectorio : String;
    FMensajeActual: Integer;
    FMaxReintentos: Integer;
    FArchivoActual : AnsiString;
    bDebeEscribirLog : Boolean;
    LogFile: TextFile;
    function ConectarBase: Boolean;
    function ConfigurarParametros: Boolean;
    function ProcesarMensajes: Boolean;
    function EnviarMensaje(const NombreArchivo, Email, Firma, Asunto, Plantilla, ContentType, EmailSender, ClaveSender, NameSender: AnsiString): Boolean;
    function ActualizarMensajeEnviado(const CodigoMensaje: Int64; Tabla:string ):Boolean;
    function SumarReintento(const CodigoMensaje: Int64; Tabla:string ):Boolean;

    procedure LogAction(Mensaje: string);
    function CreateMessage:TIdMessage;
    function CreateSMTP: TIdSMTP;
    function CreateSMTPNewSender(const EmailSender, ClaveSender: AnsiString ): TIdSMTP;
    procedure DoOnFailedSend (Sender: TObject; const AAddress, ACode, AText: string; var VContinue: Boolean);
    function EnviarMensajes: Boolean;
    function EnviarCorrespondencia: Boolean;
    procedure LiberarMemoria;
  public
    function Inicializar: Boolean;
    procedure BorrarAdjunto;
  end;

const
    // Formato de nombre de archivo a generar para cualquier reporte a disco
    // El formato es: (Dir de ejecuci�n del servicio) + TipoComprobante.NumeroComprobante.FechaHoraCreaci�n.pdf'
    FILE_NAME_TEMPLATE =  '%s.%d.%s.pdf';

    // C�digos de falla del Servidor SMTP son todos aquellos a partir de 500

    SMTP_ERR_421 = 421; // <domain> Service not available, closing transmission channel
    SMTP_ERR_450 = 450; //Requested mail action not taken: mailbox unavailable  [Irrecuperable]
    SMTP_ERR_451 = 451; //RRequested action aborted: local error in processing  [Irrecuperable]
    SMTP_ERR_452 = 452; //Requested action not taken: insufficient system storage [Irrecuperable]
    SMTP_ERR_521 = 521; //<domain> does not accept mail (see rfc1846) [Irrecuperable]

    SMTP_ERR_500 = 500; //Syntax error, command unrecognized. Also command line too long  [Irrecuperable]
    SMTP_ERR_501 = 501; //Syntax error in paramaters or arguements  [Irrecuperable]
    SMTP_ERR_502 = 502; //Command not implemented [Irrecuperable]
    SMTP_ERR_503 = 503; //Bad sequence of commands [Irrecuperable]
    SMTP_ERR_504 = 504; //Command parameters not implemented      [Irrecuperable]
    SMTP_ERR_550 = 550; //Action not taken. Mailbox unavailable. Not found, not accessible [Irrecuperable]
    SMTP_ERR_551 = 551; //User not local, please try
    SMTP_ERR_552 = 552; //Exceeded storage allocation
    SMTP_ERR_553 = 553; //Mailbox name not allowed. Mailbox syntax may be incorrect [Irrecuperable]
    SMTP_ERR_554 = 554; //Transaction failed
    { INICIO : 20160609 MGO
    SMTP_PORT = 25; // 25 TCP no configurable
    } // FIN : 20160609 MGO
var
  frmMainform: TfrmMainForm;

implementation

{$R *.dfm}

{------------------------------------------------------------------------
                BorrarAdjunto

Author  : ndroguett
Date:
Description : Intenta borrar un archivo desde la carpeta indicada

-------------------------------------------------------------------------}
procedure TfrmMainForm.BorrarAdjunto;
    resourcestring
        MSG_ERROR_BORRAR_ARCHIVO = 'Error al intentar eliminar el archivo: %s - Detalle : %s';
    var
        lVeces: Integer;
        NombreArchivoAdjunto: string;
        CaracterLeido: string[01];

    procedure EliminarAdjunto(pNombreArchivo: string);
    begin
        if not DeleteFile(FDirectorio + pNombreArchivo) then begin
            LogAction('ProcesarMensajes No pudo borrar archivo ' + FDirectorio + pNombreArchivo);
            EventLogReportEvent(elError, format(MSG_ERROR_BORRAR_ARCHIVO, [pNombreArchivo]), '');
        end else begin
	        LogAction('Archivo eliminado de disco exitosamente. Archivo: ' + pNombreArchivo);
        end;
    end;
begin
    try
		if Pos(';', FArchivoActual) = 0 then begin
        	EliminarAdjunto(FArchivoActual);
        end
        else begin
        	NombreArchivoAdjunto := EmptyStr;

            for lVeces := 1 to Length(FArchivoActual) do begin
                CaracterLeido := FArchivoActual[lVeces];

                if CaracterLeido = ';' then begin
                	EliminarAdjunto(NombreArchivoAdjunto);
                    NombreArchivoAdjunto := EmptyStr;
                end
                else begin
                    NombreArchivoAdjunto := NombreArchivoAdjunto + CaracterLeido;
                end;
            end;

            if NombreArchivoAdjunto <> EmptyStr then begin
               	EliminarAdjunto(NombreArchivoAdjunto);
            end;
        end;
    except                                                                                                                        
        on e: Exception do begin                                                                                                  
            EventLogReportEvent(elError, Format(MSG_ERROR_BORRAR_ARCHIVO, [FDirectorio + FArchivoActual,e.Message]), '');         
            LogAction(Format(MSG_ERROR_BORRAR_ARCHIVO, [FDirectorio + FArchivoActual,e.Message]));                                
        end;                                                                                                                      
    end;
end;


{------------------------------------------------------------------------
                ActualizarMensajeEnviado

Author  : ndroguett
Date:
Description : Actualiza el mensaje para indicar que est� enviado

-------------------------------------------------------------------------}
function TfrmMainForm.ActualizarMensajeEnviado( const CodigoMensaje: Int64; Tabla:string): Boolean;
resourcestring
    MSG_REP_UPDATE_SEND_ERROR = 'Ha ocurrido un error marcando el e-mail de c�digo %d como enviado. Detalle: %s';
const
    SQL_E = 'UPDATE %s SET Enviado = 1, %s = GETDATE() WHERE CodigoMensaje = %d';
begin
    Result := False;
    try
        QueryExecute(DMConnections.BaseCAC, Format(SQL_E, [Tabla, Util.iif((UpperCase(Tabla)='EMAIL_COMPROBANTES') or (UpperCase(Tabla)='EMAIL_CONTRATOADHESIONPAK'),'FechaHoraEnvio','FechaEnvio'),CodigoMensaje]));	// SS_1006_20120812
        Result := True;
    except
        on e: Exception do begin
            EventLogReportEvent(elError, Format(MSG_REP_UPDATE_SEND_ERROR, [CodigoMensaje, e.Message]), '');
            LogAction(Format(MSG_REP_UPDATE_SEND_ERROR, [CodigoMensaje, e.Message]));
        end;
    end;
end;

{------------------------------------------------------------------------
                ConectarBase

Author  : ndroguett
Date:
Description :   Devuelve True si pudo conectarse a la base de datos.
                False en caso contrario

-------------------------------------------------------------------------}
function TfrmMainForm.ConectarBase: Boolean;
resourcestring
    MSG_CONNECT_ERROR = 'Ha ocurrido un error conectando a la base de datos. Detalle: %s';
begin
    Result := False;
    try
        if not DMConnections.BaseCAC.Connected then begin
            DMConnections.SetUp;
            DMConnections.BaseCAC.Connected := True;
            { INICIO : 20160609 MGO
            //Copia el String de conexion al dataModule de los mensajes.
            if not dmMensajes.Base.Connected then
            begin
              dmMensajes.Base.ConnectionString :=DMConnections.BaseCAC.ConnectionString;
              dmMensajes.Base.KeepConnection:=True;
              dmMensajes.Base.Connected:=True;
            end;
            } // FIN : 20160609 MGO
        end;
        Result := True;
    except
        on e: Exception do begin
            EventLogReportEvent(elError, Format(MSG_CONNECT_ERROR, [e.Message]), '');
            LogAction(Format(MSG_CONNECT_ERROR, [e.Message]));
        end;
    end;
end;

{------------------------------------------------------------------------
                ConfigurarParametros

Author  : ndroguett
Date:
Description :   Obtiene el valor de cada uno de los par�metros de la aplicaci�n.
-------------------------------------------------------------------------}
function TfrmMainForm.ConfigurarParametros: Boolean;
begin
	try                                                                                                                                            //SS-601-NDR-20101116
      Result := False;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_INTERVALOSEGUNDOS', FIntervalo) then Exit;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_SENDER', FSender) then Exit;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_SERVIDORSMTP', FSMTPServer) then Exit;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_USER', FUsuarioSMTP) then Exit;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_USER_PASS', FClaveSMTP) then Exit;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_ELIMINAR_ADJUNTO', FBorrar) then Exit;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_EMAILS_ADJUNTOS', FDirectorio) then Exit;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_CANTIDAD_REINTENTOS', FMaxReintentos) then Exit; 
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_SERVIDORSMTP_PUERTO', FSMTPPort) then Exit;  // 20160609 MGO
      if FDirectorio = ''  then FDirectorio := '.';
      FDirectorio := GoodDir(FDirectorio);
      if not (DirectoryExists(FDirectorio)) then begin
          EventLogReportEvent(elError, 'El Directorio ' + FDirectorio + ' que se va a utlizar para guardar los pdf no se encuentra o no est� accesible', '');
          LogAction('El Directorio ' + FDirectorio + ' que se va a utlizar para guardar los pdf no se encuentra o no est� accesible');
          Exit;
      end;

        bDebeEscribirLog:=InstallIni.ReadInteger('LogFile', 'LogToFile', 1) = 1;

      Result := True;
    except                                                                                                                                         
        on e: Exception do begin                                                                                                                   
            EventLogReportEvent(elError, Format('El Directorio ' +                                                                                 
                                                FDirectorio +                                                                                      
                                                ' que se va a utlizar para guardar los pdf no se encuentra o no est� accesible. Detalle : $s',    
                                                [e.Message]), '');                                                                                 
            LogAction(Format('El Directorio ' +                                                                                                    
                             FDirectorio +                                                                                                         
                             ' que se va a utlizar para guardar los pdf no se encuentra o no est� accesible.  Detalle : $s',                      
                             [e.Message]));                                                                                                        
        end;                                                                                                                                       
    end;
end;

{------------------------------------------------------------------------
                CreateMessage

Author  : ndroguett
Date:
Description :   Prepara un archivo para ser enviado.
-------------------------------------------------------------------------}
function TfrmMainForm.CreateMessage: TIdMessage;
resourcestring
    MSG_CREATEMESSAGE_ERROR = 'Ha ocurrido un error creando el mensaje de correo a ser enviado';
begin
	try
      Result := TIdMessage.Create(nil);
      Result.Name := 'Msg';
      Result.AttachmentEncoding := 'UUE';
      Result.Encoding := meDefault;
      Result.ConvertPreamble := True;
      Result.ContentType := 'multipart/mixed';
    except                                                                                                                                       
        on e: Exception do begin                                                                                                                 
            EventLogReportEvent(elError, Format(MSG_CREATEMESSAGE_ERROR+'. Detalle : $s', [e.Message]), '');                                     
            LogAction(Format(MSG_CREATEMESSAGE_ERROR+'. Detalle : $s', [e.Message]));                                                            
        end;                                                                                                                                     
    end;                                                                                                                                         
end;

{------------------------------------------------------------------------
                CreateSMTP

Author  : ndroguett
Date:
Description :   Crea el objeto TidSMTP y deriva su evento a una funci�n.

-------------------------------------------------------------------------}
function TfrmMainForm.CreateSMTP: TIdSMTP;
begin
    LogAction('Creando Conexi�n SMTP ...');                
     Result := TIdSMTP.Create(Self);

    with Result do begin
        OnFailedRecipient := DoOnFailedSend;
        { INICIO : 20160609 MGO
        Port              := SMTP_PORT;
        }
        Port              := FSMTPPort;
        // FIN : 20160609 MGO
        Host              := FSMTPServer;
        Username          := FUsuarioSMTP;                  
        Password          := FClaveSMTP;                    
        LogAction('Conectando ...');                        
        Connect;                                            
        LogAction('Conectado al SMTP ' + FSMTPServer);      
    end;
end;

{------------------------------------------------------------------------
                CreateSMTP

Author  : mcabello
Date: 31-01-2013
Description :   Crea el objeto TidSMTP apartir del nuevo sender de la plantilla

-------------------------------------------------------------------------}
function TfrmMainForm.CreateSMTPNewSender(const EmailSender, ClaveSender: AnsiString ) : TIdSMTP;
begin
    LogAction('Creando Conexi�n SMTP ...');
     Result := TIdSMTP.Create(Self);
    SSLHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);

    with Result do begin

        SSLHandler.MaxLineAction := maException;
        SSLHandler.SSLOptions.Method := sslvTLSv1;
        SSLHandler.SSLOptions.Mode := sslmUnassigned;
        SSLHandler.SSLOptions.VerifyMode := [];
        SSLHandler.SSLOptions.VerifyDepth := 0;

        Result.IOHandler := SSLHandler;

        OnFailedRecipient := DoOnFailedSend;
        { INICIO : 20160609 MGO
        Port              := SMTP_PORT;
        }
        Port              := FSMTPPort;
        // FIN : 20160609 MGO
        Host              := FSMTPServer;
        Username          := EmailSender;
        Password          := ClaveSender;
        UseTLS            := utUseExplicitTLS;

        LogAction('Conectando ...');
        Connect;
        Authenticate;
        LogAction('Conectado al SMTP ' + FSMTPServer);    
    end;
end;


{------------------------------------------------------------------------
                DoOnFailedSend

Author  : ndroguett
Date:
Description :   Funci�n que se gatilla si el evento de envi Falla.

-------------------------------------------------------------------------}
procedure TfrmMainForm.DoOnFailedSend(Sender: TObject; const AAddress, ACode,
  AText: string; var VContinue: Boolean);
resourcestring
	MSG_SQL = 'INSERT INTO AlarmasEventos (FechaHora, Modulo, Categoria, Detalle) VALUES (GETDATE(), ''MailerDocumentos'', 2, ''%s'')';

const
    MSG_ERROR_TEXT_LOST = 'El servidor SMTP devolvi� el c�digo de error irrecuperable %s indicando la raz�n %s enviando el e-mail a %s';
    MSG_ERROR_TEXT_RECO = 'El servidor SMTP devolvi� el c�digo de error recuperable %s indicando la raz�n %s enviando el e-mail a %s';
var
    Mensaje : string;
begin
    LogAction('*** Error SMTP al enviar e-mail, se seguir� intentando enviarlo hasta el m�ximo de reintentos:' + Format(MSG_ERROR_TEXT_RECO, [Acode, AText, AAddress]));
    Mensaje := Format(MSG_ERROR_TEXT_RECO, [Acode, AText, AAddress]);
    EventLogReportEvent(elError, Mensaje, '');
    VContinue := False;
    try
    	QueryExecute(DMConnections.BaseCAC, Format(MSG_SQL, [Mensaje]));
    finally
      VContinue := False;
    end;
end;


{------------------------------------------------------------------------
                EnviarMensaje

Author  : ndroguett
Date:
Description :   Envia el correo por E-Mail agregando los adjuntos definidos=

-------------------------------------------------------------------------}
function TfrmMainForm.EnviarMensaje(const NombreArchivo, Email, Firma, Asunto,
  Plantilla, ContentType, EmailSender, ClaveSender, NameSender: AnsiString ): Boolean;
resourcestring
    MSG_SEND_ERROR = 'Ha ocurrido un error enviando un e-mail al destinatario %s .Detalle del error: %s';
var
    htmpart:TIdText;

    lVeces: Integer;
    NombreArchivoAdjunto: string;
    CaracterLeido: string[01];
const

    WORK_DIR = '.\'; // Directorio de ejecuci�n
    CONCESIONARIA_TILE = '';


	procedure AdjuntarArchivo(pArchivo: string);
    	var
		    Adjunto: TIdAttachmentFile;
    begin
        if not FileExists(FDirectorio + pArchivo) then begin
          LogAction('El archivo ' + FDirectorio + pArchivo + ' No existe al momento de enviarse!');
          raise Exception.Create('El archivo ' + FDirectorio + pArchivo + ' No existe al momento de enviarse!' );
        end;

        Adjunto := TIdAttachmentFile.Create(FMsg.MessageParts, FDirectorio + pArchivo);
        Adjunto.DisplayName := pArchivo;
    end;

begin
    Result := False;
    LogAction('Creando Componente de Env�o de e-mail y asignando valores');
    if not Assigned(FMsg) then FMsg := CreateMessage;


    // Armamos el mensaje
    FMsg.Clear;
    // Asunto (Subject) de este mensaje
    FMsg.Subject := Asunto;
    // Destinatario de este mensaje, s�lo una direcci�n de correo
    FMsg.Recipients.EMailAddresses := Email;


    // Texto que ver� el receptor con la firma agregada

	if LeftStr(Trim(Plantilla),9)='<!DOCTYPE' then
    begin
        LogAction('Reemplazando Firma');
	    FMsg.Body.Text := AnsiReplaceStr( AnsiReplaceStr( Plantilla,
                                                          '</BODY>',
                                                          Firma+'</BODY>'
                                                        ),
                                          '</body>',
                                          Firma+'</body>'
                                        );
      htmpart := TIdText.Create(FMsg.MessageParts, FMsg.Body);
      htmpart.ContentType := ContentType;
	  FMsg.Body.Text:='';
    end
    else
    begin
        LogAction('Asignando Firma');
      if ContentType='text/plain' then
         FMsg.Body.Text:= Plantilla  +  Firma
      else if ContentType='text/html' then
         FMsg.Body.Text:= Plantilla  + '<br><br>' + Firma;



      //FMsg.ContentType := ContentType;
      htmpart := TIdText.Create(FMsg.MessageParts, FMsg.Body);
      htmpart.ContentType := ContentType;
	  FMsg.Body.Text:='';
    end;

    if Assigned(FSMTP) then begin
        if (FSMTP.Username <> EmailSender) or (FSMTP.Password <> ClaveSender) then begin

            FreeAndNil(FSMTP);
            FSMTP:= CreateSMTPNewSender(EmailSender, ClaveSender);
        end;
    end
    else begin

        FSMTP:= CreateSMTPNewSender(EmailSender, ClaveSender);
    end;


    // Direcci�n de e-mail de qui�n env�a este Mensaje. Es obligatoria
    if EmailSender = '' then begin
        FMsg.Sender.Address := FSender;
  	    FMsg.From.Address := FSender;
    end
    else begin

        FMsg.Sender.Address := EmailSender;                                     
        FMsg.From.Address := EmailSender;                                       
    end;

    FMsg.Sender.Name := NameSender;
    FMsg.From.Name := NameSender;
    //Preparamos el adjunto que se enviar� al destinatario
    // Enviamos el e-mail
    try
        try
            if  Trim(NombreArchivo)<>'' then begin
                if Pos(';',NombreArchivo) = 0 then begin
                    FMsg.AttachmentTempDirectory := GoodDir(FDirectorio);

                    AdjuntarArchivo(NombreArchivo);
                end
                else begin
                    NombreArchivoAdjunto := EmptyStr;
                    FMsg.AttachmentTempDirectory := GoodDir(FDirectorio);

                    for lVeces := 1 to Length(NombreArchivo) do begin
                        CaracterLeido := NombreArchivo[lVeces];

                        if CaracterLeido = ';' then begin
		                    AdjuntarArchivo(NombreArchivoAdjunto);
                            NombreArchivoAdjunto := EmptyStr;
                        end
                        else begin
                            NombreArchivoAdjunto := NombreArchivoAdjunto + CaracterLeido;
                        end;
                    end;

                    if NombreArchivoAdjunto <> EmptyStr then begin
                    	AdjuntarArchivo(NombreArchivoAdjunto);
                    end;
                end;
            end;


            if not FSMTP.Connected then begin
                LogAction('Conectando...');
                FSMTP.Connect;
                LogAction('Autenticando...');
                FSMTP.Authenticate;
                LogAction('Conectado al SMTP ' + FSMTPServer);
            end;
            LogAction('Enviando ...');

            FSMTP.Send(FMsg);
            LogAction('Enviado.');
            // Listo!
            Result := True;
        except
            on e: Exception do begin
                LogAction(Format(MSG_SEND_ERROR, [Email, e.Message]));
                EventLogReportEvent(elError, Format(MSG_SEND_ERROR, [Email, e.Message]), EmptyStr);

                 if Assigned(FSMTP) then FreeAndNil(FSMTP);
            end;
        end;
    finally
        if Assigned(FMsg) then begin
            LogAction('Destruyendo Componente Mensaje.');
            if Assigned(FMsg) then FreeAndNil(FMsg);
        end;
    end;
end;

{------------------------------------------------------------------------
                Inicializar

Author  : ndroguett
Date:
Description :   Inicializa la ventana. Permite la instalaci�n/desintalaci�n
                de la aplicaci�n como servicio.

-------------------------------------------------------------------------}
function TfrmMainForm.Inicializar: Boolean;
resourcestring
  MSG_INSTALL = 'Instalar';
  MSG_UNINSTALL = 'Desinstalar';
  MSG_SERVICE_SUCCESS_INSTALLING = 'Servicio instalado con �xito';
  MSG_SERVICE_ERROR_INSTALLING = 'Error al instalar el Servicio %s:' + CRLF + '%s';
  MSG_SERVICE_SUCCESS_UNINSTALLING = 'Servicio desinstalado con �xito';
  MSG_SERVICE_ERROR_UNINSTALLING = 'Error al desinstalar el Servicio %s:' + CRLF + '%s';
  MSG_SERVICE_CANNOT_START = 'El servicio no pudo iniciarse debido al siguiente Error: %s';
var
	ErrorCode: DWORD;
begin
    DMConnections.SetUp;
    FServiceName := Service.ServiceName + '_' + IntToStr(FInstancia);
    spMails.Connection := DMConnections.BaseCAC;
    spMails.ProcedureName := 'EMAIL_ObtenerComprobantesAEnviar';
	Result := False;
	if FindCmdLineSwitch('install', ['/', '-'], True) then begin
		// Instalaci�n
		if InstallServices(ErrorCode, FServiceName) then begin
			MsgBox(MSG_SERVICE_SUCCESS_INSTALLING, MSG_INSTALL, MB_ICONINFORMATION);
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_INSTALLING , '');
		end else begin
			MsgBox(Format(MSG_SERVICE_ERROR_INSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]),
			  MSG_INSTALL, MB_ICONSTOP);
            EventLogReportEvent(elError, Format(MSG_SERVICE_ERROR_INSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]) , '');
		end;
	end else if FindCmdLineSwitch('uninstall', ['/', '-'], True) then begin
		// Desinstalaci�n
		if UninstallServices(ErrorCode, FServiceName) then begin
			MsgBox(MSG_SERVICE_SUCCESS_UNINSTALLING, MSG_UNINSTALL, MB_ICONINFORMATION);
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_UNINSTALLING , '');
		end else begin
			MsgBox(Format(MSG_SERVICE_ERROR_UNINSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]),
			  MSG_UNINSTALL, MB_ICONSTOP);
            EventLogReportEvent(elError, Format(MSG_SERVICE_ERROR_UNINSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]) , '');
		end;
	end else begin
		// Ejecuci�n
		Result := StartServices;
		if Result then begin
			if not IsServiceApplication then Show;
		end else begin
			EventLogReportEvent(elError,
              Format(MSG_SERVICE_CANNOT_START, [SysErrorMessage(GetLastError())]), '');
		end;
    end;
end;


{------------------------------------------------------------------------
                ProcesarMensajes

Author  : ndroguett
Date:
Description :   Env�a los mensajes que tenga para enviar.

-------------------------------------------------------------------------}
function TfrmMainForm.ProcesarMensajes: Boolean;
resourcestring
    MSG_GET_MESSAGES_ERROR = 'Ha ocurrido un error obteniendo mensajes a enviar. Detalle: %s';
var
    CodigoMensaje: Int64;
    Email, Firma, Asunto, Plantilla, ContentType: String;
    Reintentos : smallint;
    Parametros, Mensaje: String;
    EliminarArchivo: Boolean;
    EMailsEnviados: Integer;
    EmailSender: string;
    NameSender: string;
    ClaveSender: AnsiString;            
    CodigoPlantilla : integer;          
begin
    Result := False;
    try
        try
//            LogAction('ProcesarMensajes START');
            spMails.Close;
            spMails.ProcedureName := 'EMAIL_ObtenerComprobantesAEnviar';
            spMails.Parameters.Refresh;
            spMails.Parameters.ParamByName('@Instancia').Value := FInstancia;
            spMails.Open;

            EMailsEnviados := 0;                                
//            if not Assigned(FSMTP) then FSMTP := CreateSMTP;

            while not spMails.Eof do begin
                CodigoMensaje := spMails.FieldByName('CodigoMensaje').AsInteger;
                // Esta variable FMensajeActual privada la vamos a utilizar en el caso de una exepci�n
                // por errores de SMTP al momento del env�o de un mensaje
                // para marcar el mensaje como enviado de acuerdo al error que nos devuelva el server SMTP
                // en el caso que este error sea irrecuperable
                FMensajeActual := CodigoMensaje;
                FArchivoActual := Trim(spMails.FieldByName('NombreArchivo').AsString);
                Email          := Trim(spMails.FieldByName('Email').AsString);
                Firma          := Trim(spMails.FieldByName('Firma').AsString);
                Asunto         := Trim(spMails.FieldByName('Asunto').AsString);
                Plantilla      := Trim(spMails.FieldByName('Plantilla').AsString);
                EmailSender    := Trim(spMails.FieldByName('Email_Sender').AsString);
                ClaveSender    := Trim(spMails.FieldByName('Clave_Sender').AsString);
                NameSender     := Trim(spMails.FieldByName('Name_Sender').AsString);

//                LogAction('Clave: ' + ClaveSender);

                Reintentos     := spMails.FieldByName('Reintentos').AsInteger;
                Parametros     := Trim(spMails.FieldByName('Parametros').AsString);
                EliminarArchivo := spMails.FieldByName('EliminarArchivo').AsBoolean;
                CodigoPlantilla := spMails.FieldByName('CodigoPlantilla').AsInteger;
                ContentType    := IfThen(spMails.FieldByName('ContentType').AsInteger=1,'text/plain','text/html');
                Mensaje := ProcesarParametros(CodigoPlantilla, Plantilla, Parametros);

                LogAction('ProcesarMensajes EnviarMensaje START');
                try


                  if EnviarMensaje(FArchivoActual,
                                   Email,
                                   Firma,
                                   Asunto,
                                   Mensaje,
                                   ContentType,
                                   EmailSender,
                                   ClaveSender,
                                   NameSender) then begin
                      if not ActualizarMensajeEnviado(CodigoMensaje,'EMAIL_Comprobantes') then begin
                          LogAction('ProcesarMensajes EnviarMensaje No pudo Actualizar mensaje enviado');
                          EventLogReportEvent(elError, 'ProcesarMensajes EnviarMensaje No pudo Actualizar mensaje enviado', '');
                          Break;
                      end;
                      LogAction('Mail enviado exitosamente a: ' + Email + ' Comprobante:' + FArchivoActual);
                      if EliminarArchivo then BorrarAdjunto();
                  end else begin
                      if not SumarReintento(CodigoMensaje,'EMAIL_Comprobantes') then Break;		//SS-601-NDR-20101111
                      if Reintentos >= FMaxReintentos then begin
                          //Revision 2: Si est� indicado para borrar el adjunto, se llama al metodo de borrado correspondiente
                          if EliminarArchivo then BorrarAdjunto();
                      end;

                  end;
                except																												   
                   on e: exception do begin        																					   
                        EventLogReportEvent(elError, 'Error al enviar el mensaje ' + inttostr(CodigoMensaje) + ': ' + e.Message, '');  
                        if not SumarReintento(CodigoMensaje,'EMAIL_Comprobantes') then Break;										   
                   end;																												   
                end;

                Inc(EMailsEnviados);                    
                if EMailsEnviados = 5 then begin        
                    LiberarMemoria;                     
                    EMailsEnviados := 0;                
                end;

                spMails.Next;

                if spMails.Eof then begin

                    spMails.Close;
                    spMails.Open;
                end;
                
            end;

            spMails.Close;

//            spMails.ProcedureName := 'EMAIL_ObtenerContratosAdhesionPAKAEnviar';
//            spMails.Open;
//            Result := not spMails.IsEmpty;
//            if not Result then Exit;
//
//            EMailsEnviados := 0;
//
//            while not spMails.Eof do begin
//
//                CodigoMensaje := spMails.FieldByName('CodigoMensaje').AsInteger;
//
//                FMensajeActual := CodigoMensaje;
//                FArchivoActual := Trim(spMails.FieldByName('NombreArchivo').AsString);
//                Email          := Trim(spMails.FieldByName('Email').AsString);
//                Firma          := Trim(spMails.FieldByName('Firma').AsString);
//                Asunto         := Trim(spMails.FieldByName('Asunto').AsString);
//                Plantilla      := Trim(spMails.FieldByName('Plantilla').AsString);
//                NameSender    := Trim(spMails.FieldByName('Name_Sender').AsString);
//                EmailSender    := Trim(spMails.FieldByName('Email_Sender').AsString);
//                ClaveSender    := Trim(spMails.FieldByName('Clave_Sender').AsString);
//
//                LogAction('Clave: ' + ClaveSender);
//
//                Reintentos     := spMails.FieldByName('Reintentos').AsInteger;
//                Parametros     := Trim(spMails.FieldByName('Parametros').AsString);
//                EliminarArchivo := spMails.FieldByName('EliminarArchivo').AsBoolean;
//                CodigoPlantilla := spMails.FieldByName('CodigoPlantilla').AsInteger;
//
//                ContentType    := IfThen(spMails.FieldByName('ContentType').AsInteger=1,'text/plain','text/html');
//
//                if Parametros <> EmptyStr then begin
//	                Mensaje := ProcesarParametros(CodigoPlantilla, Plantilla, Parametros);
//                end;
//
//                LogAction('ProcesarMensajes EnviarMensaje START');
//
//                try
//                    if EnviarMensaje(FArchivoActual, Email, Firma, Asunto, Mensaje, ContentType, EmailSender,ClaveSender, NameSender) then begin
//
//                        if not ActualizarMensajeEnviado(CodigoMensaje,'EMAIL_ContratoAdhesionPAK') then begin
//
//                            LogAction('ProcesarMensajes EnviarMensaje No pudo Actualizar mensaje enviado');
//                            EventLogReportEvent(elError, 'ProcesarMensajes EnviarMensaje No pudo Actualizar mensaje enviado', '');
//                            Break;
//                        end;
//
//                        LogAction('Mail enviado exitosamente a: ' + Email + ' Contrato PAK:' + FArchivoActual);
//                        if EliminarArchivo then BorrarAdjunto();
//                    end
//                    else begin
//
//                        if not SumarReintento(CodigoMensaje,'EMAIL_ContratoAdhesionPAK') then Break;
//
//                        if Reintentos >= FMaxReintentos then begin
//                        	if EliminarArchivo then BorrarAdjunto();
//                        end;
//                    end;
//                except
//                    on e: exception do begin
//                        EventLogReportEvent(elError, 'Error al enviar el mensaje ' + inttostr(CodigoMensaje) + ': ' + e.Message, '');
//                        if not SumarReintento(CodigoMensaje,'EMAIL_ContratoAdhesionPAK') then Break;
//                    end;
//                end;
//
//                Inc(EMailsEnviados);                   
//                if EMailsEnviados = 5 then begin       
//                    LiberarMemoria;                    
//                    EMailsEnviados := 0;               
//                end;
//
//                spMails.Next;
//            end;

            Result := True;
        except
            on e: Exception do begin
                LogAction(Format(MSG_GET_MESSAGES_ERROR, [e.Message]));
                EventLogReportEvent(elError, Format(MSG_GET_MESSAGES_ERROR, [e.Message]), '');
            end;
        end;
    finally
        FMensajeActual := 0;
    end;
end;


{------------------------------------------------------------------------
                ServiceAfterInstall

Author  : ndroguett
Date:
Description :   Registra lo que ocurre posteriormente a la instalaci�n del servicio.

-------------------------------------------------------------------------}
procedure TfrmMainForm.ServiceAfterInstall(Sender: TObject);
begin
    EventLogRegisterSource;
end;

{------------------------------------------------------------------------
                ServiceAfterUninstall

Author  : ndroguett
Date:
Description :   Registra lo que ocurre posteriormente a la desinstalaci�n del servicio.

-------------------------------------------------------------------------}
procedure TfrmMainForm.ServiceAfterUninstall(Sender: TObject);
begin
    EventLogUnregisterSource;
end;

{------------------------------------------------------------------------
                ServiceStart

Author  : ndroguett
Date:
Description :   Realiza las acciones necesaris durante el inicio del servicio 

-------------------------------------------------------------------------}
procedure TfrmMainForm.ServiceStart(Sender: TObject; var Started: Boolean;
  var ExitCode, SpecificExitCode: Cardinal);
resourcestring
    MSG_SERVICE_SUCCESS_STARTED = 'Servicio iniciado con �xito';
    NombreArchivoLog            = 'EnvioMail.Log';
begin
    MoveToMyOwnDir;
    FMensajeActual := 0;

    bDebeEscribirLog:=InstallIni.ReadInteger('LogFile', 'LogToFile', 1) = 1;
    FInstancia          := InstallIni.ReadInteger('General','Instancia',0);
    FTipoInstancia          := InstallIni.ReadInteger('General','TipoInstancia',0);

    AssignFile( LogFile , NombreArchivoLog);
    if not FileExists(NombreArchivoLog) then ReWrite(LogFile);
    Append(LogFile);

    tmrReporte.Enabled := True;
    Started := True;

    LogAction(MSG_SERVICE_SUCCESS_STARTED);
	EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_STARTED , '');
end;

{------------------------------------------------------------------------
                ServiceStop

Author  : ndroguett
Date:
Description :   Realiza las acciones necesaris durante la finalizaci�n del servicio 

-------------------------------------------------------------------------}
procedure TfrmMainForm.ServiceStop(Sender: TObject);
resourcestring
    MSG_SERVICE_SUCCESS_STOPED = 'Servicio detenido con �xito';
begin
    LogAction(MSG_SERVICE_SUCCESS_STOPED);
    CloseFile(LogFile);
    EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_STOPED , '');
end;

{------------------------------------------------------------------------
                SumarReintento

Author  : ndroguett
Date:
Description :   Incrementa la cantidad de reintentos en el env�o de correo. 

-------------------------------------------------------------------------}
//Rev.9 / 11-Noviembre-2010 / Nelson Droguett Sierra
function TfrmMainForm.SumarReintento(const CodigoMensaje: Int64; Tabla:string ): Boolean;
resourcestring
    MSG_REP_UPDATE_RETRY_ERROR = 'Ha ocurrido un error actualizando los reintentos para el e-mail de c�digo %d. Detalle: %s';
const
    cSQLUPDATE = 'UPDATE %s SET Reintentos = Reintentos + 1 WHERE CodigoMensaje = %d';
begin
    Result := False;
    try
    	QueryExecute(DMConnections.BaseCAC, Format(cSQLUPDATE, [Tabla, CodigoMensaje]));
        Result := True;
    except
        on e: Exception do begin
            LogAction(Format(MSG_REP_UPDATE_RETRY_ERROR, [CodigoMensaje, e.Message]));;
            EventLogReportEvent(elError, Format(MSG_REP_UPDATE_RETRY_ERROR, [CodigoMensaje, e.Message]), '');
        end;
    end;
end;

{------------------------------------------------------------------------
                tmrReporteTimer

Author  : ndroguett
Date:
Description :   Evento que indica el tiempo de inicio del procesamiento
                de mensajes para enviar. 

-------------------------------------------------------------------------}
procedure TfrmMainForm.tmrReporteTimer(Sender: TObject);
begin
    LogAction('Inicio de la generaci�n de PDF');
    tmrReporte.Enabled  := False;
    try
        if not ConectarBase then Exit;
        if not ConfigurarParametros then Exit;
        if Fintervalo <> 0 then tmrReporte.Interval := FIntervalo * 1000;
        if FTipoInstancia = 1 then begin

            if not ProcesarMensajes() then LogAction('ProcesarMensajes retorn� Falso');
        end;
        if FTipoInstancia = 2 then begin

            if not EnviarMensajes then
                LogAction('EnviarMensajes retorn� Falso');
            if not EnviarCorrespondencia then
                LogAction('EnviarCorrespondencia retorn� Falso');
        end;
        
    finally
        if Assigned(FSMTP) then begin                           
            //if FSMTP.Connected then FSMTP.Disconnect(True);     
            FreeAndNil(FSMTP);
        end;

        tmrReporte.Enabled := True;
    end;
end;

{------------------------------------------------------------------------
                LogAction

Author  : ndroguett
Date:
Description :   Graba en un archivo un mensaje de log.

-------------------------------------------------------------------------}
procedure TfrmMainForm.LogAction(Mensaje:string);
var
    HoraYMensaje : string;
begin
    // Solo se pregunta si escribe o no el Log, consultando una variable, que toma
    // valor en el inicio del servicio.
    if bDebeEscribirLog then begin
      HoraYMensaje := DateTimeToStr(now) + ' : ' + Mensaje ;
      WriteLn( LogFile , Pchar(HoraYMensaje));
    end;
end;


{------------------------------------------------------------------------
                EnviarMensajes

Author  : ndroguett
Date:
Description :   Genera un mensaje y lo env�a por cvorreo

-------------------------------------------------------------------------}
function TfrmMainForm.EnviarMensajes: Boolean;
const
    SMTP_PORT = 25;
var
    Error: string;
    Email: TMensaje;
    EMailsEnviados: Integer;
begin
    Result := False;
    try
        with dmMensajes.spMensajesSinEnviar do begin
        	Close;
            Open;
            Result := not IsEmpty;
            if not Result then Exit;

            EMailsEnviados := 0;
//            if not Assigned(FSMTP) then FSMTP := CreateSMTP;

            while not eof do begin
                 if not ObtenerMensaje(dmMensajes,
                                       FieldByName('CodigoMensaje').AsInteger,
                                       Email,
                                       Error) then
                 begin
                    EventLogReportEvent(elWarning, 'Error: No se pudo obtener el mensaje ' + inttostr(FieldByName('CodigoMensaje').AsInteger), '');
                    exit;
                 end;

                 try
                     try
                        LogAction('Clave: ' + Email.ClaveSender);

                        if EnviarMensaje(IfThen(Email.ArchivoAdjunto,Email.NombreArchivo,''),
                                         Email.Para,
                                         Email.Firma,
                                         Email.Asunto,
                                         Email.Mensaje,
                                         Email.ContentType,
                                         Email.EmailSender,
                                         Email.ClaveSender,
                                         Email.NameSender) then begin
                            if not ActualizarMensajeEnviado(FieldByName('CodigoMensaje').AsInteger,'EMAIL_Mensajes') then begin
                                LogAction('ProcesarMensajes EnviarMensaje No pudo Actualizar mensaje enviado');
                                EventLogReportEvent(elError,
                                  'ProcesarMensajes EnviarMensaje No pudo Actualizar mensaje enviado', '');
                                Break;
                            end;
                            LogAction('Mail enviado exitosamente a: ' + Email.Para + ' Comprobante:' + FArchivoActual);
                            if Email.EliminarArchivo then begin
                               FArchivoActual:=Email.NombreArchivo;
                               BorrarAdjunto();
                            end;
                        end else begin
                            if not SumarReintento(FieldByName('CodigoMensaje').AsInteger,'EMAIL_Mensajes') then Break;

                            if Email.Reintentos >= FMaxReintentos then begin
                                if Email.EliminarArchivo then begin
                                   FArchivoActual:=Email.NombreArchivo;
                                   BorrarAdjunto();
                                end;
                            end;
                        end;
                     except
                         on e: exception do begin
                              EventLogReportEvent(elError, 'Error al enviar el mensaje ' + inttostr(FieldByName('CodigoMensaje').AsInteger) + ': ' + e.Message, '');
                              if not SumarReintento(FieldByName('CodigoMensaje').AsInteger,'EMAIL_Mensajes') then Break;
                         end;
                     end;
                 finally
                     FillChar(Email, SizeOf(Email), 0);
                 end;

                Inc(EMailsEnviados);
                if EMailsEnviados = 5 then begin
                    LiberarMemoria;
                    EMailsEnviados := 0;
                end;

                Next;
            end;
            Close;
        end;
    except
        on e: exception do begin
            dmMensajes.spMensajesSinEnviar.Close;
            { INICIO : 20160609 MGO
            dmMensajes.Base.Close;
            } // FIN : 20160609 MGO
            EventLogReportEvent(elError, 'Error al enviar los mensajes: ' + e.Message, '');
        end;
    end;
end;

{------------------------------------------------------------------------
                EnviarCorrespondencia

Author  : mgomez
Date    : 18-02-2016
Description : Genera un mensajes de Correspondencia y los env�a por correo

-------------------------------------------------------------------------}
function TfrmMainForm.EnviarCorrespondencia: Boolean;
resourcestring
    MSG_GET_MESSAGES_ERROR = 'Ha ocurrido un error obteniendo Correspondencia a enviar. Detalle: %s';
var
    CodigoMensaje, CodigoPlantilla, CodigoConvenio, CodigoPersona, Reintentos: Integer;
    Email, Firma, Asunto, Plantilla, ContentType, Parametros, Mensaje, ErrorDescription: String;
    EliminarArchivo: Boolean;
    EMailsEnviados: Integer;
    NameSender : string;
    EmailSender: string;              
    ClaveSender: AnsiString;

    function ProcesarEncabezado(Plantilla: String; CodigoMensaje, CodigoConvenio, CodigoPersona: Integer): String;
    resourcestring
        QRY_OBTENER_NUMERO_CONVENIO = 'SELECT RTRIM(dbo.ObtenerNumeroConvenio(%d))';
    var
        Calle, Numero, Piso, Dpto, Comuna, Region, Direccion: String;
        Apellido, ApellidoMaterno, Nombre, NombreCompleto, Titulo, RazonSocial: String;
        vLogo: String;
        NumeroConvenio: String;
    begin
        Result := Plantilla;

        with spObtenerDomicilioConvenio do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            Open;

            if not IsEmpty then begin
                Calle := Trim(FieldByName('DescriCalle').AsString);
                if Calle = EmptyStr then Calle := Trim(FieldByName('CalleDesnormalizada').AsString);
                Numero := Trim(FieldByName('Numero').AsString);
                Piso := Trim(FieldByName('Piso').AsString);
                Dpto := Trim(FieldByName('Dpto').AsString);
                Comuna := Trim(FieldByName('DescriComuna').AsString);
                Region := Trim(FieldByName('DescriRegion').AsString);
            end;
        end;

        Direccion := Calle + ' ' + Numero;
        if (Piso <> EmptyStr) and (Piso <> '0') then
            Direccion := Direccion + ', piso ' + Piso;
        if Dpto <> EmptyStr then
            Direccion := Direccion + ', ' + Dpto;

        with spObtenerDatosPersona do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            Open;

            if not IsEmpty then begin
                Apellido := Trim(FieldByName('Apellido').AsString);
                ApellidoMaterno := Trim(FieldByName('ApellidoMaterno').AsString);
                Nombre := Trim(FieldByName('Nombre').AsString);
                RazonSocial := Trim(FieldByName('Giro').AsString);
                Titulo := Util.iif(Trim(FieldByName('Sexo').AsString) = 'F', 'Sra.', 'Sr.');
            end;
        end;

        NombreCompleto := Apellido;
        if ApellidoMaterno <> EmptyStr then
            NombreCompleto := NombreCompleto + ' ' + ApellidoMaterno;
        NombreCompleto := NombreCompleto + ', ' + Nombre;

        ObtenerParametroGeneral(DMConnections.BaseCAC, 'Logo_Intervial_Maipo', vLogo);
        NumeroConvenio := QueryGetValue(DMConnections.BaseCAC, Format(QRY_OBTENER_NUMERO_CONVENIO, [CodigoConvenio]));

        Result := StringReplace(Result, '%Logo_Intervial_Maipo%', vLogo, [rfIgnoreCase, rfReplaceAll]);
        Result := StringReplace(Result, '%CodigoMensaje%', IntToStr(CodigoMensaje), [rfReplaceAll]);
        Result := StringReplace(Result, '%CodigoConvenio%', NumeroConvenio, [rfReplaceAll]);
        Result := StringReplace(Result, '%NumeroConvenio%', NumeroConvenio, [rfReplaceAll]);
        Result := StringReplace(Result, '%FechaEnvio%', FormatDateTime('dd-mm-yyyy', Now), [rfReplaceAll]);
        Result := StringReplace(Result, '%Direccion%', Direccion, [rfReplaceAll]);
        Result := StringReplace(Result, '%Comuna%', Comuna, [rfReplaceAll]);
        Result := StringReplace(Result, '%Ciudad%', Region, [rfReplaceAll]);
        Result := StringReplace(Result, '%Titulo%', Titulo, [rfReplaceAll]);
        Result := StringReplace(Result, '%NombreCompleto%', NombreCompleto, [rfReplaceAll]);
        Result := StringReplace(Result, '%RazonSocial%', RazonSocial, [rfReplaceAll]);
    end;
begin
    Result := False;
    try
        try
            EMailsEnviados := 0;
            spMails.Close;
            spMails.ProcedureName := 'EMAIL_Correspondencia_Obtener';
            spMails.Parameters.Refresh;
            spMails.Parameters.ParamByName('@CodigoMensaje').Value := Null;
            spMails.Parameters.ParamByName('@ErrorDescription').Value := ErrorDescription;
            spMails.Open;
            Result := not spMails.IsEmpty;
            if not Result then Exit;

//            if not Assigned(FSMTP) then FSMTP := CreateSMTP;

            while not spMails.Eof do begin
                CodigoMensaje   := spMails.FieldByName('CodigoMensaje').AsInteger;
                FMensajeActual  := CodigoMensaje;

                CodigoConvenio  := spMails.FieldByName('CodigoConvenio').AsInteger;
                CodigoPersona   := spMails.FieldByName('CodigoPersona').AsInteger;
                Email           := Trim(spMails.FieldByName('Email').AsString);
                Firma           := Trim(spMails.FieldByName('Firma').AsString);
                Asunto          := Trim(spMails.FieldByName('Asunto').AsString);
                Plantilla       := Trim(spMails.FieldByName('Plantilla').AsString);
                NameSender      := Trim(spMails.FieldByName('Name_Sender').AsString);
                EmailSender     := Trim(spMails.FieldByName('Email_Sender').AsString);
                ClaveSender     := Trim(spMails.FieldByName('Clave_Sender').AsString);
                Reintentos      := spMails.FieldByName('Reintentos').AsInteger;
                Parametros      := Trim(spMails.FieldByName('Parametros').AsString);
                CodigoPlantilla := spMails.FieldByName('CodigoPlantilla').AsInteger;
                ContentType     := IfThen(spMails.FieldByName('ContentType').AsInteger=1,'text/plain','text/html');

                Mensaje := ProcesarEncabezado(Plantilla, CodigoMensaje, CodigoConvenio, CodigoPersona);

                if Parametros <> EmptyStr then begin
	                Mensaje := ProcesarParametros(CodigoPlantilla, Mensaje, Parametros);
                end;

                LogAction('EnviarCorrespondencia - EnviarMensaje START');

                try
                    if EnviarMensaje(EmptyStr, Email, Firma, Asunto, Mensaje, ContentType, EmailSender, ClaveSender, NameSender) then begin
                        if not ActualizarMensajeEnviado(CodigoMensaje, 'EMAIL_Correspondencia') then begin
                            LogAction('EnviarCorrespondencia - EnviarMensaje No pudo Actualizar mensaje enviado');
                            EventLogReportEvent(elError, 'EnviarCorrespondencia - EnviarMensaje No pudo Actualizar mensaje enviado', '');
                            Break;
                        end;
                        LogAction('Mail enviado exitosamente a: ' + Email + '; Correspondencia:' + Asunto);
                    end else begin
                        if not SumarReintento(CodigoMensaje, 'EMAIL_Correspondencia') then Break;
                    end;
                except
                    on e: exception do begin
                        EventLogReportEvent(elError, 'EnviarCorrespondencia - Error al enviar el mensaje ' + inttostr(CodigoMensaje) + ': ' + e.Message, '');
                        if not SumarReintento(CodigoMensaje, 'EMAIL_Correspondencia') then Break;
                    end;
                end;

                Inc(EMailsEnviados);
                if EMailsEnviados = 5 then begin
                    LiberarMemoria;
                    EMailsEnviados := 0;
                end;

                spMails.Next;
            end;
            Result := True;
        except
            on e: Exception do begin
                LogAction(Format(MSG_GET_MESSAGES_ERROR, [e.Message]));
                EventLogReportEvent(elError, Format(MSG_GET_MESSAGES_ERROR, [e.Message]), '');
            end;
        end;
    finally
        FMensajeActual := 0;
    end;
end;

procedure TfrmMainForm.LiberarMemoria;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
end;

end.

