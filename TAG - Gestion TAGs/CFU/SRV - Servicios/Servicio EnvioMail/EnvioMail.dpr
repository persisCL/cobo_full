program EnvioMail;

uses
  Forms,
  DMConnection in '..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  PeaTypes in '..\..\Comunes\PeaTypes.pas',
  frmMain in 'frmMain.pas' {frmMainForm},
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  MensajesEmail in '..\..\Comunes\MensajesEmail.pas',
  dmMensaje in '..\..\Comunes\dmMensaje.pas' {dmMensajes: TDataModule},
  Crypto in '..\..\Comunes\Crypto.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDMConnections, DMConnections);
  Application.CreateForm(TdmMensajes, dmMensajes);
  Application.CreateForm(TfrmMainForm, frmMainForm);
  if not frmMainForm.Inicializar then frmMainform.Close;
  Application.Run;
end.
