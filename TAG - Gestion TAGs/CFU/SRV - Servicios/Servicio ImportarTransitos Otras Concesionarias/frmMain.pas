{---------------------------------------------------------------------------------
        	MainForm

 Author			: mbecerra
 Date			: 06-Agosto-2010
 Description	: 	Se encarga de procesar todos los archivos que contengan
 					transacciones de transitos y estacionamientos de otras
                    concesionarias e insertarlos en las tablas
                    TransitosPendientesDelCOP y TransaccionesEstacionamientos
                    respectivamente.

Revision    : 1
Author      : Nelson Droguett Sierra
Date        : 26-Marzo-2011
Description : (Ref.FaseII)
                Se agrega la funcionalidad de Importar Archivos de Folios y de Pagos
Firma       : PAR0133-NDR-20110326

Firma		: PAR00133_MBE_20110805
Description	: 	Para la concesionaria AMB, se agregan todos los campos necesarios
            	que permitan generar la anomalia del transito.

                La categoria no es obligatoria en aquellas concesionarias
                cuyos transitos los factura 100% CN


Firma       : SS1006-NDR-20111027
Description : Se agrega funcionalidad de Importar Archivos de Ajustes de Estacionamientos

Firma       : SS1006-NDR-20111212
Description : Se hacen modificaciones para resolver errores definidos por Q&A

Firma       : SS_1015_MBE_20120206
Description : Se agregan los Conceptos Movimientos de Otras Concesionarias
              Se elimina todo lo de Ajustes de Estacionamientos, pues ahora se
              incluyen en los conceptos de otras concesionarias
              
Firma       : SS_1021_HUR_20120307
Descripcion : Se agregan los campos NumeroCarril y Velocidad a la importacion del archivo
              de otras concesionarias.

Firma       : SS_1021_HUR_20120329
Descripcion : Se corrige indice de los campos NumeroCarril y Velocidad.
              Se agrega la concesionaria 3 (Vespucio Sur) para que llame al procedimiento
              InsertarRestoDeLaLinea.

Firma       : SS_1015_CQU_20120412
Descripcion : Agrega el CodigoTipoTransaccion, ese campo nos permite identificar
              en la tabla RegistrosRechazadosOtrasConcesionarias el tipo de transaccion.
              Se agrega una funci�n que devuelve el tipo de transacci�n.

Firma       : SS_1015_CQU_20120413
Descripcion : Agrega ContractSerialNumber, Context, Contract, IssuerIdentifier
              al registro de transitos rechazados y estacionamientos rechazados
              de otras concesionarias.

Firma       : SS_1015_CQU_20120423
Description : Se realizan modificaciones de acuerdo a lo indicado por QA,
              se agrega una funci�n que mueve un archivo de un directorio a otro.

Firma       : SS_1015_CQU_20120427
Descripcion : Se implementa mejora que deja en memoria los datos de la tabla TiposTransacciones,
              de esa manera reduzco la cantidad de lecturas a SQL.
              Agrega validaci�n solicitada por QA.

Firma       : SS_1015_CQU_20120502
Descripcion : Se realiza modificaci�n al nombre de archivo cuando estos est�n duplicados,
              seg�n lo indicado por JP, se agrega el concepto "DUPLICADO" y luego la fecha.

Firma       : SS-1015-NDR-20120510
Description : Deshabilitar la IMPORTACION de Folios y Pagos.
              Importar Archivo de Saldos Iniciales de Otras Concesionarias
              Importar Archivo de Cuotas de Otras Concesionarias

Firma       : SS_1015_CQU_20120510
Descripcion : Se realiza modificaci�n para que al momento de rechazar una l�nea
              genere inmediatamente el archivo de rechazos.
              Adicionalmente se elimina la funci�n ObtenerTipoTransaccion ya que no corresponde
              a las tablas que ocupa y se agrega en su lugar la funci�n ObtenerTipoRegistroFolioyPago,
              se modifica, adem�s, el nombre de un campo ya que cambi� el modelo de datos.

Firma       : SS_1015_CQU_20120514
Description : Se agrega la generaci�n inmediata del archivo de rechazos a lo desarrollado por NDR

Firma       : SS_1015_CQU_20120517
Description : Corrige un error en la definici�n de la variable FNomArchTipoTrx para Saldo Inicial y Cuotas
              ya que el archivo recibido no trae el tipo de transacci�n y este SI se usa en nuestro proceso.

Firma       : SS_1015_CQU_20120525
Description : Se agrega n�mero de l�nea a las tablas
                - EstacionamientosRechazadosOtrasConcesionarias
                - FoliosRechazadosOtrasConcesionarias
                - SaldosInicialesRechazadosOtrasConcesionarias

Firma       : SS_1015_CQU_20120530
Descripcion : Se agrega el n�mero de l�nea en los archivos ya que la variable "Linea" no la incluye.
              Se cambia el formato del nombre de archivo rechazado, se agrega la funci�n GenerarNuevoNombre

Firma       : SS_1015_CQU_20120611
Descripcion : Corrige un error en la definici�n de la variable FNomArchTipoTrx para Movimiento Cuentas
              ya que el archivo recibido no trae el tipo de transacci�n y este SI se usa en nuestro proceso.

Firma       : SS_1015_MBE_20120622
Description : Se agrega el convenio externo al archivo de movimientos cuentas a importar

Firma       : SS_1015_CQU_20120711
Descripcion : A petici�n del cliente, se eliminan todos los sufijos creados para cada archivo rechazado
              y se crea un �nico Sufijo para Todos reduciendo as� la cantidad de par�metros generales.
              No se eliminar�n las variables sino que se modificar� el nombre del par�metro, esto es debido
              a que en un futuro se necesite nuevamente volver a definir un par�metro por tipo de archivo.

Firma       : SS_1006_CQU_20120807
Descripcion : Se modifica el m�todo ImportarLinea para que valide que la fecha/hora de salida de un estacionamiento
              sea mayor o igual a la fecha/hora de entrada.
              Se agrega validaci�n en el m�todo ImportarLinea para que valide que la fecha/hora de un estacionamiento
              cumpla con el formato correspondiente (YYYYMMDDHHMMSS)

Firma       : SS_1006_1015_20120913_CQU
Descripcion : Al importar Estacionamientos, si no viene la patente se guardaba "vac�o" ahora se guarda NULL

Firma       : SS_1006_1015_MBE_20120921
Description : Ignora las l�neas vac�as

Firma       : SS_1006_1015_CQU_20120926
Description : - Se agrega validaci�n a la fecha de ingreso, no puede ser superior a la fecha actual.
              - Se agrega funci�n para validar el ContexMark
              - Se cambia el nombre de un campo en la tabla RegistrosRechazadosOtrasConcesionarias
			    y en la tabla EstacionamientosRechazadosOtrasConcesionarias
			    por lo cual se cambia el nombre en los par�metros de los respectivos SP's que lo usan.
              - Se agrega funci�n para validar que el Estacionamiento a procesar no se haya procesado con anterioridad.

Firma       : SS_1006_1015_CQU_20121002
Description : Se corrigen varias validaciones a los campos
              - Se agrega la funci�n ValidarCodigoExternoEstacionamiento para validar que exista el c�digo de entrada o salida de estacionamientos
              - Se agrega la funci�n StringEsEntero para validar si un n�mero puede ser Parseado a INTEGER o INT64 ya que la funci�n EsNumero no es suficiente

Firma       : SS_1006_NDR_20121227
Description : Los registros que no esten en lista de acceso ni en lista amarilla deben ser rechazados.


Firma       : SS_1006_NDR_20130118
Description : Se rechazan los registros que coinciden para el mismo TAG, fecha y entrada (para estacionamiento) o concepto(para movimiento)

Firma       : SS_1006_CQU_20130130
Description : Se agrega un nuevo campo que contendr� el Numero de Transaccion de estacionamientos, con ello se pretende relacionar
              el Movimiento de descuento con su estacionamiento y evitar duplicidad o rechazos innecesarios

Firma       : SS_1006_NDR_20130228
Description : En la tabla MovimientosCuentasOtrasConcesionarias ya no se graba el nombre del ArchivoEnvio sino el IDArchivo de la tabla
              ArchivosProcesadosOtrasConcesionarias. Para eso el archivo se debe grabar en esa tabla ANTES de importar las lineas.
              Lo mismo se hara para los archivos de estacionamientos

Firma       : SS_1006_CQU_20130328
Description : Corrige un problema al obtener y evaluar la variable IDArchivo recibido desde ArchivosProcesadosOtrasConcesionarias
              Corrige un problema que se produce cuando el campo LinNumTranEsta no viene en el archivo,
              desde ahora se rechaza la l�nea si no viene.

Autor       :   Claudio Quezada Ib��ez
Firma       :   SS_1147_CQU_20140408
Fecha       :   24-04-2014
Descripcion :   Se agrega variable y llamada a funci�n para obtener la Concesionaria Nativa

Firma       :   SS_1147P_20141117
Description :   Se separa el Saldo Inicial Deudor y acreedor en Afecto y exento

Firma		:	SS_1147Z_MCA_20121229
Descripcion	:	se agrega campo idarchivo a cada linea importada

Firma       :   SS_1147_P1_MBE_20150113
Description :   Se agregan los nuevos campos tipo y n�mero fiscal

Firma       :   SS_1147_P2_MBE_20150116
Description :   Ya no se usa la concesionaria y en vez del c�digo de convenio,
                viene el NumeroConvenio, que es �nico en el CAC 

Firma       :   SS_1147_P3_MBE_20150116
Description :   Se agrega el tipo de comprobante XE.
                Para evitar una doble validaci�n, se deja la validaci�n al SP
                AgregarRegistroASaldoInicialOtrasConcesionarias
                
----------------------------------------------------------------------------------}
unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, DpsServ, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase,
  IdSMTP, IdMessage, ExtCtrls,
  EventLog,
  Util,
  UtilDB,
  UtilProc,
  ConstParametrosGenerales,
  DMConnection, StdCtrls;

type
  TMainForm = class(TForm)
    dpsServiceImportarArchivosOtrasConce: TDPSService;
    spAgregarArchivo: TADOStoredProc;
    IdSMTPEnviarCorreo: TIdSMTP;
    tmerImportar: TTimer;
    aqryArchivo: TADOQuery;
    spAgregarTransitoOtraConcesionaria: TADOStoredProc;
    spAgregarTransaccionEstacionamiento: TADOStoredProc;
    spAgregarRegRechOtrasConcesio: TADOStoredProc;
    spAgregarTTORechOtraConcesio: TADOStoredProc;
    spObtenerConcesionarias: TADOStoredProc;
    spAgregarESTARechOtraConcesio: TADOStoredProc;                                                                            //PAR0133-NDR-20110326
    spAgregarMovCtaOtraConc: TADOStoredProc;                                                 //SS_1015_MBE_20120206
    spAgregarMovCtaRechazado: TADOStoredProc;                                                                //SS_1015_MBE_20120206
    spAgregarSaldoInicialRechazadoOtraConcesionaria: TADOStoredProc;                                         //SS-1015-NDR-20120510
    spAgregarSaldoInicialOtraConcesionaria: TADOStoredProc;                                                  //SS-1015-NDR-20120510
    spAgregarCuotaOtraConcesionaria: TADOStoredProc;                                                         //SS-1015-NDR-20120510
    spAgregarCuotaRechazadaOtraConcesionaria: TADOStoredProc;
    spGenerarComprobanteSaldoInicialOtrasConcesionarias: TADOStoredProc;
    spGenerarComprobanteCuotasOtrasConcesionarias: TADOStoredProc;                                                //SS-1015-NDR-20120510
    procedure dpsServiceImportarArchivosOtrasConceStart(Sender: TObject; var Started: Boolean;
      var ExitCode, SpecificExitCode: Cardinal);
    procedure dpsServiceImportarArchivosOtrasConceStop(Sender: TObject);
    procedure tmerImportarTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FCarpetaDuplicados,FCarpetaEntrada, FCarpetaProcesados : string;                                                       //SS-1015-NDR-20120510
    //FCarpetaDuplicadosFolios,FCarpetaEntradaFolios, FCarpetaProcesadosFolios : string;                                     //PAR0133-NDR-20110326
    //FCarpetaDuplicadosPagos,FCarpetaEntradaPagos, FCarpetaProcesadosPagos : string;                                        //PAR0133-NDR-20110326
    FCarpetaDuplicadosMovCta,FCarpetaEntradaMovCta, FCarpetaProcesadosMovCta : string;                                     //SS_1015_MBE_20120206
    FCarpetaDuplicadosSaldosIni,FCarpetaEntradaSaldosIni, FCarpetaProcesadosSaldosIni : string;                            //SS-1015-NDR-20120510
    FCarpetaDuplicadosCuotas,FCarpetaEntradaCuotas, FCarpetaProcesadosCuotas : string;                                     //SS-1015-NDR-20120510
    FIntervaloMinutos, FContadorMinutos : integer;

    FCarpetaRechazadasTransac, FNombreRechazadasTransac : string;   // SS_1015_CQU_20120509
    FCarpetaRechazadosMovCta, FNombreRechazadosMovCta : string;     // SS_1015_CQU_20120509
    LineaError : TStringList;                                       // SS_1015_CQU_20120509
    FCarpetaRechazadosSaldosIni, FNombreRechazadosSaldosIni : string;     // SS_1015_CQU_20120514
    FCarpetaRechazadosCuotas, FNombreRechazadosCuotas : string;     // SS_1015_CQU_20120514
    FListaConcesionariasLasFacturaCN : TStringList;          //contiene las concesionarias cuyos tr�nsitos los facturaCN
    //Datos del nombre del archivo
    FNomArchParametro,                  //Prefijo del nombre
    FCodigoConcesionariaOrigen,         //Codigo de la concesionaria que origina los tr�nsitos
    FCodigoConcesionariaDestino,        //Codigo de la concesionaria que factura los tr�nsitos
    FNomArchTipoTrx,                    //Tipo de Transacci�n
    FNomArchFecha,                      //Fecha del archivo
    FNomArchHora,                       //Hora del Archivo

    //FNomArchParametroFolios,            //Prefijo del nombre                                                              //PAR0133-NDR-20110326
    //FNomArchParametroPagos,             //Prefijo del nombre                                                              //PAR0133-NDR-20110326
    FNomArchParametroMovCta,                                                                                              //SS_1015_MBE_20120206
    FNomArchParametroSaldoIni,                                                                                            //SS-1015-NDR-20120510
    FNomArchParametroCuotas,                                                                                              //SS-1015-NDR-20120510
    //Datos del correo
    FEmailFrom, FEmailTo, FEmailServerSMTP : string;

    //NumCorrCO procesados
    FNumCorrCOMin, FNumCorrCoMax : int64;

    //Indica que todos los tr�nsitos de esa concesionaria son facturables en CN
    FConcesionariaFacturaEnCN : Boolean;
    // Indica el tipo de transacci�n SS_1015_CQU_20120412
    FTipoTransaccion : Integer;
    // Mejora: Permite cargar por �nica vez los TiposTransacciones SS_1015_CQU_20120427
    qryTiposTransacciones : TADOQuery;

  public
    { Public declarations }
    function Inicializar : boolean;
    function ConectarBase: Boolean;
    function LeerParametrosGenerales : boolean;

    procedure ImportarArchivosCAC;
    procedure ImportarArchivoMovCtas;                                                                                                                           //SS_1015_MBE_20120206
    //procedure ImportarArchivosFolios;                                                                                                                           //PAR0133-NDR-20110326
    //procedure ImportarArchivosPagos;
    procedure ImportarSaldosInicialesOtrasConcesionarias;               //SS-1015-NDR-20120510
    procedure ImportarCuotasOtrasConcesionarias;                        //SS-1015-NDR-20120510

    procedure GenerarComprobanteSaldosInicialesOtrasConcesionarias;     //SS-1015-NDR-20120514
    procedure GenerarComprobanteCuotasOtrasConcesionarias;              //SS-1015-NDR-20120514

    procedure ObtenerConcesionariasQueFacturaCN;                                                                                                              //PAR0133-NDR-20110326
    function GrabarArchivoEnCAC(NombreArchivo : string; ContenidoArchivo : TStringList) : boolean;
    function GrabarArchivoMovCta(NombreArchivo : string; ContenidoArchivo : TStringList) : boolean;                                                             //SS_1015_MBE_20120206
    //function GrabarArchivoFolios(NombreArchivo : string; ContenidoArchivo : TStringList) : boolean;                                                             //PAR0133-NDR-20110326
    //function GrabarArchivoPagos(NombreArchivo : string; ContenidoArchivo : TStringList) : boolean;                                                              //PAR0133-NDR-20110326
    function GrabarArchivoSaldoInicial(NombreArchivo : string; ContenidoArchivo : TStringList) : boolean;                                                       //SS-1015-NDR-20120510
    function GrabarArchivoCuotas(NombreArchivo : string; ContenidoArchivo : TStringList) : boolean;                                                             //SS-1015-NDR-20120510

    function GetItem(Texto : string; NumeroItem : integer; Separador : Char) : string;
    function ImportarLinea( NombreArchivo, Linea : string; CodigoConcesionaria : integer; NumLinea : Integer; var MensajeError : string ) : boolean;
    function ImportarLineaMovCta( NombreArchivo, Linea : string; CodigoConcesionaria : integer; NumLinea : Integer; var MensajeError : string ) : boolean;      //SS_1015_MBE_20120206
    //function ImportarLineaFolio( NombreArchivo, Linea : string; CodigoConcesionaria : integer; NumLinea : Integer; var MensajeError : string ) : boolean;       //PAR0133-NDR-20110326
    //function ImportarLineaPago( NombreArchivo, Linea : string; CodigoConcesionaria : integer; NumLinea : Integer; var MensajeError : string ) : boolean;        //PAR0133-NDR-20110326
	//function ImportarLineaSaldoInicial( NombreArchivo, Linea : string; CodigoConcesionaria : integer; NumLinea : Integer; var MensajeError : string ) : boolean;     //SS_1147Z_MCA_20121229 //SS-1015-NDR-20120510    
	function ImportarLineaSaldoInicial( NombreArchivo, Linea : string; CodigoConcesionaria : integer; NumLinea : Integer; IDArchivo: Integer; var MensajeError : string ) : boolean;      //SS_1147Z_MCA_20121229 //SS-1015-NDR-20120510
    function ImportarLineaCuotas( NombreArchivo, Linea : string; CodigoConcesionaria : integer; NumLinea : Integer; var MensajeError : string ) : boolean;            //SS-1015-NDR-20120510

    function EsNumero( Texto : string ) : boolean;
    function ConvierteAFecha( Fecha : string) : TDateTime;
    function RegistrarRegistroRechazado(NombreArchivo, Motivo, NumCorrCO : string; NumeroLinea : integer) : Boolean;
    function SePuedeFacturarEnCN(Rut : string) : Boolean;
//    function RegistrarTransitoRechazado(Rut, NombreArchivo, NumCorrCO, CodigoConcesionaria: string; Importe, NumeroLinea : Integer; FechaHoraTransito : TDateTime) : Boolean; //SS_1015_CQU_20120413
    function RegistrarTransitoRechazado(Rut, NombreArchivo, NumCorrCO, CodigoConcesionaria: string; Importe, NumeroLinea : Integer; FechaHoraTransito : TDateTime; ContractSerialNumber, Context, Contract, IssuerIdentifier: string) : Boolean; //SS_1015_CQU_20120413
    //function RegistrarFolioRechazado(NumCorrCA,CodigoConcesionaria,NumeroFolio : Integer; TipoDocumento,ArchivoFolio,MotivoRechazo: string; FechaEmision, FechaVencimiento: TDateTime):Boolean;     //PAR0133-NDR-20110326
    //function RegistrarPagoRechazado(CodigoConcesionaria,NumeroFolio : Integer; TipoDocumento,ArchivoPago,MotivoRechazo: string; FechaPago: TDateTime):Boolean;                                      //PAR0133-NDR-20110326


    function InsertarRestoDeLaLinea(Linea, CodigoConcesionaria, NumCorrCO : string) : boolean;     		//PAR00133_MBE_20110805
    //function ObtenerTipoTransaccion(Tipo : string) : Integer;                                                             //SS_1015_CQU_20120412 //SS_1015_CQU_20120510
    function ObtenerTipoRegistroFolioyPago(Tipo : string) : Integer;                                                        //SS_1015_CQU_20120510
    function MoverArchivo(DirectorioOrigen, DirectorioDestino, NombreArchivo : string; RenombraSiExiste : boolean) : boolean;    //SS_1015_CQU_20120423
    function MoverArchivoCambiarNombre(DirectorioOrigen, DirectorioDestino, NombreArchivoOrigen, NombreArchivoDestino : string; EsRechazado : boolean) : boolean; // SS_1015_CQU_20120510
    function GenerarNuevoNombre(NombreArchivoImportado, Sufijo : string) : string;  // SS_1015_CQU_20120530
    function ValidarContexMark(IssuerIdentifier, ContextMark, TypeOfContract : String) : Boolean ; // SS_1006_1015_CQU_20120926
    function ValidarEstacionamiento(CodigoConcesionaria, NumTransaccionExterno : String) : Boolean ; // SS_1006_1015_CQU_20120926
    function ValidarCodigoExternoEstacionamiento(TipoConsulta : Char; CodigoConcesionariaLinea, CodigoExternoLinea : string) : Boolean;     // SS_1006_1015_CQU_20121002
    function StringEsEntero(TextoEvaluar : string; EsBigInt : Boolean) : Boolean;                                                           // SS_1006_1015_CQU_20121002
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}
{$I ..\..\comunes\Ambiente.inc}

const
    TIPO_MLFF = '001';          //tipo MultiLine Fre Flow
    TIPO_PEAJ = '002';          //tipo Peaje Tradicional
    TIPO_ESTA = '003';          //tipo Estacionamiento
    TIPO_MOV  = '004';          //tipo Movimiento Cuenta    // SS_1015_CQU_20120510
    TIPO_SALD = '005';          //tipo Saldo Inicial        // SS_1015_CQU_20120510
    TIPO_CRED = '006';          //tipo Cr�ditos             // SS_1015_CQU_20120510
    TIPO_CUOT = '008';          //tipo Cuotas               // SS_1015_CQU_20120514
{-------------------------------------------------------------------------------
                        GetItem

 Author         : mbecerra
 Date           : 08-Abril-2008
 Description    :   Function que recibe un string de la forma <campo1>sep<campo2>sep<campo3>
                    y devuelve el <campo i> indicado en el par�metro, o un string vac�o
                    si no lo encuentra
-------------------------------------------------------------------------------}
function TMainForm.GetItem;
var
    Elemento : string;
    i, Cual : integer;
begin
    i := 1;
    Elemento := '';
    Cual := 0;
    Texto := Texto + Separador;  {para asegurarnos de encontrar el separador en el �ltimo elemento}
    while i <= Length(Texto) do begin
        if Texto[i] = Separador then begin
            Inc(Cual);
            if Cual = NumeroItem then   {es el elemento que se debe retornar}
            	i := Length(Texto) + 1
            else
                Elemento := '';         {no es el elemento que se debe retornar}

        end
        else
          Elemento := Elemento + Texto[i];

    	Inc(i);
    end;

    Result := Elemento;
end;

{-------------------------------------------------------------------------------
            EsNumero
            
Author          : mbecerra
Date            : 10-Agosto-2010
Description     : Devuelve verdadero si el texto es num�rico, Falso en caso contrario
-------------------------------------------------------------------------------}
function TmainForm.EsNumero;
var
    posi : integer;
begin
    if Length(Texto) > 0 then Result := True
    else Result := False;
    
    posi := 1;
    while Result and (posi <= Length(Texto)) do begin
        Result := (Texto[posi] in ['0'..'9']);
        Inc(posi);
    end;
    
end;

{---------------------------------------------------------------------------------
        	ConvierteAFecha

 Author			: mbecerra
 Date			: 06-Agosto-2010
 Description	: convierte un string de la forma AAAAMMDDHHNNSS a una fecha
                    Si hay un error retorna 0
----------------------------------------------------------------------------------}
function TMainForm.ConvierteAFecha;
var
    StrYear, StrMes, StrDia, StrHora, StrSeg, StrMin : string;
begin
    Result := 0;
    StrYear := Copy(Fecha,1,4);
    StrMes  := Copy(Fecha,5,2);
    StrDia  := Copy(Fecha,7,2);
    StrHora := Copy(Fecha,9,2);
    StrMin  := Copy(Fecha,11,2);
    StrSeg  := Copy(Fecha,13,2);

    if StrHora = EmptyStr then Strhora := '00';	     // SS_1015_PDO_20120614
    if StrMin  = EmptyStr then StrMin := '00';       // SS_1015_PDO_20120614
    if StrSeg  = EmptyStr then StrSeg := '00';       // SS_1015_PDO_20120614

    if  EsNumero(StrYear) and EsNumero(StrMes) and EsNumero(StrDia) and
        EsNumero(StrHora) and EsNumero(StrMin) and EsNumero(StrSeg) then
    begin
        try
            Result :=   EncodeDate(StrToInt(StrYear), StrToInt(StrMes), StrToInt(StrDia)) +
                            EncodeTime(StrToInt(StrHora), StrToInt(StrMin), StrToInt(StrSeg),0);
        except
            Result := 0;
        end;
    end;
end;

{---------------------------------------------------------------------------------
        	FormCreate

 Author			: mbecerra
 Date			: 31-Marzo-2011
 Description	:   Permite crear la lista de concesionarias cuyos
                    tr�nsitos los factura CN
----------------------------------------------------------------------------------}
procedure TMainForm.FormCreate(Sender: TObject);
begin
    {$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF}

    FListaConcesionariasLasFacturaCN := TStringList.Create;
    qryTiposTransacciones := TADOQuery.Create(nil); // SS_1015_CQU_20120427
end;

{---------------------------------------------------------------------------------
        	FormDestroy

 Author			: mbecerra
 Date			: 31-Marzo-2011
 Description	:   Quita de memoria la Lista de concesionarias cuyos
                    tr�nsitos los factura CN
----------------------------------------------------------------------------------}
procedure TMainForm.FormDestroy(Sender: TObject);
begin
    FListaConcesionariasLasFacturaCN.Free;
end;

{---------------------------------------------------------------------------------
        	Inicializar

 Author			: mbecerra
 Date			: 06-Agosto-2010
 Description	: Inicializa las variables de la carga inicial del cliente FTP
----------------------------------------------------------------------------------}
function TMainForm.Inicializar;
resourcestring
  MSG_INSTALL						= 'Instalar';
  MSG_UNINSTALL						= 'Desinstalar';
  MSG_SERVICE_SUCCESS_INSTALLING	= 'Servicio instalado con �xito';
  MSG_SERVICE_SUCCESS_UNINSTALLING	= 'Servicio desinstalado con �xito';
  MSG_SERVICE_ERROR_INSTALLING		= 'Error al instalar el Servicio %s:' + CRLF + '%s';
  MSG_SERVICE_ERROR_UNINSTALLING	= 'Error al desinstalar el Servicio %s:' + CRLF + '%s';
  MSG_SERVICE_CANNOT_START			= 'El servicio no pudo iniciarse debido al siguiente Error: %s';
  MSG_SERVICE_SUCCESS_STARTED		= 'Servicio iniciado con �xito';

var
	ErrorCode: DWORD;
    NombreServicio : string;
begin
    Result := True;
    NombreServicio  := dpsServiceImportarArchivosOtrasConce.ServiceName;

	if FindCmdLineSwitch('install', ['/', '-'], True) then begin	// Instalaci�n
		if InstallServices(ErrorCode, NombreServicio) then begin
			MsgBox(MSG_SERVICE_SUCCESS_INSTALLING, MSG_INSTALL, MB_ICONINFORMATION);
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_INSTALLING , '');
		end
        else begin
			MsgBox(Format(MSG_SERVICE_ERROR_INSTALLING,  [NombreServicio, SysErrorMessage(ErrorCode)]),  MSG_INSTALL, MB_ICONSTOP);
            EventLogReportEvent(elError, Format(MSG_SERVICE_ERROR_INSTALLING,  [NombreServicio, SysErrorMessage(ErrorCode)]) , '');
		end;
	end else if FindCmdLineSwitch('uninstall', ['/', '-'], True) then begin	// Desinstalaci�n
		if UninstallServices(ErrorCode, NombreServicio) then begin
			MsgBox(MSG_SERVICE_SUCCESS_UNINSTALLING, MSG_UNINSTALL, MB_ICONINFORMATION);
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_UNINSTALLING , '');
		end
        else begin
			MsgBox(Format(MSG_SERVICE_ERROR_UNINSTALLING, [NombreServicio, SysErrorMessage(ErrorCode)]),  MSG_UNINSTALL, MB_ICONSTOP);
            EventLogReportEvent(elError, Format(MSG_SERVICE_ERROR_UNINSTALLING,  [NombreServicio, SysErrorMessage(ErrorCode)]) , '');
		end;
	end
    else begin		// Ejecuci�n
		Result := StartServices;
		if Result then begin
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_STARTED , '');
			if not IsServiceApplication then Show;
		end
        else begin
			EventLogReportEvent(elError,Format(MSG_SERVICE_CANNOT_START, [SysErrorMessage(GetLastError())]), '');
		end;
    end;
end;

{---------------------------------------------------------------------------------
        	dpsServiceImportarArchivosOtrasConceStart

 Author			: mbecerra
 Date			: 06-Agosto-2010
 Description	: Inicializa los recursos del servicio
----------------------------------------------------------------------------------}
procedure TMainForm.dpsServiceImportarArchivosOtrasConceStart(Sender: TObject;
  var Started: Boolean; var ExitCode, SpecificExitCode: Cardinal);
resourcestring
    MSG_SRV_EXITO	= 'Servicio iniciado con �xito';
begin
 	MoveToMyOwnDir();
    Started := True;
    FIntervaloMinutos := 1;
    FContadorMinutos := 0;

    //Configurar el servidor de correo
    if IdSMTPEnviarCorreo.Connected then IdSMTPEnviarCorreo.Disconnect();

    IdSMTPEnviarCorreo.Password := '';
    IdSMTPEnviarCorreo.Username := '';
    IdSMTPEnviarCorreo.Port := 25;

    EventLogReportEvent(elInformation, MSG_SRV_EXITO, '');
end;

{---------------------------------------------------------------------------------
        	dpsServiceImportarArchivosOtrasConceStop

 Author			: mbecerra
 Date			: 10-Junio-2010
 Description	: Deja registro del t�rmino del servicio
----------------------------------------------------------------------------------}
procedure TMainForm.dpsServiceImportarArchivosOtrasConceStop(Sender: TObject);
resourcestring
    MSG_SRV_EXITO	= 'Servicio ha sido detenido con �xito';
begin
    EventLogReportEvent(elInformation, MSG_SRV_EXITO, '');
end;

{---------------------------------------------------------------------------------
        	ConectarBase

 Author			: mbecerra
 Date			: 06-Agosto-2010
 Description	: Intenta conectarse a la base de datos del CAC
----------------------------------------------------------------------------------}
function TMainForm.ConectarBase;
resourcestring
    MSG_CONNECT_ERROR = 'Ha ocurrido un error conectando a la base de datos. Detalle: %s';
begin
    Result := False;
    try
        if not DMConnections.BaseCAC.Connected then begin
            DMConnections.SetUp;
            DMConnections.BaseCAC.Connected := True;
        end;
        Result := True;
    except on e: Exception do begin
            EventLogReportEvent(elError, Format(MSG_CONNECT_ERROR, [e.Message]), '');
        end;
    end;
end;

{---------------------------------------------------------------------------------
        	LeerParametrosGenerales

 Author			: mbecerra
 Date			: 06-Agosto-2010
 Description	: Lee todos los parametros generales
                	Devuelve verdadero si los ley� todos, falso en caso contrario
----------------------------------------------------------------------------------}
function TMainForm.LeerParametrosGenerales;
resourcestring
	INTERVALO_IMPORTAR_ARCHIVO	= 'INTERVALO_IMPORTAR_ARCHIVO_TTOS';
    MSG_ERROR_PARAMETRO			= 'No se pudo leer el par�metro general %s';
    MSG_CARPETA_NO_EXISTE		= 'No existe la carpeta %s';
    MSG_SEGUNDOS				= '.Se configura cada una hora (60 minutos)';
    MSG_PARAMETRO_ENTRADA		= 'DIR_ARCHIVOS_TTOS';
    MSG_PARAMETRO_PROCESADOS	= 'DIR_ARCHIVOS_TTOS_PROCESADOS';
    MSG_PARAMETRO_DUPLICADOS    = 'DIR_ARCHIVOS_TTOS_DUPLICADOS';
    MSG_NOMBRE_ARCHIVO			= 'NOMBRE_ARCHIVO_TRX_TTOS';

    //MSG_PARAMETRO_ENTRADA_FOLIOS    = 'DIR_ARCHIVOS_FOLIOS';                    //PAR0133-NDR-20110326
    //MSG_PARAMETRO_PROCESADOS_FOLIOS = 'DIR_ARCHIVOS_FOLIOS_PROCESADOS';         //PAR0133-NDR-20110326
    //MSG_PARAMETRO_DUPLICADOS_FOLIOS = 'DIR_ARCHIVOS_FOLIOS_DUPLICADOS';         //PAR0133-NDR-20110326
    //MSG_NOMBRE_ARCHIVO_FOLIOS       = 'NOMBRE_ARCHIVO_TRX_FOLIOS';              //PAR0133-NDR-20110326
    //MSG_PARAMETRO_ENTRADA_PAGOS     = 'DIR_ARCHIVOS_PAGOS';                     //PAR0133-NDR-20110326
    //MSG_PARAMETRO_PROCESADOS_PAGOS  = 'DIR_ARCHIVOS_PAGOS_PROCESADOS';          //PAR0133-NDR-20110326
    //MSG_PARAMETRO_DUPLICADOS_PAGOS  = 'DIR_ARCHIVOS_PAGOS_DUPLICADOS';           //PAR0133-NDR-20110326
    //MSG_NOMBRE_ARCHIVO_PAGOS        = 'NOMBRE_ARCHIVO_TRX_PAGOS';               //PAR0133-NDR-20110326

    MSG_PARAMETRO_ENTRADA_MOVCTA     = 'DIR_ARCHIVOS_MOV_CTAS';                 //SS_1015_MBE_20120206
    MSG_PARAMETRO_PROCESADOS_MOVCTA  = 'DIR_ARCHIVOS_MOV_CTAS_PROCESADOS';      //SS_1015_MBE_20120206
    MSG_PARAMETRO_DUPLICADOS_MOVCTA  = 'DIR_ARCHIVOS_MOV_CTAS_DUPLICADOS';      //SS_1015_MBE_20120206
    MSG_NOMBRE_ARCHIVO_MOVCTA        = 'NOMBRE_ARCHIVO_MOV_CTAS';               //SS_1015_MBE_20120206

    // Inicio Bloque SS_1015_CQU_20120510
    MSG_PARAMETRO_RECHAZADOS_MOVCTA     = 'DIR_ARCHIVOS_MOV_CTAS_RECHAZADOS';
    MSG_PARAMETRO_RECHAZADOS_TTOS       = 'DIR_ARCHIVOS_TTOS_RECHAZADOS';

    //MSG_NOMBRE_ARCHIVO_RECHAZADO_MOVCTA = 'NOMBRE_ARCHIVO_MOV_CTAS_RECHAZADOS';   // SS_1015_CQU_20120711
    //MSG_NOMBRE_ARCHIVO_RECHAZADO_TTOS   = 'NOMBRE_ARCHIVO_TRX_TTOS_RECHAZADOS';   // SS_1015_CQU_20120711
    // Fin Bloque SS_1015_CQU_20120510

    MSG_PARAMETRO_ENTRADA_SALDOINI   = 'DIR_ARCHIVOS_SALDO_INI';                //SS-1015-NDR-20120510
    MSG_PARAMETRO_PROCESADOS_SALDOINI= 'DIR_ARCHIVOS_SALDO_INI_PROCESADOS';     //SS-1015-NDR-20120510
    MSG_PARAMETRO_DUPLICADOS_SALDOINI= 'DIR_ARCHIVOS_SALDO_INI_DUPLICADOS';     //SS-1015-NDR-20120510
    MSG_NOMBRE_ARCHIVO_SALDOINI      = 'NOMBRE_ARCHIVO_SALDO_INI';              //SS-1015-NDR-20120510

    MSG_PARAMETRO_ENTRADA_CUOTAS     = 'DIR_ARCHIVOS_CUOTAS_OC';                //SS-1015-NDR-20120510
    MSG_PARAMETRO_PROCESADOS_CUOTAS  = 'DIR_ARCHIVOS_CUOTAS_OC_PROCESADOS';     //SS-1015-NDR-20120510
    MSG_PARAMETRO_DUPLICADOS_CUOTAS  = 'DIR_ARCHIVOS_CUOTAS_OC_DUPLICADOS';     //SS-1015-NDR-20120510
    MSG_NOMBRE_ARCHIVO_CUOTAS        = 'NOMBRE_ARCHIVO_CUOTAS_OC';              //SS-1015-NDR-20120510

    // Inicio Bloque SS_1015_CQU_20120514
    MSG_PARAMETRO_RECHAZADOS_CUOTAS     = 'DIR_ARCHIVOS_CUOTAS_OC_RECHAZADOS';
    MSG_PARAMETRO_RECHAZADOS_SDOINI     = 'DIR_ARCHIVOS_SALDO_INI_RECHAZADOS';

    //MSG_NOMBRE_ARCHIVO_RECHAZADO_CUOTAS = 'NOMBRE_ARCHIVO_CUOTAS_OC_RECHAZADAS';  // SS_1015_CQU_20120711
    //MSG_NOMBRE_ARCHIVO_RECHAZADO_SDOINI = 'NOMBRE_ARCHIVO_SALDO_INI_RECHAZADOS';  // SS_1015_CQU_20120711
    // Fin Bloque SS_1015_CQU_20120514

    EMAIL_FROM					= 'ALARMAEVENTO_EMAIL_FROM';
    EMAIL_TO					= 'ALARMAEVENTO_EMAIL_TO';
    EMAIL_SMTP					= 'EMAIL_SERVIDORSMTP';

begin
	Result := True;
	//Intervalo en minutos
    if not ObtenerParametroGeneral(	DMConnections.BaseCAC, INTERVALO_IMPORTAR_ARCHIVO, FIntervaloMinutos) then begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [INTERVALO_IMPORTAR_ARCHIVO]) + MSG_SEGUNDOS, '');
        FIntervaloMinutos := 60; //si no se encuentra, se asume un valor
    end;

    //Carpeta de Entrada
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_ENTRADA, FCarpetaEntrada) then begin
        FCarpetaEntrada := GoodDir(FCarpetaEntrada);
        if not DirectoryExists(FCarpetaEntrada) then begin
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaEntrada]), '');
            Result := False;
        end;
    end
    else begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_ENTRADA]), '');
        Result := False;
    end;

    //Carpeta de Procesados
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_PROCESADOS, FCarpetaProcesados) then begin
    	FCarpetaProcesados := GoodDir(FCarpetaProcesados);
        if not DirectoryExists(FCarpetaProcesados) then begin
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaProcesados]), '');
            Result := False;
        end;
    end
    else begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_PROCESADOS]), '');
        Result := False;
    end;

    //Carpeta de Duplicados
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_DUPLICADOS, FCarpetaDuplicados) then begin
    	FCarpetaDuplicados := GoodDir(FCarpetaDuplicados);
        if not DirectoryExists(FCarpetaDuplicados) then begin
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaDuplicados]), '');
            Result := False;
        end;
    end
    else begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_DUPLICADOS]), '');
        Result := False;
    end;

    //Nombre del archivo
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO, FNomArchParametro) then begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO]), '');
        Result := False;
    end;


    //Rev.1 / 26-Marzo-2011 / Nelson Droguett Sierra ----------------------------------------------------------------------------------------------------------
                                                                                                                                         //PAR0133-NDR-20110326
    //Carpeta de Entrada Folios                                                                                                          //PAR0133-NDR-20110326
    //if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_ENTRADA_FOLIOS, FCarpetaEntradaFolios) then begin                    //PAR0133-NDR-20110326
    //    FCarpetaEntradaFolios := GoodDir(FCarpetaEntradaFolios);                                                                         //PAR0133-NDR-20110326
    //    if not DirectoryExists(FCarpetaEntradaFolios) then begin                                                                         //PAR0133-NDR-20110326
    //        EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaEntradaFolios]), '');                                    //PAR0133-NDR-20110326
    //        Result := False;                                                                                                             //PAR0133-NDR-20110326
    //    end;                                                                                                                             //PAR0133-NDR-20110326
    //end                                                                                                                                  //PAR0133-NDR-20110326
    //else begin                                                                                                                           //PAR0133-NDR-20110326
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_ENTRADA_FOLIOS]), '');                                   //PAR0133-NDR-20110326
    //    Result := False;                                                                                                                 //PAR0133-NDR-20110326
    //end;                                                                                                                                 //PAR0133-NDR-20110326
                                                                                                                                         //PAR0133-NDR-20110326
    //Carpeta de Folios Procesados                                                                                                       //PAR0133-NDR-20110326
    //if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_PROCESADOS_FOLIOS, FCarpetaProcesadosFolios) then begin              //PAR0133-NDR-20110326
    //	FCarpetaProcesadosFolios := GoodDir(FCarpetaProcesadosFolios);                                                                   //PAR0133-NDR-20110326
    //    if not DirectoryExists(FCarpetaProcesadosFolios) then begin                                                                      //PAR0133-NDR-20110326
    //        EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaProcesadosFolios]), '');                                 //PAR0133-NDR-20110326
    //        Result := False;                                                                                                             //PAR0133-NDR-20110326
    //    end;                                                                                                                             //PAR0133-NDR-20110326
    //end                                                                                                                                  //PAR0133-NDR-20110326
    //else begin                                                                                                                           //PAR0133-NDR-20110326
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_PROCESADOS_FOLIOS]), '');                                //PAR0133-NDR-20110326
    //    Result := False;                                                                                                                 //PAR0133-NDR-20110326
    //end;                                                                                                                                 //PAR0133-NDR-20110326
                                                                                                                                         //PAR0133-NDR-20110326
    //Carpeta de Folios Duplicados                                                                                                       //PAR0133-NDR-20110326
    //if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_DUPLICADOS_FOLIOS, FCarpetaDuplicadosFolios) then begin              //PAR0133-NDR-20110326
    //	FCarpetaDuplicadosFolios := GoodDir(FCarpetaDuplicadosFolios);                                                                   //PAR0133-NDR-20110326
    //    if not DirectoryExists(FCarpetaDuplicadosFolios) then begin                                                                      //PAR0133-NDR-20110326
    //        EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaDuplicadosFolios]), '');                                 //PAR0133-NDR-20110326
    //        Result := False;                                                                                                             //PAR0133-NDR-20110326
    //    end;                                                                                                                             //PAR0133-NDR-20110326
    //end                                                                                                                                  //PAR0133-NDR-20110326
    //else begin                                                                                                                           //PAR0133-NDR-20110326
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_DUPLICADOS_FOLIOS]), '');                                //PAR0133-NDR-20110326
    //    Result := False;                                                                                                                 //PAR0133-NDR-20110326
    //end;                                                                                                                                 //PAR0133-NDR-20110326
                                                                                                                                         //PAR0133-NDR-20110326
    //Nombre del archivo Folios                                                                                                          //PAR0133-NDR-20110326
    //if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_FOLIOS, FNomArchParametroFolios) then begin                 //PAR0133-NDR-20110326
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_FOLIOS]), '');                                      //PAR0133-NDR-20110326
    //    Result := False;                                                                                                                 //PAR0133-NDR-20110326
    //end;                                                                                                                                 //PAR0133-NDR-20110326
                                                                                                                                         //PAR0133-NDR-20110326
     //Carpeta de Entrada Pagos                                                                                                          //PAR0133-NDR-20110326
    //if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_ENTRADA_PAGOS, FCarpetaEntradaPagos) then begin                      //PAR0133-NDR-20110326
    //    FCarpetaEntradaPagos := GoodDir(FCarpetaEntradaPagos);                                                                           //PAR0133-NDR-20110326
    //    if not DirectoryExists(FCarpetaEntradaPagos) then begin                                                                          //PAR0133-NDR-20110326
    //        EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaEntradaPagos]), '');                                     //PAR0133-NDR-20110326
    //        Result := False;                                                                                                             //PAR0133-NDR-20110326
    //    end;                                                                                                                             //PAR0133-NDR-20110326
    //end                                                                                                                                  //PAR0133-NDR-20110326
    //else begin                                                                                                                           //PAR0133-NDR-20110326
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_ENTRADA_PAGOS]), '');                                    //PAR0133-NDR-20110326
    //    Result := False;                                                                                                                 //PAR0133-NDR-20110326
    //end;                                                                                                                                 //PAR0133-NDR-20110326
                                                                                                                                         //PAR0133-NDR-20110326
    //Carpeta de Pagos Procesados                                                                                                        //PAR0133-NDR-20110326
    //if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_PROCESADOS_PAGOS, FCarpetaProcesadosPagos) then begin                //PAR0133-NDR-20110326
    //	FCarpetaProcesadosPagos := GoodDir(FCarpetaProcesadosPagos);                                                                     //PAR0133-NDR-20110326
    //    if not DirectoryExists(FCarpetaProcesadosPagos) then begin                                                                       //PAR0133-NDR-20110326
    //        EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaProcesadosPagos]), '');                                  //PAR0133-NDR-20110326
    //        Result := False;                                                                                                             //PAR0133-NDR-20110326
    //    end;                                                                                                                             //PAR0133-NDR-20110326
    //end                                                                                                                                  //PAR0133-NDR-20110326
    //else begin                                                                                                                           //PAR0133-NDR-20110326
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_PROCESADOS_PAGOS]), '');                                 //PAR0133-NDR-20110326
    //    Result := False;                                                                                                                 //PAR0133-NDR-20110326
    //end;                                                                                                                                 //PAR0133-NDR-20110326
                                                                                                                                         //PAR0133-NDR-20110326
    //Carpeta de Pagos Duplicados                                                                                                        //PAR0133-NDR-20110326
    //if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_DUPLICADOS_PAGOS, FCarpetaDuplicadosPagos) then begin                //PAR0133-NDR-20110326
    //	FCarpetaDuplicadosPagos := GoodDir(FCarpetaDuplicadosPagos);                                                                     //PAR0133-NDR-20110326
    //    if not DirectoryExists(FCarpetaDuplicadosPagos) then begin                                                                       //PAR0133-NDR-20110326
    //        EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaDuplicadosPagos]), '');                                  //PAR0133-NDR-20110326
    //        Result := False;                                                                                                             //PAR0133-NDR-20110326
    //    end;                                                                                                                             //PAR0133-NDR-20110326
    //end                                                                                                                                  //PAR0133-NDR-20110326
    //else begin                                                                                                                           //PAR0133-NDR-20110326
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_DUPLICADOS_PAGOS]), '');                                 //PAR0133-NDR-20110326
    //    Result := False;                                                                                                                 //PAR0133-NDR-20110326
    //end;                                                                                                                                 //PAR0133-NDR-20110326
                                                                                                                                         //PAR0133-NDR-20110326
    //Nombre del archivo Pagos                                                                                                           //PAR0133-NDR-20110326
    //if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_PAGOS, FNomArchParametroPagos) then begin                   //PAR0133-NDR-20110326
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_PAGOS]), '');                                       //PAR0133-NDR-20110326
    //    Result := False;                                                                                                                 //PAR0133-NDR-20110326
    //end;                                                                                                                                 //PAR0133-NDR-20110326
    //FinRev.1-------------------------------------------------------------------------------------------------------------------------------------------------



     //Carpeta de Entrada MovimientosCuentas de Otras Concesionarias                                                                     //SS_1015_MBE_20120206
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_ENTRADA_MOVCTA, FCarpetaEntradaMovCta) then begin                    //SS_1015_MBE_20120206
        FCarpetaEntradaMovCta := GoodDir(FCarpetaEntradaMovCta);                                                                         //SS_1015_MBE_20120206
        if not DirectoryExists(FCarpetaEntradaMovCta) then begin                                                                         //SS_1015_MBE_20120206
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaEntradaMovCta]), '');                                    //SS_1015_MBE_20120206
            Result := False;                                                                                                             //SS_1015_MBE_20120206
        end;                                                                                                                             //SS_1015_MBE_20120206
    end                                                                                                                                  //SS_1015_MBE_20120206
    else begin                                                                                                                           //SS_1015_MBE_20120206
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_ENTRADA_MOVCTA]), '');                                   //SS_1015_MBE_20120206
        Result := False;                                                                                                                 //SS_1015_MBE_20120206
    end;                                                                                                                                 //SS_1015_MBE_20120206


   //Carpeta de MovimientosCuentas Procesados                                                                                            //SS_1015_MBE_20120206
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_PROCESADOS_MOVCTA, FCarpetaProcesadosMovCta) then begin              //SS_1015_MBE_20120206
    	FCarpetaProcesadosMovCta := GoodDir(FCarpetaProcesadosMovCta);                                                                   //SS_1015_MBE_20120206
        if not DirectoryExists(FCarpetaProcesadosMovCta) then begin                                                                      //SS_1015_MBE_20120206
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaProcesadosMovCta]), '');                                 //SS_1015_MBE_20120206
            Result := False;                                                                                                             //SS_1015_MBE_20120206
        end;                                                                                                                             //SS_1015_MBE_20120206
    end                                                                                                                                  //SS_1015_MBE_20120206
    else begin                                                                                                                           //SS_1015_MBE_20120206
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_PROCESADOS_MOVCTA]), '');                                //SS_1015_MBE_20120206
        Result := False;                                                                                                                 //SS_1015_MBE_20120206
    end;                                                                                                                                 //SS_1015_MBE_20120206


    //Carpeta de MovimientosCuentas Duplicados                                                                                           //SS_1015_MBE_20120206
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_DUPLICADOS_MOVCTA, FCarpetaDuplicadosMovCta) then begin              //SS_1015_MBE_20120206
    	FCarpetaDuplicadosMovCta := GoodDir(FCarpetaDuplicadosMovCta);                                                                   //SS_1015_MBE_20120206
        if not DirectoryExists(FCarpetaDuplicadosMovCta) then begin                                                                      //SS_1015_MBE_20120206
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaDuplicadosMovCta]), '');                                 //SS_1015_MBE_20120206
            Result := False;                                                                                                             //SS_1015_MBE_20120206
        end;                                                                                                                             //SS_1015_MBE_20120206
    end                                                                                                                                  //SS_1015_MBE_20120206
    else begin                                                                                                                           //SS_1015_MBE_20120206
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_DUPLICADOS_MOVCTA]), '');                                //SS_1015_MBE_20120206
        Result := False;                                                                                                                 //SS_1015_MBE_20120206
    end;                                                                                                                                 //SS_1015_MBE_20120206


    //Prefijo del archivo de Movimientos Cuentas                                                                                         //SS_1015_MBE_20120206
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_MOVCTA, FNomArchParametroMovCta) then begin                 //SS_1015_MBE_20120206
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_MOVCTA]), '');                                      //SS_1015_MBE_20120206
        Result := False;                                                                                                                 //SS_1015_MBE_20120206
    end;                                                                                                                                 //SS_1015_MBE_20120206


    //Origen del correo de alertas
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, EMAIL_FROM, FEmailFrom) then begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [EMAIL_FROM]), '');
        FEmailFrom := 'alerta@cnorte.cl';
    end;

    //destinatarios del correo de alertas
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, EMAIL_TO, FEmailTo) then begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [EMAIL_TO]), '');
        FEmailTo := 'mvillavicencio@cnorte.cl; beni@cnorte.cl; sarmiento@cnorte.cl';
    end;

    //servidor smtp del correo de alertas
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, EMAIL_SMTP, FEmailServerSMTP) then begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [EMAIL_SMTP]), '');
        FEmailServerSMTP := '10.10.10.4';
    end;

    // Inicio Bloque SS_1015_CQU_20120510
    // Carpeta Transitos Rechazados
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_RECHAZADOS_TTOS, FCarpetaRechazadasTransac) then begin
    	FCarpetaRechazadasTransac := GoodDir(FCarpetaRechazadasTransac);
        if not DirectoryExists(FCarpetaRechazadasTransac) then begin
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaRechazadasTransac]), '');
            Result := False;
        end;
    end
    else begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_RECHAZADOS_TTOS]), '');
        Result := False;
    end;
    // Prefijo Archivos Transacciones Rechazados
    //if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_RECHAZADO_TTOS, FNombreRechazadasTransac) then begin // SS_1015_CQU_20120711
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_RECHAZADO_TTOS]), '');                         // SS_1015_CQU_20120711
    //    Result := False;                                                                                                          // SS_1015_CQU_20120711
    //end;                                                                                                                          // SS_1015_CQU_20120711
    FNombreRechazadasTransac := 'RECHAZADO';                                                                                        // SS_1015_CQU_20120711

    // Carpeta Movimientos Rechazados
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_RECHAZADOS_MOVCTA, FCarpetaRechazadosMovCta) then begin
    	FCarpetaRechazadosMovCta := GoodDir(FCarpetaRechazadosMovCta);
        if not DirectoryExists(FCarpetaRechazadosMovCta) then begin
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaRechazadosMovCta]), '');
            Result := False;
        end;
    end
    else begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_RECHAZADOS_MOVCTA]), '');
        Result := False;
    end;
    // Prefijo Archivos Movimientos Rechazados
    //if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_RECHAZADO_MOVCTA, FNombreRechazadosMovCta) then begin    // SS_1015_CQU_20120711
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_RECHAZADO_MOVCTA]), '');                           // SS_1015_CQU_20120711
    //    Result := False;                                                                                                              // SS_1015_CQU_20120711
    //end;                                                                                                                              // SS_1015_CQU_20120711
    FNombreRechazadosMovCta := 'RECHAZADO';                                                                                             // SS_1015_CQU_20120711

     //Carpeta de Entrada SaldoInicial de Otras Concesionarias                                                                           //SS-1015-NDR-20120510
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_ENTRADA_SALDOINI, FCarpetaEntradaSaldosIni) then begin               //SS-1015-NDR-20120510
        FCarpetaEntradaSaldosIni := GoodDir(FCarpetaEntradaSaldosIni);                                                                   //SS-1015-NDR-20120510
        if not DirectoryExists(FCarpetaEntradaSaldosIni) then begin                                                                      //SS-1015-NDR-20120510
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaEntradaSaldosIni]), '');                                 //SS-1015-NDR-20120510
            Result := False;                                                                                                             //SS-1015-NDR-20120510
        end;                                                                                                                             //SS-1015-NDR-20120510
    end                                                                                                                                  //SS-1015-NDR-20120510
    else begin                                                                                                                           //SS-1015-NDR-20120510
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_ENTRADA_SALDOINI]), '');                                   //SS-1015-NDR-20120510
        Result := False;                                                                                                                 //SS-1015-NDR-20120510
    end;                                                                                                                                 //SS-1015-NDR-20120510

   //Carpeta de SaldoInicial Procesados                                                                                                  //SS-1015-NDR-20120510
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_PROCESADOS_SALDOINI, FCarpetaProcesadosSaldosIni) then begin         //SS-1015-NDR-20120510
    	FCarpetaProcesadosSaldosIni := GoodDir(FCarpetaProcesadosSaldosIni);                                                               //SS-1015-NDR-20120510
        if not DirectoryExists(FCarpetaProcesadosSaldosIni) then begin                                                                   //SS-1015-NDR-20120510
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaProcesadosSaldosIni]), '');                              //SS-1015-NDR-20120510
            Result := False;                                                                                                             //SS-1015-NDR-20120510
        end;                                                                                                                             //SS-1015-NDR-20120510
    end                                                                                                                                  //SS-1015-NDR-20120510
    else begin                                                                                                                           //SS-1015-NDR-20120510
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_PROCESADOS_SALDOINI]), '');                                //SS-1015-NDR-20120510
        Result := False;                                                                                                                 //SS-1015-NDR-20120510
    end;                                                                                                                                 //SS-1015-NDR-20120510

    //Carpeta de SaldoInicial Duplicados                                                                                                 //SS-1015-NDR-20120510
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_DUPLICADOS_SALDOINI, FCarpetaDuplicadosSaldosIni) then begin         //SS-1015-NDR-20120510
    	FCarpetaDuplicadosSaldosIni := GoodDir(FCarpetaDuplicadosSaldosIni);                                                               //SS-1015-NDR-20120510
        if not DirectoryExists(FCarpetaDuplicadosSaldosIni) then begin                                                                   //SS-1015-NDR-20120510
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaDuplicadosSaldosIni]), '');                              //SS-1015-NDR-20120510
            Result := False;                                                                                                             //SS-1015-NDR-20120510
        end;                                                                                                                             //SS-1015-NDR-20120510
    end                                                                                                                                  //SS-1015-NDR-20120510
    else begin                                                                                                                           //SS-1015-NDR-20120510
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_DUPLICADOS_SALDOINI]), '');                                //SS-1015-NDR-20120510
        Result := False;                                                                                                                 //SS-1015-NDR-20120510
    end;                                                                                                                                 //SS-1015-NDR-20120510

    //Prefijo del archivo de SaldoInicial                                                                                                //SS-1015-NDR-20120510
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_SALDOINI, FNomArchParametroSaldoIni) then begin             //SS-1015-NDR-20120510
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_SALDOINI]), '');                                      //SS-1015-NDR-20120510
        Result := False;                                                                                                                 //SS-1015-NDR-20120510
    end;                                                                                                                                 //SS-1015-NDR-20120510

     //Carpeta de Entrada Cuotas de Otras Concesionarias                                                                                 //SS-1015-NDR-20120510
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_ENTRADA_CUOTAS, FCarpetaEntradaCuotas) then begin                    //SS-1015-NDR-20120510
        FCarpetaEntradaCuotas := GoodDir(FCarpetaEntradaCuotas);                                                                         //SS-1015-NDR-20120510
        if not DirectoryExists(FCarpetaEntradaCuotas) then begin                                                                         //SS-1015-NDR-20120510
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaEntradaCuotas]), '');                                    //SS-1015-NDR-20120510
            Result := False;                                                                                                             //SS-1015-NDR-20120510
        end;                                                                                                                             //SS-1015-NDR-20120510
    end                                                                                                                                  //SS-1015-NDR-20120510
    else begin                                                                                                                           //SS-1015-NDR-20120510
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_ENTRADA_CUOTAS]), '');                                     //SS-1015-NDR-20120510
        Result := False;                                                                                                                 //SS-1015-NDR-20120510
    end;                                                                                                                                 //SS-1015-NDR-20120510

   //Carpeta de Cuotas Procesados                                                                                                        //SS-1015-NDR-20120510
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_PROCESADOS_CUOTAS, FCarpetaProcesadosCuotas) then begin              //SS-1015-NDR-20120510
    	FCarpetaProcesadosCuotas := GoodDir(FCarpetaProcesadosCuotas);                                                                     //SS-1015-NDR-20120510
        if not DirectoryExists(FCarpetaProcesadosCuotas) then begin                                                                      //SS-1015-NDR-20120510
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaProcesadosCuotas]), '');                                 //SS-1015-NDR-20120510
            Result := False;                                                                                                             //SS-1015-NDR-20120510
        end;                                                                                                                             //SS-1015-NDR-20120510
    end                                                                                                                                  //SS-1015-NDR-20120510
    else begin                                                                                                                           //SS-1015-NDR-20120510
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_PROCESADOS_CUOTAS]), '');                                  //SS-1015-NDR-20120510
        Result := False;                                                                                                                 //SS-1015-NDR-20120510
    end;                                                                                                                                 //SS-1015-NDR-20120510

    //Carpeta de Cuotas Duplicados                                                                                                       //SS-1015-NDR-20120510
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_DUPLICADOS_CUOTAS, FCarpetaDuplicadosCuotas) then begin              //SS-1015-NDR-20120510
    	FCarpetaDuplicadosCuotas := GoodDir(FCarpetaDuplicadosCuotas);                                                                     //SS-1015-NDR-20120510
        if not DirectoryExists(FCarpetaDuplicadosCuotas) then begin                                                                      //SS-1015-NDR-20120510
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaDuplicadosCuotas]), '');                                 //SS-1015-NDR-20120510
            Result := False;                                                                                                             //SS-1015-NDR-20120510
        end;                                                                                                                             //SS-1015-NDR-20120510
    end                                                                                                                                  //SS-1015-NDR-20120510
    else begin                                                                                                                           //SS-1015-NDR-20120510
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_DUPLICADOS_CUOTAS]), '');                                  //SS-1015-NDR-20120510
        Result := False;                                                                                                                 //SS-1015-NDR-20120510
    end;                                                                                                                                 //SS-1015-NDR-20120510

    //Prefijo del archivo de SaldoInicial                                                                                                //SS-1015-NDR-20120510
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_CUOTAS, FNomArchParametroCuotas) then begin                 //SS-1015-NDR-20120510
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_CUOTAS]), '');                                        //SS-1015-NDR-20120510
        Result := False;                                                                                                                 //SS-1015-NDR-20120510
    end;                                                                                                                                 //SS-1015-NDR-20120510

    // Inicio Bloque SS_1015_CQU_20120514
    // Carpeta Cuotas Rechazadas
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_RECHAZADOS_CUOTAS, FCarpetaRechazadosCuotas) then begin
    	FCarpetaRechazadosCuotas := GoodDir(FCarpetaRechazadosCuotas);
        if not DirectoryExists(FCarpetaRechazadosCuotas) then begin
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaRechazadosCuotas]), '');
            Result := False;
        end;
    end
    else begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_RECHAZADOS_CUOTAS]), '');
        Result := False;
    end;
    // Prefijo Archivos Cuotas Rechazadas
    //if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_RECHAZADO_CUOTAS, FNombreRechazadosCuotas) then begin    // SS_1015_CQU_20120711
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_RECHAZADO_CUOTAS]), '');                           // SS_1015_CQU_20120711
    //    Result := False;                                                                                                              // SS_1015_CQU_20120711
    //end;                                                                                                                              // SS_1015_CQU_20120711
    FNombreRechazadosCuotas := 'RECHAZADO';                                                                                             // SS_1015_CQU_20120711

    // Carpeta Saldo Inicial Rechazados
    if ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_PARAMETRO_RECHAZADOS_SDOINI, FCarpetaRechazadosSaldosIni) then begin
    	FCarpetaRechazadosSaldosIni := GoodDir(FCarpetaRechazadosSaldosIni);
        if not DirectoryExists(FCarpetaRechazadosSaldosIni) then begin
            EventLogReportEvent(elError, Format(MSG_CARPETA_NO_EXISTE, [FCarpetaRechazadosSaldosIni]), '');
            Result := False;
        end;
    end
    else begin
    	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_PARAMETRO_RECHAZADOS_SDOINI]), '');
        Result := False;
    end;
    // Prefijo Archivos Saldo Inicial Rechazadas
    //if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_ARCHIVO_RECHAZADO_SDOINI, FNombreRechazadosSaldosIni) then begin // SS_1015_CQU_20120711
    //	EventLogReportEvent(elError, Format(MSG_ERROR_PARAMETRO, [MSG_NOMBRE_ARCHIVO_RECHAZADO_SDOINI]), '');                           // SS_1015_CQU_20120711
    //    Result := False;                                                                                                              // SS_1015_CQU_20120711
    //end;                                                                                                                              // SS_1015_CQU_20120711
    // Fin Bloque SS_1015_CQU_20120514                                                                                                  // SS_1015_CQU_20120711
    FNombreRechazadosSaldosIni := 'RECHAZADO';                                                                                          // SS_1015_CQU_20120711

end;

{---------------------------------------------------------------------------------
        	RegistrarTransitoRechazado

 Author			: mbecerra
 Date			: 27-Enero-2011
 Description	:
                    Deja registro del tr�nsito rechazado.  Retorna false
                    en caso de un error.

----------------------------------------------------------------------------------}
function TMainForm.RegistrarTransitoRechazado;
resourcestring
    MSG_MOTIVO  = 'El Rut %s no fue encontrado para ser facturado en CN';
begin
    try
        spAgregarTTORechOtraConcesio.Close;
        spAgregarTTORechOtraConcesio.Parameters.Refresh;
        spAgregarTTORechOtraConcesio.Parameters.ParamByName('@NombreArchivo').Value         := NombreArchivo;
		spAgregarTTORechOtraConcesio.Parameters.ParamByName('@NumCorrCO').Value             := IIf(EsNumero(NumCorrCO), NumCorrCO, Null);
		spAgregarTTORechOtraConcesio.Parameters.ParamByName('@CodigoConcesionaria').Value   := CodigoConcesionaria;
        spAgregarTTORechOtraConcesio.Parameters.ParamByName('@Importe').Value               := Importe;
		spAgregarTTORechOtraConcesio.Parameters.ParamByName('@FechaHoraTransito').Value     := FechaHoraTransito;
		spAgregarTTORechOtraConcesio.Parameters.ParamByName('@Motivo').Value                := Format(MSG_MOTIVO, [Rut]);
        spAgregarTTORechOtraConcesio.Parameters.ParamByName('@NumeroLinea').Value			:= NumeroLinea;
        spAgregarTTORechOtraConcesio.Parameters.ParamByName('@ContractSerialNumber').Value  := IIf(ContractSerialNumber <> '', ContractSerialNumber, NULL); // SS_1015_CQU_20120413
        spAgregarTTORechOtraConcesio.Parameters.ParamByName('@Context').Value               := IIf(Context <> '', Context, NULL);                           // SS_1015_CQU_20120413
        spAgregarTTORechOtraConcesio.Parameters.ParamByName('@Contract').Value              := IIf(Contract <> '', Contract, NULL);                         // SS_1015_CQU_20120413
        spAgregarTTORechOtraConcesio.Parameters.ParamByName('@IssuerIdentifier').Value      := IIf(IssuerIdentifier <> '', IssuerIdentifier, NULL);         // SS_1015_CQU_20120413
        spAgregarTTORechOtraConcesio.ExecProc;
        Result := True;
    except
    	Result := False;
    end;

    spAgregarTTORechOtraConcesio.Close;
end;

{---------------------------------------------------------------------------------
        	RegistrarRegistroRechazado

 Author			: mbecerra
 Date			: 27-Enero-2011
 Description	:
                    Deja registro de la l�nea rechazada.  Retorna false
                    en caso de un error.

Firma           : SS_1015_CQU_20120510
Description     : Modifica el tipo de transacci�n ya que sus valores se obtienen de otra tabla
----------------------------------------------------------------------------------}
function TMainForm.RegistrarRegistroRechazado;
const                                                                                       // SS_1006_1015_CQU_20121002
    MSG_ERROR_REGRECHAZ = 'Error al grabar un registro rechazado, mensaje del sistema: %s'; // SS_1006_1015_CQU_20121002
var                                                                                         // SS_1006_1015_CQU_20121002
    IDTransaccionTMP : Int64;                                                               // SS_1006_1015_CQU_20121002
begin
    // Inicio Bloque SS_1015_CQU_20120510
    {
    // Obtener el Tipo de Transacci�n SS_1015_CQU_20120412
    if (FNomArchTipoTrx = TIPO_ESTA) then
        FTipoTransaccion := ObtenerTipoTransaccion('Estacionamiento')
    else
        FTipoTransaccion := ObtenerTipoTransaccion('Peaje');
    }
    if ((FNomArchTipoTrx = TIPO_MLFF) OR (FNomArchTipoTrx = TIPO_PEAJ)) then
        FTipoTransaccion := ObtenerTipoRegistroFolioyPago('1')
    else if (FNomArchTipoTrx = TIPO_ESTA) then
        FTipoTransaccion := ObtenerTipoRegistroFolioyPago('2')
    else if (FNomArchTipoTrx = TIPO_MOV) then
    //    FTipoTransaccion := ObtenerTipoRegistroFolioyPago('3'); // SS_1015_CQU_20120514
        FTipoTransaccion := ObtenerTipoRegistroFolioyPago('3')    // SS_1015_CQU_20120514
    // Fin Bloque SS_1015_CQU_20120510
    // Inicio Bloque SS_1015_CQU_20120514
    else if (FNomArchTipoTrx = TIPO_SALD) then
        FTipoTransaccion := ObtenerTipoRegistroFolioyPago('7')
    else if (FNomArchTipoTrx = TIPO_CUOT) then
        FTipoTransaccion := ObtenerTipoRegistroFolioyPago('9');
    // Fin Bloque SS_1015_CQU_20120514
    try
        spAgregarRegRechOtrasConcesio.Close;
        spAgregarRegRechOtrasConcesio.Parameters.Refresh;
        spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@NombreArchivo').Value            := NombreArchivo;
        spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@NumeroLinea').Value              := NumeroLinea;
        spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@MotivoRechazo').Value            := Motivo;
        //spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@NumCorrCO').Value		        := IIf(EsNumero(NumCorrCO), NumCorrCO, NULL);   // SS_1006_1015_CQU_20120926
        //spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@IdentificadorTransaccion').Value	:= IIf(EsNumero(NumCorrCO), NumCorrCO, NULL);   // SS_1006_1015_CQU_20120926    // SS_1006_1015_CQU_20121002
        if EsNumero(NumCorrCO) and TryStrToInt64(NumCorrCO, IDTransaccionTMP) then                                                                                                  // SS_1006_1015_CQU_20121002
            spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@IdentificadorTransaccion').Value := IDTransaccionTMP                                                             // SS_1006_1015_CQU_20121002
        else                                                                                                                                                                        // SS_1006_1015_CQU_20121002
            spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@IdentificadorTransaccion').Value := Null;                                                                        // SS_1006_1015_CQU_20121002

        spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@CodigoConcesionaria').Value      := FCodigoConcesionariaOrigen;			// SS_1015_CQU_20120412
        //spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@CodigoTipoTransaccion').Value    := FTipoTransaccion; 					// SS_1015_CQU_20120412 // SS_1015_CQU_20120510
        spAgregarRegRechOtrasConcesio.Parameters.ParamByName('@TipoRegistroFolioyPago').Value    := FTipoTransaccion; 					// SS_1015_CQU_20120510

        spAgregarRegRechOtrasConcesio.ExecProc;
        Result := True;
    except
    	//Result := False;                                                              // SS_1006_1015_CQU_20121002
        on e:exception do begin                                                         // SS_1006_1015_CQU_20121002
        	EventLogReportEvent(elError, Format(MSG_ERROR_REGRECHAZ, [e.Message]), ''); // SS_1006_1015_CQU_20121002
            Result := False;                                                            // SS_1006_1015_CQU_20121002
    	end;                                                                            // SS_1006_1015_CQU_20121002
    end;

    spAgregarRegRechOtrasConcesio.Close;
end;


{---------------------------------------------------------------------------------
        	SePuedeFacturarEnCN

 Author			: mbecerra
 Date			: 27-Enero-2011
 Description	:
                    Retorna verdadero si el rut es facturable en CN,
                    Falso en caso contrario

----------------------------------------------------------------------------------}
function TMainForm.SePuedeFacturarEnCN;
resourcestring
    MSG_SQL_EXISTE_RUT   =  'SELECT CodigoCliente, CodigoConcesionaria ' +
                            'FROM ClientesAFacturarOtrasConcesionarias (NOLOCK) ' + 
                            'WHERE NumeroDocumento = ''%s'' ';
    //MSG_CONCESIONARIA_CN = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_COSTANERA_NORTE()';                 // SS_1147_CQU_20140408
    MSG_CONCESIONARIA_NATIVA = 'SELECT dbo.ObtenerConcesionariaNativa()';                               // SS_1147_CQU_20140408
var
    //CodigoCN : Integer;                                                                               // SS_1147_CQU_20140408
    CodigoNativa : Integer;                                                                             // SS_1147_CQU_20140408

begin
    //CodigoCN := QueryGetValueInt(DMConnections.BaseCAC, MSG_CONCESIONARIA_CN);                        // SS_1147_CQU_20140408
    CodigoNativa := QueryGetValueInt(DMConnections.BaseCAC, MSG_CONCESIONARIA_NATIVA);                  // SS_1147_CQU_20140408

    //llevar el rut a formato char(9)
    while Length(Rut) < 9 do Rut := '0' + Rut;
    
    aqryArchivo.Close;
    with aqryArchivo do begin
        SQL.Clear;
        Parameters.Clear;
        SQL.Text := Format(MSG_SQL_EXISTE_RUT, [Rut]);
        Open;
        //Result := ((not IsEmpty) and (FieldByName('CodigoConcesionaria').AsInteger = CodigoCN));      // SS_1147_CQU_20140408
        Result := ((not IsEmpty) and (FieldByName('CodigoConcesionaria').AsInteger = CodigoNativa));    // SS_1147_CQU_20140408
    end;

    aqryArchivo.Close;
end;

{---------------------------------------------------------------------------------
        	InsertarRestoDeLaLinea

 Author			: mbecerra
 Date			: 05-Agosto-2011
 Description	:
                    Realiza un UPDATE de los campos faltantes de AMB MLFF
                    para Anomal�as.

----------------------------------------------------------------------------------}
function TMainForm.InsertarRestoDeLaLinea(Linea: string; CodigoConcesionaria: string; NumCorrCO: string) : Boolean;
resourcestring
	MSG_ERROR = 'Error al Hacer el UPDATE de Validacion manual NumCorrCO = %s, Concesionaria = %s, error = %s';

    MSG_SQL	= 	'	UPDATE TransitosPendientesDelCop  SET ' +
				'		PatenteDetectada = %s, ' +              			//Campo01
				'		ConfiabilidadPatenteDetectada = %s, ' +             //Campo02
				'		PatenteOCR = %s, ' +                                //Campo03
				'		ConfiabilidadPatenteOCR = %s, ' +                   //Campo04
				'		CategoriaDetectada = %s, ' +                        //Campo05
				'		CategoriaTAG = %s, ' +                              //Campo06
				'		DiscrepanciaPatenteNoConfiable = %s, ' +            //Campo07
				'		TAGPocoProbable = %s, ' +                           //Campo08
				'		ElegidoPorAzar = %s, ' +                            //Campo09
//				'		ContextMarkOriginal = %s, ' +                       //Campo10                              //PAR00133_MBE_20110819
				'		ContractSerialNumberOriginal = %s, ' +              //Campo11
				'		Validador = %s, ' +                                 //Campo12
				'		FechaHoraValidacion = %s, ' +                       //Campo13
				'		PatenteValidada = %s, ' +                           //Campo14
				'		CategoriaValidada = %s, ' +                         //Campo15
				'		ReValidador = %s, ' +                               //Campo16
				'		FechaHoraReValidacion = %s, ' +                     //Campo17
				'		PatenteReValidada = %s, ' +                         //Campo18
				'		CategoriaReValidada = %s, ' +                       //Campo19
				'		NumberCoordinatedTAG = %s, ' +                      //Campo20
				'		BatteryInsertionDate = %s, ' +                      //Campo21
				'		OBEStatus = %s, ' +                                 //Campo22
				'		EstadoPatente = %s, ' +                             //Campo23
				'		EstadoTAG = %s, ' +                                 //Campo24
				'		DiscrepanciaTAGNoDetectado = %s, ' +                //Campo25
				'		CodigoMotivoNoFacturable = %s, ' +                  //Campo26
				'		TipoValidacion = %s,  ' +                           //Campo27
        '   Velocidad = %s,  ' +                                //Campo28                              // SS_1021_HUR_20120329
        '   NumeroCarril = %s,  ' +                             //Campo29                              // SS_1021_HUR_20120329
                '		EstadoTransitoPendiente = 0 ' +						//Volver al estado 'Por Numerar'
                '	WHERE NumCorrCO = %s  AND  CodigoConcesionaria = %s';
const
    separador = ';';

var
    //Posicion : Integer;
    Campo01, Campo02, Campo03, Campo04, Campo05,
    Campo06, Campo07, Campo08, Campo09, Campo10,
    Campo11, Campo12, Campo13, Campo14, Campo15,
    Campo16, Campo17, Campo18, Campo19, Campo20,
    Campo21, Campo22, Campo23, Campo24, Campo25,
    Campo26, Campo27, Campo28, Campo29, {Campo30,}		// SS_1021_HUR_20120329
    ConsultaSql  : string;
    FechaAux : TDateTime;
    
begin
	//leer los datos
	Campo01 := Trim(GetItem(Linea, 25, separador));
    Campo02 := Trim(GetItem(Linea, 26, separador));
    Campo03 := Trim(GetItem(Linea, 27, separador));
    Campo04 := Trim(GetItem(Linea, 28, separador));
    Campo05 := Trim(GetItem(Linea, 29, separador));
    Campo06 := Trim(GetItem(Linea, 30, separador));
    Campo07 := Trim(GetItem(Linea, 31, separador));
    Campo08 := Trim(GetItem(Linea, 32, separador));
    Campo09 := Trim(GetItem(Linea, 33, separador));
    Campo10 := Trim(GetItem(Linea, 34, separador));
    Campo11 := Trim(GetItem(Linea, 35, separador));
    Campo12 := Trim(GetItem(Linea, 36, separador));
    Campo13 := Trim(GetItem(Linea, 37, separador));
    Campo14 := Trim(GetItem(Linea, 38, separador));
    Campo15 := Trim(GetItem(Linea, 39, separador));
    Campo16 := Trim(GetItem(Linea, 40, separador));
    Campo17 := Trim(GetItem(Linea, 41, separador));
    Campo18 := Trim(GetItem(Linea, 42, separador));
    Campo19 := Trim(GetItem(Linea, 43, separador));
    Campo20 := Trim(GetItem(Linea, 44, separador));
    Campo21 := Trim(GetItem(Linea, 45, separador));
    Campo22 := Trim(GetItem(Linea, 46, separador));
    Campo23 := Trim(GetItem(Linea, 47, separador));
    Campo24 := Trim(GetItem(Linea, 48, separador));
    Campo25 := Trim(GetItem(Linea, 49, separador));
    Campo26 := Trim(GetItem(Linea, 50, separador));
    Campo27 := Trim(GetItem(Linea, 51, separador));
    Campo28 := Trim(GetItem(Linea, 52, separador));  // Velocidad     // SS_1021_HUR_20120329
    Campo29 := Trim(GetItem(Linea, 53, separador));  // NumeroCarril  // SS_1021_HUR_20120329


	//setear las lineas
    Campo01 := IIf(Campo01 = '', 'NULL', '''' + Campo01 + '''');
    Campo02 := IIf(Campo02 = '', 'NULL', Campo02);
    Campo03 := IIf(Campo03 = '', 'NULL', '''' + Campo03 + '''');
    Campo04 := IIf(Campo04 = '', 'NULL', Campo04);
    Campo05 := IIf(Campo05 = '', 'NULL', Campo05);
    Campo06 := IIf(Campo06 = '', 'NULL', Campo06);
    Campo07 := IIf(Campo07 = '', 'NULL', Campo07);
    Campo08 := IIf(Campo08 = '', 'NULL', Campo08);
    Campo09 := IIf(Campo09 = '', 'NULL', Campo09);
    Campo10 := IIf(Campo10 = '', 'NULL', Campo10);
    Campo11 := IIf(Campo11 = '', 'NULL', Campo11);
    Campo12 := IIf(Campo12 = '', 'NULL', '''' + Campo12 + '''');

    FechaAux := ConvierteAFecha(Campo13);
    Campo13	:= IIf(FechaAux <= 0, 'NULL', '''' + FormatDateTime('yyyymmdd hh:nn:ss', FechaAux) + '''' );

    Campo14 := IIf(Campo14 = '', 'NULL', '''' + Campo14 + '''');
    Campo15 := IIf(Campo15 = '', 'NULL', Campo15);
    Campo16 := IIf(Campo16 = '', 'NULL', '''' + Campo16 + '''');

    FechaAux := ConvierteAFecha(Campo17);
    Campo17	:=  IIf(FechaAux <= 0, 'NULL', '''' + FormatDateTime('yyyymmdd hh:nn:ss', FechaAux) + '''' );

    Campo18 := IIf(Campo18 = '', 'NULL', '''' + Campo18 + '''');
    Campo19 := IIf(Campo19 = '', 'NULL', Campo19);
    Campo20 := IIf(Campo20 = '', 'NULL', Campo20);

    FechaAux := ConvierteAFecha(Campo21);
    Campo21 := IIf(FechaAux <= 0, 'NULL', '''' + FormatDateTime('yyyymmdd hh:nn:ss', FechaAux) + '''' );

    Campo22 := IIf(Campo22 = '', 'NULL', Campo22);
    Campo23 := IIf(Campo23 = '', 'NULL', Campo23);
    Campo24 := IIf(Campo24 = '', 'NULL', Campo24);
    Campo25 := IIf(Campo25 = '', 'NULL', Campo25);
    Campo26 := IIf(Campo26 = '', 'NULL', Campo26);
    Campo27 := IIf(Campo27 = '', 'NULL', Campo27);
    Campo28 := IIf(Campo28 = '', 'NULL', Campo28);                    // SS_1021_HUR_20120329
    Campo29 := IIf(Campo29 = '', 'NULL', Campo29);                    // SS_1021_HUR_20120329

    try
		ConsultaSql := Format(MSG_SQL, [	Campo01, Campo02, Campo03, Campo04, Campo05,
    										Campo06, Campo07, Campo08, Campo09, //Campo10,                  //PAR00133_MBE_20110819
    										Campo11, Campo12, Campo13, Campo14, Campo15,
    										Campo16, Campo17, Campo18, Campo19, Campo20,
    										Campo21, Campo22, Campo23, Campo24, Campo25,
    										Campo26, Campo27, Campo28, Campo29, NumCorrCO, CodigoConcesionaria       // SS_1021_HUR_20120329
    									] );

    	//ejecutar el UPDATE
    	DMConnections.BaseCAC.Execute(ConsultaSql);
        Result := True;
    except on e:exception do begin
            EventLogReportEvent(elError, Format(MSG_ERROR, [NumCorrCO, CodigoConcesionaria, e.Message]), '');
            LineaError.Add(Linea + ';' + Format(MSG_ERROR, [NumCorrCO, CodigoConcesionaria, e.Message])); // SS_1015_CQU_20120510
    		Result := False;
    	end;
    end;

end;

{---------------------------------------------------------------------------------
        	ImportarLinea

 Author			: mbecerra
 Date			: 10-Agosto-2010
 Description	:
                    Importa la l�nea en la tabla TransitosPendientesDelCOP

----------------------------------------------------------------------------------}
function TMainForm.ImportarLinea;
resourcestring
    MSG_ERROR_CAMPO_NUM     = 'El campo %s no es num�rico';
    MSG_ERROR_CAMPO_STR     = 'Falta el campo %s ';
//    MSG_ERROR_CAMPO_TARIFA  = 'Tipo de Tarifa desconocida %s';
//    MSG_ERROR_CAMPO_DIA     = 'Tipo de D�a desconocido %s';
    MSG_ERROR_TIPO_TRX      = 'Tipo de transacci�n no conocida %s';
    MSG_ERROR_YA_EXISTE     = 'Ya existe el registro en la tabla %s';
    MSG_ERROR_TRX_LINT      = 'Tipo Transacci�n %s es distinta a la indicada en el archivo %s';
    MSG_ERROR_CONCESIO      = 'Las Concesionaria indicada en el nombre del archivo %s es distinta a la indicada en el registro de transacci�n %s';
    MSG_ERROR_INSERT        = 'Error al insertar la transacci�n Numero = %s, para la concesionaria %s: %s';
    MSG_ERROR_CONTEXT       = 'Para el ContractSerialNumber, falta alguno de los siguientes campos: CONTRACT, CONTEXT, PROVEEDOR';
    MSG_TABLA_TTOSPEND      = 'TransitosPendientesDelCop';
    MSG_TABLA_ESTAPEND      = 'tmpTransaccionesEstacionamientos';
    MSG_ERROR_FECHA         = 'La Fecha / Hora de salida no puede ser menor que la Fecha / Hora de entrada';                // SS_1006_CQU_20120807
    MSG_ERROR_FECHA_NULA    = 'La Fecha / Hora de salida o entrada, no contiene el formato v�lido de fecha el cual es AAAAMMDDHHNNSS';   // SS_1006_CQU_20120807
    MSG_ERROR_FECHA_INI     = 'La Fecha / Hora de entrada no puede ser mayor que la Fecha / Hora actual';       // SS_1006_1015_CQU_20120926
    MSG_ERROR_CONTEXTMARKS  = 'No existe ContextMarks';                                                         // SS_1006_1015_CQU_20120926
    MSG_ERROR_TRANPROCESADA = 'Esta transacci�n fue procesada con anterioridad';                                // SS_1006_1015_CQU_20120926
    MSG_ERROR_INOUT_NOEXISTE= 'El %s indicado no existe';                                                       // SS_1006_1015_CQU_20121002

    CAMPO_IDXTRX    = 'Indentificador Transacci�n';
    CAMPO_CODCON    = 'Codigo Concesionaria';
    CAMPO_NUMTRX    = 'Numero Transacci�n';
    CAMPO_CSN       = 'Contract Serial Number';
    CAMPO_CTM       = 'ContextMarks';
    CAMPO_RUT       = 'Rut';
    CAMPO_FECHAI    = 'FechaHora Entrada';
    CAMPO_FECHAF    = 'FechaHora Salida';
    CAMPO_PUNTOE    = 'Punto de Entrada';
    CAMPO_PUNTOS    = 'Punto de Salida';
    CAMPO_IMPORTE   = 'Importe';
    CAMPO_PATENTE   = 'Patente';
    CAMPO_CATEGOR   = 'Categor�a';
    CAMPO_PATDETE   = 'PatenteDetectada';
    CAMPO_CATEDET   = 'Categor�a Detectada';
    CAMPO_NOCT      = 'NumberCoordinatedTag';
    CAMPO_EQUIPMEN  = 'Equipment Status';
    CAMPO_BATERY    = 'Batery Insertion Date';
    CAMPO_TRXRES    = 'Transaction result';
    CAMPO_OBE       = 'OBE Status';
    CAMPO_ACC       = 'Registration Accesibility';
    CAMPO_AUDIT     = 'AuditFileLineID';
    CAMPO_TARIFA    = 'Tipo Tarifa';
    CAMPO_DIA       = 'Tipo de D�a';
    CAMPO_CONTEXT   = 'Context';            // SS_1006_1015_CQU_20121002
    CAMPO_CONTRACT  = 'Contract';           // SS_1006_1015_CQU_20121002
    CAMPO_ISSUER    = 'IssuerIdentifier';   // SS_1006_1015_CQU_20121002

const
    separador = ';';


var
    //datos de la l�nea:
    LinIdxTransaccion, LinCodigoConcesionaria,
    LinNumTransaccion, LinEtiqueta,
    LinContractSerialNumber,
    //LinContextMark,
    LinContext, LinContract, LinIssuerIdentifier,
    LinSentido, LinTipoDia, LinTipoTarifa,
    LinConvenio, LinRut,
    LinFechaHoraIni, LinFechaHoraSal,
    LinPuntoEntrada, LinPuntoSalida,
    LinImporte, LinPatente, LinCategoria,
    //LinPatenteDetectada, LinCategoriaDetectada, LinNumberCoordinatedTag,
    LinEquipmentStatus,
    //LinBatery,
    LinTransactionResult,
    //LinOBEStatus,
    LinRegAccesibility,
    LinAuditFileLineID: string;             // SS_1021_HUR_20120307  // SS_1021_HUR_20120329
    //LinNumeroCarril,                      // SS_1021_HUR_20120307  // SS_1021_HUR_20120329
    //LinVelocidad : string;                // SS_1021_HUR_20120307  // SS_1021_HUR_20120329

    CodigoRetorno : integer;
    FechaActual : TDateTime;                // SS_1006_1015_CQU_20120926
begin

    Result := False;

    if Trim(Linea) = '' then begin         //SS_1006_1015_MBE_20120921
        Result := True;                    //SS_1006_1015_MBE_20120921
        Exit;                              //SS_1006_1015_MBE_20120921
    end;                                   //SS_1006_1015_MBE_20120921

    FechaActual		:= StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()')); // SS_1006_1015_CQU_20120926
    //parsear la l�nea:
    LinIdxTransaccion       := Trim(GetItem(Linea, 1, separador));
    LinCodigoConcesionaria  := Trim(GetItem(Linea, 2, separador));
    LinNumTransaccion       := Trim(GetItem(Linea, 3, separador));
    LinEtiqueta             := Trim(GetItem(Linea, 4, separador));
    LinContractSerialNumber := Trim(GetItem(Linea, 5, separador));
    LinContext              := Trim(GetItem(Linea, 6, separador));
    LinContract             := Trim(GetItem(Linea, 7, separador));
    LinIssuerIdentifier     := Trim(GetItem(Linea, 8, separador));
    LinPatente              := Trim(GetItem(Linea, 9, separador));
    LinSentido              := Trim(GetItem(Linea, 10, separador));
    LinTipoDia              := Trim(GetItem(Linea, 11, separador));
    LinTipoTarifa           := Trim(GetItem(Linea, 12, separador));
    LinConvenio             := Trim(GetItem(Linea, 13, separador));
    LinRut                  := Trim(GetItem(Linea, 14, separador));
    LinFechaHoraIni         := Trim(GetItem(Linea, 15, separador));
    LinFechaHoraSal         := Trim(GetItem(Linea, 16, separador));
    LinPuntoEntrada         := Trim(GetItem(Linea, 17, separador));
    LinPuntoSalida          := Trim(GetItem(Linea, 18, separador));
    LinImporte              := Trim(GetItem(Linea, 19, separador));
    LinCategoria            := Trim(GetItem(Linea, 20, separador));
    LinEquipmentStatus      := Trim(GetItem(Linea, 21, separador));
    LinTransactionResult    := Trim(GetItem(Linea, 22, separador));
    LinRegAccesibility      := Trim(GetItem(Linea, 23, separador));
    LinAuditFileLineID      := Trim(GetItem(Linea, 24, separador));
    // LinVelocidad            := Trim(GetItem(Linea, 55, separador));   // SS_1021_HUR_20120307 // SS_1021_HUR_20120329
    // LinNumeroCarril         := Trim(GetItem(Linea, 56, separador));   // SS_1021_HUR_20120307 // SS_1021_HUR_20120329

    //en los siguientes campos, si vienen vacios, poner un cero (0)
    if LinEquipmentStatus = '' then LinEquipmentStatus := '0';
    if LinTransactionResult = '' then LinTransactionResult := '0';
    if LinRegAccesibility = '' then LinRegAccesibility := '0';

    //convertir la concesionaria a N�mero, para apoyar las b�squedas
    //if EsNumero(LinCodigoConcesionaria) then LinCodigoConcesionaria := IntToStr(StrToInt(LinCodigoConcesionaria));    // SS_1006_1015_CQU_20121002

    //validar que:
    // no falte ning�n campo obligatorio
    // sean num�ricos los campos que deben serlo
    //
    MensajeError := '';
    //if (LinIdxTransaccion = TIPO_ESTA)                                                                                                                  // SS_1006_1015_CQU_20120926    // SS_1006_1015_CQU_20121002
    //    and (ValidarEstacionamiento(LinCodigoConcesionaria, LinNumTransaccion)) then MensajeError := MSG_ERROR_TRANPROCESADA                            // SS_1006_1015_CQU_20120926    // SS_1006_1015_CQU_20121002
    if  (LinIdxTransaccion <> TIPO_MLFF) and                                                                                                              // SS_1006_1015_CQU_20120926    // SS_1006_1015_CQU_20121002
    //else if  (LinIdxTransaccion <> TIPO_MLFF) and                                                                                                                                       // SS_1006_1015_CQU_20121002
        (LinIdxTransaccion <> TIPO_PEAJ) and
        (LinIdxTransaccion <> TIPO_ESTA)                                                then MensajeError := Format(MSG_ERROR_TIPO_TRX, [LinIdxTransaccion])
    else if LinIdxTransaccion <> FNomArchTipoTrx                                        then MensajeError := Format(MSG_ERROR_TRX_LINT, [LinIdxTransaccion, FNomArchTipoTrx])
    else if not EsNumero(LinIdxTransaccion)                                             then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IDXTRX])
    else if not EsNumero(LinCodigoConcesionaria)                                        then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CODCON])
    else if EsNumero(LinCodigoConcesionaria)                                                                                                                // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinCodigoConcesionaria, False)                           then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CODCON])    // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinNumTransaccion)                                             then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_NUMTRX])
    else if EsNumero(LinNumTransaccion)                                                                                                                     // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinNumTransaccion, True)                                 then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_NUMTRX])    // SS_1006_1015_CQU_20121002
    else if (LinIdxTransaccion = TIPO_ESTA)                                                                                                                 // SS_1006_1015_CQU_20121002
        and (ValidarEstacionamiento(LinCodigoConcesionaria, LinNumTransaccion))         then MensajeError := MSG_ERROR_TRANPROCESADA                        // SS_1006_1015_CQU_20121002
    else if (LinIdxTransaccion = TIPO_ESTA) and not EsNumero(LinContractSerialNumber)   then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CSN])
    else if ((LinIdxTransaccion = TIPO_ESTA) and EsNumero(LinContractSerialNumber))                                                                         // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinContractSerialNumber, False)                          then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CSN])       // SS_1006_1015_CQU_20121002
    //else if (LinContextMark <> '') and (not EsNumero(LinContextMark))                   then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_CTM])
    else if EsNumero(LinContractSerialNumber) and (
                                                    (LinContext = '') or
                                                    (LinContract = '') or
                                                    (LinIssuerIdentifier = '') )               then MensajeError := MSG_ERROR_CONTEXT
    else if (   (LinIdxTransaccion = TIPO_MLFF) or
                (LinIdxTransaccion = TIPO_PEAJ) ) and
                (LinTipoTarifa = '')                                                    then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_TARIFA])
    else if (   (LinIdxTransaccion = TIPO_MLFF) or
                (LinIdxTransaccion = TIPO_PEAJ) ) and
                (LinTipoDia = '')                                                       then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_DIA])
    else if (LinIdxTransaccion = TIPO_MLFF) and
            (FListaConcesionariasLasFacturaCN.IndexOf(LinCodigoConcesionaria) < 0) and
            (LinRut = '')                                                               then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_RUT])
    else if not EsNumero(LinFechaHoraIni)                                               then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECHAI])
    else if (LinIdxTransaccion = TIPO_ESTA) and (not EsNumero(LinFechaHoraSal))         then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECHAF])
    else if Trim(LinPuntoEntrada) = ''                                                  then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_PUNTOE])
    else if (LinIdxTransaccion = TIPO_ESTA) and (Trim(LinPuntoSalida) = '')             then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_PUNTOS])
    else if not EsNumero(LinImporte)                                                    then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPORTE])
    else if EsNumero(LinImporte) and not StringEsEntero(LinImporte, False)              then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPORTE])   // SS_1006_1015_CQU_20121002
    //11-01-2011 Patente no es obligatoria
    //else if Trim(LinPatente) = ''                                                       then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_PATENTE])
    else if (LinIdxTransaccion = TIPO_MLFF) and
			(FListaConcesionariasLasFacturaCN.IndexOf(LinCodigoConcesionaria) < 0) and
    		(not EsNumero(LinCategoria))            then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CATEGOR])
    //11-01-2011 Patente no es obligatoria
    //else if (LinIdxTransaccion = TIPO_MLFF) and (Trim(LinPatenteDetectada) = '')        then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_PATDETE])
    //else if (LinIdxTransaccion = TIPO_MLFF) and (not EsNumero(LinCategoriaDetectada))   then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CATEDET])
    //else if (LinIdxTransaccion = TIPO_MLFF) and (not EsNumero(LinNumberCoordinatedTag)) then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_NOCT])
    else if (LinIdxTransaccion = TIPO_MLFF) and (not EsNumero(LinEquipmentStatus))      then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_EQUIPMEN])
    else if ((LinIdxTransaccion = TIPO_MLFF) and EsNumero(LinEquipmentStatus))                                                                              // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinEquipmentStatus, False)                               then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_EQUIPMEN])  // SS_1006_1015_CQU_20121002
    //else if (LinIdxTransaccion = TIPO_MLFF) and (not EsNumero(LinBatery))               then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_BATERY])
    else if (LinIdxTransaccion = TIPO_MLFF) and (not EsNumero(LinTransactionResult))    then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_TRXRES])
    else if ((LinIdxTransaccion = TIPO_MLFF) and EsNumero(LinTransactionResult))                                                                            // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinTransactionResult, False)                             then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_TRXRES])    // SS_1006_1015_CQU_20121002
    //else if (LinIdxTransaccion = TIPO_MLFF) and (not EsNumero(LinOBEStatus))            then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_OBE])
    else if (LinIdxTransaccion = TIPO_MLFF) and (not EsNumero(LinRegAccesibility))      then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_ACC])
    else if ((LinIdxTransaccion = TIPO_MLFF) and EsNumero(LinRegAccesibility))                                                                              // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinRegAccesibility, False)                               then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_ACC])       // SS_1006_1015_CQU_20121002
    else if (LinIdxTransaccion = TIPO_MLFF) and (not EsNumero(LinAuditFileLineID))      then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_AUDIT])
    else if ((LinIdxTransaccion = TIPO_MLFF) and EsNumero(LinAuditFileLineID))      
        and not StringEsEntero(LinAuditFileLineID, True)                                then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_AUDIT])    
    else if CodigoConcesionaria <> StrToInt(LinCodigoConcesionaria)                     then MensajeError := Format(MSG_ERROR_CONCESIO, [IntToStr(CodigoConcesionaria), LinCodigoConcesionaria])// ;   // SS_1006_CQU_20120807
    else if (LinIdxTransaccion = TIPO_ESTA)                                                                                                                                                             // SS_1006_1015_CQU_20120926
            and (ConvierteAFecha(LinFechaHoraIni) > FechaActual)                                    then MensajeError := MSG_ERROR_FECHA_INI                                                            // SS_1006_1015_CQU_20120926
    else if  (LinIdxTransaccion = TIPO_ESTA)                                                                                                                                                            // SS_1006_CQU_20120807
            and ((ConvierteAFecha(LinFechaHoraIni) = 0) or (ConvierteAFecha(LinFechaHoraSal) = 0))  then MensajeError := MSG_ERROR_FECHA_NULA                                                           // SS_1006_CQU_20120807
    else if (LinIdxTransaccion = TIPO_ESTA)                                                                                                                                                             // SS_1006_CQU_20120807
            and (ConvierteAFecha(LinFechaHoraIni) >= ConvierteAFecha(LinFechaHoraSal))              then MensajeError := MSG_ERROR_FECHA                                                                // SS_1006_CQU_20120807
    else if EsNumero(LinIssuerIdentifier)                                                                                                                  // SS_1006_1015_CQU_20121002
            and not StringEsEntero(LinIssuerIdentifier, False)                          then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_ISSUER])   // SS_1006_1015_CQU_20121002
    else if EsNumero(LinContext)                                                                                                                           // SS_1006_1015_CQU_20121002
            and not StringEsEntero(LinContext, False)                                   then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONTEXT])  // SS_1006_1015_CQU_20121002
    else if EsNumero(LinContract)                                                                                                                          // SS_1006_1015_CQU_20121002
            and not StringEsEntero(LinContract, False)                                  then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONTRACT]) // SS_1006_1015_CQU_20121002
    else if (LinIdxTransaccion = TIPO_ESTA)                                                                                                                                                             // SS_1006_1015_CQU_20120926
            and not ValidarContexMark(LinIssuerIdentifier, LinContext, LinContract)           then MensajeError := MSG_ERROR_CONTEXTMARKS                                                               // SS_1006_1015_CQU_20120926
    else if (LinIdxTransaccion = TIPO_ESTA)                                                                                                                                                             // SS_1006_1015_CQU_20121002
            and not ValidarCodigoExternoEstacionamiento('E', LinCodigoConcesionaria, LinPuntoEntrada)   then MensajeError := Format(MSG_ERROR_INOUT_NOEXISTE, [CAMPO_PUNTOE])                           // SS_1006_1015_CQU_20121002
    else if (LinIdxTransaccion = TIPO_ESTA)                                                                                                                                                             // SS_1006_1015_CQU_20121002
            and not ValidarCodigoExternoEstacionamiento('S', LinCodigoConcesionaria, LinPuntoSalida)    then MensajeError := Format(MSG_ERROR_INOUT_NOEXISTE, [CAMPO_PUNTOS]);                          // SS_1006_1015_CQU_20121002


    // si hay error de parseo, entonces registrarlo
    // y retornar verdadero para que contin�e con la siguiente l�nea
    if MensajeError <> '' then begin
        Result := RegistrarRegistroRechazado(NombreArchivo, MensajeError, LinNumTransaccion, NumLinea);
        //LineaError.Add(Linea + ';' + MensajeError); // SS_1015_CQU_20120510   // SS_1015_CQU_20120530
        LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + MensajeError);  // SS_1015_CQU_20120530
        Exit;
    end;

    // si es MLFF validar que pueda ser facturado en CN
    if  (LinIdxTransaccion = TIPO_MLFF) and                                                             //es MLFF
        (FListaConcesionariasLasFacturaCN.IndexOf(LinCodigoConcesionaria) < 0) and                      //no la factura 100% CN
        (not FConcesionariaFacturaEnCN) and (not SePuedeFacturarEnCN(LinRut)) then                      //Rut debe estar en la tabla
    begin
        //Result := RegistrarTransitoRechazado(LinRut, NombreArchivo, LinNumTransaccion, LinCodigoConcesionaria, StrToInt(LinImporte), NumLinea,ConvierteAFecha(LinFechaHoraIni));  //SS_1015_CQU_20120413
        Result := RegistrarTransitoRechazado(LinRut, NombreArchivo, LinNumTransaccion, LinCodigoConcesionaria, StrToInt(LinImporte), NumLinea,ConvierteAFecha(LinFechaHoraIni), LinContractSerialNumber, LinContext, LinContract, LinIssuerIdentifier );    //SS_1015_CQU_20120413
        //LineaError.Add(Linea + ';' + MensajeError); // SS_1015_CQU_20120510  // SS_1015_CQU_20120530
        LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + MensajeError); // SS_1015_CQU_20120530
        Exit;
    end;

    //la l�nea est� OK y puede ser facturada en CN
    //Transacci�n de Peaje
    if (LinIdxTransaccion = TIPO_MLFF) or (LinIdxTransaccion = TIPO_PEAJ) then begin
        with spAgregarTransitoOtraConcesionaria do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConcesionaria').Value        := LinCodigoConcesionaria;
            Parameters.ParamByName('@NumCorrCO').Value                  := LinNumTransaccion;
            Parameters.ParamByName('@NumeroPuntoCobroExterno').Value    := LinPuntoEntrada;
            Parameters.ParamByName('@Etiqueta').Value                   := IIf(LinEtiqueta <> '', LinEtiqueta, NULL);
            Parameters.ParamByName('@ContractSerialNumber').Value       := IIf(LinContractSerialNumber <> '', LinContractSerialNumber, NULL);
            //Parameters.ParamByName('@ContextMark').Value                := LinContextMark;
            Parameters.ParamByName('@Context').Value                    := IIf(LinContext <> '', LinContext, NULL);
            Parameters.ParamByName('@Contract').Value                   := IIf(LinContract <> '', LinContract, NULL);
            Parameters.ParamByName('@IssuerIdentifier').Value           := IIf(LinIssuerIdentifier <> '', LinIssuerIdentifier, NULL);
            Parameters.ParamByName('@FechaHoraTransito').Value          := ConvierteAFecha(LinFechaHoraIni);
            Parameters.ParamByName('@Importe').Value                    := LinImporte;
            Parameters.ParamByName('@Patente').Value                    := IIf(LinPatente <> '', LinPatente, NULL);
            Parameters.ParamByName('@Sentido').Value                    := IIf(LinSentido <> '', LinSentido, NULL);
            Parameters.ParamByName('@TipoDia').Value                    := IIf(LinTipoDia <> '', LinTipoDia, NULL);
            Parameters.ParamByName('@TipoTarifa').Value                 := IIf(LinTipoTarifa <> '' , LinTipoTarifa, NULL);
            Parameters.ParamByName('@Convenio').Value                   := IIf(LinConvenio <> '', LinConvenio, NULL);
            Parameters.ParamByName('@NumeroDocumento').Value            := IIf(LinRut <> '', LinRut, NULL);
            Parameters.ParamByName('@Categoria').Value                  := IIf(LinCategoria<>'',LinCategoria, NULL);
            Parameters.ParamByName('@EquipmentStatus').Value            := IIf(LinEquipmentStatus <> '', LinEquipmentStatus, NULL);
            Parameters.ParamByName('@TransactionResult').Value          := IIf(LinTransactionResult <> '', LinTransactionResult, NULL);
            Parameters.ParamByName('@RegistrationAccesibility').Value   := IIf(LinRegAccesibility <> '', LinRegAccesibility, NULL);
            Parameters.ParamByName('@AuditFileLineID').Value            := IIf(LinAuditFileLineID <> '', LinAuditFileLineID, 0);
            //Parameters.ParamByName('@NumeroCarril').Value               := IIf(LinNumeroCarril <> '', LinNumeroCarril, NULL);                  // SS_1021_HUR_20120307  // SS_1021_HUR_20120329
            //Parameters.ParamByName('@Velocidad').Value                  := IIf(LinVelocidad <> '', LinVelocidad, NULL);                        // SS_1021_HUR_20120307  // SS_1021_HUR_20120329

            try
                ExecProc;
                CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                case CodigoRetorno of
                    0 :
                        begin
                            if (LinCodigoConcesionaria = '5') or (LinCodigoConcesionaria = '3') then begin                                    // SS_1021_HUR_20120329
                            	Result := InsertarRestoDeLaLinea(Linea, LinCodigoConcesionaria, LinNumTransaccion);
                            end
                            else Result := True;

                            if FNumCorrCOMin = 0 then FNumCorrCOMin := StrToInt64(LinNumTransaccion);
                            if FNumCorrCoMax = 0 then FNumCorrCoMax := StrToInt64(LinNumTransaccion);
                            if StrToInt64(LinNumTransaccion) > FNumCorrCoMax then FNumCorrCoMax := StrToInt64(LinNumTransaccion);
                            if StrToInt64(LinNumTransaccion) < FNumCorrCoMin then FNumCorrCoMin := StrToInt64(LinNumTransaccion);
                        end;

                   -1 :
                        begin
                            Result := True;
                            with spAgregarTTORechOtraConcesio do begin
                                Parameters.Refresh;
                                Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
                                Parameters.ParamByName('@NumCorrCO').Value              := LinNumTransaccion;
                                Parameters.ParamByName('@CodigoConcesionaria').Value    := LinCodigoConcesionaria;
                                Parameters.ParamByName('@Importe').Value                := LinImporte;
                                Parameters.ParamByName('@FechaHoraTransito').Value      := ConvierteAFecha(LinFechaHoraIni);
                                Parameters.ParamByName('@Motivo').Value                 := Format(MSG_ERROR_YA_EXISTE, [MSG_TABLA_TTOSPEND]);
                                Parameters.ParamByName('@NumeroLinea').Value			:= NumLinea;
                                Parameters.ParamByName('@ContractSerialNumber').Value   := IIf(LinContractSerialNumber <> '', LinContractSerialNumber, NULL); // SS_1015_CQU_20120413
                                Parameters.ParamByName('@Context').Value                := IIf(LinContext <> '', LinContext, NULL);                           // SS_1015_CQU_20120413
                                Parameters.ParamByName('@Contract').Value               := IIf(LinContract <> '', LinContract, NULL);                         // SS_1015_CQU_20120413
                                Parameters.ParamByName('@IssuerIdentifier').Value       := IIf(LinIssuerIdentifier <> '', LinIssuerIdentifier, NULL);         // SS_1015_CQU_20120413
                                ExecProc;
                                LineaError.Add(Linea + ';' + Format(MSG_ERROR_YA_EXISTE, [MSG_TABLA_TTOSPEND])); // SS_1015_CQU_20120510    // SS_1015_CQU_20120530
                                LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + Format(MSG_ERROR_YA_EXISTE, [MSG_TABLA_TTOSPEND])); // SS_1015_CQU_20120530
                            end;
                        end;

                   -2 :
                        begin
                            EventLogReportEvent(elError, Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, ' ']), '');
                            //LineaError.Add(Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, ' '])); // SS_1015_CQU_20120510     // SS_1015_CQU_20120530
                            LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, ' ']));    // SS_1015_CQU_20120530
                        end;
                end;

                except on e:exception do begin
                    EventLogReportEvent(elError, Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message]), '');
                    //LineaError.Add(Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message])); // SS_1015_CQU_20120510   // SS_1015_CQU_20120530
                    LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message]));  // SS_1015_CQU_20120530
                end;
            end;
        end;   {with}

    end
    else begin         //Tipo de transacci�n es Estacionamiento
        with spAgregarTransaccionEstacionamiento do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
            Parameters.ParamByName('@NumTransaccionExterno').Value  := LinNumTransaccion;
            Parameters.ParamByName('@ContractSerialNumber').Value   := LinContractSerialNumber;
            Parameters.ParamByName('@Contract').Value               := LinContract;
            Parameters.ParamByName('@Context').Value                := LinContext;
            Parameters.ParamByName('@IssuerIdentifier').Value       := LinIssuerIdentifier;
            //Parameters.ParamByName('@Patente').Value                := LinPatente;                            // SS_1006_1015_20120913_CQU
            Parameters.ParamByName('@Patente').Value                := IIf(LinPatente <> '', LinPatente, NULL); // SS_1006_1015_20120913_CQU
            Parameters.ParamByName('@FechaHoraEntrada').Value       := ConvierteAFecha(LinFechaHoraIni);
            Parameters.ParamByName('@FechaHoraSalida').Value        := ConvierteAFecha(LinFechaHoraSal);
            Parameters.ParamByName('@CodigoExternoEntrada').Value   := LinPuntoEntrada;
            Parameters.ParamByName('@CodigoExternoSalida').Value    := LinPuntoSalida;
            Parameters.ParamByName('@Importe').Value                := LinImporte;
            Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;                           //SS_1006_NDR_20130228
            try
                ExecProc;
                CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                case CodigoRetorno of
                    0 :
                        begin
                            Result := True;
                            if FNumCorrCOMin = 0 then FNumCorrCOMin := StrToInt64(LinNumTransaccion);
                            if FNumCorrCoMax = 0 then FNumCorrCoMax := StrToInt64(LinNumTransaccion);
                            if StrToInt64(LinNumTransaccion) > FNumCorrCoMax then FNumCorrCoMax := StrToInt64(LinNumTransaccion);
                            if StrToInt64(LinNumTransaccion) < FNumCorrCoMin then FNumCorrCoMin := StrToInt64(LinNumTransaccion);
                        end;

                   -1 :
                        begin
                            Result := True;
                            with spAgregarESTARechOtraConcesio do begin
                                Parameters.Refresh;
                                Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
                                //Parameters.ParamByName('@NumCorrEstacionamiento').Value := LinNumTransaccion;     // SS_1006_1015_CQU_20120926
                                Parameters.ParamByName('@IdentificadorTransaccion').Value := LinNumTransaccion;     // SS_1006_1015_CQU_20120926
                                Parameters.ParamByName('@CodigoConcesionaria').Value    := LinCodigoConcesionaria;
                                Parameters.ParamByName('@CodigoExternoEntrada').Value   := LinPuntoEntrada;
                                Parameters.ParamByName('@CodigoExternoSalida').Value    := LinPuntoSalida;
                                Parameters.ParamByName('@FechaHoraEntrada').Value       := ConvierteAFecha(LinFechaHoraIni);
                                Parameters.ParamByName('@FechaHoraSalida').Value        := ConvierteAFecha(LinFechaHoraSal);
                                Parameters.ParamByName('@Importe').Value                := LinImporte;
                                Parameters.ParamByName('@Motivo').Value                 := Format(MSG_ERROR_YA_EXISTE, [MSG_TABLA_ESTAPEND]);
                                Parameters.ParamByName('@ContractSerialNumber').Value   := LinContractSerialNumber; // SS_1015_CQU_20120413
                                Parameters.ParamByName('@Contract').Value               := LinContract;             // SS_1015_CQU_20120413
                                Parameters.ParamByName('@Context').Value                := LinContext;              // SS_1015_CQU_20120413
                                Parameters.ParamByName('@IssuerIdentifier').Value       := LinIssuerIdentifier;     // SS_1015_CQU_20120413
                                Parameters.ParamByName('@NumeroLinea').Value            := NumLinea;                // SS_1015_CQU_20120525

                                ExecProc;
                                //LineaError.Add(Linea + ';' + Format(MSG_ERROR_YA_EXISTE, [MSG_TABLA_ESTAPEND])); // SS_1015_CQU_20120510  // SS_1015_CQU_20120530
                                LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + Format(MSG_ERROR_YA_EXISTE, [MSG_TABLA_ESTAPEND])); // SS_1015_CQU_20120530
                            end;
                        end;

                   -2 :
                        begin
                            EventLogReportEvent(elError, Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, ' ']), '');
                            //LineaError.Add(Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, ' '])); // SS_1015_CQU_20120510     // SS_1015_CQU_20120530
                            LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, ' ']));    // SS_1015_CQU_20120530
                        end;

                   //BEGIN  : SS_1006_NDR_20121227-----------------------------------------------------------------------------------------------------------
                   -3 :
                        begin
                            Result := True;
                            with spAgregarESTARechOtraConcesio do begin
                                Parameters.Refresh;
                                Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
                                Parameters.ParamByName('@IdentificadorTransaccion').Value := LinNumTransaccion;
                                Parameters.ParamByName('@CodigoConcesionaria').Value    := LinCodigoConcesionaria;
                                Parameters.ParamByName('@CodigoExternoEntrada').Value   := LinPuntoEntrada;
                                Parameters.ParamByName('@CodigoExternoSalida').Value    := LinPuntoSalida;
                                Parameters.ParamByName('@FechaHoraEntrada').Value       := ConvierteAFecha(LinFechaHoraIni);
                                Parameters.ParamByName('@FechaHoraSalida').Value        := ConvierteAFecha(LinFechaHoraSal);
                                Parameters.ParamByName('@Importe').Value                := LinImporte;
                                Parameters.ParamByName('@Motivo').Value                 := 'El TAG no est� en Lista de Acceso'; // y no registra deuda pendiente';
                                Parameters.ParamByName('@ContractSerialNumber').Value   := LinContractSerialNumber;
                                Parameters.ParamByName('@Contract').Value               := LinContract;
                                Parameters.ParamByName('@Context').Value                := LinContext;
                                Parameters.ParamByName('@IssuerIdentifier').Value       := LinIssuerIdentifier;
                                Parameters.ParamByName('@NumeroLinea').Value            := NumLinea;
                                ExecProc;
                                LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + 'El TAG no est� en Lista de Acceso'); // y no registra deuda pendiente');
                            end;
                        end;
                   //END  : SS_1006_NDR_20121227-----------------------------------------------------------------------------------------------------------

                   //BEGIN  : SS_1006_NDR_20130118-----------------------------------------------------------------------------------------------------------
                   -4 :
                        begin
                            Result := True;
                            with spAgregarESTARechOtraConcesio do begin
                                Parameters.Refresh;
                                Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
                                Parameters.ParamByName('@IdentificadorTransaccion').Value := LinNumTransaccion;
                                Parameters.ParamByName('@CodigoConcesionaria').Value    := LinCodigoConcesionaria;
                                Parameters.ParamByName('@CodigoExternoEntrada').Value   := LinPuntoEntrada;
                                Parameters.ParamByName('@CodigoExternoSalida').Value    := LinPuntoSalida;
                                Parameters.ParamByName('@FechaHoraEntrada').Value       := ConvierteAFecha(LinFechaHoraIni);
                                Parameters.ParamByName('@FechaHoraSalida').Value        := ConvierteAFecha(LinFechaHoraSal);
                                Parameters.ParamByName('@Importe').Value                := LinImporte;
                                Parameters.ParamByName('@Motivo').Value                 := MSG_ERROR_TRANPROCESADA;
                                Parameters.ParamByName('@ContractSerialNumber').Value   := LinContractSerialNumber;
                                Parameters.ParamByName('@Contract').Value               := LinContract;
                                Parameters.ParamByName('@Context').Value                := LinContext;
                                Parameters.ParamByName('@IssuerIdentifier').Value       := LinIssuerIdentifier;
                                Parameters.ParamByName('@NumeroLinea').Value            := NumLinea;
                                ExecProc;
                                LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + MSG_ERROR_TRANPROCESADA);
                            end;
                        end;
                   //END  : SS_1006_NDR_20130118 -----------------------------------------------------------------------------------------------------------
                end;

                except on e:exception do begin
                    EventLogReportEvent(elError, Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message]), '');
                    //LineaError.Add(Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message])); // SS_1015_CQU_20120510   // SS_1015_CQU_20120530
                    LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message]));  // SS_1015_CQU_20120530
                end;
            end;
        end; {with}
    end;

end;

{---------------------------------------------------------------------------------
        	ImportarLineaMovCta

 Author			: mbecerra
 Date			: 06-Febrero-2012
 Description	:
                    Importa la l�nea en la tabla MovimientosCuentasOtrasConcesionarias

                    SS_1015_MBE_20120206

----------------------------------------------------------------------------------}
function TMainForm.ImportarLineaMovCta;
resourcestring
    MSG_ERROR_CAMPO_NUM     = 'El campo %s no es num�rico';
    MSG_ERROR_CAMPO_STR     = 'Falta el campo %s ';
    MSG_ERROR_YA_EXISTE     = 'Ya existe el registro en la tabla %s';
    MSG_ERROR_CONCESIO      = 'Las Concesionaria indicada en el nombre del archivo %s es distinta a la indicada en el registro de transacci�n %s';
    MSG_ERROR_CONTEXT       = 'Para el ContractSerialNumber, falta alguno de los siguientes campos: CONTRACT, CONTEXT, PROVEEDOR';
    MSG_ERROR_NEGATIVO      = 'El campo [Indicador Importe Negativo o Positivo] no es 0 ni 1';
    MSG_ERROR_INSERT        = 'Error al insertar la transacci�n Numero = %s, para la concesionaria %s: %s';

    CAMPO_CODCON    = 'Codigo Concesionaria';
    CAMPO_NUMTRX    = 'Numero Transacci�n';
    CAMPO_CSN       = 'Contract Serial Number';
    CAMPO_CONTEXT   = 'Context';
    CAMPO_CONTRACT  = 'Contract';
    CAMPO_ISSUER    = 'IssuerIdentifier';
    CAMPO_FECHA     = 'FechaHora MovCta';
    CAMPO_IMPORTE   = 'Importe';
    CAMPO_CONCEPTO  = 'C�digo Concepto';
    CAMPO_CONVENIO  = 'C�digo Convenio externo';                        //SS_1015_MBE_20120622
    CAMPO_IDESTA    = 'Numero Estacionamiento Asociado';                //SS_1006_CQU_20130328

const
    separador = ';';


var
    //datos de la l�nea:
    LinCodigoConcesionaria,
    LinNumeroConvenio,                                                  //SS_1015_MBE_20120622
    LinNumTransaccion, LinContractSerialNumber,
    LinContext, LinContract, LinIssuerIdentifier,
    LinFechaHora, LinConcepto, LinEsNegativo,
    LinImporte,
    LinNumTranEsta  : string;                      //SS_1006_CQU_20130130

    CodigoRetorno : integer;
begin

    Result := False;

    if Trim(Linea) = '' then begin                 //SS_1006_1015_MBE_20120921
        Result := True;                            //SS_1006_1015_MBE_20120921
        Exit;                                      //SS_1006_1015_MBE_20120921
    end;                                           //SS_1006_1015_MBE_20120921

    //parsear la l�nea:
    LinNumTransaccion       := Trim(GetItem(Linea, 1, separador));
    LinCodigoConcesionaria  := Trim(GetItem(Linea, 2, separador));
    LinContractSerialNumber := Trim(GetItem(Linea, 3, separador));
    LinContext              := Trim(GetItem(Linea, 4, separador));
    LinContract             := Trim(GetItem(Linea, 5, separador));
    LinIssuerIdentifier     := Trim(GetItem(Linea, 6, separador));
    LinFechaHora            := Trim(GetItem(Linea, 7, separador));
    LinImporte              := Trim(GetItem(Linea, 8, separador));
    LinConcepto             := Trim(GetItem(Linea, 9, separador));
    LinEsNegativo           := Trim(GetItem(Linea, 10, separador));
    LinNumeroConvenio       := Trim(GetItem(Linea, 11, separador));     //SS_1015_MBE_20120622
    LinNumTranEsta          := Trim(GetItem(Linea, 12, separador));     //SS_1006_CQU_20130130
    //convertir la concesionaria a N�mero, para apoyar las b�squedas
    //if EsNumero(LinCodigoConcesionaria) then LinCodigoConcesionaria := IntToStr(StrToInt(LinCodigoConcesionaria));                                        // SS_1006_1015_CQU_20121002

    //validar que:
    // no falte ning�n campo obligatorio
    // sean num�ricos los campos que deben serlo
    //
    MensajeError := '';
    if not EsNumero(LinCodigoConcesionaria)                                             then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CODCON])
    else if EsNumero(LinCodigoConcesionaria)                                                                                                                // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinCodigoConcesionaria, False)                           then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CODCON])    // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinNumTransaccion)                                             then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_NUMTRX])
    else if EsNumero(LinNumTransaccion) and not StringEsEntero(LinNumTransaccion, True) then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_NUMTRX])    // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinContractSerialNumber)                                       then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CSN])
    else if EsNumero(LinContractSerialNumber)                                                                                                               // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinContractSerialNumber, True)                           then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CSN])       // SS_1006_1015_CQU_20121002
    else if (LinContext = '') or (LinContract = '') or (LinIssuerIdentifier = '')       then MensajeError := MSG_ERROR_CONTEXT
    else if not EsNumero(LinContext)                                                    then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONTEXT])
    else if EsNumero(LinContext) and not StringEsEntero(LinContext, False)              then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONTEXT])   // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinContract)                                                   then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONTRACT])
    else if EsNumero(LinContract) and not StringEsEntero(LinContext, False)             then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONTRACT])  // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinIssuerIdentifier)                                           then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_ISSUER])
    else if EsNumero(LinIssuerIdentifier)                                                                                                                   // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinIssuerIdentifier, False)                              then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_ISSUER])    // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinFechaHora)                                                  then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECHA])
    else if not EsNumero(LinImporte)                                                    then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPORTE])
    else if EsNumero(LinImporte) and not StringEsEntero(LinImporte, True)               then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPORTE])   // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinConcepto)                                                   then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONCEPTO])
    else if EsNumero(LinConcepto) and not StringEsEntero(LinConcepto, False)            then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONCEPTO])  // SS_1006_1015_CQU_20121002
//    else if LinNumeroConvenio = ''                                                      then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_CONVENIO])                  //SS_1015_MBE_20120622 //SS_1006_MBE_20121005
    else if (LinEsNegativo <> '0') and (LinEsNegativo <> '1')                           then MensajeError := MSG_ERROR_NEGATIVO
    else if CodigoConcesionaria <> StrToInt(LinCodigoConcesionaria)                     then MensajeError := Format(MSG_ERROR_CONCESIO, [IntToStr(CodigoConcesionaria), LinCodigoConcesionaria])
    else if LinNumTranEsta = ''                                                         then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IDESTA]);   //SS_1006_CQU_20130328

    // si hay error de parseo, entonces registrarlo
    // y retornar verdadero para que contin�e con la siguiente l�nea
    if MensajeError <> '' then begin
        Result := RegistrarRegistroRechazado(NombreArchivo, MensajeError, LinNumTransaccion, NumLinea);
        //LineaError.Add(Linea + ';' + MensajeError); // SS_1015_CQU_20120514  // SS_1015_CQU_20120530
        LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + MensajeError); // SS_1015_CQU_20120530
        Exit;
    end;


    //la l�nea est� OK y puede ser facturada en CN
    with spAgregarMovCtaOtraConc do begin
        Parameters.Refresh;
        Parameters.ParamByName('@MensajeError').Value               := null;
        Parameters.ParamByName('@CodigoConcesionaria').Value        := LinCodigoConcesionaria;
        Parameters.ParamByName('@ContractSerialNumber').Value       := LinContractSerialNumber;
        Parameters.ParamByName('@Context').Value                    := LinContext;
        Parameters.ParamByName('@Contract').Value                   := LinContract;
        Parameters.ParamByName('@IssuerIdentifier').Value           := LinIssuerIdentifier;
        Parameters.ParamByName('@NumeroTransaccion').Value          := LinNumTransaccion;
        Parameters.ParamByName('@FechaHoraMovimiento').Value        := ConvierteAFecha(LinFechaHora);
        Parameters.ParamByName('@Importe').Value                    := LinImporte;
        Parameters.ParamByName('@CodigoConcepto').Value             := LinConcepto;
        Parameters.ParamByName('@Signo').Value                      := LinEsNegativo;
        Parameters.ParamByName('@NombreArchivo').Value              := NombreArchivo;
        Parameters.ParamByName('@CodigoUsuario').Value              := 'Importador MovCtas';
        Parameters.ParamByName('@NumeroConvenioExterno').Value      := LinNumeroConvenio;               //SS_1015_MBE_20120622
        Parameters.ParamByName('@NumTranEstacionamiento').Value     := LinNumTranEsta;                  //SS_1006_CQU_20130130
        try
            ExecProc;
            CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
            Result := True;
            if CodigoRetorno < 0 then begin
                MensajeError := Parameters.ParamByName('@MensajeError').Value;
                with spAgregarMovCtaRechazado do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoConcesionaria').Value    := LinCodigoConcesionaria;
                    Parameters.ParamByName('@NumeroTransaccion').Value      := LinNumTransaccion;
                    Parameters.ParamByName('@FechaHoraMovimiento').Value    := ConvierteAFecha(LinFechaHora);
                    Parameters.ParamByName('@CodigoConcepto').Value         := LinConcepto;
                    Parameters.ParamByName('@ContractSerialNumber').Value   := LinContractSerialNumber;
                    Parameters.ParamByName('@Context').Value                := LinContext;
                    Parameters.ParamByName('@Contract').Value               := LinContract;
                    Parameters.ParamByName('@IssuerIdentifier').Value       := LinIssuerIdentifier;
                    Parameters.ParamByName('@Importe').Value                := LinImporte;
                    Parameters.ParamByName('@Signo').Value                  := LinEsNegativo;
                    Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
                    Parameters.ParamByName('@CodigoUsuario').Value          := 'Importador MovCtas';
                    Parameters.ParamByName('@MotivoRechazo').Value          := MensajeError;
                    Parameters.ParamByName('@NumeroLinea').Value            := NumLinea; // SS_1015_CQU_20120525
                    ExecProc;
                    //LineaError.Add(Linea + ';' + MensajeError); // SS_1015_CQU_20120510   // SS_1015_CQU_20120530
                    LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + MensajeError);  // SS_1015_CQU_20120530
                end;
            end;
        except on e:exception do begin
                EventLogReportEvent(elError, Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message]), '');
                // LineaError.Add(Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message])); // SS_1015_CQU_20120510 // SS_1015_CQU_20120530
                LineaError.Add(IntToStr(NumLinea) + ';' + Linea + ';' + Format(MSG_ERROR_INSERT, [LinNumTransaccion, FCodigoConcesionariaOrigen, e.Message])); // SS_1015_CQU_20120530
            end;
        end;
    end;   {with}

end;

{---------------------------------------------------------------------------------
        	ImportarLineaSaldoInicial

 Author			  : Nelson Droguett Sierra
 Date			    : 10-Mayo-2012
 Description	: Importa la l�nea en la tabla SaldoInicialOtrasConcesionarias
                    SS-1015-NDR-20120510

 Firma          :   SS_1015_CQU_20120525
 Description    :   Agrega Nro de L�nea al SaldoInicialRechazadoOtrasConcesionarias
----------------------------------------------------------------------------------}
function TMainForm.ImportarLineaSaldoInicial;
resourcestring
    MSG_ERROR_CAMPO_NUM     = 'El campo %s no es num�rico';
    MSG_ERROR_CAMPO_STR     = 'Falta el campo %s ';
    MSG_ERROR_YA_EXISTE     = 'Ya existe el registro en la tabla %s';
    MSG_ERROR_CONCESIO      = 'Las Concesionaria indicada en el nombre del archivo %s es distinta a la indicada en el registro de transacci�n %s';
    MSG_ERROR_NEGATIVO      = 'El campo [Indicador Importe Negativo o Positivo] no es 0 ni 1';
    MSG_ERROR_INSERT        = 'Error al insertar la Linea = %s, para la concesionaria %s: %s';
//    MSG_ERROR_TIPO_FISCAL   = 'Tipo fiscal incorrecto: %s. debe ser FA, FE, BA, BE';                // SS_1147_P3_MBE_20150116  // SS_1147_P1_MBE_20150113

    CAMPO_CODCON    = 'Codigo Concesionaria';
    CAMPO_CONVENIO  = 'Codigo Convenio';
//    CAMPO_CONCEPTO  = 'C�digo Concepto';           // SS_1147P_20141117
    CAMPO_CONC_EXEN = 'C�digo Concepto Exento';      // SS_1147P_20141117
    CAMPO_CONC_AFEC = 'C�digo Concepto Afecto';      // SS_1147P_20141117
    CAMPO_IMPORTE   = 'Importe';
    CAMPO_IMPOR_EXE = 'Importe Exento';              // SS_1147P_20141117
    CAMPO_IMPOR_AFE = 'Importe Afecto';              // SS_1147P_20141117
    CAMPO_IVA       = 'IVA';                         // SS_1147P_20141117
    CAMPO_NEGATIVO  = 'Negativo';
    CAMPO_FECHAEMI  = 'Fecha Emisi�n';
    CAMPO_FECHAVEN  = 'Fecha Vencimiento';

const
    separador = ';';

var
    //datos de la l�nea:
    LinCodigoConcesionaria,
    LinCodigoConvenio,
//    LinConcepto,                                  // SS_1147P_20141117
    LinConceptoExento,                              // SS_1147P_20141117
    LinConceptoAfecto,                              // SS_1147P_20141117
    LinImporte,
    LinImporteExento,                               // SS_1147P_20141117
    LinImporteAfecto,                               // SS_1147P_20141117
    LinImporteIva,                                  // SS_1147P_20141117
    LinEsNegativo,
    LinFechaEmision,
    LinFechaVencimiento,
    LinTipoFiscal, LinNumeroFiscal: string;         // SS_1147_P1_MBE_20150113

    CodigoRetorno : integer;
begin

    Result := False;

    if Trim(Linea) = '' then begin                  //SS_1006_1015_MBE_20120921
        Result := True;                             //SS_1006_1015_MBE_20120921
        Exit;                                       //SS_1006_1015_MBE_20120921
    end;                                            //SS_1006_1015_MBE_20120921
    
    //parsear la l�nea:
    LinCodigoConcesionaria  := Trim(GetItem(Linea, 1, separador));
    LinCodigoConvenio       := Trim(GetItem(Linea, 2, separador));
//    LinConcepto             := Trim(GetItem(Linea, 3, separador));          // SS_1147P_20141117
    LinConceptoExento       := Trim(GetItem(Linea, 3, separador));            // SS_1147P_20141117
    LinConceptoAfecto       := Trim(GetItem(Linea, 4, separador));            // SS_1147P_20141117
    LinImporte              := Trim(GetItem(Linea, 5, separador));
    LinImporteExento        := Trim(GetItem(Linea, 6, separador));            // SS_1147P_20141117
    LinImporteAfecto        := Trim(GetItem(Linea, 7, separador));            // SS_1147P_20141117
    LinImporteIva           := Trim(GetItem(Linea, 8, separador));            // SS_1147P_20141117
    LinEsNegativo           := Trim(GetItem(Linea, 9, separador));
    LinFechaEmision         := Trim(GetItem(Linea, 10, separador));
    LinFechaVencimiento     := Trim(GetItem(Linea, 11, separador));
    LinTipoFiscal           := Trim(GetItem(Linea, 12, separador));           // SS_1147_P1_MBE_20150113
    LinNumeroFiscal         := Trim(GetItem(Linea, 13, separador));           // SS_1147_P1_MBE_20150113

    //convertir la concesionaria a N�mero, para apoyar las b�squedas
    //if EsNumero(LinCodigoConcesionaria) then LinCodigoConcesionaria := IntToStr(StrToInt(LinCodigoConcesionaria));                    // SS_1006_1015_CQU_20121002

    //validar que:
    // no falte ning�n campo obligatorio
    // sean num�ricos los campos que deben serlo
    //
    MensajeError := '';
//    if not EsNumero(LinCodigoConcesionaria)                         then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CODCON])      // SS_1147_P2_MBE_20150116
//    else if EsNumero(LinCodigoConcesionaria)                                                                                              // SS_1147_P2_MBE_20150116  //SS_1006_1015_CQU_20121002
//        and not StringEsEntero(LinCodigoConcesionaria, False)       then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CODCON])      // SS_1147_P2_MBE_20150116  //SS_1006_1015_CQU_20121002
//    else                                                                                                                                  // SS_1147_P2_MBE_20150116
    if (LinCodigoConvenio = '')                                then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_CONVENIO])
//    else if not EsNumero(LinConcepto)                               then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONCEPTO])  // SS_1147P_20141117
//    else if EsNumero(LinConcepto)                                                                                                       // SS_1147P_20141117 SS_1006_1015_CQU_20121002
//        and not StringEsEntero(LinConcepto, False)                  then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONCEPTO])  // SS_1147P_20141117 SS_1006_1015_CQU_20121002
    else if not EsNumero(LinConceptoExento)
        and not StringEsEntero(LinConceptoExento, False)            then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONC_EXEN])   // SS_1147P_20141117
    else if not EsNumero(LinConceptoAfecto)
        and not StringEsEntero(LinConceptoAfecto, False)            then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONC_AFEC])   // SS_1147P_20141117
    else if EsNumero(LinImporte)                                                                                                        // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinImporte, True)                    then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPORTE])   // SS_1006_1015_CQU_20121002
    else if EsNumero(LinImporteExento)                                                                                                    // SS_1147P_20141117
        and not StringEsEntero(LinImporteExento, True)              then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPOR_EXE])   // SS_1147P_20141117
    else if EsNumero(LinImporteAfecto)                                                                                                    // SS_1147P_20141117
        and not StringEsEntero(LinImporteAfecto, True)              then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPOR_AFE])   // SS_1147P_20141117
    else if EsNumero(LinImporteIva)                                                                                                       // SS_1147P_20141117
        and not StringEsEntero(LinImporteIva, True)                 then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IVA])         // SS_1147P_20141117
    else if (LinEsNegativo <> '0') and (LinEsNegativo <> '1')       then MensajeError := MSG_ERROR_NEGATIVO
    else if not EsNumero(LinFechaEmision)                           then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECHAEMI])
    else if not EsNumero(LinFechaVencimiento)                       then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECHAVEN]);
//    else if CodigoConcesionaria <> StrToInt(LinCodigoConcesionaria) then MensajeError := Format(MSG_ERROR_CONCESIO, [IntToStr(CodigoConcesionaria), LinCodigoConcesionaria])  // SS_1147_P2_MBE_20150116
//    else if (LinTipoFiscal <> '') and (LinTipoFiscal <> 'BA') and                                                                         // SS_1147_P3_MBE_20150116          // SS_1147_P1_MBE_20150113
//            (LinTipoFiscal <> 'BE') and (LinTipoFiscal <> 'FA') and                                                                       // SS_1147_P3_MBE_20150116          // SS_1147_P1_MBE_20150113
//            (LinTipoFiscal <> 'FE')                                 then MensajeError := Format(MSG_ERROR_TIPO_FISCAL, [LinTipoFiscal]);  // SS_1147_P3_MBE_20150116          // SS_1147_P1_MBE_20150113
         

    // si hay error de parseo, entonces registrarlo
    // y retornar verdadero para que contin�e con la siguiente l�nea
    if MensajeError <> '' then begin
        Result := RegistrarRegistroRechazado(NombreArchivo, MensajeError,'', NumLinea);
        //LineaError.Add(Linea + separador + MensajeError); // SS_1015_CQU_20120514         // SS_1015_CQU_20120530
        LineaError.Add(IntToStr(NumLinea) + separador + Linea + separador + MensajeError);  // SS_1015_CQU_20120530
        Exit;
    end;

    //la l�nea est� OK
    with spAgregarSaldoInicialOtraConcesionaria do begin
        Parameters.Refresh;
        Parameters.ParamByName('@MensajeError').Value           := null;
        Parameters.ParamByName('@CodigoConcesionaria').Value    := LinCodigoConcesionaria;
//        Parameters.ParamByName('@CodigoConvenioExterno').Value  := LinCodigoConvenio;                             // SS_1147_P2_MBE_20150116
        Parameters.ParamByName('@NumeroConvenio').Value         := LinCodigoConvenio;                               // SS_1147_P2_MBE_20150116
//        Parameters.ParamByName('@CodigoConcepto').Value        := LinConcepto;                                    // SS_1147P_20141117
        Parameters.ParamByName('@CodigoConceptoExento').Value   := LinConceptoExento;                               // SS_1147P_20141117
        Parameters.ParamByName('@CodigoConceptoAfecto').Value   := LinConceptoAfecto;                               // SS_1147P_20141117
        Parameters.ParamByName('@Importe').Value                := LinImporte;
        Parameters.ParamByName('@ImporteExento').Value          := LinImporteExento;                                // SS_1147P_20141117
        Parameters.ParamByName('@ImporteAfecto').Value          := LinImporteAfecto;                                // SS_1147P_20141117
        Parameters.ParamByName('@ImporteIVA').Value             := LinImporteIva;                                   // SS_1147P_20141117
        Parameters.ParamByName('@Negativo').Value               := LinEsNegativo;
        Parameters.ParamByName('@FechaEmision').Value           := ConvierteAFecha(LinFechaEmision);
        Parameters.ParamByName('@FechaVencimiento').Value       := ConvierteAFecha(LinFechaVencimiento);
        Parameters.ParamByName('@UsuarioCreacion').Value        := 'Importador SaldoIni';
        Parameters.ParamByName('@IdArchivo').Value              := IDArchivo;   //SS_1147Z_MCA_20121229
        Parameters.ParamByName('@TipoComprobanteFiscal').Value      := IIf(LinTipoFiscal <> '', LinTipoFiscal,NULL);        // SS_1147_P1_MBE_20150113
        Parameters.ParamByName('@NumeroComprobanteFiscal').Value    := IIf(LinNumeroFiscal <> '', LinNumeroFiscal, NULL);   // SS_1147_P1_MBE_20150113
        try
            ExecProc;
            CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
            Result := True;
            if CodigoRetorno < 0 then begin
                MensajeError := Parameters.ParamByName('@MensajeError').Value;
                //LineaError.Add(Linea + separador + MensajeError); // SS_1015_CQU_20120514         // SS_1015_CQU_20120530
                LineaError.Add(IntToStr(NumLinea) + separador + Linea + separador + MensajeError);  // SS_1015_CQU_20120530
                with spAgregarSaldoInicialRechazadoOtraConcesionaria do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoConcesionaria').Value    := LinCodigoConcesionaria;
                    Parameters.ParamByName('@CodigoConvenioExterno').Value  := LinCodigoConvenio;
//                    Parameters.ParamByName('@CodigoConcepto').Value         := LinConcepto;                       // SS_1147P_20141117
                    Parameters.ParamByName('@CodigoConceptoExento').Value   := LinConceptoExento;                   // SS_1147P_20141117
                    Parameters.ParamByName('@CodigoConceptoAfecto').Value   := LinConceptoAfecto;                   // SS_1147P_20141117
                    Parameters.ParamByName('@Importe').Value                := LinImporte;
                    Parameters.ParamByName('@ImporteExento').Value          := LinImporteExento;                    // SS_1147P_20141117
                    Parameters.ParamByName('@ImporteAfecto').Value          := LinImporteAfecto;                    // SS_1147P_20141117
                    Parameters.ParamByName('@ImporteIVA').Value             := LinImporteIva;                       // SS_1147P_20141117
                    Parameters.ParamByName('@FechaEmision').Value           := ConvierteAFecha(LinFechaEmision);
                    Parameters.ParamByName('@FechaVencimiento').Value       := ConvierteAFecha(LinFechaVencimiento);
                    Parameters.ParamByName('@UsuarioCreacion').Value        := 'Importador SaldoIni';
                    Parameters.ParamByName('@EsNegativo').Value             := LinEsNegativo;                       // SS_1147P_20141117
                    Parameters.ParamByName('@MotivoRechazo').Value          := MensajeError;
                    Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
                    Parameters.ParamByName('@NumeroLinea').Value            := NumLinea;                            // SS_1015_CQU_20120525
                    ExecProc;
                end;
            end;
        except on e:exception do begin
                EventLogReportEvent(elError, Format(MSG_ERROR_INSERT, [LinCodigoConcesionaria+LinCodigoConvenio+LinConceptoExento, FCodigoConcesionariaOrigen, e.Message]), '');
                //LineaError.Add(Linea + ';' + Format(MSG_ERROR_INSERT, [LinCodigoConcesionaria+LinCodigoConvenio+LinConcepto, FCodigoConcesionariaOrigen, e.Message]));                                        // SS_1147P_20141117  // SS_1015_CQU_20120514        // SS_1015_CQU_20120530
                LineaError.Add(IntToStr(NumLinea) + separador + Linea + ';' + Format(MSG_ERROR_INSERT, [LinCodigoConcesionaria+LinCodigoConvenio+LinConceptoExento, FCodigoConcesionariaOrigen, e.Message]));   // SS_1147P_20141117  // SS_1015_CQU_20120530
            end;
        end;
    end;   {with}
end;


{---------------------------------------------------------------------------------
        	ImportarLineaCuotas
 Author			  : Nelson Droguett Sierra
 Date			    : 10-Mayo-2012
 Description	: Importa la l�nea en la tabla CuotasOtrasConcesionarias
                    SS-1015-NDR-20120510

 Firma          :   SS_1015_CQU_20120525
 Description    :   Agrega Nro de L�nea al SaldoInicialRechazadoOtrasConcesionarias
----------------------------------------------------------------------------------}
function TMainForm.ImportarLineaCuotas;
resourcestring
    MSG_ERROR_CAMPO_NUM     = 'El campo %s no es num�rico';
    MSG_ERROR_CAMPO_STR     = 'Falta el campo %s ';
    MSG_ERROR_YA_EXISTE     = 'Ya existe el registro en la tabla %s';
    MSG_ERROR_CONCESIO      = 'Las Concesionaria indicada en el nombre del archivo %s es distinta a la indicada en el registro de transacci�n %s';
    MSG_ERROR_INSERT        = 'Error al insertar la transacci�n Numero = %s, para la concesionaria %s: %s';

    CAMPO_CODCON    = 'Codigo Concesionaria';
    CAMPO_IDCUOTA   = 'IdentificadorCuota';
    CAMPO_CONVENIO  = 'Codigo Convenio';
    CAMPO_CONCEPTO  = 'C�digo Concepto';
    CAMPO_IMPORTE   = 'Importe';
    CAMPO_FECHAEMI  = 'Fecha Emisi�n';
    CAMPO_FECHAVEN  = 'Fecha Vencimiento';

const
    separador = ';';

var
    //datos de la l�nea:
    LinIdentificadorCuota,
    LinCodigoConcesionaria,
    LinCodigoConvenio,
    LinConcepto,
    LinImporte,
//    LinEsNegativo,                                //SS_1006_1015_MBE_20120921, no se usa
    LinFechaEmision,
    LinFechaVencimiento: string;

    CodigoRetorno : integer;
begin

    Result := False;

    if Trim(Linea) = '' then begin                  //SS_1006_1015_MBE_20120921
        Result := True;                             //SS_1006_1015_MBE_20120921
        Exit;                                       //SS_1006_1015_MBE_20120921
    end;                                            //SS_1006_1015_MBE_20120921
    
    //parsear la l�nea:
    LinIdentificadorCuota   := Trim(GetItem(Linea, 1, separador));
    LinCodigoConcesionaria  := Trim(GetItem(Linea, 2, separador));
    LinCodigoConvenio       := Trim(GetItem(Linea, 3, separador));
    LinConcepto             := Trim(GetItem(Linea, 4, separador));
    LinImporte              := Trim(GetItem(Linea, 5, separador));
    LinFechaEmision         := Trim(GetItem(Linea, 6, separador));
    LinFechaVencimiento     := Trim(GetItem(Linea, 7, separador));

    //convertir la concesionaria a N�mero, para apoyar las b�squedas
    //if EsNumero(LinCodigoConcesionaria) then LinCodigoConcesionaria := IntToStr(StrToInt(LinCodigoConcesionaria));                    // SS_1006_1015_CQU_20121002

    //validar que:
    // no falte ning�n campo obligatorio
    // sean num�ricos los campos que deben serlo
    //
    MensajeError := '';
    if not EsNumero(LinIdentificadorCuota)                          then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IDCUOTA])
    else if EsNumero(LinIdentificadorCuota)                                                                                             // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinIdentificadorCuota, True)         then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IDCUOTA])   // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinCodigoConcesionaria)                    then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CODCON])
    else if EsNumero(LinCodigoConcesionaria)                                                                                            // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinCodigoConcesionaria, False)       then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CODCON])    // SS_1006_1015_CQU_20121002
    else if (LinCodigoConvenio = '')                                then MensajeError := Format(MSG_ERROR_CAMPO_STR, [CAMPO_CONVENIO])
    else if not EsNumero(LinConcepto)                               then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONCEPTO])
    else if EsNumero(LinConcepto)                                                                                                       // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinConcepto, False)                  then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_CONCEPTO])  // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinImporte)                                then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPORTE])
    else if EsNumero(LinImporte)                                                                                                        // SS_1006_1015_CQU_20121002
        and not StringEsEntero(LinImporte, True)                    then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IMPORTE])   // SS_1006_1015_CQU_20121002
    else if not EsNumero(LinFechaEmision)                           then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECHAEMI])
    else if not EsNumero(LinFechaVencimiento)                       then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECHAVEN])
    else if CodigoConcesionaria <> StrToInt(LinCodigoConcesionaria) then MensajeError := Format(MSG_ERROR_CONCESIO, [IntToStr(CodigoConcesionaria), LinCodigoConcesionaria]);

    // si hay error de parseo, entonces registrarlo
    // y retornar verdadero para que contin�e con la siguiente l�nea
    if MensajeError <> '' then begin
        Result := RegistrarRegistroRechazado(NombreArchivo, MensajeError, LinIdentificadorCuota, NumLinea);
        //LineaError.Add(Linea + separador + MensajeError); // SS_1015_CQU_20120514         // SS_1015_CQU_20120530
        LineaError.Add(IntToStr(NumLinea) + separador + Linea + separador + MensajeError);  // SS_1015_CQU_20120530
        Exit;
    end;


    //la l�nea est� OK y puede ser facturada en CN
    with spAgregarCuotaOtraConcesionaria do begin
        Parameters.Refresh;
        Parameters.ParamByName('@MensajeError').Value          := null;
        Parameters.ParamByName('@NumeroTransaccionCuota').Value:= LinIdentificadorCuota;
        Parameters.ParamByName('@CodigoConcesionaria').Value   := LinCodigoConcesionaria;
        Parameters.ParamByName('@CodigoConvenioExterno').Value := LinCodigoConvenio;
        Parameters.ParamByName('@CodigoConcepto').Value        := LinConcepto;
        Parameters.ParamByName('@Importe').Value               := LinImporte;
        Parameters.ParamByName('@FechaEmision').Value          := ConvierteAFecha(LinFechaEmision);
        Parameters.ParamByName('@FechaVencimiento').Value      := ConvierteAFecha(LinFechaVencimiento);
        Parameters.ParamByName('@UsuarioCreacion').Value       := 'Importador Cuotas';
        try
            ExecProc;
            CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
            Result := True;
            if CodigoRetorno < 0 then begin
                MensajeError := Parameters.ParamByName('@MensajeError').Value;
                //LineaError.Add(Linea + separador + MensajeError); // SS_1015_CQU_20120514         // SS_1015_CQU_20120530
                LineaError.Add(IntToStr(NumLinea) + separador + Linea + separador + MensajeError);  // SS_1015_CQU_20120530
                with spAgregarCuotaRechazadaOtraConcesionaria do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@NumeroTransaccionCuota').Value := LinIdentificadorCuota;
                    Parameters.ParamByName('@CodigoConcesionaria').Value    := LinCodigoConcesionaria;
                    Parameters.ParamByName('@CodigoConvenioExterno').Value  := LinCodigoConvenio;
                    Parameters.ParamByName('@CodigoConcepto').Value         := LinConcepto;
                    Parameters.ParamByName('@Importe').Value                := LinImporte;
                    Parameters.ParamByName('@FechaEmision').Value           := ConvierteAFecha(LinFechaEmision);
                    Parameters.ParamByName('@FechaVencimiento').Value       := ConvierteAFecha(LinFechaVencimiento);
                    Parameters.ParamByName('@UsuarioCreacion').Value        := 'Importador Cuotas';
                    Parameters.ParamByName('@MotivoRechazo').Value          := MensajeError;
                    Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
                    Parameters.ParamByName('@NumeroLinea').Value            := NumLinea;   // SS_1015_CQU_20120525
                    ExecProc;
                end;
            end;
        except on e:exception do begin
                EventLogReportEvent(elError, Format(MSG_ERROR_INSERT, [LinCodigoConcesionaria+LinCodigoConvenio+LinConcepto, FCodigoConcesionariaOrigen, e.Message]), '');
                //LineaError.Add(Linea + separador + Format(MSG_ERROR_INSERT, [LinCodigoConcesionaria+LinCodigoConvenio+LinConcepto, FCodigoConcesionariaOrigen, e.Message])); // SS_1015_CQU_20120514          // SS_1015_CQU_20120530
                LineaError.Add(IntToStr(NumLinea) + separador + Linea + separador + Format(MSG_ERROR_INSERT, [LinCodigoConcesionaria+LinCodigoConvenio+LinConcepto, FCodigoConcesionariaOrigen, e.Message]));   // SS_1015_CQU_20120530
            end;
        end;
    end;   {with}
end;


{---------------------------------------------------------------------------------
        	ObtenerConcesionariasQueFacturaCN

 Author			: mbecerra
 Date			: 31-Marzo-2011
 Description	:
 					Carga todas las concesionarias, cuyos tr�nsitos los factura CN
----------------------------------------------------------------------------------}
procedure TMainForm.ObtenerConcesionariasQueFacturaCN;
begin
    FListaConcesionariasLasFacturaCN.Clear;
    with spObtenerConcesionarias do begin
        Close;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoTipoConcesionaria').Value := NULL;
        Open;
        while not Eof do begin
            if FieldByName('TransitosLosFacturaCN').AsBoolean then begin
                FListaConcesionariasLasFacturaCN.Add(FieldByName('CodigoConcesionaria').AsString);
            end;

            Next;
        end;

        Close;
    end;
end;


{---------------------------------------------------------------------------------
        	GrabarArchivoEnCAC

 Author			: mbecerra
 Date			: 10-Agosto-2010
 Description	:
 					1.-	Determina si el archivo ya fue procesado
                    2.-	Si no fue procesado, lo graba en la tabla
                    	TransaccionesCopAMB
                    3.-	Si ya fue procesado, entonces env�a el correo

----------------------------------------------------------------------------------}
function TMainForm.GrabarArchivoEnCAC;
resourcestring
    SQL_ARCHIVO_EXISTE		= 'SELECT NombreArchivo FROM ArchivosProcesadosOtrasConcesionarias (NOLOCK) ' +
                        	  'WHERE NombreArchivo =  ''%s'' ';
    SQL_COD_CONCESIONARIA   = 'SELECT CodigoConcesionaria FROM Concesionarias (NOLOCK) WHERE CodigoConcesionaria = %s ';
    SQL_CONCE_FACTURA_CN    = 'SELECT CAST(TransitosLosFacturaCN AS INTEGER) FROM Concesionarias (NOLOCK) WHERE CodigoConcesionaria = %d';

    MSG_ERROR_INSERTANDO	= 'Error al insertar registro (L�nea %d) en tabla TransitosPendientesDelCOP: %s';
    MSG_YA_EXISTE			= 'Ya existe el NumCorrCO = %s';
    MSG_NOM_ARCHIVO_MALO    = 'El nombre del archivo no cumple formato esperado:  %s';
    MSG_NO_EXISTE_CONCES    = 'No existe concesionaria de c�digo : %s';
    MSG_TRX_NO_CONOCIDA     = 'Transacci�n %s no conocida';

    //Mensajes del correo
    EMAIL_TEMA				= 'Importar Archivo Transitos Otras Concesionarias: Se intenta re-procesar archivo';
    EMAIL_BODY				= 'Archivo : %s';


    SQL_UPDATE_ARCHIVO_NUMCORR    = 'UPDATE ArchivosProcesadosOtrasConcesionarias SET NumCorrCOInicial = %d, NumCorrCOFinal = %d WHERE IDArchivo = %d'; //SS_1006_NDR_20130228

const
    Separador = '_';         //separador del nombre del archivo

var
	Linea, PrefijoArchivo : string;
    NumLinea, CodigoConcesionaria : integer;
    FechaProceso : TDateTime;
    ArchivoYaFueProcesado : boolean;
    MensajeErrorLinea : string;

    //datos para el correo
    Mensaje : TIdMessage;

    //
    NuevoNombreArchivo : string;				// SS_1015_CQU_20120423
    IDArchivo : integer;                //SS_1006_NDR_20130228
begin

 	aqryArchivo.Close;
    spAgregarArchivo.Close;
    spAgregarTransitoOtraConcesionaria.Close;
    spAgregarTransaccionEstacionamiento.Close;
    spAgregarRegRechOtrasConcesio.Close;
    spAgregarTTORechOtraConcesio.Close;
    spAgregarESTARechOtraConcesio.Close;                                        //SS-1006-NDR-20111212
    spObtenerConcesionarias.Close;                                              //SS-1006-NDR-20111212



    aqryArchivo.Connection			                := DMConnections.BaseCAC;
    spAgregarArchivo.Connection		                := DMConnections.BaseCAC;
    spAgregarTransitoOtraConcesionaria.Connection	:= DMConnections.BaseCAC;
    spAgregarTransaccionEstacionamiento.Connection  := DMConnections.BaseCAC;
    spAgregarRegRechOtrasConcesio.Connection        := DMConnections.BaseCAC;
    spAgregarTTORechOtraConcesio.Connection         := DMConnections.BaseCAC;
    spAgregarESTARechOtraConcesio.Connection        := DMConnections.BaseCAC;
    spObtenerConcesionarias.Connection              := DMConnections.BaseCAC;


    Result := True;
    ArchivoYaFueProcesado := False;

    //cargar el listado de concesionarias cuyos tr�nsitos
    //los factura 100% CN
    ObtenerConcesionariasQueFacturaCN();

    //determinar el tipo de archivo que va a procesar
    PrefijoArchivo              := GetItem(NombreArchivo,1, Separador);
    FCodigoConcesionariaOrigen  := GetItem(NombreArchivo,2, Separador);
    FCodigoConcesionariaDestino := GetItem(NombreArchivo,3, Separador);
    FNomArchTipoTrx             := GetItem(NombreArchivo,4, Separador);
    FNomArchFecha               := GetItem(NombreArchivo,5, Separador);
    FNomArchHora                := Copy(FNomArchFecha, 9, 6);           //hora
    FNomArchFecha               := Copy(FNomArchFecha,1,8);             //fecha

    // Inicio Bloque SS_1015_CQU_20120530
    {
    // Genero el nombre del archivo en caso de ser rechazado SS_1015_CQU_20120510
    NuevoNombreArchivo := FNombreRechazadasTransac
    + Separador + '01'
    + Separador + FCodigoConcesionariaOrigen
    + Separador + FNomArchTipoTrx
    + Separador + FormatDateTime ('yyyymmddHHMMss', Date + Time)
    + '.txt';
    }
    NuevoNombreArchivo := GenerarNuevoNombre(NombreArchivo, FNombreRechazadasTransac);
    // Fin Bloque SS_1015_CQU_20120530
    if  (PrefijoArchivo <> FNomArchParametro) or (FCodigoConcesionariaOrigen = '') or
        (FNomArchTipoTrx = '') or (FNomArchFecha = '') or (FNomArchHora = '') then
    begin
        EventLogReportEvent(elError, Format(MSG_NOM_ARCHIVO_MALO, [NombreArchivo]), '');
        Result := False;
        MoverArchivoCambiarNombre(FCarpetaEntrada, FCarpetaRechazadasTransac, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120510
    end;

    CodigoConcesionaria := 0;
    if Result then begin
        CodigoConcesionaria := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_COD_CONCESIONARIA,[FCodigoConcesionariaOrigen]) );
        if CodigoConcesionaria = 0 then begin
            EventLogReportEvent(elError, Format(MSG_NO_EXISTE_CONCES, [FCodigoConcesionariaOrigen]), '');
            Result := False;
            MoverArchivoCambiarNombre(FCarpetaEntrada, FCarpetaRechazadasTransac, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120510
        end;
    end;

    if Result then begin
        FConcesionariaFacturaEnCN := (QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_CONCE_FACTURA_CN, [CodigoConcesionaria])) = 1);
    end;


    if Result then begin   //validar el tipo de Archivo
        if  (FNomArchTipoTrx <> TIPO_MLFF) and
            (FNomArchTipoTrx <> TIPO_PEAJ) and
            (FNomArchTipoTrx <> TIPO_ESTA) then
        begin
            EventLogReportEvent(elError, Format(MSG_NO_EXISTE_CONCES, [FCodigoConcesionariaOrigen]), '');
            Result := False;
            MoverArchivoCambiarNombre(FCarpetaEntrada, FCarpetaRechazadasTransac, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120510
        end;

    end;

    if Result then begin
        FechaProceso		:= StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()'));

        //validar que el archivo no ha sido procesado
        with aqryArchivo do begin
            Parameters.Clear;
            SQL.Clear;
            SQL.Text := Format(SQL_ARCHIVO_EXISTE, [NombreArchivo]);
            Open;
        	Result := IsEmpty;
            ArchivoYaFueProcesado := not Result;
        end;
        aqryArchivo.Close;
    end;

    //Importar el archivo
    if Result then begin       //Procesar las Lineas
        FNumCorrCOMin := 0;
        FNumCorrCOMax := 0;
        NumLinea := 0;
        DMConnections.BaseCAC.Execute('BEGIN TRAN spInsertarArchOtrasConc');


        //BEGIN : SS_1006_NDR_20130228-------------------------------------------------------------------
        //grabar el archivo a procesar
	      with spAgregarArchivo do
        begin
	        Parameters.Refresh;
	        Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
	        Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;
	        Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
          Parameters.ParamByName('@NumCorrCOInicial').Value       := FNumCorrCOMin;
          Parameters.ParamByName('@NumCorrCOFinal').Value         := FNumCorrCoMax;
          Parameters.ParamByName('@IDArchivo').Value              := null;
          try
          	ExecProc;
            IDArchivo := Parameters.ParamByName('@IDArchivo').Value;
            // Si no pudo insertar el archivo, error.
            if IDArchivo = -1 then
            begin
              	Result := False;
                EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + NombreArchivo, '');
            end;

          except
            on e:exception do
            begin
              Result := False;
              EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');
          	end;
          end;
    	  end;
        //END : SS_1006_NDR_20130228-------------------------------------------------------------------



        while Result and (NumLinea < ContenidoArchivo.Count) do begin
        	Linea := ContenidoArchivo[NumLinea];
            if not ImportarLinea(NombreArchivo, Linea, CodigoConcesionaria, NumLinea + 1, MensajeErrorLinea) then begin
                EventLogReportEvent(elError, Format(MSG_ERROR_INSERTANDO, [NumLinea + 1, MensajeErrorLinea]), '');
                Result := False;
                //LineaError.Add(Linea + Format(MSG_ERROR_INSERTANDO, [NumLinea + 1, MensajeErrorLinea]));  // SS_1015_CQU_20120510     // SS_1015_CQU_20120530
                LineaError.Add(IntToStr(NumLinea + 1) + ';' + Linea + Format(MSG_ERROR_INSERTANDO, [NumLinea + 1, MensajeErrorLinea])); // SS_1015_CQU_20120530
            end;

            Inc(NumLinea);
        end;

        if Result then                                                                                               //SS_1006_NDR_20130228
        begin                                                                                                        //SS_1006_NDR_20130228
          try
            DMConnections.BaseCAC.Execute(Format(SQL_UPDATE_ARCHIVO_NUMCORR,[FNumCorrCOMin,FNumCorrCoMax,IDArchivo])); //SS_1006_NDR_20130228
          except
            Result := False        
          end;
        end;                                                                                                         //SS_1006_NDR_20130228


        //BEGIN : SS_1006_NDR_20130228-------------------------------------------------------------------
        ////finalmente grabar el archivo procesado
        //if Result then begin
	      //  with spAgregarArchivo do begin
	      //      Parameters.Refresh;
	      //      Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
	      //      Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;
	      //      Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
        //        Parameters.ParamByName('@NumCorrCOInicial').Value       := FNumCorrCOMin;
        //        Parameters.ParamByName('@NumCorrCOFinal').Value         := FNumCorrCoMax;
        //        try
        //        	ExecProc;
        //        except on e:exception do begin
        //            	Result := False;
        //                EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');
        //        	end;
        //        end;
    	  //  end;
        //end;
        //END : SS_1006_NDR_20130228-------------------------------------------------------------------

        if Result then DMConnections.BaseCAC.Execute('COMMIT TRAN spInsertarArchOtrasConc')
        else DMConnections.BaseCAC.Execute('ROLLBACK TRAN spInsertarArchOtrasConc');

    end;

    if ArchivoYaFueProcesado then begin                  //enviar correo
    	try
        	Mensaje								:= TIdMessage.Create(Self);
        	Mensaje.Subject						:= EMAIL_TEMA;
            Mensaje.Body.Text					:= Format(EMAIL_BODY, [NombreArchivo]);
            Mensaje.From.Address				:= FEmailFrom;
            Mensaje.Recipients.EMailAddresses	:= FEmailTo;
            IdSMTPEnviarCorreo.Host				:= FEmailServerSMTP;
            IdSMTPEnviarCorreo.Connect;
            IdSMTPEnviarCorreo.Send(Mensaje);

            // mover a la carpeta duplicados
            //    RenameFile(FCarpetaEntrada + NombreArchivo, FCarpetaDuplicados + NombreArchivo)
            MoverArchivo(FCarpetaEntrada, FCarpetaDuplicados, NombreArchivo, True); // SS-1015-CQU-20120423

        finally
            IdSMTPEnviarCorreo.Disconnect();
            Mensaje.Free;
        end;
    end;
    // Inicio Bloque SS_1015_CQU_20150510
    if (LineaError.Count > 0) then begin
        LineaError.SaveToFile(FCarpetaRechazadasTransac + NuevoNombreArchivo);
        LineaError.Free;
    end;
    // Fin Bloque SS_1015_CQU_20150510
end;


{---------------------------------------------------------------------------------
        	GrabarArchivoMovCta

 Author			: mbecerra
 Date			: 09-Febrero-2012
 Description	:
 					1.-	Determina si el archivo ya fue procesado
                    2.-	Si no fue procesado, lo graba en la tabla
                    	MovimientosCuentasOtrasConcesionarias
                    3.-	Si ya fue procesado, entonces lo graba en archivos
                        Duplicados

                    SS_1015_MBE_20120206
----------------------------------------------------------------------------------}
function TMainForm.GrabarArchivoMovCta;
resourcestring
    SQL_ARCHIVO_EXISTE		= 'SELECT NombreArchivo FROM ArchivosProcesadosOtrasConcesionarias (NOLOCK) ' +
                        	  'WHERE NombreArchivo =  ''%s'' ';
    SQL_COD_CONCESIONARIA   = 'SELECT CodigoConcesionaria FROM Concesionarias (NOLOCK) WHERE CodigoConcesionaria = %s ';

    MSG_ERROR_INSERTANDO	= 'Error al insertar registro (L�nea %d) en tabla MovimientosCuentasOtrasConcesionarias: %s';
    MSG_NOM_ARCHIVO_MALO    = 'El nombre del archivo no cumple formato esperado:  %s';
    MSG_NO_EXISTE_CONCES    = 'No existe concesionaria de c�digo : %s';

    //Mensajes del correo
    EMAIL_TEMA				= 'Importar Archivo MovCta. Otras Concesionarias: Se intenta re-procesar archivo';
    EMAIL_BODY				= 'Archivo : %s';

const
    Separador = '_';         //separador del nombre del archivo

var
	Linea, PrefijoArchivo : string;
    NumLinea, CodigoConcesionaria : integer;
    FechaProceso : TDateTime;
    ArchivoYaFueProcesado : boolean;
    MensajeErrorLinea : string;

    //datos para el correo
    Mensaje : TIdMessage;

    NuevoNombreArchivo : string; // SS_1015_CQU_20120510

    IDArchivo : integer;                                                        //SS_1006_NDR_20130228
begin

   	aqryArchivo.Close;
    spAgregarArchivo.Close;
    spAgregarRegRechOtrasConcesio.Close;
    spAgregarMovCtaOtraConc.Close;
    spAgregarMovCtaRechazado.Close;
    spObtenerConcesionarias.Close;



    aqryArchivo.Connection			            := DMConnections.BaseCAC;
    spAgregarRegRechOtrasConcesio.Connection    := DMConnections.BaseCAC;
    spAgregarArchivo.Connection		            := DMConnections.BaseCAC;
    spAgregarMovCtaOtraConc.Connection          := DMConnections.BaseCAC;
    spAgregarMovCtaRechazado.Connection         := DMConnections.BaseCAC;
    spObtenerConcesionarias.Connection          := DMConnections.BaseCAC;


    Result := True;
    ArchivoYaFueProcesado := False;


    //determinar el tipo de archivo que va a procesar
    PrefijoArchivo              := GetItem(NombreArchivo,1, Separador);
    FCodigoConcesionariaOrigen  := GetItem(NombreArchivo,2, Separador);
    FCodigoConcesionariaDestino := GetItem(NombreArchivo,3, Separador);
    FNomArchFecha               := GetItem(NombreArchivo,4, Separador);
    FNomArchHora                := Copy(FNomArchFecha, 9, 6);           //hora
    FNomArchFecha               := Copy(FNomArchFecha,1,8);             //fecha

    FNomArchTipoTrx := TIPO_MOV;    // SS_1015_CQU_20120611

    // Inicio Bloque SS_1015_CQU_20120530
    {
    // Genero el nombre del archivo enc aso de ser rechazado SS_1015_CQU_20120510
    NuevoNombreArchivo := FNombreRechazadosMovCta
    + Separador + '01'
    + Separador + FCodigoConcesionariaOrigen
    + Separador + TIPO_MOV
    + Separador + FormatDateTime ('yyyyMMddhhnnss', Date + Time)
    + '.txt';
    }
    NuevoNombreArchivo := GenerarNuevoNombre(NombreArchivo, FNombreRechazadosMovCta);
    // Fin Bloque SS_1015_CQU_20120530
    if  (PrefijoArchivo <> FNomArchParametroMovCta) or (FCodigoConcesionariaOrigen = '') or
        (FNomArchFecha = '') or (FNomArchHora = '') then
    begin
        EventLogReportEvent(elError, Format(MSG_NOM_ARCHIVO_MALO, [NombreArchivo]), '');
        MoverArchivoCambiarNombre(FCarpetaEntradaMovCta, FCarpetaRechazadosMovCta, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120510
        Result := False;
    end;

    CodigoConcesionaria := 0;
    if Result then begin
        CodigoConcesionaria := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_COD_CONCESIONARIA,[FCodigoConcesionariaOrigen]) );
        if CodigoConcesionaria = 0 then begin
            EventLogReportEvent(elError, Format(MSG_NO_EXISTE_CONCES, [FCodigoConcesionariaOrigen]), '');
            MoverArchivoCambiarNombre(FCarpetaEntradaMovCta, FCarpetaRechazadosMovCta, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120510
            Result := False;
        end;
    end;

    if Result then begin
        FechaProceso		:= StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()'));

        //validar que el archivo no ha sido procesado
        with aqryArchivo do begin
            Parameters.Clear;
            SQL.Clear;
            SQL.Text := Format(SQL_ARCHIVO_EXISTE, [NombreArchivo]);
            Open;
        	Result := IsEmpty;
            ArchivoYaFueProcesado := not Result;
        end;
        aqryArchivo.Close;
    end;

    //Importar el archivo
    if Result then begin       //Procesar las Lineas
        NumLinea := 0;
        DMConnections.BaseCAC.Execute('BEGIN TRAN spInsertarMovCtas');


        //BEGIN : SS_1006_NDR_20130228-----------------------------------------------------------------------------
        //grabar el archivo a procesar
	        with spAgregarArchivo do
          begin
	          Parameters.Refresh;
	          Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
	          Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;
	          Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
            Parameters.ParamByName('@NumCorrCOInicial').Value       := null;
            Parameters.ParamByName('@NumCorrCOFinal').Value         := null;
            Parameters.ParamByName('@IDArchivo').Value              := null;
            try
            	ExecProc;
              // Si no pudo insertar el archivo, error.
              // if Parameters.ParamByName('@IDArchivo').Value = -1 then    // SS_1006_CQU_20130328
              IDArchivo := Parameters.ParamByName('@IDArchivo').Value;      // SS_1006_CQU_20130328
              if IDArchivo = -1 then                                        // SS_1006_CQU_20130328
              begin
                	Result := False;
                  EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + NombreArchivo, '');
                  MoverArchivoCambiarNombre(FCarpetaEntradaMovCta, FCarpetaRechazadosMovCta, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120510
              end;
            except
              on e:exception do
              begin
                	Result := False;
                  EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');
                  MoverArchivoCambiarNombre(FCarpetaEntradaMovCta, FCarpetaRechazadosMovCta, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120510
            	end;
            end;
    	    end;
        //END : SS_1006_NDR_20130228-----------------------------------------------------------------------------


        while Result and (NumLinea < ContenidoArchivo.Count) do begin
        	Linea := ContenidoArchivo[NumLinea];
            if not ImportarLineaMovCta(NombreArchivo, Linea, CodigoConcesionaria, NumLinea + 1, MensajeErrorLinea) then begin
                EventLogReportEvent(elError, Format(MSG_ERROR_INSERTANDO, [NumLinea + 1, MensajeErrorLinea]), '');
                Result := False;
            end;

            Inc(NumLinea);
        end;


        //BEGIN : SS_1006_NDR_20130228-----------------------------------------------------------------------------
        ////finalmente grabar el archivo procesado
        //if Result then begin
	      //  with spAgregarArchivo do begin
	      //      Parameters.Refresh;
	      //      Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
	      //      Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;
	      //      Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
        //        Parameters.ParamByName('@NumCorrCOInicial').Value       := null;
        //        Parameters.ParamByName('@NumCorrCOFinal').Value         := null;
        //        try
        //        	ExecProc;
        //        except on e:exception do begin
        //            	Result := False;
        //                EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');
        //                MoverArchivoCambiarNombre(FCarpetaEntradaMovCta, FCarpetaRechazadosMovCta, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120510
        //        	end;
        //        end;
    	  //  end;
        //end;
        //END : SS_1006_NDR_20130228-----------------------------------------------------------------------------

        if Result then DMConnections.BaseCAC.Execute('COMMIT TRAN spInsertarMovCtas')
        else DMConnections.BaseCAC.Execute('ROLLBACK TRAN spInsertarMovCtas');

    end;

    if ArchivoYaFueProcesado then begin                  //enviar correo
    	try
        	Mensaje								:= TIdMessage.Create(Self);
        	Mensaje.Subject						:= EMAIL_TEMA;
            Mensaje.Body.Text					:= Format(EMAIL_BODY, [NombreArchivo]);
            Mensaje.From.Address				:= FEmailFrom;
            Mensaje.Recipients.EMailAddresses	:= FEmailTo;
            IdSMTPEnviarCorreo.Host				:= FEmailServerSMTP;
            IdSMTPEnviarCorreo.Connect;
            IdSMTPEnviarCorreo.Send(Mensaje);

            //mover a la carpeta duplicados
            //RenameFile(FCarpetaEntradaMovCta + NombreArchivo, FCarpetaDuplicadosMovCta + NombreArchivo);
            MoverARchivo(FCarpetaEntradaMovCta, FCarpetaDuplicadosMovCta, NombreArchivo, True); // SS_1015_CQU_20120423

        finally
            IdSMTPEnviarCorreo.Disconnect();
            Mensaje.Free;
        end;
    end;
    // Inicio Bloque SS_1015_CQU_20150514
    if (LineaError.Count > 0) then begin
        LineaError.SaveToFile(FCarpetaRechazadosMovCta + NuevoNombreArchivo);
        LineaError.Free;
    end;
    // Fin Bloque SS_1015_CQU_20150514
end;

{---------------------------------------------------------------------------------
        	GrabarArchivoSaldoInicial
 Author			: Nelson Droguett Sierra
 Date			  : 10-Mayo-2012
 Description: 1.-	Determina si el archivo ya fue procesado
              2.-	Si no fue procesado, lo graba en la tabla
                 	SaldoInicialOtrasConcesionarias
              3.-	Si ya fue procesado, entonces lo graba en archivos Duplicados
              SS-1015-NDR-20120510
----------------------------------------------------------------------------------}
function TMainForm.GrabarArchivoSaldoInicial;
resourcestring
    SQL_ARCHIVO_EXISTE		= 'SELECT NombreArchivo FROM ArchivosProcesadosOtrasConcesionarias (NOLOCK) ' +
                        	  'WHERE NombreArchivo =  ''%s'' ';
    SQL_COD_CONCESIONARIA   = 'SELECT CodigoConcesionaria FROM Concesionarias (NOLOCK) WHERE CodigoConcesionaria = %s ';

    MSG_ERROR_INSERTANDO	= 'Error al insertar registro (L�nea %d) en tabla SaldoInicialOtrasConcesionarias: %s';
    MSG_NOM_ARCHIVO_MALO    = 'El nombre del archivo no cumple formato esperado:  %s';
    MSG_NO_EXISTE_CONCES    = 'No existe concesionaria de c�digo : %s';

    //Mensajes del correo
    EMAIL_TEMA				= 'Importar Archivo Saldo Inicial Otras Concesionarias: Se intenta re-procesar archivo';
    EMAIL_BODY				= 'Archivo : %s';

const
    Separador = '_';         //separador del nombre del archivo

var
	  Linea, PrefijoArchivo : string;
    NumLinea, CodigoConcesionaria : integer;
    FechaProceso : TDateTime;
    ArchivoYaFueProcesado : boolean;
    MensajeErrorLinea : string;

    //datos para el correo
    Mensaje : TIdMessage;
    NuevoNombreArchivo : string; // SS_1015_CQU_20120514
    IDArchivo : integer;          //SS_1006_NDR_20130228
begin

   	aqryArchivo.Close;
    spAgregarArchivo.Close;
    spAgregarRegRechOtrasConcesio.Close;
    spAgregarSaldoInicialOtraConcesionaria.Close;
    spAgregarSaldoInicialRechazadoOtraConcesionaria.Close;
    spObtenerConcesionarias.Close;



    aqryArchivo.Connection			                                := DMConnections.BaseCAC;
    spAgregarRegRechOtrasConcesio.Connection                    := DMConnections.BaseCAC;
    spAgregarArchivo.Connection		                              := DMConnections.BaseCAC;
    spAgregarSaldoInicialOtraConcesionaria.Connection		        := DMConnections.BaseCAC;
    spAgregarSaldoInicialRechazadoOtraConcesionaria.Connection	:= DMConnections.BaseCAC;
    spObtenerConcesionarias.Connection                          := DMConnections.BaseCAC;


    Result := True;
    ArchivoYaFueProcesado := False;


    //determinar el tipo de archivo que va a procesar
    PrefijoArchivo              := GetItem(NombreArchivo,1, Separador);
    FCodigoConcesionariaOrigen  := GetItem(NombreArchivo,2, Separador);
    FCodigoConcesionariaDestino := GetItem(NombreArchivo,3, Separador);
    FNomArchFecha               := GetItem(NombreArchivo,4, Separador);
    FNomArchHora                := Copy(FNomArchFecha, 9, 6);           //hora
    FNomArchFecha               := Copy(FNomArchFecha,1,8);             //fecha

    // Inicio Bloque SS_1015_CQU_20120530
    {
    // Genero el nombre del archivo enc aso de ser rechazado SS_1015_CQU_20120514
    NuevoNombreArchivo := FNombreRechazadosSaldosIni
    + Separador + '01'
    + Separador + FCodigoConcesionariaOrigen
    + Separador + TIPO_SALD
    + Separador + FormatDateTime ('yyyyMMddhhnnss', Date + Time)
    + '.txt';
    }
    NuevoNombreArchivo := GenerarNuevoNombre(NombreArchivo, FNombreRechazadosSaldosIni);
    // Fin Bloque SS_1015_CQU_20120530
    // El prefijo no viene en el archivo por lo cual se coloca SS_1015_CQU_20120514
    //if(FNomArchTipoTrx = '') then FNomArchTipoTrx := TIPO_SALD;   // SS_1015_CQU_20120517
    FNomArchTipoTrx := TIPO_SALD;                                   // SS_1015_CQU_20120517

    if  (PrefijoArchivo <> FNomArchParametroSaldoIni) or (FCodigoConcesionariaOrigen = '') or
        (FNomArchFecha = '') or (FNomArchHora = '') then
    begin
        EventLogReportEvent(elError, Format(MSG_NOM_ARCHIVO_MALO, [NombreArchivo]), '');
        MoverArchivoCambiarNombre(FCarpetaEntradaSaldosIni, FCarpetaRechazadosSaldosIni, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120514
        Result := False;
    end;

    CodigoConcesionaria := 0;
    if Result then begin
        CodigoConcesionaria := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_COD_CONCESIONARIA,[FCodigoConcesionariaOrigen]) );
        if CodigoConcesionaria = 0 then begin
            EventLogReportEvent(elError, Format(MSG_NO_EXISTE_CONCES, [FCodigoConcesionariaOrigen]), '');
            MoverArchivoCambiarNombre(FCarpetaEntradaSaldosIni, FCarpetaRechazadosSaldosIni, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120514
            Result := False;
        end;
    end;

    if Result then begin
        FechaProceso		:= StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()'));

        //validar que el archivo no ha sido procesado
        with aqryArchivo do begin
            Parameters.Clear;
            SQL.Clear;
            SQL.Text := Format(SQL_ARCHIVO_EXISTE, [NombreArchivo]);
            Open;
          	Result := IsEmpty;
            ArchivoYaFueProcesado := not Result;
        end;
        aqryArchivo.Close;
    end;

    //Importar el archivo
    if Result then begin       //Procesar las Lineas
        NumLinea := 0;
        DMConnections.BaseCAC.Execute('BEGIN TRAN spInsertarSaldoInicial');


        //BEGIN : SS_1006_NDR_20130228------------------------------------------------------------------------------------
        //grabar el archivo a procesar para tener el IDArchivo
        if Result then
        begin
          with spAgregarArchivo do
          begin
            Parameters.Refresh;
            Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
            Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;
            Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
            Parameters.ParamByName('@NumCorrCOInicial').Value       := null;
            Parameters.ParamByName('@NumCorrCOFinal').Value         := null;
            Parameters.ParamByName('@IDArchivo').Value              := null;
            try
              ExecProc;
              IDArchivo := Parameters.ParamByName('@IDArchivo').Value;
              // Si no pudo insertar el archivo, error.
              if IDArchivo = -1 then
              begin
                  Result := False;
                  EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + NombreArchivo, '');
              end;

            except
              on e:exception do
              begin
                Result := False;
                EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');
              end;
            end;
          end;
        end;
        //END : SS_1006_NDR_20130228------------------------------------------------------------------------------------



        while Result and (NumLinea < ContenidoArchivo.Count) do begin
        	Linea := ContenidoArchivo[NumLinea];
			//if not ImportarLineaSaldoInicial(NombreArchivo, Linea, CodigoConcesionaria, NumLinea + 1, MensajeErrorLinea) then begin				//SS_1147Z_MCA_20121229
            if not ImportarLineaSaldoInicial(NombreArchivo, Linea, CodigoConcesionaria, NumLinea + 1, IDArchivo, MensajeErrorLinea) then begin		//SS_1147Z_MCA_20121229
                EventLogReportEvent(elError, Format(MSG_ERROR_INSERTANDO, [NumLinea + 1, MensajeErrorLinea]), '');
                Result := False;
            end;
            Inc(NumLinea);
        end;

        //BEGIN : SS_1006_NDR_20130228------------------------------------------------------------------------------------
        ////finalmente grabar el archivo procesado
        //if Result then begin
	      //  with spAgregarArchivo do begin
	      //      Parameters.Refresh;
	      //      Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
	      //      Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;
	      //      Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
        //      Parameters.ParamByName('@NumCorrCOInicial').Value       := null;
        //      Parameters.ParamByName('@NumCorrCOFinal').Value         := null;
        //      try
        //      	ExecProc;
        //      except on e:exception do begin
        //          	Result := False;
        //              EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');
        //              MoverArchivoCambiarNombre(FCarpetaEntradaSaldosIni, FCarpetaRechazadosSaldosIni, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120514
        //      	end;
        //      end;
    	  //  end;
        //end;
        //END : SS_1006_NDR_20130228------------------------------------------------------------------------------------

        if Result then DMConnections.BaseCAC.Execute('COMMIT TRAN spInsertarSaldoInicial')
        else DMConnections.BaseCAC.Execute('ROLLBACK TRAN spInsertarSaldoInicial');

    end;

    if ArchivoYaFueProcesado then begin                  //enviar correo
    	try
        	Mensaje								            := TIdMessage.Create(Self);
        	Mensaje.Subject						        := EMAIL_TEMA;
          Mensaje.Body.Text					        := Format(EMAIL_BODY, [NombreArchivo]);
          Mensaje.From.Address				      := FEmailFrom;
          Mensaje.Recipients.EMailAddresses	:= FEmailTo;
          IdSMTPEnviarCorreo.Host				    := FEmailServerSMTP;
          IdSMTPEnviarCorreo.Connect;
          IdSMTPEnviarCorreo.Send(Mensaje);
          //mover a la carpeta duplicados
          MoverARchivo(FCarpetaEntradaSaldosIni, FCarpetaDuplicadosSaldosIni, NombreArchivo, True);

        finally
            IdSMTPEnviarCorreo.Disconnect();
            Mensaje.Free;
        end;
    end;
    // Inicio Bloque SS_1015_CQU_20150514
    if (LineaError.Count > 0) then begin
        LineaError.SaveToFile(FCarpetaRechazadosSaldosIni + NuevoNombreArchivo);
        LineaError.Free;
    end;
    // Fin Bloque SS_1015_CQU_20150514
end;

{---------------------------------------------------------------------------------
        	GrabarArchivoCuotas
 Author			: Nelson Droguett Sierra
 Date			  : 10-Mayo-2012
 Description: 1.-	Determina si el archivo ya fue procesado
              2.-	Si no fue procesado, lo graba en la tabla
                	CuotasOtrasConcesionarias
              3.-	Si ya fue procesado, entonces lo graba en archivos
                  Duplicados
              SS-1015-NDR-20120510
----------------------------------------------------------------------------------}
function TMainForm.GrabarArchivoCuotas;
resourcestring
    SQL_ARCHIVO_EXISTE		= 'SELECT NombreArchivo FROM ArchivosProcesadosOtrasConcesionarias (NOLOCK) ' +
                        	  'WHERE NombreArchivo =  ''%s'' ';
    SQL_COD_CONCESIONARIA   = 'SELECT CodigoConcesionaria FROM Concesionarias (NOLOCK) WHERE CodigoConcesionaria = %s ';

    MSG_ERROR_INSERTANDO	= 'Error al insertar registro (L�nea %d) en tabla CuotasOtrasConcesionarias: %s';
    MSG_NOM_ARCHIVO_MALO    = 'El nombre del archivo no cumple formato esperado:  %s';
    MSG_NO_EXISTE_CONCES    = 'No existe concesionaria de c�digo : %s';

    //Mensajes del correo
    EMAIL_TEMA				= 'Importar Archivo Cuotas Otras Concesionarias: Se intenta re-procesar archivo';
    EMAIL_BODY				= 'Archivo : %s';

const
    Separador = '_';         //separador del nombre del archivo

var
	  Linea, PrefijoArchivo : string;
    NumLinea, CodigoConcesionaria : integer;
    FechaProceso : TDateTime;
    ArchivoYaFueProcesado : boolean;
    MensajeErrorLinea : string;

    //datos para el correo
    Mensaje : TIdMessage;
    NuevoNombreArchivo : string; // SS_1015_CQU_20120514
    IDArchivo : integer;            //SS_1006_NDR_20130228
begin

   	aqryArchivo.Close;
    spAgregarArchivo.Close;
    spAgregarRegRechOtrasConcesio.Close;
    spAgregarCuotaOtraConcesionaria.Close;
    spAgregarCuotaOtraConcesionaria.Close;
    spAgregarCuotaRechazadaOtraConcesionaria.Close;
    spObtenerConcesionarias.Close;

    aqryArchivo.Connection			                        := DMConnections.BaseCAC;
    spAgregarRegRechOtrasConcesio.Connection            := DMConnections.BaseCAC;
    spAgregarArchivo.Connection		                      := DMConnections.BaseCAC;
    spAgregarCuotaOtraConcesionaria.Connection		      := DMConnections.BaseCAC;
    spAgregarCuotaRechazadaOtraConcesionaria.Connection := DMConnections.BaseCAC;
    spObtenerConcesionarias.Connection                  := DMConnections.BaseCAC;

    Result := True;
    ArchivoYaFueProcesado := False;

    //determinar el tipo de archivo que va a procesar
    PrefijoArchivo              := GetItem(NombreArchivo,1, Separador);
    FCodigoConcesionariaOrigen  := GetItem(NombreArchivo,2, Separador);
    FCodigoConcesionariaDestino := GetItem(NombreArchivo,3, Separador);
    FNomArchFecha               := GetItem(NombreArchivo,4, Separador);
    FNomArchHora                := Copy(FNomArchFecha, 9, 6);           //hora
    FNomArchFecha               := Copy(FNomArchFecha,1,8);             //fecha

    // Inicio Bloque SS_1015_CQU_20120530
    {
    // Genero el nombre del archivo enc aso de ser rechazado SS_1015_CQU_20120514
    NuevoNombreArchivo := FNombreRechazadosCuotas
    + Separador + '01'
    + Separador + FCodigoConcesionariaOrigen
    + Separador + TIPO_CUOT
    + Separador + FormatDateTime ('yyyyMMddhhnnss', Date + Time)
    + '.txt';
    }
    NuevoNombreArchivo := GenerarNuevoNombre(NombreArchivo, FNombreRechazadosCuotas);
    // Fin Bloque SS_1015_CQU_20120530
    // El prefijo no viene en el archivo por lo cual se coloca SS_1015_CQU_20120514
    //  if(FNomArchTipoTrx = '') then FNomArchTipoTrx := TIPO_CUOT; // SS_1015_CQU_20120517
    FNomArchTipoTrx := TIPO_CUOT;                                   // SS_1015_CQU_20120517

    if  (PrefijoArchivo <> FNomArchParametroCuotas) or (FCodigoConcesionariaOrigen = '') or
        (FNomArchFecha = '') or (FNomArchHora = '') then
    begin
        EventLogReportEvent(elError, Format(MSG_NOM_ARCHIVO_MALO, [NombreArchivo]), '');
        MoverArchivoCambiarNombre(FCarpetaEntradaCuotas , FCarpetaRechazadosCuotas, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120514
        Result := False;
    end;

    CodigoConcesionaria := 0;
    if Result then begin
        CodigoConcesionaria := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_COD_CONCESIONARIA,[FCodigoConcesionariaOrigen]) );
        if CodigoConcesionaria = 0 then begin
            EventLogReportEvent(elError, Format(MSG_NO_EXISTE_CONCES, [FCodigoConcesionariaOrigen]), '');
            MoverArchivoCambiarNombre(FCarpetaEntradaCuotas , FCarpetaRechazadosCuotas, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120514
            Result := False;
        end;
    end;

    if Result then begin
        FechaProceso		:= StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()'));

        //validar que el archivo no ha sido procesado
        with aqryArchivo do begin
            Parameters.Clear;
            SQL.Clear;
            SQL.Text := Format(SQL_ARCHIVO_EXISTE, [NombreArchivo]);
            Open;
          	Result := IsEmpty;
            ArchivoYaFueProcesado := not Result;
        end;
        aqryArchivo.Close;
    end;

    //Importar el archivo
    if Result then begin       //Procesar las Lineas
        NumLinea := 0;
        DMConnections.BaseCAC.Execute('BEGIN TRAN spInsertarCuotas');


        //BEGIN : SS_1006_NDR_20130228------------------------------------------------------------------------
        //grabar el archivo a procesar
        if Result then begin
	        with spAgregarArchivo do begin
            Parameters.Refresh;
            Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
            Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;
            Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
            Parameters.ParamByName('@NumCorrCOInicial').Value       := null;
            Parameters.ParamByName('@NumCorrCOFinal').Value         := null;
            Parameters.ParamByName('@IDArchivo').Value              := null;
            try
              ExecProc;
              IDArchivo := Parameters.ParamByName('@IDArchivo').Value;
              // Si no pudo insertar el archivo, error.
              if IDArchivo = -1 then
              begin
                  Result := False;
                  EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + NombreArchivo, '');
              end;

            except
              on e:exception do
              begin
                Result := False;
                EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');
              end;
            end;
    	    end;
        end;
        //END : SS_1006_NDR_20130228------------------------------------------------------------------------


        while Result and (NumLinea < ContenidoArchivo.Count) do begin
        	Linea := ContenidoArchivo[NumLinea];
            if not ImportarLineaCuotas(NombreArchivo, Linea, CodigoConcesionaria, NumLinea + 1, MensajeErrorLinea) then begin
                EventLogReportEvent(elError, Format(MSG_ERROR_INSERTANDO, [NumLinea + 1, MensajeErrorLinea]), '');
                Result := False;
            end;

            Inc(NumLinea);
        end;

        //BEGIN : SS_1006_NDR_20130228------------------------------------------------------------------------
        ////finalmente grabar el archivo procesado
        //if Result then begin
	      //  with spAgregarArchivo do begin
	      //      Parameters.Refresh;
	      //      Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;
	      //      Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;
	      //      Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;
        //      Parameters.ParamByName('@NumCorrCOInicial').Value       := null;
        //      Parameters.ParamByName('@NumCorrCOFinal').Value         := null;
        //      try
        //      	ExecProc;
        //      except on e:exception do begin
        //          	Result := False;
        //              EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');
        //              MoverArchivoCambiarNombre(FCarpetaEntradaCuotas , FCarpetaRechazadosCuotas, NombreArchivo, NuevoNombreArchivo, True); // SS_1015_CQU_20120514
        //      	end;
        //      end;
    	  //  end;
        //end;
        //END : SS_1006_NDR_20130228------------------------------------------------------------------------

        if Result then DMConnections.BaseCAC.Execute('COMMIT TRAN spInsertarCuotas')
        else DMConnections.BaseCAC.Execute('ROLLBACK TRAN spInsertarCuotas');

    end;

    if ArchivoYaFueProcesado then begin                  //enviar correo
    	try
        	Mensaje								            := TIdMessage.Create(Self);
        	Mensaje.Subject						        := EMAIL_TEMA;
          Mensaje.Body.Text					        := Format(EMAIL_BODY, [NombreArchivo]);
          Mensaje.From.Address				      := FEmailFrom;
          Mensaje.Recipients.EMailAddresses	:= FEmailTo;
          IdSMTPEnviarCorreo.Host				    := FEmailServerSMTP;
          IdSMTPEnviarCorreo.Connect;
          IdSMTPEnviarCorreo.Send(Mensaje);
          //mover a la carpeta duplicados
          MoverARchivo(FCarpetaEntradaCuotas, FCarpetaDuplicadosCuotas, NombreArchivo, True);

        finally
            IdSMTPEnviarCorreo.Disconnect();
            Mensaje.Free;
        end;
    end;
    // Inicio Bloque SS_1015_CQU_20150514
    if (LineaError.Count > 0) then begin
        LineaError.SaveToFile(FCarpetaRechazadosCuotas + NuevoNombreArchivo);
        LineaError.Free;
    end;
    // Fin Bloque SS_1015_CQU_20150514
end;


{---------------------------------------------------------------------------------
        	ImportarArchivosCAC

 Author			: mbecerra
 Date			: 09-Agosto-2010
 Description	: Lee todos los archivos y los importa al CAC
----------------------------------------------------------------------------------}
procedure TMainForm.ImportarArchivosCAC;
resourcestring
	MSG_ERROR_IMPORTANDO = 'Ocurri� un error al importar archivos.' + CRLF + '%s';
var
	SRegistro : TSearchRec;
    Exito : word;
    NombreArchivo : string;
    ContenidoArchivo : TStringList;
begin
	ContenidoArchivo := TStringList.Create;
    Exito := FindFirst(FCarpetaEntrada + '*.txt', faAnyFile, SRegistro); //cualquier archivo TXT
    try
    	while Exito = 0 do  begin
    		NombreArchivo := SRegistro.Name;
        	if Pos(FNomArchParametro, NombreArchivo) > 0 then begin   //es un archivo que cumple el criterio?
        		ContenidoArchivo.LoadFromFile(FCarpetaEntrada + NombreArchivo);
                LineaError := TStringList.Create; //  SS_1015_CQU_20120510
                if GrabarArchivoEnCAC(NombreArchivo, ContenidoArchivo) then begin
                	RenameFile(FCarpetaEntrada + NombreArchivo, FCarpetaProcesados + NombreArchivo);
                end;
        	end;

    		Exito := FindNext(SRegistro);
  		end;

    except on e:exception do begin
        	EventLogReportEvent(elError, Format(MSG_ERROR_IMPORTANDO, [e.Message]), '');
    	end;
    end;

  FindClose(SRegistro);
  ContenidoArchivo.Free;
end;

{---------------------------------------------------------------------------------
        	ImportarArchivoMovCtas

 Author			: mbecerra
 Date			: 09-Febrero-2012
 Description	: Lee todos los archivos de movimientos cuentas de otras
                    concesionarias, y los importa al CAC

                SS_1015_MBE_20120206
----------------------------------------------------------------------------------}
procedure TMainForm.ImportarArchivoMovCtas;
resourcestring
	MSG_ERROR_IMPORTANDO = 'Ocurri� un error al importar archivos.' + CRLF + '%s';
var
	SRegistro : TSearchRec;
    Exito : word;
    NombreArchivo : string;
    ContenidoArchivo : TStringList;
begin
	ContenidoArchivo := TStringList.Create;
    Exito := FindFirst(FCarpetaEntradaMovCta + '*.txt', faAnyFile, SRegistro); //cualquier archivo TXT
    try
    	while Exito = 0 do  begin
    		NombreArchivo := SRegistro.Name;
        	if Pos(FNomArchParametroMovCta, NombreArchivo) > 0 then begin   //es un archivo que cumple el criterio?
        		ContenidoArchivo.LoadFromFile(FCarpetaEntradaMovCta + NombreArchivo);
                LineaError := TStringList.Create; //  SS_1015_CQU_20120510
                if GrabarArchivoMovCta(NombreArchivo, ContenidoArchivo) then begin
                	RenameFile(FCarpetaEntradaMovCta + NombreArchivo, FCarpetaProcesadosMovCta + NombreArchivo);
                end;
        	end;

    		Exito := FindNext(SRegistro);
  		end;

    except on e:exception do begin
        	EventLogReportEvent(elError, Format(MSG_ERROR_IMPORTANDO, [e.Message]), '');
    	end;
    end;

  FindClose(SRegistro);
  ContenidoArchivo.Free;
end;

{---------------------------------------------------------------------------------
        	ImportarSaldosInicialesOtrasConcesionarias
 Author			  : Nelson Droguet Sierra
 Date			    : 10-Mayo-2012
 Description	: Lee los archivos de Saldos Iniciales e importa su contenido
               SS-1015-NDR-20120510
----------------------------------------------------------------------------------}
procedure TMainForm.ImportarSaldosInicialesOtrasConcesionarias;
resourcestring
	MSG_ERROR_IMPORTANDO = 'Ocurri� un error al importar archivos.' + CRLF + '%s';
var
	SRegistro : TSearchRec;
    Exito : word;
    NombreArchivo : string;
    ContenidoArchivo : TStringList;
begin
	ContenidoArchivo := TStringList.Create;
  Exito := FindFirst(FCarpetaEntradaSaldosIni + '*.txt', faAnyFile, SRegistro); //cualquier archivo TXT
  try
  	while Exito = 0 do  begin
  		NombreArchivo := SRegistro.Name;
      if Pos(FNomArchParametroSaldoIni, NombreArchivo) > 0 then begin   //es un archivo que cumple el criterio?
      	ContenidoArchivo.LoadFromFile(FCarpetaEntradaSaldosIni + NombreArchivo);
        LineaError := TStringList.Create; //  SS_1015_CQU_20120514
        if GrabarArchivoSaldoInicial(NombreArchivo, ContenidoArchivo) then begin
        	RenameFile(FCarpetaEntradaSaldosIni + NombreArchivo, FCarpetaProcesadosSaldosIni + NombreArchivo);
        end;
      end;

  		Exito := FindNext(SRegistro);
  	end;

  except on e:exception do begin
      	EventLogReportEvent(elError, Format(MSG_ERROR_IMPORTANDO, [e.Message]), '');
  	end;
  end;

  FindClose(SRegistro);
  ContenidoArchivo.Free;
end;

{---------------------------------------------------------------------------------
        	ImportarCuotasOtrasConcesionarias
 Author			  : Nelson Droguet Sierra
 Date			    : 10-Mayo-2012
 Description	: Lee los archivos de Cuotas e importa su contenido
               SS-1015-NDR-20120510
----------------------------------------------------------------------------------}
procedure TMainForm.ImportarCuotasOtrasConcesionarias;
resourcestring
	MSG_ERROR_IMPORTANDO = 'Ocurri� un error al importar archivos.' + CRLF + '%s';
var
	SRegistro : TSearchRec;
    Exito : word;
    NombreArchivo : string;
    ContenidoArchivo : TStringList;
begin
	ContenidoArchivo := TStringList.Create;
  Exito := FindFirst(FCarpetaEntradaCuotas + '*.txt', faAnyFile, SRegistro); //cualquier archivo TXT
  try
  	while Exito = 0 do  begin
  		NombreArchivo := SRegistro.Name;
      if Pos(FNomArchParametroCuotas, NombreArchivo) > 0 then begin   //es un archivo que cumple el criterio?
      	ContenidoArchivo.LoadFromFile(FCarpetaEntradaCuotas + NombreArchivo);
        LineaError := TStringList.Create; //  SS_1015_CQU_20120514
        if GrabarArchivoCuotas(NombreArchivo, ContenidoArchivo) then begin
        	RenameFile(FCarpetaEntradaCuotas + NombreArchivo, FCarpetaProcesadosCuotas + NombreArchivo);
        end;
      end;

  		Exito := FindNext(SRegistro);
  	end;
  except on e:exception do begin
      	EventLogReportEvent(elError, Format(MSG_ERROR_IMPORTANDO, [e.Message]), '');
  	end;
  end;
  FindClose(SRegistro);
  ContenidoArchivo.Free;
end;

{---------------------------------------------------------------------------------
        	tmerImportarTimer

 Author			: mbecerra
 Date			: 09-Agosto-2010
 Description	: Determina si debe procesar los archivos de Transacciones
----------------------------------------------------------------------------------}
procedure TMainForm.tmerImportarTimer(Sender: TObject);
begin
    tmerImportar.Enabled := False;

    try
    	//ver si debe importar los archivos
    	Inc(FContadorMinutos);
    	if FContadorMinutos >= FIntervaloMinutos then begin
            FContadorMinutos := 0;
    		if ConectarBase() then begin
        		if LeerParametrosGenerales() then begin
                    ImportarArchivosCAC();
                    ImportarArchivoMovCtas();                                 //SS_1015_MBE_20120206
                    //ImportarArchivosFolios();                               //SS-1015-NDR-20120510 PAR0133-NDR-20110326
                    //ImportarArchivosPagos();                                //SS-1015-NDR-20120510 PAR0133-NDR-20110326
                    ImportarSaldosInicialesOtrasConcesionarias();             //SS-1015-NDR-20120510
                    GenerarComprobanteSaldosInicialesOtrasConcesionarias();   //SS-1015-NDR-20120510
                    ImportarCuotasOtrasConcesionarias();                      //SS-1015-NDR-20120510
                    GenerarComprobanteCuotasOtrasConcesionarias();            //SS-1015-NDR-20120510
                end;
            end;
        end;

    finally
        tmerImportar.Interval := 60000;			//cada un minuto
		tmerImportar.Enabled := True;
    end;
end;








{-----------------------------------------------------------------------------------------------------------------------------------------------------------
 -----------------------------------------------------------------------------------------------------------------------------------------------------------

 CODIGO AGREGADO

 -----------------------------------------------------------------------------------------------------------------------------------------------------------
 -----------------------------------------------------------------------------------------------------------------------------------------------------------
}
//{---------------------------------------------------------------------------------
//        	ImportarLineaFolio
//
// Author			: Nelson Droguett Sierra
// Date			: 26-Marzo-2010
// Description	:
//                    Importa la l�nea en la tabla TransitosEnviadosAOtrasConcesionarias
//
//----------------------------------------------------------------------------------}
//function TMainForm.ImportarLineaFolio;                                                                                              //PAR0133-NDR-20110326
//resourcestring                                                                                                                      //PAR0133-NDR-20110326
//    MSG_ERROR_CAMPO_NUM     = 'El campo %s no es num�rico';                                                                         //PAR0133-NDR-20110326
//    MSG_ERROR_CAMPO_STR     = 'Falta el campo %s ';                                                                                 //PAR0133-NDR-20110326
//    MSG_ERROR_TIPO_TRX      = 'Tipo de transacci�n no conocida %s';                                                                 //PAR0133-NDR-20110326
//    MSG_ERROR_TRX_LINT      = 'Tipo Transacci�n %s es distinta a la indicada en el archivo %s';                                     //PAR0133-NDR-20110326
//    MSG_ERROR_NO_EXISTE     = 'Error al actualizar, el tr�nsito Numero = %s no existe, para la concesionaria %s';                   //PAR0133-NDR-20110326
//    MSG_ERROR_INSERT        = 'Error al actualizar la transacci�n Numero = %s, para la concesionaria %s: %s';                       //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    CAMPO_IDETTO    = 'Indentificador Tr�nsito';                                                                                    //PAR0133-NDR-20110326
//    CAMPO_NUMFOL    = 'N�mero de Folio';                                                                                            //PAR0133-NDR-20110326
//    CAMPO_TIPDOC    = 'Tipo de Documento';                                                                                          //PAR0133-NDR-20110326
//    CAMPO_FECEMI    = 'Fecha de Emisi�n';                                                                                           //PAR0133-NDR-20110326
//    CAMPO_FECVEN    = 'Fecha de Vencimiento';                                                                                       //PAR0133-NDR-20110326
//const                                                                                                                               //PAR0133-NDR-20110326
//    separador = ';';                                                                                                                //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//var                                                                                                                                 //PAR0133-NDR-20110326
//    //datos de la l�nea:                                                                                                            //PAR0133-NDR-20110326
//    LinIdTransito,                                                                                                                  //PAR0133-NDR-20110326
//    LinNumeroFolio,                                                                                                                 //PAR0133-NDR-20110326
//    LinTipoDocumento,                                                                                                               //PAR0133-NDR-20110326
//    LinFechaEmision,                                                                                                                //PAR0133-NDR-20110326
//    LinFechaVencimiento : string;                                                                                                   //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    CodigoRetorno : integer;                                                                                                        //PAR0133-NDR-20110326
//begin                                                                                                                               //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    //parsear la l�nea:                                                                                                             //PAR0133-NDR-20110326
//    LinIdTransito       := GetItem(Linea, 1, separador);                                                                            //PAR0133-NDR-20110326
//    LinNumeroFolio      := GetItem(Linea, 2, separador);                                                                            //PAR0133-NDR-20110326
//    LinTipoDocumento    := GetItem(Linea, 3, separador);                                                                            //PAR0133-NDR-20110326
//    LinFechaEmision     := GetItem(Linea, 4, separador);                                                                            //PAR0133-NDR-20110326
//    LinFechaVencimiento := GetItem(Linea, 5, separador);                                                                            //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    //validar que:                                                                                                                  //PAR0133-NDR-20110326
//    // no falte ning�n campo obligatorio                                                                                            //PAR0133-NDR-20110326
//    // sean num�ricos los campos que deben serlo                                                                                    //PAR0133-NDR-20110326
//    //                                                                                                                              //PAR0133-NDR-20110326
//    MensajeError := '';                                                                                                             //PAR0133-NDR-20110326
//    if      not EsNumero(LinIdTransito)         then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_IDETTO])                    //PAR0133-NDR-20110326
//    else if not EsNumero(LinNumeroFolio)        then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_NUMFOL])                    //PAR0133-NDR-20110326
//    else if not EsNumero(LinFechaEmision)       then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECEMI])                    //PAR0133-NDR-20110326
//    else if not EsNumero(LinFechaVencimiento)   then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECVEN]);                   //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    // si hay error de parseo, entonces registrarlo                                                                                 //PAR0133-NDR-20110326
//    // y retornar verdadero para que contin�e con la siguiente l�nea                                                                //PAR0133-NDR-20110326
//    if MensajeError <> '' then begin                                                                                                //PAR0133-NDR-20110326
//        Result := RegistrarRegistroRechazado(NombreArchivo, MensajeError, LinNumeroFolio, NumLinea);                                                //PAR0133-NDR-20110326
//        Exit;                                                                                                                       //PAR0133-NDR-20110326
//    end;                                                                                                                            //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    with spAgregarFolioOtraConcesionaria do begin                                                                                   //PAR0133-NDR-20110326
//        Parameters.Refresh;                                                                                                         //PAR0133-NDR-20110326
//        Parameters.ParamByName('@NumCorrCA').Value              := LinIdTransito;                                                   //PAR0133-NDR-20110326
//        Parameters.ParamByName('@NumeroFolio').Value            := LinNumeroFolio;                                                  //PAR0133-NDR-20110326
//        Parameters.ParamByName('@TipoDocumento').Value          := LinTipoDocumento;                                                //PAR0133-NDR-20110326
//        Parameters.ParamByName('@FechaEmision').Value           := ConvierteAFecha(LinFechaEmision);                                //PAR0133-NDR-20110326
//        Parameters.ParamByName('@FechaVencimiento').Value       := ConvierteAFecha(LinFechaVencimiento);                            //PAR0133-NDR-20110326
//        Parameters.ParamByName('@ArchivoFolio').Value           := NombreArchivo;                                                   //PAR0133-NDR-20110326
//        Parameters.ParamByName('@UsuarioModificacion').Value    := 'SQL_JOB';                                                       //PAR0133-NDR-20110326
//        try                                                                                                                         //PAR0133-NDR-20110326
//            ExecProc;                                                                                                               //PAR0133-NDR-20110326
//            CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;                                                         //PAR0133-NDR-20110326
//            if CodigoRetorno = 0 then begin                                                                                         //PAR0133-NDR-20110326
//                Result := True;                                                                                                     //PAR0133-NDR-20110326
//                if FNumCorrCOMin = 0 then FNumCorrCOMin := StrToInt64(LinIdTransito);                                                 //PAR0133-NDR-20110326
//                if FNumCorrCoMax = 0 then FNumCorrCoMax := StrToInt64(LinIdTransito);                                                 //PAR0133-NDR-20110326
//                if StrToInt64(LinIdTransito) > FNumCorrCoMax then FNumCorrCoMax := StrToInt64(LinIdTransito);                           //PAR0133-NDR-20110326
//                if StrToInt64(LinIdTransito) < FNumCorrCoMin then FNumCorrCoMin := StrToInt64(LinIdTransito);                           //PAR0133-NDR-20110326
//            end                                                                                                                     //PAR0133-NDR-20110326
//            else begin                                                                                                              //PAR0133-NDR-20110326
//                EventLogReportEvent(elError, Format(MSG_ERROR_NO_EXISTE, [LinIdTransito, FCodigoConcesionariaOrigen ]), '');        //PAR0133-NDR-20110326
//                Result := RegistrarFolioRechazado(  StrToInt(LinIdTransito),                                                        //PAR0133-NDR-20110326
//                                                    CodigoConcesionaria,                                                            //PAR0133-NDR-20110326
//                                                    StrToInt(LinNumeroFolio),                                                       //PAR0133-NDR-20110326
//                                                    LinTipoDocumento,                                                               //PAR0133-NDR-20110326
//                                                    NombreArchivo,                                                                  //PAR0133-NDR-20110326
//                                                    Format(MSG_ERROR_NO_EXISTE, [LinIdTransito, FCodigoConcesionariaOrigen ]),      //PAR0133-NDR-20110326
//                                                    ConvierteAFecha(LinFechaEmision),                                               //PAR0133-NDR-20110326
//                                                    ConvierteAFecha(LinFechaVencimiento)                                            //PAR0133-NDR-20110326
//                                                 );                                                                                 //PAR0133-NDR-20110326
//                Exit;                                                                                                               //PAR0133-NDR-20110326
//            end;                                                                                                                    //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//        except on e:exception do begin                                                                                              //PAR0133-NDR-20110326
//                EventLogReportEvent(elError, Format(MSG_ERROR_INSERT, [LinIdTransito, FCodigoConcesionariaOrigen, e.Message]), ''); //PAR0133-NDR-20110326
//                Result := RegistrarFolioRechazado(  StrToInt(LinIdTransito),                                                         //PAR0133-NDR-20110326
//                                                    CodigoConcesionaria,                                                             //PAR0133-NDR-20110326
//                                                    StrToInt(LinNumeroFolio),                                                        //PAR0133-NDR-20110326
//                                                    LinTipoDocumento,                                                                //PAR0133-NDR-20110326
//                                                    NombreArchivo,                                                                   //PAR0133-NDR-20110326
//                                                    Format(MSG_ERROR_INSERT, [LinIdTransito, FCodigoConcesionariaOrigen, e.Message]),//PAR0133-NDR-20110326
//                                                    ConvierteAFecha(LinFechaEmision),                                                //PAR0133-NDR-20110326
//                                                    ConvierteAFecha(LinFechaVencimiento)                                             //PAR0133-NDR-20110326
//                                                 );                                                                                  //PAR0133-NDR-20110326
//            end;                                                                                                                    //PAR0133-NDR-20110326
//        end;                                                                                                                        //PAR0133-NDR-20110326
//    end; {with}                                                                                                                     //PAR0133-NDR-20110326
//end;                                                                                                                                //PAR0133-NDR-20110326

//{---------------------------------------------------------------------------------
//        	ImportarArchivosFolios
//
// Author			: Nelson Droguett Sierra
// Date			: 26-Marzo-2010
// Description	: Lee todos los archivos de Folios y los importa al CAC
//----------------------------------------------------------------------------------}
//procedure TMainForm.ImportarArchivosFolios;                                                                                 //PAR0133-NDR-20110326
//resourcestring                                                                                                              //PAR0133-NDR-20110326
//	MSG_ERROR_IMPORTANDO = 'Ocurri� un error al importar archivos de Folios.' + CRLF + '%s';                                //PAR0133-NDR-20110326
//var                                                                                                                         //PAR0133-NDR-20110326
//	SRegistro : TSearchRec;                                                                                                 //PAR0133-NDR-20110326
//    Exito : word;                                                                                                           //PAR0133-NDR-20110326
//    NombreArchivo : string;                                                                                                 //PAR0133-NDR-20110326
//    ContenidoArchivo : TStringList;                                                                                         //PAR0133-NDR-20110326
//begin                                                                                                                       //PAR0133-NDR-20110326
//	ContenidoArchivo := TStringList.Create;                                                                                 //PAR0133-NDR-20110326
//    Exito := FindFirst(FCarpetaEntradaFolios + '*.txt', faAnyFile, SRegistro); //cualquier archivo TXT                      //PAR0133-NDR-20110326
//    try                                                                                                                     //PAR0133-NDR-20110326
//    	while Exito = 0 do  begin                                                                                           //PAR0133-NDR-20110326
//    		NombreArchivo := SRegistro.Name;                                                                                //PAR0133-NDR-20110326
//        	if Pos(FNomArchParametroFolios, NombreArchivo) > 0 then begin   //es un archivo que cumple el criterio?         //PAR0133-NDR-20110326
//        		ContenidoArchivo.LoadFromFile(FCarpetaEntradaFolios + NombreArchivo);                                       //PAR0133-NDR-20110326
//                if GrabarArchivoFolios(NombreArchivo, ContenidoArchivo) then begin                                          //PAR0133-NDR-20110326
//                	RenameFile(FCarpetaEntradaFolios + NombreArchivo, FCarpetaProcesadosFolios + NombreArchivo);            //PAR0133-NDR-20110326
//                end;                                                                                                        //PAR0133-NDR-20110326
//        	end;                                                                                                            //PAR0133-NDR-20110326
//                                                                                                                            //PAR0133-NDR-20110326
//    		Exito := FindNext(SRegistro);                                                                                   //PAR0133-NDR-20110326
//  		end;                                                                                                                //PAR0133-NDR-20110326
//                                                                                                                            //PAR0133-NDR-20110326
//    except on e:exception do begin                                                                                          //PAR0133-NDR-20110326
//        	EventLogReportEvent(elError, Format(MSG_ERROR_IMPORTANDO, [e.Message]), '');                                    //PAR0133-NDR-20110326
//    	end;                                                                                                                //PAR0133-NDR-20110326
//    end;                                                                                                                    //PAR0133-NDR-20110326
//                                                                                                                            //PAR0133-NDR-20110326
//  FindClose(SRegistro);                                                                                                     //PAR0133-NDR-20110326
//  ContenidoArchivo.Free;                                                                                                    //PAR0133-NDR-20110326
//end;                                                                                                                        //PAR0133-NDR-20110326

//{---------------------------------------------------------------------------------
//        	GrabarArchivoFolios
//
// Author			: Nelson Droguett Sierra
// Date			: 26-Marzo-2011
// Description	:
// 					1.-	Determina si el archivo ya fue procesado
//                    2.-	Si no fue procesado, lo graba en la tabla
//                    	SeguimientoTransitosOtrasConcesionarias
//                    3.-	Si ya fue procesado, entonces env�a el correo
//
//----------------------------------------------------------------------------------}
//function TMainForm.GrabarArchivoFolios;                                                                                                                    //PAR0133-NDR-20110326
//resourcestring                                                                                                                                             //PAR0133-NDR-20110326
//    SQL_ARCHIVO_EXISTE		= 'SELECT NombreArchivo FROM ArchivosProcesadosOtrasConcesionarias (NOLOCK) ' +                                                //PAR0133-NDR-20110326
//                        	  'WHERE NombreArchivo =  ''%s'' ';                                                                                            //PAR0133-NDR-20110326
//    SQL_COD_CONCESIONARIA   = 'SELECT CodigoConcesionaria FROM Concesionarias (NOLOCK) WHERE CodigoConcesionaria = %s ';                                   //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    MSG_ERROR_INSERTANDO	= 'Error al actualizar registro (L�nea %d) en tabla TransitosEnviadosAOtrasConcesionarias: %s';                               //PAR0133-NDR-20110326
//    MSG_NOM_ARCHIVO_MALO    = 'El nombre del archivo no cumple formato esperado:  %s';                                                                     //PAR0133-NDR-20110326
//    MSG_NO_EXISTE_CONCES    = 'No existe concesionaria de c�digo : %s';                                                                                   //PAR0133-NDR-20110326
//    MSG_TRX_NO_CONOCIDA     = 'Transacci�n %s no conocida';                                                                                               //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    //Mensajes del correo                                                                                                                                  //PAR0133-NDR-20110326
//    EMAIL_TEMA				= 'Importar Archivo Folios Otras Concesionarias: Se intenta re-procesar archivo';                                              //PAR0133-NDR-20110326
//    EMAIL_BODY				= 'Archivo : %s';
//
//const
//	Separador = '_';
//                                                                                                         //PAR0133-NDR-20110326
//var                                                                                                                                                        //PAR0133-NDR-20110326
//	Linea, PrefijoArchivo  : string;                                                                                            //PAR0133-NDR-20110326
//    NumLinea, CodigoConcesionaria : integer;                                                                                                               //PAR0133-NDR-20110326
//    FechaProceso : TDateTime;                                                                                                                              //PAR0133-NDR-20110326
//    ArchivoYaFueProcesado : boolean;                                                                                                                       //PAR0133-NDR-20110326
//    MensajeErrorLinea : string;                                                                                                                            //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    //datos para el correo                                                                                                                                 //PAR0133-NDR-20110326
//    Mensaje : TIdMessage;                                                                                                                                  //PAR0133-NDR-20110326
//begin                                                                                                                                                      //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
// 	aqryArchivo.Close;                                                                                                                                     //PAR0133-NDR-20110326
//    spAgregarArchivo.Close;                                                                                                                                //PAR0133-NDR-20110326
//    spAgregarFolioOtraConcesionaria.Close;                                                                                                                 //PAR0133-NDR-20110326
//    spAgregarPagoOtraConcesionaria.Close;                                                                                                                  //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    aqryArchivo.Connection			            := DMConnections.BaseCAC;                                                                                  //PAR0133-NDR-20110326
//    spAgregarArchivo.Connection		            := DMConnections.BaseCAC;                                                                                  //PAR0133-NDR-20110326
//    spAgregarFolioOtraConcesionaria.Connection	:= DMConnections.BaseCAC;                                                                                  //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    Result := True;                                                                                                                                        //PAR0133-NDR-20110326
//    ArchivoYaFueProcesado := False;                                                                                                                        //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    //determinar el tipo de archivo que va a procesar                                                                                                      //PAR0133-NDR-20110326
//    PrefijoArchivo              := GetItem(NombreArchivo,1, Separador);                                                                                           //PAR0133-NDR-20110326
//    FCodigoConcesionariaOrigen  := GetItem(NombreArchivo,2, Separador);                                                                                           //PAR0133-NDR-20110326
//    FCodigoConcesionariaDestino := GetItem(NombreArchivo,3, Separador);                                                                                           //PAR0133-NDR-20110326
//    FNomArchFecha               := GetItem(NombreArchivo,4, Separador);                                                                                           //PAR0133-NDR-20110326
//    FNomArchHora                := Copy(FNomArchFecha, 9, 6);           //hora                                                                             //PAR0133-NDR-20110326
//    FNomArchFecha               := Copy(FNomArchFecha,1,8);             //fecha                                                                            //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    if  (PrefijoArchivo <> FNomArchParametroFolios) or                                                                                                     //PAR0133-NDR-20110326
//        (FCodigoConcesionariaOrigen = '') or                                                                                                               //PAR0133-NDR-20110326
//        (FNomArchFecha = '') or                                                                                                                            //PAR0133-NDR-20110326
//        (FNomArchHora = '') then                                                                                                                           //PAR0133-NDR-20110326
//    begin                                                                                                                                                  //PAR0133-NDR-20110326
//        EventLogReportEvent(elError, Format(MSG_NOM_ARCHIVO_MALO, [NombreArchivo]), '');                                                                   //PAR0133-NDR-20110326
//        Result := False;                                                                                                                                   //PAR0133-NDR-20110326
//    end;                                                                                                                                                   //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    CodigoConcesionaria := 0;                                                                                                                              //PAR0133-NDR-20110326
//    if Result then begin                                                                                                                                   //PAR0133-NDR-20110326
//        CodigoConcesionaria := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_COD_CONCESIONARIA,[FCodigoConcesionariaOrigen]) );                       //PAR0133-NDR-20110326
//        if CodigoConcesionaria = 0 then begin                                                                                                              //PAR0133-NDR-20110326
//            EventLogReportEvent(elError, Format(MSG_NO_EXISTE_CONCES, [FCodigoConcesionariaOrigen]), '');                                                  //PAR0133-NDR-20110326
//            Result := False;                                                                                                                               //PAR0133-NDR-20110326
//        end;                                                                                                                                               //PAR0133-NDR-20110326
//    end;                                                                                                                                                   //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    if Result then begin                                                                                                                                   //PAR0133-NDR-20110326
//        FechaProceso		:= StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()'));                                                     //PAR0133-NDR-20110326
//        //validar que el archivo no ha sido procesado                                                                                                      //PAR0133-NDR-20110326
//        with aqryArchivo do begin                                                                                                                          //PAR0133-NDR-20110326
//            Parameters.Clear;                                                                                                                              //PAR0133-NDR-20110326
//            SQL.Clear;                                                                                                                                     //PAR0133-NDR-20110326
//            SQL.Text := Format(SQL_ARCHIVO_EXISTE, [NombreArchivo]);                                                                                       //PAR0133-NDR-20110326
//            Open;                                                                                                                                          //PAR0133-NDR-20110326
//        	Result := IsEmpty;                                                                                                                             //PAR0133-NDR-20110326
//            ArchivoYaFueProcesado := not Result;                                                                                                           //PAR0133-NDR-20110326
//        end;                                                                                                                                               //PAR0133-NDR-20110326
//        aqryArchivo.Close;                                                                                                                                 //PAR0133-NDR-20110326
//    end;                                                                                                                                                   //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    //Importar el archivo                                                                                                                                  //PAR0133-NDR-20110326
//    if Result then begin       //Procesar las Lineas                                                                                                       //PAR0133-NDR-20110326
//        FNumCorrCOMin := 0;                                                                                                                                //PAR0133-NDR-20110326
//        FNumCorrCOMax := 0;                                                                                                                                //PAR0133-NDR-20110326
//        NumLinea := 0;                                                                                                                                     //PAR0133-NDR-20110326
//        DMConnections.BaseCAC.Execute('BEGIN TRAN spInsertarFoliosOtrasConc');                                                                             //PAR0133-NDR-20110326
//        while Result and (NumLinea < ContenidoArchivo.Count) do begin                                                                                      //PAR0133-NDR-20110326
//        	Linea := ContenidoArchivo[NumLinea];                                                                                                           //PAR0133-NDR-20110326
//            if not ImportarLineaFolio(NombreArchivo, Linea, CodigoConcesionaria, NumLinea + 1, MensajeErrorLinea) then begin                               //PAR0133-NDR-20110326
//                EventLogReportEvent(elError, Format(MSG_ERROR_INSERTANDO, [NumLinea + 1, MensajeErrorLinea]), '');                                         //PAR0133-NDR-20110326
//                Result := False;                                                                                                                           //PAR0133-NDR-20110326
//            end;                                                                                                                                           //PAR0133-NDR-20110326
//            Inc(NumLinea);                                                                                                                                 //PAR0133-NDR-20110326
//        end;                                                                                                                                               //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//        //finalmente grabar el archivo procesado                                                                                                           //PAR0133-NDR-20110326
//        if Result then begin                                                                                                                               //PAR0133-NDR-20110326
//	        with spAgregarArchivo do begin                                                                                                                 //PAR0133-NDR-20110326
//	            Parameters.Refresh;                                                                                                                        //PAR0133-NDR-20110326
//	            Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;                                                                  //PAR0133-NDR-20110326
//	            Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;                                                                   //PAR0133-NDR-20110326
//	            Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;                                                            //PAR0133-NDR-20110326
//                Parameters.ParamByName('@NumCorrCOInicial').Value       := FNumCorrCOMin;                                                                  //PAR0133-NDR-20110326
//                Parameters.ParamByName('@NumCorrCOFinal').Value         := FNumCorrCoMax;                                                                  //PAR0133-NDR-20110326
//                try                                                                                                                                        //PAR0133-NDR-20110326
//                	ExecProc;                                                                                                                              //PAR0133-NDR-20110326
//                except on e:exception do begin                                                                                                             //PAR0133-NDR-20110326
//                    	Result := False;                                                                                                                   //PAR0133-NDR-20110326
//                        EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');                                                                //PAR0133-NDR-20110326
//                	end;                                                                                                                                   //PAR0133-NDR-20110326
//                end;                                                                                                                                       //PAR0133-NDR-20110326
//    	    end;                                                                                                                                           //PAR0133-NDR-20110326
//        end;                                                                                                                                               //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//        if Result then DMConnections.BaseCAC.Execute('COMMIT TRAN spInsertarFoliosOtrasConc')                                                              //PAR0133-NDR-20110326
//        else DMConnections.BaseCAC.Execute('ROLLBACK TRAN spInsertarFoliosOtrasConc');                                                                     //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    end;                                                                                                                                                   //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//    if ArchivoYaFueProcesado then begin                  //enviar correo                                                                                   //PAR0133-NDR-20110326
//    	try                                                                                                                                                //PAR0133-NDR-20110326
//        	Mensaje								:= TIdMessage.Create(Self);                                                                                //PAR0133-NDR-20110326
//        	Mensaje.Subject						:= EMAIL_TEMA;                                                                                             //PAR0133-NDR-20110326
//            Mensaje.Body.Text					:= Format(EMAIL_BODY, [NombreArchivo]);                                                                    //PAR0133-NDR-20110326
//            Mensaje.From.Address				:= FEmailFrom;                                                                                             //PAR0133-NDR-20110326
//            Mensaje.Recipients.EMailAddresses	:= FEmailTo;                                                                                               //PAR0133-NDR-20110326
//            IdSMTPEnviarCorreo.Host				:= FEmailServerSMTP;                                                                                       //PAR0133-NDR-20110326
//            IdSMTPEnviarCorreo.Connect;                                                                                                                    //PAR0133-NDR-20110326
//            IdSMTPEnviarCorreo.Send(Mensaje);                                                                                                              //PAR0133-NDR-20110326
//                                                                                                                                                           //PAR0133-NDR-20110326
//            //mover a la carpeta procesados                                                                                                                //PAR0133-NDR-20110326
//            //RenameFile(FCarpetaEntradaFolios + NombreArchivo, FCarpetaDuplicadosFolios + NombreArchivo);                                                   //PAR0133-NDR-20110326
//            MoverARchivo(FCarpetaEntradaFolios, FCarpetaDuplicadosFolios, NombreArchivo, True); // SS_1015_CQU_20120423                                                                                                                                               //PAR0133-NDR-20110326
//        finally                                                                                                                                            //PAR0133-NDR-20110326
//            IdSMTPEnviarCorreo.Disconnect();                                                                                                               //PAR0133-NDR-20110326
//            Mensaje.Free;                                                                                                                                  //PAR0133-NDR-20110326
//        end;                                                                                                                                               //PAR0133-NDR-20110326
//    end;                                                                                                                                                   //PAR0133-NDR-20110326
//end;                                                                                                                                                       //PAR0133-NDR-20110326











//{---------------------------------------------------------------------------------
//        	ImportarLineaPago
//
// Author			: Nelson Droguett Sierra
// Date			: 26-MArzo-2010
// Description	:
//                    Importa la l�nea en la tabla TransitosEnviadosAOtrasConcesionarias
//
//----------------------------------------------------------------------------------}
//function TMainForm.ImportarLineaPago;                                                                                               //PAR0133-NDR-20110326
//resourcestring                                                                                                                      //PAR0133-NDR-20110326
//    MSG_ERROR_CAMPO_NUM     = 'El campo %s no es num�rico';                                                                         //PAR0133-NDR-20110326
//    MSG_ERROR_CAMPO_STR     = 'Falta el campo %s ';                                                                                 //PAR0133-NDR-20110326
//    MSG_ERROR_TIPO_TRX      = 'Tipo de transacci�n no conocida %s';                                                                 //PAR0133-NDR-20110326
//    MSG_ERROR_TRX_LINT      = 'Tipo Transacci�n %s es distinta a la indicada en el archivo %s';                                     //PAR0133-NDR-20110326
//    MSG_ERROR_NO_EXISTE     = 'Error al actualizar, el tr�nsito Numero = %s no existe, para la concesionaria %s';                   //PAR0133-NDR-20110326
//    MSG_ERROR_INSERT        = 'Error al actualizar la transacci�n Numero = %s, para la concesionaria %s: %s';                       //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    CAMPO_NUMFOL    = 'N�mero de Folio';                                                                                            //PAR0133-NDR-20110326
//    CAMPO_TIPDOC    = 'Tipo de Documento';                                                                                          //PAR0133-NDR-20110326
//    CAMPO_FECPAG    = 'Fecha de Pago';                                                                                              //PAR0133-NDR-20110326
//const                                                                                                                               //PAR0133-NDR-20110326
//    separador = ';';                                                                                                                //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//var                                                                                                                                 //PAR0133-NDR-20110326
//    //datos de la l�nea:                                                                                                            //PAR0133-NDR-20110326
//    LinNumeroFolio,                                                                                                                 //PAR0133-NDR-20110326
//    LinTipoDocumento,                                                                                                               //PAR0133-NDR-20110326
//    LinFechaPago : string;                                                                                                          //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    CodigoRetorno : integer;                                                                                                        //PAR0133-NDR-20110326
//begin                                                                                                                               //PAR0133-NDR-20110326
//    //parsear la l�nea:                                                                                                             //PAR0133-NDR-20110326
//    LinNumeroFolio      := GetItem(Linea, 1, separador);                                                                            //PAR0133-NDR-20110326
//    LinTipoDocumento    := GetItem(Linea, 2, separador);                                                                            //PAR0133-NDR-20110326
//    LinFechaPago        := GetItem(Linea, 3, separador);                                                                            //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    //validar que:                                                                                                                  //PAR0133-NDR-20110326
//    // no falte ning�n campo obligatorio                                                                                            //PAR0133-NDR-20110326
//    // sean num�ricos los campos que deben serlo                                                                                    //PAR0133-NDR-20110326
//    //                                                                                                                              //PAR0133-NDR-20110326
//    MensajeError := '';                                                                                                             //PAR0133-NDR-20110326
//    if      not EsNumero(LinNumeroFolio)        then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_NUMFOL])                    //PAR0133-NDR-20110326
//    else if not EsNumero(LinFechaPago)          then MensajeError := Format(MSG_ERROR_CAMPO_NUM, [CAMPO_FECPAG]);                   //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    // si hay error de parseo, entonces registrarlo                                                                                 //PAR0133-NDR-20110326
//    // y retornar verdadero para que contin�e con la siguiente l�nea                                                                //PAR0133-NDR-20110326
//    if MensajeError <> '' then begin                                                                                                //PAR0133-NDR-20110326
//        Result := RegistrarRegistroRechazado(NombreArchivo, MensajeError, LinNumeroFolio, NumLinea);                                                //PAR0133-NDR-20110326
//        Exit;                                                                                                                       //PAR0133-NDR-20110326
//    end;                                                                                                                            //PAR0133-NDR-20110326
//                                                                                                                                    //PAR0133-NDR-20110326
//    with spAgregarPagoOtraConcesionaria do begin                                                                                   //PAR0133-NDR-20110326
//        Parameters.Refresh;                                                                                                         //PAR0133-NDR-20110326
//        Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;                                             //PAR0133-NDR-20110326
//        Parameters.ParamByName('@NumeroFolio').Value            := LinNumeroFolio;                                                  //PAR0133-NDR-20110326
//        Parameters.ParamByName('@TipoDocumento').Value          := LinTipoDocumento;                                                //PAR0133-NDR-20110326
//        Parameters.ParamByName('@FechaPago').Value              := ConvierteAFecha(LinFechaPago);                                   //PAR0133-NDR-20110326
//        Parameters.ParamByName('@ArchivoPago').Value            := NombreArchivo;                                                   //PAR0133-NDR-20110326
//        Parameters.ParamByName('@UsuarioModificacion').Value    := 'SQL_JOB';                                                       //PAR0133-NDR-20110326
//        try                                                                                                                         //PAR0133-NDR-20110326
//            ExecProc;                                                                                                               //PAR0133-NDR-20110326
//            CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;                                                         //PAR0133-NDR-20110326
//            if CodigoRetorno = 0 then begin                                                                                         //PAR0133-NDR-20110326
//                Result := True;                                                                                                     //PAR0133-NDR-20110326
//            end                                                                                                                     //PAR0133-NDR-20110326
//            else begin                                                                                                              //PAR0133-NDR-20110326
//                EventLogReportEvent(elError,                                                                                        //PAR0133-NDR-20110326
//                                    Format(MSG_ERROR_NO_EXISTE,                                                                     //PAR0133-NDR-20110326
//                                           [LinTipoDocumento+':'+LinNumeroFolio,                                                    //PAR0133-NDR-20110326
//                                            FCodigoConcesionariaOrigen ]                                                            //PAR0133-NDR-20110326
//                                          ), '');                                                                                   //PAR0133-NDR-20110326
//                Result := RegistrarPagoRechazado(   CodigoConcesionaria,                                                            //PAR0133-NDR-20110326
//                                                    StrToInt(LinNumeroFolio),                                                       //PAR0133-NDR-20110326
//                                                    LinTipoDocumento,                                                               //PAR0133-NDR-20110326
//                                                    NombreArchivo,                                                                  //PAR0133-NDR-20110326
//                                                    Format(MSG_ERROR_NO_EXISTE,                                                     //PAR0133-NDR-20110326
//                                                            [   LinTipoDocumento+':'+LinNumeroFolio,                                //PAR0133-NDR-20110326
//                                                                FCodigoConcesionariaOrigen                                          //PAR0133-NDR-20110326
//                                                            ]                                                                       //PAR0133-NDR-20110326
//                                                           ),                                                                       //PAR0133-NDR-20110326
//                                                    ConvierteAFecha(LinFechaPago)                                                   //PAR0133-NDR-20110326
//                                                );                                                                                  //PAR0133-NDR-20110326
//            end;                                                                                                                    //PAR0133-NDR-20110326
//        except on e:exception do begin                                                                                              //PAR0133-NDR-20110326
//                EventLogReportEvent(elError,                                                                                        //PAR0133-NDR-20110326
//                                    Format(MSG_ERROR_INSERT,                                                                        //PAR0133-NDR-20110326
//                                            [LinTipoDocumento+':'+LinNumeroFolio,                                                   //PAR0133-NDR-20110326
//                                             FCodigoConcesionariaOrigen,                                                            //PAR0133-NDR-20110326
//                                             e.Message]                                                                             //PAR0133-NDR-20110326
//                                           ), '');                                                                                  //PAR0133-NDR-20110326
//                Result := RegistrarPagoRechazado(   CodigoConcesionaria,                                                            //PAR0133-NDR-20110326
//                                                    StrToInt(LinNumeroFolio),                                                       //PAR0133-NDR-20110326
//                                                    LinTipoDocumento,                                                               //PAR0133-NDR-20110326
//                                                    NombreArchivo,                                                                  //PAR0133-NDR-20110326
//                                                    Format(MSG_ERROR_INSERT,                                                        //PAR0133-NDR-20110326
//                                                            [LinTipoDocumento+':'+LinNumeroFolio,                                   //PAR0133-NDR-20110326
//                                                             FCodigoConcesionariaOrigen,                                            //PAR0133-NDR-20110326
//                                                             e.Message]                                                             //PAR0133-NDR-20110326
//                                                           ),                                                                       //PAR0133-NDR-20110326
//                                                    ConvierteAFecha(LinFechaPago)                                                   //PAR0133-NDR-20110326
//                                                );                                                                                  //PAR0133-NDR-20110326
//            end;                                                                                                                    //PAR0133-NDR-20110326
//        end;                                                                                                                        //PAR0133-NDR-20110326
//    end; {with}                                                                                                                     //PAR0133-NDR-20110326
//end;

//PAR0133-NDR-20110326

//{---------------------------------------------------------------------------------
//        	ImportarArchivosPagos
//
// Author			: Nelson Droguett Sierra
// Date			: 26-Marzo-2011
// Description	: Lee todos los archivos de Pagos y los importa al CAC
//----------------------------------------------------------------------------------}
//procedure TMainForm.ImportarArchivosPagos;                                                                                  //PAR0133-NDR-20110326
//resourcestring                                                                                                              //PAR0133-NDR-20110326
//	MSG_ERROR_IMPORTANDO = 'Ocurri� un error al importar archivos de Pagos.' + CRLF + '%s';                                 //PAR0133-NDR-20110326
//var                                                                                                                         //PAR0133-NDR-20110326
//	SRegistro : TSearchRec;                                                                                                 //PAR0133-NDR-20110326
//    Exito : word;                                                                                                           //PAR0133-NDR-20110326
//    NombreArchivo : string;                                                                                                 //PAR0133-NDR-20110326
//    ContenidoArchivo : TStringList;                                                                                         //PAR0133-NDR-20110326
//begin                                                                                                                       //PAR0133-NDR-20110326
//	ContenidoArchivo := TStringList.Create;                                                                                 //PAR0133-NDR-20110326
//    Exito := FindFirst(FCarpetaEntradaPagos + '*.txt', faAnyFile, SRegistro); //cualquier archivo TXT                       //PAR0133-NDR-20110326
//    try                                                                                                                     //PAR0133-NDR-20110326
//    	while Exito = 0 do  begin                                                                                           //PAR0133-NDR-20110326
//    		NombreArchivo := SRegistro.Name;                                                                                //PAR0133-NDR-20110326
//        	if Pos(FNomArchParametroPagos, NombreArchivo) > 0 then begin   //es un archivo que cumple el criterio?          //PAR0133-NDR-20110326
//        		ContenidoArchivo.LoadFromFile(FCarpetaEntradaPagos + NombreArchivo);                                        //PAR0133-NDR-20110326
//                if GrabarArchivoPagos(NombreArchivo, ContenidoArchivo) then begin                                           //PAR0133-NDR-20110326
//                	RenameFile(FCarpetaEntradaPagos + NombreArchivo, FCarpetaProcesadosPagos + NombreArchivo);              //PAR0133-NDR-20110326
//                end;                                                                                                        //PAR0133-NDR-20110326
//        	end;                                                                                                            //PAR0133-NDR-20110326
//                                                                                                                            //PAR0133-NDR-20110326
//    		Exito := FindNext(SRegistro);                                                                                   //PAR0133-NDR-20110326
//  		end;                                                                                                                //PAR0133-NDR-20110326
//                                                                                                                            //PAR0133-NDR-20110326
//    except on e:exception do begin                                                                                          //PAR0133-NDR-20110326
//        	EventLogReportEvent(elError, Format(MSG_ERROR_IMPORTANDO, [e.Message]), '');                                    //PAR0133-NDR-20110326
//    	end;                                                                                                                //PAR0133-NDR-20110326
//    end;                                                                                                                    //PAR0133-NDR-20110326
//                                                                                                                            //PAR0133-NDR-20110326
//  FindClose(SRegistro);                                                                                                     //PAR0133-NDR-20110326
//  ContenidoArchivo.Free;                                                                                                    //PAR0133-NDR-20110326
//end;

//PAR0133-NDR-20110326

//{---------------------------------------------------------------------------------
//        	GrabarArchivoPagos
//
// Author			: Nelson Droguett Sierra
// Date			: 26-Marzo-2011
// Description	:
// 					1.-	Determina si el archivo ya fue procesado
//                    2.-	Si no fue procesado, lo graba en la tabla
//                    	TransitosEnviadosAOtrasConcesionarias
//                    3.-	Si ya fue procesado, entonces env�a el correo
//
//----------------------------------------------------------------------------------}
//function TMainForm.GrabarArchivoPagos;                                                                                                                //PAR0133-NDR-20110326
//resourcestring                                                                                                                                        //PAR0133-NDR-20110326
//    SQL_ARCHIVO_EXISTE		= 'SELECT NombreArchivo FROM ArchivosProcesadosOtrasConcesionarias (NOLOCK) ' +                                           //PAR0133-NDR-20110326
//                        	  'WHERE NombreArchivo =  ''%s'' ';                                                                                       //PAR0133-NDR-20110326
//    SQL_COD_CONCESIONARIA   = 'SELECT CodigoConcesionaria FROM Concesionarias (NOLOCK) WHERE CodigoConcesionaria = %s ';                              //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    MSG_ERROR_INSERTANDO	= 'Error al actualizar registro (L�nea %d) en tabla TransitosEnviadosAOtrasConcesionarias: %s';                          //PAR0133-NDR-20110326
//    MSG_NOM_ARCHIVO_MALO    = 'El nombre del archivo no cumple formato esperado:  %s';                                                                //PAR0133-NDR-20110326
//    MSG_NO_EXISTE_CONCES    = 'No existe concesionaria de c�digo : %s';                                                                              //PAR0133-NDR-20110326
//    MSG_TRX_NO_CONOCIDA     = 'Transacci�n %s no conocida';                                                                                          //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    //Mensajes del correo                                                                                                                             //PAR0133-NDR-20110326
//    EMAIL_TEMA				= 'Importar Archivo Pagos Otras Concesionarias: Se intenta re-procesar archivo';                                         //PAR0133-NDR-20110326
//    EMAIL_BODY				= 'Archivo : %s';
//
//const
//	Separador = '_';
//                                                                                                          //PAR0133-NDR-20110326
//var                                                                                                                                                   //PAR0133-NDR-20110326
//	Linea, PrefijoArchivo  : string;                                                                                       //PAR0133-NDR-20110326
//    NumLinea, CodigoConcesionaria : integer;                                                                                                          //PAR0133-NDR-20110326
//    FechaProceso : TDateTime;                                                                                                                         //PAR0133-NDR-20110326
//    ArchivoYaFueProcesado : boolean;                                                                                                                  //PAR0133-NDR-20110326
//    MensajeErrorLinea : string;                                                                                                                       //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    //datos para el correo                                                                                                                            //PAR0133-NDR-20110326
//    Mensaje : TIdMessage;                                                                                                                             //PAR0133-NDR-20110326
//begin                                                                                                                                                 //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
// 	aqryArchivo.Close;                                                                                                                                //PAR0133-NDR-20110326
//    spAgregarArchivo.Close;                                                                                                                           //PAR0133-NDR-20110326
//    spAgregarPagoOtraConcesionaria.Close;                                                                                                             //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    aqryArchivo.Connection			            := DMConnections.BaseCAC;                                                                             //PAR0133-NDR-20110326
//    spAgregarArchivo.Connection		            := DMConnections.BaseCAC;                                                                             //PAR0133-NDR-20110326
//    spAgregarPagoOtraConcesionaria.Connection	:= DMConnections.BaseCAC;                                                                             //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    Result := True;                                                                                                                                   //PAR0133-NDR-20110326
//    ArchivoYaFueProcesado := False;                                                                                                                   //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    //determinar el tipo de archivo que va a procesar                                                                                                 //PAR0133-NDR-20110326
//    PrefijoArchivo              := GetItem(NombreArchivo,1, Separador);                                                                                      //PAR0133-NDR-20110326
//    FCodigoConcesionariaOrigen  := GetItem(NombreArchivo,2, Separador);                                                                                      //PAR0133-NDR-20110326
//    FCodigoConcesionariaDestino := GetItem(NombreArchivo,3, Separador);                                                                                      //PAR0133-NDR-20110326
//    FNomArchFecha               := GetItem(NombreArchivo,4, Separador);                                                                                      //PAR0133-NDR-20110326
//    FNomArchHora                := Copy(FNomArchFecha, 9, 6);           //hora                                                                        //PAR0133-NDR-20110326
//    FNomArchFecha               := Copy(FNomArchFecha,1,8);             //fecha                                                                       //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    if  (PrefijoArchivo <> FNomArchParametroPagos) or                                                                                                 //PAR0133-NDR-20110326
//        (FCodigoConcesionariaOrigen = '') or                                                                                                          //PAR0133-NDR-20110326
//        (FNomArchFecha = '') or                                                                                                                       //PAR0133-NDR-20110326
//        (FNomArchHora = '') then                                                                                                                      //PAR0133-NDR-20110326
//    begin                                                                                                                                             //PAR0133-NDR-20110326
//        EventLogReportEvent(elError, Format(MSG_NOM_ARCHIVO_MALO, [NombreArchivo]), '');                                                              //PAR0133-NDR-20110326
//        Result := False;                                                                                                                              //PAR0133-NDR-20110326
//    end;                                                                                                                                              //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    CodigoConcesionaria := 0;                                                                                                                         //PAR0133-NDR-20110326
//    if Result then begin                                                                                                                              //PAR0133-NDR-20110326
//        CodigoConcesionaria := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_COD_CONCESIONARIA,[FCodigoConcesionariaOrigen]) );                  //PAR0133-NDR-20110326
//        if CodigoConcesionaria = 0 then begin                                                                                                         //PAR0133-NDR-20110326
//            EventLogReportEvent(elError, Format(MSG_NO_EXISTE_CONCES, [FCodigoConcesionariaOrigen]), '');                                             //PAR0133-NDR-20110326
//            Result := False;                                                                                                                          //PAR0133-NDR-20110326
//        end;                                                                                                                                          //PAR0133-NDR-20110326
//    end;                                                                                                                                              //PAR0133-NDR-20110326
//                                                                                                                                                     //PAR0133-NDR-20110326
//    if Result then begin                                                                                                                              //PAR0133-NDR-20110326
//        FechaProceso		:= StrToDateTime(QueryGetValue(DMConnections.BaseCAC,'SELECT GETDATE()'));                                                //PAR0133-NDR-20110326
//        //validar que el archivo no ha sido procesado                                                                                                 //PAR0133-NDR-20110326
//        with aqryArchivo do begin                                                                                                                     //PAR0133-NDR-20110326
//            Parameters.Clear;                                                                                                                         //PAR0133-NDR-20110326
//            SQL.Clear;                                                                                                                                //PAR0133-NDR-20110326
//            SQL.Text := Format(SQL_ARCHIVO_EXISTE, [NombreArchivo]);                                                                                  //PAR0133-NDR-20110326
//            Open;                                                                                                                                     //PAR0133-NDR-20110326
//        	Result := IsEmpty;                                                                                                                        //PAR0133-NDR-20110326
//            ArchivoYaFueProcesado := not Result;                                                                                                      //PAR0133-NDR-20110326
//        end;                                                                                                                                          //PAR0133-NDR-20110326
//        aqryArchivo.Close;                                                                                                                            //PAR0133-NDR-20110326
//    end;                                                                                                                                              //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    //Importar el archivo                                                                                                                             //PAR0133-NDR-20110326
//    if Result then begin       //Procesar las Lineas                                                                                                  //PAR0133-NDR-20110326
//        FNumCorrCOMin := 0;                                                                                                                           //PAR0133-NDR-20110326
//        FNumCorrCOMax := 0;                                                                                                                           //PAR0133-NDR-20110326
//        NumLinea := 0;                                                                                                                                //PAR0133-NDR-20110326
//        DMConnections.BaseCAC.Execute('BEGIN TRAN spInsertarPagosOtrasConc');                                                                         //PAR0133-NDR-20110326
//        while Result and (NumLinea < ContenidoArchivo.Count) do begin                                                                                 //PAR0133-NDR-20110326
//        	Linea := ContenidoArchivo[NumLinea];                                                                                                      //PAR0133-NDR-20110326
//            if not ImportarLineaPago(NombreArchivo, Linea, CodigoConcesionaria, NumLinea + 1, MensajeErrorLinea) then begin                           //PAR0133-NDR-20110326
//                EventLogReportEvent(elError, Format(MSG_ERROR_INSERTANDO, [NumLinea + 1, MensajeErrorLinea]), '');                                    //PAR0133-NDR-20110326
//                Result := False;                                                                                                                      //PAR0133-NDR-20110326
//            end;                                                                                                                                      //PAR0133-NDR-20110326
//            Inc(NumLinea);                                                                                                                            //PAR0133-NDR-20110326
//        end;                                                                                                                                          //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//        //finalmente grabar el archivo procesado                                                                                                      //PAR0133-NDR-20110326
//        if Result then begin                                                                                                                          //PAR0133-NDR-20110326
//	        with spAgregarArchivo do begin                                                                                                            //PAR0133-NDR-20110326
//	            Parameters.Refresh;                                                                                                                   //PAR0133-NDR-20110326
//	            Parameters.ParamByName('@NombreArchivo').Value          := NombreArchivo;                                                             //PAR0133-NDR-20110326
//	            Parameters.ParamByName('@FechaHoraProceso').Value       := FechaProceso;                                                              //PAR0133-NDR-20110326
//	            Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;                                                       //PAR0133-NDR-20110326
//                Parameters.ParamByName('@NumCorrCOInicial').Value       := FNumCorrCOMin;                                                             //PAR0133-NDR-20110326
//                Parameters.ParamByName('@NumCorrCOFinal').Value         := FNumCorrCoMax;                                                             //PAR0133-NDR-20110326
//                try                                                                                                                                   //PAR0133-NDR-20110326
//                	ExecProc;                                                                                                                         //PAR0133-NDR-20110326
//                except on e:exception do begin                                                                                                        //PAR0133-NDR-20110326
//                    	Result := False;                                                                                                              //PAR0133-NDR-20110326
//                        EventLogReportEvent(elError, MSG_ERROR_INSERTANDO + e.Message, '');                                                           //PAR0133-NDR-20110326
//                	end;                                                                                                                              //PAR0133-NDR-20110326
//                end;                                                                                                                                  //PAR0133-NDR-20110326
//    	    end;                                                                                                                                      //PAR0133-NDR-20110326
//        end;                                                                                                                                          //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//        if Result then DMConnections.BaseCAC.Execute('COMMIT TRAN spInsertarPagosOtrasConc')                                                          //PAR0133-NDR-20110326
//        else DMConnections.BaseCAC.Execute('ROLLBACK TRAN spInsertarPagosOtrasConc');                                                                 //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    end;                                                                                                                                              //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//    if ArchivoYaFueProcesado then begin                  //enviar correo                                                                              //PAR0133-NDR-20110326
//    	try                                                                                                                                           //PAR0133-NDR-20110326
//        	Mensaje								:= TIdMessage.Create(Self);                                                                           //PAR0133-NDR-20110326
//        	Mensaje.Subject						:= EMAIL_TEMA;                                                                                        //PAR0133-NDR-20110326
//            Mensaje.Body.Text					:= Format(EMAIL_BODY, [NombreArchivo]);                                                               //PAR0133-NDR-20110326
//            Mensaje.From.Address				:= FEmailFrom;                                                                                        //PAR0133-NDR-20110326
//            Mensaje.Recipients.EMailAddresses	:= FEmailTo;                                                                                          //PAR0133-NDR-20110326
//            IdSMTPEnviarCorreo.Host				:= FEmailServerSMTP;                                                                                  //PAR0133-NDR-20110326
//            IdSMTPEnviarCorreo.Connect;                                                                                                               //PAR0133-NDR-20110326
//            IdSMTPEnviarCorreo.Send(Mensaje);                                                                                                         //PAR0133-NDR-20110326
//                                                                                                                                                      //PAR0133-NDR-20110326
//            //mover a la carpeta procesados                                                                                                           //PAR0133-NDR-20110326
//            //RenameFile(FCarpetaEntradaPagos + NombreArchivo, FCarpetaDuplicadosPagos + NombreArchivo);                                                //PAR0133-NDR-20110326
//            MoverArchivo(FCarpetaEntradaPagos, FCarpetaDuplicadosPagos, NombreArchivo, True); // SS_1015_CQU_20120423
//                                                                                                                                                      //PAR0133-NDR-20110326
//        finally                                                                                                                                       //PAR0133-NDR-20110326
//            IdSMTPEnviarCorreo.Disconnect();                                                                                                          //PAR0133-NDR-20110326
//            Mensaje.Free;                                                                                                                             //PAR0133-NDR-20110326
//        end;                                                                                                                                          //PAR0133-NDR-20110326
//    end;                                                                                                                                              //PAR0133-NDR-20110326
//end;                                                                                                                                                  //PAR0133-NDR-20110326

//{---------------------------------------------------------------------------------
//        	RegistrarFolioRechazado
//
// Author			: Nelson Droguett Sierra
// Date			: 28-Marzo-2011
// Description	:
//                    Deja registro del Folio rechazado.  Retorna false
//                    en caso de un error.
//
//----------------------------------------------------------------------------------}
//function TMainForm.RegistrarFolioRechazado(NumCorrCA, CodigoConcesionaria,                                                                            //PAR0133-NDR-20110326
//  NumeroFolio: Integer; TipoDocumento, ArchivoFolio, MotivoRechazo: string;                                                                           //PAR0133-NDR-20110326
//  FechaEmision, FechaVencimiento: TDateTime): Boolean;                                                                                                //PAR0133-NDR-20110326
//resourcestring                                                                                                                                        //PAR0133-NDR-20110326
//    MSG_MOTIVO  = 'El NumCorrCA : %s no fue encontrado como enviado para ser facturado en otra concesionaria';                                        //PAR0133-NDR-20110326
//begin                                                                                                                                                 //PAR0133-NDR-20110326
//    try                                                                                                                                               //PAR0133-NDR-20110326
//        with spAgregarFolioRechazadoOtraConcesionaria do                                                                                              //PAR0133-NDR-20110326
//        begin                                                                                                                                         //PAR0133-NDR-20110326
//            Close;                                                                                                                                    //PAR0133-NDR-20110326
//            Parameters.Refresh;                                                                                                                       //PAR0133-NDR-20110326
//            Parameters.ParamByName('@NumCorrCA').Value              := NumCorrCA;                                                                     //PAR0133-NDR-20110326
//            Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;                                                           //PAR0133-NDR-20110326
//            Parameters.ParamByName('@NumeroFolio').Value            := NumeroFolio;                                                                   //PAR0133-NDR-20110326
//            Parameters.ParamByName('@TipoDocumento').Value          := TipoDocumento;                                                                 //PAR0133-NDR-20110326
//            Parameters.ParamByName('@ArchivoFolio').Value           := ArchivoFolio;                                                                  //PAR0133-NDR-20110326
//            Parameters.ParamByName('@MotivoRechazo').Value          := Format(MSG_MOTIVO, [NumCorrCA]);                                               //PAR0133-NDR-20110326
//            Parameters.ParamByName('@FechaEmision').Value           := FechaEmision;                                                                  //PAR0133-NDR-20110326
//            Parameters.ParamByName('@FechaVencimiento').Value       := FechaVencimiento;                                                              //PAR0133-NDR-20110326
//            ExecProc;                                                                                                                                 //PAR0133-NDR-20110326
//            Result:=True;                                                                                                                             //PAR0133-NDR-20110326
//        end;                                                                                                                                          //PAR0133-NDR-20110326
//    finally                                                                                                                                           //PAR0133-NDR-20110326
//        spAgregarFolioRechazadoOtraConcesionaria.Close;                                                                                               //PAR0133-NDR-20110326
//    end;                                                                                                                                              //PAR0133-NDR-20110326
//end;                                                                                                                                                  //PAR0133-NDR-20110326

//{---------------------------------------------------------------------------------
//        	RegistrarPagoRechazado
//
// Author			: Nelson Droguett Sierra
// Date			: 28-Marzo-2011
// Description	:
//                    Deja registro del Pago rechazado.  Retorna false
//                    en caso de un error.
//
//----------------------------------------------------------------------------------}
//function TMainForm.RegistrarPagoRechazado(CodigoConcesionaria,                                                                                        //PAR0133-NDR-20110326
//  NumeroFolio: Integer; TipoDocumento, ArchivoPago, MotivoRechazo: string;                                                                            //PAR0133-NDR-20110326
//  FechaPago: TDateTime): Boolean;                                                                                                                     //PAR0133-NDR-20110326
//resourcestring                                                                                                                                        //PAR0133-NDR-20110326
//    MSG_MOTIVO  = 'El Documento : %s $s no fue encontrado como facturado en otra concesionaria';                                                      //PAR0133-NDR-20110326
//begin                                                                                                                                                 //PAR0133-NDR-20110326
//    try                                                                                                                                               //PAR0133-NDR-20110326
//        with spAgregarPagoRechazadoOtraConcesionaria do                                                                                               //PAR0133-NDR-20110326
//        begin                                                                                                                                         //PAR0133-NDR-20110326
//            Close;                                                                                                                                    //PAR0133-NDR-20110326
//            Parameters.Refresh;                                                                                                                       //PAR0133-NDR-20110326
//            Parameters.ParamByName('@CodigoConcesionaria').Value    := CodigoConcesionaria;                                                           //PAR0133-NDR-20110326
//            Parameters.ParamByName('@NumeroFolio').Value            := NumeroFolio;                                                                   //PAR0133-NDR-20110326
//            Parameters.ParamByName('@TipoDocumento').Value          := TipoDocumento;                                                                 //PAR0133-NDR-20110326
//            Parameters.ParamByName('@ArchivoPago').Value            := ArchivoPago;                                                                   //PAR0133-NDR-20110326
//            Parameters.ParamByName('@MotivoRechazo').Value          := Format(MSG_MOTIVO, [TipoDocumento,NumeroFolio]);                               //PAR0133-NDR-20110326
//            Parameters.ParamByName('@FechaPago').Value              := FechaPago;                                                                     //PAR0133-NDR-20110326
//            ExecProc;                                                                                                                                 //PAR0133-NDR-20110326
//            Result:=True;                                                                                                                             //PAR0133-NDR-20110326
//        end;                                                                                                                                          //PAR0133-NDR-20110326
//    finally                                                                                                                                           //PAR0133-NDR-20110326
//        spAgregarPagoRechazadoOtraConcesionaria.Close;                                                                                                //PAR0133-NDR-20110326
//    end;                                                                                                                                              //PAR0133-NDR-20110326
//end;                                                                                                                                                  //PAR0133-NDR-20110326

// Inicio Bloque SS_1015_CQU_20120510
{---------------------------------------------------------------------------------
        	ObtenerTipoTransaccion

 Author			: Claudio Quezada Ib��ez
 Date			: 12-Abril-2012
 Description	: Obtiene el Tipo de Transacci�n a registrar.
 Firma          : SS_1015_CQU_20120412
----------------------------------------------------------------------------------}
{
function TMainForm.ObtenerTipoTransaccion(Tipo : string) : Integer;
var
    SQL_COD_CONCESIONARIA : string;
begin
        // SS_1015_CQU_20120427 Inicio Mejora: deja en memoria los datos de esta tabla, de esa manera reduzco la cantidad de lecturas a SQL
        Result := -1;
        // Verifico que no se haya cargado con anterioridad
        if (qryTiposTransacciones.Connection = nil) then begin
            qryTiposTransacciones.Create(nil);
            qryTiposTransacciones.Connection := DMConnections.BaseCAC;
            qryTiposTransacciones.SQL.Text := 'SELECT CodigoTipoTransaccion, Descripcion FROM TiposTransacciones (NOLOCK)';
            qryTiposTransacciones.ExecSQL;
        end;
        qryTiposTransacciones.Open;
        qryTiposTransacciones.First;
        // Busco el tipo en los resultados cargados
        while not qryTiposTransacciones.Eof do begin
             if (Tipo = qryTiposTransacciones.FieldByName('Descripcion').AsString) then begin
                Result := qryTiposTransacciones.FieldByName('CodigoTipoTransaccion').AsInteger;
                break;
             end
             else
                qryTiposTransacciones.Next;
        end;
        qryTiposTransacciones.Close;

        //SQL_COD_CONCESIONARIA := 'SELECT CodigoTipoTransaccion FROM TiposTransacciones (NOLOCK) WHERE Descripcion = ''' + Tipo + '''';
        //Result := QueryGetValueInt(DMConnections.BaseCAC, SQL_COD_CONCESIONARIA);
        // SS_1015_CQU_20120427 Inicio Mejora
end;
}
// Fin Bloque SS_1015_CQU_20120510

{---------------------------------------------------------------------------------
        	MoverArchivo

 Author			: Claudio Quezada Ib��ez
 Date			: 23-Abril-2012
 Description	: Mueve un archivo de una ruta a otra,
                  si se indica se le cambia el nombre si esque existe en la ruta de destino
                  por defecto se coloca la fecha y hora en que se realiz� la acci�n.
 Parameters
        DirectorioOrigen    : Directorio en el que se encuentra el archivo a mover
        DirectorioDestino   : Directorio en el que se dejar� el archivo.
        NombreArchivo       : Nombre del archivo a mover.
        RenombraSiExiste    : Indica si se renombra el archivo en el directorio de destino
                              siempre y cuando exista un archivo con el mismo nombre.

 Firma          : SS_1015_CQU_20120423

 Firma          : SS_1015_CQU_20120502
 Descripcion    : Se realiza modificaci�n al nombre de archivo cuando estos est�n duplicados,
                  seg�n lo indicado por JP, se agrega el concepto "DUPLICADO" y luego la fecha.
----------------------------------------------------------------------------------}
function TMainForm.MoverArchivo(DirectorioOrigen, DirectorioDestino, NombreArchivo : string; RenombraSiExiste : boolean) : boolean;
resourcestring
    MSG_ERROR_INSERTANDO = 'Error al mover el archivo %s';
var
    NuevoNombreArchivo : string;
begin
//    Result := False;              //SS_1006_1015_MBE_20120921, est� dem�s
    try
        // Verifico que el archivo no exista
        if not FileExists(DirectorioDestino + NombreArchivo) then
            //mover a la carpeta duplicados (m�todo antig�o)
            Result := RenameFile(DirectorioOrigen + NombreArchivo, DirectorioDestino + NombreArchivo)
        else begin
            // Si el archivo existe lo renombro, s�lo si se habilit� la opci�n 'RenombraSiExiste'
            if RenombraSiExiste then
                //NuevoNombreArchivo := ChangeFileExt(NombreArchivo, '') + '_' + FormatDateTime ('yyyymmddHHMMss', Date + Time) + '.txt' // SS_1015_CQU_20120502
                NuevoNombreArchivo := NombreArchivo + '.DUPLICADO.' + FormatDateTime ('yyyymmddHHMMss', Date + Time) // SS_1015_CQU_20120502
            else
                NuevoNombreArchivo := NombreArchivo;
            // Muevo el archivo
            Result := RenameFile(DirectorioOrigen + NombreArchivo, DirectorioDestino + NuevoNombreArchivo);
        end;
        except on e:exception do begin
            EventLogReportEvent(elError, Format (MSG_ERROR_INSERTANDO, [NombreArchivo]) + ' Error: '  + e.Message, '');
            Result := False;
        end;
    end;
end;
{---------------------------------------------------------------------------------
        	MoverArchivoCambiarNombre

 Author			: Claudio Quezada Ib��ez
 Date			: 10-Mayo-2012
 Description	: Mueve un archivo de una ruta a otra.
 Parameters
        DirectorioOrigen        : Directorio en el que se encuentra el archivo a mover
        DirectorioDestino       : Directorio en el que se dejar� el archivo.
        NombreArchivoOrigen     : Nombre del archivo a mover.
        NombreArchivoDestino    : Nombre del archivo a mover.
        EsRechazado             : Indica si el archivo es un archivo de rechazo,
                                  en este caso el archivo se debe COPIAR a la carpeta de rechazos
                                  y mover a la de procesados.
----------------------------------------------------------------------------------}
function TMainForm.MoverArchivoCambiarNombre(DirectorioOrigen, DirectorioDestino, NombreArchivoOrigen, NombreArchivoDestino : string; EsRechazado : boolean) : boolean;
resourcestring
    MSG_ERROR_INSERTANDO = 'Error al mover el archivo %s';
var
//    NuevoNombreArchivo : string;              //SS_1006_1015_MBE_20120921, no se usa
    ExisteDestino : boolean;
begin
    Result := False;
    try
        // Verifico que el archivo no exista
        if (not EsRechazado) then
            ExisteDestino := FileExists(DirectorioDestino + NombreArchivoOrigen)
        else
            ExisteDestino := FileExists(DirectorioDestino + NombreArchivoDestino);

        if (not ExisteDestino) then begin
            if (not EsRechazado) then
                Result := MoverArchivo(DirectorioOrigen, DirectorioDestino, NombreArchivoOrigen, True)
            else begin
                CopyFile(PChar(DirectorioOrigen + NombreArchivoOrigen), PChar(DirectorioDestino + NombreArchivoDestino), False);
                Result := MoverArchivo(DirectorioOrigen, FCarpetaProcesados, NombreArchivoOrigen, True);
            end;
        end;
        except on e:exception do begin
            EventLogReportEvent(elError, Format (MSG_ERROR_INSERTANDO, [NombreArchivoOrigen]) + ' Error: '  + e.Message, '');
            Result := False;
        end;
    end;
end;

{---------------------------------------------------------------------------------
        	ObtenerTipoRegistroFolioyPago

 Author			: Claudio Quezada Ib��ez
 Date			: 12-Abril-2012
 Description	: Obtiene el Tipo de Registro de Folios y Pagos
 Firma          : SS_1015_CQU_20120510
---------------------------------------------------------------------------------}
function TMainForm.ObtenerTipoRegistroFolioyPago(Tipo : string) : Integer;
//var                                                     //SS_1006_1015_MBE_20120921, no se usa
//    SQL_COD_CONCESIONARIA : string;                     //SS_1006_1015_MBE_20120921, no se usa
begin
        Result := -1;
        // Verifico que no se haya cargado con anterioridad
        if (qryTiposTransacciones.Connection = nil) then begin
            qryTiposTransacciones.Create(nil);
            qryTiposTransacciones.Connection := DMConnections.BaseCAC;
            qryTiposTransacciones.SQL.Text := 'SELECT TipoRegistroFolioyPago, Descripcion FROM TipoRegistroFoliosyPagos WITH (NOLOCK)';
            qryTiposTransacciones.ExecSQL;
        end;
        qryTiposTransacciones.Open;
        qryTiposTransacciones.First;
        // Busco el tipo en los resultados cargados
        while not qryTiposTransacciones.Eof do begin
            if (Tipo = qryTiposTransacciones.FieldByName('TipoRegistroFolioyPago').AsString) then begin
                Result := qryTiposTransacciones.FieldByName('TipoRegistroFolioyPago').AsInteger;
                break;
            end
            else
                qryTiposTransacciones.Next;
        end;
        qryTiposTransacciones.Close;
end;

{---------------------------------------------------------------------------------                        //SS-1015-NDR-20120510
        	GenerarComprobanteCuotasOtrasConcesionarias                                                     //SS-1015-NDR-20120510
                                                                                                          //SS-1015-NDR-20120510
 Author			  : Nelson Droguett Sierra                                                                    //SS-1015-NDR-20120510
 Date			    : 14-Mayo-2012                                                                              //SS-1015-NDR-20120510
 Description	: Invoca al SP que genera el comprobante de cuotas de otras                                 //SS-1015-NDR-20120510
                concesionarias, con la informacion de la tabla CuotasOtrasConcesionarias                  //SS-1015-NDR-20120510
 ----------------------------------------------------------------------------------}                      //SS-1015-NDR-20120510
procedure TMainForm.GenerarComprobanteCuotasOtrasConcesionarias;                                          //SS-1015-NDR-20120510
resourcestring                                                                                            //SS-1015-NDR-20120510
    MSG_ERROR_GENERACOMPRO = 'Error Generando Comprobantes de Cuotas de Otras Concesionarias';            //SS-1015-NDR-20120510
begin                                                                                                     //SS-1015-NDR-20120510
    try                                                                                                   //SS-1015-NDR-20120510
      with spGenerarComprobanteCuotasOtrasConcesionarias do begin                                         //SS-1015-NDR-20120510
        Close;                                                                                            //SS-1015-NDR-20120510
        Connection:= DMConnections.BaseCAC;                                                               //SS-1015-NDR-20120510
        ExecProc;                                                                                         //SS-1015-NDR-20120510
        Close;                                                                                            //SS-1015-NDR-20120510
      end;                                                                                                //SS-1015-NDR-20120510
    except on e:exception do                                                                              //SS-1015-NDR-20120510
      begin                                                                                               //SS-1015-NDR-20120510
          EventLogReportEvent(elError, MSG_ERROR_GENERACOMPRO + ' Error: '  + e.Message, '');             //SS-1015-NDR-20120510
      end;                                                                                                //SS-1015-NDR-20120510
    end;                                                                                                  //SS-1015-NDR-20120510
end;                                                                                                      //SS-1015-NDR-20120510

{---------------------------------------------------------------------------------                        //SS-1015-NDR-20120510
        	GenerarComprobanteSaldosInicialesOtrasConcesionarias                                            //SS-1015-NDR-20120510
                                                                                                          //SS-1015-NDR-20120510
 Author			  : Nelson Droguett Sierra                                                                    //SS-1015-NDR-20120510
 Date			    : 14-Mayo-2012                                                                              //SS-1015-NDR-20120510
 Description	: Invoca al SP que genera el comprobante de saldos iniciales de otras                       //SS-1015-NDR-20120510
                concesionarias, con la informacion de la tabla SaldoInicialOtrasConcesionarias            //SS-1015-NDR-20120510
----------------------------------------------------------------------------------}                       //SS-1015-NDR-20120510
procedure TMainForm.GenerarComprobanteSaldosInicialesOtrasConcesionarias;                                 //SS-1015-NDR-20120510
resourcestring                                                                                            //SS-1015-NDR-20120510
    MSG_ERROR_GENERACOMPRO = 'Error Generando Comprobantes de Saldo Inicial de Otras Concesionarias';     //SS-1015-NDR-20120510
begin                                                                                                     //SS-1015-NDR-20120510
    try                                                                                                   //SS-1015-NDR-20120510
      with spGenerarComprobanteSaldoInicialOtrasConcesionarias do begin                                   //SS-1015-NDR-20120510
        Close;                                                                                            //SS-1015-NDR-20120510
        Connection:= DMConnections.BaseCAC;                                                               //SS-1015-NDR-20120510
        ExecProc;                                                                                         //SS-1015-NDR-20120510
        Close;                                                                                            //SS-1015-NDR-20120510
      end;                                                                                                //SS-1015-NDR-20120510
    except on e:exception do                                                                              //SS-1015-NDR-20120510
      begin                                                                                               //SS-1015-NDR-20120510
          EventLogReportEvent(elError, MSG_ERROR_GENERACOMPRO + ' Error: '  + e.Message, '');             //SS-1015-NDR-20120510
      end;                                                                                                //SS-1015-NDR-20120510
    end;                                                                                                  //SS-1015-NDR-20120510
end;                                                                                                      //SS-1015-NDR-20120510

{---------------------------------------------------------------------------------
        	GenerarNuevoNombre

 Author			: Claudio Quezada Ib��ez
 Date			: 30-Mayo-2012
 Description	: Genera el nombre del archivo Rechazado
 Parameters     :
                    NombreArchivoImportado  : Nombre del archivo importado
                    Sufijo                  : Sufijo a colocar en el archivo

 Return         : String con el Nombre de Archivo Rechazado
 Firma          : SS_1015_CQU_20120530
---------------------------------------------------------------------------------}
function TMainForm.GenerarNuevoNombre(NombreArchivoImportado, Sufijo : string) : string;
var
    Nombre, Extension : string;
//    Largo : integer;              //SS_1006_1015_MBE_20120921, no se usa
begin
    // Obtengo la Extensi�n
    Extension := ExtractFileExt(NombreArchivoImportado);
    // Obtengo s�lo el nombre del archivo (Sin Extensi�n)
    Nombre := ChangeFileExt(ExtractFileName(NombreArchivoImportado),'');
    // Nuevo Nombre del Archivo
    Result := Nombre + '_' + Sufijo + Extension;
end;

{---------------------------------------------------------------------------------
        	ValidarContexMark

 Author			: Claudio Quezada Ib��ez
 Date			: 26-Septiembre-2012
 Description	: Valida que la relaci�n IssuerIdentifier, ContextMark y TypeOfContract exista
 Parameters     :
                    IssuerIdentifier
                    ContextMark
                    TypeOfContract

 Return         : True, si existe; False si no
 Firma          : SS_1006_1015_CQU_20120926
---------------------------------------------------------------------------------}
function TMainForm.ValidarContexMark(IssuerIdentifier, ContextMark, TypeOfContract : String) : Boolean ;
const
    CONSULTA_SQL    = 'SELECT 1 FROM ContextMarks WITH (NOLOCK) WHERE IssuerIdentifier = %s AND ContextVersion = %s AND TypeOfContract = %s';
    MSG_ERROR       = 'Eroror al validar ContextMark';
begin
    Result := False;
    try
        if (QueryGetValueInt(DMConnections.BaseCAC, Format(CONSULTA_SQL, [Trim(IssuerIdentifier), Trim(ContextMark), Trim(TypeOfContract)])) = 1) then
            Result := True;
    except on e:exception do
      begin
          EventLogReportEvent(elError, MSG_ERROR + ' Error: '  + e.Message, '');
      end;
    end;
end;

{---------------------------------------------------------------------------------
        	ValidarEstacionamiento

 Author			: Claudio Quezada Ib��ez
 Date			: 27-Septiembre-2012
 Description	: Valida el Estacionamiento indicado no haya sido procesado con anterioridad
 Parameters     :
                    CodigoConcesionaria
                    NumTransaccionExterno

 Return         : True, si existe; False si no
 Firma          : SS_1006_1015_CQU_20120926
---------------------------------------------------------------------------------}
function TMainForm.ValidarEstacionamiento(CodigoConcesionaria, NumTransaccionExterno : String) : Boolean;
const
    CONSULTA_SQL    = 'SELECT dbo.VerificarEstacionamientoProcesado (%s, %s)';
    MSG_ERROR       = 'Eroror al validar estacionamientos procesados';
begin
    Result := False;
    try
        if (QueryGetValue(DMConnections.BaseCAC, Format(CONSULTA_SQL, [Trim(CodigoConcesionaria), Trim(NumTransaccionExterno)])) = 'True') then
            Result := True;
    except on e:exception do
      begin
          EventLogReportEvent(elError, MSG_ERROR + ' Error: '  + e.Message, '');
      end;
    end;
end;

{---------------------------------------------------------------------------------
        	ValidarCodigoExternoEstacionamiento

 Author			: Claudio Quezada Ib��ez
 Date			: 27-Septiembre-2012
 Description	: Valida que el C�digo Externo de Estacionamiento exista en la tabla EntradasEstacionamiento o SalidasEstacionamiento
                  dependiendo del tipo de consulta a realizar.
 Parameters     :
                    TipoConsulta        :   Indica si se consulta por Salidas o por Entradas (S / E)
                    CodigoConcesionaria :   C�digo de la concesionaria a consultar
                    CodigoExterno       :   C�digo externo a consultar

 Return         : True, si existe; False si no
 Firma          : SS_1006_1015_CQU_20121002
---------------------------------------------------------------------------------}
function TMainForm.ValidarCodigoExternoEstacionamiento(TipoConsulta : Char; CodigoConcesionariaLinea, CodigoExternoLinea : string) : Boolean;
const
    CONSULTA_ENTRADAS   = 'SELECT dbo.VerificarCodigoExternoEntradasEstacionamiento (%s, ''%s'')';
    CONSULTA_SALIDAS    = 'SELECT dbo.VerificarCodigoExternoSalidasEstacionamiento (%s, ''%s'')';
    MSG_ERROR           = 'Eroror al validar C�digo Externo de Entrada / Salida de Estacionamientos ';
var
    ConsultaSQL : AnsiString;
begin
    Result := False;

    // Verifico que el TipoConsulta sea S o E, sino, salgo y devuelvo False
    if TipoConsulta = 'S' then ConsultaSQL := CONSULTA_SALIDAS
    else if TipoConsulta = 'E' then ConsultaSQL := CONSULTA_ENTRADAS
    else Exit;

    try
        if (QueryGetValue(DMConnections.BaseCAC, Format(ConsultaSQL, [CodigoConcesionariaLinea, Trim(CodigoExternoLinea)])) = 'True') then
            Result := True;
    except on e:exception do
      begin
          EventLogReportEvent(elError, MSG_ERROR + ' Error: '  + e.Message, '');
      end;
    end;
end;

{---------------------------------------------------------------------------------
        	StringEsEntero

 Author			: Claudio Quezada Ib��ez
 Date			: 03-Octubre-2012
 Description	: Valida que el campo Num�rico se pueda Parsear a Integer o Int64
 Parameters     :
                    TextoEvaluar    :   Texto a evaluar si se puede parsear
                    EsBigInt        :   Indica si es un Integer o Int64

 Return         : True, si puede convertirlo; False si no
 Firma          : SS_1006_1015_CQU_20121002
---------------------------------------------------------------------------------}
function TMainForm.StringEsEntero(TextoEvaluar : string; EsBigInt : Boolean) : Boolean;
var
    AuxiliarInt     : Integer;
    AuxiliarBigInt  : Int64;
begin
    Result := False;
    
    if EsBigInt then Result := TryStrToInt64(TextoEvaluar, AuxiliarBigInt)
    else Result := TryStrToInt(TextoEvaluar, AuxiliarInt);
end;

end.


