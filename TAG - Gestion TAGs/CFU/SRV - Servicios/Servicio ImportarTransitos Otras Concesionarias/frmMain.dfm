object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 158
  ClientWidth = 427
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object dpsServiceImportarArchivosOtrasConce: TDPSService
    ServiceName = 'ImportarArchivosTrxOtrasConcesionarias'
    DisplayName = 
      'Kapsch TrafficCom Importar archivos Transacciones Otras Concesio' +
      'narias'
    OnStart = dpsServiceImportarArchivosOtrasConceStart
    OnStop = dpsServiceImportarArchivosOtrasConceStop
    Left = 16
    Top = 16
  end
  object spAgregarArchivo: TADOStoredProc
    ProcedureName = 'AgregarArchivoProcesadoOtraConcesionaria'
    Parameters = <>
    Left = 64
    Top = 16
  end
  object IdSMTPEnviarCorreo: TIdSMTP
    SASLMechanisms = <>
    Left = 328
    Top = 16
  end
  object tmerImportar: TTimer
    OnTimer = tmerImportarTimer
    Left = 360
    Top = 16
  end
  object aqryArchivo: TADOQuery
    Parameters = <>
    Left = 296
    Top = 16
  end
  object spAgregarTransitoOtraConcesionaria: TADOStoredProc
    ProcedureName = 'AgregarTransitoOtraConcesionaria'
    Parameters = <>
    Left = 96
    Top = 16
  end
  object spAgregarTransaccionEstacionamiento: TADOStoredProc
    ProcedureName = 'AgregarTransaccionEstacionamiento'
    Parameters = <>
    Left = 128
    Top = 16
  end
  object spAgregarRegRechOtrasConcesio: TADOStoredProc
    ProcedureName = 'AgregarRegistroRechazadoOtrasConcesionarias'
    Parameters = <>
    Left = 160
    Top = 16
  end
  object spAgregarTTORechOtraConcesio: TADOStoredProc
    ProcedureName = 'AgregarTransitoRechazadoOtraConcesionaria'
    Parameters = <>
    Left = 192
    Top = 16
  end
  object spObtenerConcesionarias: TADOStoredProc
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <>
    Left = 240
    Top = 64
  end
  object spAgregarESTARechOtraConcesio: TADOStoredProc
    ProcedureName = 'AgregarEstacionamientoRechazadoOtraConcesionaria'
    Parameters = <>
    Left = 224
    Top = 16
  end
  object spAgregarMovCtaOtraConc: TADOStoredProc
    ProcedureName = 'AgregarMovimientoOtraConcesionaria'
    Parameters = <>
    Left = 288
    Top = 64
  end
  object spAgregarMovCtaRechazado: TADOStoredProc
    ProcedureName = 'AgregarMovimientoRechazadoOtraConcesionaria'
    Parameters = <>
    Left = 320
    Top = 64
  end
  object spAgregarSaldoInicialRechazadoOtraConcesionaria: TADOStoredProc
    ProcedureName = 'AgregarSaldoInicialRechazadoOtraConcesionaria'
    Parameters = <>
    Left = 320
    Top = 96
  end
  object spAgregarSaldoInicialOtraConcesionaria: TADOStoredProc
    ProcedureName = 'AgregarRegistroASaldoInicialOtrasConcesionarias'
    Parameters = <>
    Left = 288
    Top = 96
  end
  object spAgregarCuotaOtraConcesionaria: TADOStoredProc
    ProcedureName = 'AgregarRegistroACuotasOtrasConcesionarias'
    Parameters = <>
    Left = 288
    Top = 128
  end
  object spAgregarCuotaRechazadaOtraConcesionaria: TADOStoredProc
    ProcedureName = 'AgregarCuotaRechazadaOtraConcesionaria'
    Parameters = <>
    Left = 320
    Top = 128
  end
  object spGenerarComprobanteSaldoInicialOtrasConcesionarias: TADOStoredProc
    ProcedureName = 'GenerarComprobanteSaldoInicialOtrasConcesionarias'
    Parameters = <>
    Left = 392
    Top = 64
  end
  object spGenerarComprobanteCuotasOtrasConcesionarias: TADOStoredProc
    ProcedureName = 'GenerarComprobanteCuotasOtrasConcesionarias'
    Parameters = <>
    Left = 392
    Top = 96
  end
end
