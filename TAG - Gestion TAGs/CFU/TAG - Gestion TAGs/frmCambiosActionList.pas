{********************************** File Header ********************************
File Name   : frmCambiosActionList.pas
Author      : rcastro
Date Created: 09/02/2005
Language    : ES-AR
Description : Permite cambiar los ActionLists de los Telev�a

Revisi�n    : 1
Author      : nefernandez
Date        : 25/04/2007
Description : Cambi� la conversi�n del par�metro ContractSerialNumber
(de asInteger a AsFloat)

Firma: SS_660_MCA_20130610
Author: Mcabello
Date: 10/06/2013
Descripcion: Se habilita opcion de menu Cambiar Action List y se adecua a las Tablas MaestroActionList
se agrega nuevo bit MMINotOk, en ref a la SS 660
Se bloquean Columnas Exception Handling y MMINotOk si el Tag se encuentra en lista amarilla
Se bloquea Columna Lista Amarilla para que no permita modificar el bit


Firma:SS_660_MCA_20130812
Author: Mcabello
Date: 12/08/2013
Descripcion: se agrega usuario a la modificacion de los bit, EH y CO

Firma:SS_660_MCA_20130912
Author: Mcabello
Date: 12/09/2013
Descripcion: se bloquea la lista verde para que no se pueda modificar

Firma:SS_660_MCA_20130916
Author: Mcabello
Date: 12/09/2013

Descripcion: se bloquea MMINotOk para que no se pueda modificar
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
*******************************************************************************}

unit frmCambiosActionList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, DBClient, ADODB, Grids, DBGrids, DPSControls, Util,
  ExtCtrls, DMConnection, PeaProcs, UtilProc, StrUtils, VariantComboBox,
  PeaTypes;
type
  TformCambiosActionList = class(TForm)
    Panel2: TPanel;
    dbgTAGs: TDBGrid;
    spObtenerDatosTAGs: TADOStoredProc;
    dsTAGs: TDataSource;
    cdsTAGs: TClientDataSet;
    spActivarLista: TADOStoredProc;
    Panel1: TPanel;
	Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    txtEtiquetaInicial: TEdit;
    Label2: TLabel;
    txtEtiquetaFinal: TEdit;
    spCrearHistoricoPedidoEnvioActionList: TADOStoredProc;
    btnLimpiar: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnBuscar: TButton;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnLimpiarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure dbgTAGsKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure dbgTAGsKeyPress(Sender: TObject; var Key: Char);
    procedure dbgTAGsColEnter(Sender: TObject);
  private
	{ Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
	{ Public declarations }
	function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  formCambiosActionList: TformCambiosActionList;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Inicializacion de este formulario
  Parameters: txtCaption: String; MDIChild: boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformCambiosActionList.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
		Constraints.MinWidth := 800;
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');
    //abro el dataset
	cdsTAGs.CreateDataSet;
	cdsTAGs.Open;
    //deshabilito el boton aceptar y la griila
	btnAceptar.Enabled := False;
	dbgTAGs.Enabled := False;
	Result := true;
end;

{-----------------------------------------------------------------------------
  Function Name: btnLimpiarClick
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Limpiar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCambiosActionList.btnLimpiarClick(Sender: TObject);
begin
	cdsTAGs.CancelUpdates;
	btnAceptar.Enabled := False;
	dbgTAGs.Enabled := False;
	txtEtiquetaInicial.Text := '';
	txtEtiquetaFinal.Text := '';
	txtEtiquetaInicial.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Validacion
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCambiosActionList.FormKeyPress(Sender: TObject; var Key: Char);
begin
 	case key of
		#13: begin
		end;

		else Exit;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnBuscarClick
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Busco los Televias en ese Rango
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCambiosActionList.btnBuscarClick(Sender: TObject);
resourcestring
	MSG_ERROR_FALTA_INDICAR_TELEVIA = 'Debe indicarse el telev�a %s';
	MSG_ERROR_TELEVIA_INVALIDO		= 'El telev�a %s es inv�lido';
begin
	if not ValidateControls(
		[txtEtiquetaInicial, txtEtiquetaInicial, txtEtiquetaFinal],
		[(Trim(txtEtiquetaInicial.Text) <> ''),
		ValidarEtiqueta(self.Caption, 'Telev�a Inicial Ingresado', PadL(txtEtiquetaInicial.Text, 11, '0'), false),
		ValidarEtiqueta(self.Caption, 'Telev�a Final Ingresado', PadL(txtEtiquetaFinal.Text, 11, '0'), false)],
		'Selecci�n de Telev�a',
		[Format (MSG_ERROR_FALTA_INDICAR_TELEVIA, ['Inicial']),
		Format (MSG_ERROR_TELEVIA_INVALIDO, ['Inicial']), Format (MSG_ERROR_TELEVIA_INVALIDO, ['Final'])])
	then begin
		Exit;
	end;

	with spObtenerDatosTAGs, Parameters do begin
		Close;
		ParamByName('@ContractSerialNumberInicial').Value:= EtiquetaToSerialNumber(PadL(txtEtiquetaInicial.Text, 11, '0'));
		ParamByName('@ContractSerialNumberFinal').Value	 := iif (Trim(txtEtiquetaFinal.Text) <> '',
																EtiquetaToSerialNumber(PadL(txtEtiquetaFinal.Text, 11, '0')),
																EtiquetaToSerialNumber(PadL(txtEtiquetaInicial.Text, 11, '0')));
		ParamByName('@TipoLista').Value	 := 0;//
		Open;
		cdsTags.EmptyDataSet;
		if RecordCount <> 0 then begin
			cdsTAGs.DisableControls;
			while not EoF do begin
				cdsTAGs.AppendRecord([
					FieldByName ('ContextMark').AsInteger,
					FieldByName ('ContractSerialNumber').AsString,
					FieldByName ('Etiqueta').AsString,
					FieldByName ('CodigoCategoria').AsInteger,
					FieldByName ('CodigoEstadoSituacionTAG').AsInteger,
					FieldByName ('IndicadorListaVerde').AsInteger,
					FieldByName ('IndicadorListaAmarilla').AsInteger,
					FieldByName ('IndicadorListaGris').AsInteger,
					FieldByName ('IndicadorListaNegra').AsInteger,
					FieldByName ('DescripcionCategoria').AsString,
					FieldByName ('DescripcionUbicacion').AsString,
					FieldByName ('EstadoSituacion').AsString,
					dbgTAGs.Columns[2].PickList[FieldByName ('IndicadorListaVerde').AsInteger],
					dbgTAGs.Columns[3].PickList[FieldByName ('IndicadorListaAmarilla').AsInteger],
					dbgTAGs.Columns[4].PickList[FieldByName ('IndicadorListaGris').AsInteger],
					dbgTAGs.Columns[5].PickList[FieldByName ('IndicadorListaNegra').AsInteger],
                    iif(FieldByName('FechaUltimoEnvio').IsNull ,'ss', FormatDateTime('dd/mm/yyyy hh:mm', FieldByName('FechaUltimoEnvio').asDateTime)),
                    FieldByName ('MMIContactOperator').AsInteger,
					FieldByName ('ExceptionHandling').AsInteger,
          			FieldByName ('MMInotOk').AsInteger,                                   //SS_660_MCA_20130610
          			dbgTAGs.Columns[6].PickList[FieldByName('MMIContactOperator').AsInteger],
          			dbgTAGs.Columns[7].PickList[FieldByName('ExceptionHandling').AsInteger],
          			dbgTAGs.Columns[8].PickList[FieldByName('MMInotOk').AsInteger]        //SS_660_MCA_20130610
          			]);


				Next;
			end;

      //Se bloquea la columna de lista amarilla
      dbgTAGs.Columns[3].ReadOnly := True;                                      //SS_660_MCA_20130610
      dbgTAGs.Columns[2].ReadOnly := True;                                      //SS_660_MCA_20130912
      dbgTAGs.Columns[8].ReadOnly := True;                                      //SS_660_MCA_20130916
			cdsTAGs.First;
			cdsTAGs.EnableControls;
		end;

		Close;
	end;

	btnAceptar.Enabled := not cdsTAGs.IsEmpty;
	dbgTAGs.Enabled := btnAceptar.Enabled
end;

{-----------------------------------------------------------------------------
  Function Name: dbgTAGsColEnter
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Al hacer click te permite editar una celda o no
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}

procedure TformCambiosActionList.dbgTAGsColEnter(Sender: TObject);
begin
    //if ((Sender as TDBGrid).SelectedIndex >= 2) and ((Sender as TDBGrid).SelectedIndex <= 7) then //SS_660_MCA_20130916 //SS_660_MCA_20130610
    if ((Sender as TDBGrid).SelectedIndex > 3) and ((Sender as TDBGrid).SelectedIndex <= 7) then //SS_660_MCA_20130916 //SS_660_MCA_20130610
        //if ((Sender as TDBGrid).SelectedIndex >= 2) and ((Sender as TDBGrid).SelectedIndex <= 8) then //SS_660_MCA_20130916 //SS_660_MCA_20130610
        (Sender as TDBGrid).Options := (Sender as TDBGrid).Options + [dgEditing]
    else
        (Sender as TDBGrid).Options := (Sender as TDBGrid).Options - [dgEditing];
end;

{-----------------------------------------------------------------------------
  Function Name: dbgTAGsKeyDown
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Al Recorrer la grilla  te permite editar una celda o no
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCambiosActionList.dbgTAGsKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
	dbgTAGs.Options := dbgTAGs.Options + [dgEditing];
    if (ssCtrl in Shift) and (key = VK_DELETE) then key := 0;
    if (key = VK_INSERT) then key := 0;
	if (key = VK_DOWN) or (key = VK_TAB) then begin
		dbgTAGs.Options := dbgTAGs.Options - [dgEditing];
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: dbgTAGsKeyPress
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: permite escribir en la celda
  Parameters: Sender: TObject;var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCambiosActionList.dbgTAGsKeyPress(Sender: TObject;var Key: Char);
begin
  //if ((Sender as TDBGrid).SelectedIndex >= 2) and ((Sender as TDBGrid).SelectedIndex <= 7) then Key := #0   //SS_660_MCA_20130916 //SS_660_MCA_20130610
  if ((Sender as TDBGrid).SelectedIndex > 3) and ((Sender as TDBGrid).SelectedIndex <= 7) then Key := #0      //SS_660_MCA_20130916 //SS_660_MCA_20130610
    //if ((Sender as TDBGrid).SelectedIndex >= 2) and ((Sender as TDBGrid).SelectedIndex <= 8) then Key := #0 //SS_660_MCA_20130916 //SS_660_MCA_20130610
    else Key := UpCase(Key);
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Guarda los datos que se hallan Realizado
  Parameters: Sender: TObject
  Return Value: None
  revision 1:
  autor : aganc
  descripcion : se agrego las columnas  MMIContacOperator ActivarExceptionHandling
  a la grilla su correspondiente llamada a los sp q actualizan su estado

    Revision 1:
      Author : ggomez
      Date : 07/06/2006
      Description : Inicialic� las variables SetExceptionHandling y
        SetMMIContactOperator.

Revision 2:
    Author : ggomez
    Date : 06/07/2006
    Description : Agregu� el par�metro @TipoHistorico en el llamado al SP
        que inserta registro en el Hist�rico de Pedidos de Env�o de Action List.

Revision 3:
    Author : ggomez
    Date : 25/07/2006
    Description : Modifiqu� el nombre de la constante del Tipo de Hist�rico para
        Pedido Pendiente.

-----------------------------------------------------------------------------}
procedure TformCambiosActionList.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_ERROR_CAMBIOS_ACTIONLIST = 'Error en el cambio de ActionList';
var
	TodoOk: Boolean;
    SetExceptionHandling, SetMMIContactOperator, SetGreenList, SetYellowList,
    SetGrayList, SetBlackList, ClearGreenList, ClearYellowList, ClearGrayList,
    ClearBlackList: Boolean;


begin
    //si hubo algun cambio guarda los cambios
	if cdsTAGS.State in [dsEdit, dsInsert] then cdsTAGS.Post;

	Screen.Cursor := crHourGlass;
	TodoOk := False;

	DMConnections.BaseCAC.BeginTrans;
	try
		try
			// graba los datos
			with cdsTAGs do begin
				DisableControls;

				First;
				while not EoF do begin
                    // Incializar las variables con los valores leidos desde la BD.
                    SetGreenList    := iif(FieldByName ('IndicadorListaVerde').AsInteger = 1, True, False);
                    SetYellowList   := iif(FieldByName ('IndicadorListaAmarilla').AsInteger = 1, True, False);
                    SetGrayList     := iif(FieldByName ('IndicadorListaGris').AsInteger = 1, True, False);
                    SetBlackList    := iif(FieldByName ('IndicadorListaNegra').AsInteger = 1, True, False);
                    ClearGreenList  := Not SetGreenList;
                    ClearYellowList := Not SetYellowList;
                    ClearGrayList   := Not SetGrayList;
                    ClearBlackList  := Not SetBlackList;
                    SetExceptionHandling    := iif(FieldByName('ExceptionHandling').AsInteger = 1, True, False);
                    SetMMIContactOperator   := iif(FieldByName('MMIContactOperator').AsInteger = 1, True, False);

					if FieldByName ('NuevaListaVerde').AsString <> dbgTAGs.Columns[2].PickList[FieldByName ('IndicadorListaVerde').AsInteger] then begin
						spActivarLista.ProcedureName := 'ActivarGreenListFlag';
						spActivarLista.Parameters.Refresh;
						spActivarLista.Parameters.ParamByName('@ContextMark').Value 		 := FieldByName ('ContextMark').AsInteger;
						spActivarLista.Parameters.ParamByName('@ContractSerialNumber').Value := FieldByName ('ContractSerialNumber').AsFloat; // Revision 1
						spActivarLista.Parameters.ParamByName('@Activar').Value 			 := FieldByName ('NuevaListaVerde').AsString = 'SI';
            			spActivarLista.Parameters.ParamByName('@Usuario').Value 	 := UsuarioSistema;                               //SS-1006-NDR-20120715
						spActivarLista.ExecProc;

                        // preparo variables para historico
                        if FieldByName ('NuevaListaVerde').AsString = 'SI' then begin
                            SetGreenList   := True;
                            ClearGreenList := False;
                        end else begin
                            ClearGreenList := True;
                            SetGreenList   := False;
                        end;
					end else						//SS_660_MCA_20130916
              		begin							//SS_660_MCA_20130916
                 		ClearGreenList := False;	//SS_660_MCA_20130916
              		end;							//SS_660_MCA_20130916

					if FieldByName ('NuevaListaAmarilla').AsString <> dbgTAGs.Columns[3].PickList[FieldByName ('IndicadorListaAmarilla').AsInteger] then begin
						spActivarLista.ProcedureName := 'ActivarYellowListFlag';
						spActivarLista.Parameters.Refresh;
						spActivarLista.Parameters.ParamByName('@ContextMark').Value 		 := FieldByName ('ContextMark').AsInteger;
						spActivarLista.Parameters.ParamByName('@ContractSerialNumber').Value := FieldByName ('ContractSerialNumber').AsFloat; // Revision 1
						spActivarLista.Parameters.ParamByName('@Activar').Value 			 := FieldByName ('NuevaListaAmarilla').AsString = 'SI';
            			spActivarLista.Parameters.ParamByName('@Usuario').Value 	 		 := UsuarioSistema;                               //SS-1006-NDR-20120715
						spActivarLista.ExecProc;

                        // preparo variables para historico
                        if FieldByName ('NuevaListaAmarilla').AsString = 'SI' then begin
                            SetYellowList   := True;
                            ClearYellowList := False;
                        end else begin
                            ClearYellowList := True;
                            SetYellowList   := False;
                        end;
					end else						//SS_660_MCA_20130916
					begin							//SS_660_MCA_20130916
                 		ClearYellowList := False;	//SS_660_MCA_20130916
              		end;							//SS_660_MCA_20130916

					if FieldByName ('NuevaListaGris').AsString <> dbgTAGs.Columns[4].PickList[FieldByName ('IndicadorListaGris').AsInteger] then begin
                        spActivarLista.ProcedureName := 'ActivarGrayListFlag';
                        spActivarLista.Parameters.Refresh;
                        spActivarLista.Parameters.ParamByName('@ContextMark').Value 		 := FieldByName ('ContextMark').AsInteger;
                        spActivarLista.Parameters.ParamByName('@ContractSerialNumber').Value := FieldByName ('ContractSerialNumber').AsFloat; // Revision 1
                        spActivarLista.Parameters.ParamByName('@Activar').Value 			 := FieldByName ('NuevaListaGris').AsString = 'SI';
                        spActivarLista.Parameters.ParamByName('@Usuario').Value 	 := UsuarioSistema;                               //SS-1006-NDR-20120715
                        spActivarLista.ExecProc;
                   		// preparo variables para historico
                        if FieldByName ('NuevaListaGris').AsString = 'SI' then begin
                            SetGrayList   := True;
                            ClearGrayList := False;
                        end else begin
                            ClearGrayList := True;
                            SetGrayList   := False;
                        end;
                    end else					//SS_660_MCA_20130916
              		begin						//SS_660_MCA_20130916
                 		ClearGrayList := False;	//SS_660_MCA_20130916
              		end;						//SS_660_MCA_20130916
                    //  revision 1:
                    if FieldByName('NuevoExceptionHandling').AsString <> dbgTAGs.Columns[7].PickList[FieldByName('ExceptionHandling').AsInteger] then begin
                        spActivarLista.ProcedureName := 'ActivarExceptionHandling';
                        spActivarLista.Parameters.Refresh;
                        spActivarLista.Parameters.ParamByName('@ContextMark').Value 		 := FieldByName ('ContextMark').AsInteger;
                        spActivarLista.Parameters.ParamByName('@ContractSerialNumber').Value := FieldByName ('ContractSerialNumber').AsFloat; // Revision 1
                        spActivarLista.Parameters.ParamByName('@Activar').Value 			 := FieldByName ('NuevoExceptionHandling').AsString = 'SI';
						spActivarLista.Parameters.ParamByName('@Usuario').Value				 := UsuarioSistema;	//SS_660_MCA_20130812
                        spActivarLista.ExecProc;
                        SetExceptionHandling := FieldByName('NuevoExceptionHandling').AsString = 'SI';
                    end;

                    if FieldByName('NuevoMMIContactOperator').AsString <> dbgTAGs.Columns[6].PickList[FieldByName('MMIContactOperator').AsInteger] then begin
                        spActivarLista.ProcedureName := 'ActivarMMIContactOperator';
                        spActivarLista.Parameters.Refresh;
                        spActivarLista.Parameters.ParamByName('@ContextMark').Value 		 := FieldByName('ContextMark').AsInteger;
                        spActivarLista.Parameters.ParamByName('@ContractSerialNumber').Value := FieldByName('ContractSerialNumber').AsFloat; // Revision 1
                        spActivarLista.Parameters.ParamByName('@Activar').Value 			 := FieldByName('NuevoMMIContactOperator').AsString = 'SI';
						spActivarLista.Parameters.ParamByName('@Usuario').Value				 := UsuarioSistema;	//SS_660_MCA_20130812
						spActivarLista.ExecProc;
                        SetMMIContactOperator := FieldByName('NuevoMMIContactOperator').AsString = 'SI';
					end;
                    //fin   revision 1:
					//ini //SS_660_MCA_20130610
		            if FieldByName('NuevoMMINotOk').AsString <> dbgTAGs.Columns[8].PickList[FieldByName('MMINotOk').AsInteger] then begin
		                spActivarLista.ProcedureName := 'ActivarMMINotOk';
		                spActivarLista.Parameters.Refresh;
		                spActivarLista.Parameters.ParamByName('@ContextMark').Value 		 := FieldByName('ContextMark').AsInteger;
		                spActivarLista.Parameters.ParamByName('@ContractSerialNumber').Value := FieldByName('ContractSerialNumber').AsFloat; // Revision 1
		                spActivarLista.Parameters.ParamByName('@Activar').Value 			 := FieldByName('NuevoMMINotOk').AsString = 'SI';
		                spActivarLista.ExecProc;
		
		            end;
		            //fin   //SS_660_MCA_20130610

					if FieldByName ('NuevaListaNegra').AsString <> dbgTAGs.Columns[5].PickList[FieldByName ('IndicadorListaNegra').AsInteger] then begin
						spActivarLista.ProcedureName := 'ActivarBlackListFlag';
						spActivarLista.Parameters.Refresh;
						spActivarLista.Parameters.ParamByName('@ContextMark').Value 		 := FieldByName ('ContextMark').AsInteger;
						spActivarLista.Parameters.ParamByName('@ContractSerialNumber').Value := FieldByName ('ContractSerialNumber').AsFloat; // Revision 1
						spActivarLista.Parameters.ParamByName('@Activar').Value 			 := FieldByName ('NuevaListaNegra').AsString = 'SI';
						spActivarLista.Parameters.ParamByName('@FechaActivacion').Value 	 := NowBase(DMConnections.BaseCAC);
            			spActivarLista.Parameters.ParamByName('@Usuario').Value 	 		 := UsuarioSistema;                               //SS-1006-NDR-20120715
						spActivarLista.ExecProc;

                        // preparo variables para historico
                        //if FieldByName ('NuevaListaGris').AsString = 'SI' then begin	//SS_660_MCA_20130812
						if FieldByName ('NuevaListaNegra').AsString = 'SI' then begin	//SS_660_MCA_20130812
                            SetBlackList   := True;
                            ClearBlackList := False;
                        end else begin
                            ClearBlackList := True;
                            SetBlackList   := False;
                        end;
					end else						//SS_660_MCA_20130916
					begin							//SS_660_MCA_20130916
						ClearBlackList := False;	//SS_660_MCA_20130916
					end;							//SS_660_MCA_20130916
                    // guardo un registro historico para estos cambios
                spCrearHistoricoPedidoEnvioActionList.Close;
                spCrearHistoricoPedidoEnvioActionList.Parameters.Refresh;
                with spCrearHistoricoPedidoEnvioActionList.Parameters do begin

                    ParamByName('@ContextMark').Value          := FieldByName ('ContextMark').AsInteger;
                    ParamByName('@ContractSerialNumber').Value := FieldByName ('ContractSerialNumber').AsFloat; // Revision 1
                    ParamByName('@CodigoUsuario').Value        := UsuarioSistema;
                    ParamByName('@SetGreenList').Value         := SetGreenList;
                    ParamByName('@ClearGreenList').Value       := ClearGreenList;
                    ParamByName('@SetYellowList').Value        := SetYellowList;
                    ParamByName('@ClearYellowList').Value      := ClearYellowList;
                    ParamByName('@SetGrayList').Value          := SetGrayList;
                    ParamByName('@ClearGrayList').Value        := ClearGrayList;
                    ParamByName('@SetBlackList').Value         := SetBlackList;
                    ParamByName('@ClearBlackList').Value       := ClearBlackList;

                    ParamByName('@ExceptionHandling').Value    := SetExceptionHandling;
                    ParamByName('@MMIContactOperator').Value   := SetMMIContactOperator;
                    ParamByName('@CodigoSistema').Value        := SistemaActual;

                    ParamByName('@TipoHistorico').Value        := ACTION_LIST_PEDIDO_PENDIENTE;

                end; //end with spCrearHistoricoPedidoEnvioActionList.Parameters do begin
                spCrearHistoricoPedidoEnvioActionList.ExecProc;

                // sigo con el siguiente registro
		        Next; //next cdsTAGs
		    end;  //while not EoF do begin
			TodoOk := True;
          end;//with cdsTAGs do begin
		except
			on e: Exception do begin
				DMConnections.BaseCAC.RollbackTrans;
				MsgBoxErr(MSG_ERROR_CAMBIOS_ACTIONLIST, e.message, self.caption, MB_ICONSTOP)
			end;
		end;

	finally
		Screen.Cursor := crDefault;

		if TodoOk then begin
			DMConnections.BaseCAC.CommitTrans;

			MsgBox('Datos grabados con �xito', self.caption, MB_ICONINFORMATION);
			btnLimpiarClick(Sender);
		end;

		cdsTAGs.EnableControls;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCambiosActionList.btnCancelarClick(Sender: TObject);
begin
	cdsTAGs.Close;
	Close
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 09/03/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCambiosActionList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;


end.
