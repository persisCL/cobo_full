{********************************** File Header ********************************
File Name   : frmControlCalidadTAGs
Author      : Castro, Ra�l A.
Date Created: 10/05/04
Language    : ES-AR
Description : Control de calidad manual para TAGs
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

*******************************************************************************}

unit frmControlCalidadTAGs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ValEdit, DPSControls, ListBoxEx, DBListEx, DB,
  DBClient, ADODB, PeaProcs, Util, DBGrids, DMConnection, UtilProc;

type
  TFControlCalidadTAGs = class(TForm)
	lblPallet: TLabel;
    lblIDCaja: TLabel;
    lblCantidadTAGs: TLabel;
    dbgTAGS: TDBGrid;
    dsCajas: TDataSource;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnRechazar: TButton;
    btnGrabar: TButton;
    DPSButton1: TButton;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnRechazarClick(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	function EliminarTAGsIngresados: Boolean;
    procedure DPSButton1Click(Sender: TObject);
  private
    { Private declarations }
	FEstadoCaja : ANSIString;
    FCantidadTAGS,
    FCantidadTAGsEnCaja,
    FIDCaja: Integer;
    FCausaRechazo: ANSIString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    //INICIO	: 20160621 CFU
    bCierraForm: Boolean;
    //TERMINO	: 20160621 CFU
  public
    { Public declarations }
	function Inicializar(IDPallet, IDCaja, CantidadTAGS: Integer; EstadoCaja: ANSIString): Boolean;
  published
	property EstadoCaja: ANSIString Read FEstadoCaja Write FEstadoCaja;
	property CantidadTAGsEnCaja: Integer Read FCantidadTAGsEnCaja Write FCantidadTAGsEnCaja;
	property CausaRechazo: ANSIString Read FCausaRechazo Write FCausaRechazo;
  end;

var
  FControlCalidadTAGs: TFControlCalidadTAGs;

implementation

uses frmOpcionesControlCalidadTAGs,frmControlCalidadPallets;

{$R *.dfm}

function TFControlCalidadTAGs.Inicializar(IDPallet, IDCaja, CantidadTAGS: Integer; EstadoCaja: ANSIString): Boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu; 																					//SS_1147_NDR_20141216
    FCantidadTAGS		:= CantidadTAGS;
    FCantidadTAGsEnCaja := CantidadTAGs;
    FIDCaja  	 		:= IDCaja;
    FEstadoCaja	 		:= EstadoCaja;
    FCausaRechazo		:= '';

    dsCajas.DataSet  := TFControlCalidadPallets(owner).cdsTAGs;

    //ShowMessage(IntToStr(TFControlCalidadPallets(owner).cdsTAGs.RecordCount));

    lblPallet.Caption := 'ID Pallet: ' + IntToStr(IDPallet);
    lblIDCaja.Caption := 'ID Caja: ' + IntToStr(FIDCaja);
    lblCantidadTAGs.Caption := 'Telev�as: ' + IntToStr (dbgTAGs.DataSource.DataSet.RecordCount) + ' de ' + IntToStr (CantidadTAGS);
	dbgTAGs.DataSource.DataSet.Filter := 'IDCaja = ' + IntToStr(FIDCaja);
    //INICIO	: 20160621 CFU
    bCierraForm  := False;
    //TERMINO	: 20160621 CFU
	Result := true;
end;

procedure TFControlCalidadTAGs.btnAceptarClick(Sender: TObject);
begin
	if Trim(dbgTAGs.DataSource.DataSet.FieldByName('ContractSerialNumber').AsString) <> '' then begin
	    dbgTAGs.DataSource.DataSet.Edit;
    	dbgTAGs.DataSource.DataSet.FieldByName('Estado').AsString := 'ACEPTADO';
	    dbgTAGs.DataSource.DataSet.Post
    end
end;

procedure TFControlCalidadTAGs.btnRechazarClick(Sender: TObject);
begin
	if Trim(dbgTAGs.DataSource.DataSet.FieldByName('ContractSerialNumber').AsString) <> '' then begin
	    dbgTAGs.DataSource.DataSet.Edit;
    	dbgTAGs.DataSource.DataSet.FieldByName('Estado').AsString := 'RECHAZADO';
	    dbgTAGs.DataSource.DataSet.Post
    end
end;

procedure TFControlCalidadTAGs.btnGrabarClick(Sender: TObject);
begin
	Close
end;

function TFControlCalidadTAGs.EliminarTAGsIngresados: Boolean;
begin
	with dbgTAGs.DataSource.DataSet do begin
    	First;
        while not EoF do begin
        	if FieldByName('IDCaja').AsInteger = FIDCaja then begin
            	Delete
            end else
            	Next
        end
    end;

    result := True
end;

procedure TFControlCalidadTAGs.btnCancelarClick(Sender: TObject);
begin
    if MsgBox('Desea anular el proceso de control efectuado hasta el momento?', self.caption, MB_YESNO + MB_ICONQUESTION) <> ID_YES then 
    	Exit
    else begin
    //INICIO	: 20160621 CFU
    	bCierraForm := True;
    //TERMINO	: 20160621 CFU
    	Close;
    end;
end;

procedure TFControlCalidadTAGs.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
	f: TformOpcionesControlCalidadTAGs;
	StrMensaje: String;
begin
    //INICIO	: 20160621 CFU
	if bCierraForm then begin
       	CanClose := True;
		Exit
    end;
    //TERMINO	: 20160621 CFU

	if dbgTAGs.DataSource.DataSet.State in [dsEdit, dsInsert] then dbgTAGs.DataSource.DataSet.Post;

	if dbgTAGs.DataSource.DataSet.IsEmpty then begin
       	CanClose := True;
		Exit
    end;

	StrMensaje := 'Se han controlado ' + IntToStr (dbgTAGs.DataSource.DataSet.RecordCount) + ' de los ' +
					IntToStr (FCantidadTAGs) + ' Telev�as de la caja.' + CRLF + 'Indicar la acci�n a realizar';

    Application.CreateForm(TformOpcionesControlCalidadTAGs, f);
	f.Inicializar(StrMensaje, dbgTAGs.DataSource.DataSet.RecordCount <> FCantidadTAGs);
	f.ShowModal;

    case f.rgAccion.ItemIndex of
       	1: begin
			// elimino los tags ingresados ya que voy a Rechazar toda la caja
			EliminarTAGsIngresados;
           	FEstadoCaja := 'R';
            FCausaRechazo := f.txtCausaRechazo.text;

			CanClose := True
		end;

        2: begin
        	// elimino los tags ingresados ya que voy a dar por Aceptada toda la caja
   	        EliminarTAGsIngresados;
       	    FEstadoCaja := 'A';

            CanClose := True
        end;

        3: begin
            FCantidadTAGsEnCaja := dbgTAGs.DataSource.DataSet.RecordCount;
       	    FEstadoCaja := 'A';

            CanClose := True
        end;

        else CanClose := False
    end;

	f.Release;
end;

procedure TFControlCalidadTAGs.DPSButton1Click(Sender: TObject);
begin
	if dbgTAGs.DataSource.DataSet.RecordCount > 0 then dbgTAGs.DataSource.DataSet.Delete
end;

end.

