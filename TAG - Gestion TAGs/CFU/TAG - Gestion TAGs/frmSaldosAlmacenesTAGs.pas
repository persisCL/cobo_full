unit frmSaldosAlmacenesTAGs;
{
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, DPSControls, DB, ADODB, UtilProc,
  StrUtils, UtilDB, ExtCtrls, VariantComboBox, peaProcs, RStrings, Util;        //SS_1147_NDR_20141216

type
  TFSaldosAlmacenesTAGs = class(TForm)
    dsSaldosAlmacenes: TDataSource;
    DBListEx1: TDBListEx;
    GroupBox1: TGroupBox;
    ObtenerSaldosAlmacenes: TADOStoredProc;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cbBodegas: TVariantComboBox;
    cbAlmacenes: TVariantComboBox;
    cbCategorias: TVariantComboBox;
    btnFiltrar: TButton;
    btnSalir: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFiltrarClick(Sender: TObject);
    procedure cbBodegasChange(Sender: TObject);
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    { Public declarations }
	function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  FSaldosAlmacenesTAGs: TFSaldosAlmacenesTAGs;

implementation

uses DMConnection;

{$R *.dfm}

function TFSaldosAlmacenesTAGs.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
       	CenterForm(Self);
	end;
	Caption := AnsiReplaceStr(txtCaption, '&', '');

    CargarBodegas(DMConnections.BaseCAC, cbBodegas, 0);
    {INICIO:	20160720 CFU
    CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, 0);
    }
    CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, '', 0);
    //TERMINO:	20160720 CFU
    CargarCategoriasVehiculos(DMConnections.BaseCAC, cbCategorias, 0, true);
    BtnFiltrar.click;

	Result := true;
end;

procedure TFSaldosAlmacenesTAGs.FormDestroy(Sender: TObject);
begin
	ObtenerSaldosAlmacenes.Close
end;

procedure TFSaldosAlmacenesTAGs.btnSalirClick(Sender: TObject);
begin
  Close
end;

//INICIO:	20160720 CFU
procedure TFSaldosAlmacenesTAGs.cbBodegasChange(Sender: TObject);
begin
	CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, 'CodigoBodega = ' + IntToStr(cbBodegas.Value), 0);
end;
//TERMINO:	20160720 CFU

procedure TFSaldosAlmacenesTAGs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TFSaldosAlmacenesTAGs.btnFiltrarClick(Sender: TObject);
begin
	try
	    ObtenerSaldosAlmacenes.close;
    	ObtenerSaldosAlmacenes.Parameters.ParamByName('@CodigoBodega').value := cbBodegas.Value;
	    ObtenerSaldosAlmacenes.Parameters.ParamByName('@CodigoAlmacen').value := cbAlmacenes.Value;
    	ObtenerSaldosAlmacenes.Parameters.ParamByName('@CodigoCategoria').value := cbCategorias.Value;
	    ObtenerSaldosAlmacenes.open;
	except
		on e: Exception do MsgBoxErr(MSG_ERROR_SALDOS_ALMACENES, e.message, self.caption, MB_ICONSTOP);
    end
end;

end.

