object formCambiosActionList: TformCambiosActionList
  Left = 131
  Top = 152
  ClientHeight = 517
  ClientWidth = 816
  Color = clBtnFace
  Constraints.MinHeight = 550
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 479
    Width = 816
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 816
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        816
        38)
      object btnLimpiar: TButton
        Left = 532
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = '&Limpiar'
        TabOrder = 0
        OnClick = btnLimpiarClick
      end
      object btnAceptar: TButton
        Left = 619
        Top = 6
        Width = 100
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = '&Guardar Datos'
        TabOrder = 1
        OnClick = btnAceptarClick
      end
      object btnCancelar: TButton
        Left = 727
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Cancelar'
        TabOrder = 2
        OnClick = btnCancelarClick
      end
    end
  end
  object dbgTAGs: TDBGrid
    Left = 0
    Top = 40
    Width = 816
    Height = 439
    Align = alClient
    DataSource = dsTAGs
    Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnColEnter = dbgTAGsColEnter
    OnKeyDown = dbgTAGsKeyDown
    OnKeyPress = dbgTAGsKeyPress
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Etiqueta'
        ReadOnly = True
        Title.Alignment = taCenter
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DescripcionCategoria'
        ReadOnly = True
        Title.Caption = 'Categor'#237'a'
        Width = 116
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NuevaListaVerde'
        PickList.Strings = (
          'NO'
          'SI')
        Title.Alignment = taCenter
        Title.Caption = 'L.Verde'
        Width = 55
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NuevaListaAmarilla'
        PickList.Strings = (
          'NO'
          'SI')
        Title.Alignment = taCenter
        Title.Caption = 'L.Amarilla'
        Width = 55
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NuevaListaGris'
        PickList.Strings = (
          'NO'
          'SI')
        Title.Alignment = taCenter
        Title.Caption = 'L.Gris'
        Width = 56
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NuevaListaNegra'
        PickList.Strings = (
          'NO'
          'SI')
        Title.Alignment = taCenter
        Title.Caption = 'L.Negra'
        Width = 55
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NuevoMMIContactOperator'
        PickList.Strings = (
          'NO'
          'SI')
        Title.Alignment = taCenter
        Title.Caption = 'Contact Operator'
        Width = 110
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NuevoExceptionHandling'
        PickList.Strings = (
          'NO'
          'SI')
        Title.Alignment = taCenter
        Title.Caption = 'Exception Handling'
        Width = 110
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NuevoMMINotOK'
        PickList.Strings = (
          'NO'
          'SI')
        Title.Alignment = taCenter
        Title.Caption = 'Not Ok'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DescripcionUbicacion'
        ReadOnly = True
        Title.Caption = 'Ubicaci'#243'n'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EstadoSituacion'
        ReadOnly = True
        Title.Caption = 'Estado de Situaci'#243'n'
        Width = 110
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'FechaUltimoEnvio'
        ReadOnly = True
        Title.Caption = 'Fecha '#218'ltimo Env'#237'o'
        Width = 110
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 816
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 6
      Top = 12
      Width = 87
      Height = 13
      Caption = 'Telev'#237'a &Inicial:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 240
      Top = 12
      Width = 80
      Height = 13
      Caption = 'Telev'#237'a &Final:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Panel4: TPanel
      Left = 720
      Top = 0
      Width = 96
      Height = 40
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        96
        40)
      object btnBuscar: TButton
        Left = 9
        Top = 7
        Width = 78
        Height = 26
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Buscar'
        Default = True
        TabOrder = 0
        OnClick = btnBuscarClick
      end
    end
    object txtEtiquetaInicial: TEdit
      Left = 96
      Top = 9
      Width = 142
      Height = 21
      Color = 16444382
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object txtEtiquetaFinal: TEdit
      Left = 324
      Top = 9
      Width = 142
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object spObtenerDatosTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerDatosTAGs;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumberInicial'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ContractSerialNumberFinal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoLista'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 255
    Top = 210
  end
  object dsTAGs: TDataSource
    DataSet = cdsTAGs
    Left = 189
    Top = 210
  end
  object cdsTAGs: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ContextMark'
        DataType = ftSmallint
      end
      item
        Name = 'ContractSerialNumber'
        DataType = ftLargeint
      end
      item
        Name = 'Etiqueta'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'CodigoCategoria'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoEstadoSituacion'
        DataType = ftSmallint
      end
      item
        Name = 'IndicadorListaVerde'
        DataType = ftSmallint
      end
      item
        Name = 'IndicadorListaAmarilla'
        DataType = ftSmallint
      end
      item
        Name = 'IndicadorListaGris'
        DataType = ftSmallint
      end
      item
        Name = 'IndicadorListaNegra'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DescripcionUbicacion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'EstadoSituacion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'NuevaListaVerde'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'NuevaListaAmarilla'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'NuevaListaGris'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'NuevaListaNegra'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'FechaUltimoEnvio'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MMIContactOperator'
        DataType = ftSmallint
      end
      item
        Name = 'ExceptionHandling'
        DataType = ftSmallint
      end
      item
        Name = 'MMINotOk'
        DataType = ftSmallint
      end
      item
        Name = 'NuevoMMIContactOperator'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'NuevoExceptionHandling'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NuevoMMINotOk'
        DataType = ftString
        Size = 2
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 222
    Top = 210
  end
  object spActivarLista: TADOStoredProc
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 319
    Top = 211
  end
  object spCrearHistoricoPedidoEnvioActionList: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearHistoricoPedidoEnvioActionList;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@SetGreenList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetYellowList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetGrayList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetBlackList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearGreenList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearYellowList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearGrayList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearBlackList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ExceptionHandling'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@MMIContactOperator'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 288
    Top = 208
  end
end
