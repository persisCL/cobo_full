object frmEntregarATransportes: TfrmEntregarATransportes
  Left = 371
  Top = 103
  Width = 500
  Height = 550
  BorderIcons = [biSystemMenu]
  Caption = 'Entregar a Transportes'
  Color = clBtnFace
  Constraints.MaxHeight = 550
  Constraints.MaxWidth = 500
  Constraints.MinHeight = 550
  Constraints.MinWidth = 500
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    492
    516)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 9
    Width = 66
    Height = 13
    Caption = 'Transporte:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 9
    Top = 144
    Width = 94
    Height = 13
    Caption = 'Lista de Entrega'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btn_aceptar: TDPSButton
    Left = 326
    Top = 485
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 3
    OnClick = btn_aceptarClick
  end
  object btn_cancelar: TDPSButton
    Left = 406
    Top = 485
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
    OnClick = btn_cancelarClick
  end
  object GroupBox1: TGroupBox
    Left = 9
    Top = 33
    Width = 472
    Height = 103
    Caption = ' Datos de Entrega '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 100
      Height = 13
      Caption = 'Almac'#233'n Destino:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 12
      Top = 78
      Width = 94
      Height = 13
      Caption = 'Gu'#237'a Despacho:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 12
      Top = 21
      Width = 94
      Height = 13
      Caption = 'Almac'#233'n Origen:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbOrigen: TVariantComboBox
      Left = 156
      Top = 18
      Width = 301
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbOrigenChange
      Items = <>
    end
    object cbDestino: TVariantComboBox
      Left = 156
      Top = 45
      Width = 301
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object txtGuiaDespacho: TEdit
      Left = 156
      Top = 75
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      TabOrder = 2
    end
  end
  object cbTransportistas: TVariantComboBox
    Left = 82
    Top = 6
    Width = 267
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  inline frameMovimientoStock: TframeMovimientoStock
    Left = 9
    Top = 159
    Width = 472
    Height = 319
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    inherited vleIngresados: TValueListEditor
      Left = 163
      Top = 166
    end
    inherited vleRecibidos: TValueListEditor
      Left = 209
      Top = 175
    end
    inherited dbgRecibirTags: TDBGrid
      Width = 472
      Height = 94
      Align = alTop
      Columns = <
        item
          Expanded = False
          FieldName = 'Descripcion'
          ReadOnly = True
          Title.Caption = 'Categor'#237'a'
          Width = 223
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'CantidadTAGsDisponibles'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'Telev'#237'as Disponibles'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CantidadTags'
          Title.Caption = 'Cantidad de Telev'#237'as'
          Width = 110
          Visible = True
        end>
    end
    inherited dbgTags: TDBGrid
      Top = 94
      Width = 472
      Height = 225
      Align = alClient
    end
    inherited dsTAGs: TDataSource
      Left = 127
      Top = 162
    end
    inherited cdsTAGs: TClientDataSet
      Left = 92
      Top = 165
    end
    inherited spRegistrarMovimientoStock: TADOStoredProc
      Left = 162
      Top = 162
    end
  end
  object btnBorrarLinea: TDPSButton
    Left = 245
    Top = 484
    Anchors = [akRight, akBottom]
    Caption = '&Borrar l'#237'nea'
    TabOrder = 5
    OnClick = btnBorrarLineaClick
  end
  object qryOrigen: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOperacion'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = 1
      end>
    SQL.Strings = (
      'select '
      '  MaestroAlmacenes.CodigoAlmacen, MaestroAlmacenes.Descripcion '
      'from '
      '  AlmacenesOrigenOperacion WITH (NOLOCK), MaestroAlmacenes WITH (NOLOCK) '
      'where '
      '  MaestroAlmacenes.SoloNominado = 0'
      
        '  AND AlmacenesOrigenOperacion.codigoAlmacen = MaestroAlmacenes.' +
        'CodigoAlmacen'
      
        '  AND AlmacenesOrigenOperacion.CodigoOperacion = :CodigoOperacio' +
        'n')
    Left = 273
    Top = 48
  end
  object qryDestino: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    Left = 272
    Top = 77
  end
  object spGenerarGuiasDespacho: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarGuiasDespacho;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenTransporte'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 309
    Top = 114
  end
end
