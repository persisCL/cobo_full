{********************************** File Header ********************************
File Name   :
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------

*******************************************************************************}

unit frmRptAnexoEntregas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppPrnabl, ppClass, ppCtrls, ppBands, ppCache, UtilRB, ppEndUsr,
  ppComm, ppRelatv, ppProd, ppReport, ppDB, ppDBPipe, DB, ADODB,
  ppParameter, ppModule, raCodMod, ppVar, ppStrtch, ppSubRpt;

type
  TFormRptAnexoEntregas = class(TForm)
    ppReporteAnexoEntregaATransporte: TppReport;
	rbiListado: TRBInterface;
    dsEntregas: TDataSource;
    ppDBPipeline1: TppDBPipeline;
    ppParameterList1: TppParameterList;
    qryEntregas: TADOQuery;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel13: TppLabel;
    ppDBText9: TppDBText;
    ppDBText11: TppDBText;
    ppLabel15: TppLabel;
    ppDBText14: TppDBText;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppLabel7: TppLabel;
    ppLine3: TppLine;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppDBText7: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppFooterBand1: TppFooterBand;
    pplblUsuario: TppLabel;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppDBText8: TppDBText;
    ppDBText10: TppDBText;
    ppDBText12: TppDBText;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppDBPipeline2: TppDBPipeline;
    ppDBText13: TppDBText;
    ppLabel14: TppLabel;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppLabel16: TppLabel;
    ppLine6: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
	function Inicializar (CodigoGuiaDespacho: LongInt; DetalleTAGs: TDataSource): Boolean;
	function Ejecutar:Boolean;
    procedure ppReporteAnexoEntregaATransporteBeforePrint(Sender: TObject);
  private
    { Private declarations }
    FCodigoGuiaDespacho : LongInt;
  public
    { Public declarations }
  end;

var
  FormRptAnexoEntregas: TFormRptAnexoEntregas;

implementation

uses DMConnection, freMovimientoStock, EntregarATransportes;

{$R *.dfm}

function TFormRptAnexoEntregas.Inicializar (CodigoGuiaDespacho: LongInt; DetalleTAGs: TDataSource): Boolean;
begin
    FCodigoGuiaDespacho := CodigoGuiaDespacho;
    ppDBPipeline2.DataSource := DetalleTAGs;

    result := true;
end;

function TFormRptAnexoEntregas.Ejecutar:Boolean;
begin
	 rbiListado.Execute(True);
	 result := true;
end;

procedure TFormRptAnexoEntregas.ppReporteAnexoEntregaATransporteBeforePrint(Sender: TObject);
begin
	with qryEntregas, Parameters do begin
    	close;                                                                              
    	ParamByName ('CodigoGuiaDespacho').Value:= FCodigoGuiaDespacho;
        Open
    end;
end;

end.
