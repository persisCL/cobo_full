object frmEntregarATransportesCP: TfrmEntregarATransportesCP
  Left = 202
  Top = 98
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Entregar a Transportes'
  ClientHeight = 659
  ClientWidth = 808
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  DesignSize = (
    808
    659)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 9
    Width = 66
    Height = 13
    Caption = 'Transporte:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 9
    Top = 284
    Width = 94
    Height = 13
    Caption = 'Lista de Entrega'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 9
    Top = 141
    Width = 95
    Height = 13
    Caption = 'Lista de Pedidos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 9
    Top = 36
    Width = 580
    Height = 103
    Caption = ' Datos de Entrega '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 100
      Height = 13
      Caption = 'Almac'#233'n Destino:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 12
      Top = 78
      Width = 94
      Height = 13
      Caption = 'Gu'#237'a Despacho:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 12
      Top = 21
      Width = 94
      Height = 13
      Caption = 'Almac'#233'n Origen:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbOrigen: TVariantComboBox
      Left = 156
      Top = 18
      Width = 412
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbOrigenChange
      Items = <>
    end
    object cbDestino: TVariantComboBox
      Left = 156
      Top = 45
      Width = 412
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbDestinoChange
      Items = <>
    end
    object txtGuiaDespacho: TEdit
      Left = 156
      Top = 75
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      TabOrder = 2
    end
  end
  object cbTransportistas: TVariantComboBox
    Left = 82
    Top = 6
    Width = 315
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  inline frameMovimientoStock: TframeMovimientoStock
    Left = 9
    Top = 300
    Width = 791
    Height = 323
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 9
    ExplicitTop = 300
    ExplicitWidth = 791
    ExplicitHeight = 323
    inherited vleIngresados: TValueListEditor
      Left = 163
      Top = 166
      ExplicitLeft = 163
      ExplicitTop = 166
    end
    inherited vleRecibidos: TValueListEditor
      Left = 209
      Top = 175
      ExplicitLeft = 209
      ExplicitTop = 175
    end
    inherited pnlEntradaDatos: TPanel
      Top = 227
      Width = 791
      ExplicitTop = 227
      ExplicitWidth = 791
      inherited gbAgregaTelevias: TGroupBox
        Width = 787
        ExplicitWidth = 787
        inherited Label1: TLabel
          Left = 210
          Width = 67
          ExplicitLeft = 210
          ExplicitWidth = 67
        end
        inherited Label2: TLabel
          Left = 335
          Width = 62
          ExplicitLeft = 335
          ExplicitWidth = 62
        end
        inherited Label3: TLabel
          Left = 473
          Width = 42
          ExplicitLeft = 473
          ExplicitWidth = 42
        end
        inherited Label4: TLabel
          Left = 531
          Width = 104
          ExplicitLeft = 531
          ExplicitWidth = 104
        end
        inherited lbl1: TLabel
          Left = 679
          ExplicitLeft = 679
        end
        inherited dbeTeleviaInicial: TDBEdit
          Left = 188
          ExplicitLeft = 188
        end
        inherited dbeTeleviaFinal: TDBEdit
          Left = 306
          ExplicitLeft = 306
        end
        inherited dbeCantidad: TDBEdit
          Left = 424
          ExplicitLeft = 424
        end
        inherited dbeCategoria: TDBEdit
          Left = 522
          Width = 114
          ExplicitLeft = 522
          ExplicitWidth = 114
        end
        inherited btnGrabar: TButton
          Left = 614
          TabOrder = 5
          OnClick = frameMovimientoStockbtnGrabarClick
          ExplicitLeft = 614
        end
        inherited btnCancelar: TButton
          Left = 692
          TabOrder = 6
          ExplicitLeft = 692
        end
        inherited dbeCategoriaUrbana: TDBEdit
          Left = 637
          TabOrder = 4
          ExplicitLeft = 637
        end
      end
    end
    inherited Panel1: TPanel
      Width = 791
      Height = 227
      ExplicitWidth = 791
      ExplicitHeight = 227
      inherited lblDetalle: TLabel
        Top = 138
        ExplicitTop = 138
      end
      inherited dbgRecibirTags: TDBGrid
        Width = 791
        Height = 103
        TitleFont.Name = 'MS Sans Serif'
        Columns = <
          item
            Expanded = False
            FieldName = 'Descripcion'
            ReadOnly = True
            Title.Caption = 'Categor'#237'a Interurbana'
            Width = 260
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DscCategoriaUrbana'
            Title.Caption = 'Categor'#237'a Urbana'
            Width = 260
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CantidadTAGsDisponibles'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'Telev'#237'as Disponibles'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CantidadTags'
            Title.Caption = 'Cantidad de Telev'#237'as'
            Width = 120
            Visible = True
          end>
      end
      inherited dbgTags: TDBGrid
        Top = 103
        Width = 791
        Height = 124
        Align = alClient
        TitleFont.Name = 'MS Sans Serif'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NumeroInicialTAG'
            Title.Alignment = taCenter
            Title.Caption = 'Telev'#237'a Inicial'
            Width = 150
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NumeroFinalTAG'
            Title.Alignment = taCenter
            Title.Caption = 'Telev'#237'a Final'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CantidadTAGs'
            Title.Alignment = taRightJustify
            Title.Caption = 'Cantidad'
            Width = 125
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CategoriaTAGs'
            Title.Alignment = taRightJustify
            Title.Caption = 'Categor'#237'a Interurbana'
            Width = 150
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CategoriaUrbanaTAGs'
            Title.Alignment = taRightJustify
            Title.Caption = 'Categor'#237'a Urbana'
            Width = 150
            Visible = True
          end>
      end
    end
    inherited spObtenerDatosPuntoEntrega: TADOStoredProc
      Left = 281
      Top = 50
    end
    inherited spVerificarTAGEnArchivos: TADOStoredProc
      Left = 391
      Top = 18
    end
    inherited spObtenerDatosRangoTAGsAlmacen: TADOStoredProc
      Left = 517
      Top = 42
    end
    inherited dsTAGs: TDataSource
      Left = 81
      Top = 147
    end
    inherited cdsTAGs: TClientDataSet
      BeforeEdit = frameMovimientoStockcdsTAGsBeforeEdit
      AfterPost = frameMovimientoStockcdsTAGsAfterPost
      AfterCancel = frameMovimientoStockcdsTAGsAfterCancel
      OnNewRecord = frameMovimientoStockcdsTAGsNewRecord
      Left = 161
      Top = 147
    end
    inherited spRegistrarMovimientoStock: TADOStoredProc
      Left = 376
      Top = 163
    end
    inherited ObtenerSaldosTAGsAlmacen: TADOStoredProc
      Left = 187
      Top = 18
    end
    inherited cdsEntrega: TClientDataSet
      FieldDefs = <
        item
          Name = 'Descripcion'
          DataType = ftString
          Size = 60
        end
        item
          Name = 'DscCategoriaUrbana'
          DataType = ftString
          Size = 60
        end
        item
          Name = 'CantidadTAGsDisponibles'
          DataType = ftLargeint
        end
        item
          Name = 'CantidadTAGs'
          DataType = ftLargeint
        end
        item
          Name = 'Categoria'
          DataType = ftInteger
        end
        item
          Name = 'CategoriaUrbana'
          DataType = ftInteger
        end>
      AfterPost = frameMovimientoStockcdsEntregaAfterPost
      Left = 99
      Top = 35
    end
    inherited dsEntrega: TDataSource
      Left = 54
      Top = 50
    end
    inherited qryCategorias: TADOQuery
      SQL.Strings = (
        'SELECT'#9'CAT.Categoria, CAT.Descripcion, '
        
          #9#9'CatUrb.CategoriaUrbana, CatUrb.Descripcion AS DscCategoriaUrba' +
          'na'
        'FROM'#9'Categorias CAT WITH (NOLOCK)'
        #9#9'LEFT JOIN CategoriasUrbanas CatUrb WITH (NOLOCK)'
        #9#9#9'ON CatUrb.CategoriaUrbana = CAT.CategoriaCambio')
      Left = 603
      Top = 18
    end
  end
  object dbgPedidos: TDBGrid
    Left = 9
    Top = 156
    Width = 793
    Height = 126
    DataSource = dsPedidos
    Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnExit = dbgPedidosExit
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CodigoOrdenPedido'
        Title.Alignment = taRightJustify
        Title.Caption = 'Orden Pedido'
        Width = 74
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descripcion'
        Title.Caption = 'Categor'#237'a Interurbana'
        Width = 196
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DscCategoriaUrbana'
        Title.Caption = 'Categor'#237'a Urbana'
        Width = 196
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'Cantidad'
        Title.Alignment = taRightJustify
        Title.Caption = 'Pedido'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CantidadDespachada'
        Title.Alignment = taRightJustify
        Title.Caption = 'Despachado'
        Width = 70
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CantidadPendiente'
        Title.Alignment = taRightJustify
        Title.Caption = 'Pendiente'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ADespachar'
        Title.Alignment = taRightJustify
        Title.Caption = 'A Despachar'
        Width = 70
        Visible = True
      end>
  end
  object btnAceptar: TButton
    Left = 644
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object btn_cancelar: TButton
    Left = 722
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
    OnClick = btn_cancelarClick
  end
  object btnBorrarLinea: TButton
    Left = 378
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Borrar l'#237'nea'
    Enabled = False
    TabOrder = 5
    OnClick = btnBorrarLineaClick
  end
  object btnAgregarLinea: TButton
    Left = 221
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&A'#241'adir L'#237'nea'
    TabOrder = 7
    OnClick = btnAgregarLineaClick
  end
  object btnEditarLinea: TButton
    Left = 299
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Editar L'#237'nea'
    TabOrder = 8
    OnClick = btnEditarLineaClick
  end
  object qryOrigen: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOperacion'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = 1
      end>
    SQL.Strings = (
      'select '
      '  MaestroAlmacenes.CodigoAlmacen, MaestroAlmacenes.Descripcion '
      'from '
      
        '  AlmacenesOrigenOperacion WITH (NOLOCK), MaestroAlmacenes WITH ' +
        '(NOLOCK) '
      'where '
      '  MaestroAlmacenes.SoloNominado = 0'
      
        '  AND AlmacenesOrigenOperacion.codigoAlmacen = MaestroAlmacenes.' +
        'CodigoAlmacen'
      
        '  AND AlmacenesOrigenOperacion.CodigoOperacion = :CodigoOperacio' +
        'n')
    Left = 273
    Top = 48
  end
  object qryDestino: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    Left = 272
    Top = 77
  end
  object spGenerarGuiasDespacho: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarGuiasDespacho;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenTransporte'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 411
    Top = 9
  end
  object dsPedidos: TDataSource
    DataSet = cdsPedidos
    Left = 99
    Top = 210
  end
  object spObtenerOrdenesPedidoPendientes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOrdenesPedidoPendientes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 279
    Top = 250
    object spObtenerOrdenesPedidoPendientesCodigoOrdenPedido: TIntegerField
      FieldName = 'CodigoOrdenPedido'
    end
    object spObtenerOrdenesPedidoPendientesCategoria: TWordField
      FieldName = 'Categoria'
    end
    object spObtenerOrdenesPedidoPendientesCategoriaUrbana: TIntegerField
      FieldName = 'CategoriaUrbana'
    end
    object spObtenerOrdenesPedidoPendientesCantidad: TIntegerField
      FieldName = 'Cantidad'
    end
    object spObtenerOrdenesPedidoPendientesDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 60
    end
    object spObtenerOrdenesPedidoPendientesDscCategoriaUrbana: TStringField
      FieldName = 'DscCategoriaUrbana'
    end
    object spObtenerOrdenesPedidoPendientesCantidadDespachada: TIntegerField
      FieldName = 'CantidadDespachada'
      ReadOnly = True
    end
    object spObtenerOrdenesPedidoPendientesCantidadPendiente: TIntegerField
      FieldName = 'CantidadPendiente'
      ReadOnly = True
    end
  end
  object cdsPedidos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoOrdenPedido'
        DataType = ftInteger
      end
      item
        Name = 'Categoria'
        DataType = ftInteger
      end
      item
        Name = 'CategoriaUrbana'
        DataType = ftInteger
      end
      item
        Name = 'Cantidad'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DscCategoriaUrbana'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CantidadDespachada'
        DataType = ftInteger
      end
      item
        Name = 'CantidadPendiente'
        DataType = ftInteger
      end
      item
        Name = 'ADespachar'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 161
    Top = 234
  end
  object spRegistrarDespachoOrdenPedido: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarDespachoOrdenPedido'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CantidadDespachada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 194
  end
end
