object FInterfaseProveedorTAG: TFInterfaseProveedorTAG
  Left = 278
  Top = 271
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  ClientHeight = 306
  ClientWidth = 586
  Color = clBtnFace
  Constraints.MaxHeight = 340
  Constraints.MaxWidth = 594
  Constraints.MinHeight = 334
  Constraints.MinWidth = 592
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 9
    Width = 109
    Height = 13
    Caption = 'Archivo a Importar:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblImportacion: TLabel
    Left = 12
    Top = 54
    Width = 72
    Height = 13
    Caption = 'Procesando:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 12
    Top = 107
    Width = 185
    Height = 13
    Caption = 'Registros en Archivo Proveedor:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 12
    Top = 131
    Width = 121
    Height = 13
    Caption = 'Registros sin errores:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 12
    Top = 155
    Width = 126
    Height = 13
    Caption = 'Registros con errores:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 12
    Top = 179
    Width = 164
    Height = 13
    Caption = 'Resultado de la Importaci'#243'n:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblVerificando: TLabel
    Left = 360
    Top = 54
    Width = 209
    Height = 35
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Label6: TLabel
    Left = 12
    Top = 84
    Width = 191
    Height = 13
    Caption = 'Propiedad del Archivo a Importar:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object pbProgreso: TProgressBar
    Left = 90
    Top = 54
    Width = 253
    Height = 17
    Min = 1
    Position = 1
    Smooth = True
    Step = 1
    TabOrder = 0
  end
  object BuscaArchivo: TBuscaTabEdit
    Left = 12
    Top = 25
    Width = 563
    Height = 21
    Hint = 'El archivo debe ser de texto sin formato'
    Enabled = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    EditorStyle = bteTextEdit
    OnButtonClick = BuscaArchivoButtonClick
    Text = 'Buscar...'
  end
  object numExitosos: TNumericEdit
    Left = 222
    Top = 128
    Width = 121
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
  end
  object numRegistros: TNumericEdit
    Left = 222
    Top = 104
    Width = 121
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
  end
  object numErroneos: TNumericEdit
    Left = 222
    Top = 152
    Width = 121
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
  end
  object memResultado: TMemo
    Left = 222
    Top = 176
    Width = 352
    Height = 98
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 6
  end
  object btnCancelar: TButton
    Left = 411
    Top = 278
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 7
    OnClick = btnCancelarClick
  end
  object btnSalir: TButton
    Left = 495
    Top = 278
    Width = 75
    Height = 25
    Caption = '&Salir'
    TabOrder = 8
    OnClick = btnSalirClick
  end
  object CbPropiedad: TComboBox
    Left = 222
    Top = 81
    Width = 121
    Height = 21
    Hint = 'Seleccione la Propiedad de los Telev'#237'as a Importar'
    AutoComplete = False
    Style = csDropDownList
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ItemHeight = 13
    MaxLength = 3
    ParentFont = False
    TabOrder = 2
    Items.Strings = (
      'MOP')
  end
  object Open: TOpenDialog
    Filter = '????-??-?? RDM.txt|*.txt'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 438
    Top = 195
  end
  object spBorrarMovimientosImportacionRecepcionTAGsNulos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BorrarMovimientosImportacionRecepcionTAGsNulos'
    Parameters = <>
    Left = 276
    Top = 3
  end
  object spRegistarLineaInterfase: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistarLineaInterfase;1'
    Parameters = <>
    Left = 388
    Top = 153
  end
  object spVerificarInterfaseTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarInterfaseTAGs;1'
    Parameters = <>
    Left = 292
    Top = 183
  end
  object spObtenerDatosAImportar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosAImportar'
    Parameters = <>
    Left = 330
    Top = 51
  end
  object spActualizarMovimientosImportacionRecepcionTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMovimientosImportacionRecepcionTAGs'
    Parameters = <>
    Left = 458
    Top = 81
  end
  object spPasarAInterfase: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PasarAInterfase'
    Parameters = <>
    Left = 490
    Top = 135
  end
  object spObtenerDetalleTAGsAImportar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDetalleTAGsAImportar'
    Parameters = <>
    Left = 464
    Top = 11
  end
end
