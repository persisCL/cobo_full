{
    File Name: Main
    Author :
    Date Created:
    Language : ES-AR

    Description :

    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a

            - Se a�aden/modifican los siguientes procedimientos/funciones:
                EntregaraTransportes1Click,
                mnu_CambiarUbicacionClick

        Author: Mcabello
        Firma: SS_660_MCO_20130316
        Date: 16/03/2013
        Description: Se elimina opcion de menu Cambiar Action List y se comenta el codigo
                    -- asociado al men�

      Author: Mcabello
        Firma: SS_660_MCO_20130610
        Date: 16/03/2013
        Description: Se Agrega opcion de menu Cambiar Action List y se descomenta el codigo
                    -- asociado al men�

      Author: Mcabello
      Firma:  SS_660_MCA_20131014
      Date:   14/10/2013
      Description: Se agrega opcion de men� Tag en Seguimiento


      Firma       :   SS_925_NDR_20131122
      Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)

      
      Firma       :   SS_925_MBE_20131223
      Description :   Se cambia la ventana de Acerca de.

      Firma       : SS_1147_NDR_20141216
      Descripcion : Color de menues segun concesionaria nativa         

    Etiqueta    : 20160315 MGO
    Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

      }
unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, Menus, ExtCtrls, IconBars, ImgList, Buttons, UtilDB,
  StdCtrls, UtilProc, ComCtrls, ToolWin, ActnList, AppEvnts, PeaTypes,
  DmConnection, util, peaprocs, login,DB, ADODB, jpeg, CambioPassword,
  DpsAbout, CacProcs, RStrings, frmInterfaseProveedorTAG, CambiarUbicaciones,
  EntregarATransportes, frmTeleviasImportados, frmSaldosAlmacenesTAGs, XPMan,
  CustomAssistance, frmTagEnSeguimiento;	// SS_660_MCA_20131014

type
  TMainForm = class(TForm)
    PageImagenes: TImageList;
    MainMenu: TMainMenu;
    mnu_sistema: TMenuItem;
    mnu_salir: TMenuItem;
    StatusBar: TStatusBar;
    HotLinkImagenes: TImageList;
    MenuImagenes: TImageList;
    ImagenFondo: TImage;
    Recepcion1: TMenuItem;
    Ayuda1: TMenuItem;
    Acercade1: TMenuItem;
    mnu_CambiarUbicacion: TMenuItem;
    Configuracion1: TMenuItem;
    mnu_Ventana: TMenuItem;
    mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    N2: TMenuItem;
    mnu_Siguiente: TMenuItem;
    mnu_Anterior: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambiodeContrasena: TMenuItem;
    InterfaseProveedor1: TMenuItem;
    Recepcin1: TMenuItem;
    ControldeCalidad1: TMenuItem;
    EntregaraTransportes1: TMenuItem;
    Movimientos1: TMenuItem;
    mnu_RecibirdeTransportes: TMenuItem;
    VerTelevaspendientesdeingresaraMaestro1: TMenuItem;
    VerSaldosenAlmacenes1: TMenuItem;
    ConfiguracindeInicio1: TMenuItem;
    mnuStockPuntosEntrega: TMenuItem;
    mnuPedidosPendientes: TMenuItem;
    mnuIdentificarTelevia: TMenuItem;
    mnuCambiarEstadoTAG: TMenuItem;
    mnu_Ver: TMenuItem;
    mnu_IconBar: TMenuItem;
    mnu_BarraDeNavegacion: TMenuItem;
    mnu_BarradeHotLinks: TMenuItem;
    XPManifest1: TXPManifest;
    HistorioListTelevia: TMenuItem;
    mnuCambiarActionList: TMenuItem;
    mnuTagEnSeguimiento: TMenuItem;
    Informes1: TMenuItem;	// SS_660_MCA_20131014
    procedure HistorioListTeleviaClick(Sender: TObject);
    procedure mnu_salirClick(Sender: TObject);
    procedure Cerrar1Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure mnu_CambiarUbicacionClick(Sender: TObject);
    procedure Acercade1Click(Sender: TObject);
    procedure mnu_CascadaClick(Sender: TObject);
    procedure mnu_CambiodeContrasenaClick(Sender: TObject);
    procedure mnu_VentanaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure InterfaseProveedor1Click(Sender: TObject);
    procedure Recepcin1Click(Sender: TObject);
    procedure ControldeCalidad1Click(Sender: TObject);
    procedure EntregaraTransportes1Click(Sender: TObject);
    procedure mnu_RecibirdeTransportesClick(Sender: TObject);
    procedure VerTelevaspendientesdeingresaraMaestro1Click(
      Sender: TObject);
    procedure VerSaldosenAlmacenes1Click(Sender: TObject);
    procedure ConfiguracindeInicio1Click(Sender: TObject);
    procedure mnuPedidosPendientesClick(Sender: TObject);
    procedure mnuIdentificarTeleviaClick(Sender: TObject);
    procedure mnuStockPuntosEntregaClick(Sender: TObject);
    procedure mnuCambiarEstadoTAGClick(Sender: TObject);
    procedure mnuCambiarActionListClick(Sender: TObject);                     
    procedure mnu_IconBarClick(Sender: TObject);
    procedure mnu_BarraDeNavegacionClick(Sender: TObject);
    procedure mnu_BarradeHotLinksClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mnuTagEnSeguimientoClick(Sender: TObject);	// SS_660_MCA_20131014
    function CambiarEventoMenu(Menu: TMenu): Boolean;                           //SS_1147_NDR_20141216
  private
	{ Private declarations }
	AlmacenTAGs: integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                //SS_1147_NDR_20141216
	procedure RefrescarBarraDeEstado(StatusBar: TStatusBar);

    procedure RefrescarEstadosIconBar;
    procedure InicializarIconBar;
    procedure GuardarUserSettings;
  public
	{ Public declarations }
	function Inicializar: Boolean;
    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas;ARect: TRect; Selected: Boolean);             //SS_1147_NDR_20141216
  end;

var
  MainForm: TMainForm;


implementation

uses frmControlCalidadPallets, FrmRecibirTagsNominados, GeneradorDeIni,
  frmRptStockPuntosEntrega, frmOrdenesPedidoPendientes, frmRecepcionTAGs,
  EntregarATransportesConPedidos, frmIdentificarTAG, frmListaPuntosEntrega,
  frmCambiosActionList, FrmHistoricoListTag, frmAcercaDe;

resourcestring
    PAGINA_RECEPCION        = 'Recepcion';
    PAGINA_MOVIMIENTOS      = 'Movimientos';
    PAGINA_MANTENIMIENTO    = 'Mantenimiento';
    
var
	Salir: Boolean;

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}   

function TMainForm.Inicializar: Boolean;
var
	f: TLoginForm;
    ok: boolean;
begin
	// Navegador
    SetBounds(Screen.WorkAreaLeft, Screen.WorkAreaTop, Screen.WorkAreaWidth, Screen.WorkAreaHeight);
    Application.Title := SYS_GESTION_TAGS_TITLE;
    SistemaActual := SYS_GESTION_TAGS;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    CambiarEventoMenu(MainMenu);                                                                            //SS_1147_NDR_20141216

    try
        Application.CreateForm(TLoginForm, f);
        if f.Inicializar(DMConnections.BaseCAC, SYS_GESTION_TAGS) and (f.ShowModal = mrOk) then begin
            UsuarioSistema := f.CodigoUsuario;

            //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
            //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
            //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
            //    Application.Terminate;
            //end;
            //
            //ok := true;
            //// Men�es
            //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin
            //    ok := GenerateMenuFile(MainMenu, SYS_GESTION_TAGS, DMConnections.BaseCAC, []);
            //end;

            if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, MainMenu, []) then begin
            	//INICIO	: 20160624 CFU
                Result	:= False;
                //TERMINO	: 20160624 CFU
                MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            ok := True;
            //END : SS_925_NDR_20131122-----------------------------------------------------------------------

            Salir := False;
            if ok then begin
                //INICIO	: 20161021 CFU TASK_059_CFU_20161021-TAGs_Correccion_Almacen_Barra_De_Estado
                AlmacenTAGs := InstallIni.ReadInteger('General', 'AlmacenOrigen', -1);
                //TERMINO	: 20161021 CFU TASK_059_CFU_20161021-TAGs_Correccion_Almacen_Barra_De_Estado
                RefrescarBarraDeEstado(StatusBar);

                CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_GESTION_TAGS);
                HabilitarPermisosMenu(Menu);

                CargarInformes(DMConnections.BaseCAC, SYS_GESTION_TAGS, MainMenu);
                CambiarEventoMenu(MainMenu);                                    //SS_1147_NDR_20141216
                Result := True;
            end else begin
                Result := False;
            end;
        end
        else result := False;

        if not Result then Exit;

        if Result then InicializarIconBar;

        { INICIO : 20160315 MGO
        AlmacenTAGs := ApplicationIni.ReadInteger('General', 'AlmacenOrigen', -1);
        }
        {INICIO	: 20161021 CFU TASK_059_CFU_20161021-TAGs_Correccion_Almacen_Barra_De_Estado
        AlmacenTAGs := InstallIni.ReadInteger('General', 'AlmacenOrigen', -1);
        TERMINO	: 20160624 CFU TASK_059_CFU_20161021-TAGs_Correccion_Almacen_Barra_De_Estado} 
        // FIN : 20160315 MGO
    finally
        f.Release;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TMainForm.RefrescarBarraDeEstado(StatusBar: TStatusBar);
var
	UltimoPanel: Integer;
begin
	StatusBar.Panels.Items[0].Text  := Format(MSG_USER, [UsuarioSistema]);
    StatusBar.Panels[0].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[0].text));
	StatusBar.Panels[1].Text := Trim(Format(MSG_CONECTADO_A, [InstallIni.ReadString('Database CAC', 'Server', ''), InstallIni.ReadString('Database CAC', 'DatabaseName', '')]));
	StatusBar.Panels[1].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[1].text));
	UltimoPanel := StatusBar.Panels.Count-1;
	StatusBar.Panels[UltimoPanel].Text := iif(AlmacenTAGs = 0,' Almac�n Central CN',format(' %s (%s)',[Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MaestroAlmacenes WITH (NOLOCK) WHERE CodigoAlmacen = ' + IntToStr(AlmacenTAGs))),inttoStr(AlmacenTAGs)]));
	StatusBar.Panels[UltimoPanel].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[UltimoPanel].text));
end;

procedure TMainForm.mnu_salirClick(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.Cerrar1Click(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
    DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
    DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

{
    Revision : 1
        Author: pdominguez
        Date: 30/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se introduce un bloque Try Finally, para asegurar la liberaci�n
            del Form y los recursos que consume.
}
procedure TMainForm.mnu_CambiarUbicacionClick(Sender: TObject);
var
    f:TfrmCambiarUbicaciones;
begin
    try
        Application.CreateForm(TfrmCambiarUbicaciones,f);
        if f.Inicializa(false, mnu_CambiarUbicacion.caption, 0, AlmacenTAGs) then f.ShowModal;
    finally
        if Assigned(f) then FreeAndNil(f);
    end;
end;


{******************************** Function Header ******************************
Function Name: Acercade1Click
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None
Revision : 1
    Author : ggomez
    Date : 29/01/2007
    Description : En la llamada f.Inicializa, agregu� el segundo par�metro ''
        debido a que cambi� el perfil de la funci�n en las Utiles.
*******************************************************************************}
procedure TMainForm.Acercade1Click(Sender: TObject);
{                                                       SS_925_MBE_20131223
resourcestring
    CAPTION_ACERCADE = 'Atenci�n al Usuario';
var
    f : TAboutFrm;
    a: TfrmLocalAssistance;
}
begin
{                                                       SS_925_MBE_20131223
    Application.CreateForm(TfrmLocalAssistance, a);
    try
        if a.Inicializar then begin
            Application.CreateForm(TAboutFrm, f);
            if f.Inicializa(CAPTION_ACERCADE, '', '', a) then f.ShowModal;
            f.Release;
        end;
    finally
        a.Release;
    end; // finally
}
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;

procedure TMainForm.mnu_CascadaClick(Sender: TObject);
begin
    if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

procedure TMainForm.mnu_CambiodeContrasenaClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
    if not mnu_cambiodecontrasena.Enabled then exit;
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DMConnections.BaseCac,UsuarioSistema) then f.ShowModal;
	f.Release;
end;

procedure TMainForm.mnu_VentanaClick(Sender: TObject);
begin
	mnu_cascada.Enabled := GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled := mnu_cascada.Enabled;
	mnu_anterior.Enabled := GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled := mnu_anterior.Enabled;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 //   ChequearTareasBloqueadas(DMConnections.BaseCAC);
    (* Guardar las opciones de configuraci�n del usuario. *)
    GuardarUserSettings;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
	{$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF}
end;

procedure TMainForm.InterfaseProveedor1Click(Sender: TObject);
var
    f: TFInterfaseProveedorTAG;
begin
	if FindFormOrCreate(TFInterfaseProveedorTAG, f) then
		f.Show
	else begin
		if not f.Inicializar(InterfaseProveedor1.Caption, True) then f.Release;
	end
end;

procedure TMainForm.Recepcin1Click(Sender: TObject);
var
    f: TFRecepcionTAGs;
begin
	if FindFormOrCreate(TFRecepcionTAGs, f) then
		f.Show
	else begin
		if not f.Inicializar(Recepcin1.Caption, True) then f.Release;
	end
end;

procedure TMainForm.ControldeCalidad1Click(Sender: TObject);
var
    f: TFControlCalidadPallets;
begin
	if FindFormOrCreate(TFControlCalidadPallets, f) then
		f.Show
	else begin
        if not f.Inicializar(ControldeCalidad1.Caption, True) then f.Release;
	end;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 30/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se introduce un bloque Try Finally, para asegurar la liberaci�n
            del Form y los recursos que consume.
}
procedure TMainForm.EntregaraTransportes1Click(Sender: TObject);
var
    f:TfrmEntregarATransportesCP;
begin
    try
        Application.CreateForm(TfrmEntregarATransportesCP, f);
        if f.InicializaR(False, EntregaraTransportes1.caption, AlmacenTAGs) then f.ShowModal;
    finally
        if Assigned(f) Then FreeAndNil(f);
    end;
end;

procedure TMainForm.mnu_RecibirdeTransportesClick(Sender: TObject);
var
    f: TFormRecibirTagsNominados;
begin
    Application.CreateForm(TFormRecibirTagsNominados, f);
    if (f.Inicializar(LimpiarCaracteresLabel(mnu_RecibirdeTransportes.Caption),UsuarioSistema, -1, false, AlmacenTAGs)) then
        f.ShowModal;
    f.Release;
end;

procedure TMainForm.VerTelevaspendientesdeingresaraMaestro1Click(
  Sender: TObject);
var
    f:TFTAGsImportados;
begin
	if FindFormOrCreate(TFTAGsImportados, f) then
		f.Show
	else begin
        if not f.Inicializar(True) then f.Release;
	end;
end;

procedure TMainForm.VerSaldosenAlmacenes1Click(Sender: TObject);
var
    f:TFSaldosAlmacenesTAGs;
begin
	if FindFormOrCreate(TFSaldosAlmacenesTAGs, f) then
		f.Show
	else begin
		if not f.Inicializar(VerSaldosenAlmacenes1.Caption, True) then f.Release;
	end
end;

procedure TMainForm.ConfiguracindeInicio1Click(Sender: TObject);
var
    f : TFormGeneradorIni;
begin
	Application.CreateForm(TFormGeneradorIni, f);
    if (f.Inicializa and (f.ShowModal = mrOk)) then AlmacenTAGs := f.AlmacenOrigen;
    f.Release;
end;

procedure TMainForm.mnuPedidosPendientesClick(Sender: TObject);
var
	f:TFormOrdenesPedidoPendientes;
begin
	if FindFormOrCreate(TFormOrdenesPedidoPendientes, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuPedidosPendientes.Caption, True) then f.Release;
	end
end;

procedure TMainForm.mnuIdentificarTeleviaClick(Sender: TObject);
var
    f: TFormIdentificarTAG;
begin
	if FindFormOrCreate(TFormIdentificarTAG, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuIdentificarTelevia.Caption, True, False) then f.Release;
	end
end;

procedure TMainForm.mnuStockPuntosEntregaClick(Sender: TObject);
var
	f: TFormRptStockPuntosEntrega;
begin
	Application.CreateForm(TFormRptStockPuntosEntrega, f);
	if not f.Inicializar then exit;
	if f.Ejecutar then f.Free;
end;

//INICIO SS_660_MCA_20131014
procedure TMainForm.mnuTagEnSeguimientoClick(Sender: TObject);
var
	f: TTagEnSeguimientoForm;
begin
	if FindFormOrCreate(TTagEnSeguimientoForm, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuTagEnSeguimiento.Caption, True) then f.Release;
	end

end;
//FIN SS_660_MCA_20131014

procedure TMainForm.mnuCambiarEstadoTAGClick(Sender: TObject);
var
	f: TFormIdentificarTAG;
begin
	if FindFormOrCreate(TFormIdentificarTAG, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuCambiarEstadoTAG.Caption, True, True) then f.Release;
	end
end;

//INICIO SS_660_MCA_20130610
procedure TMainForm.mnuCambiarActionListClick(Sender: TObject);
var
	f: TformCambiosActionList;
begin
	if FindFormOrCreate(TformCambiosActionList, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuCambiarActionList.Caption, True) then f.Release;
	end
end;
//FIN SS_660_MCA_20130610

{-----------------------------------------------------------------------------
  Procedure: TMainForm.InicializarIconBar
  Author:    ggomez
  Date:      21-Jun-2005
  Arguments: None
  Result:    None
  Description: Inicializa la barra de �conos.
-----------------------------------------------------------------------------}
procedure TMainForm.InicializarIconBar;
var
    Bmp: TBitmap;
    NavigatorHasPages : Boolean;
begin
    //------------------------------------------------------------------------
	// Navegador
    //------------------------------------------------------------------------
	Bmp := TBitmap.Create;
	GNavigator := TNavigator.Create(Self);

    GNavigator.ShowBrowserBar       := GetUserSettingShowBrowserBar;
    mnu_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;

    GNavigator.ShowHotLinkBar   := GetUserSettingShowHotLinksBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;

    GNavigator.IconBarVisible   := False;
    mnu_IconBar.Checked         := GNavigator.IconBarVisible;
    NavigatorHasPages           := False;

	//----------------------------------------------------------
	// Agregamos las p�ginas e �conos HABILITADAS al toolbar
	//----------------------------------------------------------

    // RECEPCI�N
    if ExisteAcceso('InterfaseProveedor1') or ExisteAcceso('Recepcin1')
            or ExisteAcceso('ControldeCalidad1') then begin

        GNavigator.AddPage(PAGINA_RECEPCION, MSG_RECEPCION_TAGS, EmptyStr);
        NavigatorHasPages := True;

        if ExisteAcceso('InterfaseProveedor1') then begin
            PageImagenes.GetBitmap(7, Bmp);
            GNavigator.AddIcon(PAGINA_RECEPCION, MSG_IMPORTAR_ARCHIVO_PROVEEDOR, EmptyStr, Bmp, InterfaseProveedor1.OnClick);
            Bmp.Assign(nil);
        end;

        if ExisteAcceso('Recepcin1') then begin
            PageImagenes.GetBitmap(10, Bmp);
            GNavigator.AddIcon(PAGINA_RECEPCION, MSG_RECIBIR_ENVIOS_TAGS, EmptyStr, Bmp, Recepcin1.OnClick);
            Bmp.Assign(nil);
        end;

        if ExisteAcceso('ControldeCalidad1') then begin
            PageImagenes.GetBitmap(5, Bmp);
            GNavigator.AddIcon(PAGINA_RECEPCION, MSG_CONTROL_CALIDAD, EmptyStr, Bmp, ControldeCalidad1.OnClick);
            Bmp.Assign(nil);
        end;
    end;

    // MOVIMIENTOS
    if ExisteAcceso('EntregaraTransportes1') or ExisteAcceso('mnu_CambiarUbicacion') then begin
        GNavigator.AddPage(PAGINA_MOVIMIENTOS, MSG_MOVIMIENTOS_TAGS, EmptyStr);
        NavigatorHasPages := True;

        if ExisteAcceso('EntregaraTransportes1') then begin
            PageImagenes.GetBitmap(6, Bmp);
            GNavigator.AddIcon(PAGINA_MOVIMIENTOS, MSG_ENTREGAR_TAGS_A_TRANSPORTES, EmptyStr, Bmp, EntregaraTransportes1.OnClick);
            Bmp.Assign(nil);
        end;

        if ExisteAcceso('mnu_CambiarUbicacion') then begin
            PageImagenes.GetBitmap(4, Bmp);
            GNavigator.AddIcon(PAGINA_MOVIMIENTOS, MSG_CAMBIAR_UBICACION_TAGS, EmptyStr, Bmp, mnu_CambiarUbicacion.OnClick);
            Bmp.Assign(nil);
        end;
    end;

    // MANTENIMIENTO
    if ExisteAcceso('mnu_CambiodeContrasena') then begin
		GNavigator.AddPage(PAGINA_MANTENIMIENTO, STR_MANTENIMIENTO, EmptyStr);

        if ExisteAcceso('mnu_CambiodeContrasena') then begin
            PageImagenes.GetBitmap(2, Bmp);
            GNavigator.AddIcon(PAGINA_MANTENIMIENTO, MSG_CAMBIO_CONTRASENIA, EmptyStr, Bmp, mnu_CambiodeContrasena.OnClick);
            Bmp.Assign(nil);
        end;
    end;

	if NavigatorHasPages then GNavigator.IconBarActivePage := 0;

	Bmp.Free;
	Update;

    RefrescarEstadosIconBar;

    GNavigator.IconBarVisible   := GetUserSettingShowIconBar;
    mnu_IconBar.Checked		    := GNavigator.IconBarVisible;
    GNavigator.ConnectionCOP    := Nil;
    GNavigator.ConnectionCAC    := DMConnections.BaseCAC;

end;

{-----------------------------------------------------------------------------
  Function Name: RefrescarEstadosIconBar
  Author:    ggomez
  Date Created: 21/06/2005
  Description: Refresca los Estados de la IconBar
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.RefrescarEstadosIconBar;
begin
    if (GNavigator = Nil) then Exit;

    GNavigator.SetIconEnabled(PAGINA_RECEPCION, MSG_IMPORTAR_ARCHIVO_PROVEEDOR, InterfaseProveedor1.Enabled);
    GNavigator.SetIconEnabled(PAGINA_RECEPCION, MSG_RECIBIR_ENVIOS_TAGS, Recepcin1.Enabled);
    GNavigator.SetIconEnabled(PAGINA_RECEPCION, MSG_CONTROL_CALIDAD, ControldeCalidad1.Enabled);

    GNavigator.SetIconEnabled(PAGINA_MOVIMIENTOS, MSG_ENTREGAR_TAGS_A_TRANSPORTES, EntregaraTransportes1.Enabled);
    GNavigator.SetIconEnabled(PAGINA_MOVIMIENTOS, MSG_CAMBIAR_UBICACION_TAGS, mnu_CambiarUbicacion.Enabled);

    GNavigator.SetIconEnabled(PAGINA_MANTENIMIENTO, MSG_CAMBIO_CONTRASENIA, mnu_CambiodeContrasena.Enabled);

end;

procedure TMainForm.mnu_IconBarClick(Sender: TObject);
begin
	GNavigator.IconBarVisible	:= not GNavigator.IconBarVisible;
    mnu_IconBar.Checked			:= GNavigator.IconBarVisible;
end;

procedure TMainForm.mnu_BarraDeNavegacionClick(Sender: TObject);
begin
	GNavigator.ShowBrowserBar       := not GNavigator.ShowBrowserBar;
    mnu_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;
end;

procedure TMainForm.mnu_BarradeHotLinksClick(Sender: TObject);
begin
	GNavigator.ShowHotLinkBar   := not GNavigator.ShowHotLinkBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.GuardarUserSettings
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: None
  Result:    None
  Description: Guarda las opciones de configuraci�n para el usuario logueado
    en el Sistema Operativo.
-----------------------------------------------------------------------------}
procedure TMainForm.GuardarUserSettings;
begin
    SetUserSettingShowIconBar(mnu_IconBar.Checked);
    SetUserSettingShowBrowserBar(mnu_BarraDeNavegacion.Checked);
    SetUserSettingShowHotLinksBar(mnu_BarraDeHotLinks.Checked);
end;

procedure TMainForm.HistorioListTeleviaClick(Sender: TObject);
var
	f: TfrmHistoricoListTagForm;
begin
	if FindFormOrCreate(TfrmHistoricoListTagForm, f) then
		f.Show
	else begin
	 	//if not f.Inicializar(mnuCambiarActionList.Caption, True) then f.Release;	// SS_660_MCO_20130610
		if not f.Inicializar(HistorioListTelevia.Caption, True) then f.Release;		// SS_660_MCO_20130610
	end;
end;

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
procedure TMainForm.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
var
  S: String;
begin
  with ACanvas do
  begin
    S := TMenuItem(Sender).Caption;
    if Selected then
    begin
        Brush.Color := FColorMenuSel;
        Font.Color := FColorFontSel;
    end
    else
    begin
        if TMenuItem(Sender).Enabled then
        begin
            Brush.Color := FColorMenu;
            Font.Color := FColorFont;
            Font.Style:=[fsBold];
        end
        else
        begin
            Brush.Color := FColorMenu;
            Font.Color := clGrayText;
            Font.Style:=[];
        end;
    end;


    if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 6 ) and not Selected then
        ARect.Right := Width;

    FillRect(ARect);
    DrawText(ACanvas.Handle, PChar(S), Length(S), ARect, DT_SINGLELINE or DT_VCENTER);

    if S='-' then
    begin
      with ACanvas do
      begin
        ACanvas.MoveTo(ARect.Left,ARect.top + 4);
        ACanvas.Pen.Color := FColorFont;
        ACanvas.LineTo(ARect.Right, ARect.top + 4 );
      end;
    end;

  end;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
function TMainForm.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------


end.

