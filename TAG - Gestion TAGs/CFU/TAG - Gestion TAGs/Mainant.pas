{********************************** File Header ********************************
File Name   :
Author      :
Date Created:
Language    : ES-AR
Description : Pantalla ppal. de Gesti�n de Telev�as
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Author      : Castro, Ra�l
Date Created: 28/05/04
Description : Agregado de opciones de men� - Redise�o de men� ppal.
*******************************************************************************}

unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, Menus, ExtCtrls, IconBars, ImgList, Buttons, UtilDB,
  StdCtrls, UtilProc, ComCtrls, ToolWin, ActnList, AppEvnts, PeaTypes,
  DmConnection, util, peaprocs, login,DB, ADODB, jpeg, CambioPassword,
  DpsAbout, CacProcs, RStrings, frmInterfaseProveedorTAG, CambiarUbicaciones,
  EntregarATransportes, frmTeleviasImportados, frmSaldosAlmacenesTAGs;

type
  TMainForm = class(TForm)
    PageImagenes: TImageList;
    MainMenu: TMainMenu;
    mnu_sistema: TMenuItem;
    mnu_salir: TMenuItem;
    StatusBar: TStatusBar;
    tmr_llamada: TTimer;
    HotLinkImagenes: TImageList;
    MenuImagenes: TImageList;
    ImagenFondo: TImage;
    Recepcion1: TMenuItem;
    Ayuda1: TMenuItem;
    Acercade1: TMenuItem;
    mnu_CambiarUbicacion: TMenuItem;
    Configuracion1: TMenuItem;
    mnu_Ventana: TMenuItem;
    mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    N2: TMenuItem;
    mnu_Siguiente: TMenuItem;
    mnu_Anterior: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambiodeContrasena: TMenuItem;
    InterfaseProveedor1: TMenuItem;
    Recepcin1: TMenuItem;
    ControldeCalidad1: TMenuItem;
    EntregaraTransportes1: TMenuItem;
    Movimientos1: TMenuItem;
    mnu_RecibirdeTransportes: TMenuItem;
    VerTelevaspendientesdeingresaraMaestro1: TMenuItem;
    VerSaldosenAlmacenes1: TMenuItem;
    ConfiguracindeInicio1: TMenuItem;
    mnuStockPuntosEntrega: TMenuItem;
    mnuPedidosPendientes: TMenuItem;
    mnuIdentificarTelevia: TMenuItem;
    mnuRegistrarRoboTelevia: TMenuItem;
    mnu_informes: TMenuItem;
    mnuCambiarEstadoTAG: TMenuItem;
    procedure mnu_salirClick(Sender: TObject);
    procedure Cerrar1Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure mnu_CambiarUbicacionClick(Sender: TObject);
    procedure Acercade1Click(Sender: TObject);
    procedure mnu_CascadaClick(Sender: TObject);
    procedure mnu_CambiodeContrasenaClick(Sender: TObject);
    procedure mnu_VentanaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure InterfaseProveedor1Click(Sender: TObject);
    procedure Recepcin1Click(Sender: TObject);
    procedure ControldeCalidad1Click(Sender: TObject);
    procedure EntregaraTransportes1Click(Sender: TObject);
    procedure mnu_RecibirdeTransportesClick(Sender: TObject);
    procedure VerTelevaspendientesdeingresaraMaestro1Click(
      Sender: TObject);
    procedure VerSaldosenAlmacenes1Click(Sender: TObject);
    procedure ConfiguracindeInicio1Click(Sender: TObject);
    procedure mnuPedidosPendientesClick(Sender: TObject);
    procedure mnuIdentificarTeleviaClick(Sender: TObject);
    procedure mnuStockPuntosEntregaClick(Sender: TObject);
    procedure mnuRegistrarRoboTeleviaClick(Sender: TObject);
    procedure mnuCambiarEstadoTAGClick(Sender: TObject);
  private
    { Private declarations }
   	Navegador : TNavigator;
    procedure CargarPaginasItemsNavegador;
  public
	{ Public declarations }
	function Inicializar: Boolean;
  end;

var
  MainForm: TMainForm;


implementation

uses frmControlCalidadPallets, FrmRecibirTagsNominados, GeneradorDeIni,
  frmRptStockPuntosEntrega, frmOrdenesPedidoPendientes, frmRecepcionTAGs,
  EntregarATransportesConPedidos, frmIdentificarTAG, frmListaPuntosEntrega;

var
	Salir: Boolean;

{$R *.dfm}

function TMainForm.Inicializar: Boolean;
var
    f: TLoginForm;
    ok: boolean;
begin
	// Navegador
    application.Title:=SYS_GESTION_TAGS_TITLE;
    SistemaActual:=SYS_GESTION_TAGS;

    if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
   		MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
        Application.Terminate;
    end;

	Navegador := TNavigator.Create(self);
    Navegador.IconBarVisible := false;
	// Deberiamos obtener todas las paginas a agregar aca... pero bueno.. es lo que hay

    ok := true;
    DMConnections.SeConectoBaseCAC := false;
    // Men�es
	//if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) and FileExists('GestionTAG.dpr') then begin
	if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin
		ok := GenerateMenuFile(MainMenu, SYS_GESTION_TAGS, True, DMConnections.BaseCAC);
	end;
	Salir := False;
    if ok then begin
        // Login
        Application.CreateForm(TLoginForm, f);
        DMConnections.BaseCAC.Connected := False;
        if f.Inicializar(DMConnections.BaseCAC, SYS_GESTION_TAGS) and (f.ShowModal = mrOk) then begin
			UsuarioSistema := f.CodigoUsuario;

            RefrescarBarraDeEstado(StatusBar, SYS_GESTION_TAGS);

            CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_GESTION_TAGS);
            HabilitarPermisosMenu(Menu);

            CargarPaginasItemsNavegador;

            CargarInformes(DMConnections.BaseCAC, SYS_GESTION_TAGS, MainMenu, DMConnections.cnInformes);
            Result := True;
            DMConnections.SeConectoBaseCAC := true;
        end else begin
            Result := False;
        end;
        f.Release;
    end
    else result := False;

    if not Result then Exit;
    Navegador.ConnectionCOP := nil;
    Navegador.ConnectionCAC := DMConnections.BaseCAC;

    AlmacenTAGs := ApplicationIni.ReadInteger('General', 'AlmacenOrigen', -1);
end;

procedure TMainForm.mnu_salirClick(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.Cerrar1Click(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
    DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
    DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

procedure TMainForm.mnu_CambiarUbicacionClick(Sender: TObject);
var
    f:TfrmCambiarUbicaciones;
begin
   Application.CreateForm(TfrmCambiarUbicaciones,f);
   if f.Inicializa(false, mnu_CambiarUbicacion.caption, 0, AlmacenTAGs) then f.ShowModal;
   f.release;
end;


procedure TMainForm.Acercade1Click(Sender: TObject);
var
	f:TAboutFrm;
begin
    if not Acercade1.Enabled then exit;
    Application.CreateForm(TAboutFrm, f);
	f.ShowModal;
    f.Release;
end;

procedure TMainForm.mnu_CascadaClick(Sender: TObject);
begin
    if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

procedure TMainForm.mnu_CambiodeContrasenaClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
    if not mnu_cambiodecontrasena.Enabled then exit;
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DMConnections.BaseCac,UsuarioSistema) then f.ShowModal;
	f.Release;
end;

procedure TMainForm.mnu_VentanaClick(Sender: TObject);
begin
	mnu_cascada.Enabled := GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled := mnu_cascada.Enabled;
	mnu_anterior.Enabled := GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled := mnu_anterior.Enabled;
end;

procedure TMainForm.CargarPaginasItemsNavegador;
var
	Bmp: TBitmap;
	hayAlguno: boolean;
begin
	hayAlguno := false;
    Bmp := TBitmap.Create;

    Navegador.IconBarVisible := false;
	(* Creamos las paginas del Icon Bar que necesitamos *)
	(* OJO! que los nombres de las p�ginas no pueden contener caracteres con acentos !!!!! *)

    (*HABILITACI�N DE LOS ICONOS:
    Para cada p�gina pregunto si alguno de los �conos que la componen
    est�n habilitados; si es as� creo la p�gina y procedo a agregar los �tems.
    Por cada �cono se chequea si est� habilitado el �tem del men� que ejecuta,
    si no es as�, no se agrega el �cono.
    *)

    if Recepcion1.Enabled then begin
        Navegador.AddPage('Recepcion', MSG_RECEPCION_TAGS, '');

        if InterfaseProveedor1.Enabled then begin
	        Bmp.Assign(nil);
    	    PageImagenes.GetBitmap(7, Bmp);
        	Navegador.AddIcon('Recepcion', MSG_IMPORTAR_ARCHIVO_PROVEEDOR, '', Bmp, InterfaseProveedor1Click);
        end;

        if Recepcin1.Enabled then begin
	        Bmp.Assign(nil);
    	    PageImagenes.GetBitmap(10, Bmp);
        	Navegador.AddIcon('Recepcion', MSG_RECIBIR_ENVIOS_TAGS, '', Bmp, Recepcin1Click);
        end;

        if ControldeCalidad1.Enabled then begin
        	Bmp.Assign(nil);
    	    PageImagenes.GetBitmap(5, Bmp);
	        Navegador.AddIcon('Recepcion', MSG_CONTROL_CALIDAD, '', Bmp, ControldeCalidad1Click);
        end;

        hayAlguno := true;
    end;

    if Movimientos1.Enabled then begin
        Navegador.AddPage('Movimientos', MSG_MOVIMIENTOS_TAGS, '');

        if EntregaraTransportes1.Enabled then begin
        	Bmp.Assign(nil);
    	    PageImagenes.GetBitmap(6, Bmp);
	        Navegador.AddIcon('Movimientos', MSG_ENTREGAR_TAGS_A_TRANSPORTES, '', Bmp, EntregaraTransportes1Click);
        end;

//        Bmp.Assign(nil);
//        PageImagenes.GetBitmap(3, Bmp);
//        Navegador.AddIcon('Movimientos', MSG_RECIBIR_TAGS_DE_TRANSPORTES, '', Bmp, RecibirdeTransportes1Click);

        if mnu_CambiarUbicacion.Enabled then begin
	        Bmp.Assign(nil);
    	    PageImagenes.GetBitmap(4, Bmp);
        	Navegador.AddIcon('Movimientos', MSG_CAMBIAR_UBICACION_TAGS, '', Bmp, mnu_CambiarUbicacionClick);
        end;

        hayAlguno := true;
    end;

    Bmp.Free;
    Bmp := TBitmap.Create;

    (* pagina y botones de mantenimiento *)
    if (Configuracion1.Enabled or mnu_CambiodeContrasena.Enabled) then begin
		Navegador.AddPage('Mantenimiento', STR_MANTENIMIENTO, '');

        Bmp.Assign(nil);
        PageImagenes.GetBitmap(2, Bmp);
        if mnu_CambiodeContrasena.Enabled then
            Navegador.AddIcon('Mantenimiento', MSG_CAMBIO_CONTRASENIA, '', Bmp, mnu_CambiodeContrasenaClick);
        hayAlguno := true;
    end;

    (* Fijamos la primer p�gina como inicial *)
    if hayAlguno then begin
    	Navegador.IconBarActivePage := 0;
    	Navegador.iconBarVisible := true;
    end;

    (* Listo!, seguimos.. *)
	Bmp.Free;
	Update;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ChequearTareasBloqueadas(DMConnections.BaseCAC);
end;

procedure TMainForm.InterfaseProveedor1Click(Sender: TObject);
var
    f: TFInterfaseProveedorTAG;
begin
	if FindFormOrCreate(TFInterfaseProveedorTAG, f) then
		f.Show
	else begin
		if not f.Inicializar(InterfaseProveedor1.Caption, True) then f.Release;
	end
end;

procedure TMainForm.Recepcin1Click(Sender: TObject);
var
    f: TFRecepcionTAGs;
begin
	if FindFormOrCreate(TFRecepcionTAGs, f) then
		f.Show
	else begin
		if not f.Inicializar(Recepcin1.Caption, True) then f.Release;
	end
end;

procedure TMainForm.ControldeCalidad1Click(Sender: TObject);
var
    f: TFControlCalidadPallets;
begin
	if FindFormOrCreate(TFControlCalidadPallets, f) then
		f.Show
	else begin
        if not f.Inicializar(ControldeCalidad1.Caption, True) then f.Release;
	end;
end;

procedure TMainForm.EntregaraTransportes1Click(Sender: TObject);
var
    f:TfrmEntregarATransportesCP;
begin
	if FindFormOrCreate(TfrmEntregarATransportesCP, f) then
		f.Show
	else begin
        if not f.InicializaR(True, EntregaraTransportes1.caption, AlmacenTAGs) then f.Release;
	end;
end;

procedure TMainForm.mnu_RecibirdeTransportesClick(Sender: TObject);
var
    f: TFormRecibirTagsNominados;
begin
    Application.CreateForm(TFormRecibirTagsNominados, f);
    if (f.Inicializar(LimpiarCaracteresLabel(mnu_RecibirdeTransportes.Caption),UsuarioSistema, -1, false, AlmacenTAGs)) then
        f.ShowModal;
    f.Release;
end;

procedure TMainForm.VerTelevaspendientesdeingresaraMaestro1Click(
  Sender: TObject);
var
    f:TFTAGsImportados;
begin
	if FindFormOrCreate(TFTAGsImportados, f) then
		f.Show
	else begin
        if not f.Inicializar(True) then f.Release;
	end;
end;

procedure TMainForm.VerSaldosenAlmacenes1Click(Sender: TObject);
var
    f:TFSaldosAlmacenesTAGs;
begin
	if FindFormOrCreate(TFSaldosAlmacenesTAGs, f) then
		f.Show
	else begin
		if not f.Inicializar(VerSaldosenAlmacenes1.Caption, True) then f.Release;
	end
end;

procedure TMainForm.ConfiguracindeInicio1Click(Sender: TObject);
var
    f : TFormGeneradorIni;
begin
	Application.CreateForm(TFormGeneradorIni, f);
    if (f.Inicializa and (f.ShowModal = mrOk)) then AlmacenTAGs := f.AlmacenOrigen;
    f.Release;
end;

procedure TMainForm.mnuPedidosPendientesClick(Sender: TObject);
var
	f:TFormOrdenesPedidoPendientes;
begin
	if FindFormOrCreate(TFormOrdenesPedidoPendientes, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuPedidosPendientes.Caption, True) then f.Release;
	end
end;

procedure TMainForm.mnuIdentificarTeleviaClick(Sender: TObject);
var
    f: TFormIdentificarTAG;
begin
	if FindFormOrCreate(TFormIdentificarTAG, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuIdentificarTelevia.Caption, True, False) then f.Release;
	end
end;

procedure TMainForm.mnuStockPuntosEntregaClick(Sender: TObject);
var
	f: TFormRptStockPuntosEntrega;
begin
	Application.CreateForm(TFormRptStockPuntosEntrega, f);
	if not f.Inicializar then exit;
	if f.Ejecutar then f.Free;
end;

procedure TMainForm.mnuRegistrarRoboTeleviaClick(Sender: TObject);
var
    f: TFormIdentificarTAG;
begin
	if FindFormOrCreate(TFormIdentificarTAG, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuRegistrarRoboTelevia.Caption, True, True) then f.Release;
	end
end;

procedure TMainForm.mnuCambiarEstadoTAGClick(Sender: TObject);
var
    f: TFormIdentificarTAG;
begin
	if FindFormOrCreate(TFormIdentificarTAG, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuCambiarEstadoTAG.Caption, True, True) then f.Release;
	end
end;

end.


