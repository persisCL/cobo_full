unit frmOpcionesControlCalidadTAGs;
{
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Util, PeaProcs;

type
  TformOpcionesControlCalidadTAGs = class(TForm)
    lblMensaje: TLabel;
    rgAccion: TRadioGroup;
    txtCausaRechazo: TEdit;
    Label1: TLabel;
    btnProceder: TButton;
    procedure btnProcederClick(Sender: TObject);
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    { Public declarations }
    function Inicializar(Mensaje: ANSIString; AceptaParcial: Boolean): Boolean;
  end;

var
  formOpcionesControlCalidadTAGs: TformOpcionesControlCalidadTAGs;

implementation

{$R *.dfm}

function TformOpcionesControlCalidadTAGs.Inicializar(Mensaje: ANSIString; AceptaParcial: Boolean): Boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	lblMensaje.Caption := Mensaje;
    if not AceptaParcial then rgAccion.Items.Delete(3);
	txtCausaRechazo.Clear;

    result := true
end;

procedure TformOpcionesControlCalidadTAGs.btnProcederClick(
  Sender: TObject);
resourcestring
	MSG_CAPTION_OPCION = 'Opciones para Control de Calidad';
    MSG_ERROR_OPCION = 'Debe especificarse la causa de rechazo';
begin
    if rgAccion.ItemIndex <> 1 then txtCausaRechazo.Clear;

	if not ValidateControls([txtCausaRechazo],
    	[((rgAccion.ItemIndex = 1) and (Trim(txtCausaRechazo.Text) <> '')) or
        ((rgAccion.ItemIndex <> 1) and (Trim(txtCausaRechazo.Text) = ''))],
		Format(MSG_CAPTION_OPCION, ['Control de Calidad']),
		[MSG_ERROR_OPCION]) then begin
		Exit;
	end;

    Close
end;

end.
