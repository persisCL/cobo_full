object FormIdentificarTAG: TFormIdentificarTAG
  Left = 348
  Top = 184
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Identificar Telev'#237'a'
  ClientHeight = 392
  ClientWidth = 372
  Color = clBtnFace
  Constraints.MaxHeight = 420
  Constraints.MaxWidth = 380
  Constraints.MinHeight = 392
  Constraints.MinWidth = 378
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 355
    Height = 232
  end
  object Label1: TLabel
    Left = 29
    Top = 21
    Width = 83
    Height = 13
    Caption = 'Etiqueta leida:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 29
    Top = 54
    Width = 136
    Height = 13
    Caption = 'Contract Serial Number:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 29
    Top = 78
    Width = 76
    Height = 13
    Caption = 'ContextMark:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 29
    Top = 97
    Width = 70
    Height = 26
    Caption = 'Categor'#237'a Interurbana:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object lblNoExiste: TLabel
    Left = 15
    Top = 308
    Width = 251
    Height = 13
    Caption = 'Telev'#237'a no existente en Maestro de Telev'#237'a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
    WordWrap = True
  end
  object Label9: TLabel
    Left = 29
    Top = 162
    Width = 62
    Height = 13
    Caption = 'Ubicaci'#243'n:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblNoPuedeCambiarEstadoSituacion: TLabel
    Left = 15
    Top = 323
    Width = 238
    Height = 13
    Caption = 'El Telev'#237'a no puede cambiarse de estado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
    WordWrap = True
  end
  object Label10: TLabel
    Left = 29
    Top = 188
    Width = 84
    Height = 13
    Caption = 'Estado Actual:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblNuevoEstado: TLabel
    Left = 28
    Top = 216
    Width = 85
    Height = 13
    Caption = 'Nuevo Estado:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblOtraConcesionaria: TLabel
    Left = 15
    Top = 338
    Width = 235
    Height = 13
    Caption = 'El Telev'#237'a pertence a otra Concesionaria'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
    WordWrap = True
  end
  object Label11: TLabel
    Left = 29
    Top = 129
    Width = 70
    Height = 24
    Caption = 'Categor'#237'a Urbana:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object txtEtiqueta: TEdit
    Left = 167
    Top = 21
    Width = 121
    Height = 21
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 11
    ParentFont = False
    TabOrder = 0
    OnChange = txtEtiquetaChange
  end
  object txtContextMark: TEdit
    Left = 167
    Top = 75
    Width = 31
    Height = 21
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
  end
  object txtCategoria: TEdit
    Left = 103
    Top = 99
    Width = 31
    Height = 21
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
  end
  object GBListas: TGroupBox
    Left = 8
    Top = 242
    Width = 355
    Height = 60
    Caption = ' Listas '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object ledVerde: TLed
      Left = 18
      Top = 18
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGreen
      ColorOff = clBtnFace
    end
    object ledGris: TLed
      Left = 18
      Top = 36
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clGray
      ColorOff = clBtnFace
    end
    object ledAmarilla: TLed
      Left = 189
      Top = 18
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clYellow
      ColorOff = clBtnFace
    end
    object ledNegra: TLed
      Left = 189
      Top = 36
      Width = 12
      Height = 12
      Blinking = False
      Diameter = 11
      Enabled = False
      ColorOn = clBlack
      ColorOff = clBtnFace
    end
    object Label5: TLabel
      Left = 39
      Top = 18
      Width = 34
      Height = 13
      Caption = 'Verde'
    end
    object Label6: TLabel
      Left = 39
      Top = 36
      Width = 23
      Height = 13
      Caption = 'Gris'
    end
    object Label7: TLabel
      Left = 210
      Top = 18
      Width = 45
      Height = 13
      Caption = 'Amarilla'
    end
    object Label8: TLabel
      Left = 210
      Top = 36
      Width = 35
      Height = 13
      Caption = 'Negra'
    end
  end
  object txtContractSerialNumber: TEdit
    Left = 167
    Top = 51
    Width = 121
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
  end
  object txtDescripcionCategoria: TEdit
    Left = 139
    Top = 99
    Width = 214
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 6
  end
  object txtDescripcionUbicacion: TEdit
    Left = 92
    Top = 159
    Width = 253
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 7
  end
  object txtEstadoActual: TEdit
    Left = 119
    Top = 185
    Width = 172
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 9
  end
  object cbEstadosPosibles: TVariantComboBox
    Left = 119
    Top = 212
    Width = 173
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 10
    Items = <>
  end
  object btnSalir: TButton
    Left = 277
    Top = 357
    Width = 75
    Height = 25
    Caption = '&Salir'
    TabOrder = 4
    OnClick = btnSalirClick
  end
  object btnAceptar: TButton
    Left = 117
    Top = 357
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 198
    Top = 357
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 11
    OnClick = btnCancelarClick
  end
  object txtCategoriaUrbana: TEdit
    Left = 103
    Top = 129
    Width = 31
    Height = 21
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 12
  end
  object txtDescripcionCatUrbana: TEdit
    Left = 139
    Top = 126
    Width = 214
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 13
  end
  object spLeerDatosTAG: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'LeerDatosTAG'
    Parameters = <>
    Left = 318
    Top = 206
    object spLeerDatosTAGContextMark: TWordField
      FieldName = 'ContextMark'
    end
    object spLeerDatosTAGContractSerialNumber: TLargeintField
      FieldName = 'ContractSerialNumber'
    end
    object spLeerDatosTAGEtiqueta: TStringField
      FieldName = 'Etiqueta'
      FixedChar = True
      Size = 11
    end
    object spLeerDatosTAGCodigoCategoria: TWordField
      FieldName = 'CodigoCategoria'
    end
    object spLeerDatosTAGCodigoEstadoSituacionTAG: TWordField
      FieldName = 'CodigoEstadoSituacionTAG'
    end
    object spLeerDatosTAGIndicadorListaVerde: TBooleanField
      FieldName = 'IndicadorListaVerde'
    end
    object spLeerDatosTAGIndicadorListaAmarilla: TBooleanField
      FieldName = 'IndicadorListaAmarilla'
    end
    object spLeerDatosTAGIndicadorListaGris: TBooleanField
      FieldName = 'IndicadorListaGris'
    end
    object spLeerDatosTAGIndicadorListaNegra: TBooleanField
      FieldName = 'IndicadorListaNegra'
    end
    object spLeerDatosTAGDescripcionCategoria: TStringField
      FieldName = 'DescripcionCategoria'
      FixedChar = True
      Size = 60
    end
    object spLeerDatosTAGDescripcionUbicacion: TStringField
      FieldName = 'DescripcionUbicacion'
      ReadOnly = True
      FixedChar = True
      Size = 50
    end
    object spLeerDatosTAGEstadoSituacion: TStringField
      FieldName = 'EstadoSituacion'
      FixedChar = True
      Size = 50
    end
    object spLeerDatosTAGOtraConcesionaria: TIntegerField
      FieldName = 'OtraConcesionaria'
      ReadOnly = True
    end
    object spLeerDatosTAGCodigoCategoriaUrbana: TWordField
      FieldName = 'CodigoCategoriaUrbana'
    end
    object spLeerDatosTAGPatente: TStringField
      FieldName = 'Patente'
    end
    object spLeerDatosTAGCategoriaUrbana: TStringField
      FieldName = 'CategoriaUrbana'
      Size = 60
    end
  end
  object spActualizarSituacionTag: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarSituacionTag'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEstadoSituacionTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 318
    Top = 237
  end
  object qryEstadosPosibles: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoEstadoSituacionTAG'
        DataType = ftWord
        Precision = 3
        Value = 3
      end>
    SQL.Strings = (
      'select DISTINCT'
      '  EST.CodigoEstadoSituacionTAG, EST.Descripcion'
      'from'
      
        '  CambiosEstadosTAGs CET WITH (NOLOCK), EstadosSituacionTAGs EST' +
        ' WITH (NOLOCK) '
      'where'
      '  EST.CodigoEstadoSituacionTAG = CET.CodigoEstadoPosible'
      '  AND CET.CodigoEstadoActual = :CodigoEstadoSituacionTAG')
    Left = 315
    Top = 289
  end
end
