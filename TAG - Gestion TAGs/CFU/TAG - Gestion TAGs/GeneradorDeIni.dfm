object FormGeneradorIni: TFormGeneradorIni
  Left = 286
  Top = 256
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Configurar Almacen de Telev'#237'as'
  ClientHeight = 123
  ClientWidth = 342
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poNone
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 6
    Top = 8
    Width = 331
    Height = 73
  end
  object Label1: TLabel
    Left = 22
    Top = 37
    Width = 112
    Height = 13
    Caption = 'Almac'#233'n de &Origen:'
    FocusControl = edtAlmacenOrigen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edtAlmacenOrigen: TNumericEdit
    Left = 181
    Top = 34
    Width = 121
    Height = 21
    Color = 16444382
    TabOrder = 0
  end
  object btnAceptar: TButton
    Left = 180
    Top = 90
    Width = 75
    Height = 27
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 260
    Top = 89
    Width = 75
    Height = 27
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelarClick
  end
end
