unit frmTeleviasImportados;
{
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, PeaProcs, StdCtrls, DPSControls, Util,
  ListBoxEx, DBListEx, ExtCtrls, UtilProc, DPSGrid, UtilDB, RStrings,
  DmiCtrls;

type
  TFTAGsImportados = class(TForm)
    dsInterfaseTAGs: TDataSource;
    Panel1: TPanel;
    dblInterfaseTAGs: TDBListEx;
    spObtenerTAGsInterfase: TADOStoredProc;
    spObtenerTAGsInterfaseContractSerialNumber: TLargeIntField;
    spObtenerTAGsInterfaseFechaMovimiento: TDateTimeField;
    spObtenerTAGsInterfaseArchivoImportacion: TStringField;
    spObtenerTAGsInterfaseUsuario: TStringField;
    spObtenerTAGsInterfaseEtiqueta: TStringField;
    spObtenerTAGsInterfaseCategoria: TStringField;
    lblCantidad: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Cantidad: TNumericEdit;
    btnActualizar: TButton;
    btnSalir: TButton;
    spObtenerTAGsInterfaseCatUrbana: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure spObtenerTAGsInterfaseCalcFields(DataSet: TDataSet);
    procedure DBListEx1Columns0HeaderClick(Sender: TObject);
	function AbrirInterfaseTAGs: Boolean;
    procedure dblInterfaseTAGsDrawBackground(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure btnActualizarClick(Sender: TObject);
  private
    FEtiqueta: String;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	function DarOrderBy: String;
  public
    property Etiqueta: String read FEtiqueta write FEtiqueta;
	function Inicializar(MDIChild: boolean): Boolean;
  end;

var
  FTAGsImportados: TFTAGsImportados;

implementation

uses DMConnection;

{$R *.dfm}

function TFTAGsImportados.Inicializar(MDIChild: boolean): Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
       	CenterForm(Self);
	end;

	Result := AbrirInterfaseTAGs
end;

procedure TFTAGsImportados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TFTAGsImportados.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFTAGsImportados.FormDestroy(Sender: TObject);
begin
	spObtenerTAGsInterfase.Close
end;

procedure TFTAGsImportados.spObtenerTAGsInterfaseCalcFields(
  DataSet: TDataSet);
begin
    DataSet.FieldByName('Etiqueta').AsString := SerialNumberToEtiqueta(DataSet.FieldByName('ContractSerialNumber').AsString);
end;

procedure TFTAGsImportados.DBListEx1Columns0HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending
    end else begin
        TDBListExColumn(sender).Sorting := csAscending;
    end; //if
    if spObtenerTAGsInterfase.Active then AbrirInterfaseTAGs
end;

function TFTAGsImportados.AbrirInterfaseTAGs: Boolean;
begin
    spObtenerTAGsInterfase.DisableControls;
   	Screen.Cursor := crHourGlass;
    try
    	try
		    with spObtenerTAGsInterfase do begin
    		    close;
		        Parameters.ParamByName('@OrderBy').Value := DarOrderBy;
		        Parameters.ParamByName('@Cantidad').Value := IntToStr(Cantidad.ValueInt);
    		end;

	    	result := OpenTables([spObtenerTAGsInterfase]);
	    	lblCantidad.Caption := 'Cantidad Total de Telev�as (visualizados): ' + IntToStr(spObtenerTAGsInterfase.RecordCount) +
    								'          Pendientes de Recibir: ' + IntToStr (TAGSLibres(0));
		except
			on e: Exception do begin
            	MsgBoxErr(MSG_ERROR_TAGS_INTERFACE, e.message, self.caption, MB_ICONSTOP);
                result := false
            end
        end
    finally
	    spObtenerTAGsInterfase.EnableControls;
		Screen.Cursor := crDefault
    end
end;

function TFTAGsImportados.DarOrderBy:String;
var
    i:Integer;
begin
    result:='';
    for i := 0 to  dblInterfaseTAGs.Columns.Count-1 do
        if dblInterfaseTAGs.Columns[i].Sorting <> csNone then begin
        	if dblInterfaseTAGs.Columns[i].Header.Caption = 'Etiqueta' then
            	result := 'ContractSerialNumber' + iif(dblInterfaseTAGs.Columns[i].Sorting=csDescending,' DESC','')
            else
	        	result := dblInterfaseTAGs.Columns[i].FieldName + iif(dblInterfaseTAGs.Columns[i].Sorting=csDescending,' DESC','');
        end;
end;

procedure TFTAGsImportados.dblInterfaseTAGsDrawBackground(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
	label1.Caption := 'Telev�a #' + IntToStr(spObtenerTAGsInterfase.RecNo) + ': ' +
                      spObtenerTAGsInterfase.FieldByName ('Etiqueta').AsString
end;

procedure TFTAGsImportados.btnActualizarClick(Sender: TObject);
begin
    if (Cantidad.ValueInt < 0) or (Cantidad.ValueInt = 0)then begin
        MsgBoxErr('Error de datos', 'La cantidad de Telev�as a visualizar ' +
          'no puede ser menor a cero o igual a cero', 'Visualizar Telev�as a Incorporar a Maestro', MB_ICONERROR);
        Exit;
    end;
	AbrirInterfaseTAGs;
end;

end.

