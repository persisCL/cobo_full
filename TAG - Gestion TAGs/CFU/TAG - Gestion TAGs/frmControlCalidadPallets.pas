{********************************** File Header ********************************
File Name   : frmControlCalidadPallets
Author      : Castro, Ra�l A.
Date Created: 10/05/04
Language    : ES-AR
Description : M�dulo de control de calidad
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

*******************************************************************************}

unit frmControlCalidadPallets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, DBClient, StdCtrls, DPSControls, Grids, DBGrids, Peaprocs,
  DmiCtrls, Validate, DateEdit, ExtCtrls, Buttons, UtilProc, StrUtils, Util,
  VariantComboBox, PeaTypes, UtilDB;

type
  TFControlCalidadPallets = class(TForm)
    Panel1: TPanel;
    gbDatos: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    txtCantidadCajas: TNumericEdit;
    txtCantidadTAGs: TNumericEdit;
    gbControl: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    lblControlCajas: TLabel;
    lblControlTAGs: TLabel;
    Panel2: TPanel;
    dbgCajas: TDBGrid;
    Panel3: TPanel;
    cdsCajas: TClientDataSet;
    dsCajas: TDataSource;
    spObtenerDatosPalletTAGs: TADOStoredProc;
    spObtenerDatosRangoTAGs: TADOStoredProc;
    txtIDPallet: TNumericEdit;
    spObtenerDatosCajaTAGs: TADOStoredProc;
    spRegistrarControlCalidad: TADOStoredProc;
    cdsTAGs: TClientDataSet;
    spVerificarTAGEnArchivos: TADOStoredProc;
    spRegistrarControlCalidadManual: TADOStoredProc;
    VariantComboBox1: TVariantComboBox;
    Label2: TLabel;
    Panel4: TPanel;
    DPSButton1: TButton;
    btnControlManual: TButton;
    btnImprimir: TButton;
    btnLimpiar: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnBorrarLinea: TButton;
    procedure cdsCajasAfterPost(DataSet: TDataSet);
    procedure txtCantidadCajasChange(Sender: TObject);
    procedure txtCantidadTAGsChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtIDPalletChange(Sender: TObject);
    procedure cdsCajasNewRecord(DataSet: TDataSet);
    procedure btnCancelarClick(Sender: TObject);
    function ControlManual: Integer;
    procedure cdsCajasBeforePost(DataSet: TDataSet);
    procedure btnControlManualClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure cdsTAGsNewRecord(DataSet: TDataSet);
    procedure cdsTAGsBeforePost(DataSet: TDataSet);
    procedure cdsCajasBeforeDelete(DataSet: TDataSet);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnLimpiarClick(Sender: TObject);
    procedure DPSButton1Click(Sender: TObject);
    procedure cdsCajasAfterDelete(DataSet: TDataSet);
    procedure cdsCajasBeforeScroll(DataSet: TDataSet);
    procedure btnBorrarLineaClick(Sender: TObject);
  private
    { Private declarations }
	TAGInicial,
    TAGFinal: DWORD;
	FCargando: Boolean;
    FListaEmbalajesCDS: TStringList;
	CodigoEmbalajePadre : Integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function CantidadTAGs(DataSet: TDataSet): Integer;
    function ConsistenciaDatosLinea(dataset: TDataset; var descriError: AnsiString): boolean;
	function ExisteCaja(CodigoCaja: LongInt): Boolean;
	function ValidarIDCaja(DataSet: TDataSet): Boolean;
	function ValidarExistenciaTAG (CodigoTAG : DWORD; StrTipo: ANSIString; ConMsg: Boolean): Byte;
  public
    { Public declarations }
    x : integer;
	function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  FControlCalidadPallets: TFControlCalidadPallets;

implementation

uses frmControlCalidadTAGs, RStrings, DMComunicacion, frmTeleviasImportados,
  DMConnection;

{$R *.dfm}

function TFControlCalidadPallets.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

    cdsCajas.CreateDataSet;
    cdsCajas.Open;

    cdsTAGs.CreateDataSet;
    cdsTAGs.Open;

    txtIDPallet.Clear;
    txtCantidadCajas.Clear;
    txtCantidadTAGs.Clear;

	FListaEmbalajesCDS := TStringList.Create;

	Result := true;
end;

function TFControlCalidadPallets.ControlManual: Integer;
var
	f: TFControlCalidadTAGs;
begin

	result := mrCancel;

 //   Application.CreateForm(TFControlCalidadTAGs, f);

    f:= TFControlCalidadTAGs.Create(Self);

  	if f.Inicializar(txtIDPallet.ValueInt, cdsCajas.FieldByName ('CodigoCajaTAG').AsInteger, cdsCajas.FieldByName ('CantidadTAGs').AsInteger, cdsCajas.FieldByName ('EstadoControl').AsString) then begin
		FCargando := True;
    	result := f.Showmodal;
		FCargando := False;

        //INICIO	: 20160623 CFU
        if result = mrCancel then
        Exit;
        //TERMINO	: 20160623 CFU
        
        cdsCajas.Edit;
        cdsCajas.FieldByName ('EstadoControl').AsString := f.EstadoCaja;
        cdsCajas.FieldByName ('CantidadTAGs').AsInteger := f.CantidadTAGsEnCaja;
        cdsCajas.FieldByName ('Consistencia').AsString := f.CausaRechazo;
        cdsCajas.Post
    end;
	f.Release;
end;

procedure TFControlCalidadPallets.cdsCajasAfterPost(DataSet: TDataSet);
begin
	FListaEmbalajesCDS.Add(DataSet.FieldByName('CodigoCajaTAG').AsString);

	txtCantidadCajasChange(nil);
    txtCantidadTAGsChange(nil);
end;

procedure TFControlCalidadPallets.txtCantidadCajasChange(Sender: TObject);
begin
	lblControlCajas.Caption:= IntToStr(cdsCajas.RecordCount) + ' de ' + txtCantidadCajas.Text;
    lblControlCajas.Font.Color := clBlack;
    if cdsCajas.RecordCount > txtCantidadCajas.ValueInt then lblControlCajas.Font.Color := clRed
end;

function TFControlCalidadPallets.CantidadTAGs(DataSet: TDataSet): Integer;
begin
	result := 0;

	DataSet.First;
    while not DataSet.EoF do Begin
        result := result + DataSet.FieldByName('CantidadTAGs').AsInteger;

       	DataSet.Next
	end;
end;

procedure TFControlCalidadPallets.txtCantidadTAGsChange(Sender: TObject);
var
	CantTAGs : Integer;
begin
	CantTAGs := CantidadTags (cdsCajas);

	lblControlTAGs.Caption:= IntToStr(CantTAGs) + ' de ' + txtCantidadTAGs.Text;
    lblControlTAGs.Font.Color := clBlack;
    if CantTAGs > txtCantidadTAGs.ValueInt then lblControlTAGs.Font.Color := clRed
end;

procedure TFControlCalidadPallets.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TFControlCalidadPallets.txtIDPalletChange(Sender: TObject);
begin
    cdsCajas.EmptyDataSet;
    FCargando := True;

   	try
		with spObtenerDatosPalletTAGs, Parameters do begin
    		Close;
            //INICIO:	20160816 CFU
            Parameters.Refresh;
            //TERMINO:	20160816 CFU
    		ParamByName('@CodigoPalletTAG').Value := txtIDPallet.ValueInt;
	        Open;

    	    txtCantidadCajas.Clear;
        	txtCantidadTAGs.Clear;

			txtCantidadCajas.ValueInt 	:= FieldByName('CantidadCajas').AsInteger;
    	    CodigoEmbalajePadre 		:= FieldByName('CodigoEmbalaje').AsInteger;
        	txtCantidadTAGs.ValueInt 	:= FieldByName('CantidadTAGs').AsInteger;

	        Close
    	end;
	except
		on e: Exception do MsgBoxErr(Format(MSG_ERROR_DATOS_EMBALAJE, [STR_PALLET]), e.message, self.caption, MB_ICONSTOP);
	end;

   	try
		with spObtenerDatosCajaTAGs, Parameters do begin
    		Close;
    		ParamByName('@CodigoEmbalajePadre').Value := CodigoEmbalajePadre;
	        Open;

    	    if RecordCount <> 0 then begin
       			FListaEmbalajesCDS.Clear;
    	    	while not EoF do Begin
        	    	cdsCajas.AppendRecord([FieldByName('CodigoEmbalaje').AsInteger,
											FieldByName('CodigoCajaTAG').AsInteger,
											SerialNumberToEtiqueta(FieldByName('NumeroInicialTAG').AsString),
											SerialNumberToEtiqueta(FieldByName('NumeroFinalTAG').AsString),
											FieldByName('CantidadTAGs').AsInteger,
											FieldByName('CategoriaTAGs').AsInteger,
											FieldByName('EstadoControl').AsString,
	                                        FieldByName('Observaciones').AsString,
    	                                    FieldByName('EstadoControl').AsString = 'P']);
				    FListaEmbalajesCDS.Add(FieldByName ('CodigoCajaTAG').AsString);

	        		Next
    	    	end
	        end;

    	    Close
	    end;
	except
		on e: Exception do MsgBoxErr(Format(MSG_ERROR_DATOS_EMBALAJE, [STR_CAJA]), e.message, self.caption, MB_ICONSTOP);
	end;

    FCargando := False;
end;

procedure TFControlCalidadPallets.cdsCajasNewRecord(DataSet: TDataSet);
begin
    cdsCajas.FieldByName ('CodigoEmbalaje').AsInteger	:= 0;
	cdsCajas.FieldByName ('CodigoCajaTAG').AsInteger	:= 0;
    cdsCajas.FieldByName ('NumeroInicialTAG').AsFloat 	:= 0;
    cdsCajas.FieldByName ('NumeroFinalTAG').AsFloat 	:= 0;
    cdsCajas.FieldByName ('CantidadTAGs').AsInteger 	:= 0;
    cdsCajas.FieldByName ('CategoriaTAGs').AsString 	:= '1';
    cdsCajas.FieldByName ('EstadoControl').AsString		:= 'P';
    cdsCajas.FieldByName ('Editable').AsBoolean			:= True;
end;

procedure TFControlCalidadPallets.btnCancelarClick(Sender: TObject);
begin
	cdsCajas.Close;

    Close
end;

Function TFControlCalidadPallets.ConsistenciaDatosLinea(dataset: TDataset; var descriError: AnsiString): boolean;
var
    CantidadTAGsRango: Integer;
    CantidadTAGsCategoria: Integer;
begin
    result := true;

   	try
	    with spObtenerDatosRangoTAGs, Parameters do begin
    	    Close;
            //INICIO:	20160812 CFU
            Parameters.Refresh;
            //TERMINO:	20160812 CFU
        	ParamByName('@TAGInicial').Value 			:= TAGInicial;
	        ParamByName('@TAGFinal').Value  			:= TAGFinal;
    	    ParamByName('@CategoriaTAG').Value			:= DataSet.FieldByName('CategoriaTAGs').AsString;
        	ParamByName('@CategoriaProveedor').Value	:= 1;
        	ParamByName('@CualquierUbicacion').Value	:= 1;
	        ParamByName('@CantidadTAGs').Value 			:= 0;
    	    ParamByName('@CantidadTAGsCategoria').Value := 0;
        	ParamByName('@CantidadTAGsMaestro').Value 	:= 0;
	        ParamByName('@CantidadTAGsCategoriaMaestro').Value:= 0;
    	    ExecProc;

        	CantidadTAGsRango 		:= ParamByName('@CantidadTAGs').Value;
	        CantidadTAGsCategoria 	:= ParamByName('@CantidadTAGsCategoria').Value;

    	    Close
	    end;

    	if (CantidadTAGsRango <> DataSet.FieldByName('CantidadTAGS').AsInteger) then begin
        	DescriError := DescriError + 'Cantidad General Incorrecta';
	        result := False
    	end;

	    if (CantidadTAGsCategoria <> DataSet.FieldByName('CantidadTAGS').AsInteger) then begin
    	    DescriError := DescriError + iif(DescriError = '', '', ' - ') + 'Cantidad por categor�a incorrecta';
	        result := False
    	end;
	except
		on e: Exception do begin
        	MsgBoxErr(MSG_ERROR_CONSISTENCIA_LINEA, e.message, self.caption, MB_ICONSTOP);
	        result := False
        end
	end;
end;

procedure TFControlCalidadPallets.btnControlManualClick(Sender: TObject);
begin

   	if (cdsCajas.FieldByName ('EstadoControl').AsString = 'P') and (cdsCajas.FieldByName ('CodigoCajaTAG').AsInteger > 0) then begin
		if cdsCajas.State in [dsEdit, dsInsert] then cdsCajas.Post;
		ControlManual
    end
    else begin
    	if cdsCajas.FieldByName ('CodigoCajaTAG').AsInteger <> 0 then MsgBox('La caja ya ha sido procesada', self.caption, MB_ICONSTOP)
    end;
end;

procedure TFControlCalidadPallets.btnAceptarClick(Sender: TObject);
var
	StrAdvertencia: ANSIString;
	CodigoEmbalaje: LongInt;
    TodoOk: Boolean;
begin
	if not ValidateControls([txtIDPallet],
    	[(txtIDPallet.ValueInt > 0)],
		Format(MSG_CAPTION_ACTUALIZAR,['Control de Calidad de Pallets']),
		[MSG_ERROR_ID_PALLET]) then begin
		Exit;
	end;

	if (not cdsCajas.IsEmpty) and (cdsCajas.State in [dsEdit, dsInsert]) then cdsCajas.Post;

    StrAdvertencia := '';
    if cdsCajas.RecordCount <> txtCantidadCajas.ValueInt then StrAdvertencia := 'La cantidad de Cajas ingresadas no coincide con la indicada a cargar' + #13;
    if CantidadTAGs(cdsCajas) <> txtCantidadTAGs.ValueInt then StrAdvertencia := StrAdvertencia + 'La cantidad de Telev�as ingresados no coincide con la indicada a cargar' + #13;

    if (StrAdvertencia <> '') And (MsgBox(StrAdvertencia + 'Desea a�n grabar los datos?', STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) <> IDYES)
    then begin
    	Exit
    end;

	Screen.Cursor := crHourGlass;
    TodoOk:= False;
    DMConnections.BaseCAC.BeginTrans;

    try
        try
			cdsTAGs.DisableControls;
            cdsCajas.DisableControls;

		    // graba los datos
		    cdsCajas.First;
		    while not cdsCajas.EoF do begin
    			cdsTAGs.Filter := 'IDCaja = ' + IntToStr(cdsCajas.FieldByName ('CodigoCajaTAG').AsInteger);

		        // no es una caja aceptada o rechazada previamente
    			if (cdsCajas.FieldByName ('Editable').AsBoolean) then begin
					if cdsTAGs.RecordCount = 0 then begin
	    		        // registro una caja completa
		    			with spRegistrarControlCalidad, Parameters do begin
                            //INICIO:	20160812 CFU
                            Parameters.Refresh;
                            //TERMINO:	20160812 CFU
		    		    	ParamByName ('@CodigoEmbalaje').Value 			:= cdsCajas.FieldByName ('CodigoEmbalaje').AsInteger;
        					ParamByName ('@CodigoEmbalajePadre').Value		:= CodigoEmbalajePadre;
	        				ParamByName ('@CodigoCajaTAG').Value 			:= cdsCajas.FieldByName ('CodigoCajaTAG').AsInteger;
				        	ParamByName ('@NumeroInicialTAG').Value 		:= EtiquetaToSerialNumber(PadL(cdsCajas.FieldByName('NumeroInicialTAG').AsString, 11, '0'));
    				    	ParamByName ('@NumeroFinalTAG').Value 			:= EtiquetaToSerialNumber(PadL(cdsCajas.FieldByName('NumeroFinalTAG').AsString, 11, '0'));
	        			 	ParamByName ('@CantidadTAGs').Value 			:= cdsCajas.FieldByName ('CantidadTAGs').AsInteger;
		    	     		ParamByName ('@CantidadTAGsAceptados').Value  	:= cdsCajas.FieldByName ('CantidadTAGs').AsInteger;
	    			       	ParamByName ('@CategoriaTAG').Value 			:= cdsCajas.FieldByName ('CategoriaTAGs').AsString;
    	    				ParamByName ('@EstadoControl').Value 		 	:= cdsCajas.FieldByName ('EstadoControl').AsString;
		    	    		ParamByName ('@Observaciones').Value 		 	:= cdsCajas.FieldByName ('Consistencia').AsString;
        		            case cdsCajas.FieldByName ('EstadoControl').AsString[1] of
    	    					'P': ParamByName ('@EstadoSituacion').Value	:= CONST_ESTADO_SITUACION_PENDIENTE;
    	    					'A': ParamByName ('@EstadoSituacion').Value	:= CONST_ESTADO_SITUACION_DISPONIBLE;
		    	    			'R': ParamByName ('@EstadoSituacion').Value	:= CONST_ESTADO_SITUACION_RECHAZADO
        		            end;
        					ParamByName ('@CodigoAlmacenOrigen').Value   	:= 1; // ojo!!!
		        			ParamByName ('@CodigoAlmacenTAG').Value  	 	:= 2; // ojo!!!
	    		    		ParamByName ('@Usuario').Value	 				:= UsuarioSistema;

				        	ExecProc
    				    end
		            end else begin
        		    	// registro los TAGs de la caja uno a uno
						CodigoEmbalaje := cdsCajas.FieldByName ('CodigoEmbalaje').AsInteger;

            		    cdsTAGs.First;
		                while not cdsTAGs.EoF do begin
					    	with spRegistrarControlCalidadManual, Parameters do begin
                                //INICIO:	20160812 CFU
                                Parameters.Refresh;
                                //TERMINO:	20160812 CFU
		    			    	ParamByName ('@CodigoEmbalaje').Value 			:= CodigoEmbalaje;
        						ParamByName ('@CodigoEmbalajePadre').Value		:= CodigoEmbalajePadre;
			        			ParamByName ('@CodigoCajaTAG').Value 			:= cdsCajas.FieldByName ('CodigoCajaTAG').AsInteger;
				        		ParamByName ('@ContractSerialNumber').Value 	:= EtiquetaToSerialNumber(PadL(cdsTAGs.FieldByName('ContractSerialNumber').AsString, 11, '0'));
			    		       	ParamByName ('@CategoriaTAG').Value 			:= cdsCajas.FieldByName ('CategoriaTAGs').AsString;
	   			    			ParamByName ('@EstadoControl').Value 		 	:= cdsTAGs.FieldByName ('Estado').AsString;
			    	    		ParamByName ('@Observaciones').Value 		 	:= cdsCajas.FieldByName ('Consistencia').AsString;
	    		                case cdsCajas.FieldByName ('EstadoControl').AsString[1] of
		    		    			'P': ParamByName ('@EstadoSituacion').Value	:= CONST_ESTADO_SITUACION_PENDIENTE;
    				    			'A': ParamByName ('@EstadoSituacion').Value	:= CONST_ESTADO_SITUACION_DISPONIBLE;
		    	    				'R': ParamByName ('@EstadoSituacion').Value	:= CONST_ESTADO_SITUACION_RECHAZADO
		                	    end;
	    		    			ParamByName ('@CodigoAlmacenOrigen').Value   	:= 1; // ojo!!!
        						ParamByName ('@CodigoAlmacenTAG').Value  	 	:= 2; // ojo!!!
				        		ParamByName ('@Usuario').Value	 				:= UsuarioSistema;

					        	ExecProc;

                		        CodigoEmbalaje := ParamByName ('@CodigoEmbalaje').Value;
		    			    end;

        		            cdsTAGs.Next
                		end
		            end
        		end;

			   	cdsCajas.Next
			end;

			TodoOk:= True;
        except
		    on e: Exception do begin
			    Screen.Cursor := crDefault;

	        	DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ERROR_CONTROL_CALIDAD, e.message, self.caption, MB_ICONSTOP);

			    Exit;
    		end;
        end;
    finally
        Screen.Cursor := crDefault;
        cdsTAGs.EnableControls;
        cdsCajas.EnableControls;

        if TodoOk then begin
            DMConnections.BaseCAC.CommitTrans;

		    if not cdsCajas.isEmpty then
//        MsgBox('Datos grabados con �xito. Imprimir reporte de control de calidad', self.caption, MB_ICONINFORMATION);
		        MsgBox('Datos grabados con �xito', self.caption, MB_ICONINFORMATION);
			btnLimpiarClick(Sender);
        end
    end;
end;

procedure TFControlCalidadPallets.cdsTAGsNewRecord(DataSet: TDataSet);
begin
	DataSet.FieldByName('Estado').AsString := 'ACEPTADO';
	DataSet.FieldByName('IDCaja').AsInteger:= cdsCajas.FieldByName ('CodigoCajaTAG').AsInteger;
    DataSet.FieldByName('RazonRechazo').AsString:= '';
end;

procedure TFControlCalidadPallets.cdsCajasBeforeDelete(DataSet: TDataSet);
begin
    if not Dataset.FieldByName('Editable').AsBoolean then begin
		MsgBox('La caja no puede ser eliminada ya que su contenido fue procesado', self.caption, MB_OK + MB_ICONSTOP);
        abort
    end;
    FListaEmbalajesCDS.Delete(FListaEmbalajesCDS.IndexOf(Dataset.FieldByName('CodigoCajaTAG').AsString))
end;

procedure TFControlCalidadPallets.btnImprimirClick(Sender: TObject);
begin
	if not cdsCajas.IsEmpty then begin
	    if cdsCajas.State in [dsEdit, dsInsert] then cdsCajas.Post;

	    ShowMessage('Imprimir reporte de control de calidad')
    end;
end;

procedure TFControlCalidadPallets.btnLimpiarClick(Sender: TObject);
begin
	cdsCajas.CancelUpdates;

    txtIDPallet.Clear;
    txtIDPallet.SetFocus;
end;

procedure TFControlCalidadPallets.DPSButton1Click(Sender: TObject);
var
    f:TFTAGsImportados;
begin
	if FindFormOrCreate(TFTAGsImportados, f) then
		f.Show
	else begin
        if not f.Inicializar(True) then f.Release;
	end;
end;

function TFControlCalidadPallets.ValidarExistenciaTAG (CodigoTAG : DWORD; StrTipo: ANSIString; ConMsg: Boolean): Byte;
begin
	try
	    with spVerificarTAGEnArchivos, Parameters do begin
    	    ParamByName('@ContractSerialNumber').Value := CodigoTAG;
        	ExecProc;

	       	result := ParamByName('@RETURN_VALUE').Value;
    	    if ConMsg then begin
			    case result of
					1: MsgBox(StrTipo + MSG_ERROR_TAG_YA_INGRESADO, self.Caption, MB_OK + MB_ICONSTOP);
   					2: MsgBox(StrTipo + MSG_ERROR_TAG_NO_EXISTE_EN_LISTA, self.Caption, MB_OK + MB_ICONSTOP);
			    end
    	    end;

        	Close;
	    end
    except
		on e: Exception do begin
        	MsgBoxErr(MSG_ERROR_VALIDACION_EXISTENCIA_TAG, e.message, self.caption, MB_ICONSTOP);
			result := 3
        end
    end
end;

procedure TFControlCalidadPallets.cdsTAGsBeforePost(DataSet: TDataSet);
Var
    TAGActual: DWORD;
begin
	TAGInicial := EtiquetaToSerialNumber(PadL(cdsCajas.FieldByName('NumeroInicialTAG').AsString, 11, '0'));
   	TAGFinal   := EtiquetaToSerialNumber(PadL(cdsCajas.FieldByName('NumeroFinalTAG').AsString, 11, '0'));
   	TAGActual  := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('ContractSerialNumber').AsString, 11, '0'));

    if ValidarExistenciaTAG (TAGActual, 'Telev�a Ingresado', true) <> TAG_EN_INTERFASE then abort;

	// controlo valor del control de calidad
    if 	(UpperCase(DataSet.FieldByName('Estado').AsString) <> 'ACEPTADO') and
        (UpperCase(DataSet.FieldByName('Estado').AsString) <> 'RECHAZADO')
    then begin
		MsgBox('Valor de control de calidad incorrecto', self.caption, MB_OK + MB_ICONSTOP);
        abort
    end;

	// notifico fuera de rango de la caja
    if not ((TAGActual >= TAGInicial) and (TAGActual <= TAGFinal)) and
		(MsgBox('ContextSerialNumber del Telev�a fuera del rango de la caja.' + CRLF + 'Lo ACEPTA?', self.caption, MB_YESNO + MB_ICONINFORMATION) = ID_NO)
    then begin
		DataSet.FieldByName('Estado').AsString := 'RECHAZADO';
    	DataSet.FieldByName('RazonRechazo').AsString := 'Telev�a fuera del rango de la caja'
    end;

   	if DataSet.FieldByName('Estado').AsString = 'ACEPTADO' then begin
    	DataSet.FieldByName('RazonRechazo').AsString := ''
    end else begin
		// notifico falta de Causa de Rechazo
    	if not FCargando and
			(DataSet.FieldByName('Estado').AsString = 'RECHAZADO') and
    		(Trim(DataSet.FieldByName('RazonRechazo').AsString) = '')
	    then begin
			MsgBox('Debe indicarse causa del rechazo', self.caption, MB_OK + MB_ICONSTOP);
        	abort
	    end
    end
end;

function TFControlCalidadPallets.ExisteCaja(CodigoCaja: LongInt): Boolean;
begin
	// esta en archivo?
   	result := QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select 1 From CajasTAGs where CajasTAGs.CodigoCajaTAG = ' + IntToStr(CodigoCaja) + '), 0)') <> 0;
    if result then Exit;

    result := FListaEmbalajesCDS.IndexOf(IntToStr(CodigoCaja)) <> -1;
end;

function TFControlCalidadPallets.ValidarIDCaja(DataSet: TDataSet): Boolean;
begin
	result := Dataset.FieldByName('CodigoCajaTAG').AsInteger > 0;
    if not result then begin
		MsgBox('El ID de la Caja no puede ser 0', 'Telev�as', MB_OK + MB_ICONSTOP);
       	exit
    end;

    result := not ((Dataset.State in [dsInsert]) and ExisteCaja (DataSet.FieldByName('CodigoCajaTAG').AsInteger));
    if not result then begin
		MsgBox('El ID de la Caja ya fue ingresado previamente', 'Telev�as', MB_OK + MB_ICONSTOP);
		exit
	end;
end;

procedure TFControlCalidadPallets.cdsCajasBeforePost(DataSet: TDataSet);
var
    DescriError: AnsiString;
begin
	TAGInicial := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroInicialTAG').AsString, 11, '0'));
   	TAGFinal  := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroFinalTAG').AsString, 11, '0'));

	// validaciones exclu�das al momento de la carga
    if not FCargando then begin
		if not ValidarEditable(self.caption, Dataset.FieldByName('Editable').AsBoolean, true) then abort;
		if not ValidarIDCaja(DataSet) then abort;
		if not ValidarEtiqueta(self.caption, 'Telev�a Inicial', DataSet.FieldByName('NumeroInicialTAG').AsString, true) then abort;
		if not ValidarEtiqueta(self.caption, 'Telev�a Final', DataSet.FieldByName('NumeroFinalTAG').AsString, true) then abort;
		if not ValidarCategoria (self.caption, DataSet.FieldByName('CategoriaTAGs').AsInteger, true) then abort;
		if not ValidarCategoriaTAG(self.caption, 'Telev�a Inicial', TAGInicial, DataSet.FieldByName('CategoriaTAGs').AsInteger) then abort;
		if not ValidarCategoriaTAG(self.caption, 'Telev�a Final', TAGFinal, DataSet.FieldByName('CategoriaTAGs').AsInteger) then abort;
		if not ValidarRangosTAGs(self.caption, TAGInicial, TAGFinal, true, true) then abort;
        if not ValidarCantidadTAGs (self.caption, DataSet.FieldByName('CategoriaTAGs').AsInteger, DataSet.FieldByName('CantidadTAGS').AsInteger, false, true) then abort;
    	if ValidarExistenciaTAG (TAGInicial, 'Telev�a Inicial', true) <> TAG_EN_INTERFASE then abort;
	    if ValidarExistenciaTAG (TAGFinal, 'Telev�a Final', true) <> TAG_EN_INTERFASE then abort;

	    if dbgCajas.Columns[5].PickList.IndexOf(DataSet.FieldByName('EstadoControl').AsString) = -1 then begin
			MsgBox('Valor de control de calidad incorrecto', self.caption, MB_OK + MB_ICONSTOP);
        	abort
	    end;

    	if not ConsistenciaDatosLinea(DataSet, DescriError) then begin
		    case dataset.fieldbyName('EstadoControl').asString[1] of
            	'P': begin
      				MsgBox('Existe la siguiente inconsistencia en la informaci�n: ' + CRLF +
            		  descriError + CRLF + CRLF + 'Se recomienda realizar el control de calidad de forma manual e individual.',
	            	  self.caption, MB_ICONINFORMATION + MB_Ok);
    	    	end;

                'A': begin
	      			if MsgBox('Existe la siguiente inconsistencia en la informaci�n: ' + CRLF + descriError + CRLF + CRLF +
                    	'Se recomienda realizar el control de calidad de forma manual e individual.' + CRLF + CRLF +
                    	'Desea a�n registrar la Caja como ACEPTADA?', self.caption, MB_ICONQUESTION + MB_YESNO) <> mrYes
                    then Dataset.fieldbyName('EstadoControl').asString := 'P'
                end;
            end;

       	    dataset.fieldbyName('Consistencia').asString := DescriError;
	    end
	end;
end;

procedure TFControlCalidadPallets.cdsCajasAfterDelete(DataSet: TDataSet);
begin
	txtCantidadCajasChange(nil);
    txtCantidadTAGsChange(nil);
end;

procedure TFControlCalidadPallets.cdsCajasBeforeScroll(DataSet: TDataSet);
begin
	btnControlManual.Enabled:= not cdsCajas.IsEmpty;
    btnBorrarLinea.Enabled	:= not cdsCajas.IsEmpty;
    btnAceptar.Enabled		:= not cdsCajas.IsEmpty;
end;

procedure TFControlCalidadPallets.btnBorrarLineaClick(Sender: TObject);
begin
	cdsCajas.Delete
end;

end.

