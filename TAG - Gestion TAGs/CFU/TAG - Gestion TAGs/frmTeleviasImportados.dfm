object FTAGsImportados: TFTAGsImportados
  Left = 34
  Top = 266
  Caption = 'Telev'#237'as disponibles para incorporar en Maestro'
  ClientHeight = 178
  ClientWidth = 844
  Color = clBtnFace
  Constraints.MinHeight = 216
  Constraints.MinWidth = 860
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 143
    Width = 844
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      844
      35)
    object lblCantidad: TLabel
      Left = 9
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Cantidad:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 9
      Top = 18
      Width = 57
      Height = 13
      Caption = 'Telev'#237'a #'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 502
      Top = 10
      Width = 111
      Height = 13
      Anchors = [akLeft]
      Caption = 'Visualizar los 1ros.:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Cantidad: TNumericEdit
      Left = 616
      Top = 8
      Width = 62
      Height = 21
      Anchors = [akLeft]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Value = 100.000000000000000000
    end
    object btnActualizar: TButton
      Left = 688
      Top = 5
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = '&Actualizar'
      TabOrder = 1
      OnClick = btnActualizarClick
    end
    object btnSalir: TButton
      Left = 769
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = '&Salir'
      TabOrder = 2
      OnClick = btnSalirClick
    end
  end
  object dblInterfaseTAGs: TDBListEx
    Left = 0
    Top = 0
    Width = 844
    Height = 143
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Etiqueta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Arial'
        Header.Font.Style = []
        Header.Alignment = taCenter
        Sorting = csAscending
        IsLink = False
        OnHeaderClick = DBListEx1Columns0HeaderClick
        FieldName = 'Etiqueta'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 175
        Header.Caption = 'Categor'#237'a Interurbana'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Arial'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListEx1Columns0HeaderClick
        FieldName = 'Categoria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 175
        Header.Caption = 'Categor'#237'a Urbana'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Arial'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'CatUrbana'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 120
        Header.Caption = 'Fecha Movimiento'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Arial'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListEx1Columns0HeaderClick
        FieldName = 'FechaMovimiento'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 130
        Header.Caption = 'Archivo Importaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Arial'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListEx1Columns0HeaderClick
        FieldName = 'ArchivoImportacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Usuario'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Arial'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBListEx1Columns0HeaderClick
        FieldName = 'Usuario'
      end>
    DataSource = dsInterfaseTAGs
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDrawBackground = dblInterfaseTAGsDrawBackground
  end
  object dsInterfaseTAGs: TDataSource
    DataSet = spObtenerTAGsInterfase
    Left = 47
    Top = 52
  end
  object spObtenerTAGsInterfase: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    OnCalcFields = spObtenerTAGsInterfaseCalcFields
    ProcedureName = 'ObtenerTAGsInterfase'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@OrderBy'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 100
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 174
    Top = 51
    object spObtenerTAGsInterfaseContractSerialNumber: TLargeintField
      FieldName = 'ContractSerialNumber'
    end
    object spObtenerTAGsInterfaseFechaMovimiento: TDateTimeField
      FieldName = 'FechaMovimiento'
    end
    object spObtenerTAGsInterfaseArchivoImportacion: TStringField
      FieldName = 'ArchivoImportacion'
      FixedChar = True
      Size = 50
    end
    object spObtenerTAGsInterfaseUsuario: TStringField
      FieldName = 'Usuario'
      ReadOnly = True
      Size = 122
    end
    object spObtenerTAGsInterfaseEtiqueta: TStringField
      FieldKind = fkCalculated
      FieldName = 'Etiqueta'
      Size = 11
      Calculated = True
    end
    object spObtenerTAGsInterfaseCategoria: TStringField
      FieldName = 'Categoria'
      FixedChar = True
      Size = 60
    end
    object spObtenerTAGsInterfaseCatUrbana: TStringField
      FieldName = 'CatUrbana'
      Size = 60
    end
  end
end
