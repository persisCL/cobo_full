{-----------------------------------------------------------------------------
 File Name: 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 26/02/2009
Author: pdominguez
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit frmRptStockPuntosEntrega;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppCtrls, ppBands, ppClass, ppPrnabl, ppDB, ppCache, DB, ADODB,
  ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, UtilRB, ppModule, raCodMod,
  ppStrtch, ppSubRpt, ppParameter, Util, UtilProc;

type
  TFormRptStockPuntosEntrega = class(TForm)
	rbiListado: TRBInterface;
    ppDBPipeline1: TppDBPipeline;
    ppReporteStockPuntosEntrega: TppReport;
    spObtenerStockPuntosEntrega: TADOStoredProc;
    dsObtenerStockPuntosEntrega: TDataSource;
	spObtenerStockPuntosEntregaCodigoPuntoEntrega: TIntegerField;
	spObtenerStockPuntosEntregaPuntoEntrega: TStringField;
	spObtenerStockPuntosEntregaCodigoCategoria: TWordField;
	spObtenerStockPuntosEntregaCategoria: TStringField;
	spObtenerStockPuntosEntregaCodigoAlmacenRecepcion: TIntegerField;
	spObtenerStockPuntosEntregaAlmacenRecepcion: TStringField;
	spObtenerStockPuntosEntregaStockActualRecepcion: TIntegerField;
	spObtenerStockPuntosEntregaCodigoAlmacenProveedor: TIntegerField;
	spObtenerStockPuntosEntregaAlmacenProveedor: TStringField;
	spObtenerStockPuntosEntregaStockActualProveedor: TIntegerField;
	spObtenerStockPuntosEntregaPedidoPendiente: TIntegerField;
	spObtenerStockPuntosEntregaTotal: TIntegerField;
    qryTAGs: TADOQuery;
    ppDBPipeline2: TppDBPipeline;
    DataSource1: TDataSource;
    ppParameterList1: TppParameterList;
    ppHeaderBand1: TppHeaderBand;
    ppLabel9: TppLabel;
    pplblUsuario: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLine1: TppLine;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLine2: TppLine;
    DetalleTAGs: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
	ppLabel12: TppLabel;
	ppLine4: TppLine;
	ppDetailBand2: TppDetailBand;
	ppDBText9: TppDBText;
	ppDBText10: TppDBText;
	ppDBText11: TppDBText;
	ppSummaryBand1: TppSummaryBand;
	ppLine3: TppLine;
	raCodeModule1: TraCodeModule;
	procedure spObtenerStockPuntosEntregaCalcFields(DataSet: TDataSet);
	procedure ppReporteStockPuntosEntregaAfterPrint(Sender: TObject);
	procedure ppReporteStockPuntosEntregaBeforePrint(Sender: TObject);
    procedure rbiListadoSelect(Sender: TObject);
  private
	{ Private declarations }
	FDetalle: Boolean;
    FPuntoEntrega: Integer;
  public
	{ Public declarations }
	function Inicializar: Boolean;
	function Ejecutar:Boolean;
  end;

var
  FormRptStockPuntosEntrega: TFormRptStockPuntosEntrega;

implementation

uses DMConnection, frmListaPuntosEntrega;

{$R *.dfm}

function TFormRptStockPuntosEntrega.Inicializar: Boolean;
begin
	FDetalle := false;
    FPuntoEntrega := 0;

	result := true;
end;

function TFormRptStockPuntosEntrega.Ejecutar:Boolean;
begin
	 rbiListado.Execute(True);
	 result := true;
end;

procedure TFormRptStockPuntosEntrega.ppReporteStockPuntosEntregaBeforePrint(Sender: TObject);
begin
	spObtenerStockPuntosEntrega.Close;
	spObtenerStockPuntosEntrega.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FPuntoEntrega;
	spObtenerStockPuntosEntrega.Open;

    //INICIO	: 20160624 CFU
    //Correcci�n error en consulta qryTAGs, en cl�usula order by dec�a :
    //MT.Categoria, MT.ContractSerialNumber debiendo decir:
    //MT.CodigoCategoria, MT.ContractSerialNumber
    //TERMINO	: 20160624 CFU
	qryTAGs.Open;
	DetalleTAGs.Visible := FDetalle
end;

procedure TFormRptStockPuntosEntrega.spObtenerStockPuntosEntregaCalcFields(DataSet: TDataSet);
begin
	if spObtenerStockPuntosEntregaCodigoAlmacenRecepcion.AsInteger = spObtenerStockPuntosEntregaCodigoAlmacenProveedor.AsInteger then begin
		spObtenerStockPuntosEntregaTotal.AsInteger := spObtenerStockPuntosEntregaStockActualProveedor.AsInteger +
														spObtenerStockPuntosEntregaPedidoPendiente.AsInteger
	end
	else begin
		spObtenerStockPuntosEntregaTotal.AsInteger :=  spObtenerStockPuntosEntregaStockActualRecepcion.AsInteger +
														spObtenerStockPuntosEntregaStockActualProveedor.AsInteger +
														spObtenerStockPuntosEntregaPedidoPendiente.AsInteger
	end
end;

procedure TFormRptStockPuntosEntrega.ppReporteStockPuntosEntregaAfterPrint(Sender: TObject);
begin
	spObtenerStockPuntosEntrega.Close;
	qryTAGs.Close
end;

procedure TFormRptStockPuntosEntrega.rbiListadoSelect(Sender: TObject);
var
	f: TFormListaPuntosEntrega;
begin
    Application.CreateForm(TFormListaPuntosEntrega, f);
  	if f.Inicializar(FDetalle, FPuntoEntrega) then begin
    	f.ShowModal;
		FDetalle	:= f.Detallar;
		FPuntoEntrega:= f.PuntoEntrega;
    end;
	f.Release;
end;

end.
