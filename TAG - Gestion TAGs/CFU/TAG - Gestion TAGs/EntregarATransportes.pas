{-----------------------------------------------------------------------------
 File Name: 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 26/02/2009
Author: pdominguez
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
unit EntregarATransportes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, BuscaClientes, DmiCtrls,Util, UtilDB, UtilProc, StrUtils,
  DMConnection, PeaProcs, DB, ADODB, ExtCtrls, DPSControls, Buttons,
  DBCtrls, peatypes, VariantComboBox, Grids, DBGrids, DBClient,
  freMovimientoStock;

type
  TfrmEntregarATransportes = class(TForm)
	btn_aceptar: TDPSButton;
	btn_cancelar: TDPSButton;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cbTransportistas: TVariantComboBox;
    qryOrigen: TADOQuery;
    qryDestino: TADOQuery;
    cbOrigen: TVariantComboBox;
    cbDestino: TVariantComboBox;
    txtGuiaDespacho: TEdit;
    spGenerarGuiasDespacho: TADOStoredProc;
    frameMovimientoStock: TframeMovimientoStock;
    btnBorrarLinea: TDPSButton;
	procedure ImprimirAnexo(CodigoGuiaDespacho: LongInt; dsTAgs: TDataSource);
	procedure btn_aceptarClick(Sender: TObject);
	procedure btn_cancelarClick(Sender: TObject);
    procedure cbOrigenChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBorrarLineaClick(Sender: TObject);
  private
    FCodigoBodegaOrigen		: integer;
	FUsuarioAdministrador	: boolean;
	procedure CargarAlmacenesOrigen(CodigoAlmacen, CodigoOperacionTAG: Integer);
	procedure CargarAlmacenesDestino(CodigoAlmacen, CodigoOperacionTAG: Integer);
  public
    function Inicializar(MDIChild: boolean; aCaption: AnsiString; CodigoAlmacenOrigen: integer = -1):Boolean;
  end;

const
    OP_TAG_ENTREGA_A_TRANSPORTE = 8;
    OP_TAG_RECEPCION_TRANSPORTE = 9;

implementation

uses frmRptAnexoEntregas;

{$R *.dfm}

procedure TfrmEntregarATransportes.CargarAlmacenesOrigen(CodigoAlmacen, CodigoOperacionTAG: Integer);
var
    i: Integer;
begin
    i := -1;
    qryOrigen.Parameters.ParamByName('CodigoOperacion').value := CodigoOperacionTAG;
 	qryOrigen.Open;
    While not qryOrigen.Eof do begin
    	cbOrigen.Items.Add(qryOrigen.FieldByName('Descripcion').AsString, qryOrigen.FieldByName('CodigoAlmacen').AsInteger);
		if qryOrigen.FieldByName('CodigoAlmacen').AsInteger = CodigoAlmacen then i := cbOrigen.Items.Count - 1;
		qryOrigen.Next;
	end;
	cbOrigen.ItemIndex := i;
end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
Procedure TfrmEntregarATransportes.CargarAlmacenesDestino(CodigoAlmacen, CodigoOperacionTAG: Integer);
var
    i: Integer;
begin
    i := -1;
    cbDestino.Clear;
    qryDestino.Close;
    qryDestino.SQL.Text := 'select  MaestroAlmacenes.CodigoAlmacen, MaestroAlmacenes.Descripcion ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'from AlmacenesDestinoOperacion WITH (NOLOCK), MaestroAlmacenes WITH (NOLOCK) ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'where  AlmacenesDestinoOperacion.codigoALmacen = MaestroAlmacenes.CodigoAlmacen AND ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'CodigoOperacion = ' + inttostr(CodigoOperacionTAG);
    if not FUsuarioAdministrador then begin
        qryDestino.SQL.Text := qryDestino.SQL.Text + ' AND MaestroAlmacenes.CodigoBodega = ' + inttostr(FCodigoBodegaOrigen);
    end;

 	qryDestino.Open;
    While not qryDestino.Eof do begin
    	cbDestino.Items.Add(qryDestino.FieldByName('Descripcion').AsString, qryDestino.FieldByName('CodigoAlmacen').AsInteger);
		if qryDestino.FieldByName('CodigoAlmacen').AsInteger = CodigoAlmacen then i := cbDestino.Items.Count - 1;
		qryDestino.Next;
	end;
	cbDestino.ItemIndex := i;
end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TfrmEntregarATransportes.Inicializar(MDIChild: boolean; aCaption: AnsiString; CodigoAlmacenOrigen: integer = -1): Boolean;
Var
	S: TSize;
begin
    result := True;

	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

    CenterForm(Self);

	Caption := AnsiReplaceStr(aCaption, '&', '');
    FCodigoBodegaOrigen  := QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select CodigoBodega From MaestroAlmacenes WITH (NOLOCK) where MaestroAlmacenes.CodigoAlmacen = ' + IntToStr(CodigoAlmacenOrigen) + '), -1)');
	FUsuarioAdministrador:= QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select 1 From GruposUsuario WITH (NOLOCK) where GruposUsuario.CodigoUsuario = ' + QuotedStr(UsuarioSistema) + ' and CodigoGrupo = ' + QuotedStr('Administradores') + '), 0)') = 1;

    try
        CargarTransportistas(DMConnections.BaseCAC, cbTransportistas);
//        if CodigoAlmacenOrigen = -1 then begin
        if FUsuarioAdministrador then begin
            CargarAlmacenesOrigen(1, OP_TAG_ENTREGA_A_TRANSPORTE);
            frameMovimientoStock.inicializar(cbOrigen.Value)
//            frameMovimientoStock.inicializar(1);
        end
        else begin
            frameMovimientoStock.inicializar(CodigoAlmacenOrigen);
            CargarAlmacenesOrigen(CodigoAlmacenOrigen, OP_TAG_ENTREGA_A_TRANSPORTE);
            cbOrigen.Enabled := False;

            if (cbOrigen.ItemIndex < 1) then begin
                MsgBox('El almac�n origen no existe o no puede realizar esta operaci�n',self.Caption,MB_ICONSTOP);
                result := false;
                Close;
                Exit;
            end;
        end;

        CargarAlmacenesDestino(-1, OP_TAG_RECEPCION_TRANSPORTE);
    except
	    result := False;
	end;

    txtGuiaDespacho.Clear
end;

Procedure TfrmEntregarATransportes.ImprimirAnexo(CodigoGuiaDespacho: LongInt; dsTAgs: TDataSource);
var
    f: TFormRptAnexoEntregas;
begin
	Application.CreateForm(TFormRptAnexoEntregas, f);
   	if not f.Inicializar (CodigoGuiaDespacho, dsTAGs) then exit;
    if f.Ejecutar then f.Free;
end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TfrmEntregarATransportes.btn_aceptarClick(Sender: TObject);
var
	CodigoGuiaDespacho: LongInt;
    CodigoAlmacenTransportista: integer;
resourceString
    CAPTION_VALIDAR_ENTREGA	= 'Entregar a Transportes';
    ERROR_VALIDAR_TRANSPORTE= 'Debe seleccionarse un Transporte';
    ERROR_VALIDAR_ORIGEN 	= 'Debe seleccionarse un Almac�n Origen';
    ERROR_VALIDAR_DESTINO	= 'Debe seleccionarse un Almac�n Destino';
    ERROR_VALIDAR_ALMACENES	= 'Los almacenes Origen/Destino no pueden ser los mismos';
    ERROR_VALIDAR_GUIA   	= 'Debe indicarse una Gu�a de Despacho';
begin
    if not ValidateControls([cbTransportistas, cbOrigen, cbDestino, cbDestino, txtGuiaDespacho],
    	[(cbTransportistas.Value <> 0), (cbOrigen.ItemIndex <> -1), (cbDestino.ItemIndex <> -1), (cbOrigen.Value <> cbDestino.Value), (Trim(txtGuiaDespacho.Text) <> '')],
        CAPTION_VALIDAR_ENTREGA,
        [ERROR_VALIDAR_TRANSPORTE, ERROR_VALIDAR_ORIGEN, ERROR_VALIDAR_DESTINO, ERROR_VALIDAR_ALMACENES, ERROR_VALIDAR_GUIA])
    then begin
    	Exit
    end;

    CodigoAlmacenTransportista := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT AlmacenAsociado FROM MaestroTransportistas WITH (NOLOCK) WHERE CodigoTransportista = ''' + inttostr(cbTransportistas.Items[cbTransportistas.ItemIndex].Value) + '''');

    if frameMovimientoStock.ValidarMovimiento(CodigoAlmacenTransportista) then begin
        try
            // Agrego Guia Despacho
            with spGenerarGuiasDespacho, Parameters do begin
                ParamByName ('@NumeroGuiaDespacho').Value 	:= Trim(txtGuiaDespacho.Text);
                ParamByName ('@CodigoAlmacenOrigen').Value 	:= cbOrigen.Value;
                ParamByName ('@CodigoAlmacenDestino').Value := cbDestino.Value;
                ParamByName ('@CodigoTransportista').Value 	:= cbTransportistas.Value;

                ExecProc;
                CodigoGuiaDespacho := ParamByName ('@CodigoGuiaDespacho').Value;

                if frameMovimientoStock.GenerarMovimiento(ParamByName ('@CodigoAlmacenTransporte').Value, CodigoGuiaDespacho, OP_TAG_ENTREGA_A_TRANSPORTE) then
                    Close
                else begin
                    QueryExecute(DMConnections.BaseCAC, 'DELETE FROM GUIASDESPACHO WHERE CodigoGuiaDespacho = ' + inttostr(CodigoGuiaDespacho));
                    Exit;
                end;

            end;
        except
			on e: Exception do begin
            	MsgBoxErr('Error al generar el movimiento', e.message, self.caption, MB_ICONSTOP);
                Exit;
            end
        end;

        MsgBox('Datos grabados con �xito. Se imprimir� un anexo a la Gu�a de Despacho', self.caption, MB_ICONINFORMATION);
        ImprimirAnexo (CodigoGuiaDespacho, frameMovimientoStock.dsTAGs);

        frameMovimientoStock.Limpiar;

        cbDestino.ItemIndex := -1;
        txtGuiaDespacho.Clear;
        frameMovimientoStock.CodigoAlmacenOrigen := cbOrigen.Value;
        if cbOrigen.Enabled
        	then cbOrigen.SetFocus
        	else cbDestino.SetFocus
      end;
end;

procedure TfrmEntregarATransportes.btn_cancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmEntregarATransportes.cbOrigenChange(Sender: TObject);
begin
    frameMovimientoStock.CodigoAlmacenOrigen := cbOrigen.Value;
end;

procedure TfrmEntregarATransportes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmEntregarATransportes.btnBorrarLineaClick(Sender: TObject);
begin
    if (frameMovimientoStock.cdsTAGs.Active) and (frameMovimientoStock.cdsTAGs.RecordCount > 0) then
    	frameMovimientoStock.cdsTAGs.Delete
end;

end.
