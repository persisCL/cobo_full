unit frmInterfaseProveedorTAG;

interface
{-----------------------------------------------------------------------------
 Revision 1
 	Author: FSandi
    Date: 09/08/2005
    Description: Se agrega la columna propiedad a la tabla InterfaseTAGs y a la tabla
				MaestroTags para identificar si el televia es MOP o CN

 Firma		: SS_1147_MCA_20140408
 Descripcion: se elimina del combo CbPropiedad la concesionaria CN y se agrega la concesionaria
        	nativa.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

 -----------------------------------------------------------------------------}
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Util, DB, DBClient, ComCtrls, DmiCtrls, BuscaTab,
  DPSControls, UtilProc, StrUtils, ADODB, PeaProcs;

type
	TEFCContextMarkLong = packed record
		ContractProvider: array[1..6] of char;
        NotUsed: array[1..2] of char;
		TypeOfContract: array[1..2] of char;
		ContextVersion: array[1..2] of char;
	end;


  TFInterfaseProveedorTAG = class(TForm)
    pbProgreso: TProgressBar;
    BuscaArchivo: TBuscaTabEdit;
    Open: TOpenDialog;
    Label1: TLabel;
    lblImportacion: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    numExitosos: TNumericEdit;
    numRegistros: TNumericEdit;
    numErroneos: TNumericEdit;
    memResultado: TMemo;
    spBorrarMovimientosImportacionRecepcionTAGsNulos: TADOStoredProc;
    spRegistarLineaInterfase: TADOStoredProc;
    spVerificarInterfaseTAGs: TADOStoredProc;
    spObtenerDatosAImportar: TADOStoredProc;
    spActualizarMovimientosImportacionRecepcionTAGs: TADOStoredProc;
    spPasarAInterfase: TADOStoredProc;
    lblVerificando: TLabel;
    spObtenerDetalleTAGsAImportar: TADOStoredProc;
    btnCancelar: TButton;
    btnSalir: TButton;
    Label6: TLabel;
    CbPropiedad: TComboBox;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscaArchivoButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FSCM: String;
    FACM: Array[1..12] of Char;
    CodigoOperacion,
    CantidadErrores: Integer;
   	DescriError: array[0..255] of char;
    FNombreArchivoProveedor,
    FNombreArchivoErrores: TFileName;
    FArchivoEnMemoria,
    FListaErrores: TStringList;
    FEtiqueta: String;
 	FContractSerialNumber: DWORD;
    FFechaPersonalizacion: TDate;
    FFechaPersLeida: String;
    FCategoria: Integer;
    //INICIO:	20160811 CFU
    FCodConcesionaria, FCategoriaUrbana, FTypeOfContract, FContextVersion, FCodigoProveedorRNUT: Integer;
    //TERMINO:	20160811 CFU
	FContextMark: ^TEFCContextMarkLong;
    FCancel: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	procedure Limpiar;
	procedure RegistrarOperacionErronea (CodigoOperacion: Integer; TotalRegistros, Errores: LongInt; Error, evento:ANSIString);
    function CargarArchivo(NombreArchivo: TFileName; var Error: AnsiString):Boolean;
	function AnalizarErrores(var CantErrores: Integer): integer;
	Function PasarAInterfase: Boolean;
  public
    { Public declarations }
    function Inicializar(txtCaption: String; MDIChild: Boolean):Boolean;
  end;

var
  FInterfaseProveedorTAG: TFInterfaseProveedorTAG;

implementation

uses DMConnection, PeaTypes, RStrings;

{$R *.dfm}

resourcestring
    MSG_IMPORTACION_EXITOSA				= 'Importaci�n de datos del proveedor exitosa.';
	MSG_IMPORTACION_CANCELADA 			= 'Importaci�n cancelada.';

	MSG_ERROR_ARCHIVO_VACIO				= 'El archivo de texto del proveedor %s est� vac�o.';
    MSG_ERROR_LECTURA_ARCHIVO 			= 'El archivo de texto del proveedor %s no pudo leerse.';
    MSG_ERROR_IMPORTACION	  			= 'Errores al importar datos desde el archivo del proveedor.';
    MSG_ERROR_VERIFICAR_ERRORES			= 'Error al verificar errores';
	MSG_ERROR_REGISTRO_IMPORTACION		= 'La importaci�n de datos no ha podido realizarse correctamente.';
	MSG_ERROR_ERRORES_ENCONTRADOS		= 'Se han encontrado errores en los datos.' + CRLF + 'Se ha registrado el evento y se ha generado el archivo %s';
    MSG_ERROR_INSERTANDO_ARCHIVO_INTERFAZ = 'Se produjo un error al generando los registros de la interfaz.';
    MSG_EVENTO_ERROR                    = 'Importaci�n de datos del proveedor cancelada por error.';
    MSG_ERROR_ERRORES_EN_REGISTRO		= 'Los datos en el archivo de texto del proveedor %s no han podido registrarse completamente.';
	MSG_ERROR_ARCHIVO_PROVEEDOR			= 'Error al procesar el archivo del proveedor.';

    MSG_CANCELAR_PROCESO                = 'Para cerrar la ventana primero se debe cancelar el proceso.'; 

function TFInterfaseProveedorTAG.Inicializar(txtCaption: String; MDIChild: Boolean):Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
   	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

    CbPropiedad.Items.Add(ObtenerNombreCortoConcesionariaNativa);               //SS_1147_MCA_20140408

    Limpiar;
	result := true;
end;

procedure TFInterfaseProveedorTAG.btnCancelarClick(Sender: TObject);
begin
    FCancel := True;
    pbProgreso.Position := 0;
    lblVerificando.Caption := '';
end;

procedure TFInterfaseProveedorTAG.btnSalirClick(Sender: TObject);
begin
	Close
end;

procedure TFInterfaseProveedorTAG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFInterfaseProveedorTAG.Limpiar;
begin
	numExitosos.Clear;
    numRegistros.Clear;
	numErroneos.Clear;
    memResultado.Lines.Clear;
    btnCancelar.Enabled := False;

	BuscaArchivo.Text := 'Seleccionar Archivo a Procesar...';
end;


procedure TFInterfaseProveedorTAG.RegistrarOperacionErronea (CodigoOperacion: Integer; TotalRegistros, Errores: LongInt; Error, Evento: ANSIString);
begin
    ActualizarRegistroOperacion(
      DMCOnnections.BaseCAC,
      SYS_GESTION_TAGS,
      CodigoOperacion,
      SEV_ALTA,
      False,
      Evento,
      TotalRegistros - Errores,
      Errores,
      ExtractFileName (BuscaArchivo.Text),
      Error);

	// se genera archivo de errores
	FListaErrores.SaveToFile (FNombreArchivoErrores);
    Run ('notepad.exe '+ FNombreArchivoErrores);

	btnCancelar.Enabled := False;
    numExitosos.ValueInt := TotalRegistros - Errores;
    numErroneos.ValueInt := Errores;
    memResultado.Lines.Add(MSG_IMPORTACION_CANCELADA + CRLF + Error)
end;


function TFInterfaseProveedorTAG.CargarArchivo(NombreArchivo: TFileName; Var Error: AnsiString): Boolean;
var
    f: TextFile;
    Linea: String;
begin
    Result := False;
	if not Assigned(FArchivoEnMemoria) then Exit;
	Screen.Cursor := crHourGlass;
	AssignFile(f, NombreArchivo);
    try
        Reset(f);
        FArchivoEnMemoria.Clear;
        while not Eof(f) do begin
			ReadLn(f, Linea);
			FArchivoEnMemoria.Add(Linea);
        end;
        CloseFile(f);

        if FArchivoEnMemoria.Count > 0 then begin
            Result := True
        end else begin
		    btnCancelar.Enabled := False;
        	memResultado.Lines.Add(MSG_IMPORTACION_CANCELADA + CRLF +
              Format(MSG_ERROR_ARCHIVO_VACIO, [ExtractFileName (NombreArchivo)]))
        end
    except
        on e: Exception do begin
	        btnCancelar.Enabled := False;
            memResultado.Lines.Add(MSG_IMPORTACION_CANCELADA + CRLF +
			  Format(MSG_ERROR_LECTURA_ARCHIVO, [ExtractFileName (NombreArchivo)]));
              Error := e.Message;
        end;
    end;
	Screen.Cursor := crDefault;
end;

function TFInterfaseProveedorTAG.AnalizarErrores(var CantErrores: Integer): integer;
    // registra un error en lista de errores
	function RegistrarError (LineaError: Integer; TextoError: ANSIString): boolean;
    begin
	    FListaErrores.Add('L�nea ' + IStr0(LineaError, 6) + ': ' + TextoError);
    	result := true;
    end;
var
    Errores : ANSIString;
begin
	Screen.Cursor := crHourGlass;
    try
		try
        	lblVerificando.Caption := 'Verificando informaci�n, por favor aguarde...';
            lblVerificando.Update;
            spVerificarInterfaseTAGs.close;
            spVerificarInterfaseTAGs.Parameters.Refresh;
			spVerificarInterfaseTAGs.Open;
            result := spVerificarInterfaseTAGs.RecordCount;

            if result <> 0 then begin
				with spVerificarInterfaseTAGs do begin
                	First;

                    while not EoF do begin
                        Errores := iif (FieldByName('ErrorContractSerialNumber').AsString = '', '', FieldByName('ErrorContractSerialNumber').AsString + ' * ') +
								   iif (FieldByName('ErrorContextMark').AsString = '', '', FieldByName('ErrorContextMark').AsString + ' * ') +
								   iif (FieldByName('ErrorFechaPersonalizacion').AsString = '', '', FieldByName('ErrorFechaPersonalizacion').AsString + ' * ') +
								   iif (FieldByName('ErrorEnInterfase').AsString = '', '', FieldByName('ErrorEnInterfase').AsString + ' * ') +
								   iif (FieldByName('ErrorEnMaestro').AsString = '', '', FieldByName('ErrorEnMaestro').AsString + ' * ') +
								   iif (FieldByName('ErrorCategoria').AsString = '', '', FieldByName('ErrorCategoria').AsString);

    	   				RegistrarError (FieldByName('Linea').AsInteger, Errores);
                    	Next
                    end;
                    CantErrores := recordCount;
                end
            end;
        except
			on e: Exception do begin
	            MsgBoxErr(MSG_ERROR_VERIFICAR_ERRORES, e.Message, self.Caption, MB_ICONERROR);

                result := -1;
            	memResultado.Lines.Add(MSG_IMPORTACION_CANCELADA + CRLF + MSG_ERROR_VERIFICAR_ERRORES)
            end
        end
    finally
       	lblVerificando.Caption := '';
        lblVerificando.Update;
		Screen.Cursor := crDefault;
    end
end;

Function TFInterfaseProveedorTAG.PasarAInterfase:Boolean;
var
	CodigoMovimiento: LongInt;
    TodoOk: Boolean;
begin
	Screen.Cursor := crHourGlass;
    TodoOK := False;
	try
    	try
        	with spObtenerDetalleTAGsAImportar do begin
            	Open;

                while not EoF do begin
					// Genero Cabecera de Movimiento con fecha NULL
					with spActualizarMovimientosImportacionRecepcionTAGs, Parameters do begin
       					Close;
                        Parameters.Refresh;
		        		ParamByName('@CodigoMovimiento').Value 	:= 0;
				        ParamByName('@Usuario').Value 			:= UsuarioSistema;
				    	ParamByName('@ArchivoImportacion').Value:= ExtractFileName (BuscaArchivo.Text);
       					ParamByName('@CantidadMovimiento').Value:= spObtenerDetalleTAGsAImportar.FieldByName ('CantidadMovimiento').AsInteger;
       					ParamByName('@CodigoCategoria').Value	:= spObtenerDetalleTAGsAImportar.FieldByName ('CodigoCategoria').AsInteger;
		       			ParamByName('@TipoMovimiento').Value 	:= 1;

				        ExecProc;

    		    		CodigoMovimiento := ParamByName('@CodigoMovimiento').Value;
			    		Close
	    		    end;

					with spPasarAInterfase, Parameters do begin
						Close;
                        Parameters.Refresh;
   	    				ParamByName('@CodigoMovimiento').Value 				:= CodigoMovimiento;
   	    				ParamByName('@CodigoCategoria').Value 				:= spObtenerDetalleTAGsAImportar.FieldByName ('CodigoCategoria').AsInteger;
                        //INICIO:	20160811 CFU
   	    				ParamByName('@CodigoCategoriaUrbana').Value 		:= spObtenerDetalleTAGsAImportar.FieldByName ('CodigoCategoriaUrbana').AsInteger;
                        //TERMINO:	20160811 CFU
                        ParamByName('@Propietario').Value := Trim(CbPropiedad.Text); // Revision 1
		    		    ExecProc
        			end;

			        // Actualizo Cabecera de Movimiento con fecha actual
   					with spActualizarMovimientosImportacionRecepcionTAGs, Parameters do begin
   	   					Close;
		   				ParamByName('@CodigoMovimiento').Value 	:= CodigoMovimiento;

				        ExecProc
                    end;

                    Next
                end;

                Close
    	    end;
            TodoOK := True;
        except
		    on e: Exception do MsgBoxErr(MSG_ERROR_REGISTRO_IMPORTACION, e.message, self.caption, MB_ICONSTOP);
        end
	finally
		Screen.Cursor := crDefault;

        if TodoOk then begin
	        numExitosos.ValueInt := numRegistros.ValueInt;
   			memResultado.Lines.Add(MSG_IMPORTACION_EXITOSA)
        end else begin
			memResultado.Lines.Add(MSG_IMPORTACION_CANCELADA + CRLF + MSG_ERROR_REGISTRO_IMPORTACION)
        end;
        Result := TodoOk;
    end;
end;

procedure TFInterfaseProveedorTAG.BuscaArchivoButtonClick(Sender: TObject);
// Revision 1
resourcestring
    MSG_ERROR_SELECCIONAR_PROPIETARIO   = 'Debe Seleccionar un Propietario para el Archivo';
    MSG_ERROR_IMPORTAR_ARCHIVO          = 'Importar Archivo de Proveedor';
    {INICIO:	20160804 CFU
    MSG_ERROR_PROPIETARIO_INCORRECTO    = 'El Propietario Seleccionado es Incorrecto, solo se permite Propiedad MOP o CN';
    TERMINO:	20160804 CFU}
// Fin de Revision 1
var
    i,p: Integer;
    Error: AnsiString;
    ActualDS: Char;
    ActualSDF: String;
    TodoOk: Boolean;
    CantErrores: INteger;
    //INICIO:	20160804 CFU
    strNombreCortoConcesionariaNativa, strMensajeError: string;
    //TERMINO:	20160804 CFU
begin
// Revision 1
    //if not ((CbPropiedad.Text='MOP') or (CbPropiedad.Text = 'CN')) then begin	//SS_1147_MCA_20140408
    {INICIO:	20160804 CFU
    if not ((CbPropiedad.Text='MOP') or (CbPropiedad.Text = ObtenerNombreCortoConcesionariaNativa)) then begin	//SS_1147_MCA_20140408
        MsgBoxBalloon(MSG_ERROR_PROPIETARIO_INCORRECTO,MSG_ERROR_IMPORTAR_ARCHIVO,MB_ICONSTOP,CbPropiedad);
    }
    strNombreCortoConcesionariaNativa 	:= ObtenerNombreCortoConcesionariaNativa;
    strMensajeError						:= 'El Propietario Seleccionado es Incorrecto, solo se permite Propiedad MOP o ' + strNombreCortoConcesionariaNativa;
    if not ((CbPropiedad.Text='MOP') or (CbPropiedad.Text = strNombreCortoConcesionariaNativa)) then begin	//SS_1147_MCA_20140408
        MsgBoxBalloon(strMensajeError, MSG_ERROR_IMPORTAR_ARCHIVO,MB_ICONSTOP,CbPropiedad);
        //TERMINO:	20160804 CFU
        Exit;
    end;

// Fin de Revision 1
    FCancel := False;
    Limpiar;

    if not Open.Execute then begin
	    BuscaArchivo.Text := 'Seleccionar Archivo a Procesar...';
    	Exit;
// Revision 1
    end else begin
        if Trim(CbPropiedad.Text) <> EmptyStr then begin
            if not(MsgBox('�Est� seguro de procesar el archivo: ' +
              ExtractFileName(Open.FileName) + ' con Propiedad '+ CbPropiedad.Text +'?', MSG_ERROR_IMPORTAR_ARCHIVO,
              MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;

            FNombreArchivoProveedor := Open.FileName;
            FNombreArchivoErrores := ChangeFileExt(FNombreArchivoProveedor, '.ERR');
            BuscaArchivo.Text := FNombreArchivoProveedor;
            BuscaArchivo.Enabled := false
        end else begin
            FNombreArchivoProveedor := EmptyStr;
            FNombreArchivoErrores :=EmptyStr;
            BuscaArchivo.Text := EmptyStr;
            BuscaArchivo.Enabled := True;
            MsgBoxBalloon(MSG_ERROR_SELECCIONAR_PROPIETARIO,MSG_ERROR_IMPORTAR_ARCHIVO,MB_ICONSTOP,CbPropiedad);
            Exit;
        end;
    end;
// Fin de Revision 1
    FArchivoEnMemoria := TStringList.Create;

    try
        if not CargarArchivo(FNombreArchivoProveedor, Error) then begin
            MsgBoxErr('Error al abrir el archivo', Error, self.Caption, MB_ICONERROR);

            Exit;
        end else begin
            btnCancelar.Enabled := true;
        end;

        numRegistros.ValueInt := FArchivoEnMemoria.Count;
        pbProgreso.Max := FArchivoEnMemoria.Count;
	    FListaErrores := TStringList.Create;

        TodoOk := False;
	    DMConnections.BaseCAC.BeginTrans;
        try
        	try
    		    // Borro TAGs y Cabeceras de Movimientos hu�rfanos
    			spBorrarMovimientosImportacionRecepcionTAGsNulos.ExecProc;
				CantidadErrores := 0;

                // Genero el registro de operaci�n
      		 	CodigoOperacion := CrearRegistroOperaciones(
          			DMCOnnections.BaseCAC,
               		SYS_GESTION_TAGS,
	                RO_MOD_IMPORTACION_TAGS,
			  	    RO_AC_IMPORTAR_TAGS,
       	    		SEV_INFO,
           	    	GetMachineName,
	            	UsuarioSistema,
   		            'Importaci�n de datos del proveedor exitosa.',
	    		    0,
					FArchivoEnMemoria.Count,
	        	    0,
   		        	ExtractFileName (BuscaArchivo.Text),
  			        DescriError);

	        	// Listo, tenemos el archivo en memoria, lo empezamos a descomponer
	    	    // y lo registro en la tabla auxiliar
    	    	lblVerificando.Caption := 'Cargando archivo, por favor aguarde...';

	    	    btnSalir.Enabled := False;
    	    	for i:= 0 to FArchivoEnMemoria.Count - 1 do begin
		    	    // extrae y valida CSN
		    	    FEtiqueta := ParseParamByNumber(FArchivoEnMemoria.Strings[i], 1, ';');
					FContractSerialNumber:= EtiquetaToSerialNumber(FEtiqueta);
	    		    if (FContractSerialNumber = 0) or (FEtiqueta <> SerialNumberToEtiqueta(IntToStr(FContractSerialNumber))) then begin
						FContractSerialNumber := 0;
		        	end;

                    {INICIO:	20160811 CFU
			        // Extrae fecha
                    FFechaPersLeida := ParseParamByNumber(FArchivoEnMemoria.Strings[i], 2, ';');
                    ActualDS := DateSeparator;
                	ActualSDF:= ShortDateFormat;
                   	DateSeparator := '/';
	   	    		ShortDateFormat := 'mm/dd/yy';
                    try
        			    try
      						FFechaPersonalizacion := StrToDate(FFechaPersLeida);
        			    except
            				on EConvertError do begin
  		        			    FFechaPersonalizacion := NullDate;
		            	    end
                        end;
                    finally;
   	                	DateSeparator := ActualDS;
	    	            ShortDateFormat := actualSDF;
	    		    end;

        			// extrae Categor�a
					FCategoria := HexToInt(ParseParamByNumber(FArchivoEnMemoria.Strings[i], 3, ';'));

			        FSCM := ParseParamByNumber(FArchivoEnMemoria.Strings[i], 4, ';');
    			    for p := 1 to Length(FSCM) do
        				FACM[p] := FSCM[p];
	  				FContextMark := @FACM;
                    }

                    FCodConcesionaria	:= StrToInt(ParseParamByNumber(FArchivoEnMemoria.Strings[i], 2, ';'));
                    FTypeOfContract		:= StrToInt(ParseParamByNumber(FArchivoEnMemoria.Strings[i], 3, ';'));
                    FContextVersion		:= StrToInt(ParseParamByNumber(FArchivoEnMemoria.Strings[i], 4, ';'));
                    FCategoriaUrbana	:= StrToInt(ParseParamByNumber(FArchivoEnMemoria.Strings[i], 5, ';'));
                    FCategoria 			:= StrToInt(ParseParamByNumber(FArchivoEnMemoria.Strings[i], 6, ';'));
                    FCodigoProveedorRNUT:= StrToInt(ParseParamByNumber(FArchivoEnMemoria.Strings[i], 8, ';'));

			        // Extrae fecha
                    FFechaPersLeida := ParseParamByNumber(FArchivoEnMemoria.Strings[i], 9, ';');
                    FFechaPersLeida := Copy(FFechaPersLeida, 1, 2) + '/' + Copy(FFechaPersLeida, 3, 2) + '/' + Copy(FFechaPersLeida, 5, 4);
                    ActualDS := DateSeparator;
                	ActualSDF:= ShortDateFormat;
                   	DateSeparator := '/';
                    {INICIO		: 20160922 CFU
	   	    		ShortDateFormat := 'mm/dd/yy';
                    }
                    ShortDateFormat := 'dd/mm/yyyy';
                    //TERMINO	: 20160922 CFU
                    try
        			    try
      						FFechaPersonalizacion := StrToDate(FFechaPersLeida);
        			    except
            				on EConvertError do begin
  		        			    FFechaPersonalizacion := NullDate;
		            	    end
                        end;
                    finally;
   	                	DateSeparator := ActualDS;
	    	            ShortDateFormat := actualSDF;
	    		    end;
                    //TERMINO:	20160811 CFU

		        	// registra datos le�dos
    			    with spRegistarLineaInterfase, Parameters do begin
   				    	Close;
                        Parameters.Refresh;
						ParamByName('@Linea').Value				 	:= i+1;
        				ParamByName('@Etiqueta').Value 				:= FEtiqueta;
                        {INICIO:	20160811 CFU
						ParamByName('@ContractSerialNumber').Value 	:= FContractSerialNumber;
                        TERMINO:	20160811 CFU}
						ParamByName('@CategoriaLeida').Value 	  	:= FCategoria;
                        //INICIO:	20160811 CFU
                        ParamByName('@CategoriaUrbana').Value 	  	:= FCategoriaUrbana;
                        ParamByName('@CodigoConcesionaria').Value 	:= FCodConcesionaria;
                        //TERMINO:	20160811 CFU
       			    	ParamByName('@FechaPersonalizacion').Value 	:= iif(FFechaPersonalizacion = Nulldate, null, FFechaPersonalizacion);
       	    			ParamByName('@FechaPersLeida').Value 		:= FFechaPersLeida;
       					ParamByName('@FSCM').Value 					:= FSCM;

                        {INICIO:	20160811 CFU
		        		ParamByName('@ContractProvider').Value 		:= HexToInt(FContextMark^.ContractProvider);
		    	    	ParamByName('@TypeOfContract').Value 		:= HexToInt(FContextMark^.TypeOfContract);
    	   				ParamByName('@ContextVersion').Value 		:= HexToInt(FContextMark^.ContextVersion);
                        }
		        		ParamByName('@ContractProvider').Value 		:= FCodigoProveedorRNUT;
		    	    	ParamByName('@TypeOfContract').Value 		:= FTypeOfContract;
    	   				ParamByName('@ContextVersion').Value 		:= FContextVersion;
                        ParamByName('@CodigoOperacion').Value 		:= CodigoOperacion;
                        
    			        ExecProc;
                	end;

					pbProgreso.StepIt;

					Application.ProcessMessages;
        		    if FCancel then Exit;
            	end;

   			    // Paso a la interfase si no hay errores de formato
                // Si hay errores actualizo el registro de operaciones
   			    if AnalizarErrores(CantErrores) = 0 then begin
                    if not PasarAInterfase() then begin
                        RegistrarOperacionErronea(CodigoOperacion, 0, 0, MSG_ERROR_INSERTANDO_ARCHIVO_INTERFAZ, MSG_EVENTO_ERROR);
                    end;
                end else begin
                    RegistrarOperacionErronea(CodigoOperacion, numRegistros.ValueInt, CantErrores,
                      Format(MSG_ERROR_ERRORES_ENCONTRADOS, [ExtractFileName(FNombreArchivoErrores)]), MSG_EVENTO_ERROR);
                end;

				TodoOk := True
            except
				on e: Exception do begin
					DMConnections.BaseCAC.RollbackTrans;
                   	MsgBoxErr(MSG_ERROR_REGISTRO_IMPORTACION, e.message, self.caption, MB_ICONSTOP)
                end;
            end
        finally
	        if TodoOk then begin
    	        DMConnections.BaseCAC.CommitTrans;
			end else begin
				if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;

				if FCancel then begin
                	memResultado.Lines.Add('Cancelada por el usuario.');
    	        end else
                	memResultado.Lines.Add(MSG_IMPORTACION_CANCELADA + CRLF + MSG_ERROR_ARCHIVO_PROVEEDOR);
            end
        end;
    finally
        BuscaArchivo.Enabled := true;

        pbProgreso.Position := 1;
	    btnCancelar.Enabled := False;
        btnSalir.Enabled := True;

        FreeAndNil(FArchivoEnMemoria);
        FreeAndNil(FListaErrores);
    end;
end;

procedure TFInterfaseProveedorTAG.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    if btnCancelar.Enabled then begin
        msgBox(MSG_CANCELAR_PROCESO, caption, MB_ICONINFORMATION);
        CanClose := False;
    end else
        CanClose := True;    
end;

end.

