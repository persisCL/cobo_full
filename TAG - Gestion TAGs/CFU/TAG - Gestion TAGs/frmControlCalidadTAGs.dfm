object FControlCalidadTAGs: TFControlCalidadTAGs
  Left = 265
  Top = 132
  BorderStyle = bsToolWindow
  Caption = 'Control de Calidad Manual de Telev'#237'as'
  ClientHeight = 478
  ClientWidth = 613
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object lblPallet: TLabel
    Left = 9
    Top = 6
    Width = 65
    Height = 13
    Caption = 'Pallet ID: 0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblIDCaja: TLabel
    Left = 9
    Top = 24
    Width = 58
    Height = 13
    Caption = 'Caja ID: 0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblCantidadTAGs: TLabel
    Left = 9
    Top = 42
    Width = 95
    Height = 13
    Caption = 'Telev'#237'as: 0 de 0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object dbgTAGS: TDBGrid
    Left = 9
    Top = 60
    Width = 595
    Height = 379
    DataSource = dsCajas
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ContractSerialNumber'
        Width = 124
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Estado'
        PickList.Strings = (
          'APROBADO'
          'RECHAZADO')
        Width = 114
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RazonRechazo'
        Title.Caption = 'Raz'#243'n de Rechazo'
        Width = 333
        Visible = True
      end>
  end
  object btnAceptar: TButton
    Left = 448
    Top = 447
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 0
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 529
    Top = 447
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
    OnClick = btnCancelarClick
  end
  object btnRechazar: TButton
    Left = 261
    Top = 447
    Width = 75
    Height = 25
    Caption = '&Rechazar'
    TabOrder = 2
    OnClick = btnRechazarClick
  end
  object btnGrabar: TButton
    Left = 345
    Top = 447
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Grabar'
    TabOrder = 3
    OnClick = btnGrabarClick
  end
  object DPSButton1: TButton
    Left = 177
    Top = 447
    Width = 75
    Height = 25
    Caption = '&Eliminar'
    TabOrder = 5
    OnClick = DPSButton1Click
  end
  object dsCajas: TDataSource
    DataSet = FControlCalidadPallets.cdsTAGs
    Left = 153
    Top = 264
  end
end
