object formOpcionesControlCalidadTAGs: TformOpcionesControlCalidadTAGs
  Left = 366
  Top = 196
  BorderStyle = bsDialog
  Caption = 'Opciones de Control de Calidad'
  ClientHeight = 252
  ClientWidth = 495
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblMensaje: TLabel
    Left = 9
    Top = 6
    Width = 409
    Height = 46
    AutoSize = False
    Caption = 'Mensaje'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Label1: TLabel
    Left = 9
    Top = 195
    Width = 115
    Height = 13
    Caption = 'Causa del Rechazo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object rgAccion: TRadioGroup
    Left = 6
    Top = 54
    Width = 484
    Height = 130
    Caption = ' Acci'#243'n a Realizar '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ItemIndex = 0
    Items.Strings = (
      'No hacer nada'
      'Rechazar la Caja en su totalidad'
      'Aceptar la caja en su totalidad'
      
        'Aceptar la Caja parcialmente (esto REDEFINE la cantidad de Telev' +
        #237'as en ella)')
    ParentFont = False
    TabOrder = 0
  end
  object txtCausaRechazo: TEdit
    Left = 126
    Top = 192
    Width = 364
    Height = 21
    MaxLength = 50
    TabOrder = 2
  end
  object btnProceder: TButton
    Left = 414
    Top = 222
    Width = 75
    Height = 25
    Caption = '&Proceder'
    TabOrder = 1
    OnClick = btnProcederClick
  end
end
