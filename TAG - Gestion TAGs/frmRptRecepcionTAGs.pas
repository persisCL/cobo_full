{********************************** File Header ********************************
File Name   :
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------

*******************************************************************************}

unit frmRptRecepcionTAGs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppPrnabl, ppClass, ppCtrls, ppBands, ppCache, UtilRB, ppEndUsr,
  ppComm, ppRelatv, ppProd, ppReport, ppDB, ppDBPipe, DB, ADODB, PeaProcs,
  ppParameter, ppModule, raCodMod, ppVar, Util, UtilProc, RStrings;

type
  TFormRptRecepcionTAGs = class(TForm)
    ppReporteRecepcionTAGs: TppReport;
    ppDesigner1: TppDesigner;
	rbiListado: TRBInterface;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppLabel1: TppLabel;
    spObtenerDatosRecepcionTAGs: TADOStoredProc;
    dsObtenerDatosRecepcionTAGs: TDataSource;
    ppDBText1: TppDBText;
    ppDBPipeline1: TppDBPipeline;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppDBText9: TppDBText;
    ppLabel14: TppLabel;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppLabel15: TppLabel;
    ppDBText14: TppDBText;
    ppParameterList1: TppParameterList;
    ppLine1: TppLine;
    spObtenerDatosRecepcionTAGsCodigoEmbalaje: TIntegerField;
    spObtenerDatosRecepcionTAGsContenedor: TStringField;
    spObtenerDatosRecepcionTAGsCodigoRecepcionEnvioTAG: TIntegerField;
    spObtenerDatosRecepcionTAGsCodigoProveedorTAG: TIntegerField;
    spObtenerDatosRecepcionTAGsOrdenCompra: TStringField;
    spObtenerDatosRecepcionTAGsGuiaDespacho: TStringField;
    spObtenerDatosRecepcionTAGsFechaRecepcion: TDateTimeField;
    spObtenerDatosRecepcionTAGsUsuario: TStringField;
    spObtenerDatosRecepcionTAGsAceptado: TStringField;
    spObtenerDatosRecepcionTAGsFechaProceso: TDateTimeField;
    spObtenerDatosRecepcionTAGsIDContenedor: TIntegerField;
    spObtenerDatosRecepcionTAGsNumeroInicialTAG: TLargeIntField;
    spObtenerDatosRecepcionTAGsNumeroFinalTAG: TLargeIntField;
    spObtenerDatosRecepcionTAGsCantidadTAGs: TLargeintField;
    spObtenerDatosRecepcionTAGsCategoriaTAGs: TWordField;
    spObtenerDatosRecepcionTAGsDescripcion: TStringField;
    spObtenerDatosRecepcionTAGsObservaciones: TStringField;
    spObtenerDatosRecepcionTAGsEtiqueta: TStringField;
    spObtenerDatosRecepcionTAGsEtiquetaFinal: TStringField;
    pplblUsuario: TppLabel;
	function Inicializar (GuiaDespacho, OrdenCompra: ANSIString): Boolean;
	function Ejecutar:Boolean;
    procedure ppReporteRecepcionTAGsBeforePrint(Sender: TObject);
    procedure spObtenerDatosRecepcionTAGsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FGuiaDespacho : ANSIString;
    FOrdenCompra  : ANSIString;
  public
    { Public declarations }
  end;

var
  FormRptRecepcionTAGs: TFormRptRecepcionTAGs;

implementation

{$R *.dfm}

function TFormRptRecepcionTAGs.Inicializar (GuiaDespacho, OrdenCompra: ANSIString): Boolean;
begin
    FGuiaDespacho := GuiaDespacho;
    FOrdenCompra  := OrdenCompra;

    result := true;
end;

function TFormRptRecepcionTAGs.Ejecutar:Boolean;
begin
	 rbiListado.Execute(True);
	 result := true;
end;

procedure TFormRptRecepcionTAGs.ppReporteRecepcionTAGsBeforePrint(Sender: TObject);
begin
   	try
		with spObtenerDatosRecepcionTAGs, Parameters do begin
        	Parameters.Refresh;
    		ParamByName ('@GuiaDespacho').Value:= FGuiaDespacho;
    		ParamByName ('@OrdenCompra').Value := FOrdenCompra;
	        Open
    	end;
	except
		on e: Exception do MsgBoxErr(MSG_ERROR_DATOS_LISTADO, e.message, self.caption, MB_ICONSTOP);
	end
end;

procedure TFormRptRecepcionTAGs.spObtenerDatosRecepcionTAGsCalcFields(
  DataSet: TDataSet);
begin
    DataSet.FieldByName('EtiquetaInicial').AsString := iif (DataSet.FieldByName('NumeroInicialTAG').AsFloat=0, '', SerialNumberToEtiqueta(DataSet.FieldByName('NumeroInicialTAG').AsString));
    DataSet.FieldByName('EtiquetaFinal').AsString := iif (DataSet.FieldByName('NumeroFinalTAG').AsFloat=0, '', SerialNumberToEtiqueta(DataSet.FieldByName('NumeroFinalTAG').AsString));
end;

end.
