{
    File Name: EntregarATransportesConPedidos
    Author :
    Date Created:
    Language : ES-AR

    Description :

    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se a�aden los siguientes objetos:
                btnAgregarLinea, btnEditarLinea: TButton;

            - Se a�aden/modifican los siguientes procedimientos/funciones:
                btnAgregarLineaClick,
                btnBorrarLineaClick,
                btnEditarLineaClick,
                frameMovimientoStockcdsTAGsAfterCancel,
                frameMovimientoStockcdsTAGsAfterPost,
                frameMovimientoStockcdsTAGsBeforeEdit,
                frameMovimientoStockcdsTAGsNewRecord,
                ActualizaControlesEntradaDatos,
                Inicializar,
                btnAceptarClick.


Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Etiqueta    : 20160401 MGO
Descripci�n : Se cambia conexi�n de query a BO_Master
}

unit EntregarATransportesConPedidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, BuscaClientes, DmiCtrls,Util, UtilDB, UtilProc, StrUtils,
  DMConnection, PeaProcs, DB, ADODB, ExtCtrls, DPSControls, Buttons,
  DBCtrls, peatypes, VariantComboBox, Grids, DBGrids, DBClient,
  freMovimientoStock;

type
  TfrmEntregarATransportesCP = class(TForm)
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cbTransportistas: TVariantComboBox;
    qryOrigen: TADOQuery;
    qryDestino: TADOQuery;
    cbOrigen: TVariantComboBox;
    cbDestino: TVariantComboBox;
    txtGuiaDespacho: TEdit;
    spGenerarGuiasDespacho: TADOStoredProc;
    frameMovimientoStock: TframeMovimientoStock;
    dbgPedidos: TDBGrid;
    Label6: TLabel;
    dsPedidos: TDataSource;
    spObtenerOrdenesPedidoPendientes: TADOStoredProc;
    cdsPedidos: TClientDataSet;
    spObtenerOrdenesPedidoPendientesCodigoOrdenPedido: TIntegerField;
    spObtenerOrdenesPedidoPendientesCategoria: TWordField;
    spObtenerOrdenesPedidoPendientesCantidad: TIntegerField;
    spObtenerOrdenesPedidoPendientesDescripcion: TStringField;
    spObtenerOrdenesPedidoPendientesCantidadDespachada: TIntegerField;
    spObtenerOrdenesPedidoPendientesCantidadPendiente: TIntegerField;
    spRegistrarDespachoOrdenPedido: TADOStoredProc;
    btnAceptar: TButton;
    btn_cancelar: TButton;
    btnBorrarLinea: TButton;
    btnAgregarLinea: TButton;
    btnEditarLinea: TButton;
	procedure ImprimirAnexo(CodigoGuiaDespacho: LongInt; dsTAgs: TDataSource);
	procedure btnAceptarClick(Sender: TObject);
	procedure btn_cancelarClick(Sender: TObject);
    procedure cbOrigenChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBorrarLineaClick(Sender: TObject);
    procedure cbDestinoChange(Sender: TObject);
    procedure frameMovimientoStockcdsEntregaAfterPost(DataSet: TDataSet);
    procedure dbgPedidosExit(Sender: TObject);
    procedure btnEditarLineaClick(Sender: TObject);
    procedure btnAgregarLineaClick(Sender: TObject);
    procedure frameMovimientoStockcdsTAGsAfterCancel(DataSet: TDataSet);
    procedure frameMovimientoStockcdsTAGsAfterPost(DataSet: TDataSet);
    procedure frameMovimientoStockcdsTAGsBeforeEdit(DataSet: TDataSet);
    procedure frameMovimientoStockcdsTAGsNewRecord(DataSet: TDataSet);
  private
    FCodigoBodegaOrigen		: integer;
	FUsuarioAdministrador	: boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	procedure CargarAlmacenesOrigen(CodigoAlmacen, CodigoOperacionTAG: Integer);
	procedure CargarAlmacenesDestino(CodigoAlmacen, CodigoOperacionTAG: Integer);
    procedure ActualizaControlesEntradaDatos(EntradaDatosHabilitada: Boolean);
  public
    function Inicializar(MDIChild: boolean; aCaption: AnsiString; CodigoAlmacenOrigen: integer = -1; CodigoAlmacenDestino: integer = -1):Boolean;
  end;

const
    OP_TAG_ENTREGA_A_TRANSPORTE = 8;
    OP_TAG_RECEPCION_TRANSPORTE = 9;

implementation

uses frmRptAnexoEntregas, RStrings;

{$R *.dfm}

procedure TfrmEntregarATransportesCP.CargarAlmacenesOrigen(CodigoAlmacen, CodigoOperacionTAG: Integer);
var
    i: Integer;
begin
    i := -1;
    qryOrigen.Parameters.ParamByName('CodigoOperacion').value := CodigoOperacionTAG;
 	qryOrigen.Open;
    While not qryOrigen.Eof do begin
    	cbOrigen.Items.Add(qryOrigen.FieldByName('Descripcion').AsString, qryOrigen.FieldByName('CodigoAlmacen').AsInteger);
		if qryOrigen.FieldByName('CodigoAlmacen').AsInteger = CodigoAlmacen then i := cbOrigen.Items.Count - 1;
		qryOrigen.Next;
	end;
	cbOrigen.ItemIndex := i;
end;

procedure TfrmEntregarATransportesCP.CargarAlmacenesDestino(CodigoAlmacen, CodigoOperacionTAG: Integer);
var
    i: Integer;
begin
    i := -1;
    cbDestino.Clear;
    qryDestino.Close;
    qryDestino.SQL.Text := 'select  MaestroAlmacenes.CodigoAlmacen, MaestroAlmacenes.Descripcion ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'from AlmacenesDestinoOperacion WITH (NOLOCK), MaestroAlmacenes WITH (NOLOCK) ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'where  AlmacenesDestinoOperacion.codigoALmacen = MaestroAlmacenes.CodigoAlmacen AND ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'CodigoOperacion = ' + inttostr(CodigoOperacionTAG);
    if not FUsuarioAdministrador then begin
        qryDestino.SQL.Text := qryDestino.SQL.Text + ' AND MaestroAlmacenes.CodigoBodega = ' + inttostr(FCodigoBodegaOrigen);
    end;

 	qryDestino.Open;
    While not qryDestino.Eof do begin
    	cbDestino.Items.Add(qryDestino.FieldByName('Descripcion').AsString, qryDestino.FieldByName('CodigoAlmacen').AsInteger);
		if qryDestino.FieldByName('CodigoAlmacen').AsInteger = CodigoAlmacen then i := cbDestino.Items.Count - 1;
		qryDestino.Next;
	end;
	cbDestino.ItemIndex := i;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se coloca el cursor en espera mientras el formulario carga.
}
function TfrmEntregarATransportesCP.Inicializar(MDIChild: boolean; aCaption: AnsiString; CodigoAlmacenOrigen: integer = -1; CodigoAlmacenDestino: integer = -1): Boolean;
Var
	S: TSize;
begin
    Try
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        result := True;

    	if MDIChild then begin
      		S := GetFormClientSize(Application.MainForm);
	    	SetBounds(0, 0, S.cx, S.cy);
    	end else begin
	    	FormStyle := fsNormal;
		    Visible := False;
            CenterForm(Self);            
    	end;

    	Caption := AnsiReplaceStr(aCaption, '&', '');
        FCodigoBodegaOrigen  := QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select CodigoBodega From MaestroAlmacenes WITH (NOLOCK) where MaestroAlmacenes.CodigoAlmacen = ' + IntToStr(CodigoAlmacenOrigen) + '), -1)');
        { INICIO : 20160401 MGO
    	FUsuarioAdministrador:= QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select 1 From GruposUsuario WITH (NOLOCK) where GruposUsuario.CodigoUsuario = ' + QuotedStr(UsuarioSistema) + ' and CodigoGrupo = ' + QuotedStr('Administradores') + '), 0)') = 1;
        }
        FUsuarioAdministrador:= QueryGetValueInt(DMConnections.BaseBO_Master, 'select ISNULL((Select 1 From GruposUsuario WITH (NOLOCK) where GruposUsuario.CodigoUsuario = ' + QuotedStr(UsuarioSistema) + ' and CodigoGrupo = ' + QuotedStr('Administradores') + '), 0)') = 1;
        // FIN : 20160401 MGO

        FUsuarioAdministrador:= True;   //dejo la funcionalidad anterior por si en algun momento la requieren nuevamente...

        cdsPedidos.CreateDataSet;
        cdsPedidos.Open;

        try
            CargarTransportistas(DMConnections.BaseCAC, cbTransportistas);
//          if CodigoAlmacenOrigen = -1 then begin
            if FUsuarioAdministrador then begin
                CargarAlmacenesOrigen(1, OP_TAG_ENTREGA_A_TRANSPORTE);
                frameMovimientoStock.inicializar(cbOrigen.Value)
//              frameMovimientoStock.inicializar(1);
            end
            else begin
                frameMovimientoStock.inicializar(CodigoAlmacenOrigen);
                CargarAlmacenesOrigen(CodigoAlmacenOrigen, OP_TAG_ENTREGA_A_TRANSPORTE);
                cbOrigen.Enabled := False;

                if (cbOrigen.ItemIndex < 1) then begin
                    MsgBox('El almac�n origen no existe o no puede realizar esta operaci�n',self.Caption,MB_ICONSTOP);
                    result := false;
                    Close;
                    Exit;
                end;
            end;

            CargarAlmacenesDestino(CodigoAlmacenDestino, OP_TAG_RECEPCION_TRANSPORTE);
	    	cbDestinoChange(nil);
        except
	        result := False;
    	end;

        txtGuiaDespacho.Clear;
    Finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    End;
end;

Procedure TfrmEntregarATransportesCP.ImprimirAnexo(CodigoGuiaDespacho: LongInt; dsTAgs: TDataSource);
var
    f: TFormRptAnexoEntregas;
begin
	Application.CreateForm(TFormRptAnexoEntregas, f);
   	if not f.Inicializar (CodigoGuiaDespacho, dsTAGs) then exit;
    if f.Ejecutar then f.Free;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se habilitan los combos de los almacenes al finalizar el proceso.
}
procedure TfrmEntregarATransportesCP.btnAceptarClick(Sender: TObject);
var
	CodigoGuiaDespacho: LongInt;
    CodigoAlmacenTransportista: integer;
    TodoOk: Boolean;
resourceString
    CAPTION_VALIDAR_ENTREGA	= 'Entregar a Transportes';
    ERROR_VALIDAR_TRANSPORTE= 'Debe seleccionarse un Transporte';
    ERROR_VALIDAR_ORIGEN 	= 'Debe seleccionarse un Almac�n Origen';
    ERROR_VALIDAR_DESTINO	= 'Debe seleccionarse un Almac�n Destino';
    ERROR_VALIDAR_ALMACENES	= 'Los almacenes Origen/Destino no pueden ser los mismos';
    ERROR_VALIDAR_GUIA   	= 'Debe indicarse una Gu�a de Despacho';
    MSG_ERROR_REGISTRO_ATENCION_ORDENES_PEDIDO = 'No se pudo registrar el detalle de despachos de cantidades de Ordenes de Pedido';
begin
    if not ValidateControls([cbTransportistas, cbOrigen, cbDestino, cbDestino, txtGuiaDespacho],
    	[(cbTransportistas.Value <> 0), (cbOrigen.ItemIndex <> -1), (cbDestino.ItemIndex <> -1), (cbOrigen.Value <> cbDestino.Value), (Trim(txtGuiaDespacho.Text) <> '')],
        CAPTION_VALIDAR_ENTREGA,
        [ERROR_VALIDAR_TRANSPORTE, ERROR_VALIDAR_ORIGEN, ERROR_VALIDAR_DESTINO, ERROR_VALIDAR_ALMACENES, ERROR_VALIDAR_GUIA])
    then begin
    	Exit
    end;

    CodigoAlmacenTransportista := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT AlmacenAsociado FROM MaestroTransportistas WITH (NOLOCK) WHERE CodigoTransportista = ''' + inttostr(cbTransportistas.Items[cbTransportistas.ItemIndex].Value) + '''');

    if frameMovimientoStock.ValidarMovimiento(CodigoAlmacenTransportista) then begin
        try
            // Agrego Guia Despacho
            with spGenerarGuiasDespacho, Parameters do begin
                ParamByName ('@NumeroGuiaDespacho').Value 	:= Trim(txtGuiaDespacho.Text);
                ParamByName ('@CodigoAlmacenOrigen').Value 	:= cbOrigen.Value;
                ParamByName ('@CodigoAlmacenDestino').Value := cbDestino.Value;
                ParamByName ('@CodigoTransportista').Value 	:= cbTransportistas.Value;

                ExecProc;
                CodigoGuiaDespacho := ParamByName ('@CodigoGuiaDespacho').Value;

                if frameMovimientoStock.GenerarMovimiento(ParamByName ('@CodigoAlmacenTransporte').Value, CodigoGuiaDespacho, OP_TAG_ENTREGA_A_TRANSPORTE) then begin
                    Close;

		            if not cdsPedidos.IsEmpty then begin
						Screen.Cursor := crHourGlass;
					    TodoOk:= false;
					    DMConnections.BaseCAC.BeginTrans;

            	        try
	        	            with cdsPedidos do begin
	                            DisableControls;

    	        	        	First;
            	    	        while not EoF do begin
				                    try
                                    	if cdsPedidos.FieldByName('ADespachar').AsInteger <> 0 then begin
	                				        with spRegistrarDespachoOrdenPedido, Parameters do begin
					                            Close;

            	    				            ParamByName('@CodigoGuiaDespacho').Value:= CodigoGuiaDespacho;
                					            ParamByName('@CodigoOrdenPedido').Value := cdsPedidos.FieldByName('CodigoOrdenPedido').AsInteger;
                					            ParamByName('@Categoria').Value 		:= cdsPedidos.FieldByName('Categoria').AsInteger;
                					            ParamByName('@CantidadDespachada').Value:= cdsPedidos.FieldByName('ADespachar').AsInteger;

				            	                ExecProc;
                					        end
                                        end;

				                        Next;
                				    except
				                        on e: Exception do begin
                				            Screen.Cursor := crDefault;

									        DMConnections.BaseCAC.RollbackTrans;
				                            MsgBoxErr(MSG_ERROR_REGISTRO_ATENCION_ORDENES_PEDIDO, e.message, MSG_CAPTION_MOVIMIENTOS_STOCK, MB_ICONSTOP);

                				            Exit;
				                        end;
                				    end;
		                        end
    		                end;

                            TodoOk := true
        	            finally
					        Screen.Cursor := crDefault;

					        cdsPedidos.EnableControls;
				    	    if TodoOk then begin
			    	    	    DMConnections.BaseCAC.CommitTrans;
					        end
                        end
                    end
                end else begin
                    QueryExecute(DMConnections.BaseCAC, 'DELETE FROM GUIASDESPACHO WHERE CodigoGuiaDespacho = ' + inttostr(CodigoGuiaDespacho));
                    Exit;
                end;

            end;
        except
			on e: Exception do begin
            	MsgBoxErr('Error al generar el movimiento', e.message, self.caption, MB_ICONSTOP);
                Exit;
            end
        end;

        MsgBox('Datos grabados con �xito. Se imprimir� un anexo a la Gu�a de Despacho', self.caption, MB_ICONINFORMATION);
        ImprimirAnexo (CodigoGuiaDespacho, frameMovimientoStock.dsTAGs);

        frameMovimientoStock.Limpiar;
        cdsPedidos.EmptyDataSet;

        cbDestino.ItemIndex := -1;
        txtGuiaDespacho.Clear;
        frameMovimientoStock.CodigoAlmacenOrigen := cbOrigen.Value;
        cbOrigen.Enabled := True;
        cbDestino.Enabled := True;
        cbOrigen.SetFocus;
      end;
end;

procedure TfrmEntregarATransportesCP.btn_cancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmEntregarATransportesCP.cbOrigenChange(Sender: TObject);
begin
    frameMovimientoStock.CodigoAlmacenOrigen := cbOrigen.Value;
end;

procedure TfrmEntregarATransportesCP.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

{
    Procedure Name: btnAgregarLineaClick
    Parameters : Sender: TObject
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - A�ade un Registro en el objeto cdsTAGs.
}
procedure TfrmEntregarATransportesCP.btnAgregarLineaClick(Sender: TObject);
begin
    if (frameMovimientoStock.cdsTAGs.Active) then frameMovimientoStock.cdsTAGs.Append;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se deshabilita el bot�n de borrado, en el caso de que se haya
            elimando el �ltimo registro.
            - S�lo se habiltan los combos de almacenes si no existe ning�n
            registro en el objeto cdsTAGs.            
}
procedure TfrmEntregarATransportesCP.btnBorrarLineaClick(Sender: TObject);
begin
    try
        if (frameMovimientoStock.cdsTAGs.Active) and (frameMovimientoStock.cdsTAGs.RecordCount > 0) then
    	    frameMovimientoStock.cdsTAGs.Delete;
    finally
        btnBorrarLinea.Enabled := frameMovimientoStock.cdsTAGs.RecordCount > 0;
        cbOrigen.Enabled  := frameMovimientoStock.cdsTAGs.RecordCount = 0;
        cbDestino.Enabled := frameMovimientoStock.cdsTAGs.RecordCount = 0;
        frameMovimientoStock.dbgTags.SetFocus;
    end;
end;

{
    Procedure Name: btnEditarLineaClick
    Parameters : Sender: TObject
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Pone en edici�n el Registro activo del objeto cdsTAGs.
}
procedure TfrmEntregarATransportesCP.btnEditarLineaClick(Sender: TObject);
begin
    if (frameMovimientoStock.cdsTAGs.Active) then frameMovimientoStock.cdsTAGs.Edit;
end;

procedure TfrmEntregarATransportesCP.cbDestinoChange(Sender: TObject);
begin
	with spObtenerOrdenesPedidoPendientes, Parameters do begin
    	Close;
		ParamByName ('@CodigoAlmacen').Value := cbDestino.Value;
        Open;

        cdsPedidos.EmptyDataSet;
        while not EoF do begin
        	cdsPedidos.AppendRecord([FieldByName ('CodigoOrdenPedido').AsInteger,
								    FieldByName ('Categoria').AsInteger,
								    FieldByName ('Cantidad').AsInteger,
								    FieldByName ('Descripcion').AsString,
								    FieldByName ('CantidadDespachada').AsInteger,
								    FieldByName ('CantidadPendiente').AsInteger]);
        	Next
        end;

        Close
    end;

	frameMovimientoStockcdsEntregaAfterPost(frameMovimientoStock.cdsEntrega);
end;

procedure TfrmEntregarATransportesCP.frameMovimientoStockcdsEntregaAfterPost(
  DataSet: TDataSet);

	procedure Distribuir (Categoria, CantidadADistribuir: integer);
    begin
    	with cdsPedidos do begin
        	DisableControls;

        	First;
            while not EoF do begin
            	if FieldByName('Categoria').AsInteger = Categoria then begin
                	Edit;
                    
                	if CantidadADistribuir = 0 then begin
                    	// pone en 0 los pedidos que no pueden ser atendidos
                        FieldByName('ADespachar').AsInteger := 0
                    end else begin
	                	if CantidadADistribuir >= FieldByName('CantidadPendiente').AsInteger then begin
                        	// puede atender el pedido completo
                        	FieldByName('ADespachar').AsInteger := FieldByName('CantidadPendiente').AsInteger
        	            end else begin
                        	// puede atender el pedido parcialmente
                        	FieldByName('ADespachar').AsInteger := CantidadADistribuir
                	    end;

                        // nueva cantidad disponible
                    	CantidadADistribuir := CantidadADistribuir - FieldByName('ADespachar').AsInteger
                    end;

                    Post
                end;

            	Next
            end;

            EnableControls
        end
    end;

begin
    // distribuye la cantidad ingresada entre los pedidos, si es que los hay
    if not cdsPedidos.IsEmpty then begin
        with DataSet do begin
            DisableControls;
            First;
            while not EoF do begin
                Distribuir (FieldByName('Categoria').AsInteger, FieldByName('CantidadTAGs').AsInteger);
                Next
            end;
            EnableControls
        end
    end;
end;

{
    Procedure Name: frameMovimientoStockcdsTAGsAfterCancel
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Ejecuta el evento correspondiente del frame y actualiza los controles del Formulario.
}
procedure TfrmEntregarATransportesCP.frameMovimientoStockcdsTAGsAfterCancel(DataSet: TDataSet);
begin
    frameMovimientoStock.cdsTAGsAfterPost(DataSet);
    ActualizaControlesEntradaDatos(False);
end;

{
    Procedure Name: frameMovimientoStockcdsTAGsAfterPost
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Ejecuta el evento correspondiente del frame y actualiza los controles del Formulario.
}
procedure TfrmEntregarATransportesCP.frameMovimientoStockcdsTAGsAfterPost(
  DataSet: TDataSet);
begin
    frameMovimientoStock.cdsTAGsAfterPost(DataSet);
    ActualizaControlesEntradaDatos(False);
end;

{
    Procedure Name: frameMovimientoStockcdsTAGsBeforeEdit
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Ejecuta el evento correspondiente del frame y actualiza los controles del Formulario.
}
procedure TfrmEntregarATransportesCP.frameMovimientoStockcdsTAGsBeforeEdit(
  DataSet: TDataSet);
begin
    frameMovimientoStock.cdsTAGsBeforeEdit(DataSet);
    ActualizaControlesEntradaDatos(True);
end;

{
    Procedure Name: frameMovimientoStockcdsTAGsNewRecord
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Ejecuta el evento correspondiente del frame y actualiza los controles del Formulario.
}
procedure TfrmEntregarATransportesCP.frameMovimientoStockcdsTAGsNewRecord(
  DataSet: TDataSet);
begin
    frameMovimientoStock.cdsTAGsNewRecord(DataSet);
    ActualizaControlesEntradaDatos(True);
end;

procedure TfrmEntregarATransportesCP.dbgPedidosExit(Sender: TObject);
begin
	frameMovimientoStockcdsEntregaAfterPost(frameMovimientoStock.cdsEntrega);
end;

{
    Procedure Name: ActualizaControlesEntradaDatos
    Parameters    : EntradaDatosHabilitada: Boolean

    Author       : pdominguez
    Date Created : 29/06/2010
    Description  : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Habilita o Deshabilita los controles de la pantalla en funci�n de si
        est� o no el panel de entrada de Datos habilitado.
}
procedure TfrmEntregarATransportesCP.ActualizaControlesEntradaDatos(EntradaDatosHabilitada: Boolean);
begin
    btnBorrarLinea.Enabled   := (Not EntradaDatosHabilitada) and (frameMovimientoStock.cdsTAGs.RecordCount > 0);
    btnAgregarLinea.Enabled  := Not EntradaDatosHabilitada;
    btnEditarLinea.Enabled   := Not EntradaDatosHabilitada;
    btnaceptar.Enabled       := Not EntradaDatosHabilitada;
    btn_cancelar.Enabled     := Not EntradaDatosHabilitada;
    cbTransportistas.Enabled := Not EntradaDatosHabilitada;
    cbOrigen.Enabled         := Not EntradaDatosHabilitada;
    cbDestino.Enabled        := Not EntradaDatosHabilitada;
    txtGuiaDespacho.Enabled  := (Not EntradaDatosHabilitada);
    dbgPedidos.Enabled       := (Not EntradaDatosHabilitada);
    cbOrigen.Enabled         := (Not EntradaDatosHabilitada) and (frameMovimientoStock.cdsTAGs.RecordCount = 0);
    cbDestino.Enabled        := (Not EntradaDatosHabilitada) and (frameMovimientoStock.cdsTAGs.RecordCount = 0);
end;

end.
