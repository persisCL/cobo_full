object FormListaPuntosEntrega: TFormListaPuntosEntrega
  Left = 439
  Top = 374
  BorderStyle = bsToolWindow
  Caption = 'Selecci'#243'n de Punto de Entrega'
  ClientHeight = 126
  ClientWidth = 342
  Color = clBtnFace
  Constraints.MaxHeight = 160
  Constraints.MaxWidth = 350
  Constraints.MinHeight = 160
  Constraints.MinWidth = 350
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 12
    Width = 100
    Height = 13
    Caption = 'Punto de Entrega'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnCancelar: TDPSButton
    Left = 252
    Top = 93
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 0
    OnClick = btnCancelarClick
  end
  object btnAceptar: TDPSButton
    Left = 168
    Top = 93
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object cbPuntosEntrega: TVariantComboBox
    Left = 9
    Top = 27
    Width = 322
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 2
    Items = <>
  end
  object ckbDetalle: TCheckBox
    Left = 9
    Top = 60
    Width = 259
    Height = 17
    Caption = 'Detallar Telev'#237'a en el Punto de Entrega'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
end
