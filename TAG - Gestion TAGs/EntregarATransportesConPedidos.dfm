object frmEntregarATransportesCP: TfrmEntregarATransportesCP
  Left = 202
  Top = 98
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Entregar a Transportes'
  ClientHeight = 659
  ClientWidth = 599
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  DesignSize = (
    599
    659)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 9
    Width = 66
    Height = 13
    Caption = 'Transporte:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 9
    Top = 294
    Width = 94
    Height = 13
    Caption = 'Lista de Entrega'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 9
    Top = 141
    Width = 95
    Height = 13
    Caption = 'Lista de Pedidos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 9
    Top = 36
    Width = 580
    Height = 103
    Caption = ' Datos de Entrega '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 100
      Height = 13
      Caption = 'Almac'#233'n Destino:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 12
      Top = 78
      Width = 94
      Height = 13
      Caption = 'Gu'#237'a Despacho:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 12
      Top = 21
      Width = 94
      Height = 13
      Caption = 'Almac'#233'n Origen:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbOrigen: TVariantComboBox
      Left = 156
      Top = 18
      Width = 412
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbOrigenChange
      Items = <>
    end
    object cbDestino: TVariantComboBox
      Left = 156
      Top = 45
      Width = 412
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbDestinoChange
      Items = <>
    end
    object txtGuiaDespacho: TEdit
      Left = 156
      Top = 75
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      TabOrder = 2
    end
  end
  object cbTransportistas: TVariantComboBox
    Left = 82
    Top = 6
    Width = 315
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  inline frameMovimientoStock: TframeMovimientoStock
    Left = 9
    Top = 309
    Width = 582
    Height = 312
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 9
    ExplicitTop = 309
    ExplicitWidth = 582
    ExplicitHeight = 312
    inherited vleIngresados: TValueListEditor
      Left = 163
      Top = 166
      ExplicitLeft = 163
      ExplicitTop = 166
    end
    inherited vleRecibidos: TValueListEditor
      Left = 209
      Top = 175
      ExplicitLeft = 209
      ExplicitTop = 175
    end
    inherited pnlEntradaDatos: TPanel
      Top = 216
      Width = 582
      ExplicitTop = 216
      ExplicitWidth = 582
      inherited gbAgregaTelevias: TGroupBox
        Width = 578
        ExplicitWidth = 578
        inherited Label1: TLabel
          Left = 149
          Width = 67
          ExplicitLeft = 149
          ExplicitWidth = 67
        end
        inherited Label2: TLabel
          Left = 274
          Width = 62
          ExplicitLeft = 274
          ExplicitWidth = 62
        end
        inherited Label3: TLabel
          Left = 412
          Width = 42
          ExplicitLeft = 412
          ExplicitWidth = 42
        end
        inherited Label4: TLabel
          Left = 504
          ExplicitLeft = 504
        end
        inherited dbeTeleviaInicial: TDBEdit
          Left = 127
          ExplicitLeft = 127
        end
        inherited dbeTeleviaFinal: TDBEdit
          Left = 245
          ExplicitLeft = 245
        end
        inherited dbeCantidad: TDBEdit
          Left = 363
          ExplicitLeft = 363
        end
        inherited dbeCategoria: TDBEdit
          Left = 461
          ExplicitLeft = 461
        end
        inherited btnGrabar: TButton
          Left = 405
          ExplicitLeft = 405
        end
        inherited btnCancelar: TButton
          Left = 483
          ExplicitLeft = 483
        end
      end
    end
    inherited Panel1: TPanel
      Width = 582
      Height = 216
      ExplicitWidth = 582
      ExplicitHeight = 216
      inherited lblDetalle: TLabel
        Top = 138
        ExplicitTop = 138
      end
      inherited dbgRecibirTags: TDBGrid
        Width = 582
        Height = 94
        TitleFont.Name = 'MS Sans Serif'
        Columns = <
          item
            Expanded = False
            FieldName = 'Descripcion'
            ReadOnly = True
            Title.Caption = 'Categor'#237'a'
            Width = 290
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CantidadTAGsDisponibles'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'Telev'#237'as Disponibles'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CantidadTags'
            Title.Caption = 'Cantidad de Telev'#237'as'
            Width = 130
            Visible = True
          end>
      end
      inherited dbgTags: TDBGrid
        Top = 94
        Width = 582
        Height = 122
        Align = alClient
        TitleFont.Name = 'MS Sans Serif'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NumeroInicialTAG'
            Title.Alignment = taCenter
            Title.Caption = 'Telev'#237'a Inicial'
            Width = 150
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NumeroFinalTAG'
            Title.Alignment = taCenter
            Title.Caption = 'Telev'#237'a Final'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CantidadTAGs'
            Title.Alignment = taRightJustify
            Title.Caption = 'Cantidad'
            Width = 125
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CategoriaTAGs'
            Title.Alignment = taRightJustify
            Title.Caption = 'Categor'#237'a'
            Width = 125
            Visible = True
          end>
      end
    end
    inherited spObtenerDatosPuntoEntrega: TADOStoredProc
      Left = 201
      Top = 42
    end
    inherited spVerificarTAGEnArchivos: TADOStoredProc
      Left = 231
      Top = 42
    end
    inherited spObtenerDatosRangoTAGsAlmacen: TADOStoredProc
      Left = 261
      Top = 42
    end
    inherited dsTAGs: TDataSource
      Left = 129
      Top = 147
    end
    inherited cdsTAGs: TClientDataSet
      BeforeEdit = frameMovimientoStockcdsTAGsBeforeEdit
      AfterPost = frameMovimientoStockcdsTAGsAfterPost
      AfterCancel = frameMovimientoStockcdsTAGsAfterCancel
      OnNewRecord = frameMovimientoStockcdsTAGsNewRecord
      Left = 161
      Top = 147
    end
    inherited spRegistrarMovimientoStock: TADOStoredProc
      Left = 192
      Top = 147
    end
    inherited ObtenerSaldosTAGsAlmacen: TADOStoredProc
      Left = 171
      Top = 42
    end
    inherited cdsEntrega: TClientDataSet
      AfterPost = frameMovimientoStockcdsEntregaAfterPost
      Left = 139
      Top = 42
    end
    inherited dsEntrega: TDataSource
      Left = 110
      Top = 42
    end
    inherited qryCategorias: TADOQuery
      Left = 291
      Top = 42
    end
  end
  object dbgPedidos: TDBGrid
    Left = 9
    Top = 156
    Width = 580
    Height = 136
    DataSource = dsPedidos
    Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnExit = dbgPedidosExit
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CodigoOrdenPedido'
        Title.Alignment = taRightJustify
        Title.Caption = 'Orden Pedido'
        Width = 74
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descripcion'
        Title.Caption = 'Categor'#237'a'
        Width = 196
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'Cantidad'
        Title.Alignment = taRightJustify
        Title.Caption = 'Pedido'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CantidadDespachada'
        Title.Alignment = taRightJustify
        Title.Caption = 'Despachado'
        Width = 70
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CantidadPendiente'
        Title.Alignment = taRightJustify
        Title.Caption = 'Pendiente'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ADespachar'
        Title.Alignment = taRightJustify
        Title.Caption = 'A Despachar'
        Width = 70
        Visible = True
      end>
  end
  object btnAceptar: TButton
    Left = 435
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object btn_cancelar: TButton
    Left = 513
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
    OnClick = btn_cancelarClick
  end
  object btnBorrarLinea: TButton
    Left = 169
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Borrar l'#237'nea'
    Enabled = False
    TabOrder = 5
    OnClick = btnBorrarLineaClick
  end
  object btnAgregarLinea: TButton
    Left = 12
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&A'#241'adir L'#237'nea'
    TabOrder = 7
    OnClick = btnAgregarLineaClick
  end
  object btnEditarLinea: TButton
    Left = 90
    Top = 628
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Editar L'#237'nea'
    TabOrder = 8
    OnClick = btnEditarLineaClick
  end
  object qryOrigen: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOperacion'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = 1
      end>
    SQL.Strings = (
      'select '
      '  MaestroAlmacenes.CodigoAlmacen, MaestroAlmacenes.Descripcion '
      'from '
      
        '  AlmacenesOrigenOperacion WITH (NOLOCK), MaestroAlmacenes WITH ' +
        '(NOLOCK) '
      'where '
      '  MaestroAlmacenes.SoloNominado = 0'
      
        '  AND AlmacenesOrigenOperacion.codigoAlmacen = MaestroAlmacenes.' +
        'CodigoAlmacen'
      
        '  AND AlmacenesOrigenOperacion.CodigoOperacion = :CodigoOperacio' +
        'n')
    Left = 273
    Top = 48
  end
  object qryDestino: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    Left = 272
    Top = 77
  end
  object spGenerarGuiasDespacho: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarGuiasDespacho;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenTransporte'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 411
    Top = 9
  end
  object dsPedidos: TDataSource
    DataSet = cdsPedidos
    Left = 147
    Top = 210
  end
  object spObtenerOrdenesPedidoPendientes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOrdenesPedidoPendientes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 207
    Top = 210
    object spObtenerOrdenesPedidoPendientesCodigoOrdenPedido: TIntegerField
      FieldName = 'CodigoOrdenPedido'
    end
    object spObtenerOrdenesPedidoPendientesCategoria: TWordField
      FieldName = 'Categoria'
    end
    object spObtenerOrdenesPedidoPendientesCantidad: TIntegerField
      FieldName = 'Cantidad'
    end
    object spObtenerOrdenesPedidoPendientesDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 60
    end
    object spObtenerOrdenesPedidoPendientesCantidadDespachada: TIntegerField
      FieldName = 'CantidadDespachada'
      ReadOnly = True
    end
    object spObtenerOrdenesPedidoPendientesCantidadPendiente: TIntegerField
      FieldName = 'CantidadPendiente'
      ReadOnly = True
    end
  end
  object cdsPedidos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoOrdenPedido'
        DataType = ftInteger
      end
      item
        Name = 'Categoria'
        DataType = ftInteger
      end
      item
        Name = 'Cantidad'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CantidadDespachada'
        DataType = ftInteger
      end
      item
        Name = 'CantidadPendiente'
        DataType = ftInteger
      end
      item
        Name = 'ADespachar'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 177
    Top = 210
  end
  object spRegistrarDespachoOrdenPedido: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarDespachoOrdenPedido'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CantidadDespachada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 240
    Top = 210
  end
end
