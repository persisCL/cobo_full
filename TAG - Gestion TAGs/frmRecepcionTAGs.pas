    {********************************** File Header ********************************
File Name   : frmRecepcionTAGs
Author      : Castro, Ra�l A.
Date Created: 10/05/04
Language    : ES-AR
Description : Recepci�n de TAGs
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Revision 1
Author: FSandi
Date: 23-08-2007
Description: Validamos las longitudes de todos los campos del DbGrid

Revision : 2
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

*******************************************************************************}

unit frmRecepcionTAGs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Grids, DBGrids, DBCtrls, UtilDB,
  Validate, DateEdit, DmiCtrls, UtilProc, ADODB, DB, DBClient, Provider, Util,
  StrUtils, RStrings, Mask, Comm, Menus;

type
//Revision 1
  TNewStringGrid = class(TDbGrid)
  function GetCellInEditMode: Boolean;
  function GetEditor : TInplaceEdit;
  public
      property CellInEditMode: Boolean read GetCellInEditMode;
      property Editor : TInplaceEdit read GetEditor;
  end;

  TNewInplaceEditor = class (Tinplaceedit)
  function GetMaxLength: Integer;
  public
     procedure SetMaxLength(Value: Integer);
     property MaxLength: Integer read GetMaxLength write SetMaxLength;
  end;
//Fin de Revision 1

  TFRecepcionTAGs = class(TForm)
    Panel1: TPanel;
    Label5: TLabel;
    dedFechaRecepcion: TDateEdit;
    cdsEmbalajes: TClientDataSet;
    dsProveedoresTAGs: TDataSource;
    qryProveedoresTAGs: TADOQuery;
    dsEmbalajes: TDataSource;
    spObtenerDatosRecepcionTAGs: TADOStoredProc;
    gbDatos: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    lupProveedores: TDBLookupComboBox;
    Label2: TLabel;
    txtOrdenCompra: TEdit;
    txtGuiaDespacho: TEdit;
    Label4: TLabel;
    nedCantItems: TNumericEdit;
    Label6: TLabel;
    nedCantidadTAGs: TNumericEdit;
    gbControl: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    lblControlItems: TLabel;
    lblControlTAGs: TLabel;
    spObtenerDatosRangoTAGs: TADOStoredProc;
    spActualizarRecepcionEnviosTAGs: TADOStoredProc;
    Panel3: TPanel;
    dbgItems: TDBGrid;
    Panel4: TPanel;
    spCantidadTAGsPendientesControlCalidad: TADOStoredProc;
    spActualizarMovimientosImportacionRecepcionTAGs: TADOStoredProc;
    Panel2: TPanel;
    btnVerTelevias: TButton;
    btnImprimir: TButton;
    btnLimpiar: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnBorrarLinea: TButton;
    PopupEmpty: TPopupMenu;
    procedure txtGuiaDespachoChange(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cdsEmbalajesAfterPost(DataSet: TDataSet);
    procedure nedCantItemsChange(Sender: TObject);
    procedure cdsEmbalajesAfterDelete(DataSet: TDataSet);
    procedure nedCantidadTAGsChange(Sender: TObject);
	function ConsistenciaDatos (DataSet: TDataSet): Boolean;
    procedure btnAceptarClick(Sender: TObject);
    procedure cdsEmbalajesBeforePost(DataSet: TDataSet);
    procedure cdsEmbalajesNewRecord(DataSet: TDataSet);
    procedure btnImprimirClick(Sender: TObject);
    procedure cdsEmbalajesBeforeDelete(DataSet: TDataSet);
    procedure btnLimpiarClick(Sender: TObject);
    procedure btnVerTeleviasClick(Sender: TObject);
    procedure cdsEmbalajesAfterScroll(DataSet: TDataSet);
    procedure btnBorrarLineaClick(Sender: TObject);
    procedure dbgItemsKeyPress(Sender: TObject; var Key: Char);
    procedure dbgItemsExit(Sender: TObject);
  private
    { Private declarations }
    FNuevaRecepcion,
    FCargando: Boolean;
	TAGInicial,
    TAGFinal: DWORD;
    CantidadTAGsRango: LongInt;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216

    function CantidadTAGs(DataSet: TDataSet): Integer;
    Function ConsistenciaDatosLinea(dataset: TDataset; var descriError: AnsiString): boolean;
	function ExisteEmbalaje(DS: TDataSet): Boolean;
	function ValidarIDContenedor (DataSet: TDataSet): Boolean;
	function ValidarRangosSuperpuestos(CdsRangos: TClientDataSet; ConMsg: Boolean): Boolean;
  public
    { Public declarations }
	FListaEmbalajesCDS: TStringList;
	function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  FRecepcionTAGs: TFRecepcionTAGs;

implementation

uses DMConnection, PeaProcs, frmTeleviasImportados, frmRptRecepcionTAGs,
  PeaTypes;

{$R *.dfm}

//Revision 1
function TNewStringGrid.GetCellInEditMode: Boolean;
begin
  if not Assigned(InplaceEditor) then
    Result := False
  else
    if InplaceEditor.Visible then
      Result := True
    else
      Result := False;
end;

function TNewInplaceEditor.GetMaxLength: Integer;
begin
    Result := inherited maxlength;
end;

procedure TNewInplaceEditor.SetMaxLength(Value: Integer);
begin 
  if not IsMasked then
    inherited MaxLength := Value
  else
    inherited MaxLength := 10;
end;
//Fin de Revision 1

function TFRecepcionTAGs.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
Var
	S: TSize;
begin
	Result := false;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
   	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	if not OpenTables([qryProveedoresTAGs]) then exit;

    cdsEmbalajes.CreateDataSet;
    cdsEmbalajes.Open;

    txtGuiaDespacho.Clear;
    txtOrdenCompra.Clear;
    nedCantItems.Clear;
    dedFechaRecepcion.Clear;
    lupProveedores.KeyValue := -1;

	FListaEmbalajesCDS := TStringList.Create;

    KeyPreview := True;
	Result := true;
end;

function TFRecepcionTAGs.CantidadTAGs(DataSet: TDataSet): Integer;
begin
	result := 0;

	DataSet.First;
    while not DataSet.EoF do Begin
        result := result + DataSet.FieldByName('CantidadTAGs').AsInteger;

       	DataSet.Next
	end;
end;

procedure TFRecepcionTAGs.txtGuiaDespachoChange(Sender: TObject);
var
	CantidadTAGsPendientes:LongInt;
begin
	with spObtenerDatosRecepcionTAGs, Parameters do begin
    	Close;
        Parameters.Refresh;
    	ParamByName('@GuiaDespacho').Value := Trim(txtGuiaDespacho.Text);
    	ParamByName('@OrdenCompra').Value  := Trim(txtOrdenCompra.Text);
        Open;

        lupProveedores.KeyValue := FieldByName('CodigoProveedorTAG').AsInteger;
        dedFechaRecepcion.Date := FieldByName('FechaRecepcion').AsDateTime;
		nedCantItems.ValueInt := RecordCount;
        nedCantidadTAGs.Clear;

        FNuevaRecepcion := recordCount = 0;

        cdsEmbalajes.EmptyDataSet;
        if recordCount <> 0 then begin
            FCargando := True;
       		FListaEmbalajesCDS.Clear;

            while not EoF do Begin
            	with spCantidadTAGsPendientesControlCalidad, Parameters do begin
                	Close;
                    //INICIO:	20160816 CFU
                    Parameters.Refresh;
                    //TERMINO:	20160816 CFU
                	ParamByName('@TAGInicial').Value:= spObtenerDatosRecepcionTAGs.FieldByName('NumeroInicialTAG').AsInteger;
                	ParamByName('@TAGFinal').Value 	:= spObtenerDatosRecepcionTAGs.FieldByName('NumeroFinalTAG').AsInteger;
					ParamByName ('@CantidadTAGs').Value := 0;
                    ExecProc;
                    CantidadTAGsPendientes := ParamByName ('@CantidadTAGs').Value;
                    Close;
                end;

                cdsEmbalajes.AppendRecord([FieldByName('Contenedor').AsString,
                                            FieldByName('IDContenedor').AsInteger,
                                            SerialNumberToEtiqueta(FieldByName('NumeroInicialTAG').AsString),
                                            SerialNumberToEtiqueta(FieldByName('NumeroFinalTAG').AsString),
                                            FieldByName('CantidadTAGS').AsInteger,
                                            FieldByName('CategoriaTAGs').AsInteger,
                                            FieldByName('Aceptado').AsString,
                                            '',
                                            FieldByName ('CodigoEmbalaje').AsInteger,
                                            FieldByName ('CodigoRecepcionEnvioTAG').AsInteger,
                                            (FieldByName ('FechaProceso').AsDateTime = NullDate) Or
	                                            (FieldByName('CantidadTAGS').AsInteger = CantidadTAGsPendientes)]);

                nedCantidadTAGs.ValueInt := nedCantidadTAGs.ValueInt + FieldByName('CantidadTAGs').AsInteger;
			    FListaEmbalajesCDS.Add(Trim(FieldByName('Contenedor').AsString) + ' ' + FieldByName ('IDContenedor').AsString);

                Next
            end;
            FCargando := False;
        end else begin
            FCargando := False;
            nedCantItems.ValueInt := 0;
            nedCantItems.OnChange(nedCantItems);
            nedCantidadTAGs.ValueInt := 0;
            nedCantidadTAGs.OnChange(nedCantidadTAGs);
            lupProveedores.KeyValue := -1;
            dedFechaRecepcion.date := date;
        end;
        Close;
    end;
end;

procedure TFRecepcionTAGs.btnCancelarClick(Sender: TObject);
begin
	cdsEmbalajes.Close;
	qryProveedoresTAGs.Close;
    Close
end;

procedure TFRecepcionTAGs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TFRecepcionTAGs.cdsEmbalajesAfterPost(DataSet: TDataSet);
begin
	FListaEmbalajesCDS.Add(Trim(DataSet.FieldByName('Contenedor').AsString) + ' ' + DataSet.FieldByName('IDContenedor').AsString);

	nedCantItemsChange(nil);
    nedCantidadTAGsChange(nil);
end;

procedure TFRecepcionTAGs.nedCantItemsChange(Sender: TObject);
begin
	lblControlItems.Caption:= IntToStr(cdsEmbalajes.RecordCount) + ' de ' + IntToStr(nedCantItems.valueInt);
    lblControlItems.Font.Color := clBlack;
    if cdsEmbalajes.RecordCount > nedCantItems.ValueInt then lblControlItems.Font.Color := clRed
end;

procedure TFRecepcionTAGs.nedCantidadTAGsChange(Sender: TObject);
var
	CantTAGs : Integer;
begin
	CantTAGs := CantidadTags (dbgItems.DataSource.DataSet);

	lblControlTAGs.Caption:= IntToStr(CantTAGs) + ' de ' + IntToStr(nedCantidadTAGs.valueInt);
    lblControlTAGs.Font.Color := clBlack;
    if CantTAGs > nedCantidadTAGs.ValueInt then lblControlTAGs.Font.Color := clRed
end;

procedure TFRecepcionTAGs.cdsEmbalajesAfterDelete(DataSet: TDataSet);
begin
	nedCantItemsChange(nil);
    nedCantidadTAGsChange(nil);
end;

Function TFRecepcionTAGs.ConsistenciaDatosLinea(dataset: TDataset; var descriError: AnsiString): boolean;
var
    CantidadTAGsCategoria: LongInt;
begin
    result := true;
    TAGInicial:= EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroInicialTAG').AsString, 11, '0'));
    TAGFinal  := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroFinalTAG').AsString, 11, '0'));

	try
	    with spObtenerDatosRangoTAGs, Parameters do begin
    	    Close;
            //INICIO:	20160816 CFU
            Parameters.Refresh;
            //TERMINO:	20160816 CFU
        	ParamByName('@TAGInicial').Value 			:= TAGInicial;
	        ParamByName('@TAGFinal').Value  			:= TAGFinal;
    	    ParamByName('@CategoriaTAG').Value			:= DataSet.FieldByName('CategoriaTAGs').AsString;
        	ParamByName('@CategoriaProveedor').Value	:= 1;
        	ParamByName('@CualquierUbicacion').Value	:= 1;
	        ParamByName('@CantidadTAGs').Value 			:= 0;
    	    ParamByName('@CantidadTAGsCategoria').Value := 0;
        	ParamByName('@CantidadTAGsMaestro').Value 	:= 0;
	        ParamByName('@CantidadTAGsCategoriaMaestro').Value := 0;
    	    ExecProc;

        	CantidadTAGsRango 		:= ParamByName('@CantidadTAGs').Value;
	        CantidadTAGsCategoria 	:= ParamByName('@CantidadTAGsCategoria').Value;

    	    Close
	    end;

	    if ((TAGInicial <> 0) and (TAGFinal <> 0)) and
    	    (CantidadTAGsRango <> DataSet.FieldByName('CantidadTAGS').AsInteger)
	    then begin
        	DescriError := DescriError + 'La cantidad indicada difiere de la contenida en el rango';
	        result := False
    	end;

	    if ((TAGInicial <> 0) and (TAGFinal <> 0)) and
    	    (CantidadTAGsCategoria <> DataSet.FieldByName('CantidadTAGS').AsInteger) then begin
	        DescriError := DescriError + iif(DescriError = '', '', ' - ') + 'La cantidad indicada para la categor�a difiere de la contenida en el rango';
    	    result := False
	    end;
    except
		on e: Exception do begin
        	MsgBoxErr(MSG_ERROR_CONSISTENCIA_LINEA, e.message, self.caption, MB_ICONSTOP);
	        result := False
        end
    end;

end;

function TFRecepcionTAGs.ConsistenciaDatos(DataSet: TDataSet): Boolean;
var
    cons: Boolean;
    DescriError: AnsiString;
begin
    result := True;
    DataSet.First;
    while not DataSet.EoF do begin
        cons := ConsistenciaDatosLinea(DataSet, descrierror);
        if not cons then begin
            DataSet.Edit;
            DataSet.FieldByName('Consistencia').AsString := DescriError;
            DataSet.Post
        end;
        result := result and cons;
    	DataSet.Next
	end
end;

procedure TFRecepcionTAGs.btnAceptarClick(Sender: TObject);
var
	StrAdvertencia: ANSIString;
    TodoOk: Boolean;
begin
	// Validar fecha
	if not ValidateControls([txtGuiaDespacho, txtOrdenCompra, lupProveedores, dedFechaRecepcion, dedFechaRecepcion],
	  [(trim(txtGuiaDespacho.text) <> ''), (trim(txtOrdenCOmpra.text) <> ''), lupProveedores.KeyValue <> -1, (dedFechaRecepcion.date <> Nulldate), IsValidDate(dedFechaRecepcion.text)],
	  Format(MSG_CAPTION_ACTUALIZAR,['Recepci�n de Telev�a']),
	  [MSG_ERROR_GUIA_DESPACHO, MSG_ERROR_ORDEN_COMPRA, MSG_ERROR_PROVEEDOR, MSG_ERROR_FECHA, MSG_ERROR_FECHA]) then begin
		Exit;
	end;

	if (not cdsEmbalajes.IsEmpty) and (cdsEmbalajes.State in [dsEdit, dsInsert]) then cdsEmbalajes.Post;

    if not ValidarRangosSuperpuestos(cdsEmbalajes, true) then exit;

    StrAdvertencia := '';
    if cdsEmbalajes.RecordCount <> nedCantItems.ValueInt then StrAdvertencia := 'La cantidad de embalajes ingresados no coincide con la indicada a cargar' + #13;
    if CantidadTAGs(cdsEmbalajes) <> nedCantidadTAGs.ValueInt then StrAdvertencia := StrAdvertencia + 'La cantidad de Telev�a ingresados no coincide con la indicada a cargar' + #13;

    if (StrAdvertencia <> '') And (MsgBox(StrAdvertencia + 'Desea a�n grabar los datos?', STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) <> IDYES)
    then begin
    	Exit
    end;

	Screen.Cursor := crHourGlass;
    TodoOk:= False;

    DMConnections.BaseCAC.BeginTrans;
	try
		try
		    // graba los datos
            cdsEmbalajes.DisableControls;
		    cdsEmbalajes.First;
	    	while not cdsEmbalajes.EoF do begin
	   			if (cdsEmbalajes.FieldByName ('Editable').AsBoolean) then begin
			    	with spActualizarRecepcionEnviosTAGs, Parameters do begin
                    	//INICIO:	20160812 CFU
                        Parameters.Refresh;
                        //TERMINO:	20160812 CFU
   				    	ParamByName ('@CodigoEmbalaje').Value 			:= cdsEmbalajes.FieldByName ('CodigoEmbalaje').AsInteger;
       					ParamByName ('@CodigoRecepcionEnvioTAG').Value	:= cdsEmbalajes.FieldByName ('CodigoRecepcionEnvioTAG').AsInteger;
		        		ParamByName ('@GuiaDespacho').Value 	 		:= Trim(txtGuiaDespacho.Text);
   			    		ParamByName ('@OrdenCompra').Value 				:= Trim(txtOrdenCompra.Text);
	       				ParamByName ('@CodigoProveedorTAG').Value	 	:= qryProveedoresTAGs.FieldByName('CodigoProveedorTAG').AsInteger;
   		        		ParamByName ('@FechaRecepcion').Value 			:= dedFechaRecepcion.Date;
    			    	ParamByName ('@Contenedor').Value 				:= cdsEmbalajes.FieldByName ('Contenedor').AsString;
   	    				ParamByName ('@IDContenedor').Value 			:= cdsEmbalajes.FieldByName ('IDContenedor').AsInteger;
	        			ParamByName ('@NumeroInicialTAG').Value 		:= EtiquetaToSerialNumber(PadL(cdsEmbalajes.FieldByName ('NumeroInicialTAG').AsString, 11, '0'));
       					ParamByName ('@NumeroFinalTAG').Value 			:= EtiquetaToSerialNumber(PadL(cdsEmbalajes.FieldByName ('NumeroFinalTAG').AsString, 11, '0'));
	         			ParamByName ('@CantidadTAGs').Value 			:= cdsEmbalajes.FieldByName ('CantidadTAGs').AsInteger;
		    	       	ParamByName ('@CategoriaTAG').Value 			:= cdsEmbalajes.FieldByName ('CategoriaTAGs').AsString;
       					ParamByName ('@Aceptado').Value 				:= cdsEmbalajes.FieldByName ('Aceptado').AsString;
		    	    	ParamByName ('@Usuario').Value	 				:= UsuarioSistema;
                        //INICIO	: 20160923 CFU TASK_040_CFU_20160922-TAGs_Correccion_Errores_Pruebas_RDM
                        //ParamByName ('@CodigoAlmacenDestino').Value	 	:= 1;
                        ParamByName ('@CodigoAlmacenDestino').Value	 	:= 18;
    		    		//TERMINO	: 20160923 CFU TASK_040_CFU_20160922-TAGs_Correccion_Errores_Pruebas_RDM

	                   	ExecProc
					end;

			        with spActualizarMovimientosImportacionRecepcionTAGs, Parameters do begin
		   	    		Close;
                        Parameters.Refresh;
        				ParamByName('@CodigoMovimiento').Value 	:= 0;
   		    			ParamByName('@Usuario').Value 			:= UsuarioSistema;
   	   		    		ParamByName('@ArchivoImportacion').Value:= '';
	       				ParamByName('@CantidadMovimiento').Value:= cdsEmbalajes.FieldByName ('CantidadTAGs').AsInteger;
		       			ParamByName('@TipoMovimiento').Value 	:= 2;
                        ParamByName('@CodigoCategoria').Value 	:= cdsEmbalajes.FieldByName ('CategoriaTAGs').AsInteger;
						ExecProc;
   	    		    	Close;
		        	end;
       			end;

		   		cdsEmbalajes.Next
            end;

	        TodoOk := True
        except
			on e: Exception do begin
				DMConnections.BaseCAC.RollbackTrans;
            	MsgBoxErr(MSG_ERROR_REGISTRO_RECEPCION, e.message, self.caption, MB_ICONSTOP)
            end;
        end;
	finally
        Screen.Cursor := crDefault;

        if TodoOk then begin
            DMConnections.BaseCAC.CommitTrans;

       		MsgBox('Datos grabados con �xito. Se imprimir� un reporte de recepci�n', self.caption, MB_ICONINFORMATION);
        	btnImprimirClick(Sender);
		    btnLimpiarClick(Sender)
        end;

        cdsEmbalajes.EnableControls;
    end
end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFRecepcionTAGs.ExisteEmbalaje(DS: TDataSet): Boolean;
var
	TipoEmbalaje: Byte;
    CodigoEmbalaje: LongInt;

begin
	TipoEmbalaje	:= dbgItems.Columns[0].PickList.IndexOf(DS.FieldByName('Contenedor').AsString);
    CodigoEmbalaje	:= DS.FieldByName('IDContenedor').AsInteger;

    result := FListaEmbalajesCDS.IndexOf(Trim(DS.FieldByName('Contenedor').AsString) + ' ' + DS.FieldByName('IDContenedor').AsString) <> -1;
    if result then Exit;

	case TipoEmbalaje of
    	CONST_EMBALAJE_PALLET: begin
         	result := QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select 1 From PalletsTAGs WITH (NOLOCK) where PalletsTAGs.CodigoPalletTAG = ' + IntToStr(CodigoEmbalaje) + '), 0)') <> 0;
        end;

    	CONST_EMBALAJE_CAJA: begin
        	result := QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select 1 From CajasTAGs WITH (NOLOCK) where CajasTAGs.CodigoCajaTAG = ' + IntToStr(CodigoEmbalaje) + '), 0)') <> 0;
        end;

        CONST_EMBALAJE_UNIDAD: begin
        end;
    end
end;

function TFRecepcionTAGs.ValidarIDContenedor (DataSet: TDataSet): Boolean;
begin
	result := DataSet.FieldByName('IDContenedor').AsInteger > 0;
    if not result then begin
		MsgBox(MSG_ERROR_ID_EMBALAJE_NULO, self.Caption, MB_OK + MB_ICONSTOP);
       	exit
    end;

    result := not ((Dataset.State in [dsInsert]) and ExisteEmbalaje (DataSet));
    if not result then begin
		MsgBox(MSG_ERROR_YA_EXISTE_EMBALAJE, self.Caption, MB_OK + MB_ICONSTOP);
		exit
	end;
end;

procedure TFRecepcionTAGs.cdsEmbalajesBeforePost(DataSet: TDataSet);
var
    DescriError: AnsiString;
begin
	CantidadTAGsRango := DataSet.FieldByName('CantidadTAGS').AsInteger;
   	TAGInicial := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroInicialTAG').AsString, 11, '0'));
   	TAGFinal  := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroFinalTAG').AsString, 11, '0'));

    // validaciones exclu�das al momento de la carga
    if not FCargando then begin
		if not ValidarEditable(self.caption, Dataset.FieldByName('Editable').AsBoolean, DAR_MENSAJE) then abort;
		if not ValidarContenedor (self.caption, dbgItems.Columns[0].PickList.IndexOf(DataSet.FieldByName('Contenedor').AsString), DAR_MENSAJE) then abort;
		if not ValidarIDContenedor (DataSet) then abort;
		if (DataSet.FieldByName('NumeroInicialTAG').AsFloat <> 0) and (not ValidarEtiqueta(self.caption, 'Telev�a Inicial', DataSet.FieldByName('NumeroInicialTAG').AsString, DAR_MENSAJE)) then abort;
		if (DataSet.FieldByName('NumeroFinalTAG').AsFloat <> 0) and (not ValidarEtiqueta(self.caption, 'Telev�a Final', DataSet.FieldByName('NumeroFinalTAG').AsString, DAR_MENSAJE)) then abort;
		if not ValidarCategoria (self.caption, DataSet.FieldByName('CategoriaTAGs').AsInteger, DAR_MENSAJE) then abort;
		if (TAGInicial <> 0) and (not ValidarCategoriaTAG(self.caption, 'Telev�a Inicial', TAGInicial, DataSet.FieldByName('CategoriaTAGs').AsInteger, DAR_MENSAJE)) then abort;
		if (TAGInicial <> 0) and (not ValidarCategoriaTAG(self.caption, 'Telev�a Final', TAGFinal, DataSet.FieldByName('CategoriaTAGs').AsInteger, DAR_MENSAJE)) then abort;
		if (DataSet.FieldByName('Contenedor').AsString <> CONST_CONTENEDOR_PALLET) and (not ValidarRangosTAGs(self.caption, TAGInicial, TAGFinal, NOMINADO, DAR_MENSAJE)) then abort;
		if (DataSet.FieldByName('Contenedor').AsString = CONST_CONTENEDOR_PALLET) and (not ValidarRangosTAGs(self.caption, TAGInicial, TAGFinal, (TAGInicial*TAGFinal) <> 0, DAR_MENSAJE)) then abort;
        if not ValidarCantidadTAGs (self.caption, DataSet.FieldByName('CategoriaTAGs').AsInteger, DataSet.FieldByName('CantidadTAGS').AsInteger, NOMINADO, DAR_MENSAJE) then abort;

	    if dbgItems.Columns[6].PickList.IndexOf(DataSet.FieldByName('Aceptado').AsString) = -1 then begin
			MsgBox('Valor de aceptaci�n incorrecto', self.caption, MB_OK + MB_ICONSTOP);
        	abort
	    end;

    	if (DataSet.FieldByName('Aceptado').AsString = 'N') and (TAGInicial * TAGFinal = 0) then begin
			MsgBox('S�lo puden rechazarse embalajes con rangos de Telev�a definidos/v�lidos', self.caption, MB_OK + MB_ICONSTOP);
	        abort
    	end;
    end;

    if not ConsistenciaDatosLinea(cdsEmbalajes, DescriError) then begin
        if not FCargando then begin
      		if MsgBox('Existe la siguiente inconsistencia en la informaci�n: ' + CRLF +
              descriError + CRLF + ' �Desea aceptar con la observaci�n?',
              self.caption, MB_ICONWARNING + MB_YESNO) <> idYes then begin
                dataset.fieldbyName('Consistencia').asString := DescriError;
                abort;
            end else begin
                dataset.fieldbyName('Consistencia').asString := DescriError;
            end;
        end else begin
            dataset.fieldbyName('Consistencia').asString := DescriError;
        end;
    end else
        dataset.fieldbyName('Consistencia').asString := '';
end;

procedure TFRecepcionTAGs.cdsEmbalajesNewRecord(DataSet: TDataSet);
begin
    cdsEmbalajes.FieldByName ('CodigoEmbalaje').AsInteger		:= 0;
	cdsEmbalajes.FieldByName ('CodigoRecepcionEnvioTAG').AsInteger:= 0;
	cdsEmbalajes.FieldByName ('Contenedor').AsString 			:= '';
    cdsEmbalajes.FieldByName ('NumeroInicialTAG').AsFloat 		:= 0;
	cdsEmbalajes.FieldByName ('IDContenedor').AsInteger			:= 0;
    cdsEmbalajes.FieldByName ('NumeroFinalTAG').AsFloat 		:= 0;
    cdsEmbalajes.FieldByName ('CantidadTAGs').AsInteger 		:= 0;
    cdsEmbalajes.FieldByName ('CategoriaTAGs').AsString 		:= '1';
	cdsEmbalajes.FieldByName ('Aceptado').AsString 				:= 'S';
    cdsEmbalajes.FieldByName ('Editable').AsBoolean				:= True;
end;

procedure TFRecepcionTAGs.btnImprimirClick(Sender: TObject);
var
    f: TFormRptRecepcionTAGs;
begin
    if cdsEmbalajes.State in [dsEdit, dsInsert] then cdsEmbalajes.Post;

	if not cdsEmbalajes.IsEmpty then begin
	    if cdsEmbalajes.State in [dsEdit, dsInsert] then cdsEmbalajes.Post;

	    Application.CreateForm(TFormRptRecepcionTAGs, f);
	    if not f.Inicializar (Trim(txtGuiaDespacho.Text), Trim(txtOrdenCompra.Text)) then exit;
    	if f.Ejecutar then f.Free;
    end;
end;

procedure TFRecepcionTAGs.cdsEmbalajesBeforeDelete(DataSet: TDataSet);
begin
	if not ValidarEditable(self.Caption, DataSet.FieldByName('Editable').AsBoolean, false) then begin
		MsgBox(MSG_ERROR_NO_EDITABLE, self.caption, MB_OK + MB_ICONSTOP);
        abort
    end;
    FListaEmbalajesCDS.Delete(FListaEmbalajesCDS.IndexOf(Trim(Dataset.FieldByName('Contenedor').AsString) + ' ' + Dataset.FieldByName('IDContenedor').AsString))
end;

procedure TFRecepcionTAGs.btnLimpiarClick(Sender: TObject);
begin
	cdsEmbalajes.CancelUpdates;

    txtGuiaDespacho.Text := '';
    txtOrdenCompra.Text := '';
    txtGuiaDespacho.SetFocus;
end;

procedure TFRecepcionTAGs.btnVerTeleviasClick(Sender: TObject);
var
    f:TFTAGsImportados;
begin
	if FindFormOrCreate(TFTAGsImportados, f) then
		f.Show
	else begin
        if not f.Inicializar(True) then f.Release;
	end;
end;

procedure TFRecepcionTAGs.cdsEmbalajesAfterScroll(DataSet: TDataSet);
begin
    btnBorrarLinea.Enabled	:= not cdsEmbalajes.IsEmpty;
    btnImprimir.Enabled		:= (not cdsEmbalajes.IsEmpty) and (not FNuevaRecepcion);
    btnAceptar.Enabled		:= not cdsEmbalajes.IsEmpty
end;

procedure TFRecepcionTAGs.btnBorrarLineaClick(Sender: TObject);
begin
	cdsEmbalajes.Delete
end;

function TFRecepcionTAGs.ValidarRangosSuperpuestos(
  CdsRangos: TClientDataSet; ConMsg: Boolean): Boolean;
var
	Actual: Integer;
    bm: TBookmark;
    NumeroInicialTAG,
    NumeroFinalTAG: DWORD;
begin
    result := true;
    cursor := crHourGlass;
    cdsRangos.disableControls;
    try
        with cdsRangos do begin
            first;
            while not eof do begin
                // Tomamos el registro actual para no verificar
                Actual := RecNo;
                NumeroInicialTAG := EtiquetaToSerialNumber(FieldByName('NumeroInicialTAG').asString);
                NumeroFinalTAG := EtiquetaToSerialNumber(FieldByName('NumeroFinalTAG').asString);
                bm := GetBookMark;
                try
                	if (NumeroInicialTAG <> 0) and (NumeroFinalTAG <> 0) then begin
                    	// Recorremos el dataset verificando el n�mero
                	    first;
            	        while not eof do begin
        	                if Actual = RecNo then
    	                        next
	                        else begin
                            	if ((NumeroInicialTAG >= EtiquetaToSerialNumber(FieldByName('NumeroInicialTAG').asString)) and
                        	      (NumeroInicialTAG <= EtiquetaToSerialNumber(FieldByName('NumeroFinalTAG').asString))) then begin
                	                result := false;
            	                    break;
        	                    end;

    	                        if ((NumeroFinalTAG >= EtiquetaToSerialNumber(FieldByName('NumeroInicialTAG').asString)) and
	                              (NumeroFinalTAG <= EtiquetaToSerialNumber(FieldByName('NumeroFinalTAG').asString))) then begin
                        	        result := false;
                    	            break;
                	            end;
            	            next;
        	                end;
    	                end;
	                end;

                    GotoBookMark(bm);
                    next;
                finally
                    FreeBookMark(bm);
                end;
            end;
        end;
    finally
        cdsRangos.EnableControls;
        cursor := crDefault;
    end;

	if ConMsg and (not result) then MsgBox(MSG_ERROR_RANGO_SUPERPUESTO, MSG_CAPTION_MOVIMIENTOS_STOCK, MB_OK + MB_ICONSTOP);
end;

procedure TFRecepcionTAGs.dbgItemsKeyPress(Sender: TObject; var Key: Char);
var
    xDbGrid: TNewStringGrid;           //Revision 1
    xInplaceEditor: TNewInplaceEditor; //Revision 1
begin
	if Key in ['-', '+'] then Key := #0 ;
    //Revision 1
    if ORD(key) = 22 then Key := #0; //Evita el comando ctrl+v para pegar texto en los campos
    xdbgrid := TNewStringGrid(dbgitems);
    xInplaceEditor := TNewInplaceEditor(xdbgrid.InplaceEditor);
    Case xdbgrid.SelectedField.Index of
        0: xInplaceEditor.SetMaxLength(8);  //tipo embalaje
        1: xInplaceEditor.SetMaxLength(8);  //id embalaje
        2: xInplaceEditor.SetMaxLength(12); //tag inicial
        3: xInplaceEditor.SetMaxLength(12); //tag final
        4: xInplaceEditor.SetMaxLength(9);  //cantidad televias
        5: xInplaceEditor.SetMaxLength(3);  //categoria interurbana
        6: xInplaceEditor.SetMaxLength(1);  //aceptado
        7: xInplaceEditor.SetMaxLength(100); //descripcion
    End;
    //Fin de Revision 1
end;

procedure TFRecepcionTAGs.dbgItemsExit(Sender: TObject);
begin
    if cdsEmbalajes.State in [dsEdit, dsInsert] then cdsEmbalajes.Post;
end;

function TNewStringGrid.GetEditor: TInplaceEdit;
begin
    Result := InPlaceEditor;
end;

end.




