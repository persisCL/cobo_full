object FSaldosAlmacenesTAGs: TFSaldosAlmacenesTAGs
  Left = 75
  Top = 209
  Caption = 'Saldos de Telev'#237'as en Bodegas y Almacenes'
  ClientHeight = 292
  ClientWidth = 800
  Color = clBtnFace
  Constraints.MinHeight = 330
  Constraints.MinWidth = 816
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object DBListEx1: TDBListEx
    Left = 0
    Top = 61
    Width = 800
    Height = 190
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 200
        Header.Caption = 'Bodega'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Bodega'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 200
        Header.Caption = 'Almac'#233'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Almacen'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 200
        Header.Caption = 'Categor'#237'a'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Categoria'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Saldo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'Saldo'
      end>
    DataSource = dsSaldosAlmacenes
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 800
    Height = 61
    Align = alTop
    Caption = ' Filtros '
    TabOrder = 1
    ExplicitTop = -6
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 45
      Height = 13
      Caption = 'Bodegas:'
    end
    object Label2: TLabel
      Left = 217
      Top = 16
      Width = 55
      Height = 13
      Caption = 'Almacenes:'
    end
    object Label3: TLabel
      Left = 481
      Top = 16
      Width = 53
      Height = 13
      Caption = 'Categorias:'
    end
    object cbBodegas: TVariantComboBox
      Left = 24
      Top = 31
      Width = 173
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbBodegasChange
      Items = <>
    end
    object cbAlmacenes: TVariantComboBox
      Left = 217
      Top = 31
      Width = 244
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object cbCategorias: TVariantComboBox
      Left = 481
      Top = 31
      Width = 192
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object btnFiltrar: TButton
      Left = 693
      Top = 29
      Width = 75
      Height = 25
      Caption = '&Filtrar'
      TabOrder = 3
      OnClick = btnFiltrarClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 251
    Width = 800
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 800
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        800
        41)
      object btnSalir: TButton
        Left = 716
        Top = 6
        Width = 75
        Height = 29
        Anchors = [akRight]
        Caption = '&Salir'
        TabOrder = 0
        OnClick = btnSalirClick
      end
    end
  end
  object dsSaldosAlmacenes: TDataSource
    DataSet = ObtenerSaldosAlmacenes
    Left = 370
    Top = 127
  end
  object ObtenerSaldosAlmacenes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerSaldosAlmacenes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoBodega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 399
    Top = 128
  end
end
