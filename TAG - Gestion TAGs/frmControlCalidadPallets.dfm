object FControlCalidadPallets: TFControlCalidadPallets
  Left = 230
  Top = 81
  Caption = 'Control de Calidad de Pallets'
  ClientHeight = 532
  ClientWidth = 786
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 760
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 786
    Height = 121
    Align = alTop
    TabOrder = 0
    object Label2: TLabel
      Left = 378
      Top = 15
      Width = 83
      Height = 13
      Caption = 'Almac'#233'n Destino:'
      Visible = False
    end
    object gbDatos: TGroupBox
      Left = 6
      Top = 9
      Width = 367
      Height = 103
      Caption = ' Datos del Pallet '
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 21
        Width = 60
        Height = 13
        Caption = 'I&D del Pallet:'
      end
      object Label4: TLabel
        Left = 12
        Top = 51
        Width = 109
        Height = 13
        Caption = 'Cantidad total de C&ajas'
        FocusControl = txtCantidadCajas
      end
      object Label6: TLabel
        Left = 12
        Top = 75
        Width = 128
        Height = 13
        Caption = 'Cantidad total de &Telev'#237'as:'
        FocusControl = txtCantidadTAGs
      end
      object txtCantidadCajas: TNumericEdit
        Left = 147
        Top = 48
        Width = 46
        Height = 21
        TabOrder = 1
        OnChange = txtCantidadCajasChange
      end
      object txtCantidadTAGs: TNumericEdit
        Left = 147
        Top = 72
        Width = 78
        Height = 21
        TabStop = False
        TabOrder = 2
        OnChange = txtCantidadTAGsChange
      end
      object txtIDPallet: TNumericEdit
        Left = 78
        Top = 18
        Width = 79
        Height = 21
        TabOrder = 0
        OnChange = txtIDPalletChange
      end
    end
    object gbControl: TGroupBox
      Left = 378
      Top = 39
      Width = 232
      Height = 73
      Caption = ' Control de Ingreso '
      TabOrder = 1
      object Label7: TLabel
        Left = 12
        Top = 24
        Width = 29
        Height = 13
        Caption = 'Cajas:'
        FocusControl = txtCantidadCajas
      end
      object Label8: TLabel
        Left = 12
        Top = 48
        Width = 45
        Height = 13
        Caption = 'Telev'#237'as:'
        FocusControl = txtCantidadTAGs
      end
      object lblControlCajas: TLabel
        Left = 63
        Top = 24
        Width = 43
        Height = 13
        Caption = '# cajas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblControlTAGs: TLabel
        Left = 63
        Top = 48
        Width = 44
        Height = 13
        Caption = '# TAGs'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object VariantComboBox1: TVariantComboBox
      Left = 465
      Top = 12
      Width = 214
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      Visible = False
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 121
    Width = 786
    Height = 373
    Align = alClient
    TabOrder = 1
    object dbgCajas: TDBGrid
      Left = 1
      Top = 1
      Width = 784
      Height = 371
      Align = alClient
      DataSource = dsCajas
      Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CodigoCajaTAG'
          Title.Caption = 'ID Caja'
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NumeroInicialTAG'
          Title.Caption = 'N'#186' Telev'#237'a Inicial'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NumeroFinalTAG'
          Title.Caption = 'N'#186' Telev'#237'a Final'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CantidadTAGs'
          Title.Caption = 'Cantidad Telev'#237'as'
          Width = 93
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CategoriaTAGs'
          Title.Caption = 'Categor'#237'a'
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstadoControl'
          PickList.Strings = (
            'P'
            'A'
            'R')
          Title.Caption = 'Estado'
          Width = 39
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Consistencia'
          ReadOnly = True
          Title.Caption = 'Observaciones/Consistencia c/Archivo Proveedor'
          Width = 320
          Visible = True
        end>
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 494
    Width = 786
    Height = 38
    Align = alBottom
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 2
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 786
      Height = 38
      Align = alClient
      Anchors = [akRight]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        786
        38)
      object DPSButton1: TButton
        Left = 166
        Top = 7
        Width = 138
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Ver Telev'#237'as pendientes'
        TabOrder = 0
        OnClick = DPSButton1Click
      end
      object btnControlManual: TButton
        Left = 312
        Top = 7
        Width = 97
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Control &Manual'
        TabOrder = 1
        OnClick = btnControlManualClick
      end
      object btnImprimir: TButton
        Left = 417
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = 'I&mprimir'
        TabOrder = 2
        OnClick = btnImprimirClick
      end
      object btnLimpiar: TButton
        Left = 504
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = '&Limpiar'
        TabOrder = 3
        OnClick = btnLimpiarClick
      end
      object btnAceptar: TButton
        Left = 591
        Top = 6
        Width = 100
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = '&Guardar Datos'
        Default = True
        TabOrder = 4
        OnClick = btnAceptarClick
      end
      object btnCancelar: TButton
        Left = 699
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Cancelar'
        TabOrder = 5
        OnClick = btnCancelarClick
      end
      object btnBorrarLinea: TButton
        Left = 85
        Top = 6
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Borrar l'#237'nea'
        TabOrder = 6
        OnClick = btnBorrarLineaClick
      end
    end
  end
  object cdsCajas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoEmbalaje'
        DataType = ftInteger
      end
      item
        Name = 'CodigoCajaTAG'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'NumeroInicialTAG'
        Attributes = [faRequired]
        DataType = ftFloat
      end
      item
        Name = 'NumeroFinalTAG'
        Attributes = [faRequired]
        DataType = ftFloat
      end
      item
        Name = 'CantidadTAGs'
        Attributes = [faRequired]
        DataType = ftLargeint
      end
      item
        Name = 'CategoriaTAGs'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'EstadoControl'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Consistencia'
        DataType = ftString
        Size = 500
      end
      item
        Name = 'Editable'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    PacketRecords = 0
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = 0
      end
      item
        DataType = ftInteger
        Name = '@GuiaDespacho'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@OrdenCompra'
        ParamType = ptInput
        Value = Null
      end
      item
        DataType = ftInteger
        Name = '@CodigoProveedor'
        ParamType = ptInput
        Value = Null
      end>
    StoreDefs = True
    BeforePost = cdsCajasBeforePost
    AfterPost = cdsCajasAfterPost
    BeforeDelete = cdsCajasBeforeDelete
    AfterDelete = cdsCajasAfterDelete
    BeforeScroll = cdsCajasBeforeScroll
    AfterScroll = cdsCajasBeforeScroll
    OnNewRecord = cdsCajasNewRecord
    Left = 294
    Top = 333
  end
  object dsCajas: TDataSource
    DataSet = cdsCajas
    Left = 264
    Top = 333
  end
  object spObtenerDatosPalletTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosPalletTAGs;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPalletTAG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 264
    Top = 303
  end
  object spObtenerDatosRangoTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosRangoTAGs;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TAGInicial'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TAGFinal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CategoriaProveedor'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CualquierUbicacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CantidadTAGs'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadTAGsCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadTAGsMaestro'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadTAGsCategoriaMaestro'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 264
    Top = 363
  end
  object spObtenerDatosCajaTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosCajaTAGs;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmbalajePadre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 264
    Top = 273
  end
  object spRegistrarControlCalidad: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarControlCalidad;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmbalaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmbalajePadre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCajaTAG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroInicialTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroFinalTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadTAGs'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadTAGsAceptados'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@EstadoControl'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@EstadoSituacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenTAG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 264
    Top = 393
  end
  object cdsTAGs: TClientDataSet
    Aggregates = <>
    Filtered = True
    FieldDefs = <
      item
        Name = 'ContractSerialNumber'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Estado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'IDCaja'
        DataType = ftInteger
      end
      item
        Name = 'RazonRechazo'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    BeforePost = cdsTAGsBeforePost
    OnNewRecord = cdsTAGsNewRecord
    Left = 264
    Top = 244
  end
  object spVerificarTAGEnArchivos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarTAGEnArchivos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 1
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = '69206093'
      end>
    Left = 264
    Top = 453
  end
  object spRegistrarControlCalidadManual: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarControlCalidadManual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmbalaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmbalajePadre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCajaTAG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@EstadoControl'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@EstadoSituacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenTAG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 264
    Top = 423
  end
end
