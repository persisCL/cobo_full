unit frmImagenesSPC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls,

  ImgTypes, UtilDB, DMConnection;

type
  TImagenesSPCForm = class(TForm)
    pcImages: TPageControl;
    tsFrontal1: TTabSheet;
    imgFrontal1: TImage;
    tsFrontal2: TTabSheet;
    imgFrontal2: TImage;
    tsContexto1: TTabSheet;
    imgContexto1: TImage;
    tsContexto2: TTabSheet;
    imgContexto2: TImage;
    tsContexto3: TTabSheet;
    imgContexto3: TImage;
    tsContexto4: TTabSheet;
    imgContexto4: TImage;
    tsContexto5: TTabSheet;
    imgContexto5: TImage;
    tsContexto6: TTabSheet;
    imgContexto6: TImage;
  private
    { Private declarations }
  public
    { Public declarations }

    function Inicializar(NumCorrCA, RegistrationAccessibility: Int64): Boolean;
  end;

var
  ImagenesSPCForm: TImagenesSPCForm;

implementation

{$R *.dfm}

function TImagenesSPCForm.Inicializar(NumCorrCA: Int64; RegistrationAccessibility: Int64): Boolean;
    var
        lImagen: string;
begin
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiFrontalSPC1]) = RegistrationAccessibilityImagen[tiFrontalSPC1] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiFrontalSPC1]]));

        imgFrontal1.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiFrontalSPC2]) = RegistrationAccessibilityImagen[tiFrontalSPC2] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiFrontalSPC2]]));

        imgFrontal2.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC1]) = RegistrationAccessibilityImagen[tiContextoSPC1] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC1]]));

        imgContexto1.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC2]) = RegistrationAccessibilityImagen[tiContextoSPC2] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC2]]));

        imgContexto2.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC3]) = RegistrationAccessibilityImagen[tiContextoSPC3] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC3]]));

        imgContexto3.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC4]) = RegistrationAccessibilityImagen[tiContextoSPC4] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC4]]));

        imgContexto4.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC5]) = RegistrationAccessibilityImagen[tiContextoSPC5] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC5]]));

        imgContexto5.Picture.LoadFromFile(lImagen);
    end;
    if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC6]) = RegistrationAccessibilityImagen[tiContextoSPC6] then begin

        lImagen := QueryGetValue(
                            DMConnections.BaseCAC,
                            Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC6]]));

        imgContexto6.Picture.LoadFromFile(lImagen);
    end;

end;

end.
