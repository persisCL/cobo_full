object FRecepcionTAGs: TFRecepcionTAGs
  Left = 151
  Top = 120
  Caption = 'Recepci'#243'n de Telev'#237'as'
  ClientHeight = 559
  ClientWidth = 841
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 740
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 841
    Height = 163
    Align = alTop
    TabOrder = 0
    object Label5: TLabel
      Left = 374
      Top = 12
      Width = 103
      Height = 13
      Caption = '&Fecha de Recepci'#243'n:'
      FocusControl = dedFechaRecepcion
    end
    object dedFechaRecepcion: TDateEdit
      Left = 483
      Top = 9
      Width = 103
      Height = 21
      AutoSelect = False
      Color = clWhite
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object gbDatos: TGroupBox
      Left = 6
      Top = 9
      Width = 343
      Height = 145
      Caption = ' Datos de Env'#237'o '
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 18
        Width = 94
        Height = 13
        Caption = 'Gu'#237'a de &Despacho:'
        FocusControl = txtGuiaDespacho
      end
      object Label3: TLabel
        Left = 12
        Top = 66
        Width = 52
        Height = 13
        Caption = '&Proveedor:'
        FocusControl = lupProveedores
      end
      object Label2: TLabel
        Left = 12
        Top = 42
        Width = 86
        Height = 13
        Caption = '&Orden de Compra:'
        FocusControl = txtOrdenCompra
      end
      object Label4: TLabel
        Left = 12
        Top = 93
        Width = 183
        Height = 13
        Caption = 'Cantidad total de &Embalajes a ingresar:'
        FocusControl = nedCantItems
      end
      object Label6: TLabel
        Left = 12
        Top = 117
        Width = 172
        Height = 13
        Caption = 'Cantidad total de &Telev'#237'a a ingresar:'
        FocusControl = nedCantidadTAGs
      end
      object lupProveedores: TDBLookupComboBox
        Left = 111
        Top = 63
        Width = 220
        Height = 21
        KeyField = 'CodigoProveedorTAG'
        ListField = 'Descripcion'
        ListSource = dsProveedoresTAGs
        TabOrder = 2
      end
      object txtOrdenCompra: TEdit
        Left = 111
        Top = 39
        Width = 76
        Height = 21
        CharCase = ecUpperCase
        Color = clWhite
        TabOrder = 1
        OnChange = txtGuiaDespachoChange
      end
      object txtGuiaDespacho: TEdit
        Left = 111
        Top = 15
        Width = 76
        Height = 21
        CharCase = ecUpperCase
        Color = clWhite
        TabOrder = 0
        OnChange = txtGuiaDespachoChange
      end
      object nedCantItems: TNumericEdit
        Left = 198
        Top = 90
        Width = 46
        Height = 21
        TabOrder = 3
        OnChange = nedCantItemsChange
      end
      object nedCantidadTAGs: TNumericEdit
        Left = 198
        Top = 114
        Width = 78
        Height = 21
        TabOrder = 4
        OnChange = nedCantidadTAGsChange
      end
    end
    object gbControl: TGroupBox
      Left = 354
      Top = 81
      Width = 322
      Height = 73
      Caption = ' Control de Ingreso '
      TabOrder = 2
      object Label7: TLabel
        Left = 12
        Top = 24
        Width = 111
        Height = 13
        Caption = 'Cantidad de Embalajes:'
        FocusControl = nedCantItems
      end
      object Label8: TLabel
        Left = 12
        Top = 48
        Width = 100
        Height = 13
        Caption = 'Cantidad de Telev'#237'a:'
        FocusControl = nedCantidadTAGs
      end
      object lblControlItems: TLabel
        Left = 135
        Top = 24
        Width = 84
        Height = 13
        Caption = 'lblControlItems'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblControlTAGs: TLabel
        Left = 135
        Top = 48
        Width = 85
        Height = 13
        Caption = 'lblControlTAGs'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 163
    Width = 841
    Height = 358
    Align = alClient
    TabOrder = 1
    object dbgItems: TDBGrid
      Left = 1
      Top = 1
      Width = 839
      Height = 356
      Align = alClient
      DataSource = dsEmbalajes
      Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgCancelOnExit]
      PopupMenu = PopupEmpty
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnExit = dbgItemsExit
      OnKeyPress = dbgItemsKeyPress
      Columns = <
        item
          Expanded = False
          FieldName = 'Contenedor'
          PickList.Strings = (
            'PALLET'
            'CAJA')
          Title.Caption = 'Tipo Embalaje'
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDContenedor'
          Title.Caption = 'ID Embalaje'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NumeroInicialTAG'
          Title.Caption = 'N'#186' Telev'#237'a Inicial'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NumeroFinalTAG'
          Title.Caption = 'N'#186' Telev'#237'a Final'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CantidadTAGs'
          Title.Caption = 'Cantidad Telev'#237'a'
          Width = 95
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CategoriaTAGs'
          Title.Caption = 'Categor'#237'a Interurbana'
          Width = 111
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Aceptado'
          PickList.Strings = (
            'S'
            'N')
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Consistencia'
          ReadOnly = True
          Title.Caption = 'Observaciones/Consistencia c/Archivo Proveedor'
          Width = 327
          Visible = True
        end>
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 521
    Width = 841
    Height = 38
    Align = alBottom
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 2
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 841
      Height = 38
      Align = alClient
      Anchors = [akLeft, akTop, akBottom]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        841
        38)
      object btnVerTelevias: TButton
        Left = 325
        Top = 7
        Width = 138
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Ver Telev'#237'as disponibles'
        TabOrder = 0
        OnClick = btnVerTeleviasClick
      end
      object btnImprimir: TButton
        Left = 470
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = 'I&mprimir'
        TabOrder = 1
        OnClick = btnImprimirClick
      end
      object btnLimpiar: TButton
        Left = 557
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = '&Limpiar'
        TabOrder = 2
        OnClick = btnLimpiarClick
      end
      object btnAceptar: TButton
        Left = 644
        Top = 6
        Width = 100
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = '&Guardar Datos'
        Default = True
        TabOrder = 3
        OnClick = btnAceptarClick
      end
      object btnCancelar: TButton
        Left = 752
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Cancelar'
        TabOrder = 4
        OnClick = btnCancelarClick
      end
      object btnBorrarLinea: TButton
        Left = 242
        Top = 7
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Borrar l'#237'nea'
        TabOrder = 5
        OnClick = btnBorrarLineaClick
      end
    end
  end
  object cdsEmbalajes: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Contenedor'
        Attributes = [faRequired]
        DataType = ftString
        Size = 6
      end
      item
        Name = 'IDContenedor'
        Attributes = [faRequired]
        DataType = ftLargeint
      end
      item
        Name = 'NumeroInicialTAG'
        Attributes = [faRequired]
        DataType = ftFloat
      end
      item
        Name = 'NumeroFinalTAG'
        Attributes = [faRequired]
        DataType = ftFloat
      end
      item
        Name = 'CantidadTAGs'
        Attributes = [faRequired]
        DataType = ftLargeint
      end
      item
        Name = 'CategoriaTAGs'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Aceptado'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Consistencia'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'CodigoEmbalaje'
        DataType = ftInteger
      end
      item
        Name = 'CodigoRecepcionEnvioTAG'
        DataType = ftInteger
      end
      item
        Name = 'Editable'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    BeforePost = cdsEmbalajesBeforePost
    AfterPost = cdsEmbalajesAfterPost
    BeforeDelete = cdsEmbalajesBeforeDelete
    AfterDelete = cdsEmbalajesAfterDelete
    BeforeScroll = cdsEmbalajesAfterScroll
    AfterScroll = cdsEmbalajesAfterScroll
    OnNewRecord = cdsEmbalajesNewRecord
    Left = 490
    Top = 369
  end
  object dsProveedoresTAGs: TDataSource
    DataSet = qryProveedoresTAGs
    Left = 267
    Top = 33
  end
  object qryProveedoresTAGs: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM ProveedoresTAGs WITH (NOLOCK)'
      'ORDER BY Descripcion')
    Left = 297
    Top = 33
  end
  object dsEmbalajes: TDataSource
    DataSet = cdsEmbalajes
    Left = 252
    Top = 369
  end
  object spObtenerDatosRecepcionTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosRecepcionTAGs;1'
    Parameters = <>
    Left = 252
    Top = 339
  end
  object spObtenerDatosRangoTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosRangoTAGs;1'
    Parameters = <>
    Left = 252
    Top = 399
  end
  object spActualizarRecepcionEnviosTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'ActualizarRecepcionEnviosTAGs'
    Parameters = <>
    Left = 252
    Top = 429
  end
  object spCantidadTAGsPendientesControlCalidad: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CantidadTAGsPendientesControlCalidad;1'
    Parameters = <>
    Left = 252
    Top = 459
  end
  object spActualizarMovimientosImportacionRecepcionTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMovimientosImportacionRecepcionTAGs'
    Parameters = <>
    Left = 252
    Top = 489
  end
  object PopupEmpty: TPopupMenu
    Left = 280
    Top = 400
  end
end
