unit frmListaPuntosEntrega;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, DB, ADODB, ListBoxEx, DBListEx, UtilProc,
  StrUtils, UtilDB, PeaProcs, VariantComboBox;

type
  TFormListaPuntosEntrega = class(TForm)
    btnCancelar: TDPSButton;
    btnAceptar: TDPSButton;
    cbPuntosEntrega: TVariantComboBox;
    ckbDetalle: TCheckBox;
    Label1: TLabel;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
	FPuntoEntrega: Integer;
    FDetallar : Boolean;
  public
    { Public declarations }
	function Inicializar(Detallar: Boolean; PuntoEntrega: Integer): Boolean;
	property PuntoEntrega: Integer Read FPuntoEntrega Write FPuntoEntrega;
	property Detallar: Boolean Read FDetallar Write FDetallar;
  end;

var
  FormListaPuntosEntrega: TFormListaPuntosEntrega;

implementation

uses DMConnection;

{$R *.dfm}

function TFormListaPuntosEntrega.Inicializar(Detallar: Boolean; PuntoEntrega: Integer): Boolean;
begin
   	CenterForm(Self);

	FPuntoEntrega := PuntoEntrega;
    FDetallar := Detallar;

    CargarPuntosEntrega(DMConnections.BaseCAC, cbPuntosEntrega, True, FPuntoEntrega);

    Result := True;
end;

procedure TFormListaPuntosEntrega.btnAceptarClick(Sender: TObject);
begin
	FPuntoEntrega := cbPuntosEntrega.Value;
    FDetallar := ckbDetalle.Checked;

    Close
end;

procedure TFormListaPuntosEntrega.btnCancelarClick(Sender: TObject);
begin
	Close
end;

end.
